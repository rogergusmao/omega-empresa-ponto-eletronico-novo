import * as React from 'react';
import { Modal } from 'react-bootstrap';

import { PainelFlutuante } from 'lib/uiComponente';
import { ContextoGenerico } from 'lib/contexto';
import { Helper } from 'aplicacao/comum/geral';
import { Usuario } from 'aplicacao/modulo/mapa/model';

export class DetalhesUsuario extends PainelFlutuante<DetalhesUsuario.DetalhesUsuarioProps>
{        
    public setDefaultBody() :void
    {
        let modelUsuario = this.props.modelUsuario;
        this.bodyContent = (           
            <div className="corpo-modal-detalhes">   
                <p><b>Nome do Usuário:</b>{modelUsuario.nomeUsuario}</p>                  
                <p><b>Identificador do Usuário:</b> {modelUsuario.idUsuario}</p>
                <p><b>Sexo:</b> {Helper.getSexoAsString(modelUsuario.sexoUsuario)}</p>
                <p><b>Posição em:</b> {DateUtil.getFormattedDateTime(modelUsuario.dataPosicao)}</p>    
            </div>                           
        );

    }
    
    public setDefaultHeader(): void
    {
        let modelUsuario = this.props.modelUsuario;
        this.headerContent = (
            <Modal.Title id="contained-modal-title-lg">
                {modelUsuario.nomeUsuario}
            </Modal.Title>
        );
    }

}

export namespace DetalhesUsuario
{
    export interface DetalhesUsuarioProps extends PainelFlutuante.PainelFlutuanteProps
    {
        modelUsuario: Usuario;
    }    
}