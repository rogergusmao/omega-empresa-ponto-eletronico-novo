import { DomContainer, MapaElemento, ElementoVisualizacao } from "lib/uiComponente";
import { ContextoMapa } from "lib/contexto";
import { MapaTemplate } from 'lib/uiTemplate';

export class Mapa<T extends ContextoMapa> extends DomContainer<T>
{
    private mainMap: google.maps.Map;
    private componentesMapa: Array<MapaElemento>;
    private template: MapaTemplate;
    private tiposElementosVisiveis :Array<ElementoVisualizacao.Tipo>

    public mainMapDiv: JQuery;
    public mainMapOptions: google.maps.MapOptions;
    
    public constructor(contexto: T)
    { 
        super(contexto, contexto.mapContainerSelector);

        this.mainMapOptions = {
            panControl: false,
            zoomControl: true,
            mapTypeControl: false,
            scaleControl: false,
            streetViewControl: false,
            overviewMapControl: true
        };

        this.mainMapDiv = $(contexto.mapContainerSelector);
        this.componentesMapa = new Array<MapaElemento>();
    }

    public getGoogleMapInstance()
    {
        return this.mainMap;
    }

    public render()
    {
        var me = this;
        me.mainMap = new google.maps.Map(me.$()[0], me.mainMapOptions);
        me.fixMainMapSize();
    }

    public isRendered()
    {
        return this.mainMap != null && this.mainMap instanceof google.maps.Map;
    }

    public setDefaultValues() : void
    {
        this.setDefaultCenter();
        this.setDefaultZoom();
    }

    public getDefaultZoom(): number
    {
        return 5;
    }

    public setTiposElementosVisiveis(tiposElementosVisiveis :Array<ElementoVisualizacao.Tipo>) :void
    {
        this.tiposElementosVisiveis = tiposElementosVisiveis;
        this.refreshTiposElementosVisiveis();
    }

    private refreshTiposElementosVisiveis() :void
    {
        let tiposElementosVisiveis = this.tiposElementosVisiveis;
        
        if(this.isRendered())
        {
            for(let componente of this.componentesMapa)
            {
                if(componente.isRendered())
                {
                    if(tiposElementosVisiveis.indexOf(componente.getTipoElemento()) > -1)
                    {
                        componente.setVisible(true);
                    }
                    else
                    {
                        componente.setVisible(false);
                    }
                }
            }
    
            if(tiposElementosVisiveis.indexOf('estabelecimento') > -1)
            {
                this.setInternalMapElementVisibility('poi.business', true);
            }
            else
            {
                this.setInternalMapElementVisibility('poi.business', false);
            }
    
            if(tiposElementosVisiveis.indexOf('estrada')  > -1)
            {
                this.setInternalMapElementVisibility('road.highway', true);
            }
            else
            {
                this.setInternalMapElementVisibility('road.highway', false);
            }
    
            if(tiposElementosVisiveis.indexOf('rua')  > -1)
            {
                this.setInternalMapElementVisibility('road.arterial', true);
                this.setInternalMapElementVisibility('road.local', true);
            }
            else
            {
                this.setInternalMapElementVisibility('road.arterial', false);
                this.setInternalMapElementVisibility('road.local', false);
            }
    
            this.refreshMapLayers();
        }
        
    }

    public refreshMapLayers()
    {
        if(this.isRendered())
        {
            this.mainMap.setZoom(this.mainMap.getZoom());
        }
    }

    public setDefaultZoom()
    {
        var defaultZoom = this.getDefaultZoom();
        this.mainMapOptions.zoom = defaultZoom;
        if(this.isRendered())
        {
            this.mainMap.setZoom(defaultZoom);
        }
    }

    public getCenter(): google.maps.LatLng
    {
        return this.isRendered() ? this.mainMap.getCenter() : null;
    }

    public setCenter(latLng :google.maps.LatLng) : void
    {
        this.mainMapOptions.center = latLng;
        if(this.isRendered())
        {
            this.mainMap.setCenter(latLng);
        }
    }

    public getDefaultCenter(): google.maps.LatLng
    {
        return new google.maps.LatLng(-10.333333, -53.200000);
    }

    public setDefaultCenter(): void
    {
        var defaultCenter = this.getDefaultCenter();
        this.mainMapOptions.center = defaultCenter;

        if(this.isRendered())
        {
            this.mainMap.setCenter(defaultCenter);
        }
    }

    public setTemplate(template :MapaTemplate) : void
    {        
        let styles = template.getStyles();
        this.mainMapOptions.styles = styles;

        if(this.isRendered() && (this.template == null || this.template.nomeTemplate != template.nomeTemplate))
        {
            this.mainMap.set('styles', styles);
            this.refreshTiposElementosVisiveis();
        }

        this.template = template;
    }

    private setInternalMapElementVisibility(tipoElemento: google.maps.MapTypeStyleFeatureType, visible :boolean)
    {
        let currentStyles :google.maps.MapTypeStyle[] = this.mainMap.get('styles');
        let estiloEncontrado = false;
        let googleMapsVisibility = visible ? 'on' : 'off';

        for(let currentStyle of currentStyles)
        {
            if(currentStyle.featureType == tipoElemento)
            {
                for(let styler of currentStyle.stylers)
                {
                    if(styler.visibility != null)
                    {
                        styler.visibility = googleMapsVisibility;
                        estiloEncontrado = true;
                        break;
                    }                    
                }

                if(!estiloEncontrado)
                {
                    currentStyle.stylers.push({ visibility: googleMapsVisibility });
                    estiloEncontrado = true;
                }
            }

            if(estiloEncontrado)
            {
                break;
            }
        }

        if(!estiloEncontrado)
        {
            currentStyles.push({
                featureType: tipoElemento,
                stylers: [{visibility: googleMapsVisibility}]
            });
        }

        this.mainMap.set('styles', currentStyles);
    }

    public addResizeListener() : void
    {
        $(window).resize(() =>
        {
            this.fixMainMapSize();
        });
    }

    public fitBounds()
    {
        if(this.componentesMapa.length > 0)
        {
            let latLngBounds = new google.maps.LatLngBounds();
            for(let componente of this.componentesMapa)
            {
                for(let latLng of componente.elementsLatLngs)
                {
                    latLngBounds.extend(latLng);
                }
    
            }
    
            this.mainMap.fitBounds(latLngBounds);
        }
    }

    public fixMainMapSize(): void
    {
        var maxHeight: number = DomContainer.getMaxAllowedHeight();
        var maxWidth: number = DomContainer.getMaxAllowedWidth();

        this.$root('.over-main-map, .below-main-map').each(function ()
        {
            maxHeight -= $(this).outerHeight();
        });

        this.mainMapDiv.height(maxHeight - 1);

        this.$root('.left-main-map, .right-main-map').each(function ()
        {
            maxWidth -= $(this).outerWidth();
        });

        this.mainMapDiv.width(maxWidth);

        google.maps.event.trigger(this.mainMap, "resize");
    }

    public addComponente(elemento :MapaElemento)
    {
        this.componentesMapa.push(elemento);
    }

    public removeAllComponentes(tipo : typeof MapaElemento)
    {
        for(let i = this.componentesMapa.length; i >-0; i--)
        {
            var componente = this.componentesMapa[i];
            if(typeof componente == tipo.toString())
            {
                if(componente.isRendered())
                {
                    componente.removeFromGoogleMap();
                }
                this.componentesMapa.splice(i, 1);
            }
        }
    }

}