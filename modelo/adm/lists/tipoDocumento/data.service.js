/*

 Arquivo gerado atrav�s de gerador de c�digo em 07/01/2018 as 15:54:13.
 Para que o arquivo n�o seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

 Tabela correspondente: tipo_documento
 Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

 */

returnObject.getAllComboBoxesDataForTipoDocumentoFilter = function ()
{
    return $http({
        url: baseURLAdm + "/webservice.php?class=EXTDAO_Tipo_documento&jsonOutput=true&action=__getFilterComboBoxesData",
        method: "POST",
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    });

};

returnObject.getTipoDocumentoList = function (listParams)
{
    listParams.listMappingType = Constants.ListMappingType.GRID;
    return $http.post(baseURLAdm + "/webservice.php?class=EXTDAO_Tipo_documento&action=__getList", listParams);
};

returnObject.removeRecords = function (recordsPKs)
{
    return $http.post(baseURLAdm + "/webservice.php?class=EXTDAO_Tipo_documento&action=remove", recordsPKs);
};



