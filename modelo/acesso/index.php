<?php

include_once '../recursos/php/constants.php';
include_once '../recursos/php/database_config.php';
include_once '../client_area/imports/header.php';
include_once '../recursos/php/funcoes.php';
include_once '../client_area/imports/instancias.php';
include_once '../adm/imports/lingua.php';
include_once '../client_area/imports/sessao.php';
include_once '../recursos/libs/pag_seguro/PagSeguroLibrary.php';

include_once '../recursos/classes/class/Javascript.php';
include_once '../recursos/classes/class/Ajax.php';
include_once '../recursos/classes/class/AES.php';
include_once '../recursos/classes/class/Crypt.php';
include_once '../recursos/classes/class/Seguranca_Pagamento.php';
include_once '../recursos/classes/class/Generic_Argument.php';
include_once '../recursos/classes/EXTDAO/EXTDAO_Acesso_cliente.php';

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Frameset//EN"
    "http://www.w3.org/TR/html4/frameset.dtd">
<html>
<head>
    <title><?= TITULO_PAGINAS_CLIENTE ?></title>

    <?= Helper::getTagDoFavicon(); ?>

    <?= Javascript::importarTodasAsBibliotecas(); ?>

    <?= Helper::carregarCssFlatty(); ?>

    <?= Helper::imprimirComandoJavascript("var dialogo = false"); ?>


</head>

<body class='contrast-red without-footer'>

<?php

Javascript::imprimirCorpoDoTooltip();

Helper::includePHP(1, "client_area/imports/mensagens.php");
Ajax::imprimirCorpoDaDivDeRetornoAjax();

include("imports/topo_pagina.php");
include("imports/modal_carregando.php");

?>
<div id="divAlterarSenha">
</div>
<div id='wrapper'>
    <? include("imports/menu_esquerdo.php"); ?>
    <section id='content'>


        <div class='container'>
            <div class='row' id='content-wrapper'>
                <div class='col-xs-12'>
                    <?php

                    if (!Helper::GET("tipo") || !Helper::GET("page")) {

                        if (Seguranca_Pagamento::getIdDoClienteLogado()) {

                            $paginaInicial = "paginas/assinaturas_do_cliente.php";

                        } else {
                            Helper::mudarLocation("login.php");
                            return;

                        }


                    } else {

                        if (!Seguranca_Pagamento::verificaPermissao(Helper::GET("tipo"), Helper::GET("page"))) {
                            Helper::mudarLocation("login.php");
                            return;
                        } else{
                            Helper::includePHP(1, "client_area/" . Helper::GET("tipo") . "/" . Helper::GET("page") . ".php");
                        }
                    }

                    ?>
                </div>
            </div>
            <? include("imports/rodape.php"); ?>
        </div>
    </section>
</div>

<?= Javascript::importarBibliotecasFlatty(); ?>

</body>
</html>
