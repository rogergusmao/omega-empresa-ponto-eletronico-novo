"use strict";
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
exports.__esModule = true;
var geral_1 = require("lib/geral");
var Sheet = (function () {
    function Sheet(nomeSheet) {
        this.nomeSheet = nomeSheet;
        this.celulaInicial = new geral_1.Cell(1, 1);
        this.celulaCorrente = this.celulaInicial.clone();
        this.conteudoSheet = {};
    }
    Sheet.prototype.getMensagemTopo = function (mensagemTopo, numeroColunasMerge) {
        var celulaCorrente = this.celulaInicial.clone();
        var celulaFinalMerge = celulaCorrente.clone().moveCol(numeroColunasMerge - 1);
        var conteudoMensagemTopo = {
            "!merges": [{ s: celulaCorrente.getObject(), e: celulaFinalMerge.getObject() }]
        };
        conteudoMensagemTopo[celulaCorrente.getString()] = { t: 's', v: mensagemTopo, s: this.getCabecalhoTabelaStyle() };
        return conteudoMensagemTopo;
    };
    Sheet.prototype.getCabecalhoTabelaStyle = function () {
        return {
            alignment: { vertical: 'center', horizontal: 'center' },
            font: { sz: 14, bold: true, color: '#FF00FF' },
            fill: { patternType: 'solid', fgColor: { rgb: 'FFFFAA00' }, bgcolor: { rgb: 'FFFFAAFF' } }
        };
    };
    Sheet.prototype.appendCabecalho = function (arrCabecalhoTabela, mensagemTopo) {
        if (mensagemTopo === void 0) { mensagemTopo = null; }
        var cabecalho = {};
        if (mensagemTopo != null) {
            var numeroColunas = arrCabecalhoTabela.length;
            var conteudoMensagemTopo = this.getMensagemTopo(mensagemTopo, numeroColunas);
            this.celulaCorrente.moveRow(2);
            cabecalho = __assign({}, cabecalho, conteudoMensagemTopo);
        }
        var cabecalhoTabela = {};
        for (var _i = 0, arrCabecalhoTabela_1 = arrCabecalhoTabela; _i < arrCabecalhoTabela_1.length; _i++) {
            var colunaCabecalho = arrCabecalhoTabela_1[_i];
            cabecalhoTabela[this.celulaCorrente.getString()] = { t: 's', v: colunaCabecalho, s: this.getCabecalhoTabelaStyle() };
            this.celulaCorrente.moveCol(1);
        }
        this.conteudoSheet = __assign({}, this.conteudoSheet, cabecalho, cabecalhoTabela);
    };
    Sheet.prototype.appendDados = function (arrDados) {
    };
    Sheet.prototype.getSheetName = function () {
        return this.nomeSheet;
    };
    Sheet.prototype.getConteudo = function () {
        this.conteudoSheet["!ref"] = this.celulaInicial.getString() + ":" + this.celulaCorrente.getString();
        return this.conteudoSheet;
    };
    return Sheet;
}());
exports.Sheet = Sheet;
