<?php

function unlockFiles()
{

    if (is_array(EXTDAO_Sistema_sequencia::$lockHandler))
    {

        foreach (EXTDAO_Sistema_sequencia::$lockHandler as $nomeTabela => $fileHandler)
        {

            if (is_resource(EXTDAO_Sistema_sequencia::$lockHandler[$nomeTabela]))
            {

                flock(EXTDAO_Sistema_sequencia::$lockHandler[$nomeTabela], LOCK_UN);
                fclose(EXTDAO_Sistema_sequencia::$lockHandler[$nomeTabela]);
            }
        }
    }
}

class BkpEXTDAO_Sistema_sequencia
{
    private static $instance = null;
    private static $objBanco = null;
    public static $lockHandler = null;
    public static $tempoTotalSemaforo = 0;
    private static $IS_UTILIZANDO_LOCK = true;
    private static $IS_UTILIZANDO_APC = true;
    public static $VALOR_INVALIDO = -1;

    private function __construct()
    {

        if (self::$IS_UTILIZANDO_APC
            && (!function_exists("apc_add")
                || strtoupper(substr(PHP_OS, 0, 3)) === 'WIN'))
        {
            self::$IS_UTILIZANDO_APC = false;
        }

        if (!self::$IS_UTILIZANDO_APC)
        {

            //register_shutdown_function('unlockFiles');

        }

        if (self::$objBanco == null)
        {

            //inicia objeto criando nova conex�o (link) com o banco de dados
            //ir� persistir durante toodo o tempo da request
            self::$objBanco = new Database(
                false,
                false,
                false,
                false,
                false,
                true);
        }
    }

    public static function getInstance()
    {

        if (is_null(self::$instance))
        {

            self::$instance = new self();
        }

        return self::$instance;
    }

    private static function __getSemaphorePath($nomeTabela)
    {

        $path = ConfiguracaoSite::getPathConteudo() . self::getLockName($nomeTabela);

        return $path;
    }

    private static function semaphoreGet($nomeTabela)
    {

        $inicio = microtime();
        $maxIterations = 10;

        if (!self::$IS_UTILIZANDO_LOCK)
        {
            return true;
        }

        if (!self::$IS_UTILIZANDO_APC)
        {

            if (!is_resource(self::$lockHandler[$nomeTabela]))
            {
                self::$lockHandler[$nomeTabela] = fopen(
                    self::__getSemaphorePath($nomeTabela),
                    'w');
            }
        }
        else
        {

            $maxIterations = 50;
        }

        for ($i = 1; $i <= $maxIterations; $i++)
        {

            if (self::$IS_UTILIZANDO_APC)
            {

                $isLocked = apc_exists(self::getLockName($nomeTabela));
                $successLock = !$isLocked;

                if (!$isLocked)
                {

                    $adicionadoComSucesso = apc_add(
                        self::getLockName($nomeTabela),
                        "L",
                        10);

                    //se n�o foi adicionado com sucesso, � pq outra requisi��o j� adicionou antes
                    if (!$adicionadoComSucesso)
                    {

                        $successLock = false;
                    }
                }
            }
            else
            {

                $successLock = flock(self::$lockHandler[$nomeTabela], LOCK_EX);
            }

            if ($successLock)
            {

                $termino = microtime();
                self::$tempoTotalSemaforo += (($termino - $inicio) * 1000);

                return true;
            }

            usleep($i * 10000);
        }

        $termino = microtime();
        self::$tempoTotalSemaforo += (($termino - $inicio) * 1000);

        return false;
    }

    private static function getLockName($nomeTabela)
    {
        return "lc_" . str_replace('.', '_', self::$objBanco->getHost()) . "_" . self::$objBanco->getDBName() . "_$nomeTabela";
    }

    private static function semaphoreRelease($nomeTabela)
    {

        if (self::$IS_UTILIZANDO_APC)
        {

            apc_delete(self::getLockName($nomeTabela));
        }
        else
        {

            if (is_resource(self::$lockHandler[$nomeTabela]))
            {
                flock(self::$lockHandler[$nomeTabela], LOCK_UN);
            }
        }
    }

    public static function gerarId($nomeTabela, $totGerar = 1)
    {

        return self::getInstance()->gerarIdInterna($nomeTabela, $totGerar);
    }

    public function gerarIdInterna($nomeTabela, $totGerar = 1)
    {

        $nomeTabela = strtolower($nomeTabela);

        if (self::semaphoreGet($nomeTabela))
        {

            self::$objBanco->iniciarTransacao();
            $msg = self::$objBanco->queryMensagem("UPDATE sistema_sequencia ss1 SET ss1.id_ultimo_INT=ss1.id_ultimo_INT+$totGerar WHERE ss1.tabela='{$nomeTabela}'");

            if ($msg != null && ($msg->mCodRetorno == PROTOCOLO_SISTEMA::ERRO_EXECUCAO_CONSULTA_SQL || $msg->mValor === false))
            {
                self::$objBanco->rollbackTransacao();
                throw new SequenciaException("Falha ao realizar update da sequ�ncia.", SequenciaException::THROW_FALHA_UPDATE_SEQUENCIA);
            }
            else
            {

                try
                {

                    $msg = self::$objBanco->queryMensagem("SELECT id_ultimo_INT FROM sistema_sequencia WHERE tabela='{$nomeTabela}'");

                    if ($msg != null && ($msg->mCodRetorno == PROTOCOLO_SISTEMA::ERRO_EXECUCAO_CONSULTA_SQL || $msg->mValor === false))
                    {
                        self::$objBanco->rollbackTransacao();
                        throw new SequenciaException("Falha ao realizar select na sequ�ncia.", SequenciaException::THROW_FALHA_SELECT_SEQUENCIA);
                    }
                    elseif (self::$objBanco->rows() > 0)
                    {

                        $ultimoId = self::$objBanco->getPrimeiraTuplaDoResultSet(0);
                        self::$objBanco->commitTransacao();
                    }
                    else
                    {

                        try
                        {

                            //ultimo id na tabela pesquisada
                            $ultimoId = 1;
                            $msg = self::$objBanco->queryMensagem("SELECT MAX(id) FROM {$nomeTabela}");

                            if ($msg != null && ($msg->mCodRetorno == PROTOCOLO_SISTEMA::ERRO_EXECUCAO_CONSULTA_SQL || $msg->mValor === false))
                            {

                                throw new SequenciaException("Falha ao obter MAX id da tabela {$nomeTabela}.", SequenciaException::THROW_FALHA_SELECT_TABELA);
                            }

                            $ultimoIdNaTabela = self::$objBanco->getPrimeiraTuplaDoResultSet(0);

                            if ($ultimoIdNaTabela)
                            {
                                $ultimoId = $ultimoIdNaTabela + $totGerar;
                            }

                            $msg = self::$objBanco->queryMensagem("INSERT INTO sistema_sequencia(tabela, id_ultimo_INT) VALUES('{$nomeTabela}', {$ultimoId})");

                            self::$objBanco->commitTransacao();

                            if ($msg != null && ($msg->mCodRetorno == PROTOCOLO_SISTEMA::ERRO_EXECUCAO_CONSULTA_SQL || $msg->mValor === false))
                            {

                                throw new SequenciaException("Falha ao realizar insert na sequ�ncia.", SequenciaException::THROW_FALHA_INSERT_SEQUENCIA);
                            }

                            //libera o lock
                            if (self::$IS_UTILIZANDO_LOCK)
                            {
                                self::semaphoreRelease($nomeTabela);
                            }
                        }
                        catch (Exception $ex)
                        {
                            self::$objBanco->commitTransacao();

                            if (self::$IS_UTILIZANDO_LOCK)
                            {
                                self::semaphoreRelease($nomeTabela);
                            }

                            throw $ex;
                        }
                    }

//                    self::$objBanco->commitTransacao();

                    //libera o lock
                    if (self::$IS_UTILIZANDO_LOCK)
                    {
                        self::semaphoreRelease($nomeTabela);
                    }
                }
                catch (Exception $ex)
                {

                    if (self::$IS_UTILIZANDO_LOCK)
                    {
                        self::semaphoreRelease($nomeTabela);
                    }

                    throw $ex;
                }
            }
        }
        else
        {

            throw new SequenciaException("Falha ao obter lock da sequ�ncia.", SequenciaException::THROW_FALHA_OBTER_LOCK);
        }

        return $ultimoId;
    }

    public static function close()
    {
        if (self::$objBanco != null)
        {

            self::$objBanco->close();
            self::$objBanco = null;
//            unset(self::$objBanco );

        }

        if (self::$instance != null)
        {
//            unset(self::$instance );
            self::$instance = null;
        }
    }

    function __destruct()
    {

        EXTDAO_Sistema_sequencia::close();
    }
}

