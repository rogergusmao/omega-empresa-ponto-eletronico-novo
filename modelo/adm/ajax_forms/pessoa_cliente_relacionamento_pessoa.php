<?php
/*
 *
 * -------------------------------------------------------
 * NOME DO FORMUL�RIO: pessoa_empresa
 * DATA DE GERA��O:    02.07.2012
 * ARQUIVO:            pessoa_empresa.php
 * TABELA MYSQL:       pessoa_empresa
 * BANCO DE DADOS:     ponto_eletronico
 * -------------------------------------------------------
 *
 */

if (isset($_GET["contador"]))
{

    $numeroRegistroInterno = Helper::GET("contador");
}

if (isset($identificadorRelacionamento) && is_numeric($identificadorRelacionamento))
{

    $objPessoa_empresa = new EXTDAO_Pessoa_empresa();
    $objPessoa_empresa->select($identificadorRelacionamento);
}
else
{

    $objPessoa_empresa = new EXTDAO_Pessoa_empresa();
}

$objArgPessoa_empresa = new Generic_Argument();
$objPessoa_empresa->formatarParaExibicao();
?>

<input type="hidden" name="pessoa_cliente_id_<?= $numeroRegistroInterno ?>"
       id="pessoa_cliente_id_<?= $numeroRegistroInterno ?>" value="<?= $objPessoa_empresa->getId(); ?>">

<table class="tabela_form">

    <tr class="tr_form">

        <?

        $objArgPessoa_empresa = new Generic_Argument();
        $objArgPessoa_empresa->numeroDoRegistro = "";
        $objArgPessoa_empresa->label = "Cliente";
        $objArgPessoa_empresa->valor = $objPessoa_empresa->getEmpresa_id_INT();
        $objArgPessoa_empresa->classeCss = "input_text";
        $objArgPessoa_empresa->classeCssFocus = "focus_text";
        $objArgPessoa_empresa->obrigatorio = true;
        $objArgPessoa_empresa->largura = 400;
        $objArgPessoa_empresa->nome = "pessoa_cliente_empresa_id_INT_{$numeroRegistroInterno}";
        $objArgPessoa_empresa->id = "pessoa_cliente_empresa_id_INT_{$numeroRegistroInterno}";
        $objArgPessoa_empresa->funcaoGetOptions = "getOptionsClientesPJComRede";

        $objPessoa_empresa->addInfoCampos("empresa_id_INT", $objArgPessoa_empresa->label, "TEXTO", $objArgPessoa_empresa->obrigatorio);
        ?>

        <td class="td_form_label"><?= $objArgPessoa_empresa->getLabel() ?></td>
        <td class="td_form_campo">
            <?= $objPessoa_empresa->objEmpresa->getComboBox($objArgPessoa_empresa); ?>
        </td>

        <?
        $objArgPessoa_empresa = new Generic_Argument();

        $objArgPessoa_empresa->numeroDoRegistro = "";
        $objArgPessoa_empresa->label = "Profiss�o/Papel";
        $objArgPessoa_empresa->valor = $objPessoa_empresa->getProfissao_id_INT();
        $objArgPessoa_empresa->classeCss = "input_text";
        $objArgPessoa_empresa->classeCssFocus = "focus_text";
        $objArgPessoa_empresa->obrigatorio = true;
        $objArgPessoa_empresa->largura = 200;
        $objArgPessoa_empresa->nome = "pessoa_cliente_profissao_id_INT_{$numeroRegistroInterno}";
        $objArgPessoa_empresa->id = "pessoa_cliente_profissao_id_INT_{$numeroRegistroInterno}";

        $objPessoa_empresa->addInfoCampos("profissao_id_INT", $objArgPessoa_empresa->label, "TEXTO", $objArgPessoa_empresa->obrigatorio);
        ?>

        <td class="td_form_label"><?= $objArgPessoa_empresa->getLabel() ?></td>
        <td class="td_form_campo">
            <?= $objPessoa_empresa->objProfissao->getComboBox($objArgPessoa_empresa); ?>
        </td>


    </tr>
    <tr>
        <td colspan="4" class="td_botao_remover_da_lista"><input class="botoes_form" type="button"
                                                                 value="Remover Cliente"
                                                                 onclick="javascript:removerDivAjaxEmLista(this);"></td>
    </tr>
</table><br/>

