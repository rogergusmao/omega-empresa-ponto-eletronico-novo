<?php
    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  Database_ponto_eletronico
    * DATA DE GERAÇÃO: 14.02.2018
    * ARQUIVO:         Database_ponto_eletronico.php
    * BANCO DE DADOS:  
    * -------------------------------------------------------
    *
    */
    
    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************
    class Database_ponto_eletronico {
	

    // *************************
    // DECLARAÇÃO DE TODAS AS TABELAS DO BANCO DE DADOS
    // *************************
    public static $tabelas = array ( 
);
        public static $chavesUnicas = null;

        public static function getChaveUnica($tabela){
            if(Database_ponto_eletronico::$chavesUnicas == null){
                Database_ponto_eletronico::$chavesUnicas = array();
                
            }
            
            return Database_ponto_eletronico::$chavesUnicas[$tabela];
        }
        
        
    // *************************
    // CONSTRUTOR
    // *************************
    
    public function __construct(){
            

    }

    
    // *************************
    // FACTORY
    // *************************
    public function factory(){
        return new Database_ponto_eletronico();

    }
    
        
	public function getVetorTable() {
		// TODO Auto-generated method stub
		return Database_ponto_eletronico::$tabelas;
	}

    } // classe: fim
    

    ?>