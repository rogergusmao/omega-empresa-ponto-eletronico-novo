"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var React = require("react");
var react_bootstrap_1 = require("react-bootstrap");
var uiComponente_1 = require("lib/uiComponente");
var geral_1 = require("aplicacao/comum/geral");
var DetalhesPontoEletronico = (function (_super) {
    __extends(DetalhesPontoEletronico, _super);
    function DetalhesPontoEletronico() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    DetalhesPontoEletronico.prototype.setDefaultBody = function () {
        var modelPontoEletronico = this.props.modelPontoEletronico;
        this.bodyContent = (<div className="corpo-modal-detalhes">   
                <p><b>Pessoa:</b>{modelPontoEletronico.nomePessoa}</p>                  
                <p><b>Tipo:</b> {geral_1.Helper.getTipoPontoAsString(modelPontoEletronico.tipoPonto)}</p>
                <p><b>Data do Ponto:</b> {DateUtil.getFormattedDateTime(modelPontoEletronico.dataPonto)}</p>    
            </div>);
    };
    DetalhesPontoEletronico.prototype.setDefaultHeader = function () {
        this.headerContent = (<react_bootstrap_1.Modal.Title id="contained-modal-title-lg">
                Ponto Eletrônico
            </react_bootstrap_1.Modal.Title>);
    };
    return DetalhesPontoEletronico;
}(uiComponente_1.PainelFlutuante));
exports.DetalhesPontoEletronico = DetalhesPontoEletronico;
