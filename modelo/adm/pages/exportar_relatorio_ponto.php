<?php

include '../../recursos/languages/pt-br.php';
include '../../recursos/php/funcoes.php';
include '../../recursos/php/constants.php';
include '../../recursos/php/database_config.php';

$nomeScript = Helper::getNomeDoScriptAtual();

$vIsPrimeiraVez = Helper::GET("is_primeira_vez_BOOLEAN");

$registrosPorPagina = REGISTROS_POR_PAGINA;

$registrosPesquisa = 1;

$obj = new EXTDAO_Ponto();
$obj->setByGet($registrosPesquisa);
$obj->formatarParaSQL();

$strCondicao = array();
$strGET = array();

if (!Helper::isNull($obj->getPessoa_id_INT()))
{

    $strCondicao[] = "p.pessoa_id_INT={$obj->getPessoa_id_INT()}";
    $strGET[] = "pessoa_id_INT1={$obj->getPessoa_id_INT()}";
}

if (!Helper::isNull($obj->getHora_TIME()))
{

    $strCondicao[] = "p.hora_TIME LIKE '%{$obj->getHora_TIME()}%'";
    $strGET[] = "hora_TIME1={$obj->getHora_TIME()}";
}

if (!Helper::isNull($obj->getDia_DATE()))
{

    $strCondicao[] = "p.dia_DATE={$obj->getDia_DATE()}";
    $strGET[] = "dia_DATE1={$obj->getDia_DATE()}";
}

if (!Helper::isNull($obj->getEmpresa_id_INT()))
{

    $strCondicao[] = "p.empresa_id_INT={$obj->getEmpresa_id_INT()}";
    $strGET[] = "empresa_id_INT1={$obj->getEmpresa_id_INT()}";
}

if (!Helper::isNull($obj->getProfissao_id_INT()))
{

    $strCondicao[] = "p.profissao_id_INT={$obj->getProfissao_id_INT()}";
    $strGET[] = "profissao_id_INT1={$obj->getProfissao_id_INT()}";
}

if (!Helper::isNull($obj->getUsuario_id_INT()))
{

    $strCondicao[] = "p.usuario_id_INT={$obj->getUsuario_id_INT()}";
    $strGET[] = "usuario_id_INT1={$obj->getUsuario_id_INT()}";
}

if (!Helper::isNull($obj->getIs_entrada_BOOLEAN()))
{

    $strCondicao[] = "p.is_entrada_BOOLEAN={$obj->getIs_entrada_BOOLEAN()}";
    $strGET[] = "is_entrada_BOOLEAN1={$obj->getIs_entrada_BOOLEAN()}";
}

$consulta = "";

for ($i = 0; $i < count($strCondicao); $i++)
{
    if (strlen($consulta))
    {
        $consulta .= " AND " . $strCondicao[$i];
    }
    else
    {
        $consulta .= $strCondicao[$i];
    }
}

for ($i = 0; $i < count($strGET); $i++)
{

    $varGET .= "&" . $strGET[$i];
}

if (strlen($consulta))
{
    $consulta .= " AND p.corporacao_id_INT = " . Seguranca::getIdDaCorporacaoLogada();
}
else
{
    $consulta .= " p.corporacao_id_INT = " . Seguranca::getIdDaCorporacaoLogada();
}

if ($nomeScript == "index.php" && !isset($vIsPrimeiraVez))
{
    echo "<center>";
    Helper::imprimirMensagem("A janela de download abrirá em alguns segundos.\nCaso não abra automaticamente, <a class='link_padrao' href='pages/exportar_relatorio_mensagem_sms_pessoa.php?is_primeira_vez_BOOLEAN=0&$varGET'>clique aqui</a>.");
    echo "</center>";

    Helper::mudarLocation("pages/exportar_relatorio_mensagem_sms_pessoa.php?is_primeira_vez_BOOLEAN=0&$varGET");
    exit();
}

$strWhere = "";
if (strlen($consulta) > 0)
{
    $strWhere .= "WHERE $consulta ";
}

$objBanco = new Database();

$numeroRegistros = $objBanco->getPrimeiraTuplaDoResultSet(0);

$limites = Helper::getLimitesRegsPaginacao($registrosPorPagina, $numeroRegistros);
$consultaWhere = "";
if (strlen($consulta) > 0)
{
    $consultaWhere .= "WHERE $consulta AND p.corporacao_id_INT = " . Seguranca::getIdDaCorporacaoLogada();
}
else
{
    $consultaWhere .= "WHERE p.corporacao_id_INT = " . Seguranca::getIdDaCorporacaoLogada();
}

$consultaRegistros = "SELECT p.id, p.is_entrada_BOOLEAN, u.nome, pe.nome, e.nome, pr.nome
            FROM ponto p JOIN pessoa pe ON p.pessoa_id_INT = pe.id 
            JOIN usuario u ON u.id = p.usuario_id_INT 
            LEFT JOIN empresa e ON p.empresa_id_INT = e.id 
            LEFT JOIN pessoa_empresa pee ON e.id = pee.empresa_id_INT 
            LEFT JOIN profissao pr ON pee.profissao_id_INT = pr.id 
            {$consultaWhere} 
            ORDER BY CONCAT(p.dia_DATE, p.hora_TIME) DESC, pe.nome ASC LIMIT {$limites[0]},{$limites[1]}";

$objBanco->query($consultaRegistros);

$stringRetorno = "";
$vetorCabecalho = array("Id no Sistema",
    "Entrada ou Saída?",
    "Data",
    "Hora",
    "Usuário do Telefone Utilizado",
    "Nome da Pessoa",
    "Nome da Empresa",
    "Profissão da Pessoa na Empresa");
$stringRetorno .= Helper::getStrLinhaCSVDoVetorDeDados($vetorCabecalho);
//SELECT p.id, p.is_entrada_BOOLEAN, u.nome, pe.nome, e.nome, pr.nome 
for ($i = 1; $regs = $objBanco->fetchArray(); $i++)
{
    $vId = $regs[0];
    $obj->select($vId);
    $obj->formatarParaExibicao();
    $vIsEntrada = $regs[1];
    $vStrEntrada = "";
    if ($vIsEntrada == "1")
    {
        $vStrEntrada = "Entrada";
    }
    else
    {
        if ($vIsEntrada == "0")
        {
            $vStrEntrada = "Saída";
        }
    }
    $vStrData = $obj->getDia_DATE();
    $vStrHora = $obj->getHora_TIME();
    $vUsuarioNome = $regs[2];
    $vPessoaNome = $regs[3];
    $vEmpresaNome = $regs[4];

    $vProfissaoNome = $regs[5];
    if (!strlen($vProfissaoNome))
    {
        $vProfissaoNome = "Não há profissão cadastrada da pessoa nessa empresa";
    }
    $vetorTupla = array($vId, $vStrEntrada, $vStrData, $vStrHora, $vUsuarioNome, $vPessoaNome, $vEmpresaNome, $vProfissaoNome);
    $stringRetorno .= Helper::getStrLinhaCSVDoVetorDeDados($vetorTupla);
}

$objDownload = new Download("relatorio_ponto_marcado.csv");
print $objDownload->ds_download($stringRetorno);

?>
