"use strict";
exports.__esModule = true;
var Label = (function () {
    function Label(text) {
        this.text = text;
        this.setDefaultValues();
    }
    Label.prototype.setDefaultValues = function () {
        this.fontWeight = 'bold';
        this.fontSize = '12px';
        this.fontFamily = '"Helvetica Neue", Helvetica, "Noto Sans", sans-serif, Arial, sans-serif',
            this.color = '#303641';
    };
    return Label;
}());
exports.Label = Label;
