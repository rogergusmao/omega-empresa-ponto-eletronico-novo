<?php

/*
*
* -------------------------------------------------------
* NOME DO FORMUL�RIO: bairro
* DATA DE GERA��O:    25.06.2012
* ARQUIVO:            bairro.php
* TABELA MYSQL:       bairro
* BANCO DE DADOS:     ponto_eletronico
* -------------------------------------------------------
*
*/

if (isset($_GET["contador"]))
{

    $numeroRegistroInterno = Helper::GET("contador");
}

if (isset($identificadorRelacionamento) && is_numeric($identificadorRelacionamento))
{

    $objBairro = new EXTDAO_Bairro();
    $objBairro->select($identificadorRelacionamento);
}
else
{

    $objBairro = new EXTDAO_Bairro();
}

$objArgBairro = new Generic_Argument();
$objBairro->formatarParaExibicao();

?>

<input type="hidden" name="bairro_id_<?= $numeroRegistroInterno ?>" id="bairro_id_<?= $numeroRegistroInterno ?>"
       value="<?= $objBairro->getId(); ?>">

<table class="tabela_form">

    <tr class="tr_form">


        <?

        $objArgBairro = new Generic_Argument();

        $objArgBairro->numeroDoRegistro = "";
        $objArgBairro->label = $objBairro->label_nome;
        $objArgBairro->valor = $objBairro->getNome();
        $objArgBairro->classeCss = "input_text";
        $objArgBairro->classeCssFocus = "focus_text";
        $objArgBairro->obrigatorio = true;
        $objArgBairro->largura = 200;
        $objArgBairro->nome = "bairro_nome_{$numeroRegistroInterno}";
        $objArgBairro->id = "bairro_nome_{$numeroRegistroInterno}";

        ?>

        <td class="td_form_label"><?= $objArgBairro->getLabel() ?></td>
        <td class="td_form_campo"><?= $objBairro->campoTexto($objArgBairro); ?></td>


        <?

        $objArgBairro = new Generic_Argument();

        $objArgBairro->numeroDoRegistro = "";
        $objArgBairro->label = $objBairro->label_nome_normalizado;
        $objArgBairro->valor = $objBairro->getNome_normalizado();
        $objArgBairro->classeCss = "input_text";
        $objArgBairro->classeCssFocus = "focus_text";
        $objArgBairro->obrigatorio = false;
        $objArgBairro->largura = 200;
        $objArgBairro->nome = "bairro_nome_normalizado_{$numeroRegistroInterno}";
        $objArgBairro->id = "bairro_nome_normalizado_{$numeroRegistroInterno}";

        ?>

        <td class="td_form_label"><?= $objArgBairro->getLabel() ?></td>
        <td class="td_form_campo"><?= $objBairro->campoTexto($objArgBairro); ?></td>
    </tr>
    <tr class="tr_form">


        <?

        $objArgBairro = new Generic_Argument();

        $objArgBairro->numeroDoRegistro = "";
        $objArgBairro->label = $objBairro->label_corporacao_id_INT;
        $objArgBairro->valor = $objBairro->getCorporacao_id_INT();
        $objArgBairro->classeCss = "input_text";
        $objArgBairro->classeCssFocus = "focus_text";
        $objArgBairro->obrigatorio = false;
        $objArgBairro->largura = 200;
        $objArgBairro->nome = "bairro_corporacao_id_INT_{$numeroRegistroInterno}";
        $objArgBairro->id = "bairro_corporacao_id_INT_{$numeroRegistroInterno}";

        $objBairro->addInfoCampos("corporacao_id_INT", $objArgBairro->label, "TEXTO", $objArgBairro->obrigatorio);

        ?>

        <td class="td_form_label"><?= $objArgBairro->getLabel() ?></td>
        <td class="td_form_campo">
            <?= $objBairro->objCorporacao->getComboBox($objArgBairro); ?>
        </td>


        <td class="td_form_label"></td>
        <td class="td_form_campo"></td>
    </tr>
    <tr>
        <td colspan="4" class="td_botao_remover_da_lista"><input class="botoes_form" type="button"
                                                                 value="Remover Bairro"
                                                                 onclick="javascript:removerDivAjaxEmLista(this);"></td>
    </tr>
</table><br/>

