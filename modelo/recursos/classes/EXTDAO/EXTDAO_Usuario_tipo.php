<?php

/*

Arquivo gerado atrav?s de gerador de c?digo em 24/05/2017 as 07:53:29.
Para que o arquivo n?o seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

Tabela correspondente: usuario_tipo
Sobrescrita de dados: ---SOBRESCRITA_NAO_AUTORIZADA---

*/
?>

<?php

class EXTDAO_Usuario_tipo extends DAO_Usuario_tipo
{
    const NOME_USUARIO_TIPO_PADRAO_ADM = "USUARIO ADMINISTRADOR";

    public function __construct($configDAO = null)
    {
        parent::__construct($configDAO);
        $this->nomeClasse = "EXTDAO_Usuario_tipo";
    }

    public function factory()
    {
        return new EXTDAO_Usuario_tipo();
    }

    public function __getFormComboBoxesData()
    {
        try
        {
            $returnObject = new stdClass();
            $returnObject->comboBoxesData = $this->getFormComboBoxesData();
            $returnObject->listaDeItensDoMenuSistemaWeb = $this->getListOfItemMenuSistemaWeb()->mObj;

            static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
            return new Mensagem_generica($returnObject);
        }
        catch (Exception $ex)
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
        }
    }

    public function __actionAdd($parameters = null)
    {
        if (is_null($parameters))
        {
            $parameters = Helper::getPhpInputObject();
        }

        try
        {
            $this->setByObject($parameters);
            $this->setDataCadastroAndOffsetInSecondsFromUser($parameters);

            $this->formatarParaSQL();
            $msg = $this->insert(true);

            if ($msg != null && $msg->erro())
            {
                return $msg;
            }

            $this->select($this->getId());

            $msgOperacao = $this->gravarPermissoesDoMenu($this->id, $parameters->itensMenuSelecionados);
            if ($msgOperacao != null && !$msgOperacao->ok())
            {
                return $msgOperacao;
            }

            return new Mensagem(null, I18N::getExpression("Tipo de usuário adicionado com sucesso."));

        }
        catch (Exception $ex)
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
        }

    }

    public function __actionRemove($parameters = null)
    {
        $successulRemovalCount = 0;
        if (is_null($parameters))
        {
            $parameters = Helper::getPhpInputObject();
        }

        try
        {
            $msgIdTipoAdministrador = $this->getIdDoTipoDeUsuarioAdministrador();
            if ($msgIdTipoAdministrador != null && $msgIdTipoAdministrador->erro())
            {
                return $msgIdTipoAdministrador;
            }

            foreach ($parameters as $record)
            {
                if (is_numeric($record->id))
                {
                    //não remove o registro caso seja o administrador
                    if($msgIdTipoAdministrador->mObj == $record->id)
                    {
                        continue;
                    }

                    $msg = $this->delete($record->id, true);
                    if ($msg == null || $msg->ok())
                    {
                        $successulRemovalCount++;
                    }
                }
            }

            if ($successulRemovalCount > 0)
            {
                $successMessage = count($successulRemovalCount) > 1 ? I18N::getExpression("{0} Tipos de usuário foram removidos com sucesso.", $successulRemovalCount) : I18N::getExpression("Tipo de usuário removido com sucesso.");
                return new Mensagem(null, $successMessage);
            }
            else
            {
                $errorMessage = count($parameters) > 1 ? I18N::getExpression("Falha ao remover tipos de usuário.") : I18N::getExpression("Falha ao remover tipo de usuário.");
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, $errorMessage);
            }
        }
        catch (Exception $ex)
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
        }
    }

    public function getIdDoTipoDeUsuarioAdministrador()
    {
        try
        {
            $msgProtocolo = BO_SICOB::getCaracteristicasDoPacoteDaCorporacao();
            if ($msgProtocolo != null && !$msgProtocolo->ok())
            {
                $mensagemErroRecuperarCaracteristicasPacote = I18N::getExpression("Falha ao recuperar dados do seu pacote. Por favor tente novamente mais tarde.");
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, $mensagemErroRecuperarCaracteristicasPacote);
            }
            else
            {
                $protocolo = $msgProtocolo->mObj;
                $emailAdministradorDaConta = $protocolo->emailUsuarioPrincipal;
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();

                $msg = $this->database->queryMensagem("SELECT utc.usuario_tipo_id_INT AS idTipoUsuarioAdministrador
                                                                FROM usuario u 
                                                                JOIN usuario_tipo_corporacao utc
                                                                  ON utc.usuario_id_INT=u.id
                                                                  AND utc.corporacao_id_INT={$idCorporacao}
                                                                WHERE u.email='{$emailAdministradorDaConta}'");
                if ($msg != null && $msg->erro())
                {
                    return $msg;
                }
                else if($this->database->rows() > 0)
                {
                    $objRetornoDB = Helper::getPrimeiroRegistroDoResultSetAsObject($this->database->result);
                    return new Mensagem_generica($objRetornoDB->idTipoUsuarioAdministrador, PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO);
                }
                else
                {
                    $mensagemRegistroNaoEncontrado = I18N::getExpression("Falha ao recuperar dados. Por favor tente novamente mais tarde.");
                    return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, $mensagemRegistroNaoEncontrado);
                }

            }

        }
        catch (Exception $ex)
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
        }

    }

    public function __actionEdit($parameters = null)
    {
        if (is_null($parameters))
        {
            $parameters = Helper::getPhpInputObject();
        }

        try
        {
            $msg = $this->getIdDoTipoDeUsuarioAdministrador();
            if (!Interface_mensagem::checkOk($msg))
            {
                return $msg;
            }
            else
            {
                if($msg->mObj == $parameters->id)
                {
                    return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, I18N::getExpression("Operação não permitida para o registro."));
                }
            }

            $this->setByObject($parameters);
            $this->setAllFieldsToHumanFormat();
            $this->formatarParaSQL();

            $msg = $this->updateByInstanceUsingParameters($this->id, $parameters, true);
            if ($msg != null && $msg->erro())
            {
                return $msg;
            }

            $msgOperacao = $this->gravarPermissoesDoMenu($this->id, $parameters->itensMenuSelecionados);
            if ($msgOperacao != null && !$msgOperacao->ok())
            {
                return $msgOperacao;
            }

            return new Mensagem(null, I18N::getExpression("Tipo de usuário editado com sucesso."));
        }
        catch (Exception $ex)
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
        }

    }

    public function __getList($parameters = null, $idCorporacao = null)
    {
        if (is_null($parameters))
        {
            $parameters = Helper::getPhpInputObject();
        }

        try
        {
            $msg = $this->getIdDoTipoDeUsuarioAdministrador();
            if ($msg != null && $msg->erro())
            {
                return $msg;
            }

            $idTipoUsuarioAdministrador = $msg->mObj;

            $filterParameters = new stdClass();
            if (isset($parameters->filterParameters))
            {
                $filterParameters = $parameters->filterParameters;
            }

            if (is_null($idCorporacao))
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            ListParameters::addFilterParameterToObject($filterParameters, "corporacaoId", $idCorporacao);

            $sortingParameters = null;
            if (isset($parameters->sortingParameters))
            {
                $sortingParameters = $parameters->sortingParameters;
            }

            $paginationParameters = null;
            if (isset($parameters->paginationParameters))
            {
                $paginationParameters = $parameters->paginationParameters;
            }

            $listMappingType = static::getListMappingType($parameters->listMappingType);
            $mainTableAlias = "ut";

            $whereClause = static::getWhereClauseForFilter($filterParameters, $mainTableAlias);
            $orderByClause = static::getOrderByClauseForFilter($sortingParameters, $mainTableAlias);
            $limitClause = static::getLimitClauseForFilter($paginationParameters);

            $queryWithoutLimit = "SELECT ut.id FROM usuario_tipo ut {$whereClause}";
            $query = "SELECT ut.id AS usuario_tipo__id, 
                             ut.nome AS usuario_tipo__nome, 
                             ut.nome_visivel AS usuario_tipo__nome_visivel, 
                             ut.status_BOOLEAN AS usuario_tipo__status_BOOLEAN 
                      FROM usuario_tipo ut 
                      {$whereClause} {$orderByClause} {$limitClause}";

            $msg = $this->database->queryMensagem($queryWithoutLimit);
            if ($msg != null && $msg->erro())
            {
                return $msg;
            }

            $totalNumberOfRecords = $this->database->rows();
            if ($totalNumberOfRecords == 0)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Não encontrou nenhum registro"));
            }

            $msg = $this->database->queryMensagem($query);

            //operacao realizaca com sucesso
            if ($msg == null)
            {
                if ($this->database->rows() == 0)
                {
                    return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Não encontrou nenhum registro"));
                }

                $resultSet = $this->database->result;

                $objReturn = new stdClass();
                $objReturn->dataSet = static::mapResultSetDataToList($resultSet, $listMappingType, static::$listAliasTypes);

                foreach($objReturn->dataSet as $record)
                {
                    if($record->usuario_tipo__id == $idTipoUsuarioAdministrador)
                    {
                        $record->allowEdit = false;
                        $record->allowRemove = false;
                    }

                    $record->usuario_tipo__status_BOOLEAN_forHuman = Helper::formatarBooleanParaExibicao($record->usuario_tipo__status_BOOLEAN, "Ativo", "Inativo");
                }

                $lastPage = ceil($totalNumberOfRecords / $paginationParameters->recordsPerPage);
                $objReturn->paginationParameters = new PaginationParams($paginationParameters->recordsPerPage, $paginationParameters->currentPage, $lastPage);

                return new Mensagem_generica($objReturn);
            }
            else
            {
                return $msg;
            }
        }
        catch (Exception $ex)
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
        }

    }

    public function getListOfItemMenuSistemaWeb()
    {
        try
        {
            $arrRetorno = array();
            $objMenu = new Menu();

            $arrConteudoMenu = $objMenu->getArrayDoConteudoDoMenuCompleto();
            EXTDAO_Usuario::montarEstuturaDoMenuParaCadastroDoUsuario( $arrConteudoMenu,  $arrRetorno);

            return new Mensagem_generica($arrRetorno);
        }
        catch (Exception $ex)
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
        }
    }

    public function getListOfAreasDoMenuWebDoTipoDeUsuario($filterParameters = null)
    {
        try
        {
            if (isset($filterParameters->idTipoDeUsuario) && !is_null($filterParameters->idTipoDeUsuario))
            {
                $query = "SELECT area_menu AS usuarioMenuArea
                            FROM usuario_tipo_menu AS um
                            WHERE um.usuario_tipo_id_INT={$filterParameters->idTipoDeUsuario}";

                $fieldTypes = new stdClass();
                $fieldTypes->usuarioMenuArea = TEXT;
                $msg = $this->database->queryMensagem($query);

                if ($msg == null)
                {
                    $resultSet = $this->database->result;
                    $areasDoUsuarioComPermissao = $this->mapGenericResultSetDataToList($resultSet);

                    $arrChavesAreasDoUsuarioComPermissao = array();
                    for ($i = 0; $i < count($areasDoUsuarioComPermissao); $i++)
                    {
                        $arrChavesAreasDoUsuarioComPermissao[] = $areasDoUsuarioComPermissao[$i]->usuarioMenuArea;
                    }

                    $msgMenuCompleto = $this->getListOfItemMenuSistemaWeb();

                    $arrRetorno = array();
                    if ($msgMenuCompleto->ok())
                    {
                        for ($i = 0; $i < count($msgMenuCompleto->mObj); $i++)
                        {
                            $itemMenuAtual = $msgMenuCompleto->mObj[$i];
                            $chaveItem = $itemMenuAtual->chaveItem;

                            if (in_array($chaveItem, $arrChavesAreasDoUsuarioComPermissao))
                            {
                                $arrRetorno[] = clone $itemMenuAtual;
                            }
                        }

                        return new Mensagem_generica($arrRetorno);
                    }
                    else
                    {
                        return $msgMenuCompleto;
                    }
                }
                else
                {
                    return $msg;
                }
            }
            else
            {
                return new Mensagem_generica(null, PROTOCOLO_SISTEMA::RESULTADO_VAZIO);
            }
        }
        catch (Exception $ex)
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
        }
    }

    public function __getRecord($parameters = null)
    {
        if (is_null($parameters))
        {
            $parameters = Helper::getPhpInputObject();
        }

        try
        {
            $returnObject = new stdClass();
            $whereClause = static::getWhereClauseForFilter($parameters->filterParameters);
            $query = "SELECT  id, nome, nome_visivel, status_BOOLEAN, pagina_inicial, corporacao_id_INT FROM usuario_tipo {$whereClause}";

            $msg = $this->database->queryMensagem($query);
            if ($msg == null)
            {
                if ($this->database->rows() == 0)
                {
                    return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Não foi encontrado nenhum registro"));
                }
                $resultSet = $this->database->result;
                $resultObject = static::mapResultSetDataToRecord($resultSet);

                $returnObject->formData = $resultObject;
                $returnObject->comboBoxesData = null;
                if ($parameters->loadComboBoxesData)
                {
                    $this->setByObject($resultObject);
                    $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                    $returnObject->listaDeItensDoMenuSistemaWeb = $this->getListOfItemMenuSistemaWeb()->mObj;

                    $filterParametersDadosAdicionais = new stdClass();
                    $filterParametersDadosAdicionais->idTipoDeUsuario = static::getFilterValueFromFilterArray("id", $parameters->filterParameters);
                    $returnObject->listaDeItensDoMenuSistemaWebDoTipoDeUsuario = $this->getListOfAreasDoMenuWebDoTipoDeUsuario($filterParametersDadosAdicionais)->mObj;

                }

                return new Mensagem_generica($returnObject);
            }
            else
            {
                return $msg;
            }

        }
        catch (Exception $ex)
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
        }

    }


    public function gravarPermissoesDoMenu($pIdTipoUsuario, $arrayDePermissoes)
    {
        try
        {
            if (!strlen($pIdTipoUsuario))
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, I18N::getExpression("Tipo de usuário inválido."));
            }

            $idCorporacaoLogada = Seguranca::getIdDaCorporacaoLogada();

            $arrayChavesNovasPermissoes = array();
            foreach($arrayDePermissoes as $objPermissao)
            {
                $arrayChavesNovasPermissoes[] = $objPermissao->chaveItem;
            }

            //copia array
            $arrayChavesPermissoesInsert = array();
            $arrayIdsPermissoesDelete = array();

            $this->database->query("SELECT id AS id, area_menu AS areaMenu
                                            FROM usuario_tipo_menu
                                            WHERE usuario_tipo_id_INT={$pIdTipoUsuario} 
                                            AND corporacao_id_INT={$idCorporacaoLogada}");

            $arrayAreasAtuaisUsuario = Helper::getResultSetToArrayDeObjetos($this->database->result);
            $arrayAreasAtuaisPermanecer = array();

            foreach ($arrayAreasAtuaisUsuario as $objAreaAtualUsuario)
            {
                if(!in_array($objAreaAtualUsuario->areaMenu, $arrayChavesNovasPermissoes))
                {
                    $arrayIdsPermissoesDelete[] = $objAreaAtualUsuario->id;
                }
                else
                {
                    $arrayAreasAtuaisPermanecer[] = $objAreaAtualUsuario->areaMenu;
                }
            }

            foreach($arrayChavesNovasPermissoes as $areaNovaPermissao)
            {
                if(!in_array($areaNovaPermissao,$arrayAreasAtuaisPermanecer))
                {
                    $arrayChavesPermissoesInsert[] = $areaNovaPermissao;
                }
            }

            foreach($arrayChavesPermissoesInsert as $itemInsert)
            {
                $vObjUsuarioMenu = new EXTDAO_Usuario_tipo_menu();
                $vObjUsuarioMenu->setUsuario_tipo_id_INT($pIdTipoUsuario);
                $vObjUsuarioMenu->setArea_menu($itemInsert);
                $vObjUsuarioMenu->setCorporacao_id_INT($idCorporacaoLogada);
                $vObjUsuarioMenu->formatarParaSQL();
                $vObjUsuarioMenu->insert(true);
            }

            foreach($arrayIdsPermissoesDelete as $idDelete)
            {
                $vObjUsuarioMenu = new EXTDAO_Usuario_tipo_menu();
                $vObjUsuarioMenu->delete($idDelete, true);
            }

            if ($pIdTipoUsuario == Seguranca::getIdDoTipoDoUsuarioLogado())
            {
                $objMenu = new Menu();
                Helper::setSession(FlattyMenu::CHAVE_DE_CACHE_DO_MENU, $objMenu->getArrayDoMenuDoUsuarioLogado());
            }
        }
        catch (Exception $ex)
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
        }

        return new Mensagem(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO, null);
    }

    public static function getIdUsuarioTipo($nome, $idCorporacao, $db = null)
    {
        $q = "SELECT id "
            . " FROM usuario_tipo "
            . " WHERE nome LIKE '{$nome}' AND corporacao_id_INT = {$idCorporacao}";
        if ($db == null)
        {
            $db = new Database();
        }
        $db->query($q);

        return $db->getPrimeiraTuplaDoResultSet(0);
    }

    public static function cadastraTipoDoUsuarioAdm(
        $idUsuario, $idCorporacao, $idUsuarioAdm,
        $imei, $idPrograma, $db = null, $objSincronizador = null,
        $operacoes = null)
    {
        if ($db == null)
        {
            $db = new Database();
        }
        if (empty($idCorporacao))
        {
            $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
        }

        if (!empty($idUsuario))
        {
            if ($objSincronizador == null)
            {
                $objSincronizador = new EXTDAO_Sistema_registro_sincronizador($db);
            }

            $nomeSemAcento = Helper::retiraAcento(EXTDAO_Usuario_tipo::NOME_USUARIO_TIPO_PADRAO_ADM);
            $idUsuarioTipo = EXTDAO_Usuario_tipo::getIdUsuarioTipo($nomeSemAcento, $idCorporacao, $db);

            if (empty($idUsuarioTipo))
            {
                $idSistemaTabelaUsuarioTipo = EXTDAO_Sistema_tabela::getIdDaSistemaTabela("usuario_tipo", $db);
                $objUsuarioTipo = new EXTDAO_Usuario_tipo($db);
                $objUsuarioTipo->setNome($nomeSemAcento);
                $objUsuarioTipo->setNome_visivel(EXTDAO_Usuario_tipo::NOME_USUARIO_TIPO_PADRAO_ADM);
                $objUsuarioTipo->setStatus_BOOLEAN("1");
                $objUsuarioTipo->formatarParaSQL();
                $msg = $objUsuarioTipo->insert(false, $idCorporacao);

                if ($msg != null && $msg->erro())
                {
                    return $msg;
                }

                $idUsuarioTipo = $objUsuarioTipo->getIdDoUltimoRegistroInserido($idCorporacao);

                $objSincronizador->inserirRegistro(
                    $idSistemaTabelaUsuarioTipo, $idUsuarioAdm, $idCorporacao,
                    '0', DatabaseSincronizador::$INDEX_OPERACAO_INSERT, $imei,
                    $idPrograma, $idUsuarioTipo);
            }

            $idSistemaTabelaUsuarioTipoCorporacao = EXTDAO_Sistema_tabela::getIdDaSistemaTabela("usuario_tipo_corporacao", $db);

            $idUsuarioTipoCorporacao = EXTDAO_Usuario_tipo_corporacao::getIdUsuarioTipoCorporacao($idUsuario, $idCorporacao, null, $db);

            if (empty($idUsuarioTipoCorporacao))
            {
                $objUsuarioTipoCorporacao = new EXTDAO_Usuario_tipo_corporacao($db);
                $objUsuarioTipoCorporacao->setUsuario_id_INT($idUsuario);
                $objUsuarioTipoCorporacao->setUsuario_tipo_id_INT($idUsuarioTipo);
                $objUsuarioTipoCorporacao->formatarParaSQL();
                $msg = $objUsuarioTipoCorporacao->insert(false, $idCorporacao);
                if ($msg != null && !$msg->ok())
                {
                    return $msg;
                }

                $idUsuarioTipoCorporacao = $objUsuarioTipoCorporacao->getIdDoUltimoRegistroInserido();

                $objSincronizador->inserirRegistro(
                    $idSistemaTabelaUsuarioTipoCorporacao, $idUsuarioAdm,
                    $idCorporacao, '0', DatabaseSincronizador::$INDEX_OPERACAO_INSERT,
                    $imei, $idPrograma, $idUsuarioTipoCorporacao);

                if ($operacoes != null)
                {
                    $operacoes["usuario_tipo_corporacao"] = $idUsuarioTipoCorporacao;
                }
            }

            $objMenu = new Menu();
            $vetorAreaMenu = $objMenu->getVetorDasOpcoesDoMenu();
            Helper::corrigirObjetoParaJsonEncode($vetorAreaMenu);

            $idSistemaTabelaUsuarioTipoMenu = EXTDAO_Sistema_tabela::getIdDaSistemaTabela("usuario_tipo_menu", $db);

            $objUsuarioTipoMenu = new EXTDAO_Usuario_tipo_menu($db);
            $idsUsuarioTipoMenu = array();
            foreach ($vetorAreaMenu as $areaMenu)
            {
                $idUsuarioTipoMenu = EXTDAO_Usuario_tipo_menu::getIdUsuarioTipoMenu($idUsuarioTipo, $areaMenu, $idCorporacao, $db);
                if (empty($idUsuarioTipoMenu))
                {
                    $objUsuarioTipoMenu->clear();
                    $objUsuarioTipoMenu->setUsuario_tipo_id_INT($idUsuarioTipo);
                    $objUsuarioTipoMenu->setArea_menu($areaMenu);
                    $objUsuarioTipoMenu->formatarParaSQL();
                    $msg = $objUsuarioTipoMenu->insert(false, $idCorporacao);

                    if ($msg != null && !$msg->ok())
                    {
                        return $msg;
                    }

                    $idUsuarioTipoMenu = $objUsuarioTipoMenu->getIdDoUltimoRegistroInserido($idCorporacao);

                    $idsUsuarioTipoMenu[count($idsUsuarioTipoMenu)] = $idUsuarioTipoMenu;
                    $objSincronizador->inserirRegistro(
                        $idSistemaTabelaUsuarioTipoMenu, $idUsuarioAdm,
                        $idCorporacao, '0', DatabaseSincronizador::$INDEX_OPERACAO_INSERT,
                        $imei, $idPrograma, $idUsuarioTipoMenu);

                }
                if ($operacoes != null)
                {
                    $operacoes["usuario_tipo_menu"] = $idsUsuarioTipoMenu;
                }
            }

            return new Mensagem(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO);
        }

        return new Mensagem(PROTOCOLO_SISTEMA::ERRO_PARAMETRO_INVALIDO, "UsuarioModel indefindo");
    }

}
