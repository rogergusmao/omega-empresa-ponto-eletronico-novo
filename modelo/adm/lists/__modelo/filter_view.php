<?php
global $objFilter;
?>

<?= $objFilter->getFormDebugPanel(); ?>

<div class='row'>
    <div class='col-sm-12'>
        <div class='box'>
            <div class='box-header blue-background'>
                <div class='title'>
                    <div class='icon-edit'></div>
                    <?= I18N::getExpression("Filtro"); ?>
                </div>
                <div class='actions'>
                    <a class="btn box-collapse btn-xs btn-link" href="#"><i></i></a>
                </div>
            </div>
            <div class='box-content'>

                <form name="<?= $objFilter->getFormName() ?>" class="form form-horizontal" style="margin-bottom: 0;">

                    <fieldset>

                        <?
                        $objCampo = $objFilter->getTextInputInstance("nome");
                        $objCampo->setLabel("Nome");
                        $objCampo->setPlaceHolder("Informe o nome.");
                        $objCampo->addValidation(new MinLengthValidation(10, "O tamanho mínimo do nome deve ser de {0} caracteres."));
                        ?>

                        <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                             ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                            <?= $objCampo->renderLabel(); ?>
                            <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                <?= $objCampo->render(); ?>
                                <?= $objCampo->renderValidationBlock(); ?>

                            </div>
                        </div>

                        <?= $objFilter->renderHorizontalLine(); ?>

                        <?
                        $objCampo = $objFilter->getTextareaInstance("comentarios");
                        $objCampo->setLabel(I18N::getExpression("Comentários"));
                        $objCampo->setPlaceHolder(I18N::getExpression("Digite comentários adicionais."));
                        ?>

                        <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                             ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                            <?= $objCampo->renderLabel(); ?>
                            <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                <?= $objCampo->render(); ?>
                                <?= $objCampo->renderValidationBlock(); ?>

                            </div>
                        </div>

                        <?= $objFilter->renderHorizontalLine(); ?>

                        <?
                        $objCampo = $objFilter->getSelectInstance("pessoa");
                        $objCampo->setLabel(I18N::getExpression("Pessoa"));
                        $objCampo->setPlaceHolder(I18N::getExpression("Selecione uma pessoa da lista..."), false);
                        $objCampo->setMatchExpression("{{nome}} {{idade}}");
                        $objCampo->setCloseOnSelect(true);
                        $objCampo->setRefreshDelay(0);
                        $propsFilter = new PropsFilter();
                        $propsFilter->addFieldToFilter("nome");
                        $propsFilter->addFieldToFilter("email");
                        $selectChoiceContent = new SelectChoiceContent("pessoa");
                        $selectChoiceContent->setMainString(new AngularString("nome", "div", Select::getSelectHighLightFilter()));
                        $selectChoiceContent->addSecondaryString(I18N::getExpression("E-mail") . ": ", new AngularString("email"));
                        $selectChoiceContent->addSecondaryString(I18N::getExpression("Idade") . ": ", new AngularString("idade", "span", Select::getSelectHighLightFilter()));
                        $objCampo->setFilter($propsFilter);
                        $objCampo->setSelectChoiceContent($selectChoiceContent);
                        ?>

                        <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                             ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                            <?= $objCampo->renderLabel(); ?>
                            <div class='<?= $objCampo->getContainerCssClass() ?>'>

                                <?= $objCampo->render(); ?>
                                <?= $objCampo->renderValidationBlock(); ?>

                            </div>

                        </div>

                        <?= $objFilter->renderHorizontalLine(); ?>

                        <?
                        $objCampo = $objFilter->getCurrencyInputInstance("preco");
                        $objCampo->setLabel(I18N::getExpression("Preço"));
                        $objCampo->setDecimalPlaces(2);
                        $objCampo->setPlaceHolder(I18N::getExpression("Informe o valor em R$"));
                        $objCampo->addValidation(new MaxValueValidation(1500, I18N::getExpression("O valor máximo permitido é de R$ {f0}.")));
                        ?>

                        <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                             ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                            <?= $objCampo->renderLabel(); ?>
                            <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                <?= $objCampo->render(); ?>
                                <?= $objCampo->renderValidationBlock(); ?>

                            </div>
                        </div>

                        <?= $objFilter->renderHorizontalLine(); ?>

                        <?
                        $objCampo = $objFilter->getCepInputInstance("cep");
                        $objCampo->setLabel("CEP");
                        $objCampo->setPlaceHolder("Informe o CEP");
                        ?>

                        <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                             ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                            <?= $objCampo->renderLabel(); ?>
                            <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                <?= $objCampo->render(); ?>
                                <?= $objCampo->renderValidationBlock(); ?>

                            </div>
                        </div>

                        <?= $objFilter->renderHorizontalLine(); ?>

                        <?
                        $objCampo = $objFilter->getCpfInputInstance("cpf");
                        $objCampo->setLabel("CPF");
                        $objCampo->setPlaceHolder("Informe o CPF");
                        ?>

                        <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                             ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                            <?= $objCampo->renderLabel(); ?>
                            <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                <?= $objCampo->render(); ?>
                                <?= $objCampo->renderValidationBlock(); ?>

                            </div>
                        </div>

                        <?= $objFilter->renderHorizontalLine(); ?>

                        <?
                        $objCampo = $objFilter->getCnpjInputInstance("cnpj");
                        $objCampo->setLabel(I18N::getExpression("CNPJ"));
                        $objCampo->setPlaceHolder(I18N::getExpression("Informe o CNPJ"));
                        ?>

                        <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                             ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                            <?= $objCampo->renderLabel(); ?>
                            <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                <?= $objCampo->render(); ?>
                                <?= $objCampo->renderValidationBlock(); ?>

                            </div>
                        </div>

                        <?= $objFilter->renderHorizontalLine(); ?>

                        <?
                        $objCampo = $objFilter->getPhoneInputInstance("telefone");
                        $objCampo->setLabel(I18N::getExpression("Telefone"));
                        $objCampo->setPlaceHolder(I18N::getExpression("Telefone em padrão brasileiro"));
                        ?>

                        <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                             ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                            <?= $objCampo->renderLabel(); ?>
                            <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                <?= $objCampo->render(); ?>
                                <?= $objCampo->renderValidationBlock(); ?>

                            </div>
                        </div>

                        <?= $objFilter->renderHorizontalLine(); ?>

                        <?
                        $objCampo = $objFilter->getPercentageInputInstance("porcentagem");
                        $objCampo->setLabel(I18N::getExpression("Porcentagem"));
                        $objCampo->setPlaceHolder(I18N::getExpression("Porcentagem"));
                        ?>

                        <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                             ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                            <?= $objCampo->renderLabel(); ?>
                            <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                <?= $objCampo->render(); ?>
                                <?= $objCampo->renderValidationBlock(); ?>

                            </div>
                        </div>

                        <?= $objFilter->renderHorizontalLine(); ?>

                    </fieldset>

                    <div class='form-actions form-actions-padding-sm'>
                        <div class='row'>
                            <div class='col-md-11 col-md-offset-1'>

                                <?= $objFilter->renderSubmitButton(); ?>
                                <?= $objFilter->renderClearButton(); ?>

                            </div>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>

