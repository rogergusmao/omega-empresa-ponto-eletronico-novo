/*

Arquivo gerado atrav�s de gerador de c�digo em 21/01/2018 as 18:27:38.
Para que o arquivo n�o seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

Tabela correspondente: empresa
Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

*/

omegaApp.generatedFormEmpresaController = function ($scope, $rootScope, $element, $window, $interval, $timeout, dataService, Upload, vm)
{
    try
    {
        vm.rootControllerElement = $element;
        vm.fieldsTypes = {};

        vm.isModalView = false;
        vm.isEdition = false;
        vm.entityIdForEdition = null;

        vm.messages = {
            error: [],
            warning: [],
            info: [],
            success: []
        };

        vm.formData = {
            nome: null,
            telefone1: null,
            telefone2: null,
            fax: null,
            celular: null,
            operadoraId: null,
            celularSms: null,
            email: null,
            tipoDocumentoId: null,
            numeroDocumento: null,
            tipoEmpresaId: null,
            logradouro: null,
            numero: null,
            complemento: null,
            bairroId: null,
            cidadeId: null
        };

        vm.disabledFields = {
            fieldNome: false,
            fieldTelefone1: false,
            fieldTelefone2: false,
            fieldFax: false,
            fieldCelular: false,
            fieldOperadoraId: false,
            fieldCelularSms: false,
            fieldEmail: false,
            fieldTipoDocumentoId: false,
            fieldNumeroDocumento: false,
            fieldTipoEmpresaId: false,
            fieldLogradouro: false,
            fieldNumero: false,
            fieldComplemento: false,
            fieldBairroId: false,
            fieldCidadeId: false
        };

        vm.requiredFields = {
            fieldNome: true,
            fieldTelefone1: true,
            fieldTelefone2: true,
            fieldFax: true,
            fieldCelular: true,
            fieldOperadoraId: true,
            fieldCelularSms: true,
            fieldEmail: true,
            fieldTipoDocumentoId: true,
            fieldNumeroDocumento: true,
            fieldTipoEmpresaId: true,
            fieldLogradouro: true,
            fieldNumero: true,
            fieldComplemento: true,
            fieldBairroId: true,
            fieldCidadeId: true
        };

        vm.relatedEntities =
            {};

        vm.fieldsParameters = {
            fieldNome: null,
            fieldTelefone1: null,
            fieldTelefone2: null,
            fieldFax: null,
            fieldCelular: null,
            fieldOperadoraId: {remoteData: null},
            fieldCelularSms: null,
            fieldEmail: null,
            fieldTipoDocumentoId: {remoteData: null},
            fieldNumeroDocumento: null,
            fieldTipoEmpresaId: {remoteData: null},
            fieldLogradouro: null,
            fieldNumero: null,
            fieldComplemento: null,
            fieldBairroId: {remoteData: null},
            fieldCidadeId: {remoteData: null}
        };

        vm.arrForeignKeys = ['operadoraId', 'tipoDocumentoId', 'tipoEmpresaId', 'bairroId', 'cidadeId', 'corporacaoId'];

        var relationshipObject = {
            operadoraId: 'fieldOperadoraId',
            tipoDocumentoId: 'fieldTipoDocumentoId',
            tipoEmpresaId: 'fieldTipoEmpresaId',
            bairroId: 'fieldBairroId',
            cidadeId: 'fieldCidadeId'
        };

        vm.isInitialLoadFinished = false;
        var initialLoading = function ()
        {
            $timeout(function ()
            {
                if (AngularControllerUtil.isEditionMode(vm))
                {

                    var filterParameters = AngularControllerUtil.getFilterParametersForEdition(vm);
                    dataService.getEmpresa(filterParameters).then(function (responseContent)
                    {
                        if (RemoteDataUtil.validateRemoteResponse(responseContent, [PROTOCOLO_SISTEMA.OPERACAO_REALIZADA_COM_SUCESSO]))
                        {
                            var responseObject = RemoteDataUtil.getResponseObject(responseContent);
                            RemoteDataUtil.refactorInitialFormLoadRemoteResponseData(vm, responseObject, relationshipObject);
                            OmegaFormUtil.expandSectionPanels(vm);
                        }
                        else
                        {
                            RemoteDataUtil.defaultRemoteErrorHandling(responseContent, vm);
                        }

                        $timeout(function ()
                        {
                            vm.isInitialLoadFinished = true;
                        }, 100);

                    });
                }
                else
                {
                    dataService.getAllComboBoxesDataForEmpresaForm().then(function (returnContent)
                    {
                        if (RemoteDataUtil.validateRemoteResponse(returnContent))
                        {
                            var responseObject = RemoteDataUtil.getResponseObject(returnContent);
                            RemoteDataUtil.refactorInitialFormLoadRemoteResponseData(vm, responseObject, relationshipObject);
                        }
                        else
                        {
                            LogUtil.logRemoteResponseError(returnContent);
                        }

                        $timeout(function ()
                        {
                            vm.isInitialLoadFinished = true;
                        }, 100);

                    });
                }

            }, 200);

        };

        vm.submitForm = function (formInstance)
        {
            if ($rootScope.validateForm(formInstance, vm))
            {
                var postData = RemoteDataUtil.refactorRemoteRequest(vm.formData, vm.relatedEntities);
                if (AngularControllerUtil.isEditionMode(vm))
                {
                    dataService.editEmpresa(postData).then(function (returnContent)
                    {
                        if (RemoteDataUtil.validateRemoteResponse(returnContent))
                        {
                            var successMessage = RemoteDataUtil.getResponseMessageString(returnContent);
                            $rootScope.defaultAfterUpdateAction(vm, 'after-empresa-update-template.html', successMessage);
                        }
                        else
                        {
                            RemoteDataUtil.defaultRemoteErrorHandling(returnContent, vm);
                        }
                    });
                }
                else
                {
                    dataService.addEmpresa(postData).then(function (returnContent)
                    {
                        if (RemoteDataUtil.validateRemoteResponse(returnContent))
                        {
                            var successMessage = RemoteDataUtil.getResponseMessageString(returnContent);
                            $rootScope.defaultAfterInsertAction(vm, 'after-empresa-insert-template.html', successMessage);
                        }
                        else
                        {
                            RemoteDataUtil.defaultRemoteErrorHandling(returnContent, vm);
                        }
                    });
                }
            }

        };

        vm.openOperadoraIdModal = function ()
        {
            var modalIndex = $rootScope.getNextModalIndex();
            $rootScope.registerModalInstance($rootScope.showModalDialog('forms', 'operadora', modalIndex));

            //atualizando valores da lista
            var unregisterModal = $rootScope.$watch(function ()
            {
                return $rootScope.uibModalInstance[modalIndex]
            }, function ()
            {

                if ($rootScope.uibModalInstance[modalIndex] == null)
                {
                    vm.getListOfOperadoraId();
                    vm.formData.operadoraId = null;
                    unregisterModal();
                }

            });

        };
        vm.openTipoDocumentoIdModal = function ()
        {
            var modalIndex = $rootScope.getNextModalIndex();
            $rootScope.registerModalInstance($rootScope.showModalDialog('forms', 'tipoDocumento', modalIndex));

            //atualizando valores da lista
            var unregisterModal = $rootScope.$watch(function ()
            {
                return $rootScope.uibModalInstance[modalIndex]
            }, function ()
            {

                if ($rootScope.uibModalInstance[modalIndex] == null)
                {
                    vm.getListOfTipoDocumentoId();
                    vm.formData.tipoDocumentoId = null;
                    unregisterModal();
                }

            });

        };
        vm.openTipoEmpresaIdModal = function ()
        {
            var modalIndex = $rootScope.getNextModalIndex();
            $rootScope.registerModalInstance($rootScope.showModalDialog('forms', 'tipoEmpresa', modalIndex));

            //atualizando valores da lista
            var unregisterModal = $rootScope.$watch(function ()
            {
                return $rootScope.uibModalInstance[modalIndex]
            }, function ()
            {

                if ($rootScope.uibModalInstance[modalIndex] == null)
                {
                    vm.getListOfTipoEmpresaId();
                    vm.formData.tipoEmpresaId = null;
                    unregisterModal();
                }

            });

        };
        vm.openBairroIdModal = function ()
        {
            var modalIndex = $rootScope.getNextModalIndex();
            $rootScope.registerModalInstance($rootScope.showModalDialog('forms', 'bairro', modalIndex));

            //atualizando valores da lista
            var unregisterModal = $rootScope.$watch(function ()
            {
                return $rootScope.uibModalInstance[modalIndex]
            }, function ()
            {

                if ($rootScope.uibModalInstance[modalIndex] == null)
                {
                    vm.getListOfBairroId();
                    vm.formData.bairroId = null;
                    unregisterModal();
                }

            });

        };
        vm.openCidadeIdModal = function ()
        {
            var modalIndex = $rootScope.getNextModalIndex();
            $rootScope.registerModalInstance($rootScope.showModalDialog('forms', 'cidade', modalIndex));

            //atualizando valores da lista
            var unregisterModal = $rootScope.$watch(function ()
            {
                return $rootScope.uibModalInstance[modalIndex]
            }, function ()
            {

                if ($rootScope.uibModalInstance[modalIndex] == null)
                {
                    vm.getListOfCidadeId();
                    vm.formData.cidadeId = null;
                    unregisterModal();
                }

            });

        };

        vm.getListOfOperadoraId = function ()
        {
            if (!vm.isInitialLoadFinished)
            {
                return;
            }

            dataService.getListOfOperadoraForEmpresaForm().then(function (returnContent)
            {
                if (RemoteDataUtil.validateRemoteResponse(returnContent))
                {
                    var remoteDataSet = RemoteDataUtil.getResponseObjectDataSetOrEmptyArray(returnContent);
                    vm.fieldsParameters.fieldOperadoraId.remoteData = remoteDataSet;
                }
                else
                {
                    LogUtil.logRemoteResponseError(returnContent);
                }

            });

        };
        vm.getListOfTipoDocumentoId = function ()
        {
            if (!vm.isInitialLoadFinished)
            {
                return;
            }

            dataService.getListOfTipoDocumentoForEmpresaForm().then(function (returnContent)
            {
                if (RemoteDataUtil.validateRemoteResponse(returnContent))
                {
                    var remoteDataSet = RemoteDataUtil.getResponseObjectDataSetOrEmptyArray(returnContent);
                    vm.fieldsParameters.fieldTipoDocumentoId.remoteData = remoteDataSet;
                }
                else
                {
                    LogUtil.logRemoteResponseError(returnContent);
                }

            });

        };
        vm.getListOfTipoEmpresaId = function ()
        {
            if (!vm.isInitialLoadFinished)
            {
                return;
            }

            dataService.getListOfTipoEmpresaForEmpresaForm().then(function (returnContent)
            {
                if (RemoteDataUtil.validateRemoteResponse(returnContent))
                {
                    var remoteDataSet = RemoteDataUtil.getResponseObjectDataSetOrEmptyArray(returnContent);
                    vm.fieldsParameters.fieldTipoEmpresaId.remoteData = remoteDataSet;
                }
                else
                {
                    LogUtil.logRemoteResponseError(returnContent);
                }

            });

        };
        vm.getListOfBairroId = function ()
        {
            if (!vm.isInitialLoadFinished)
            {
                return;
            }

            dataService.getListOfBairroForEmpresaForm().then(function (returnContent)
            {
                if (RemoteDataUtil.validateRemoteResponse(returnContent))
                {
                    var remoteDataSet = RemoteDataUtil.getResponseObjectDataSetOrEmptyArray(returnContent);
                    vm.fieldsParameters.fieldBairroId.remoteData = remoteDataSet;
                }
                else
                {
                    LogUtil.logRemoteResponseError(returnContent);
                }

            });

        };
        vm.getListOfCidadeId = function ()
        {
            if (!vm.isInitialLoadFinished)
            {
                return;
            }

            dataService.getListOfCidadeForEmpresaForm().then(function (returnContent)
            {
                if (RemoteDataUtil.validateRemoteResponse(returnContent))
                {
                    var remoteDataSet = RemoteDataUtil.getResponseObjectDataSetOrEmptyArray(returnContent);
                    vm.fieldsParameters.fieldCidadeId.remoteData = remoteDataSet;
                }
                else
                {
                    LogUtil.logRemoteResponseError(returnContent);
                }

            });

        };

        vm.loadListOfEmpresa = function ()
        {
            document.location.href = 'index.php?tipo=lists&page=empresa';
        };

        vm.addNewEmpresa = function ()
        {
            document.location.href = document.location.href;
        };

        return {

            scope: this,
            initFunction: initialLoading

        };

    }
    catch (ex)
    {
        ApplicationUtil.showErrorMessage(ex.message, 'Erro no controller generatedFormEmpresaController', vm);
    }

}