"use strict";
exports.__esModule = true;
var FonteInformacao;
(function (FonteInformacao) {
    FonteInformacao[FonteInformacao["NAO_DEFINIDO"] = null] = "NAO_DEFINIDO";
    FonteInformacao[FonteInformacao["GPS"] = 1] = "GPS";
    FonteInformacao[FonteInformacao["AGPS"] = 2] = "AGPS";
})(FonteInformacao = exports.FonteInformacao || (exports.FonteInformacao = {}));
