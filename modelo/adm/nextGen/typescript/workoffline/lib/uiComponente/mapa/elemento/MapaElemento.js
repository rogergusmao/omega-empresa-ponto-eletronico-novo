"use strict";
exports.__esModule = true;
var MapaElemento = (function () {
    function MapaElemento(contexto, latLngs, tipoElemento) {
        this.contexto = contexto;
        this.contexto.mapObject.addComponente(this);
        this.elementsLatLngs = new Array();
        this.elementsLatLngs = this.elementsLatLngs.concat(latLngs);
        this.tipoElemento = tipoElemento;
        this.htmlElement = null;
    }
    MapaElemento.getNextIdentifier = function () {
        return "workoffline-element-" + (++MapaElemento.CurrentIdentifierCounter);
    };
    MapaElemento.prototype.getTipoElemento = function () {
        return this.tipoElemento;
    };
    MapaElemento.prototype.getGoogleMapInstance = function () {
        return this.contexto.mapObject.getGoogleMapInstance();
    };
    return MapaElemento;
}());
MapaElemento.CurrentIdentifierCounter = 0;
exports.MapaElemento = MapaElemento;
