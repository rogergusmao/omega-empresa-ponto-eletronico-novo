<?php

if (strlen(Helper::GET("logradouro")) && strlen(Helper::GET("numero")) && strlen(Helper::GET("bairro")) && strlen(Helper::GET("cidade")))
{

    $enderecoGoogle = Helper::GET("logradouro") . ", " . Helper::GET("numero") . " - " . Helper::GET("bairro") . " - " . Helper::GET("cidade");

    $api = new NXGoogleMapsAPI();

    // setup the visual design of the control
    $api->setWidth(750);
    $api->setHeight(550);
    $api->setZoomFactor(14);
    $api->addControl(GLargeMapControl);
    $api->addControl(GMapTypeControl);
    $api->addControl(GOverviewMapControl);

    $api->addAddress($enderecoGoogle, $enderecoGoogle);

    echo $api->getHeadCode();

    echo "<center>";
    echo $api->getBodyCode();
    echo "</center>";

    ?>

    <script language="javascript">

        $(document).ready(function ()
        {

            <?=$api->getOnLoadCode(); ?>

        });

    </script>

    <?
}
else
{

    Helper::imprimirMensagem("Endereço Inválido.
                              Os campos <font color=#FF0000>Logradouro</font>, <font color=#FF0000>Número</font>, <font color=#FF0000>Cidade</font> e <font color=#FF0000>Bairro</font> são obrigatórios.", MENSAGEM_ERRO);

    Helper::imprimirComandoJavascriptComTimer(COMANDO_FECHAR_DIALOG, 5);
}

?>
