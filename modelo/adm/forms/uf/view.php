<?php

/*

Arquivo gerado através de gerador de código em 11/01/2018 as 18:52:37.
Para que o arquivo não seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

Tabela correspondente: uf
Sobrescrita de dados: ---SOBRESCRITA_NAO_AUTORIZADA---

*/

?>

<!--Definição da View Angular JS -->
<?

$objForm = new FormEnvironment("mainForm", "formUfController", "vm");
$objForm->setLayoutOneColumn();

?>
<div class='row' id='content-wrapper' ng-controller="<?= $objForm->getAngularControllerAttributeValue() ?>">

    <?= $objForm->setModalEnvironmentIfApplicable(); ?>
    <?= $objForm->setEditionVariablesIfApplicable(); ?>

    <!--Definição do template do dialog exibido após inserção -->
    <script type="text/ng-template" id="after-uf-insert-template.html">
        <div class="modal-header">
            <h4 class="modal-title">
                <span class="glyphicon"></span>
                <?= I18N::getExpression("Unidade federativa adicionada com sucesso!"); ?>
            </h4>
        </div>
        <div class="modal-body">
            <span class="help-block"><?= I18N::getExpression("O que deseja fazer agora?"); ?></span>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" ng-click="vm.loadListOfUf()">
                <?= I18N::getExpression("Listar unidades federativas"); ?>
            </button>
            <button type="button" class="btn" ng-click="closeMessageDialog()">
                <?= I18N::getExpression("Permanecer na tela"); ?>
            </button>
            <button type="button" class="btn btn-success" ng-click="vm.addNewUf()">
                <?= I18N::getExpression("Adicionar unidade federativa"); ?>
            </button>
        </div>
    </script>

    <!--Definição do template do exibido dialog após update -->
    <script type="text/ng-template" id="after-uf-update-template.html">
        <div class="modal-header">
            <h4 class="modal-title">
                <span class="glyphicon"></span>
                <?= I18N::getExpression("Unidade federativa alterada com sucesso!"); ?>

            </h4>
        </div>
        <div class="modal-body">
            <span class="help-block"><?= I18N::getExpression("O que deseja fazer agora?"); ?></span>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" ng-click="vm.loadListOfUf()">
                <?= I18N::getExpression("Listar unidade federativa"); ?>
            </button>
            <button type="button" class="btn" ng-click="closeMessageDialog()">
                <?= I18N::getExpression("Permanecer na tela"); ?>
            </button>
            <button type="button" class="btn btn-success" ng-click="vm.addNewUf()">
                <?= I18N::getExpression("Adicionar unidade federativa"); ?>
            </button>
        </div>
    </script>

    <div class='col-xs-12'>

        <div class='row'>
            <div class='col-sm-12'>
                <div class='page-header'>
                    <h1 class='pull-left'>
                        <i class='icon-edit'></i>
                        <span><?= I18N::getExpression("Cadastro de unidade federativa"); ?></span>
                    </h1>
                    <div class='pull-right close-button-container'>
                        <?= $objForm->renderCloseModalButtonIfExists(); ?>
                    </div>
                </div>
            </div>
        </div>

        <?= $objForm->getFormDebugPanel(); ?>
        <?= $objForm->getModalMessagesPanel(); ?>

        <div class='row'>
            <div class='col-sm-12'>
                <div class='box'>
                    <div class='box-header blue-background'>
                        <div class='title'>
                            <div class='icon-edit'></div>
                            <?= I18N::getExpression("Dados da unidade federativa"); ?>
                        </div>
                        <div class='actions hidden'>
                            <a class="btn box-collapse btn-xs btn-link" href="javascript:void(0);"><i></i></a>
                        </div>
                    </div>
                    <div class='box-content'>

                        <form name="<?= $objForm->getFormName() ?>" class="form form-horizontal"
                              style="margin-bottom: 0;" novalidate>


                            <?php

                            $objCampo = $objForm->getTextInputInstance("nome", TextInput::TRANSFORMATION_DIRECTIVE_UPPER);
                            $objCampo->setLabel(I18N::getExpression("Nome"));

                            $objCampo->setDefaultValues();
                            $objCampo->setPlaceHolder("Informe o Nome.");
                            $objCampo->addValidation(new MaxLengthValidation(255, I18N::getExpression("O tamanho máximo do Nome deve ser de {0} caracteres.")));
                            $objCampo->addValidation(new RequiredValidation(I18N::getExpression("O Nome é de preenchimento obrigatório.")));

                            ?>

                            <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                 ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                                <?= $objCampo->renderLabel(); ?>
                                <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                    <?= $objCampo->render(); ?>
                                    <?= $objCampo->renderValidationBlock(); ?>

                                </div>


                            </div>

                            <?= $objForm->renderHorizontalLine(); ?>



                            <?php

                            $objCampo = $objForm->getTextInputInstance("sigla", TextInput::TRANSFORMATION_DIRECTIVE_UPPER);
                            $objCampo->setLabel(I18N::getExpression("Sigla"));

                            $objCampo->setDefaultValues();
                            $objCampo->setPlaceHolder("Informe o Sigla.");
                            $objCampo->addValidation(new MaxLengthValidation(2, I18N::getExpression("O tamanho máximo do Sigla deve ser de {0} caracteres.")));
                            $objCampo->addValidation(new RequiredValidation(I18N::getExpression("O Sigla é de preenchimento obrigatório.")));

                            ?>

                            <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                 ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                                <?= $objCampo->renderLabel(); ?>
                                <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                    <?= $objCampo->render(); ?>
                                    <?= $objCampo->renderValidationBlock(); ?>

                                </div>


                            </div>

                            <?= $objForm->renderHorizontalLine(); ?>



                            <?php

                            $objCampo = $objForm->getSelectInstance("paisId");
                            $objCampo->setLabel(I18N::getExpression("País"));

                            $objCampo->setPlaceHolder(I18N::getExpression("Selecione um país da lista..."), false);
                            $objCampo->setMatchExpression("{{nome}}");
                            $objCampo->setCloseOnSelect(true);

                            $propsFilter = new PropsFilter();
                            $propsFilter->addFieldToFilter("nome");

                            $selectChoiceContent = new SelectChoiceContent("paisId");
                            $selectChoiceContent->setMainString(new AngularString("nome", "div", Select::getSelectHighLightFilter()));

                            $objCampo->setFilter($propsFilter);
                            $objCampo->setSelectChoiceContent($selectChoiceContent);
                            $objCampo->addValidation(new RequiredValidation(I18N::getExpression("O País é de preenchimento obrigatório.")));

                            ?>

                            <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                 ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                                <?= $objCampo->renderLabel(); ?>
                                <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                    <?= $objCampo->render(); ?>
                                    <?= $objCampo->renderValidationBlock(); ?>

                                </div>

                                <div class='col-md-2'>

                                    <?= $objCampo->renderAddEntityButton(I18N::getExpression("Adicionar País")); ?>

                                </div>

                            </div>

                            <?= $objForm->renderHorizontalLine(); ?>

                            <div class='form-actions form-actions-padding-sm'>
                                <div class='row'>
                                    <div class='col-md-10 action-buttons-bar'>

                                        <?= $objForm->renderSubmitButton(); ?>

                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php

if (!Helper::isAngularModal())
{
    $arrDataServicesRelacionados = array("forms/pais");
    $arrControllersRelacionados = array("forms/pais");

    ?>

    <!--Definição do Controller Angular JS -->
    <script type="text/javascript">

        <?=Helper::incluirConteudoDataServices(true, $arrDataServicesRelacionados); ?>

    </script>

    <?= Helper::incluirControllerAngularJS(true, $arrControllersRelacionados); ?>

<?php } ?>

        
