<?php

class NextGenLoader
{
    public static function importarBibliotecaGoogleMaps($jsCallbackFunction = null)
    {
        $str = "";
        $strPathBase = __DOMINIO_BIBLIOTECAS_COMPARTILHADAS . "/libs/google-maps";
        $apiKey = "AIzaSyCcvsX-Fm2bSzGCx0LUMTQQbZZ0Q21hRjY";

        if(!is_null($jsCallbackFunction))
        {
            $str .= "<script src=\"https://maps.googleapis.com/maps/api/js?key={$apiKey}&callback={$jsCallbackFunction}\" async defer></script>";
        }
        else
        {
            $str .= "<script src=\"https://maps.googleapis.com/maps/api/js?key={$apiKey}\"></script>";
            $str .= "<script src=\"{$strPathBase}/marker-with-label/1.1.10.js\" type=\"text/javascript\"></script>";
        }

        return $str;
    }

    public static function importarBibliotecasMapa()
    {
        $str = "";
        $raizProjeto = Javascript::getPathAteRaizDoProjeto();

        $pathRelativoJavascript = "{$raizProjeto}/adm/nextGen/js";
        $pathRelativoCss = "{$raizProjeto}/adm/nextGen/css";

        $str .= Javascript::importarBibliotecaMoment();
        $arrArquivosJS = array("vendor.MapaIntegradoLoader", "MapaIntegradoLoader");
        foreach ($arrArquivosJS as $arquivoJS)
        {
            $str .= Helper::carregarArquivoJavascript(0, $pathRelativoJavascript,$arquivoJS);
        }

        $arrArquivosCSS = array("default", "mapa");
        foreach ($arrArquivosCSS as $arquivoCSS)
        {
            $str .= Helper::carregarArquivoCss(0, $pathRelativoCss,$arquivoCSS);
        }

        return $str;
    }

    public static function importarBibliotecasCalendarioDePontos()
    {
        $str = "";
        $raizProjeto = Javascript::getPathAteRaizDoProjeto();

        $pathRelativoJavascript = "{$raizProjeto}/adm/nextGen/js";
        $pathRelativoCss = "{$raizProjeto}/adm/nextGen/css";

        $str .= Javascript::importarBibliotecaMoment();
        $str .= Javascript::importarBibliotecaFullCalendar();

        $arrArquivosJS = array("vendor.CalendarioDePontosLoader", "CalendarioDePontosLoader");
        foreach ($arrArquivosJS as $arquivoJS)
        {
            $str .= Helper::carregarArquivoJavascript(0, $pathRelativoJavascript,$arquivoJS);
        }

        $arrArquivosCSS = array("default", "calendarioDePontos");
        foreach ($arrArquivosCSS as $arquivoCSS)
        {
            $str .= Helper::carregarArquivoCss(0, $pathRelativoCss,$arquivoCSS);
        }

        return $str;
    }

}