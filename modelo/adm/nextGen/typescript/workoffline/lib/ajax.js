"use strict";
exports.__esModule = true;
var RemoteRequest_1 = require("./ajax/RemoteRequest");
exports.RemoteRequest = RemoteRequest_1.RemoteRequest;
var RemoteRequestEmContexto_1 = require("./ajax/RemoteRequestEmContexto");
exports.RemoteRequestEmContexto = RemoteRequestEmContexto_1.RemoteRequestEmContexto;
var RemoteResponse_1 = require("./ajax/RemoteResponse");
exports.RemoteResponse = RemoteResponse_1.RemoteResponse;
