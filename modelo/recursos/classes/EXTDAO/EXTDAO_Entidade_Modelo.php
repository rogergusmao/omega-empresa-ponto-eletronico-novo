<?php //@@NAO_MODIFICAR

/*
*
* -------------------------------------------------------
* NOME DA CLASSE:     EXTDAO_Acesso
* NOME DA CLASSE DAO: DAO_Acesso
* DATA DE GERAÇÃO:    08.02.2010
* ARQUIVO:            EXTDAO_Acesso.php
* TABELA MYSQL:       acesso
* BANCO DE DADOS:     dep_pesquisas
* -------------------------------------------------------
*
*/

// **********************
// DECLARAÇÃO DA CLASSE
// **********************

class EXTDAO_Entidade_Modelo extends DAO_Entidade_Modelo
{
    public function __construct($configDAO = null)
    {
        parent::__construct($configDAO);
        $this->nomeClasse = "EXTDAO_Entidade_Modelo";
    }

    public function factory()
    {
        return new EXTDAO_Entidade_Modelo();
    }
}

