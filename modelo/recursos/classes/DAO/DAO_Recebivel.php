<?php


        /*

        Arquivo gerado através de gerador de código em 12/05/2018 as 14:48:57.
        Para que o arquivo não seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

        Tabela correspondente: recebivel
        Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

        */
        


class DAO_Recebivel extends Generic_DAO
                        {
                            const NOME_ENTIDADE = "recebivel";

                            public static $nomeTabela = null;
                            public static $idSistemaTabela = false;
                            public static $databaseFieldsRelatedAttributes = null;
	                        public static $databaseFieldTypes = null;
	                        public static $databaseFieldNames = null;
	                        public static $listAliasTypes = null;
	                        public static $listAliasRelatedAttributes = null;
	                        
	                        public $id;
public $valorFloat;
public $devedorEmpresaId;
public $devedorPessoaId;
public $cadastroUsuarioId;
public $cadastroSec;
public $cadastroOffsec;
public $valorPagamentoFloat;
public $pagamentoSec;
public $pagamentoOffsec;
public $recebedorUsuarioId;
public $relatorioId;
public $protocoloInt;
public $corporacaoId;

public $labelId;
public $labelValorFloat;
public $labelDevedorEmpresaId;
public $labelDevedorPessoaId;
public $labelCadastroUsuarioId;
public $labelCadastroSec;
public $labelCadastroOffsec;
public $labelValorPagamentoFloat;
public $labelPagamentoSec;
public $labelPagamentoOffsec;
public $labelRecebedorUsuarioId;
public $labelRelatorioId;
public $labelProtocoloInt;
public $labelCorporacaoId;

public static $arrPK = array("id");

public function __construct($configDAO = null)
        {
            parent::__construct($configDAO);
            static::__setStaticValues($this->database);           
        }

public static function idSistemaTabela($databaseObject = null){
            if(!(static::$idSistemaTabela))
            {
                static::$idSistemaTabela = EXTDAO_Sistema_tabela::getIdDaSistemaTabela(static::nomeTabela(), $databaseObject);
            }
            return static::$idSistemaTabela;
        }

public function getIdSistemaTabela(){
            return static::idSistemaTabela();
        }

public static function nomeTabela(){
            if(is_null(static::$nomeTabela))
            {
                static::$nomeTabela = "recebivel";
            }
            return static::$nomeTabela;
        }

public function getNomeTabela(){
            return static::nomeTabela();
        }

public static function __setStaticValues($databaseObject = null)
                   {    
                        static::nomeTabela();
                        
                        static::idSistemaTabela($databaseObject);
                   
                        static::setDatabaseFieldNames();
                        static::setDatabaseFieldTypes();
                        static::setDatabaseFieldsRelatedAttributeNames();
                        
                        static::setListAliasTypes();
                        static::setListAliasRelatedAttributes();
                   }

public function getExpressaoLabel()
                   {
                        return "{id}";
                   }

public static function setDatabaseFieldNames()
        {
            if(is_null(static::$databaseFieldNames))
            {
                static::$databaseFieldNames = new stdClass();
                static::$databaseFieldNames->id = "id";
static::$databaseFieldNames->valorFloat = "valor_FLOAT";
static::$databaseFieldNames->devedorEmpresaId = "devedor_empresa_id_INT";
static::$databaseFieldNames->devedorPessoaId = "devedor_pessoa_id_INT";
static::$databaseFieldNames->cadastroUsuarioId = "cadastro_usuario_id_INT";
static::$databaseFieldNames->cadastroSec = "cadastro_SEC";
static::$databaseFieldNames->cadastroOffsec = "cadastro_OFFSEC";
static::$databaseFieldNames->valorPagamentoFloat = "valor_pagamento_FLOAT";
static::$databaseFieldNames->pagamentoSec = "pagamento_SEC";
static::$databaseFieldNames->pagamentoOffsec = "pagamento_OFFSEC";
static::$databaseFieldNames->recebedorUsuarioId = "recebedor_usuario_id_INT";
static::$databaseFieldNames->relatorioId = "relatorio_id_INT";
static::$databaseFieldNames->protocoloInt = "protocolo_INT";
static::$databaseFieldNames->corporacaoId = "corporacao_id_INT";

            }
                      
        }

public static function setDatabaseFieldTypes()
        {
            if(is_null(static::$databaseFieldTypes))
            {
                static::$databaseFieldTypes = new stdClass();
                static::$databaseFieldTypes->id = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->valorFloat = static::TIPO_VARIAVEL_FLOAT;
static::$databaseFieldTypes->devedorEmpresaId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->devedorPessoaId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->cadastroUsuarioId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->cadastroSec = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$databaseFieldTypes->cadastroOffsec = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$databaseFieldTypes->valorPagamentoFloat = static::TIPO_VARIAVEL_FLOAT;
static::$databaseFieldTypes->pagamentoSec = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$databaseFieldTypes->pagamentoOffsec = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$databaseFieldTypes->recebedorUsuarioId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->relatorioId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->protocoloInt = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->corporacaoId = static::TIPO_VARIAVEL_INTEGER;

            }
            
        }

public static function setDatabaseFieldsRelatedAttributeNames()
        {
            if(is_null(static::$databaseFieldsRelatedAttributes))
            {
                static::$databaseFieldsRelatedAttributes = new stdClass();
                static::$databaseFieldsRelatedAttributes->id = "id";
static::$databaseFieldsRelatedAttributes->valor_FLOAT = "valorFloat";
static::$databaseFieldsRelatedAttributes->devedor_empresa_id_INT = "devedorEmpresaId";
static::$databaseFieldsRelatedAttributes->devedor_pessoa_id_INT = "devedorPessoaId";
static::$databaseFieldsRelatedAttributes->cadastro_usuario_id_INT = "cadastroUsuarioId";
static::$databaseFieldsRelatedAttributes->cadastro_SEC = "cadastroSec";
static::$databaseFieldsRelatedAttributes->cadastro_OFFSEC = "cadastroOffsec";
static::$databaseFieldsRelatedAttributes->valor_pagamento_FLOAT = "valorPagamentoFloat";
static::$databaseFieldsRelatedAttributes->pagamento_SEC = "pagamentoSec";
static::$databaseFieldsRelatedAttributes->pagamento_OFFSEC = "pagamentoOffsec";
static::$databaseFieldsRelatedAttributes->recebedor_usuario_id_INT = "recebedorUsuarioId";
static::$databaseFieldsRelatedAttributes->relatorio_id_INT = "relatorioId";
static::$databaseFieldsRelatedAttributes->protocolo_INT = "protocoloInt";
static::$databaseFieldsRelatedAttributes->corporacao_id_INT = "corporacaoId";

            }           
        }

public function setAllFieldsToHumanFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["valor_FLOAT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["devedor_empresa_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["devedor_pessoa_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["cadastro_usuario_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["cadastro_SEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["cadastro_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["valor_pagamento_FLOAT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["pagamento_SEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["pagamento_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["recebedor_usuario_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["relatorio_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["protocolo_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
}

public function setAllFieldsToDatabaseFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["valor_FLOAT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["devedor_empresa_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["devedor_pessoa_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["cadastro_usuario_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["cadastro_SEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["cadastro_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["valor_pagamento_FLOAT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["pagamento_SEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["pagamento_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["recebedor_usuario_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["relatorio_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["protocolo_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
}

  public function getFkObjDevedor_empresa() 
                {
                    if($this->objDevedor_empresa == null)
                    {                        
                        $this->objDevedor_empresa = new EXTDAO_Devedor_empresa_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getDevedor_empresa_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objDevedor_empresa->clear();
                    }
                    elseif($this->objDevedor_empresa->getId() != $idFK)
                    {
                        $this->objDevedor_empresa->select($idFK);
                    }
                    return $this->objDevedor_empresa;
                }
  public function getFkObjDevedor_pessoa() 
                {
                    if($this->objDevedor_pessoa == null)
                    {                        
                        $this->objDevedor_pessoa = new EXTDAO_Devedor_pessoa_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getDevedor_pessoa_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objDevedor_pessoa->clear();
                    }
                    elseif($this->objDevedor_pessoa->getId() != $idFK)
                    {
                        $this->objDevedor_pessoa->select($idFK);
                    }
                    return $this->objDevedor_pessoa;
                }
  public function getFkObjCadastro_usuario() 
                {
                    if($this->objCadastro_usuario == null)
                    {                        
                        $this->objCadastro_usuario = new EXTDAO_Cadastro_usuario_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getCadastro_usuario_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objCadastro_usuario->clear();
                    }
                    elseif($this->objCadastro_usuario->getId() != $idFK)
                    {
                        $this->objCadastro_usuario->select($idFK);
                    }
                    return $this->objCadastro_usuario;
                }
  public function getFkObjRecebedor_usuario() 
                {
                    if($this->objRecebedor_usuario == null)
                    {                        
                        $this->objRecebedor_usuario = new EXTDAO_Recebedor_usuario_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getRecebedor_usuario_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objRecebedor_usuario->clear();
                    }
                    elseif($this->objRecebedor_usuario->getId() != $idFK)
                    {
                        $this->objRecebedor_usuario->select($idFK);
                    }
                    return $this->objRecebedor_usuario;
                }
  public function getFkObjRelatorio() 
                {
                    if($this->objRelatorio == null)
                    {                        
                        $this->objRelatorio = new EXTDAO_Relatorio_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getRelatorio_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objRelatorio->clear();
                    }
                    elseif($this->objRelatorio->getId() != $idFK)
                    {
                        $this->objRelatorio->select($idFK);
                    }
                    return $this->objRelatorio;
                }
  public function getFkObjCorporacao() 
                {
                    if($this->objCorporacao == null)
                    {                        
                        $this->objCorporacao = new EXTDAO_Corporacao_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getCorporacao_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objCorporacao->clear();
                    }
                    elseif($this->objCorporacao->getId() != $idFK)
                    {
                        $this->objCorporacao->select($idFK);
                    }
                    return $this->objCorporacao;
                }


public function setLabels()
        {$this->labelId = "";
$this->labelValorFloat = "";
$this->labelDevedorEmpresaId = "";
$this->labelDevedorPessoaId = "";
$this->labelCadastroUsuarioId = "";
$this->labelCadastroSec = "";
$this->labelCadastroOffsec = "";
$this->labelValorPagamentoFloat = "";
$this->labelPagamentoSec = "";
$this->labelPagamentoOffsec = "";
$this->labelRecebedorUsuarioId = "";
$this->labelRelatorioId = "";
$this->labelProtocoloInt = "";
$this->labelCorporacaoId = "";
}

public function __actionAdd($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                $this->setByObject($parameters);
                $this->setDataCadastroAndOffsetInSecondsFromUser($parameters);
                
                $this->formatarParaSQL();
                $msg = $this->insert(true);
                
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                return new Mensagem(null, I18N::getExpression("Recebivel adicionado com sucesso."));
                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionEdit($parameters = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try {

                $this->setByObject($parameters);
                $this->setAllFieldsToHumanFormat();
                
                $this->formatarParaSQL();
                $msg = $this->updateByInstanceUsingParameters($this->id, $parameters, true);
                if($msg != null && $msg->erro())
                {
                     return $msg;
                }
        
                return new Mensagem(null, I18N::getExpression("Recebivel editado com sucesso."));
            }
            catch(Exception $ex)
            {                
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionRemove($parameters = null)
        {
            $successulRemovalCount = 0;
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                foreach($parameters as $record)
                {
                    if(is_numeric($record->id))
                    {
                        $msg = $this->delete($record->id, true);
                        if($msg == null || $msg->ok())
                        {
                            $successulRemovalCount++;
                        }
                    }
                }
                
                if($successulRemovalCount > 0)
                {
                    $successMessage = count($successulRemovalCount) > 1 ? I18N::getExpression("{0} Recebivel foram removidos com sucesso.", $successulRemovalCount) : I18N::getExpression("Recebivel removido com sucesso.");
                    return new Mensagem(null, $successMessage);
                }
                else
                {
                    $errorMessage = count($parameters) > 1 ? I18N::getExpression("Falha ao remover Recebivel.") : I18N::getExpression("Falha ao remover Recebivel.");
                    return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, $errorMessage);                    
                }
            }
            catch (Exception $ex) 
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }

public function __getRecord($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }
            
            try
            {            
                $returnObject = new stdClass();            
                $whereClause = static::getWhereClauseForFilter($parameters->filterParameters);
                $query = "SELECT  id, valor_FLOAT, devedor_empresa_id_INT, devedor_pessoa_id_INT, cadastro_usuario_id_INT, cadastro_SEC, cadastro_OFFSEC, valor_pagamento_FLOAT, pagamento_SEC, pagamento_OFFSEC, recebedor_usuario_id_INT, relatorio_id_INT, protocolo_INT, corporacao_id_INT FROM recebivel {$whereClause}";
    
                $msg = $this->database->queryMensagem($query);                
                if ($msg == null)
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Não foi encontrado nenhum registro"));
                    }                    
                    $resultSet = $this->database->result;
                    $resultObject = static::mapResultSetDataToRecord($resultSet);
                    
                    $returnObject->formData = $resultObject;
                    $returnObject->comboBoxesData = null;
                    if($parameters->loadComboBoxesData)
                    {
                        $this->setByObject($resultObject);
                        $returnObject->comboBoxesData =  $this->getFormComboBoxesData();
                    }                    
    
                    
    
                    return new Mensagem_generica($returnObject);
                } 
                else 
                {
                    return $msg;
                }
                
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }


        
        public function __getFilterComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
                
        public function __getFormComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
        
        protected function getFormComboBoxesData()
        {
            $comboBoxesData = new stdClass();
            $listParameters = new stdClass();
            $listParameters->listMappingType = static::LIST_MAPPING_TYPE_COMBOBOX;
            
            
                    
                    $objRelatorio = new EXTDAO_Relatorio();
                    $comboBoxesData->fieldRelatorioId = $objRelatorio->__getList($listParameters);

            return $comboBoxesData;
        }
        
        


        
        public static function setListAliasTypes()
        {
            if(is_null(static::$listAliasTypes))
            {
                static::$listAliasTypes = new stdClass();
                static::$listAliasTypes->relatorio__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->recebivel__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->recebivel__valor_FLOAT = static::TIPO_VARIAVEL_FLOAT;
static::$listAliasTypes->recebivel__devedor_empresa_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->recebivel__devedor_pessoa_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->recebivel__cadastro_usuario_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->recebivel__cadastro_SEC = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$listAliasTypes->recebivel__valor_pagamento_FLOAT = static::TIPO_VARIAVEL_FLOAT;
static::$listAliasTypes->recebivel__pagamento_SEC = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$listAliasTypes->recebivel__recebedor_usuario_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->recebivel__tipo_relatorio_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->recebivel__protocolo_INT = static::TIPO_VARIAVEL_INTEGER;
            }        
        }
        
        public static function setListAliasRelatedAttributes()
        {
            if(is_null(static::$listAliasRelatedAttributes))
            {
                static::$listAliasRelatedAttributes = new stdClass();
                static::$listAliasRelatedAttributes->relatorio__id = "relatorioId";
static::$listAliasRelatedAttributes->recebivel__id = "id";
static::$listAliasRelatedAttributes->recebivel__valor_FLOAT = "valorFloat";
static::$listAliasRelatedAttributes->recebivel__devedor_empresa_id_INT = "devedorEmpresaId";
static::$listAliasRelatedAttributes->recebivel__devedor_pessoa_id_INT = "devedorPessoaId";
static::$listAliasRelatedAttributes->recebivel__cadastro_usuario_id_INT = "cadastroUsuarioId";
static::$listAliasRelatedAttributes->recebivel__cadastro_SEC = "cadastroSec";
static::$listAliasRelatedAttributes->recebivel__valor_pagamento_FLOAT = "valorPagamentoFloat";
static::$listAliasRelatedAttributes->recebivel__pagamento_SEC = "pagamentoSec";
static::$listAliasRelatedAttributes->recebivel__recebedor_usuario_id_INT = "recebedorUsuarioId";
static::$listAliasRelatedAttributes->recebivel__tipo_relatorio_id_INT = "tipoRelatorioId";
static::$listAliasRelatedAttributes->recebivel__protocolo_INT = "protocoloInt";
            }         
        }

        public function __getList($parameters = null, $idCorporacao = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try
            {
                $filterParameters = new stdClass();
                if(isset($parameters->filterParameters))
                {
                    $filterParameters = $parameters->filterParameters;
                }
                                    
                            
            if(is_null($idCorporacao))
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            ListParameters::addFilterParameterToObject($filterParameters, "corporacaoId", $idCorporacao);           
                
                
                $sortingParameters = null;
                if(isset($parameters->sortingParameters))
                {
                    $sortingParameters = $parameters->sortingParameters;
                }
                
                $paginationParameters = null;
                if(isset($parameters->paginationParameters))
                {
                    $paginationParameters = $parameters->paginationParameters;
                }
                
                $listMappingType = static::getListMappingType($parameters->listMappingType);
                $mainTableAlias = "r";

                $whereClause = static::getWhereClauseForFilter($filterParameters, $mainTableAlias);
                $orderByClause = static::getOrderByClauseForFilter($sortingParameters, $mainTableAlias);
                $limitClause = static::getLimitClauseForFilter($paginationParameters);

                $queryWithoutLimit = "SELECT r.id FROM recebivel r {$whereClause}";
                $query = "SELECT r1.id AS relatorio__id, r.id AS recebivel__id, r.valor_FLOAT AS recebivel__valor_FLOAT, r.devedor_empresa_id_INT AS recebivel__devedor_empresa_id_INT, r.devedor_pessoa_id_INT AS recebivel__devedor_pessoa_id_INT, r.cadastro_usuario_id_INT AS recebivel__cadastro_usuario_id_INT, r.cadastro_SEC AS recebivel__cadastro_SEC, r.valor_pagamento_FLOAT AS recebivel__valor_pagamento_FLOAT, r.pagamento_SEC AS recebivel__pagamento_SEC, r.recebedor_usuario_id_INT AS recebivel__recebedor_usuario_id_INT, r.tipo_relatorio_id_INT AS recebivel__tipo_relatorio_id_INT, r.protocolo_INT AS recebivel__protocolo_INT FROM recebivel r LEFT JOIN relatorio r1 ON r1.id = r.relatorio_id_INT {$whereClause} {$orderByClause} {$limitClause}";

                $msg = $this->database->queryMensagem($queryWithoutLimit);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                
                $totalNumberOfRecords = $this->database->rows();
                if($totalNumberOfRecords == 0)
                {
                    return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                }                        
                        
                $msg = $this->database->queryMensagem($query);
                
                //operacao realizaca com sucesso
                if ($msg == null) 
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                    }                        
                
                    $resultSet = $this->database->result;

                    $objReturn = new stdClass();
                    $objReturn->dataSet = static::mapResultSetDataToList($resultSet, $listMappingType, static::$listAliasTypes);

                    if(!Helper::isNullOrEmpty($paginationParameters->recordsPerPage) && !Helper::isNullOrEmpty($paginationParameters->currentPage))
                    {
                        $lastPage = ceil($totalNumberOfRecords / $paginationParameters->recordsPerPage);
                        $objReturn->paginationParameters = new PaginationParams($paginationParameters->recordsPerPage, $paginationParameters->currentPage, $lastPage);
                    }
                    
                    return new Mensagem_generica($objReturn);
                } 
                else
                {
                    return $msg;
                }                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }



public function getId()
            {
                return $this->id;
            }

public function getValorFloat()
            {
                return $this->valorFloat;
            }

public function getValor_FLOAT()
                {
                    return $this->valorFloat;
                }

public function getDevedorEmpresaId()
            {
                return $this->devedorEmpresaId;
            }

public function getDevedor_empresa_id_INT()
                {
                    return $this->devedorEmpresaId;
                }

public function getDevedorPessoaId()
            {
                return $this->devedorPessoaId;
            }

public function getDevedor_pessoa_id_INT()
                {
                    return $this->devedorPessoaId;
                }

public function getCadastroUsuarioId()
            {
                return $this->cadastroUsuarioId;
            }

public function getCadastro_usuario_id_INT()
                {
                    return $this->cadastroUsuarioId;
                }

public function getCadastroSec()
            {
                return $this->cadastroSec;
            }

public function getCadastro_SEC()
                {
                    return $this->cadastroSec;
                }

public function getCadastroOffsec()
            {
                return $this->cadastroOffsec;
            }

public function getCadastro_OFFSEC()
                {
                    return $this->cadastroOffsec;
                }

public function getValorPagamentoFloat()
            {
                return $this->valorPagamentoFloat;
            }

public function getValor_pagamento_FLOAT()
                {
                    return $this->valorPagamentoFloat;
                }

public function getPagamentoSec()
            {
                return $this->pagamentoSec;
            }

public function getPagamento_SEC()
                {
                    return $this->pagamentoSec;
                }

public function getPagamentoOffsec()
            {
                return $this->pagamentoOffsec;
            }

public function getPagamento_OFFSEC()
                {
                    return $this->pagamentoOffsec;
                }

public function getRecebedorUsuarioId()
            {
                return $this->recebedorUsuarioId;
            }

public function getRecebedor_usuario_id_INT()
                {
                    return $this->recebedorUsuarioId;
                }

public function getRelatorioId()
            {
                return $this->relatorioId;
            }

public function getRelatorio_id_INT()
                {
                    return $this->relatorioId;
                }

public function getProtocoloInt()
            {
                return $this->protocoloInt;
            }

public function getProtocolo_INT()
                {
                    return $this->protocoloInt;
                }

public function getCorporacaoId()
            {
                return $this->corporacaoId;
            }

public function getCorporacao_id_INT()
                {
                    return $this->corporacaoId;
                }



function setId($value)
            {
                $this->id = $value;
            }

function setValorFloat($value)
            {
                $this->valorFloat = $value;
            }

public function setValor_FLOAT($value)
                { 
                    $this->valorFloat = $value; 
                }

function setDevedorEmpresaId($value)
            {
                $this->devedorEmpresaId = $value;
            }

public function setDevedor_empresa_id_INT($value)
                { 
                    $this->devedorEmpresaId = $value; 
                }

function setDevedorPessoaId($value)
            {
                $this->devedorPessoaId = $value;
            }

public function setDevedor_pessoa_id_INT($value)
                { 
                    $this->devedorPessoaId = $value; 
                }

function setCadastroUsuarioId($value)
            {
                $this->cadastroUsuarioId = $value;
            }

public function setCadastro_usuario_id_INT($value)
                { 
                    $this->cadastroUsuarioId = $value; 
                }

function setCadastroSec($value)
            {
                $this->cadastroSec = $value;
            }

public function setCadastro_SEC($value)
                { 
                    $this->cadastroSec = $value; 
                }

function setCadastroOffsec($value)
            {
                $this->cadastroOffsec = $value;
            }

public function setCadastro_OFFSEC($value)
                { 
                    $this->cadastroOffsec = $value; 
                }

function setValorPagamentoFloat($value)
            {
                $this->valorPagamentoFloat = $value;
            }

public function setValor_pagamento_FLOAT($value)
                { 
                    $this->valorPagamentoFloat = $value; 
                }

function setPagamentoSec($value)
            {
                $this->pagamentoSec = $value;
            }

public function setPagamento_SEC($value)
                { 
                    $this->pagamentoSec = $value; 
                }

function setPagamentoOffsec($value)
            {
                $this->pagamentoOffsec = $value;
            }

public function setPagamento_OFFSEC($value)
                { 
                    $this->pagamentoOffsec = $value; 
                }

function setRecebedorUsuarioId($value)
            {
                $this->recebedorUsuarioId = $value;
            }

public function setRecebedor_usuario_id_INT($value)
                { 
                    $this->recebedorUsuarioId = $value; 
                }

function setRelatorioId($value)
            {
                $this->relatorioId = $value;
            }

public function setRelatorio_id_INT($value)
                { 
                    $this->relatorioId = $value; 
                }

function setProtocoloInt($value)
            {
                $this->protocoloInt = $value;
            }

public function setProtocolo_INT($value)
                { 
                    $this->protocoloInt = $value; 
                }

function setCorporacaoId($value)
            {
                $this->corporacaoId = $value;
            }

public function setCorporacao_id_INT($value)
                { 
                    $this->corporacaoId = $value; 
                }



public function clear()
        {$this->id = null;
$this->valorFloat = null;
$this->devedorEmpresaId = null;
if($this->objDevedor_empresa != null) unset($this->objDevedor_empresa);
$this->devedorPessoaId = null;
if($this->objDevedor_pessoa != null) unset($this->objDevedor_pessoa);
$this->cadastroUsuarioId = null;
if($this->objCadastro_usuario != null) unset($this->objCadastro_usuario);
$this->cadastroSec = null;
$this->cadastroOffsec = null;
$this->valorPagamentoFloat = null;
$this->pagamentoSec = null;
$this->pagamentoOffsec = null;
$this->recebedorUsuarioId = null;
if($this->objRecebedor_usuario != null) unset($this->objRecebedor_usuario);
$this->relatorioId = null;
if($this->objRelatorio != null) unset($this->objRelatorio);
$this->protocoloInt = null;
$this->corporacaoId = null;
if($this->objCorporacao != null) unset($this->objCorporacao);
if(is_array($this->arrEstadosCampos)){
                        unset($this->arrEstadosCampos);
                     $this->arrEstadosCampos = array();
                    }
                }

public function formatarParaSQL()
        {
if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->id)){
$this->id = $this->formatarIntegerParaComandoSQL($this->id);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->id);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->valorFloat)){
$this->valorFloat = $this->formatarFloatParaComandoSQL($this->valorFloat);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->valorFloat);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->devedorEmpresaId)){
$this->devedorEmpresaId = $this->formatarIntegerParaComandoSQL($this->devedorEmpresaId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->devedorEmpresaId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->devedorPessoaId)){
$this->devedorPessoaId = $this->formatarIntegerParaComandoSQL($this->devedorPessoaId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->devedorPessoaId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->cadastroUsuarioId)){
$this->cadastroUsuarioId = $this->formatarIntegerParaComandoSQL($this->cadastroUsuarioId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->cadastroUsuarioId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->cadastroSec)){
$this->cadastroSec = $this->formatarIntegerParaComandoSQL($this->cadastroSec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->cadastroSec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->cadastroOffsec)){
$this->cadastroOffsec = $this->formatarIntegerParaComandoSQL($this->cadastroOffsec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->cadastroOffsec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->valorPagamentoFloat)){
$this->valorPagamentoFloat = $this->formatarFloatParaComandoSQL($this->valorPagamentoFloat);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->valorPagamentoFloat);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->pagamentoSec)){
$this->pagamentoSec = $this->formatarIntegerParaComandoSQL($this->pagamentoSec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->pagamentoSec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->pagamentoOffsec)){
$this->pagamentoOffsec = $this->formatarIntegerParaComandoSQL($this->pagamentoOffsec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->pagamentoOffsec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->recebedorUsuarioId)){
$this->recebedorUsuarioId = $this->formatarIntegerParaComandoSQL($this->recebedorUsuarioId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->recebedorUsuarioId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->relatorioId)){
$this->relatorioId = $this->formatarIntegerParaComandoSQL($this->relatorioId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->relatorioId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->protocoloInt)){
$this->protocoloInt = $this->formatarIntegerParaComandoSQL($this->protocoloInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->protocoloInt);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->corporacaoId)){
$this->corporacaoId = $this->formatarIntegerParaComandoSQL($this->corporacaoId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->corporacaoId);
}

}

public function formatarParaExibicao()
        {if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->valorFloat)){
$this->valorFloat = $this->formatarFloatParaExibicao($this->valorFloat);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->valorFloat);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->valorPagamentoFloat)){
$this->valorPagamentoFloat = $this->formatarFloatParaExibicao($this->valorPagamentoFloat);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->valorPagamentoFloat);
}

}

function select($id, $idCorporacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }   

            $sql = "SELECT id, valor_FLOAT, devedor_empresa_id_INT, devedor_pessoa_id_INT, cadastro_usuario_id_INT, cadastro_SEC, cadastro_OFFSEC, valor_pagamento_FLOAT, pagamento_SEC, pagamento_OFFSEC, recebedor_usuario_id_INT, relatorio_id_INT, protocolo_INT, corporacao_id_INT FROM recebivel WHERE id = $id AND corporacao_id_INT = $idCorporacao";

            $msg = $this->database->queryMensagem($sql);
            $result = $this->database->result;

            if($msg != null && $msg->erro() )
            { 
            		$this->clear();
            		return $msg;
        	} 
        	else if($this->database->rows() == 0)
        	{
                return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nenhum registro foi encontrado."));
            }
        

            $row = $this->database->fetchArray(MYSQLI_NUM);
            
            		$this->id = $row[0];
		$this->valorFloat = $row[1];
		$this->devedorEmpresaId = $row[2];
		$this->devedorPessoaId = $row[3];
		$this->cadastroUsuarioId = $row[4];
		$this->cadastroSec = $row[5];
		$this->cadastroOffsec = $row[6];
		$this->valorPagamentoFloat = $row[7];
		$this->pagamentoSec = $row[8];
		$this->pagamentoOffsec = $row[9];
		$this->recebedorUsuarioId = $row[10];
		$this->relatorioId = $row[11];
		$this->protocoloInt = $row[12];

            
        }

public function delete($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (is_null($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }

            $sql = "DELETE FROM recebivel WHERE id= $id AND corporacao_id_INT = $idCorporacao";
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                    && $sincronizar
                        && static::idSistemaTabela() != null
                            && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_REMOVE,
                                null,
                                null,
                                $id,
                                $this->database);
            }

            return $msg;
        }

public function insert($sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null, $opcoes = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $this->defineDataCadastroInSecondsIfNotDefined();
$this->defineDataCadastroOffsetInSecondsIfNotDefined();

            
            if (is_null($opcoes))
            {
                $opcoes = $this->opcoesDAO;
            }

            if (!is_numeric($this->id))
            {
                $this->id = "null";
                if ($opcoes == null || $opcoes[Database::OPCAO_GERAR_ID] != false) 
                {
                    $this->id = EXTDAO_Sistema_sequencia::gerarId(static::nomeTabela());                    
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }    
                    else if($msg == null)
                    {
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
                else 
                {
                    $this->id = null;
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao));
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }
                    else if($msg == null)
                    {
                        $this->id = $this->database->getLastInsertId();
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
            }
            else 
            {
                $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }                        
                else if($msg == null)
                {
                   $msg = new Mensagem_token();
                   $msg->mValor = $this->id;
                }
            }
            if($msg == null || $msg->ok())
            {                
                if($sincronizar
                    && static::idSistemaTabela() != null
                    && isset($this->id)){
                    
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_INSERT,
                                null,
                                null,
                                $this->id,
                                $this->database); 
                	}
            } 
            else
            {
                $this->id = null;
            }
            
            return $msg;
            
        }

public function getSQLInsert($idCorporacao){ 

return "INSERT INTO recebivel (id, valor_FLOAT, devedor_empresa_id_INT, devedor_pessoa_id_INT, cadastro_usuario_id_INT, cadastro_SEC, cadastro_OFFSEC, valor_pagamento_FLOAT, pagamento_SEC, pagamento_OFFSEC, recebedor_usuario_id_INT, relatorio_id_INT, protocolo_INT, corporacao_id_INT) VALUES ( $this->id ,  $this->valorFloat ,  $this->devedorEmpresaId ,  $this->devedorPessoaId ,  $this->cadastroUsuarioId ,  $this->cadastroSec ,  $this->cadastroOffsec ,  $this->valorPagamentoFloat ,  $this->pagamentoSec ,  $this->pagamentoOffsec ,  $this->recebedorUsuarioId ,  $this->relatorioId ,  $this->protocoloInt ,  $idCorporacao ) "; 

}




        
        public function updateByParameters($id, $objParametros, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao))
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();

            
                
                if (isset($objParametros->valorFloat)) 
                {
                    $arrUpdateFields[] = " valor_FLOAT = {$objParametros->valorFloat} ";
                }


                
                if (isset($objParametros->devedorEmpresaId)) 
                {
                    $arrUpdateFields[] = " devedor_empresa_id_INT = {$objParametros->devedorEmpresaId} ";
                }


                
                if (isset($objParametros->devedorPessoaId)) 
                {
                    $arrUpdateFields[] = " devedor_pessoa_id_INT = {$objParametros->devedorPessoaId} ";
                }


                
                if (isset($objParametros->cadastroUsuarioId)) 
                {
                    $arrUpdateFields[] = " cadastro_usuario_id_INT = {$objParametros->cadastroUsuarioId} ";
                }


                
                if (isset($objParametros->cadastroSec)) 
                {
                    $arrUpdateFields[] = " cadastro_SEC = {$objParametros->cadastroSec} ";
                }


                
                if (isset($objParametros->cadastroOffsec)) 
                {
                    $arrUpdateFields[] = " cadastro_OFFSEC = {$objParametros->cadastroOffsec} ";
                }


                
                if (isset($objParametros->valorPagamentoFloat)) 
                {
                    $arrUpdateFields[] = " valor_pagamento_FLOAT = {$objParametros->valorPagamentoFloat} ";
                }


                
                if (isset($objParametros->pagamentoSec)) 
                {
                    $arrUpdateFields[] = " pagamento_SEC = {$objParametros->pagamentoSec} ";
                }


                
                if (isset($objParametros->pagamentoOffsec)) 
                {
                    $arrUpdateFields[] = " pagamento_OFFSEC = {$objParametros->pagamentoOffsec} ";
                }


                
                if (isset($objParametros->recebedorUsuarioId)) 
                {
                    $arrUpdateFields[] = " recebedor_usuario_id_INT = {$objParametros->recebedorUsuarioId} ";
                }


                
                if (isset($objParametros->relatorioId)) 
                {
                    $arrUpdateFields[] = " relatorio_id_INT = {$objParametros->relatorioId} ";
                }


                
                if (isset($objParametros->protocoloInt)) 
                {
                    $arrUpdateFields[] = " protocolo_INT = {$objParametros->protocoloInt} ";
                }



            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE recebivel SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";

            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null)
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function update($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($this->valorFloat)) 
                {                                      
                    $arrUpdateFields[] = " valor_FLOAT = {$this->valorFloat} ";
                }


                
                if (isset($this->devedorEmpresaId)) 
                {                                      
                    $arrUpdateFields[] = " devedor_empresa_id_INT = {$this->devedorEmpresaId} ";
                }


                
                if (isset($this->devedorPessoaId)) 
                {                                      
                    $arrUpdateFields[] = " devedor_pessoa_id_INT = {$this->devedorPessoaId} ";
                }


                
                if (isset($this->cadastroUsuarioId)) 
                {                                      
                    $arrUpdateFields[] = " cadastro_usuario_id_INT = {$this->cadastroUsuarioId} ";
                }


                
                if (isset($this->cadastroSec)) 
                {                                      
                    $arrUpdateFields[] = " cadastro_SEC = {$this->cadastroSec} ";
                }


                
                if (isset($this->cadastroOffsec)) 
                {                                      
                    $arrUpdateFields[] = " cadastro_OFFSEC = {$this->cadastroOffsec} ";
                }


                
                if (isset($this->valorPagamentoFloat)) 
                {                                      
                    $arrUpdateFields[] = " valor_pagamento_FLOAT = {$this->valorPagamentoFloat} ";
                }


                
                if (isset($this->pagamentoSec)) 
                {                                      
                    $arrUpdateFields[] = " pagamento_SEC = {$this->pagamentoSec} ";
                }


                
                if (isset($this->pagamentoOffsec)) 
                {                                      
                    $arrUpdateFields[] = " pagamento_OFFSEC = {$this->pagamentoOffsec} ";
                }


                
                if (isset($this->recebedorUsuarioId)) 
                {                                      
                    $arrUpdateFields[] = " recebedor_usuario_id_INT = {$this->recebedorUsuarioId} ";
                }


                
                if (isset($this->relatorioId)) 
                {                                      
                    $arrUpdateFields[] = " relatorio_id_INT = {$this->relatorioId} ";
                }


                
                if (isset($this->protocoloInt)) 
                {                                      
                    $arrUpdateFields[] = " protocolo_INT = {$this->protocoloInt} ";
                }


            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE recebivel SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function updateByInstanceUsingParameters($id, $parameters, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($parameters->valorFloat)) 
                {                                      
                    $arrUpdateFields[] = " valor_FLOAT = {$this->valorFloat} ";
                }
                
                if (isset($parameters->devedorEmpresaId)) 
                {                                      
                    $arrUpdateFields[] = " devedor_empresa_id_INT = {$this->devedorEmpresaId} ";
                }
                
                if (isset($parameters->devedorPessoaId)) 
                {                                      
                    $arrUpdateFields[] = " devedor_pessoa_id_INT = {$this->devedorPessoaId} ";
                }
                
                if (isset($parameters->cadastroUsuarioId)) 
                {                                      
                    $arrUpdateFields[] = " cadastro_usuario_id_INT = {$this->cadastroUsuarioId} ";
                }
                
                if (isset($parameters->cadastroSec)) 
                {                                      
                    $arrUpdateFields[] = " cadastro_SEC = {$this->cadastroSec} ";
                }
                
                if (isset($parameters->cadastroOffsec)) 
                {                                      
                    $arrUpdateFields[] = " cadastro_OFFSEC = {$this->cadastroOffsec} ";
                }
                
                if (isset($parameters->valorPagamentoFloat)) 
                {                                      
                    $arrUpdateFields[] = " valor_pagamento_FLOAT = {$this->valorPagamentoFloat} ";
                }
                
                if (isset($parameters->pagamentoSec)) 
                {                                      
                    $arrUpdateFields[] = " pagamento_SEC = {$this->pagamentoSec} ";
                }
                
                if (isset($parameters->pagamentoOffsec)) 
                {                                      
                    $arrUpdateFields[] = " pagamento_OFFSEC = {$this->pagamentoOffsec} ";
                }
                
                if (isset($parameters->recebedorUsuarioId)) 
                {                                      
                    $arrUpdateFields[] = " recebedor_usuario_id_INT = {$this->recebedorUsuarioId} ";
                }
                
                if (isset($parameters->relatorioId)) 
                {                                      
                    $arrUpdateFields[] = " relatorio_id_INT = {$this->relatorioId} ";
                }
                
                if (isset($parameters->protocoloInt)) 
                {                                      
                    $arrUpdateFields[] = " protocolo_INT = {$this->protocoloInt} ";
                }
            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE recebivel SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }

}

?>
