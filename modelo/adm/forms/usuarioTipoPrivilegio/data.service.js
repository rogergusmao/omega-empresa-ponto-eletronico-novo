

        /*

        Arquivo gerado atrav�s de gerador de c�digo em 07/09/2017 as 01:32:16.
        Para que o arquivo n�o seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

        Tabela correspondente: usuario_tipo_privilegio
        Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

        */

returnObject.getUsuarioTipoPrivilegio = function(parameters){

            return $http({
                url: baseURLAdm + "/webservice.php?class=EXTDAO_Usuario_tipo_privilegio&action=__getRecord",
                method: "POST",
                data: parameters,
                headers: {
                    'Content-Type': 'application/json'
                }
            });

        };



        returnObject.addUsuarioTipoPrivilegio = function(formData){

            return $http({
                url: baseURLAdm + "/webservice.php?class=EXTDAO_Usuario_tipo_privilegio&jsonOutput=true&action=add",
                method: "POST",
                data: formData,
                headers: {
                    'Content-Type':'application/x-www-form-urlencoded'
                }
            });

        };

        returnObject.editUsuarioTipoPrivilegio = function(formData){

            return $http({
                url: baseURLAdm + "/webservice.php?class=EXTDAO_Usuario_tipo_privilegio&jsonOutput=true&action=edit",
                method: "POST",
                data: formData,
                headers: {
                    'Content-Type':'application/x-www-form-urlencoded'
                }
            });

        };

returnObject.getListOfUsuarioTipo = function()
                {
                    var params = { filterParameters: null, isRelatedEntity: true };
                    return $http.post(baseURLAdm + "/webservice.php?class=EXTDAO_Usuario_tipo&action=__getList", params);
                };
                

