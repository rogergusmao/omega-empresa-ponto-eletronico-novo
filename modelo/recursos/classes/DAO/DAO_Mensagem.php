<?php


        /*

        Arquivo gerado através de gerador de código em 12/05/2018 as 14:34:57.
        Para que o arquivo não seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

        Tabela correspondente: mensagem
        Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

        */
        


class DAO_Mensagem extends Generic_DAO
                        {
                            const NOME_ENTIDADE = "mensagem";

                            public static $nomeTabela = null;
                            public static $idSistemaTabela = false;
                            public static $databaseFieldsRelatedAttributes = null;
	                        public static $databaseFieldTypes = null;
	                        public static $databaseFieldNames = null;
	                        public static $listAliasTypes = null;
	                        public static $listAliasRelatedAttributes = null;
	                        
	                        public $id;
public $titulo;
public $mensagem;
public $remetenteUsuarioId;
public $tipoMensagemId;
public $destinatarioUsuarioId;
public $destinatarioPessoaId;
public $destinatarioEmpresaId;
public $dataMinEnvioSec;
public $dataMinEnvioOffsec;
public $protocoloInt;
public $registroEstadoId;
public $empresaParaClienteBoolean;
public $corporacaoId;

public $labelId;
public $labelTitulo;
public $labelMensagem;
public $labelRemetenteUsuarioId;
public $labelTipoMensagemId;
public $labelDestinatarioUsuarioId;
public $labelDestinatarioPessoaId;
public $labelDestinatarioEmpresaId;
public $labelDataMinEnvioSec;
public $labelDataMinEnvioOffsec;
public $labelProtocoloInt;
public $labelRegistroEstadoId;
public $labelEmpresaParaClienteBoolean;
public $labelCorporacaoId;

public static $arrPK = array("id");

public function __construct($configDAO = null)
        {
            parent::__construct($configDAO);
            static::__setStaticValues($this->database);           
        }

public static function idSistemaTabela($databaseObject = null){
            if(!(static::$idSistemaTabela))
            {
                static::$idSistemaTabela = EXTDAO_Sistema_tabela::getIdDaSistemaTabela(static::nomeTabela(), $databaseObject);
            }
            return static::$idSistemaTabela;
        }

public function getIdSistemaTabela(){
            return static::idSistemaTabela();
        }

public static function nomeTabela(){
            if(is_null(static::$nomeTabela))
            {
                static::$nomeTabela = "mensagem";
            }
            return static::$nomeTabela;
        }

public function getNomeTabela(){
            return static::nomeTabela();
        }

public static function __setStaticValues($databaseObject = null)
                   {    
                        static::nomeTabela();
                        
                        static::idSistemaTabela($databaseObject);
                   
                        static::setDatabaseFieldNames();
                        static::setDatabaseFieldTypes();
                        static::setDatabaseFieldsRelatedAttributeNames();
                        
                        static::setListAliasTypes();
                        static::setListAliasRelatedAttributes();
                   }

public function getExpressaoLabel()
                   {
                        return "{id}";
                   }

public static function setDatabaseFieldNames()
        {
            if(is_null(static::$databaseFieldNames))
            {
                static::$databaseFieldNames = new stdClass();
                static::$databaseFieldNames->id = "id";
static::$databaseFieldNames->titulo = "titulo";
static::$databaseFieldNames->mensagem = "mensagem";
static::$databaseFieldNames->remetenteUsuarioId = "remetente_usuario_id_INT";
static::$databaseFieldNames->tipoMensagemId = "tipo_mensagem_id_INT";
static::$databaseFieldNames->destinatarioUsuarioId = "destinatario_usuario_id_INT";
static::$databaseFieldNames->destinatarioPessoaId = "destinatario_pessoa_id_INT";
static::$databaseFieldNames->destinatarioEmpresaId = "destinatario_empresa_id_INT";
static::$databaseFieldNames->dataMinEnvioSec = "data_min_envio_SEC";
static::$databaseFieldNames->dataMinEnvioOffsec = "data_min_envio_OFFSEC";
static::$databaseFieldNames->protocoloInt = "protocolo_INT";
static::$databaseFieldNames->registroEstadoId = "registro_estado_id_INT";
static::$databaseFieldNames->empresaParaClienteBoolean = "empresa_para_cliente_BOOLEAN";
static::$databaseFieldNames->corporacaoId = "corporacao_id_INT";

            }
                      
        }

public static function setDatabaseFieldTypes()
        {
            if(is_null(static::$databaseFieldTypes))
            {
                static::$databaseFieldTypes = new stdClass();
                static::$databaseFieldTypes->id = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->titulo = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->mensagem = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->remetenteUsuarioId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->tipoMensagemId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->destinatarioUsuarioId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->destinatarioPessoaId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->destinatarioEmpresaId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->dataMinEnvioSec = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$databaseFieldTypes->dataMinEnvioOffsec = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$databaseFieldTypes->protocoloInt = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->registroEstadoId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->empresaParaClienteBoolean = static::TIPO_VARIAVEL_BOOLEAN;
static::$databaseFieldTypes->corporacaoId = static::TIPO_VARIAVEL_INTEGER;

            }
            
        }

public static function setDatabaseFieldsRelatedAttributeNames()
        {
            if(is_null(static::$databaseFieldsRelatedAttributes))
            {
                static::$databaseFieldsRelatedAttributes = new stdClass();
                static::$databaseFieldsRelatedAttributes->id = "id";
static::$databaseFieldsRelatedAttributes->titulo = "titulo";
static::$databaseFieldsRelatedAttributes->mensagem = "mensagem";
static::$databaseFieldsRelatedAttributes->remetente_usuario_id_INT = "remetenteUsuarioId";
static::$databaseFieldsRelatedAttributes->tipo_mensagem_id_INT = "tipoMensagemId";
static::$databaseFieldsRelatedAttributes->destinatario_usuario_id_INT = "destinatarioUsuarioId";
static::$databaseFieldsRelatedAttributes->destinatario_pessoa_id_INT = "destinatarioPessoaId";
static::$databaseFieldsRelatedAttributes->destinatario_empresa_id_INT = "destinatarioEmpresaId";
static::$databaseFieldsRelatedAttributes->data_min_envio_SEC = "dataMinEnvioSec";
static::$databaseFieldsRelatedAttributes->data_min_envio_OFFSEC = "dataMinEnvioOffsec";
static::$databaseFieldsRelatedAttributes->protocolo_INT = "protocoloInt";
static::$databaseFieldsRelatedAttributes->registro_estado_id_INT = "registroEstadoId";
static::$databaseFieldsRelatedAttributes->empresa_para_cliente_BOOLEAN = "empresaParaClienteBoolean";
static::$databaseFieldsRelatedAttributes->corporacao_id_INT = "corporacaoId";

            }           
        }

public function setAllFieldsToHumanFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["titulo"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["mensagem"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["remetente_usuario_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["tipo_mensagem_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["destinatario_usuario_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["destinatario_pessoa_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["destinatario_empresa_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["data_min_envio_SEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["data_min_envio_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["protocolo_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["registro_estado_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["empresa_para_cliente_BOOLEAN"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
}

public function setAllFieldsToDatabaseFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["titulo"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["mensagem"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["remetente_usuario_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["tipo_mensagem_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["destinatario_usuario_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["destinatario_pessoa_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["destinatario_empresa_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["data_min_envio_SEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["data_min_envio_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["protocolo_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["registro_estado_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["empresa_para_cliente_BOOLEAN"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
}

  public function getFkObjRemetente_usuario() 
                {
                    if($this->objRemetente_usuario == null)
                    {                        
                        $this->objRemetente_usuario = new EXTDAO_Remetente_usuario_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getRemetente_usuario_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objRemetente_usuario->clear();
                    }
                    elseif($this->objRemetente_usuario->getId() != $idFK)
                    {
                        $this->objRemetente_usuario->select($idFK);
                    }
                    return $this->objRemetente_usuario;
                }
  public function getFkObjTipo_mensagem() 
                {
                    if($this->objTipo_mensagem == null)
                    {                        
                        $this->objTipo_mensagem = new EXTDAO_Tipo_mensagem_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getTipo_mensagem_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objTipo_mensagem->clear();
                    }
                    elseif($this->objTipo_mensagem->getId() != $idFK)
                    {
                        $this->objTipo_mensagem->select($idFK);
                    }
                    return $this->objTipo_mensagem;
                }
  public function getFkObjDestinatario_usuario() 
                {
                    if($this->objDestinatario_usuario == null)
                    {                        
                        $this->objDestinatario_usuario = new EXTDAO_Destinatario_usuario_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getDestinatario_usuario_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objDestinatario_usuario->clear();
                    }
                    elseif($this->objDestinatario_usuario->getId() != $idFK)
                    {
                        $this->objDestinatario_usuario->select($idFK);
                    }
                    return $this->objDestinatario_usuario;
                }
  public function getFkObjDestinatario_pessoa() 
                {
                    if($this->objDestinatario_pessoa == null)
                    {                        
                        $this->objDestinatario_pessoa = new EXTDAO_Destinatario_pessoa_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getDestinatario_pessoa_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objDestinatario_pessoa->clear();
                    }
                    elseif($this->objDestinatario_pessoa->getId() != $idFK)
                    {
                        $this->objDestinatario_pessoa->select($idFK);
                    }
                    return $this->objDestinatario_pessoa;
                }
  public function getFkObjDestinatario_empresa() 
                {
                    if($this->objDestinatario_empresa == null)
                    {                        
                        $this->objDestinatario_empresa = new EXTDAO_Destinatario_empresa_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getDestinatario_empresa_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objDestinatario_empresa->clear();
                    }
                    elseif($this->objDestinatario_empresa->getId() != $idFK)
                    {
                        $this->objDestinatario_empresa->select($idFK);
                    }
                    return $this->objDestinatario_empresa;
                }
  public function getFkObjRegistro_estado() 
                {
                    if($this->objRegistro_estado == null)
                    {                        
                        $this->objRegistro_estado = new EXTDAO_Registro_estado_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getRegistro_estado_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objRegistro_estado->clear();
                    }
                    elseif($this->objRegistro_estado->getId() != $idFK)
                    {
                        $this->objRegistro_estado->select($idFK);
                    }
                    return $this->objRegistro_estado;
                }
  public function getFkObjCorporacao() 
                {
                    if($this->objCorporacao == null)
                    {                        
                        $this->objCorporacao = new EXTDAO_Corporacao_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getCorporacao_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objCorporacao->clear();
                    }
                    elseif($this->objCorporacao->getId() != $idFK)
                    {
                        $this->objCorporacao->select($idFK);
                    }
                    return $this->objCorporacao;
                }


public function setLabels()
        {$this->labelId = "";
$this->labelTitulo = "";
$this->labelMensagem = "";
$this->labelRemetenteUsuarioId = "";
$this->labelTipoMensagemId = "";
$this->labelDestinatarioUsuarioId = "";
$this->labelDestinatarioPessoaId = "";
$this->labelDestinatarioEmpresaId = "";
$this->labelDataMinEnvioSec = "";
$this->labelDataMinEnvioOffsec = "";
$this->labelProtocoloInt = "";
$this->labelRegistroEstadoId = "";
$this->labelEmpresaParaClienteBoolean = "";
$this->labelCorporacaoId = "";
}

public function __actionAdd($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                $this->setByObject($parameters);
                $this->setDataCadastroAndOffsetInSecondsFromUser($parameters);
                
                $this->formatarParaSQL();
                $msg = $this->insert(true);
                
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                return new Mensagem(null, I18N::getExpression("Mensagem adicionado com sucesso."));
                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionEdit($parameters = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try {

                $this->setByObject($parameters);
                $this->setAllFieldsToHumanFormat();
                
                $this->formatarParaSQL();
                $msg = $this->updateByInstanceUsingParameters($this->id, $parameters, true);
                if($msg != null && $msg->erro())
                {
                     return $msg;
                }
        
                return new Mensagem(null, I18N::getExpression("Mensagem editado com sucesso."));
            }
            catch(Exception $ex)
            {                
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionRemove($parameters = null)
        {
            $successulRemovalCount = 0;
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                foreach($parameters as $record)
                {
                    if(is_numeric($record->id))
                    {
                        $msg = $this->delete($record->id, true);
                        if($msg == null || $msg->ok())
                        {
                            $successulRemovalCount++;
                        }
                    }
                }
                
                if($successulRemovalCount > 0)
                {
                    $successMessage = count($successulRemovalCount) > 1 ? I18N::getExpression("{0} Mensagem foram removidos com sucesso.", $successulRemovalCount) : I18N::getExpression("Mensagem removido com sucesso.");
                    return new Mensagem(null, $successMessage);
                }
                else
                {
                    $errorMessage = count($parameters) > 1 ? I18N::getExpression("Falha ao remover Mensagem.") : I18N::getExpression("Falha ao remover Mensagem.");
                    return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, $errorMessage);                    
                }
            }
            catch (Exception $ex) 
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }

public function __getRecord($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }
            
            try
            {            
                $returnObject = new stdClass();            
                $whereClause = static::getWhereClauseForFilter($parameters->filterParameters);
                $query = "SELECT  id, titulo, mensagem, remetente_usuario_id_INT, tipo_mensagem_id_INT, destinatario_usuario_id_INT, destinatario_pessoa_id_INT, destinatario_empresa_id_INT, data_min_envio_SEC, data_min_envio_OFFSEC, protocolo_INT, registro_estado_id_INT, empresa_para_cliente_BOOLEAN, corporacao_id_INT FROM mensagem {$whereClause}";
    
                $msg = $this->database->queryMensagem($query);                
                if ($msg == null)
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Não foi encontrado nenhum registro"));
                    }                    
                    $resultSet = $this->database->result;
                    $resultObject = static::mapResultSetDataToRecord($resultSet);
                    
                    $returnObject->formData = $resultObject;
                    $returnObject->comboBoxesData = null;
                    if($parameters->loadComboBoxesData)
                    {
                        $this->setByObject($resultObject);
                        $returnObject->comboBoxesData =  $this->getFormComboBoxesData();
                    }                    
    
                    
    
                    return new Mensagem_generica($returnObject);
                } 
                else 
                {
                    return $msg;
                }
                
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }


        
        public function __getFilterComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
                
        public function __getFormComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
        
        protected function getFormComboBoxesData()
        {
            $comboBoxesData = new stdClass();
            $listParameters = new stdClass();
            $listParameters->listMappingType = static::LIST_MAPPING_TYPE_COMBOBOX;
            
            
                    
                    $objMensagem = new EXTDAO_Mensagem();
                    $comboBoxesData->fieldTipoMensagemId = $objMensagem->__getList($listParameters);
                    
                    $objRegistroEstado = new EXTDAO_Registro_estado();
                    $comboBoxesData->fieldRegistroEstadoId = $objRegistroEstado->__getList($listParameters);

            return $comboBoxesData;
        }
        
        


        
        public static function setListAliasTypes()
        {
            if(is_null(static::$listAliasTypes))
            {
                static::$listAliasTypes = new stdClass();
                static::$listAliasTypes->mensagem__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->registro_estado__nome = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->mensagem__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->mensagem__titulo = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->mensagem__mensagem = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->mensagem__remetente_usuario_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->mensagem__corporacao_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->mensagem__destinatario_usuario_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->mensagem__destinatario_pessoa_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->mensagem__destinatario_empresa_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->mensagem__data_min_envio_SEC = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$listAliasTypes->mensagem__protocolo_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->mensagem__tipo_registro_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->mensagem__empresa_para_cliente_BOOLEAN = static::TIPO_VARIAVEL_BOOLEAN;
            }        
        }
        
        public static function setListAliasRelatedAttributes()
        {
            if(is_null(static::$listAliasRelatedAttributes))
            {
                static::$listAliasRelatedAttributes = new stdClass();
                static::$listAliasRelatedAttributes->mensagem__id = "mensagemId";
static::$listAliasRelatedAttributes->registro_estado__nome = "registroEstadoNome";
static::$listAliasRelatedAttributes->mensagem__id = "id";
static::$listAliasRelatedAttributes->mensagem__titulo = "titulo";
static::$listAliasRelatedAttributes->mensagem__mensagem = "mensagem";
static::$listAliasRelatedAttributes->mensagem__remetente_usuario_id_INT = "remetenteUsuarioId";
static::$listAliasRelatedAttributes->mensagem__corporacao_id_INT = "corporacaoId";
static::$listAliasRelatedAttributes->mensagem__destinatario_usuario_id_INT = "destinatarioUsuarioId";
static::$listAliasRelatedAttributes->mensagem__destinatario_pessoa_id_INT = "destinatarioPessoaId";
static::$listAliasRelatedAttributes->mensagem__destinatario_empresa_id_INT = "destinatarioEmpresaId";
static::$listAliasRelatedAttributes->mensagem__data_min_envio_SEC = "dataMinEnvioSec";
static::$listAliasRelatedAttributes->mensagem__protocolo_INT = "protocoloInt";
static::$listAliasRelatedAttributes->mensagem__tipo_registro_id_INT = "tipoRegistroId";
static::$listAliasRelatedAttributes->mensagem__empresa_para_cliente_BOOLEAN = "empresaParaClienteBoolean";
            }         
        }

        public function __getList($parameters = null, $idCorporacao = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try
            {
                $filterParameters = new stdClass();
                if(isset($parameters->filterParameters))
                {
                    $filterParameters = $parameters->filterParameters;
                }
                                    
                            
            if(is_null($idCorporacao))
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            ListParameters::addFilterParameterToObject($filterParameters, "corporacaoId", $idCorporacao);           
                
                
                $sortingParameters = null;
                if(isset($parameters->sortingParameters))
                {
                    $sortingParameters = $parameters->sortingParameters;
                }
                
                $paginationParameters = null;
                if(isset($parameters->paginationParameters))
                {
                    $paginationParameters = $parameters->paginationParameters;
                }
                
                $listMappingType = static::getListMappingType($parameters->listMappingType);
                $mainTableAlias = "m";

                $whereClause = static::getWhereClauseForFilter($filterParameters, $mainTableAlias);
                $orderByClause = static::getOrderByClauseForFilter($sortingParameters, $mainTableAlias);
                $limitClause = static::getLimitClauseForFilter($paginationParameters);

                $queryWithoutLimit = "SELECT m.id FROM mensagem m {$whereClause}";
                $query = "SELECT m1.id AS mensagem__id, re.nome AS registro_estado__nome, m.id AS mensagem__id, m.titulo AS mensagem__titulo, m.mensagem AS mensagem__mensagem, m.remetente_usuario_id_INT AS mensagem__remetente_usuario_id_INT, m.corporacao_id_INT AS mensagem__corporacao_id_INT, m.destinatario_usuario_id_INT AS mensagem__destinatario_usuario_id_INT, m.destinatario_pessoa_id_INT AS mensagem__destinatario_pessoa_id_INT, m.destinatario_empresa_id_INT AS mensagem__destinatario_empresa_id_INT, m.data_min_envio_SEC AS mensagem__data_min_envio_SEC, m.protocolo_INT AS mensagem__protocolo_INT, m.tipo_registro_id_INT AS mensagem__tipo_registro_id_INT, m.empresa_para_cliente_BOOLEAN AS mensagem__empresa_para_cliente_BOOLEAN FROM mensagem m LEFT JOIN registro_estado re ON re.id = m.registro_estado_id_INT {$whereClause} {$orderByClause} {$limitClause}";

                $msg = $this->database->queryMensagem($queryWithoutLimit);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                
                $totalNumberOfRecords = $this->database->rows();
                if($totalNumberOfRecords == 0)
                {
                    return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                }                        
                        
                $msg = $this->database->queryMensagem($query);
                
                //operacao realizaca com sucesso
                if ($msg == null) 
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                    }                        
                
                    $resultSet = $this->database->result;

                    $objReturn = new stdClass();
                    $objReturn->dataSet = static::mapResultSetDataToList($resultSet, $listMappingType, static::$listAliasTypes);

                    if(!Helper::isNullOrEmpty($paginationParameters->recordsPerPage) && !Helper::isNullOrEmpty($paginationParameters->currentPage))
                    {
                        $lastPage = ceil($totalNumberOfRecords / $paginationParameters->recordsPerPage);
                        $objReturn->paginationParameters = new PaginationParams($paginationParameters->recordsPerPage, $paginationParameters->currentPage, $lastPage);
                    }
                    
                    return new Mensagem_generica($objReturn);
                } 
                else
                {
                    return $msg;
                }                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }



public function getId()
            {
                return $this->id;
            }

public function getTitulo()
            {
                return $this->titulo;
            }

public function getMensagem()
            {
                return $this->mensagem;
            }

public function getRemetenteUsuarioId()
            {
                return $this->remetenteUsuarioId;
            }

public function getRemetente_usuario_id_INT()
                {
                    return $this->remetenteUsuarioId;
                }

public function getTipoMensagemId()
            {
                return $this->tipoMensagemId;
            }

public function getTipo_mensagem_id_INT()
                {
                    return $this->tipoMensagemId;
                }

public function getDestinatarioUsuarioId()
            {
                return $this->destinatarioUsuarioId;
            }

public function getDestinatario_usuario_id_INT()
                {
                    return $this->destinatarioUsuarioId;
                }

public function getDestinatarioPessoaId()
            {
                return $this->destinatarioPessoaId;
            }

public function getDestinatario_pessoa_id_INT()
                {
                    return $this->destinatarioPessoaId;
                }

public function getDestinatarioEmpresaId()
            {
                return $this->destinatarioEmpresaId;
            }

public function getDestinatario_empresa_id_INT()
                {
                    return $this->destinatarioEmpresaId;
                }

public function getDataMinEnvioSec()
            {
                return $this->dataMinEnvioSec;
            }

public function getData_min_envio_SEC()
                {
                    return $this->dataMinEnvioSec;
                }

public function getDataMinEnvioOffsec()
            {
                return $this->dataMinEnvioOffsec;
            }

public function getData_min_envio_OFFSEC()
                {
                    return $this->dataMinEnvioOffsec;
                }

public function getProtocoloInt()
            {
                return $this->protocoloInt;
            }

public function getProtocolo_INT()
                {
                    return $this->protocoloInt;
                }

public function getRegistroEstadoId()
            {
                return $this->registroEstadoId;
            }

public function getRegistro_estado_id_INT()
                {
                    return $this->registroEstadoId;
                }

public function getEmpresaParaClienteBoolean()
            {
                return $this->empresaParaClienteBoolean;
            }

public function getEmpresa_para_cliente_BOOLEAN()
                {
                    return $this->empresaParaClienteBoolean;
                }

public function getCorporacaoId()
            {
                return $this->corporacaoId;
            }

public function getCorporacao_id_INT()
                {
                    return $this->corporacaoId;
                }



function setId($value)
            {
                $this->id = $value;
            }

function setTitulo($value)
            {
                $this->titulo = $value;
            }

function setMensagem($value)
            {
                $this->mensagem = $value;
            }

function setRemetenteUsuarioId($value)
            {
                $this->remetenteUsuarioId = $value;
            }

public function setRemetente_usuario_id_INT($value)
                { 
                    $this->remetenteUsuarioId = $value; 
                }

function setTipoMensagemId($value)
            {
                $this->tipoMensagemId = $value;
            }

public function setTipo_mensagem_id_INT($value)
                { 
                    $this->tipoMensagemId = $value; 
                }

function setDestinatarioUsuarioId($value)
            {
                $this->destinatarioUsuarioId = $value;
            }

public function setDestinatario_usuario_id_INT($value)
                { 
                    $this->destinatarioUsuarioId = $value; 
                }

function setDestinatarioPessoaId($value)
            {
                $this->destinatarioPessoaId = $value;
            }

public function setDestinatario_pessoa_id_INT($value)
                { 
                    $this->destinatarioPessoaId = $value; 
                }

function setDestinatarioEmpresaId($value)
            {
                $this->destinatarioEmpresaId = $value;
            }

public function setDestinatario_empresa_id_INT($value)
                { 
                    $this->destinatarioEmpresaId = $value; 
                }

function setDataMinEnvioSec($value)
            {
                $this->dataMinEnvioSec = $value;
            }

public function setData_min_envio_SEC($value)
                { 
                    $this->dataMinEnvioSec = $value; 
                }

function setDataMinEnvioOffsec($value)
            {
                $this->dataMinEnvioOffsec = $value;
            }

public function setData_min_envio_OFFSEC($value)
                { 
                    $this->dataMinEnvioOffsec = $value; 
                }

function setProtocoloInt($value)
            {
                $this->protocoloInt = $value;
            }

public function setProtocolo_INT($value)
                { 
                    $this->protocoloInt = $value; 
                }

function setRegistroEstadoId($value)
            {
                $this->registroEstadoId = $value;
            }

public function setRegistro_estado_id_INT($value)
                { 
                    $this->registroEstadoId = $value; 
                }

function setEmpresaParaClienteBoolean($value)
            {
                $this->empresaParaClienteBoolean = $value;
            }

public function setEmpresa_para_cliente_BOOLEAN($value)
                { 
                    $this->empresaParaClienteBoolean = $value; 
                }

function setCorporacaoId($value)
            {
                $this->corporacaoId = $value;
            }

public function setCorporacao_id_INT($value)
                { 
                    $this->corporacaoId = $value; 
                }



public function clear()
        {$this->id = null;
$this->titulo = null;
$this->mensagem = null;
$this->remetenteUsuarioId = null;
if($this->objRemetente_usuario != null) unset($this->objRemetente_usuario);
$this->tipoMensagemId = null;
if($this->objTipo_mensagem != null) unset($this->objTipo_mensagem);
$this->destinatarioUsuarioId = null;
if($this->objDestinatario_usuario != null) unset($this->objDestinatario_usuario);
$this->destinatarioPessoaId = null;
if($this->objDestinatario_pessoa != null) unset($this->objDestinatario_pessoa);
$this->destinatarioEmpresaId = null;
if($this->objDestinatario_empresa != null) unset($this->objDestinatario_empresa);
$this->dataMinEnvioSec = null;
$this->dataMinEnvioOffsec = null;
$this->protocoloInt = null;
$this->registroEstadoId = null;
if($this->objRegistro_estado != null) unset($this->objRegistro_estado);
$this->empresaParaClienteBoolean = null;
$this->corporacaoId = null;
if($this->objCorporacao != null) unset($this->objCorporacao);
if(is_array($this->arrEstadosCampos)){
                        unset($this->arrEstadosCampos);
                     $this->arrEstadosCampos = array();
                    }
                }

public function formatarParaSQL()
        {
if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->id)){
$this->id = $this->formatarIntegerParaComandoSQL($this->id);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->id);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->titulo)){
$this->titulo = $this->formatarStringParaComandoSQL($this->titulo);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->titulo);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->mensagem)){
$this->mensagem = $this->formatarStringParaComandoSQL($this->mensagem);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->mensagem);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->remetenteUsuarioId)){
$this->remetenteUsuarioId = $this->formatarIntegerParaComandoSQL($this->remetenteUsuarioId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->remetenteUsuarioId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->tipoMensagemId)){
$this->tipoMensagemId = $this->formatarIntegerParaComandoSQL($this->tipoMensagemId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->tipoMensagemId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->destinatarioUsuarioId)){
$this->destinatarioUsuarioId = $this->formatarIntegerParaComandoSQL($this->destinatarioUsuarioId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->destinatarioUsuarioId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->destinatarioPessoaId)){
$this->destinatarioPessoaId = $this->formatarIntegerParaComandoSQL($this->destinatarioPessoaId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->destinatarioPessoaId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->destinatarioEmpresaId)){
$this->destinatarioEmpresaId = $this->formatarIntegerParaComandoSQL($this->destinatarioEmpresaId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->destinatarioEmpresaId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->dataMinEnvioSec)){
$this->dataMinEnvioSec = $this->formatarIntegerParaComandoSQL($this->dataMinEnvioSec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->dataMinEnvioSec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->dataMinEnvioOffsec)){
$this->dataMinEnvioOffsec = $this->formatarIntegerParaComandoSQL($this->dataMinEnvioOffsec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->dataMinEnvioOffsec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->protocoloInt)){
$this->protocoloInt = $this->formatarIntegerParaComandoSQL($this->protocoloInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->protocoloInt);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->registroEstadoId)){
$this->registroEstadoId = $this->formatarIntegerParaComandoSQL($this->registroEstadoId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->registroEstadoId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->empresaParaClienteBoolean)){
$this->empresaParaClienteBoolean = $this->formatarBooleanParaComandoSQL($this->empresaParaClienteBoolean);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->empresaParaClienteBoolean);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->corporacaoId)){
$this->corporacaoId = $this->formatarIntegerParaComandoSQL($this->corporacaoId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->corporacaoId);
}

}

public function formatarParaExibicao()
        {if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->titulo)){
$this->titulo = $this->formatarStringParaExibicao($this->titulo);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->titulo);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->mensagem)){
$this->mensagem = $this->formatarStringParaExibicao($this->mensagem);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->mensagem);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->empresaParaClienteBoolean)){
$this->empresaParaClienteBoolean = $this->formatarBooleanParaExibicao($this->empresaParaClienteBoolean);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->empresaParaClienteBoolean);
}

}

function select($id, $idCorporacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }   

            $sql = "SELECT id, titulo, mensagem, remetente_usuario_id_INT, tipo_mensagem_id_INT, destinatario_usuario_id_INT, destinatario_pessoa_id_INT, destinatario_empresa_id_INT, data_min_envio_SEC, data_min_envio_OFFSEC, protocolo_INT, registro_estado_id_INT, empresa_para_cliente_BOOLEAN, corporacao_id_INT FROM mensagem WHERE id = $id AND corporacao_id_INT = $idCorporacao";

            $msg = $this->database->queryMensagem($sql);
            $result = $this->database->result;

            if($msg != null && $msg->erro() )
            { 
            		$this->clear();
            		return $msg;
        	} 
        	else if($this->database->rows() == 0)
        	{
                return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nenhum registro foi encontrado."));
            }
        

            $row = $this->database->fetchArray(MYSQLI_NUM);
            
            		$this->id = $row[0];
		$this->titulo = $row[1];
		$this->mensagem = $row[2];
		$this->remetenteUsuarioId = $row[3];
		$this->tipoMensagemId = $row[4];
		$this->destinatarioUsuarioId = $row[5];
		$this->destinatarioPessoaId = $row[6];
		$this->destinatarioEmpresaId = $row[7];
		$this->dataMinEnvioSec = $row[8];
		$this->dataMinEnvioOffsec = $row[9];
		$this->protocoloInt = $row[10];
		$this->registroEstadoId = $row[11];
		$this->empresaParaClienteBoolean = $row[12];

            
        }

public function delete($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (is_null($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }

            $sql = "DELETE FROM mensagem WHERE id= $id AND corporacao_id_INT = $idCorporacao";
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                    && $sincronizar
                        && static::idSistemaTabela() != null
                            && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_REMOVE,
                                null,
                                null,
                                $id,
                                $this->database);
            }

            return $msg;
        }

public function insert($sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null, $opcoes = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            
            
            if (is_null($opcoes))
            {
                $opcoes = $this->opcoesDAO;
            }

            if (!is_numeric($this->id))
            {
                $this->id = "null";
                if ($opcoes == null || $opcoes[Database::OPCAO_GERAR_ID] != false) 
                {
                    $this->id = EXTDAO_Sistema_sequencia::gerarId(static::nomeTabela());                    
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }    
                    else if($msg == null)
                    {
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
                else 
                {
                    $this->id = null;
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao));
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }
                    else if($msg == null)
                    {
                        $this->id = $this->database->getLastInsertId();
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
            }
            else 
            {
                $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }                        
                else if($msg == null)
                {
                   $msg = new Mensagem_token();
                   $msg->mValor = $this->id;
                }
            }
            if($msg == null || $msg->ok())
            {                
                if($sincronizar
                    && static::idSistemaTabela() != null
                    && isset($this->id)){
                    
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_INSERT,
                                null,
                                null,
                                $this->id,
                                $this->database); 
                	}
            } 
            else
            {
                $this->id = null;
            }
            
            return $msg;
            
        }

public function getSQLInsert($idCorporacao){ 

return "INSERT INTO mensagem (id, titulo, mensagem, remetente_usuario_id_INT, tipo_mensagem_id_INT, destinatario_usuario_id_INT, destinatario_pessoa_id_INT, destinatario_empresa_id_INT, data_min_envio_SEC, data_min_envio_OFFSEC, protocolo_INT, registro_estado_id_INT, empresa_para_cliente_BOOLEAN, corporacao_id_INT) VALUES ( $this->id ,  $this->titulo ,  $this->mensagem ,  $this->remetenteUsuarioId ,  $this->tipoMensagemId ,  $this->destinatarioUsuarioId ,  $this->destinatarioPessoaId ,  $this->destinatarioEmpresaId ,  $this->dataMinEnvioSec ,  $this->dataMinEnvioOffsec ,  $this->protocoloInt ,  $this->registroEstadoId ,  $this->empresaParaClienteBoolean ,  $idCorporacao ) "; 

}




        
        public function updateByParameters($id, $objParametros, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao))
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();

            
                
                if (isset($objParametros->titulo)) 
                {
                    $arrUpdateFields[] = " titulo = {$objParametros->titulo} ";
                }


                
                if (isset($objParametros->mensagem)) 
                {
                    $arrUpdateFields[] = " mensagem = {$objParametros->mensagem} ";
                }


                
                if (isset($objParametros->remetenteUsuarioId)) 
                {
                    $arrUpdateFields[] = " remetente_usuario_id_INT = {$objParametros->remetenteUsuarioId} ";
                }


                
                if (isset($objParametros->tipoMensagemId)) 
                {
                    $arrUpdateFields[] = " tipo_mensagem_id_INT = {$objParametros->tipoMensagemId} ";
                }


                
                if (isset($objParametros->destinatarioUsuarioId)) 
                {
                    $arrUpdateFields[] = " destinatario_usuario_id_INT = {$objParametros->destinatarioUsuarioId} ";
                }


                
                if (isset($objParametros->destinatarioPessoaId)) 
                {
                    $arrUpdateFields[] = " destinatario_pessoa_id_INT = {$objParametros->destinatarioPessoaId} ";
                }


                
                if (isset($objParametros->destinatarioEmpresaId)) 
                {
                    $arrUpdateFields[] = " destinatario_empresa_id_INT = {$objParametros->destinatarioEmpresaId} ";
                }


                
                if (isset($objParametros->dataMinEnvioSec)) 
                {
                    $arrUpdateFields[] = " data_min_envio_SEC = {$objParametros->dataMinEnvioSec} ";
                }


                
                if (isset($objParametros->dataMinEnvioOffsec)) 
                {
                    $arrUpdateFields[] = " data_min_envio_OFFSEC = {$objParametros->dataMinEnvioOffsec} ";
                }


                
                if (isset($objParametros->protocoloInt)) 
                {
                    $arrUpdateFields[] = " protocolo_INT = {$objParametros->protocoloInt} ";
                }


                
                if (isset($objParametros->registroEstadoId)) 
                {
                    $arrUpdateFields[] = " registro_estado_id_INT = {$objParametros->registroEstadoId} ";
                }


                
                if (isset($objParametros->empresaParaClienteBoolean)) 
                {
                    $arrUpdateFields[] = " empresa_para_cliente_BOOLEAN = {$objParametros->empresaParaClienteBoolean} ";
                }



            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE mensagem SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";

            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null)
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function update($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($this->titulo)) 
                {                                      
                    $arrUpdateFields[] = " titulo = {$this->titulo} ";
                }


                
                if (isset($this->mensagem)) 
                {                                      
                    $arrUpdateFields[] = " mensagem = {$this->mensagem} ";
                }


                
                if (isset($this->remetenteUsuarioId)) 
                {                                      
                    $arrUpdateFields[] = " remetente_usuario_id_INT = {$this->remetenteUsuarioId} ";
                }


                
                if (isset($this->tipoMensagemId)) 
                {                                      
                    $arrUpdateFields[] = " tipo_mensagem_id_INT = {$this->tipoMensagemId} ";
                }


                
                if (isset($this->destinatarioUsuarioId)) 
                {                                      
                    $arrUpdateFields[] = " destinatario_usuario_id_INT = {$this->destinatarioUsuarioId} ";
                }


                
                if (isset($this->destinatarioPessoaId)) 
                {                                      
                    $arrUpdateFields[] = " destinatario_pessoa_id_INT = {$this->destinatarioPessoaId} ";
                }


                
                if (isset($this->destinatarioEmpresaId)) 
                {                                      
                    $arrUpdateFields[] = " destinatario_empresa_id_INT = {$this->destinatarioEmpresaId} ";
                }


                
                if (isset($this->dataMinEnvioSec)) 
                {                                      
                    $arrUpdateFields[] = " data_min_envio_SEC = {$this->dataMinEnvioSec} ";
                }


                
                if (isset($this->dataMinEnvioOffsec)) 
                {                                      
                    $arrUpdateFields[] = " data_min_envio_OFFSEC = {$this->dataMinEnvioOffsec} ";
                }


                
                if (isset($this->protocoloInt)) 
                {                                      
                    $arrUpdateFields[] = " protocolo_INT = {$this->protocoloInt} ";
                }


                
                if (isset($this->registroEstadoId)) 
                {                                      
                    $arrUpdateFields[] = " registro_estado_id_INT = {$this->registroEstadoId} ";
                }


                
                if (isset($this->empresaParaClienteBoolean)) 
                {                                      
                    $arrUpdateFields[] = " empresa_para_cliente_BOOLEAN = {$this->empresaParaClienteBoolean} ";
                }


            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE mensagem SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function updateByInstanceUsingParameters($id, $parameters, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($parameters->titulo)) 
                {                                      
                    $arrUpdateFields[] = " titulo = {$this->titulo} ";
                }
                
                if (isset($parameters->mensagem)) 
                {                                      
                    $arrUpdateFields[] = " mensagem = {$this->mensagem} ";
                }
                
                if (isset($parameters->remetenteUsuarioId)) 
                {                                      
                    $arrUpdateFields[] = " remetente_usuario_id_INT = {$this->remetenteUsuarioId} ";
                }
                
                if (isset($parameters->tipoMensagemId)) 
                {                                      
                    $arrUpdateFields[] = " tipo_mensagem_id_INT = {$this->tipoMensagemId} ";
                }
                
                if (isset($parameters->destinatarioUsuarioId)) 
                {                                      
                    $arrUpdateFields[] = " destinatario_usuario_id_INT = {$this->destinatarioUsuarioId} ";
                }
                
                if (isset($parameters->destinatarioPessoaId)) 
                {                                      
                    $arrUpdateFields[] = " destinatario_pessoa_id_INT = {$this->destinatarioPessoaId} ";
                }
                
                if (isset($parameters->destinatarioEmpresaId)) 
                {                                      
                    $arrUpdateFields[] = " destinatario_empresa_id_INT = {$this->destinatarioEmpresaId} ";
                }
                
                if (isset($parameters->dataMinEnvioSec)) 
                {                                      
                    $arrUpdateFields[] = " data_min_envio_SEC = {$this->dataMinEnvioSec} ";
                }
                
                if (isset($parameters->dataMinEnvioOffsec)) 
                {                                      
                    $arrUpdateFields[] = " data_min_envio_OFFSEC = {$this->dataMinEnvioOffsec} ";
                }
                
                if (isset($parameters->protocoloInt)) 
                {                                      
                    $arrUpdateFields[] = " protocolo_INT = {$this->protocoloInt} ";
                }
                
                if (isset($parameters->registroEstadoId)) 
                {                                      
                    $arrUpdateFields[] = " registro_estado_id_INT = {$this->registroEstadoId} ";
                }
                
                if (isset($parameters->empresaParaClienteBoolean)) 
                {                                      
                    $arrUpdateFields[] = " empresa_para_cliente_BOOLEAN = {$this->empresaParaClienteBoolean} ";
                }
            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE mensagem SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }

}

?>
