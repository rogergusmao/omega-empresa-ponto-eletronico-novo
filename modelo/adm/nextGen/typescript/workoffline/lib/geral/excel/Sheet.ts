import { Cell } from 'lib/geral';

export class Sheet
{
    private nomeSheet: string;
    private conteudoSheet :any;
    private celulaInicial :Cell;
    private celulaCorrente :Cell;

    public constructor(nomeSheet :string)
    {
        this.nomeSheet = nomeSheet;
        this.celulaInicial = new Cell(1, 1);
        this.celulaCorrente = this.celulaInicial.clone();
        this.conteudoSheet = {};
    }

    private getMensagemTopo(mensagemTopo :string, numeroColunasMerge :number) :any
    {
        let celulaCorrente = this.celulaInicial.clone();
        let celulaFinalMerge = celulaCorrente.clone().moveCol(numeroColunasMerge-1);

        let conteudoMensagemTopo :any = { 
            "!merges": [{ s: celulaCorrente.getObject(), e: celulaFinalMerge.getObject()}]                
        };

        conteudoMensagemTopo[celulaCorrente.getString()] = {t: 's', v: mensagemTopo, s: this.getCabecalhoTabelaStyle()};
        return conteudoMensagemTopo;
    }

    private getCabecalhoTabelaStyle() :any
    {
        return { 
            alignment: { vertical: 'center', horizontal: 'center' },
            font: { sz: 14, bold: true, color: '#FF00FF' },
            fill: { patternType: 'solid', fgColor: { rgb: 'FFFFAA00' }, bgcolor: { rgb: 'FFFFAAFF' } }
        };
    }

    public appendCabecalho(arrCabecalhoTabela :Array<string>, mensagemTopo :string = null) :void
    {
        let cabecalho :any = {};

        if(mensagemTopo != null)
        {
            let numeroColunas = arrCabecalhoTabela.length;
            let conteudoMensagemTopo = this.getMensagemTopo(mensagemTopo, numeroColunas); 
            this.celulaCorrente.moveRow(2);
            cabecalho = { ...cabecalho, ...conteudoMensagemTopo};
        }
        
        let cabecalhoTabela :any = {};
        for(let colunaCabecalho of arrCabecalhoTabela)
        {
            cabecalhoTabela[this.celulaCorrente.getString()] = {t: 's', v: colunaCabecalho, s: this.getCabecalhoTabelaStyle()};
            this.celulaCorrente.moveCol(1);
        }
        
        this.conteudoSheet = { ...this.conteudoSheet, ...cabecalho, ...cabecalhoTabela };            
    }

    public appendDados(arrDados :Array<any>)
    {

    }

    public getSheetName() :string
    {
        return this.nomeSheet;
    }

    public getConteudo() :any
    {
        this.conteudoSheet["!ref"] = `${this.celulaInicial.getString()}:${this.celulaCorrente.getString()}`;
        return this.conteudoSheet;
    }

}
