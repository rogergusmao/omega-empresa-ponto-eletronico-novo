<?php

class SingletonRaizWorkspace
{
    public static $raizWorkspace = null;
}

function acharRaizWorkspace()
{
    if (SingletonRaizWorkspace::$raizWorkspace != null)
    {
        return SingletonRaizWorkspace::$raizWorkspace;
    }
    $pathDir = '';
    $niveis = 0;

    if (php_sapi_name() != 'cli')
    {

        $pathDir = $_SERVER["SCRIPT_FILENAME"];
        $niveis = substr_count($pathDir, "/");
    }
    else
    {

        $pathDir = getcwd();
        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN')
        {
            $niveis = substr_count($pathDir, "\\");
        }
        else
        {
            $niveis = substr_count($pathDir, "/");
        }

    }

    $root = "";

    for ($i = 0; $i < $niveis; $i++)
    {
        if (is_dir("{$root}__workspaceRoot"))
        {

            SingletonRaizWorkspace::$raizWorkspace = $root;
            return SingletonRaizWorkspace::$raizWorkspace;

        }
        else
        {
            $root .= "../";
        }
    }
    SingletonRaizWorkspace::$raizWorkspace = "";
    return SingletonRaizWorkspace::$raizWorkspace;
}

$rw = acharRaizWorkspace();

require_once $rw. DIRETORIO_BIBLIOTECAS_COMPARTILHADAS.'/classes/Funcoes.php';

$singletonFuncoes = Funcoes::getSingleton();

$raizWorkspace = Helper::acharRaizWorkspace();
$raiz = Helper::acharRaiz();

$classe = Helper::POSTGET("class");

$singletonFuncoes->setDiretorios(array(
    array($classe, Helper::acharRaiz() . "recursos/classes/", array("class/", "BO/")),
    array($classe, Helper::acharRaiz() . "web_service_pontoeletronico/", array('/')),
    array($classe, Helper::acharRaiz() . "web_service_sihop/", array('/')),
    array($classe, Helper::acharRaiz() . "recursos/protocolo/", array("in/", "out/")),
    array($classe, $raizWorkspace . DIRETORIO_BIBLIOTECAS_COMPARTILHADAS,
        array(
            "classes/",
            "php/",
            "adm_flatty/",
            "adm_padrao/",
            "imports/",
            "UI/Menu/",
            "UI/Controls/",
            "UI/Controls/Auxiliares/",
            "UI/Environment/",
            "UI/I18N/",
            "UI/Validation/",
            "pipeline_sync/")),
    array($classe, $raizWorkspace . DIRETORIO_BIBLIOTECAS_COMPARTILHADAS . "classes/", array("protocolo/")),
    array($classe, $raizWorkspace . DIRETORIO_BIBLIOTECAS_COMPARTILHADAS . "adm_flatty/", array("imports/")),
    array($classe, $raizWorkspace . DIRETORIO_BIBLIOTECAS_COMPARTILHADAS . "adm_padrao/", array("imports/")),

));

require_once acharRaizWorkspace().DIRETORIO_BIBLIOTECAS_COMPARTILHADAS.'/imports/instancias.php';
require_once '../'.PATH_RELATIVO_PROJETO.'imports/instancias.php';
