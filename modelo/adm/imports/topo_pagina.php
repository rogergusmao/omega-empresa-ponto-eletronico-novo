<?php

$idUsuario = Seguranca::getId();
$pathLogo = Helper::getUrlRaizDaBibliotecaCompartilhada('adm_flatty/imgs/omega_empresa.png');

?>

<header>
    <nav class='navbar navbar-default'>
        <a class='navbar-brand' href='index.php'>
            <img width="21" height="21" class="logo-xs" alt="Flatty"
                 src="<?= Javascript::getPathRaizBibliotecaFlatty() ?>/images/logo_xs.svg"/>
        </a>
        <a class='toggle-nav btn pull-left' href='#'>
            <i class='icon-reorder'></i>
        </a>

        <ul class='nav'>

            <li class='dropdown dark user-menu'>
                <a class='dropdown-toggle' data-toggle='dropdown' href='#'>
                    <span class='user-name'>
                        <?

                        if (strlen($idUsuario))
                        {
                            echo Seguranca::getEmail();
                        }

                        ?>
                    </span>
                    <b class='caret'></b>
                </a>
                <ul class='dropdown-menu'>
                    <li>
                        <a href='index.php?tipo=forms&page=usuario&id=<?= Seguranca::getId(); ?>'>
                            <i class='icon-user'></i>
                            <?= I18N::getExpression("Perfil do Usuário"); ?>
                        </a>
                    </li>
                    <li class='divider'></li>
                    <li>
                        <a data-toggle="modal" href="#modalAlterarSenha" onclick="onAlterarSenhaModalShow();">
                            <i class='icon-key'></i>
                            <?= I18N::getExpression("Alterar Senha"); ?>
                        </a>
                    </li>
                    <li class='divider'></li>
                    <li>
                        <a href='actions.php?class=Seguranca&action=logout'>
                            <i class='icon-signout'></i>
                            <?= I18N::getExpression("Logout"); ?>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>

        <ul class='nav top-bar-user-info'>

            <li class="notifications dropdown">
                <i class="icon-user"></i>
                <span class="top-bar-label"><?= I18N::getExpression("Usuário"); ?>: </span>
                <span class="top-bar-value"><?= Seguranca::getNomeDoUsuarioLogado() ?></span>
            </li>

            <li class="notifications dropdown">
                <i class="icon-building"></i>
                <span class="top-bar-label"><?= I18N::getExpression("Corporação"); ?>: </span>
                <span class="top-bar-value"><?= Seguranca::getNomeCorporacao() ?></span>
            </li>

            <li class="notifications dropdown">
                <i class="icon-group"></i>
                <span class="top-bar-label"></span><span class="top-bar-value">
                    <?= EXTDAO_Usuario::getStringDeUtilizacaoDeUsuariosDaCorporacao(); ?>
                </span>
            </li>

            <li class="notifications dropdown">
                <i class="icon-plus"></i>
                <span class="top-bar-label"></span><span class="top-bar-value">
                    <a href="//my.workoffline.com.br" target="_blank">
                        <?= I18N::getExpression("Aumentar/diminuir usuários"); ?>
                    </a>
                </span>
            </li>

        </ul>

    </nav>
</header>

<div class="modal fade" id="modalAlterarSenha" tabindex="-1" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" class="close" data-dismiss="modal" type="button">×</button>
                <h4 class="modal-title" id="myModalLabel">
                    <?= I18N::getExpression("Alterar Senha"); ?>
                </h4>
            </div>
            <div class="modal-body">

                <div class="alert alert-info" id="mensagem-1" style="display: none;">
                    <h4>
                        <i class="icon-info-sign"></i>
                        <?= I18N::getExpression("Aviso"); ?>
                    </h4>
                    <?= I18N::getExpression("Informe a sua senha atual."); ?>
                </div>

                <div class="alert alert-info" id="mensagem-2" style="display: none;">
                    <h4>
                        <i class="icon-info-sign"></i>
                        <?= I18N::getExpression("Aviso"); ?>
                    </h4>
                    <?= I18N::getExpression("A nova senha deve ter pelo menos 6 caracteres."); ?>
                </div>

                <div class="alert alert-info" id="mensagem-3" style="display: none;">
                    <h4>
                        <i class="icon-info-sign"></i>
                        <?= I18N::getExpression("Aviso"); ?>
                    </h4>
                    <?= I18N::getExpression("A confirmação de senha não confere com a nova senha informada."); ?>
                </div>

                <div class="alert alert-info" id="mensagem-4" style="display: none;">
                    <h4>
                        <i class="icon-info-sign"></i>
                        <?= I18N::getExpression("Aviso"); ?>
                    </h4>
                    <?= I18N::getExpression("A nova senha deve ser diferente da senha atual."); ?>
                </div>

                <div class="alert alert-info" id="mensagem-5" style="display: none;">
                    <h4>
                        <i class="icon-info-sign"></i>
                        <?= I18N::getExpression("Aviso"); ?>
                    </h4>
                    <span class="conteudo-mensagem"></span>
                </div>

                <form class="form" style="margin-bottom: 0;" method="post" action="#" accept-charset="UTF-8"><input
                            name="authenticity_token" type="hidden">

                    <div class="form-group">
                        <label for="senhaAtual">
                            <?= I18N::getExpression("Senha atual"); ?>:
                        </label>
                        <input class="form-control" id="senhaAtual" placeholder="" type="password" value="">
                    </div>

                    <div class="form-group">
                        <label for="senhaNova">
                            <?= I18N::getExpression("Nova senha"); ?>:
                        </label>
                        <input class="form-control" id="senhaNova" placeholder="" type="password" value="">
                    </div>

                    <div class="form-group">
                        <label for="senhaNovaConfirmacao">
                            <?= I18N::getExpression("Confirmar nova senha"); ?>:
                        </label>
                        <input class="form-control" id="senhaNovaConfirmacao" placeholder="" type="password" value="">
                    </div>

                </form>

            </div>
            <div class="modal-footer">
                <button class="btn btn-default" data-dismiss="modal" type="button">
                    Fechar
                </button>
                <button class="btn btn-primary" type="button" id="botaoAlterarSenha">
                    Alterar Senha
                </button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?= Helper::acharRaiz() ?>recursos/js/tela/alterar_senha.js"></script>

