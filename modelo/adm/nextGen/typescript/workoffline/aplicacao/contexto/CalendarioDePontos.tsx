import * as React from 'react';
import * as Moment from 'moment';
import { Button } from 'react-bootstrap';
import { EventObjectInput, View } from 'fullcalendar';

import { ContextoGenerico } from 'lib/contexto';
import { PontoEletronicoServices } from 'aplicacao/comum/dataService';
import { Workbook } from 'lib/geral';
import { TipoPontoEletronico } from 'aplicacao/comum/geral';
import { Calendario } from '../../lib/uiComponente/calendario/Calendario';

import { 
    TotalizadorDeHoras, 
    DistribuicaoDeHoras, 
    ProblemasEncontrados,
    CalendarioDePontos as CalendarioDePontosComponente, 
    ExportarDados as ModalExportarDados,
    CorrigirPonto as ModalCorrigirPonto
} from 'aplicacao/modulo/calendario/uiComponente';

import {
    PontoEletronico as PontoEletronicoModel,
    Pessoa as PessoaModel,
    Evento as EventoModel
} from 'aplicacao/modulo/calendario/model';    

export class CalendarioDePontos extends ContextoGenerico
{        
    public static contextFactory() : void
    {
        $(document).ready(function ()
        {
            let aplicacaoContext = new CalendarioDePontos('section#content');
            
            CalendarioDePontos.appContext = aplicacaoContext;
        });
    }

    
    public calendario: CalendarioDePontosComponente;
    public totalizador: TotalizadorDeHoras;
    public graficoDistribuicao: DistribuicaoDeHoras;
    public painelProblemasEncontrados: ProblemasEncontrados;

    public pontoEletronicoDataService: PontoEletronicoServices;
    public dadosPessoas: Map<number, PessoaModel>;

    public dataInicialVisualizacao: Moment.Moment;
    public dataFinalVisualizacao: Moment.Moment;

    public constructor(domRootSelector: string)
    {
        //chama o construtor do pai
        super(domRootSelector);

        //instancia o data service
        this.pontoEletronicoDataService = new PontoEletronicoServices();

        //inicializa dicionário de cores
        this.dadosPessoas = new Map<number, PessoaModel>();

        //corrige o layout da página para aplicar o calendário
        this.fixPageLayout();

        //renderiza componentes
        this.renderComponents();         
        
        //define lingua padrão do MomentJS
        Moment.locale('pt-br');        
    }

    public renderComponents() :void
    {
        //renderiza o calendário e seta a instância
        this.renderCalendar();

        //renderiza o totalizador de horas
        this.renderTotalizadorDeHoras();

        //renderiza botão de exportar dados
        this.renderBotaoExportarDados();

        //renderiza o gráfico de distribuição de horas
        this.renderDistribuicaoDeHoras();  

        //renderiza o painel de problemas detectados
        this.renderPainelProblemasEncontrados();
    }

    public renderBotaoExportarDados() :void
    {
        var botaoExportarDadosJSX = <a className="btn btn-xs btn-link" onClick={this.onButtonExportarDadosClick.bind(this)}>Exportar Dados</a>;
        this.renderReactComponent(botaoExportarDadosJSX, '#botao-exportar-dados-container'); 
    }

    public onButtonExportarDadosClick()
    {
        const janelaModalJSX = <ModalExportarDados                                    
                                    dataInicial={this.dataInicialVisualizacao}  
                                    dataFinal={this.dataFinalVisualizacao}  
                                    onExportarDadosClick={this.onConfirmarExportacaoDadosClick.bind(this)}
                            />;
        
        let janelaModal = this.renderReactComponent<ModalExportarDados>(janelaModalJSX, '#modal-container');
        janelaModal.show();
        janelaModal.atualizarDatasPadrao();
    }

    public async onSalvarDadosCorrigirPontoClick(pontoEletronico: PontoEletronicoModel)
    {
        await this.pontoEletronicoDataService.gravarPontoEletronicoParaCorrecao(pontoEletronico);

        //chama webservice para recarregar os dados atualizados
        if(this.dataInicialVisualizacao && this.dataFinalVisualizacao)
        {
            this.loadDataFromService(this.dataInicialVisualizacao, this.dataFinalVisualizacao);
        }
    }

    public onConfirmarExportacaoDadosClick(dataInicial :Moment.Moment, dataFinal :Moment.Moment)
    {
        let workbook = new Workbook("Pontos Eletronicos.xlsx");
        let sheet1 = workbook.createNewSheet("Pontos_Eletronicos");
        sheet1.appendCabecalho(["Teste 1", "Teste 2", "Teste 3", "Teste 4"], "Workoffline.com.br - Pontos Eletronicos");

        workbook.exportar();
    }

    public renderCalendar() :void
    {
        var calendarioJSX = <CalendarioDePontosComponente
                                onEventClick={this.onCalendarEventClick.bind(this)}
                                onCalendarViewChange={this.onCalendarViewChange.bind(this)}
                                setDefaultOptions  />;

        this.calendario = this.renderReactComponent(calendarioJSX, '#calendario-container'); 
        this.calendario.resizeCalendar();
    }

    public renderTotalizadorDeHoras() :void
    {
        var totalizadorJSX = <TotalizadorDeHoras />
        this.totalizador = this.renderReactComponent(totalizadorJSX, '#painel-totalizador-horas-container');
    }

    public renderDistribuicaoDeHoras() :void
    {
        var totalizadorJSX = <DistribuicaoDeHoras 
                                largura={$('#painel-distribuicao-horas-container').width()} 
                             />

        this.graficoDistribuicao = this.renderReactComponent(totalizadorJSX, '#painel-distribuicao-horas-container');
    }

    public renderPainelProblemasEncontrados() :void
    {
        var painelProblemasEncontradosJSX = <ProblemasEncontrados 
                                                onProblemaClick={this.onPainelProblemasEncontradosItemClick.bind(this)}                                                
                                            />

        this.painelProblemasEncontrados = this.renderReactComponent(painelProblemasEncontradosJSX, '#painel-problemas-encontrador-container');
    }

    public onCalendarViewChange(view: View, element: JQuery): any
    {
        this.dataInicialVisualizacao = view.intervalStart;
        this.dataFinalVisualizacao = view.intervalEnd;

        this.loadDataFromService(view.intervalStart, view.intervalEnd);
    }
    
    public onCalendarEventClick(event: EventoModel.FullCalendarEvent, jsEvent: MouseEvent, view: View): any
    {
        let idPontoInicial = event.idPontoInicial;
        let idPessoa = event.idPessoa;
        this.showModalCorrigirPonto(idPessoa, idPontoInicial);     
    }

    public onPainelProblemasEncontradosItemClick(evento: EventoModel)
    {
        let idPontoInicial = evento.pontoInicial.idPonto;
        let idPessoa = evento.idPessoa;
        this.showModalCorrigirPonto(idPessoa, idPontoInicial);
    }

    public showModalCorrigirPonto(idPessoa: number, idPontoInicial :number)
    {
        let pessoa = this.dadosPessoas.get(idPessoa);
        let eventoCorrigir :EventoModel = null;

        for(let evento of pessoa.eventos)
        {
            if(evento.pontoInicial.idPonto == idPontoInicial)
            {
                eventoCorrigir = evento;
                break;                    
            }
        }

        if(eventoCorrigir != null)
        {
            let dataPontoAnterior :Moment.Moment = null;
            let dataPontoPosterior :Moment.Moment = null;

            //percorre todos os pontos da pessoa para encontrar
            //o ponto imediatamente antes e o ponto imediatamente depois do ponto a corrigir
            //a fim de limitar as datas válidas no controle de data na modal a ser aberta        
            for(let evento of pessoa.eventos)
            {
                //caso o ponto corrente não seja o ponto a ser corrigido
                if(evento.pontoInicial.idPonto != eventoCorrigir.pontoInicial.idPonto)
                {
                    if(evento.pontoFinal != null 
                        && evento.pontoFinal.dataPonto.isBefore(eventoCorrigir.pontoInicial.dataPonto))
                    {
                        if(dataPontoAnterior == null || (dataPontoAnterior.isBefore(evento.pontoFinal.dataPonto)))
                        {
                            dataPontoAnterior = evento.pontoFinal.dataPonto;
                        }
                                        
                    }
                    else if(evento.pontoFinal == null 
                                && evento.pontoInicial.dataPonto.isBefore(eventoCorrigir.pontoInicial.dataPonto))
                    {
                        if(dataPontoAnterior == null || (dataPontoAnterior.isBefore(evento.pontoInicial.dataPonto)))
                        {
                            dataPontoAnterior = evento.pontoInicial.dataPonto;
                        }

                    }

                    if(evento.pontoInicial.dataPonto.isAfter(eventoCorrigir.pontoInicial.dataPonto))
                    {
                        if(dataPontoPosterior == null || (dataPontoPosterior.isAfter(evento.pontoInicial.dataPonto)))
                        {
                            dataPontoPosterior = evento.pontoInicial.dataPonto;
                        }
                                        
                    }

                }                

            }

            let dataMinimaPermitida :Moment.Moment = dataPontoAnterior ? dataPontoAnterior.clone() : null;
            let dataMaximaPermitida :Moment.Moment = dataPontoPosterior ? dataPontoPosterior.clone() : null;
            
            if(eventoCorrigir.pontoInicial.tipoPonto == TipoPontoEletronico.PONTO_ENTRADA)
            {
                dataMinimaPermitida = eventoCorrigir.pontoInicial.dataPonto.clone();
            }
            else if(eventoCorrigir.pontoInicial.tipoPonto == TipoPontoEletronico.PONTO_SAIDA)
            {
                dataMaximaPermitida = eventoCorrigir.pontoInicial.dataPonto.clone();
            }

            const janelaModalJSX = <ModalCorrigirPonto
                                        id={"modal-corrigir-ponto-" + eventoCorrigir.pontoInicial.idPonto}
                                        evento={eventoCorrigir}
                                        dataMinimaPermitida={dataMinimaPermitida}
                                        dataMaximaPermitida={dataMaximaPermitida}                         
                                        onSalvarDadosClick={this.onSalvarDadosCorrigirPontoClick.bind(this)}
                                    />;
        
            let janelaModal = this.renderReactComponent<ModalCorrigirPonto>(janelaModalJSX, '#modal-container');
            janelaModal.show();
        }
    }

    public fixPageLayout() :void
    {
        //corrige o layout no contexto específico da aplicação
        $('#wrapper').css('min-height', 'auto');
        $('#content > .container').css('padding-left', '0px');
        $('.internal-page-container').css('padding-left', '0px');
        $('.internal-page-container').css('padding-right', '0px');
    }

    public async loadDataFromService(startDatetime: Moment.Moment, endDatetime :Moment.Moment, append: boolean = false)
    {
        try
        {
            if(!append)
            {
                this.dadosPessoas = new Map<number, PessoaModel>();                     
            }

            var dadosIniciais = await this.pontoEletronicoDataService.getDadosIniciaisDoCalendario(startDatetime, endDatetime);
            if(dadosIniciais != null)
            {
                if(dadosIniciais.pontos != null)
                {
                    //adiciona um ponto nulo no final do array
                    dadosIniciais.pontos.push(null);

                    //estrutura para controle das iterações
                    let pontoEletronicoAnterior :PontoEletronicoModel = null;

                    //faz iteração dos dados recebidos do servidor
                    //tenta montar o par pontoEletronicoAnterior,pontoEletronicoAtual
                    //em cada evento a ser exibido no calendário
                    for(let pontoEletronico of dadosIniciais.pontos)
                    {
                        if(pontoEletronico == null)
                        {
                            if(pontoEletronicoAnterior != null)
                            {
                                this.criarEvento(pontoEletronicoAnterior);
                            }
                            
                            break;
                        }

                        let pontoEletronicoAtual = PontoEletronicoModel.factory(pontoEletronico);
                        if(pontoEletronicoAnterior == null)
                        {
                            pontoEletronicoAnterior = pontoEletronicoAtual;
                        }
                        else
                        {
                            //caso a pessoa da iteração atual seja diferente da iteração anterior
                            if(pontoEletronicoAtual.idPessoa != pontoEletronicoAnterior.idPessoa)
                            {
                                this.criarEvento(pontoEletronicoAnterior);
                                pontoEletronicoAnterior = pontoEletronicoAtual;
                            }
                            else
                            {
                                if(pontoEletronicoAnterior.tipoPonto == TipoPontoEletronico.PONTO_ENTRADA
                                    && pontoEletronicoAtual.tipoPonto == TipoPontoEletronico.PONTO_SAIDA)
                                {
                                    this.criarEvento(pontoEletronicoAnterior, pontoEletronicoAtual);
                                    pontoEletronicoAnterior = null;
                                }
                                else
                                {
                                    this.criarEvento(pontoEletronicoAnterior);
                                    pontoEletronicoAnterior = pontoEletronicoAtual;
                                }

                            }
                            
                        }                                                        
                        
                    }

                    //apos iterar a estrutura, adicionar eventos ao calendário              
                    this.adicionarEventosAoCalendario();   
                    this.atualizarViewsAuxiliares();
                }

            }

        }
        catch(ex)
        {
            console.log(ex);
        }

    }

    private adicionarEventosAoCalendario() :void
    {
        this.calendario.limparEventos();
        
        let arrEventos = new Array<EventoModel>();
        this.dadosPessoas.forEach((value, key) => arrEventos = arrEventos.concat(value.eventos));
        
        let fullCalendarEvents = arrEventos.map((evento) => evento.getEventoFullCalendar());
        this.calendario.adicionarEventos(fullCalendarEvents);
    }

    private atualizarViewsAuxiliares() :void
    {
        var arrDados = Array.from(this.dadosPessoas.values());
        this.totalizador.setData(arrDados);     
        this.graficoDistribuicao.setData(arrDados);
        this.painelProblemasEncontrados.setData(arrDados);
    }

    private criarEvento(pontoInicial :PontoEletronicoModel, pontoFinal? :PontoEletronicoModel) :void
    {
        if(!this.dadosPessoas.has(pontoInicial.idPessoa))
        {
            let pessoaModel = PessoaModel.factory(pontoInicial, pontoFinal);
            pessoaModel.addEvento(pontoInicial, pontoFinal);
            this.dadosPessoas.set(pontoInicial.idPessoa, pessoaModel);
        }
        else
        {
            this.dadosPessoas.get(pontoInicial.idPessoa).addEvento(pontoInicial, pontoFinal);
        }

    }

}

export namespace CalendarioDePontos
{
    export var appContext: CalendarioDePontos;    
}