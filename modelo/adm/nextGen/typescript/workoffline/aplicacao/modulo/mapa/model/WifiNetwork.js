"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var model_1 = require("lib/model");
var geral_1 = require("aplicacao/comum/geral");
var WifiNetwork = (function (_super) {
    __extends(WifiNetwork, _super);
    function WifiNetwork() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    WifiNetwork.parseFromRemoteObject = function (remoteObject) {
        var instance = new WifiNetwork();
        instance.ssid = remoteObject.ssid;
        instance.idEmpresa = geral_1.Helper.parseInt(remoteObject.idEmpresa);
        instance.idUsuario = geral_1.Helper.parseInt(remoteObject.idUsuario);
        instance.dataInicio = DateUtil.getDateObject(geral_1.Helper.parseInt(remoteObject.dataInicioSec) + geral_1.Helper.parseInt(remoteObject.dataInicioOffsec));
        instance.dataFim = DateUtil.getDateObject(geral_1.Helper.parseInt(remoteObject.dataFimSec) + geral_1.Helper.parseInt(remoteObject.dataFimOffsec));
        return instance;
    };
    return WifiNetwork;
}(model_1.Model));
exports.WifiNetwork = WifiNetwork;
