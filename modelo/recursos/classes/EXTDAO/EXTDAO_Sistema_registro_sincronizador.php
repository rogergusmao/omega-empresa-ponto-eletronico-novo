<?php

/*

Arquivo gerado atravï¿½s de gerador de cï¿½digo em 02/03/2017 as 19:48:07.
Para que o arquivo nï¿½o seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

Tabela correspondente: sistema_registro_sincronizador
Sobrescrita de dados: ---SOBRESCRITA_NAO_AUTORIZADA---

*/

class EXTDAO_Sistema_registro_sincronizador extends DAO_Sistema_registro_sincronizador
{
    public function __construct($configDAO = null)
    {
        parent::__construct($configDAO);
        $this->nomeClasse = "EXTDAO_SistemaRegistroSincronizador";
    }

    static $objSincronizador = null;

    public static function insertTupla(

        $pIdSistemaTabela, $pIdUsuarioAdm, $pIdCorporacao, $pIsFromAndroid,
        $pIdTipoOperacaoBanco, $pImei, $pIdPrograma, $pIdTabela)
    {

        if (!strlen($pIdTabela))
        {
            return false;
        }

        if (self::$objSincronizador == null)
        {
            self::$objSincronizador = new EXTDAO_Sistema_registro_sincronizador();
        }
        self::$objSincronizador->clear();
        self::$objSincronizador->setSistema_tabela_id_INT($pIdSistemaTabela);
        self::$objSincronizador->setUsuario_id_INT($pIdUsuarioAdm);

        self::$objSincronizador->setIs_from_android_BOOLEAN($pIsFromAndroid);
        self::$objSincronizador->setTipo_operacao_banco_id_INT($pIdTipoOperacaoBanco);
        self::$objSincronizador->setImei($pImei);
        self::$objSincronizador->setSistema_produto_INT(null);
        self::$objSincronizador->setSistema_projetos_versao_INT(null);
        self::$objSincronizador->setId_tabela_INT($pIdTabela);
        self::$objSincronizador->formatarParaSQL();

        return self::$objSincronizador->insert(false, $pIdCorporacao);
    }

    public function factory()
    {
        return new EXTDAO_SistemaRegistroSincronizador();
    }

    public function inserirRegistro(
        $pIdSistemaTabela, $pIdUsuarioAdm, $pIdCorporacao, $pIsFromAndroid,
        $pIdTipoOperacaoBanco, $pImei, $pIdPrograma, $pIdTabela)
    {
        if (empty($pIdTabela))
        {
            return false;
        }
        $this->clear();
        $this->setSistema_tabela_id_INT($pIdSistemaTabela);
        $this->setUsuario_id_INT($pIdUsuarioAdm);
        $this->setIs_from_android_BOOLEAN($pIsFromAndroid);
        $this->setTipo_operacao_banco_id_INT($pIdTipoOperacaoBanco);
        $this->setImei($pImei);
        $this->setId_tabela_INT($pIdTabela);
        $this->setSistema_projetos_versao_INT($pIdPrograma);

        $this->formatarParaSQL();
        $this->insert(false, $pIdCorporacao);

    }

    public static function inserirMultiplosRegistros(
        Database $db, $tabela, $pIdUsuarioAdm,
        $pIdCorporacao, $pIsFromAndroid,
        $pIdTipoOperacaoBanco, $imei, $pIdPrograma, $ids
    )
    {
        $pIdSistemaTabela = EXTDAO_Sistema_tabela::getIdDaSistemaTabela($tabela, $db);

        $cabecalho = "INSERT INTO sistema_registro_sincronizador (
sistema_tabela_id_INT
,usuario_id_INT,
,is_from_android_BOOLEAN
,tipo_operacao_banco_id_INT
,imei
,id_tabela_INT
,sistema_projetos_versao_INT
,corporacao_id_INT)
VALUES ";

        $imei = empty($imei) ? "null" : $imei;
        $pIdPrograma = empty($pIdPrograma) ? "null" : "'$pIdPrograma'";
        $pIdCorporacao = empty($pIdCorporacao) ? "null" : $pIdCorporacao;
        $pIsFromAndroid = !empty($pIsFromAndroid) && $pIsFromAndroid == true ? "1" : "0";
        $pIdUsuarioAdm = empty($pIdUsuarioAdm) ? "null" : $pIdUsuarioAdm;
        $valores = "";
        if (is_array($ids))
        {
            foreach ($ids as $id)
            {
                if (!empty($valores))
                {
                    $valores .= ", ";
                }
                $valores .= "($pIdSistemaTabela, $pIdUsuarioAdm, $pIsFromAndroid, 
                $pIdTipoOperacaoBanco, $imei, $id, $pIdPrograma, $pIdCorporacao)";
            }
        }
        else
        {
            $valores .= "($pIdSistemaTabela, $pIdUsuarioAdm, $pIsFromAndroid, 
                $pIdTipoOperacaoBanco, $imei, $ids, $pIdPrograma, $pIdCorporacao)";
        }

        return $db->queryMensagem($cabecalho . $valores);
    }

    public static function deleteMultiplo(
        Database $db, $tabela, $idUsuarioResponsavel, $idCorporacao, $idPrograma, $imei,
        $querySelectIds)
    {

        $msg = $db->queryMensagem($querySelectIds);
        if ($msg != null && !Interface_mensagem::checkOk($msg))
        {
            return $msg;
        }
        $idOuIdsRegistro = Helper::getResultSetToArrayDeUmCampo($db->result);

        foreach ($idOuIdsRegistro as $id)
        {
            if (!empty($strIn))
            {
                $strIn .= ", ";
            }
            $strIn .= $id;

        }

        $msg = $db->queryMensagem("DELETE FROM $tabela WHERE id IN ($strIn)");
        if ($msg != null && !Interface_mensagem::checkOk($msg))
        {
            return $msg;
        }

        EXTDAO_Sistema_registro_sincronizador::inserirMultiplosRegistros(
            $db, $tabela, $idUsuarioResponsavel,
            $idCorporacao, '0',
            DatabaseSincronizador::$INDEX_OPERACAO_REMOVE, $imei, $idPrograma,
            $idOuIdsRegistro
        );
        return null;
    }
}
