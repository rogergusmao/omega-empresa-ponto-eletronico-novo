import { Helper } from 'lib/geral';

export class GeoLocationHelper
{
    static getFloatValue(integerCoordinate :number) :number
    static getFloatValue(integerCoordinate :string) :number
    static getFloatValue(integerCoordinate :any) :number
    {
        if(integerCoordinate instanceof String)
        {
            integerCoordinate = Helper.parseInt(integerCoordinate as string);
        }
        return integerCoordinate / 1e6;
    }

    static getIntegerValue(floatCoordinate :number) :number
    {
        return floatCoordinate * 1e6;
    }

    static getGoogleLatLng(integerLat :number, integerLng :number) : google.maps.LatLng
    {
        var floatLat = GeoLocationHelper.getFloatValue(integerLat);
        var floatLng = GeoLocationHelper.getFloatValue(integerLng);

        return new google.maps.LatLng(floatLat, floatLng);
    }

    static isValidCoordinate(floatLat :number, floatLng :number)
    {
        return (floatLat != null && floatLat >= -90 && floatLat <=90
                    && floatLng != null && floatLng >= -180 && floatLng <=180);
    }

}