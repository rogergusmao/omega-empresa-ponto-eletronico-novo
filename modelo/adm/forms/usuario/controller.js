/*

 Arquivo gerado atrav�s de gerador de c�digo em 04/06/2017 as 23:37:04.
 Para que o arquivo n�o seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

 Tabela correspondente: usuario
 Sobrescrita de dados: ---SOBRESCRITA_NAO_AUTORIZADA---

 */
omegaApp.controller('formUsuarioController',
    ['$scope', '$rootScope', '$element', '$window', '$interval', '$timeout', 'dataService', 'Upload',
     function ($scope, $rootScope, $element, $window, $interval, $timeout, dataService, Upload)
     {
         var vm = this;
         omegaApp.generatedFormUsuarioController($scope, $rootScope, $element, $window, $interval, $timeout, dataService, Upload, vm);

         try
         {
             vm.rootControllerElement = $element;
             vm.enderecoDeEmailVerificado = null;

             //dados do formul�rio
             vm.formData =
                 {
                     id: null,
                     nome: null,
                     email: null,
                     senha: null,
                     confirmacaoSenha: null,
                     usuarioTipoId: null,
                     empresaId: null,
                     isAtivo: true,

                     pessoaSexoId: null,
                     pessoaTelefone: null,
                     pessoaCelular: null,
                     pessoaTipoDocumentoId: null,
                     pessoaNumeroDocumento: null,
                     pessoaLogradouro: null,
                     pessoaNumero: null,
                     pessoaComplemento: null,
                     pessoaCidadeId: null,
                     pessoaBairroId: null,

                     categoriaPermissaoAndroidId: null,
                     isAdministradorAndroid: true,
                     isAdministradorWeb: false,
                     servicosAndroidSelecionados: [],
                     itensMenuSelecionados: [],

                     isAdministradorGlobalDaConta: false

                 };

             //campos desativados
             vm.disabledFields =
                 {
                     fieldNome: false,
                     fieldEmail: false,
                     fieldSenha: false,
                     fieldConfirmacaoSenha: false,
                     fieldUsuarioTipoId: false,
                     fieldIsAtivo: false,

                     fieldPessoaSexoId: false,
                     fieldPessosaTelefone: false,
                     fieldPessoaCelular: false,
                     fieldPessoaTipoDocumentoId: false,
                     fieldPessoaNumeroDocumento: false,
                     fieldPessoaLogradouro: false,
                     fieldPessoaNumero: false,
                     fieldPessoaComplemento: false,
                     fieldPessoaCidadeId: false,
                     fieldPessoaBairroId: false,

                     fieldIsAdministradorAndroid: false,
                     fieldIsAdministradorWeb: false,
                     fieldCategoriaPermissaoAndroidId: false
                 };

             //parametros dos campos
             vm.fieldsParameters =
                 {
                     fieldNome: null,
                     fieldEmail: null,
                     fieldSenha: null,
                     fieldConfirmacaoSenha: null,
                     fieldUsuarioTipoId: {remoteData: null},
                     fieldEmpresaId: {remoteData: null},

                     fieldPessosaTelefone: null,
                     fieldPessoaCelular: null,
                     fieldPessoaNumeroDocumento: null,
                     fieldPessoaLogradouro: null,
                     fieldPessoaNumero: null,
                     fieldPessoaComplemento: null,

                     fieldPessoaTipoDocumentoId: {remoteData: null},

                     fieldPessoaSexoId: {remoteData: null},
                     fieldPessoaCidadeId: {remoteData: null},
                     fieldPessoaBairroId: {remoteData: null},

                     fieldIsAtivo: null,
                     fieldIsAdministradorAndroidBoolean: null,

                     fieldCategoriaPermissaoAndroidId: {remoteData: null}
                 };

             vm.requiredFields =
                 {
                     fieldNome: true,
                     fieldEmail: true,
                     fieldSenha: true
                 };

             var relationshipObject =
                 {
                     usuarioTipoId: 'fieldUsuarioTipoId',
                     empresaId: 'fieldEmpresaId',
                     pessoaTipoDocumentoId: 'fieldPessoaTipoDocumentoId',
                     pessoaSexoId: 'fieldPessoaSexoId',
                     pessoaCidadeId: 'fieldPessoaCidadeId',
                     pessoaBairroId: 'fieldPessoaBairroId'
                 };

             /*
              RECUPERA OS DADOS DE EDI��O DO USU�RIO E RECUPERAR LISTAS INICIAIS NO MESMO REQUEST.
              UTILIZADO SOMENTE NA EDI��O, POIS NO CADASTRO O FORMUL�RIO � SIMPLIFICADO E N�O POSSUI LISTAS
              */

             var initialLoading = function()
             {
                 vm.isInitialLoadFinished = false;
                 $timeout(function ()
                 {
                     if (AngularControllerUtil.isEditionMode(vm))
                     {
                         var filterParameters = AngularControllerUtil.getFilterParametersForEdition(vm);
                         dataService.getDadosCompletosDoUsuarioAndEstruturasAdicionais(filterParameters).then(function (returnContent)
                         {
                             doOperationAfterEditionDataReceived(returnContent);
                         });
                     }

                 }, 200);

             };

             //carregamento inicial
             initialLoading();

             var doOperationAfterEditionDataReceived = function (returnContent)
             {
                 if (RemoteDataUtil.validateRemoteResponse(returnContent, [PROTOCOLO_SISTEMA.OPERACAO_REALIZADA_COM_SUCESSO]))
                 {
                     var returnObject = RemoteDataUtil.getResponseObject(returnContent);
                     RemoteDataUtil.refactorInitialFormLoadRemoteResponseData(vm, returnObject, relationshipObject);

                     vm.listaItensMenuWeb = returnObject.listaDeItensDoMenuSistemaWeb;
                     vm.listaServicosAndroid = returnObject.listaDeServicosAndroid;
                     vm.disabledFields.fieldEmail = true;

                     if (!GeneralUtil.isNullOrEmptyArray(returnObject.listaDeItensDoMenuSistemaWebDoUsuario))
                     {
                         vm.formData.itensMenuSelecionados = returnObject.listaDeItensDoMenuSistemaWebDoUsuario;
                     }
                     else
                     {
                         vm.formData.itensMenuSelecionados = [];
                     }

                     if (!GeneralUtil.isNullOrEmptyArray(returnObject.listaDeServicosAndroidDoUsuario))
                     {
                         vm.formData.servicosAndroidSelecionados = returnObject.listaDeServicosAndroidDoUsuario;
                     }
                     else
                     {
                         vm.formData.servicosAndroidSelecionados = [];
                     }

                     gerarListaDivididaDeItensDoMenuSistemaWeb(vm.listaItensMenuWeb);
                     OmegaFormUtil.expandSectionPanels(vm);
                 }
                 else
                 {
                     RemoteDataUtil.defaultRemoteErrorHandling(returnContent);
                 }

                 $timeout(function ()
                 {
                     vm.isInitialLoadFinished = true;
                 }, 100);
             };

             /*
              POST DO FORMUL�RIO (CADASTRO OU EDI��O
              */
             vm.submitForm = function (formInstance)
             {
                 if ($rootScope.validateForm(formInstance))
                 {
                     //caso seja cadastro de usu�rio, valida a senha e a confirma��o
                     if (!AngularControllerUtil.isEditionMode(vm) && vm.formData.senha != vm.formData.confirmacaoSenha)
                     {
                         ApplicationUtil.showErrorMessage(I18NUtil.getExpression("A senha e a confirma��o de senha n�o correspondem.", "Password does not match with confirmation."));
                     }
                     else
                     {
                         //caso seja cadastro
                         if (!AngularControllerUtil.isEditionMode(vm))
                         {
                             var formData = RemoteDataUtil.refactorRemoteRequest(vm.formData, vm.relatedEntities);

                             //faz o cadastro simples, caso seja feito com sucesso, recupera automaticamente os dados
                             // para o modo de edi��o e carrega o formul�rio para edi��o
                             dataService.addUsuario(formData).then(function (returnContent)
                             {
                                 if (RemoteDataUtil.validateRemoteResponse(returnContent, [PROTOCOLO_SISTEMA.OPERACAO_REALIZADA_COM_SUCESSO]))
                                 {
                                     var returnObject = RemoteDataUtil.getResponseObject(returnContent);

                                     if (GeneralUtil.isNumberOrNumericString(returnObject.formData.id))
                                     {
                                         vm.isEdition = true;
                                         vm.entityIdForEdition = returnObject.formData.id;

                                         ApplicationUtil.addParameterToQueryString("id", vm.entityIdForEdition);
                                         doOperationAfterEditionDataReceived(returnContent);
                                     }

                                 }
                                 else
                                 {
                                     RemoteDataUtil.defaultRemoteErrorHandling(returnContent);
                                 }
                             });
                         }
                         //prossegue com a submiss�o dos dados
                         else
                         {
                             var formData = RemoteDataUtil.refactorRemoteRequest(vm.formData, vm.relatedEntities);
                             dataService.editUsuario(formData).then(function (returnContent)
                             {
                                 if (RemoteDataUtil.validateRemoteResponse(returnContent))
                                 {
                                     $rootScope.showDialog(vm, 'after-usuario-update-template.html');
                                 }
                                 else
                                 {
                                     RemoteDataUtil.defaultRemoteErrorHandling(returnContent);
                                 }
                             });
                         }
                     }
                 }
             };

             /*
              M�TODOS REFERENTES A VALIDA��O DE EMAIL
              */
             var procedimentoValidarEmail = function (callbackEmailExistente, callbackEmailNaoExistente, callbackErro)
             {
                 callbackErro = callbackErro || function ()
                 {
                 };

                 var enderecoEmail = vm.formData.email;
                 var idUsuario = AngularControllerUtil.isEditionMode(vm) ? vm.formData.id : null;
                 dataService.validarEmailUsuario(enderecoEmail, idUsuario).then(function (returnContent)
                 {
                     if (RemoteDataUtil.validateRemoteResponse(returnContent, [PROTOCOLO_SISTEMA.SIM, PROTOCOLO_SISTEMA.NAO]))
                     {
                         var responseCode = RemoteDataUtil.getResponseCode(returnContent);

                         //caso n�o exista outro usu�rio com o mesmo e-mail
                         if (responseCode == PROTOCOLO_SISTEMA.NAO)
                         {
                             callbackEmailNaoExistente(returnContent);

                         }
                         //caso exista outro usu�rio com o mesmo e-mail
                         else
                         {
                             callbackEmailExistente(returnContent);
                         }
                     }
                     else
                     {
                         callbackErro(returnContent);
                     }
                 });

             };

             vm.validarEmail = function ()
             {
                 procedimentoValidarEmail(
                     //email existente
                     function (returnContent)
                     {
                         var dialogOptions =
                             {
                                 ws: 'lg', //tamanho da janela de dialog
                                 kb: false //n�o fecha com 'ESC'
                             };
                         $rootScope.showDialog(vm, 'email-ja-existente-template.html', dialogOptions);
                     },

                     //email n�o existente
                     function (returnContent)
                     {
                         vm.disabledFields.fieldEmail = true;
                         ApplicationUtil.showSuccessMessage("O endere�o de informado � v�lido para utiliza��o", "E-mail v�lido", vm);
                     },

                     //erro na opera��o
                     function (returnContent)
                     {
                         RemoteDataUtil.defaultRemoteErrorHandling(returnContent);
                     }
                 );

             };

             vm.onDialogEmailJaExistenteTrocarEmailClick = function ()
             {
                 $rootScope.closeMessageDialog();
                 vm.formData.email = '';
             };

             vm.onDialogEmailJaExistenteSairClick = function ()
             {
                 window.history.back();
             };

             /*
              M�TODOS REFERENTES � TROCA DE E-MAIL
              */
             vm.utilizarOutroEmail = function ()
             {
                 var modalIndex = $rootScope.getNextModalIndex();
                 $rootScope.registerModalInstance($rootScope.showModalDialog('forms', 'usuarioTrocarEmail', modalIndex, {
                     idUsuario: vm.formData.id,
                     emailAtual: vm.formData.email
                 }, {size: 'md'}));

                 //recarrega o formul�rio ap�s o fechamento da modal
                 var unregisterModal = $rootScope.$watch(function ()
                     {
                         return $rootScope.uibModalInstance[modalIndex];
                     },
                     function ()
                     {
                         initialLoading();
                     });
             };

             vm.getSubmitButtonLabel = function ()
             {
                 return AngularControllerUtil.isEditionMode(vm) ? "Salvar Dados" : "Prosseguir";
             };

             /*
              JANELAS MODAL DE CADASTROS DE APOIO
              */
             vm.openUsuarioTipoIdModal = function ()
             {
                 var modalIndex = $rootScope.getNextModalIndex();
                 $rootScope.registerModalInstance($rootScope.showModalDialog('forms', 'usuarioTipo', modalIndex));

                 //atualizando valores da lista ap�s fechamento da modal
                 var unregisterModal = $rootScope.$watch(function ()
                     {
                         return $rootScope.uibModalInstance[modalIndex]
                     },
                     function ()
                     {
                         if ($rootScope.uibModalInstance[modalIndex] == null)
                         {
                             vm.getListOfUsuarioTipoId();
                             vm.formData.usuarioTipoId = null;
                             unregisterModal();
                         }
                     });
             };

             vm.openEmpresaIdModal = function ()
             {
                 var modalIndex = $rootScope.getNextModalIndex();
                 $rootScope.registerModalInstance($rootScope.showModalDialog('forms', 'empresa', modalIndex));

                 //atualizando valores da lista ap�s fechamento da modal
                 var unregisterModal = $rootScope.$watch(function ()
                     {
                         return $rootScope.uibModalInstance[modalIndex]
                     },
                     function ()
                     {
                         if ($rootScope.uibModalInstance[modalIndex] == null)
                         {
                             vm.getListOfEmpresaId();
                             vm.formData.empresaId = null;
                             unregisterModal();
                         }
                     });
             };

             vm.openPessoaTipoDocumentoIdModal = function ()
             {
                 var modalIndex = $rootScope.getNextModalIndex();
                 $rootScope.registerModalInstance($rootScope.showModalDialog('forms', 'tipoDocumento', modalIndex));

                 //atualizando valores da lista
                 var unregisterModal = $rootScope.$watch(function ()
                     {
                         return $rootScope.uibModalInstance[modalIndex]
                     },
                     function ()
                     {
                         if ($rootScope.uibModalInstance[modalIndex] == null)
                         {
                             vm.getListOfPessoaTipoDocumentoId();
                             vm.formData.pessoaTipoDocumentoId = null;
                             unregisterModal();
                         }
                     });
             };

             vm.openPessoaSexoIdModal = function ()
             {
                 var modalIndex = $rootScope.getNextModalIndex();
                 $rootScope.registerModalInstance($rootScope.showModalDialog('forms', 'sexo', modalIndex));

                 //atualizando valores da lista ap�s fechamento da modal
                 var unregisterModal = $rootScope.$watch(function ()
                     {
                         return $rootScope.uibModalInstance[modalIndex]
                     },
                     function ()
                     {
                         if ($rootScope.uibModalInstance[modalIndex] == null)
                         {
                             vm.getListOfPessoaSexoId();
                             vm.formData.pessoaSexoId = null;
                             unregisterModal();
                         }
                     });
             };

             vm.openPessoaCidadeIdModal = function ()
             {
                 var modalIndex = $rootScope.getNextModalIndex();
                 $rootScope.registerModalInstance($rootScope.showModalDialog('forms', 'cidade', modalIndex));

                 //atualizando valores da lista ap�s fechamento da modal
                 var unregisterModal = $rootScope.$watch(function ()
                     {
                         return $rootScope.uibModalInstance[modalIndex]
                     },
                     function ()
                     {
                         if ($rootScope.uibModalInstance[modalIndex] == null)
                         {
                             vm.getListOfPessoaCidadeId();
                             vm.formData.pessoaCidadeId = null;
                             unregisterModal();
                         }
                     });
             };

             vm.openPessoaBairroIdModal = function ()
             {
                 var modalIndex = $rootScope.getNextModalIndex();
                 $rootScope.registerModalInstance($rootScope.showModalDialog('forms', 'bairro', modalIndex));

                 //atualizando valores da lista ap�s fechamento da modal
                 var unregisterModal = $rootScope.$watch(function ()
                     {
                         return $rootScope.uibModalInstance[modalIndex];
                     },
                     function ()
                     {
                         if ($rootScope.uibModalInstance[modalIndex] == null)
                         {
                             if (!GeneralUtil.isNullOrEmpty(vm.formData.pessoaCidadeId))
                             {
                                 vm.getListOfPessoaBairroId(vm.formData.pessoaCidadeId.id);
                             }

                             vm.formData.pessoaBairroId = null;
                             unregisterModal();
                         }
                     });
             };

             vm.openCategoriaPermissaoAndroidIdModal = function ()
             {
                 var modalIndex = $rootScope.getNextModalIndex();
                 $rootScope.registerModalInstance($rootScope.showModalDialog('forms', 'categoriaPermissao', modalIndex));

                 //atualizando valores da lista ap�s fechamento da modal
                 var unregisterModal = $rootScope.$watch(function ()
                     {
                         return $rootScope.uibModalInstance[modalIndex];
                     },
                     function ()
                     {
                         if ($rootScope.uibModalInstance[modalIndex] == null)
                         {
                             vm.getListOfCategoriaPermissaoAndroidId();
                             vm.formData.categoriaPermissaoId = null;
                             unregisterModal();
                         }
                     });
             };

             /*
              M�TODOS WRAPPER DOS DATASERVICES
              */
             vm.getListOfUsuarioTipoId = function ()
             {
                 if (!vm.isInitialLoadFinished)
                 {
                     return;
                 }

                 dataService.getListOfUsuarioTipoForUsuarioForm().then(function (returnContent)
                 {
                     if (RemoteDataUtil.validateRemoteResponse(returnContent))
                     {
                         vm.fieldsParameters.fieldUsuarioTipoId.remoteData = RemoteDataUtil.getResponseObjectDataSetOrEmptyArray(returnContent);
                     }
                     else
                     {
                         LogUtil.logRemoteResponseError(returnContent);
                     }
                 });
             };

             vm.getListOfEmpresaId = function ()
             {
                 if (!vm.isInitialLoadFinished)
                 {
                     return;
                 }

                 dataService.getListOfEmpresaForUsuarioForm().then(function (returnContent)
                 {
                     if (RemoteDataUtil.validateRemoteResponse(returnContent))
                     {
                         vm.fieldsParameters.fieldEmpresaId.remoteData = RemoteDataUtil.getResponseObjectDataSetOrEmptyArray(returnContent);
                     }
                     else
                     {
                         LogUtil.logRemoteResponseError(returnContent);
                     }
                 });
             };

             vm.getListOfPessoaTipoDocumentoId = function ()
             {
                 if (!vm.isInitialLoadFinished)
                 {
                     return;
                 }

                 dataService.getListOfTipoDocumentoForUsuarioForm().then(function (returnContent)
                 {
                     if (RemoteDataUtil.validateRemoteResponse(returnContent))
                     {
                         vm.fieldsParameters.fieldPessoaTipoDocumentoId.remoteData = RemoteDataUtil.getResponseObjectDataSetOrEmptyArray(returnContent);
                     }
                     else
                     {
                         LogUtil.logRemoteResponseError(returnContent);
                     }
                 });
             };

             vm.getListOfPessoaSexoId = function ()
             {
                 dataService.getListOfSexoForUsuarioForm().then(function (returnContent)
                 {
                     if (RemoteDataUtil.validateRemoteResponse(returnContent))
                     {
                         vm.fieldsParameters.fieldPessoaSexoId.remoteData = RemoteDataUtil.getResponseObjectDataSetOrEmptyArray(returnContent);
                     }
                     else
                     {
                         LogUtil.logRemoteResponseError(returnContent);
                     }
                 });
             };

             vm.getListOfPessoaCidadeId = function ()
             {
                 if (!vm.isInitialLoadFinished)
                 {
                     return;
                 }

                 dataService.getListOfCidadeForUsuarioForm().then(function (returnContent)
                 {
                     if (RemoteDataUtil.validateRemoteResponse(returnContent))
                     {
                         vm.fieldsParameters.fieldPessoaCidadeId.remoteData = RemoteDataUtil.getResponseObjectDataSetOrEmptyArray(returnContent);
                     }
                     else
                     {
                         LogUtil.logRemoteResponseError(returnContent);
                     }
                 });
             };

             //caso altere a cidade, carrega os bairros
             $scope.$watch(
                 //express�o do Watch
                 function ()
                 {
                     return vm.formData.pessoaCidadeId
                 },
                 //callback em caso de altera��o
                 function ()
                 {
                     if (!vm.isInitialLoadFinished)
                     {
                         return;
                     }

                     if (!GeneralUtil.isNullOrEmpty(vm.formData.pessoaCidadeId))
                     {
                         vm.getListOfPessoaBairroId(vm.formData.pessoaCidadeId.id);
                         vm.formData.pessoaBairroId = null;
                     }
                 }
             );

             vm.getListOfPessoaBairroId = function (cidadeId)
             {
                 if (!vm.isInitialLoadFinished)
                 {
                     return;
                 }

                 dataService.getListOfBairroForUsuarioForm(cidadeId).then(function (returnContent)
                 {
                     if (RemoteDataUtil.validateRemoteResponse(returnContent))
                     {
                         var remoteDataSet = RemoteDataUtil.getResponseObjectDataSetOrEmptyArray(returnContent);
                         vm.fieldsParameters.fieldPessoaBairroId.remoteData = remoteDataSet;
                     }
                     else
                     {
                         LogUtil.logRemoteResponseError(returnContent);
                     }
                 });
             };

             /*
              M�TODOS REFERENTES �S PERMISS�ES DO USU�RIO
              */
             vm.getListOfCategoriaPermissaoAndroidId = function ()
             {
                 if (!vm.isInitialLoadFinished)
                 {
                     return;
                 }

                 dataService.getListOfCategoriaPermissaoAndroidForUsuarioForm().then(function (returnContent)
                 {
                     if (RemoteDataUtil.validateRemoteResponse(returnContent))
                     {
                         vm.fieldsParameters.fieldCategoriaPermissaoAndroidId.remoteData = RemoteDataUtil.getResponseObjectDataSetOrEmptyArray(returnContent);
                     }
                     else
                     {
                         LogUtil.logRemoteResponseError(returnContent);
                     }
                 });
             };

             vm.getListOfServicosAndroid = function ()
             {
                 if (!vm.isInitialLoadFinished)
                 {
                     return;
                 }

                 dataService.getListOfServicosAndroidForUsuarioForm().then(function (returnContent)
                 {
                     if (RemoteDataUtil.validateRemoteResponseForList(returnContent))
                     {
                         vm.listaServicosAndroid = RemoteDataUtil.getResponseObjectDataSetOrEmptyArray(returnContent);
                     }
                     else
                     {
                         LogUtil.logRemoteResponseError(returnContent);
                     }
                 });
             };

             vm.getListOfItemMenuSistemaWeb = function ()
             {
                 if (!vm.isInitialLoadFinished)
                 {
                     return;
                 }

                 dataService.getListOfItemMenuSistemaWebForUsuarioForm().then(function (returnContent)
                 {
                     if (RemoteDataUtil.validateRemoteResponse(returnContent))
                     {
                         vm.listaItensMenuWeb = RemoteDataUtil.getResponseObject(returnContent);
                         gerarListaDivididaDeItensDoMenuSistemaWeb(vm.listaItensMenuWeb);
                     }
                     else
                     {
                         LogUtil.logRemoteResponseError(returnContent);
                     }
                 });
             };

             var gerarListaDivididaDeItensDoMenuSistemaWeb = function ()
             {
                 if (ArrayUtil.isArray(vm.listaItensMenuWeb))
                 {
                     vm.listaItensMenuWebEsquerda = vm.listaItensMenuWeb.slice(0, Math.floor(vm.listaItensMenuWeb.length / 2));
                     vm.listaItensMenuWebDireita = vm.listaItensMenuWeb.slice(Math.floor(vm.listaItensMenuWeb.length / 2), vm.listaItensMenuWeb.length);
                 }
             };

             vm.getMenuWebItemClass = function (nivelItemMenu)
             {
                 return "menu-item-nivel-" + nivelItemMenu;
             };

             var deselecionarItemDoMenu = function (itemMenu)
             {
                 var listaDeItensSelecionados = vm.formData.itensMenuSelecionados;
                 for (var i = 0; i < listaDeItensSelecionados.length; i++)
                 {
                     if (listaDeItensSelecionados[i].chaveItem == itemMenu.chaveItem)
                     {
                         listaDeItensSelecionados.splice(i, 1);
                     }
                 }

                 vm.formData.isAdministradorWeb = (vm.formData.itensMenuSelecionados.length == vm.listaItensMenuWeb);

             };

             var selecionarItemDoMenu = function (itemMenu)
             {
                 var listaDeItensSelecionados = vm.formData.itensMenuSelecionados;
                 for (var i = 0; i < listaDeItensSelecionados.length; i++)
                 {
                     if (listaDeItensSelecionados[i].chaveItem == itemMenu.chaveItem)
                     {
                         return;
                     }
                 }
                 vm.formData.itensMenuSelecionados.push(angular.copy(itemMenu));
                 vm.formData.isAdministradorWeb = (vm.formData.itensMenuSelecionados.length == vm.listaItensMenuWeb);

             };

             vm.changeMenuItem = function (menuItem, isChecked)
             {
                 var chaveItem = menuItem.chaveItem;
                 if (isChecked)
                 {
                     selecionarItemDoMenu(menuItem);
                     for (var i = 0; i < vm.listaItensMenuWeb.length; i++)
                     {
                         var itemAtual = vm.listaItensMenuWeb[i];
                         if (itemAtual.itemPai == chaveItem)
                         {
                             selecionarItemDoMenu(itemAtual);
                             vm.changeMenuItem(itemAtual, isChecked);
                         }
                     }
                 }
                 else
                 {
                     deselecionarItemDoMenu(menuItem);
                     for (var i = 0; i < vm.listaItensMenuWeb.length; i++)
                     {
                         var itemAtual = vm.listaItensMenuWeb[i];
                         if (itemAtual.itemPai == chaveItem)
                         {
                             deselecionarItemDoMenu(itemAtual);
                             vm.changeMenuItem(itemAtual, isChecked);
                         }
                     }
                 }
             };

             vm.loadListOfUsuario = function ()
             {
                 document.location.href = 'index.php?tipo=lists&page=usuario';
             };

             vm.addNewUsuario = function ()
             {
                 document.location.href = document.location.href;
             };

         }
         catch (ex)
         {
             ApplicationUtil.showErrorMessage(ex.message, 'Erro no controller formUsuarioController', vm);
         }
     }]
);