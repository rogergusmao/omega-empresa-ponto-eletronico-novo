import { MapaTemplate } from './uiTemplate/mapa/MapaTemplate';
import { Default } from './uiTemplate/mapa/Default';
import { Light } from './uiTemplate/mapa/Light';

export
{
    MapaTemplate,
    Default,
    Light    
}