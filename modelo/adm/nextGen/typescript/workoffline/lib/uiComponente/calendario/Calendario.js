"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var React = require("react");
var Moment = require("moment");
var uiComponente_1 = require("lib/uiComponente");
var geral_1 = require("lib/geral");
var Calendario = (function (_super) {
    __extends(Calendario, _super);
    function Calendario(props, reactContext) {
        var _this = _super.call(this, props || {}, reactContext) || this;
        _this.jQuerySelector = "." + Calendario.INTERNAL_CONTAINER_CLASSNAME;
        if (_this.props.addResizeListener) {
            _this.addResizeListener();
        }
        return _this;
    }
    Calendario.prototype.setDefaultOptions = function () {
        var defaultOptions = {
            columnFormat: 'ddd',
            defaultView: 'agendaWeek',
            timeFormat: 'hh:mm',
            editable: false,
            selectable: true,
            allDaySlot: false,
            themeSystem: 'bootstrap3',
            nowIndicator: true,
            displayEventTime: true,
            eventOverlap: false,
            slotEventOverlap: false,
            selectOverlap: false,
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            contentHeight: 'auto',
            now: Moment(new Date()),
            views: {
                month: {
                    titleFormat: 'MMMM YYYY',
                    columnFormat: 'ddd'
                },
                day: {
                    titleFormat: 'DD MMMM YYYY'
                },
                week: {
                    titleFormat: "DD MMMM YYYY",
                    titleRangeSeparator: '-'
                }
            }
        };
        this.calendarInputOptions = defaultOptions;
        this.setDefaultEventListeners();
    };
    Calendario.prototype.setDefaultEventListeners = function () {
        if (this.calendarInputOptions == null) {
            this.calendarInputOptions = {};
        }
        if (this.props.onEventClick) {
            this.calendarInputOptions.eventClick = this.props.onEventClick.bind(this);
        }
        if (this.props.onEventMouseOver) {
            this.calendarInputOptions.eventMouseover = this.props.onEventMouseOver.bind(this);
        }
        if (this.props.onCalendarViewChange) {
            this.calendarInputOptions.viewRender = this.props.onCalendarViewChange.bind(this);
        }
    };
    Calendario.prototype.fixLayout = function () {
    };
    Calendario.prototype.addResizeListener = function () {
        var _this = this;
        $(window).resize(function () {
            _this.fixLayout();
            _this.setState({ resize: true });
        });
    };
    Calendario.prototype.render = function () {
        if (this.props.setDefaultOptions) {
            this.setDefaultOptions();
        }
        if (!geral_1.Helper.isNullOrEmpty(this.props.titulo)) {
            return (<div className="box">
                    <div className="box-header">
                        <div className="title">
                            <i className="icon-group"/> {this.props.titulo}
                        </div>
                    </div>
                    <div className={"box-content " + Calendario.INTERNAL_CONTAINER_CLASSNAME}></div>
                </div>);
        }
        else {
            return <div className={"" + Calendario.INTERNAL_CONTAINER_CLASSNAME}></div>;
        }
    };
    Calendario.prototype.setDateRange = function (initialDate, finalDate) {
        this.calendarInputOptions.visibleRange =
            {
                start: initialDate,
                end: finalDate
            };
        this.setState({ dateRangeChanged: true });
    };
    Calendario.prototype.limparEventos = function () {
        this.calendarEvents = new Array();
        this.setState({ eventsCleared: true });
    };
    Calendario.prototype.adicionarEventos = function (events) {
        this.calendarEvents = events;
        this.setState({ eventsAdded: true });
    };
    Calendario.prototype.resizeCalendar = function () {
        this.setState({ resize: true });
    };
    Calendario.prototype.updateRenderFull = function () {
        $(this.jQuerySelector).fullCalendar('destroy');
        $(this.jQuerySelector).fullCalendar(this.calendarInputOptions);
    };
    Calendario.prototype.componentDidMount = function () {
        this.fixLayout();
        $(this.jQuerySelector).fullCalendar(this.calendarInputOptions);
    };
    Calendario.prototype.componentDidUpdate = function () {
        if (this.state.resize) {
            var height = $(this.jQuerySelector).height();
            var width = $(this.jQuerySelector).width();
            $(this.jQuerySelector).fullCalendar('option', 'aspectRatio', width / height);
        }
        if (this.state.eventsCleared) {
            $(this.jQuerySelector).fullCalendar('removeEvents', undefined);
        }
        if (this.state.eventsAdded) {
            $(this.jQuerySelector).fullCalendar('renderEvents', this.calendarEvents);
        }
        if (this.state.dateRangeChanged) {
            $(this.jQuerySelector).fullCalendar('option', 'visibleRange', this.calendarInputOptions.visibleRange);
        }
        if (this.state.optionsChanged) {
            this.updateRenderFull();
        }
    };
    return Calendario;
}(uiComponente_1.ReactComponent));
Calendario.INTERNAL_CONTAINER_CLASSNAME = "calendar-internal-container";
exports.Calendario = Calendario;
