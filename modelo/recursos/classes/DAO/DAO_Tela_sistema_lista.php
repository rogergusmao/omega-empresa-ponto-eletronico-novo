<?php


        /*

        Arquivo gerado através de gerador de código em 12/05/2018 as 15:01:34.
        Para que o arquivo não seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

        Tabela correspondente: tela_sistema_lista
        Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

        */
        


class DAO_Tela_sistema_lista extends Generic_DAO
                        {
                            const NOME_ENTIDADE = "tela_sistema_lista";

                            public static $nomeTabela = null;
                            public static $idSistemaTabela = false;
                            public static $databaseFieldsRelatedAttributes = null;
	                        public static $databaseFieldTypes = null;
	                        public static $databaseFieldNames = null;
	                        public static $listAliasTypes = null;
	                        public static $listAliasRelatedAttributes = null;
	                        
	                        public $id;
public $telaId;
public $sistemaListaId;
public $isPrincipalBoolean;
public $tipoCardinalidadeId;
public $corporacaoId;

public $labelId;
public $labelTelaId;
public $labelSistemaListaId;
public $labelIsPrincipalBoolean;
public $labelTipoCardinalidadeId;
public $labelCorporacaoId;

public static $arrPK = array("id");

public function __construct($configDAO = null)
        {
            parent::__construct($configDAO);
            static::__setStaticValues($this->database);           
        }

public static function idSistemaTabela($databaseObject = null){
            if(!(static::$idSistemaTabela))
            {
                static::$idSistemaTabela = EXTDAO_Sistema_tabela::getIdDaSistemaTabela(static::nomeTabela(), $databaseObject);
            }
            return static::$idSistemaTabela;
        }

public function getIdSistemaTabela(){
            return static::idSistemaTabela();
        }

public static function nomeTabela(){
            if(is_null(static::$nomeTabela))
            {
                static::$nomeTabela = "tela_sistema_lista";
            }
            return static::$nomeTabela;
        }

public function getNomeTabela(){
            return static::nomeTabela();
        }

public static function __setStaticValues($databaseObject = null)
                   {    
                        static::nomeTabela();
                        
                        static::idSistemaTabela($databaseObject);
                   
                        static::setDatabaseFieldNames();
                        static::setDatabaseFieldTypes();
                        static::setDatabaseFieldsRelatedAttributeNames();
                        
                        static::setListAliasTypes();
                        static::setListAliasRelatedAttributes();
                   }

public function getExpressaoLabel()
                   {
                        return "{id}";
                   }

public static function setDatabaseFieldNames()
        {
            if(is_null(static::$databaseFieldNames))
            {
                static::$databaseFieldNames = new stdClass();
                static::$databaseFieldNames->id = "id";
static::$databaseFieldNames->telaId = "tela_id_INT";
static::$databaseFieldNames->sistemaListaId = "sistema_lista_id_INT";
static::$databaseFieldNames->isPrincipalBoolean = "is_principal_BOOLEAN";
static::$databaseFieldNames->tipoCardinalidadeId = "tipo_cardinalidade_id_INT";
static::$databaseFieldNames->corporacaoId = "corporacao_id_INT";

            }
                      
        }

public static function setDatabaseFieldTypes()
        {
            if(is_null(static::$databaseFieldTypes))
            {
                static::$databaseFieldTypes = new stdClass();
                static::$databaseFieldTypes->id = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->telaId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->sistemaListaId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->isPrincipalBoolean = static::TIPO_VARIAVEL_BOOLEAN;
static::$databaseFieldTypes->tipoCardinalidadeId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->corporacaoId = static::TIPO_VARIAVEL_INTEGER;

            }
            
        }

public static function setDatabaseFieldsRelatedAttributeNames()
        {
            if(is_null(static::$databaseFieldsRelatedAttributes))
            {
                static::$databaseFieldsRelatedAttributes = new stdClass();
                static::$databaseFieldsRelatedAttributes->id = "id";
static::$databaseFieldsRelatedAttributes->tela_id_INT = "telaId";
static::$databaseFieldsRelatedAttributes->sistema_lista_id_INT = "sistemaListaId";
static::$databaseFieldsRelatedAttributes->is_principal_BOOLEAN = "isPrincipalBoolean";
static::$databaseFieldsRelatedAttributes->tipo_cardinalidade_id_INT = "tipoCardinalidadeId";
static::$databaseFieldsRelatedAttributes->corporacao_id_INT = "corporacaoId";

            }           
        }

public function setAllFieldsToHumanFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["tela_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["sistema_lista_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["is_principal_BOOLEAN"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["tipo_cardinalidade_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
}

public function setAllFieldsToDatabaseFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["tela_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["sistema_lista_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["is_principal_BOOLEAN"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["tipo_cardinalidade_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
}

  public function getFkObjTela() 
                {
                    if($this->objTela == null)
                    {                        
                        $this->objTela = new EXTDAO_Tela_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getTela_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objTela->clear();
                    }
                    elseif($this->objTela->getId() != $idFK)
                    {
                        $this->objTela->select($idFK);
                    }
                    return $this->objTela;
                }
  public function getFkObjSistema_lista() 
                {
                    if($this->objSistema_lista == null)
                    {                        
                        $this->objSistema_lista = new EXTDAO_Sistema_lista_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getSistema_lista_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objSistema_lista->clear();
                    }
                    elseif($this->objSistema_lista->getId() != $idFK)
                    {
                        $this->objSistema_lista->select($idFK);
                    }
                    return $this->objSistema_lista;
                }
  public function getFkObjTipo_cardinalidade() 
                {
                    if($this->objTipo_cardinalidade == null)
                    {                        
                        $this->objTipo_cardinalidade = new EXTDAO_Tipo_cardinalidade_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getTipo_cardinalidade_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objTipo_cardinalidade->clear();
                    }
                    elseif($this->objTipo_cardinalidade->getId() != $idFK)
                    {
                        $this->objTipo_cardinalidade->select($idFK);
                    }
                    return $this->objTipo_cardinalidade;
                }
  public function getFkObjCorporacao() 
                {
                    if($this->objCorporacao == null)
                    {                        
                        $this->objCorporacao = new EXTDAO_Corporacao_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getCorporacao_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objCorporacao->clear();
                    }
                    elseif($this->objCorporacao->getId() != $idFK)
                    {
                        $this->objCorporacao->select($idFK);
                    }
                    return $this->objCorporacao;
                }


public function setLabels()
        {$this->labelId = "id";
$this->labelTelaId = "telaidINT";
$this->labelSistemaListaId = "sistemalistaidINT";
$this->labelIsPrincipalBoolean = "isprincipalBOOLEAN";
$this->labelTipoCardinalidadeId = "tipocardinalidadeidINT";
$this->labelCorporacaoId = "corporacaoidINT";
}

public function __actionAdd($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                $this->setByObject($parameters);
                $this->setDataCadastroAndOffsetInSecondsFromUser($parameters);
                
                $this->formatarParaSQL();
                $msg = $this->insert(true);
                
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                return new Mensagem(null, I18N::getExpression("Tela sistema lista adicionado com sucesso."));
                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionEdit($parameters = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try {

                $this->setByObject($parameters);
                $this->setAllFieldsToHumanFormat();
                
                $this->formatarParaSQL();
                $msg = $this->updateByInstanceUsingParameters($this->id, $parameters, true);
                if($msg != null && $msg->erro())
                {
                     return $msg;
                }
        
                return new Mensagem(null, I18N::getExpression("Tela sistema lista editado com sucesso."));
            }
            catch(Exception $ex)
            {                
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionRemove($parameters = null)
        {
            $successulRemovalCount = 0;
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                foreach($parameters as $record)
                {
                    if(is_numeric($record->id))
                    {
                        $msg = $this->delete($record->id, true);
                        if($msg == null || $msg->ok())
                        {
                            $successulRemovalCount++;
                        }
                    }
                }
                
                if($successulRemovalCount > 0)
                {
                    $successMessage = count($successulRemovalCount) > 1 ? I18N::getExpression("{0} Tela sistema lista foram removidos com sucesso.", $successulRemovalCount) : I18N::getExpression("Tela sistema lista removido com sucesso.");
                    return new Mensagem(null, $successMessage);
                }
                else
                {
                    $errorMessage = count($parameters) > 1 ? I18N::getExpression("Falha ao remover Tela sistema lista.") : I18N::getExpression("Falha ao remover Tela sistema lista.");
                    return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, $errorMessage);                    
                }
            }
            catch (Exception $ex) 
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }

public function __getRecord($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }
            
            try
            {            
                $returnObject = new stdClass();            
                $whereClause = static::getWhereClauseForFilter($parameters->filterParameters);
                $query = "SELECT  id, tela_id_INT, sistema_lista_id_INT, is_principal_BOOLEAN, tipo_cardinalidade_id_INT, corporacao_id_INT FROM tela_sistema_lista {$whereClause}";
    
                $msg = $this->database->queryMensagem($query);                
                if ($msg == null)
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Não foi encontrado nenhum registro"));
                    }                    
                    $resultSet = $this->database->result;
                    $resultObject = static::mapResultSetDataToRecord($resultSet);
                    
                    $returnObject->formData = $resultObject;
                    $returnObject->comboBoxesData = null;
                    if($parameters->loadComboBoxesData)
                    {
                        $this->setByObject($resultObject);
                        $returnObject->comboBoxesData =  $this->getFormComboBoxesData();
                    }                    
    
                    
    
                    return new Mensagem_generica($returnObject);
                } 
                else 
                {
                    return $msg;
                }
                
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }


        
        public function __getFilterComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
                
        public function __getFormComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
        
        protected function getFormComboBoxesData()
        {
            $comboBoxesData = new stdClass();
            $listParameters = new stdClass();
            $listParameters->listMappingType = static::LIST_MAPPING_TYPE_COMBOBOX;
            
            
                    
                    $objTela = new EXTDAO_Tela();
                    $comboBoxesData->fieldTelaId = $objTela->__getList($listParameters);
                    
                    $objSistemaLista = new EXTDAO_Sistema_lista();
                    $comboBoxesData->fieldSistemaListaId = $objSistemaLista->__getList($listParameters);
                    
                    $objTipoCardinalidade = new EXTDAO_Tipo_cardinalidade();
                    $comboBoxesData->fieldTipoCardinalidadeId = $objTipoCardinalidade->__getList($listParameters);

            return $comboBoxesData;
        }
        
        


        
        public static function setListAliasTypes()
        {
            if(is_null(static::$listAliasTypes))
            {
                static::$listAliasTypes = new stdClass();
                static::$listAliasTypes->tela__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->sistema_lista__nome = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->tipo_cardinalidade__nome = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->tela_sistema_lista__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->tela_sistema_lista__corporacao_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->tela_sistema_lista__criador_usuario_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->tela_sistema_lista__is_principal_BOOLEAN = static::TIPO_VARIAVEL_BOOLEAN;
static::$listAliasTypes->tela_sistema_lista__seq_INT = static::TIPO_VARIAVEL_INTEGER;
            }        
        }
        
        public static function setListAliasRelatedAttributes()
        {
            if(is_null(static::$listAliasRelatedAttributes))
            {
                static::$listAliasRelatedAttributes = new stdClass();
                static::$listAliasRelatedAttributes->tela__id = "telaId";
static::$listAliasRelatedAttributes->sistema_lista__nome = "sistemaListaNome";
static::$listAliasRelatedAttributes->tipo_cardinalidade__nome = "tipoCardinalidadeNome";
static::$listAliasRelatedAttributes->tela_sistema_lista__id = "id";
static::$listAliasRelatedAttributes->tela_sistema_lista__corporacao_id_INT = "corporacaoId";
static::$listAliasRelatedAttributes->tela_sistema_lista__criador_usuario_id_INT = "criadorUsuarioId";
static::$listAliasRelatedAttributes->tela_sistema_lista__is_principal_BOOLEAN = "isPrincipalBoolean";
static::$listAliasRelatedAttributes->tela_sistema_lista__seq_INT = "seqInt";
            }         
        }

        public function __getList($parameters = null, $idCorporacao = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try
            {
                $filterParameters = new stdClass();
                if(isset($parameters->filterParameters))
                {
                    $filterParameters = $parameters->filterParameters;
                }
                                    
                            
            if(is_null($idCorporacao))
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            ListParameters::addFilterParameterToObject($filterParameters, "corporacaoId", $idCorporacao);           
                
                
                $sortingParameters = null;
                if(isset($parameters->sortingParameters))
                {
                    $sortingParameters = $parameters->sortingParameters;
                }
                
                $paginationParameters = null;
                if(isset($parameters->paginationParameters))
                {
                    $paginationParameters = $parameters->paginationParameters;
                }
                
                $listMappingType = static::getListMappingType($parameters->listMappingType);
                $mainTableAlias = "tsl";

                $whereClause = static::getWhereClauseForFilter($filterParameters, $mainTableAlias);
                $orderByClause = static::getOrderByClauseForFilter($sortingParameters, $mainTableAlias);
                $limitClause = static::getLimitClauseForFilter($paginationParameters);

                $queryWithoutLimit = "SELECT tsl.id FROM tela_sistema_lista tsl {$whereClause}";
                $query = "SELECT t.id AS tela__id, sl.nome AS sistema_lista__nome, tc.nome AS tipo_cardinalidade__nome, tsl.id AS tela_sistema_lista__id, tsl.corporacao_id_INT AS tela_sistema_lista__corporacao_id_INT, tsl.criador_usuario_id_INT AS tela_sistema_lista__criador_usuario_id_INT, tsl.is_principal_BOOLEAN AS tela_sistema_lista__is_principal_BOOLEAN, tsl.seq_INT AS tela_sistema_lista__seq_INT FROM tela_sistema_lista tsl LEFT JOIN tela t ON t.id = tsl.tela_id_INT LEFT JOIN sistema_lista sl ON sl.id = tsl.sistema_lista_id_INT LEFT JOIN tipo_cardinalidade tc ON tc.id = tsl.tipo_cardinalidade_id_INT {$whereClause} {$orderByClause} {$limitClause}";

                $msg = $this->database->queryMensagem($queryWithoutLimit);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                
                $totalNumberOfRecords = $this->database->rows();
                if($totalNumberOfRecords == 0)
                {
                    return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                }                        
                        
                $msg = $this->database->queryMensagem($query);
                
                //operacao realizaca com sucesso
                if ($msg == null) 
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                    }                        
                
                    $resultSet = $this->database->result;

                    $objReturn = new stdClass();
                    $objReturn->dataSet = static::mapResultSetDataToList($resultSet, $listMappingType, static::$listAliasTypes);

                    if(!Helper::isNullOrEmpty($paginationParameters->recordsPerPage) && !Helper::isNullOrEmpty($paginationParameters->currentPage))
                    {
                        $lastPage = ceil($totalNumberOfRecords / $paginationParameters->recordsPerPage);
                        $objReturn->paginationParameters = new PaginationParams($paginationParameters->recordsPerPage, $paginationParameters->currentPage, $lastPage);
                    }
                    
                    return new Mensagem_generica($objReturn);
                } 
                else
                {
                    return $msg;
                }                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }



public function getId()
            {
                return $this->id;
            }

public function getTelaId()
            {
                return $this->telaId;
            }

public function getTela_id_INT()
                {
                    return $this->telaId;
                }

public function getSistemaListaId()
            {
                return $this->sistemaListaId;
            }

public function getSistema_lista_id_INT()
                {
                    return $this->sistemaListaId;
                }

public function getIsPrincipalBoolean()
            {
                return $this->isPrincipalBoolean;
            }

public function getIs_principal_BOOLEAN()
                {
                    return $this->isPrincipalBoolean;
                }

public function getTipoCardinalidadeId()
            {
                return $this->tipoCardinalidadeId;
            }

public function getTipo_cardinalidade_id_INT()
                {
                    return $this->tipoCardinalidadeId;
                }

public function getCorporacaoId()
            {
                return $this->corporacaoId;
            }

public function getCorporacao_id_INT()
                {
                    return $this->corporacaoId;
                }



function setId($value)
            {
                $this->id = $value;
            }

function setTelaId($value)
            {
                $this->telaId = $value;
            }

public function setTela_id_INT($value)
                { 
                    $this->telaId = $value; 
                }

function setSistemaListaId($value)
            {
                $this->sistemaListaId = $value;
            }

public function setSistema_lista_id_INT($value)
                { 
                    $this->sistemaListaId = $value; 
                }

function setIsPrincipalBoolean($value)
            {
                $this->isPrincipalBoolean = $value;
            }

public function setIs_principal_BOOLEAN($value)
                { 
                    $this->isPrincipalBoolean = $value; 
                }

function setTipoCardinalidadeId($value)
            {
                $this->tipoCardinalidadeId = $value;
            }

public function setTipo_cardinalidade_id_INT($value)
                { 
                    $this->tipoCardinalidadeId = $value; 
                }

function setCorporacaoId($value)
            {
                $this->corporacaoId = $value;
            }

public function setCorporacao_id_INT($value)
                { 
                    $this->corporacaoId = $value; 
                }



public function clear()
        {$this->id = null;
$this->telaId = null;
if($this->objTela != null) unset($this->objTela);
$this->sistemaListaId = null;
if($this->objSistema_lista != null) unset($this->objSistema_lista);
$this->isPrincipalBoolean = null;
$this->tipoCardinalidadeId = null;
if($this->objTipo_cardinalidade != null) unset($this->objTipo_cardinalidade);
$this->corporacaoId = null;
if($this->objCorporacao != null) unset($this->objCorporacao);
if(is_array($this->arrEstadosCampos)){
                        unset($this->arrEstadosCampos);
                     $this->arrEstadosCampos = array();
                    }
                }

public function formatarParaSQL()
        {
if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->id)){
$this->id = $this->formatarIntegerParaComandoSQL($this->id);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->id);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->telaId)){
$this->telaId = $this->formatarIntegerParaComandoSQL($this->telaId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->telaId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->sistemaListaId)){
$this->sistemaListaId = $this->formatarIntegerParaComandoSQL($this->sistemaListaId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->sistemaListaId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->isPrincipalBoolean)){
$this->isPrincipalBoolean = $this->formatarBooleanParaComandoSQL($this->isPrincipalBoolean);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->isPrincipalBoolean);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->tipoCardinalidadeId)){
$this->tipoCardinalidadeId = $this->formatarIntegerParaComandoSQL($this->tipoCardinalidadeId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->tipoCardinalidadeId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->corporacaoId)){
$this->corporacaoId = $this->formatarIntegerParaComandoSQL($this->corporacaoId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->corporacaoId);
}

}

public function formatarParaExibicao()
        {if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->isPrincipalBoolean)){
$this->isPrincipalBoolean = $this->formatarBooleanParaExibicao($this->isPrincipalBoolean);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->isPrincipalBoolean);
}

}

function select($id, $idCorporacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }   

            $sql = "SELECT id, tela_id_INT, sistema_lista_id_INT, is_principal_BOOLEAN, tipo_cardinalidade_id_INT, corporacao_id_INT FROM tela_sistema_lista WHERE id = $id AND corporacao_id_INT = $idCorporacao";

            $msg = $this->database->queryMensagem($sql);
            $result = $this->database->result;

            if($msg != null && $msg->erro() )
            { 
            		$this->clear();
            		return $msg;
        	} 
        	else if($this->database->rows() == 0)
        	{
                return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nenhum registro foi encontrado."));
            }
        

            $row = $this->database->fetchArray(MYSQLI_NUM);
            
            		$this->id = $row[0];
		$this->telaId = $row[1];
		$this->sistemaListaId = $row[2];
		$this->isPrincipalBoolean = $row[3];
		$this->tipoCardinalidadeId = $row[4];

            
        }

public function delete($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (is_null($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }

            $sql = "DELETE FROM tela_sistema_lista WHERE id= $id AND corporacao_id_INT = $idCorporacao";
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                    && $sincronizar
                        && static::idSistemaTabela() != null
                            && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_REMOVE,
                                null,
                                null,
                                $id,
                                $this->database);
            }

            return $msg;
        }

public function insert($sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null, $opcoes = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            
            
            if (is_null($opcoes))
            {
                $opcoes = $this->opcoesDAO;
            }

            if (!is_numeric($this->id))
            {
                $this->id = "null";
                if ($opcoes == null || $opcoes[Database::OPCAO_GERAR_ID] != false) 
                {
                    $this->id = EXTDAO_Sistema_sequencia::gerarId(static::nomeTabela());                    
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }    
                    else if($msg == null)
                    {
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
                else 
                {
                    $this->id = null;
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao));
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }
                    else if($msg == null)
                    {
                        $this->id = $this->database->getLastInsertId();
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
            }
            else 
            {
                $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }                        
                else if($msg == null)
                {
                   $msg = new Mensagem_token();
                   $msg->mValor = $this->id;
                }
            }
            if($msg == null || $msg->ok())
            {                
                if($sincronizar
                    && static::idSistemaTabela() != null
                    && isset($this->id)){
                    
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_INSERT,
                                null,
                                null,
                                $this->id,
                                $this->database); 
                	}
            } 
            else
            {
                $this->id = null;
            }
            
            return $msg;
            
        }

public function getSQLInsert($idCorporacao){ 

return "INSERT INTO tela_sistema_lista (id, tela_id_INT, sistema_lista_id_INT, is_principal_BOOLEAN, tipo_cardinalidade_id_INT, corporacao_id_INT) VALUES ( $this->id ,  $this->telaId ,  $this->sistemaListaId ,  $this->isPrincipalBoolean ,  $this->tipoCardinalidadeId ,  $idCorporacao ) "; 

}




        
        public function updateByParameters($id, $objParametros, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao))
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();

            
                
                if (isset($objParametros->telaId)) 
                {
                    $arrUpdateFields[] = " tela_id_INT = {$objParametros->telaId} ";
                }


                
                if (isset($objParametros->sistemaListaId)) 
                {
                    $arrUpdateFields[] = " sistema_lista_id_INT = {$objParametros->sistemaListaId} ";
                }


                
                if (isset($objParametros->isPrincipalBoolean)) 
                {
                    $arrUpdateFields[] = " is_principal_BOOLEAN = {$objParametros->isPrincipalBoolean} ";
                }


                
                if (isset($objParametros->tipoCardinalidadeId)) 
                {
                    $arrUpdateFields[] = " tipo_cardinalidade_id_INT = {$objParametros->tipoCardinalidadeId} ";
                }



            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE tela_sistema_lista SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";

            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null)
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function update($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($this->telaId)) 
                {                                      
                    $arrUpdateFields[] = " tela_id_INT = {$this->telaId} ";
                }


                
                if (isset($this->sistemaListaId)) 
                {                                      
                    $arrUpdateFields[] = " sistema_lista_id_INT = {$this->sistemaListaId} ";
                }


                
                if (isset($this->isPrincipalBoolean)) 
                {                                      
                    $arrUpdateFields[] = " is_principal_BOOLEAN = {$this->isPrincipalBoolean} ";
                }


                
                if (isset($this->tipoCardinalidadeId)) 
                {                                      
                    $arrUpdateFields[] = " tipo_cardinalidade_id_INT = {$this->tipoCardinalidadeId} ";
                }


            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE tela_sistema_lista SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function updateByInstanceUsingParameters($id, $parameters, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($parameters->telaId)) 
                {                                      
                    $arrUpdateFields[] = " tela_id_INT = {$this->telaId} ";
                }
                
                if (isset($parameters->sistemaListaId)) 
                {                                      
                    $arrUpdateFields[] = " sistema_lista_id_INT = {$this->sistemaListaId} ";
                }
                
                if (isset($parameters->isPrincipalBoolean)) 
                {                                      
                    $arrUpdateFields[] = " is_principal_BOOLEAN = {$this->isPrincipalBoolean} ";
                }
                
                if (isset($parameters->tipoCardinalidadeId)) 
                {                                      
                    $arrUpdateFields[] = " tipo_cardinalidade_id_INT = {$this->tipoCardinalidadeId} ";
                }
            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE tela_sistema_lista SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }

}

?>
