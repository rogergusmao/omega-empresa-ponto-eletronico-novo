"use strict";
exports.__esModule = true;
var SearchableModel = (function () {
    function SearchableModel(nomeEntidade, isVisible, isSelectable, selectableCssClass, selectedCssClass) {
        if (isVisible === void 0) { isVisible = true; }
        if (isSelectable === void 0) { isSelectable = true; }
        if (selectableCssClass === void 0) { selectableCssClass = SearchableModel.DEFAULT_SELECTABLE_ITEM_CLASS; }
        if (selectedCssClass === void 0) { selectedCssClass = SearchableModel.DEFAULT_SELECTED_ITEM_CLASS; }
        //nome da entidade
        this.nomeEntidade = nomeEntidade;
        this.hasChanged = false;
        this.isVisible = isVisible;
        this.isSelectable = isSelectable;
        this.selectableCssClass = selectableCssClass;
        this.selectedCssClass = selectedCssClass;
    }
    SearchableModel.prototype.getIsVisible = function () {
        return this.isVisible;
    };
    SearchableModel.prototype.getIsSelectable = function () {
        return this.isSelectable;
    };
    SearchableModel.prototype.setIsSelectable = function (isSelectable) {
        this.isSelectable = isSelectable;
    };
    SearchableModel.prototype.getSelectableCssClass = function () {
        return this.selectableCssClass;
    };
    SearchableModel.prototype.setSelectableCssClass = function (selectableCssClass) {
        this.selectableCssClass = selectableCssClass;
    };
    SearchableModel.prototype.getSelectedCssClass = function () {
        return this.selectedCssClass;
    };
    SearchableModel.prototype.setSelectedCssClass = function (selectedCssClass) {
        this.selectedCssClass = selectedCssClass;
    };
    SearchableModel.prototype.getPrimaryKeyString = function (forceReload) {
        forceReload = (typeof forceReload == 'undefined') ? false : forceReload;
        if (forceReload || this.primaryKeyString == null) {
            this.primaryKeyString = '';
            var attributes = Object.getOwnPropertyNames(this);
            for (var i = 0; i < attributes.length; i++) {
                /*
                if (this[attributes[i]] instanceof IBaseAttribute && this[attributes[i]].getPrimaryKey())
                {
                    this.primaryKeyString += this[attributes[i]].getValue() + ",";
                }
                */
            }
            if (this.primaryKeyString.length > 0) {
                this.primaryKeyString = this.primaryKeyString.substr(0, this.primaryKeyString.length - 1);
            }
        }
        return this.primaryKeyString;
    };
    SearchableModel.prototype.setVisible = function () {
        this.isVisible = true;
    };
    SearchableModel.prototype.setInvisible = function () {
        this.isVisible = false;
    };
    SearchableModel.prototype.doActionsAfterEdit = function () {
        this.setSearchableString();
    };
    SearchableModel.prototype.getSearchableString = function () {
        return this.searchableString;
    };
    SearchableModel.prototype.setSearchableString = function () {
        this.searchableString = '';
        var attributes = Object.getOwnPropertyNames(this);
        for (var i = 0; i < attributes.length; i++) {
            /*
            if (this[attributes[i]] instanceof IBaseAttribute && this[attributes[i]].getSearchable())
            {
                this.searchableString += this[attributes[i]].getValue() + "|";
            }
            */
        }
    };
    SearchableModel.prototype.setAttributeValue = function (attr, value) {
        attr.setValue(value);
        this.hasChanged = true;
    };
    SearchableModel.prototype.markAsChanged = function () {
        this.hasChanged = true;
    };
    SearchableModel.dynamicSort = function (property) {
        var sortOrder = 1;
        if (property[0] === "-") {
            sortOrder = -1;
            property = property.substr(1);
        }
        return function (a, b) {
            var result = (a[property].getValue() < b[property].getValue()) ? -1 : (a[property].getValue() > b[property].getValue()) ? 1 : 0;
            return result * sortOrder;
        };
    };
    SearchableModel.dynamicSortMultiple = function () {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        var props = arguments[0];
        return function (obj1, obj2) {
            var i = 0, result = 0, numberOfProperties = props.length;
            while (result === 0 && i < numberOfProperties) {
                result = SearchableModel.dynamicSort(props[i])(obj1, obj2);
                i++;
            }
            return result;
        };
    };
    return SearchableModel;
}());
SearchableModel.DEFAULT_SELECTABLE_ITEM_CLASS = 'selectable';
SearchableModel.DEFAULT_SELECTED_ITEM_CLASS = 'selected';
exports.SearchableModel = SearchableModel;
