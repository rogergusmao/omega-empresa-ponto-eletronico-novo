<?php //@@NAO_MODIFICAR

/*
*
* -------------------------------------------------------
* NOME DA CLASSE:     EXTDAO_Acesso
* NOME DA CLASSE DAO: DAO_Acesso
* DATA DE GERAï¿½ï¿½O:    08.02.2010
* ARQUIVO:            EXTDAO_Acesso.php
* TABELA MYSQL:       acesso
* BANCO DE DADOS:     dep_pesquisas
* -------------------------------------------------------
*
*/

// **********************
// DECLARAï¿½ï¿½O DA CLASSE
// **********************

class EXTDAO_Acesso extends DAO_Acesso
{
    public function __construct($configDAO = null)
    {

        parent::__construct($configDAO);

        $this->nomeClasse = "EXTDAO_Acesso";
        $this->setLabels();
    }

    public function getListaPessoas()
    {

        try
        {

            $arrRetorno = array();

            $objRetorno = new stdClass();
            $objRetorno->nome = "Eduardo";
            $objRetorno->email = "eduardo@omegasoftware.com.br";
            $objRetorno->idade = 29;
            $arrRetorno[] = clone $objRetorno;

            $objRetorno->nome = "Roger";
            $objRetorno->email = "roger@omegasoftware.com.br";
            $objRetorno->idade = 28;
            $arrRetorno[] = clone $objRetorno;

            $objRetorno->nome = "Joï¿½o";
            $objRetorno->email = "joao@omegasoftware.com.br";
            $objRetorno->idade = 30;
            $arrRetorno[] = clone $objRetorno;

            $objRetorno->nome = "Josï¿½";
            $objRetorno->email = "jose@omegasoftware.com.br";
            $objRetorno->idade = 29;
            $arrRetorno[] = clone $objRetorno;

            $objRetorno->nome = "Paulo";
            $objRetorno->email = "paulo@omegasoftware.com.br";
            $objRetorno->idade = 29;
            $arrRetorno[] = clone $objRetorno;

            $retorno = new WebserviceResponse(true, $arrRetorno);
            $retorno->printJson();
        }
        catch (Exception $ex)
        {

            $retorno = new WebserviceResponse(false, null, $ex->getMessage());
            $retorno->printJson();
        }
    }

    public function getListaPaises()
    {

        try
        {

            $arrRetorno = array();

            $objRetorno = new stdClass();
            $objRetorno->nome = "Austrï¿½lia";
            $objRetorno->codigo = "AUS";
            $arrRetorno[] = clone $objRetorno;

            $objRetorno->nome = "Brasil";
            $objRetorno->codigo = "BRA";
            $arrRetorno[] = clone $objRetorno;

            $objRetorno->nome = "Argentina";
            $objRetorno->codigo = "ARG";
            $arrRetorno[] = clone $objRetorno;

            $objRetorno->nome = "Canadï¿½";
            $objRetorno->codigo = "CAN";
            $arrRetorno[] = clone $objRetorno;

            $objRetorno->nome = "Estados Unidos";
            $objRetorno->codigo = "USA";
            $arrRetorno[] = clone $objRetorno;

            $retorno = new WebserviceResponse(true, $arrRetorno);
            $retorno->printJson();
        }
        catch (Exception $ex)
        {

            $retorno = new WebserviceResponse(false, null, $ex->getMessage());
            $retorno->printJson();
        }
    }

    public function filterClientes()
    {
        $parameters = json_decode(file_get_contents('php://input'));

        $filterData = $parameters->filterData;
        $paginationParameters = $parameters->paginationParameters;
        $sortingParamters = $parameters->sortingParameters;

        $page = $paginationParameters->currentPage;
        $regsPerPage = $paginationParameters->recordsPerPage;

        $offset = ($page - 1) * $regsPerPage;
        $arrGridData = array();
        for ($i = 0; $i < $regsPerPage; $i++)
        {
            $objGridData = new stdClass();
            $objGridData->primaryKey = $offset + $i;
            $objGridData->id = $offset + $i;
            $objGridData->nome = "Eduardo Caetano de Olveira Alves";
            $objGridData->valor = 0.78;
            $objGridData->dataNascimento = mktime(0, 0, 0, 7, 27, 1986);
            $objGridData->sexo = "M";
            $objGridData->allowEdit = true;
            $objGridData->allowRemove = true;
            $objGridData->isChecked = false;

            $arrGridData[] = $objGridData;
        }

        $objReturn = new stdClass();
        $objReturn->gridData = $arrGridData;
        $objReturn->paginationParameters = new PaginationParams($regsPerPage, $page, 20);

        $retorno = new WebserviceResponse(true, $objReturn);
        $retorno->printJson();
    }

    public function uploadDocumentoUploadFile()
    {
        $retorno = new FileUploadResponse(true, rand(0, 1000));
        $retorno->printJson();
    }

    public function removeDocumentoUploadFile()
    {
        $parameters = json_decode(file_get_contents('php://input'));
        $fileUniqueId = $parameters->fileUniqueId;

        $retorno = new FileUploadResponse(true, $fileUniqueId);
        $retorno->printJson();
    }

    public function acoesPrimeiroLoginDoDia()
    {
        //remove todos os arquivos temporarios antigos
        Helper::removerArquivosTemporarios();
    }

    public function acoesTodosOsLogins()
    {


        return true;
    }

    public function gravarLogin($idUsuario)
    {
        $this->setUsuario_id_INT($idUsuario);
        $this->setData_login_OFFSEC(Helper::getTimezoneOffsetSec());
        $this->setData_login_SEC(Helper::getUTCNowSec());

        $this->formatarParaSQL();
        $this->insert();

        return true;
    }

    public function setLabels()
    {

        $this->label_id = I18N::getExpression("Id");
        $this->label_usuario_id_INT = I18N::getExpression("Usuï¿½rio");
        $this->label_data_login_DATETIME = I18N::getExpression("Data do Login");
        $this->label_data_logout_DATETIME = I18N::getExpression("Data do Logout");
    }

    public function factory()
    {

        return new EXTDAO_Acesso();
    }
}

