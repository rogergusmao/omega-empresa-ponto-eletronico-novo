/*

Arquivo gerado atrav�s de gerador de c�digo em 21/01/2018 as 18:27:39.
Para que o arquivo n�o seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

Tabela correspondente: ponto
Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

*/

omegaApp.generatedFormPontoController = function ($scope, $rootScope, $element, $window, $interval, $timeout, dataService, Upload, vm)
{
    try
    {
        vm.rootControllerElement = $element;
        vm.fieldsTypes = {};

        vm.isModalView = false;
        vm.isEdition = false;
        vm.entityIdForEdition = null;

        vm.messages = {
            error: [],
            warning: [],
            info: [],
            success: []
        };

        vm.formData = {
            pessoaId: null,
            foto: null,
            empresaId: null,
            profissaoId: null,
            usuarioId: null,
            isEntradaBoolean: false,
            latitudeInt: null,
            longitudeInt: null,
            tipoPontoId: null
        };

        vm.disabledFields = {
            fieldPessoaId: false,
            fieldFoto: false,
            fieldEmpresaId: false,
            fieldProfissaoId: false,
            fieldUsuarioId: false,
            fieldIsEntradaBoolean: false,
            fieldLatitudeInt: false,
            fieldLongitudeInt: false,
            fieldTipoPontoId: false
        };

        vm.requiredFields = {
            fieldPessoaId: true,
            fieldFoto: true,
            fieldEmpresaId: true,
            fieldProfissaoId: true,
            fieldUsuarioId: true,
            fieldIsEntradaBoolean: true,
            fieldLatitudeInt: true,
            fieldLongitudeInt: true,
            fieldTipoPontoId: true
        };

        vm.relatedEntities =
            {};

        vm.fieldsParameters = {
            fieldPessoaId: {remoteData: null},
            fieldFoto: null,
            fieldEmpresaId: {remoteData: null},
            fieldProfissaoId: {remoteData: null},
            fieldUsuarioId: {remoteData: null},
            fieldIsEntradaBoolean: null,
            fieldLatitudeInt: null,
            fieldLongitudeInt: null,
            fieldTipoPontoId: {remoteData: null}
        };

        vm.arrForeignKeys = ['pessoaId', 'empresaId', 'profissaoId', 'idUsuario', 'tipoPontoId', 'corporacaoId'];

        var relationshipObject = {
            pessoaId: 'fieldPessoaId',
            empresaId: 'fieldEmpresaId',
            profissaoId: 'fieldProfissaoId',
            usuarioId: 'fieldUsuarioId',
            tipoPontoId: 'fieldTipoPontoId'
        };

        vm.isInitialLoadFinished = false;
        var initialLoading = function ()
        {
            $timeout(function ()
            {
                if (AngularControllerUtil.isEditionMode(vm))
                {

                    var filterParameters = AngularControllerUtil.getFilterParametersForEdition(vm);
                    dataService.getPonto(filterParameters).then(function (responseContent)
                    {
                        if (RemoteDataUtil.validateRemoteResponse(responseContent, [PROTOCOLO_SISTEMA.OPERACAO_REALIZADA_COM_SUCESSO]))
                        {
                            var responseObject = RemoteDataUtil.getResponseObject(responseContent);
                            RemoteDataUtil.refactorInitialFormLoadRemoteResponseData(vm, responseObject, relationshipObject);
                            OmegaFormUtil.expandSectionPanels(vm);
                        }
                        else
                        {
                            RemoteDataUtil.defaultRemoteErrorHandling(responseContent, vm);
                        }

                        $timeout(function ()
                        {
                            vm.isInitialLoadFinished = true;
                        }, 100);

                    });
                }
                else
                {
                    dataService.getAllComboBoxesDataForPontoForm().then(function (returnContent)
                    {
                        if (RemoteDataUtil.validateRemoteResponse(returnContent))
                        {
                            var responseObject = RemoteDataUtil.getResponseObject(returnContent);
                            RemoteDataUtil.refactorInitialFormLoadRemoteResponseData(vm, responseObject, relationshipObject);
                        }
                        else
                        {
                            LogUtil.logRemoteResponseError(returnContent);
                        }

                        $timeout(function ()
                        {
                            vm.isInitialLoadFinished = true;
                        }, 100);

                    });
                }

            }, 200);

        };

        vm.submitForm = function (formInstance)
        {
            if ($rootScope.validateForm(formInstance, vm))
            {
                var postData = RemoteDataUtil.refactorRemoteRequest(vm.formData, vm.relatedEntities);
                if (AngularControllerUtil.isEditionMode(vm))
                {
                    dataService.editPonto(postData).then(function (returnContent)
                    {
                        if (RemoteDataUtil.validateRemoteResponse(returnContent))
                        {
                            var successMessage = RemoteDataUtil.getResponseMessageString(returnContent);
                            $rootScope.defaultAfterUpdateAction(vm, 'after-ponto-update-template.html', successMessage);
                        }
                        else
                        {
                            RemoteDataUtil.defaultRemoteErrorHandling(returnContent, vm);
                        }
                    });
                }
                else
                {
                    dataService.addPonto(postData).then(function (returnContent)
                    {
                        if (RemoteDataUtil.validateRemoteResponse(returnContent))
                        {
                            var successMessage = RemoteDataUtil.getResponseMessageString(returnContent);
                            $rootScope.defaultAfterInsertAction(vm, 'after-ponto-insert-template.html', successMessage);
                        }
                        else
                        {
                            RemoteDataUtil.defaultRemoteErrorHandling(returnContent, vm);
                        }
                    });
                }
            }

        };

        vm.openPessoaIdModal = function ()
        {
            var modalIndex = $rootScope.getNextModalIndex();
            $rootScope.registerModalInstance($rootScope.showModalDialog('forms', 'pessoa', modalIndex));

            //atualizando valores da lista
            var unregisterModal = $rootScope.$watch(function ()
            {
                return $rootScope.uibModalInstance[modalIndex]
            }, function ()
            {

                if ($rootScope.uibModalInstance[modalIndex] == null)
                {
                    vm.getListOfPessoaId();
                    vm.formData.pessoaId = null;
                    unregisterModal();
                }

            });

        };
        vm.openEmpresaIdModal = function ()
        {
            var modalIndex = $rootScope.getNextModalIndex();
            $rootScope.registerModalInstance($rootScope.showModalDialog('forms', 'empresa', modalIndex));

            //atualizando valores da lista
            var unregisterModal = $rootScope.$watch(function ()
            {
                return $rootScope.uibModalInstance[modalIndex]
            }, function ()
            {

                if ($rootScope.uibModalInstance[modalIndex] == null)
                {
                    vm.getListOfEmpresaId();
                    vm.formData.empresaId = null;
                    unregisterModal();
                }

            });

        };
        vm.openProfissaoIdModal = function ()
        {
            var modalIndex = $rootScope.getNextModalIndex();
            $rootScope.registerModalInstance($rootScope.showModalDialog('forms', 'profissao', modalIndex));

            //atualizando valores da lista
            var unregisterModal = $rootScope.$watch(function ()
            {
                return $rootScope.uibModalInstance[modalIndex]
            }, function ()
            {

                if ($rootScope.uibModalInstance[modalIndex] == null)
                {
                    vm.getListOfProfissaoId();
                    vm.formData.profissaoId = null;
                    unregisterModal();
                }

            });

        };
        vm.openUsuarioIdModal = function ()
        {
            var modalIndex = $rootScope.getNextModalIndex();
            $rootScope.registerModalInstance($rootScope.showModalDialog('forms', 'usuario', modalIndex));

            //atualizando valores da lista
            var unregisterModal = $rootScope.$watch(function ()
            {
                return $rootScope.uibModalInstance[modalIndex]
            }, function ()
            {

                if ($rootScope.uibModalInstance[modalIndex] == null)
                {
                    vm.getListOfUsuarioId();
                    vm.formData.usuarioId = null;
                    unregisterModal();
                }

            });

        };
        vm.openTipoPontoIdModal = function ()
        {
            var modalIndex = $rootScope.getNextModalIndex();
            $rootScope.registerModalInstance($rootScope.showModalDialog('forms', 'tipoPonto', modalIndex));

            //atualizando valores da lista
            var unregisterModal = $rootScope.$watch(function ()
            {
                return $rootScope.uibModalInstance[modalIndex]
            }, function ()
            {

                if ($rootScope.uibModalInstance[modalIndex] == null)
                {
                    vm.getListOfTipoPontoId();
                    vm.formData.tipoPontoId = null;
                    unregisterModal();
                }

            });

        };

        vm.getListOfPessoaId = function ()
        {
            if (!vm.isInitialLoadFinished)
            {
                return;
            }

            dataService.getListOfPessoaForPontoForm().then(function (returnContent)
            {
                if (RemoteDataUtil.validateRemoteResponse(returnContent))
                {
                    var remoteDataSet = RemoteDataUtil.getResponseObjectDataSetOrEmptyArray(returnContent);
                    vm.fieldsParameters.fieldPessoaId.remoteData = remoteDataSet;
                }
                else
                {
                    LogUtil.logRemoteResponseError(returnContent);
                }

            });

        };
        vm.getListOfEmpresaId = function ()
        {
            if (!vm.isInitialLoadFinished)
            {
                return;
            }

            dataService.getListOfEmpresaForPontoForm().then(function (returnContent)
            {
                if (RemoteDataUtil.validateRemoteResponse(returnContent))
                {
                    var remoteDataSet = RemoteDataUtil.getResponseObjectDataSetOrEmptyArray(returnContent);
                    vm.fieldsParameters.fieldEmpresaId.remoteData = remoteDataSet;
                }
                else
                {
                    LogUtil.logRemoteResponseError(returnContent);
                }

            });

        };
        vm.getListOfProfissaoId = function ()
        {
            if (!vm.isInitialLoadFinished)
            {
                return;
            }

            dataService.getListOfProfissaoForPontoForm().then(function (returnContent)
            {
                if (RemoteDataUtil.validateRemoteResponse(returnContent))
                {
                    var remoteDataSet = RemoteDataUtil.getResponseObjectDataSetOrEmptyArray(returnContent);
                    vm.fieldsParameters.fieldProfissaoId.remoteData = remoteDataSet;
                }
                else
                {
                    LogUtil.logRemoteResponseError(returnContent);
                }

            });

        };
        vm.getListOfUsuarioId = function ()
        {
            if (!vm.isInitialLoadFinished)
            {
                return;
            }

            dataService.getListOfUsuarioForPontoForm().then(function (returnContent)
            {
                if (RemoteDataUtil.validateRemoteResponse(returnContent))
                {
                    var remoteDataSet = RemoteDataUtil.getResponseObjectDataSetOrEmptyArray(returnContent);
                    vm.fieldsParameters.fieldUsuarioId.remoteData = remoteDataSet;
                }
                else
                {
                    LogUtil.logRemoteResponseError(returnContent);
                }

            });

        };
        vm.getListOfTipoPontoId = function ()
        {
            if (!vm.isInitialLoadFinished)
            {
                return;
            }

            dataService.getListOfTipoPontoForPontoForm().then(function (returnContent)
            {
                if (RemoteDataUtil.validateRemoteResponse(returnContent))
                {
                    var remoteDataSet = RemoteDataUtil.getResponseObjectDataSetOrEmptyArray(returnContent);
                    vm.fieldsParameters.fieldTipoPontoId.remoteData = remoteDataSet;
                }
                else
                {
                    LogUtil.logRemoteResponseError(returnContent);
                }

            });

        };

        vm.loadListOfPonto = function ()
        {
            document.location.href = 'index.php?tipo=lists&page=ponto';
        };

        vm.addNewPonto = function ()
        {
            document.location.href = document.location.href;
        };

        return {

            scope: this,
            initFunction: initialLoading

        };

    }
    catch (ex)
    {
        ApplicationUtil.showErrorMessage(ex.message, 'Erro no controller generatedFormPontoController', vm);
    }

}