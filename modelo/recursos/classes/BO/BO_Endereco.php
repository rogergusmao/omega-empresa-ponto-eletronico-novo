<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BO_Assinatura
 *
 * @author home
 */
class BO_Endereco
{

    public static function getEnderecos()
    {
        
        $db = new Database();
        $paises = EXTDAO_Pais::getPaises($db, false);
        $bairros = null;
        $cidades = null;
        $estados = null;
        $idBairro = null;
        $idCidade = null;
        $idEstado = null;
        $idPais = EXTDAO_Pais::getIdPeloNome(Helper::POSTGET('pais'), $db);
        if(strlen($idPais)){
            $estados = EXTDAO_Uf::getEstados($idPais, $db, false);
            $idEstado = EXTDAO_Uf::getIdPeloNome(Helper::POSTGET('estado'), $db);    
            if(strlen($idEstado)){
                $cidades = EXTDAO_Cidade::getCidades($idEstado, $db, false);
                $idCidade = EXTDAO_Cidade::getIdPeloNome(Helper::POSTGET('cidade'), $db);    
                if(strlen($idCidade)){
                    $bairros = EXTDAO_Bairro::getBairros($idCidade, $db, false);
                }
            }
        }
        
        
        $protocolo = new Protocolo_endereco();
        $protocolo->idBairro = $idBairro;
        $protocolo->bairros = $bairros;
        $protocolo->idCidade = $idCidade;
        $protocolo->cidades = $cidades;
        $protocolo->idEstado = $idEstado;
        $protocolo->estados = $estados;
        $protocolo->idPais = $idPais;
        $protocolo->paises = $paises;
        
        return new Mensagem_protocolo($protocolo);
    }

    
    public static function factory(){
        return new BO_Endereco();
    }
}
