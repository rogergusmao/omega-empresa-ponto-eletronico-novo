<?php
/*

Arquivo gerado através de gerador de código em 04/06/2017 as 23:37:04.
Para que o arquivo não seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

Tabela correspondente: usuario
Sobrescrita de dados: ---SOBRESCRITA_NAO_AUTORIZADA---

*/
?>

<!--Definição da View Angular JS -->
<?
$objForm = new FormEnvironment("mainForm", "formUsuarioController", "vm");
$objForm->setLayoutOneColumn();

?>

<div class='row' id='content-wrapper' ng-controller="<?= $objForm->getAngularControllerAttributeValue() ?>">

    <?= $objForm->setModalEnvironmentIfApplicable(); ?>
    <?= $objForm->setEditionVariablesIfApplicable(); ?>

    <!--Definição do template do dialog de e-mail já existente-->
    <script type="text/ng-template" id="email-ja-existente-template.html">
        <div class="modal-header">
            <h4 class="modal-title">
                <span class="glyphicon"></span>
                <?= I18N::getExpression("O endereço de e-mail informado já é utilizado por outro usuário, favor informar outro endereço de e-mail."); ?>
            </h4>
        </div>
        <div class="modal-body">
            <span class="help-block"><?= I18N::getExpression("Qual ação quer tomar?"); ?></span>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" ng-click="vm.onDialogEmailJaExistenteTrocarEmailClick()">
                <?= I18N::getExpression("Utilizar outro e-mail"); ?>
            </button>
            <button type="button" class="btn btn-danger" ng-click="vm.onDialogEmailJaExistenteSairClick()">
                <?= I18N::getExpression("Sair do Cadastro do Usuário"); ?>
            </button>
        </div>
    </script>

    <!--Definição do template do dialog exibido após inserção -->
    <script type="text/ng-template" id="after-usuario-insert-template.html">
        <div class="modal-header">
            <h4 class="modal-title">
                <span class="glyphicon"></span>
                <?= I18N::getExpression("Usuário adicionado com sucesso!"); ?>
            </h4>
        </div>
        <div class="modal-body">
            <span class="help-block"><?= I18N::getExpression("O que deseja fazer agora?"); ?></span>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" ng-click="vm.loadListOfUsuario()">
                <?= I18N::getExpression("Listar usuários"); ?>
            </button>
            <button type="button" class="btn" ng-click="closeMessageDialog()">
                <?= I18N::getExpression("Permanecer na tela"); ?>
            </button>
            <button type="button" class="btn btn-danger" ng-click="vm.addNewUsuario()">
                <?= I18N::getExpression("Adicionar usuário"); ?>
            </button>
        </div>
    </script>

    <!--Definição do template do exibido dialog após update -->
    <script type="text/ng-template" id="after-usuario-update-template.html">
        <div class="modal-header">
            <h4 class="modal-title">
                <span class="glyphicon"></span>
                <?= I18N::getExpression("Usuário alterado com sucesso!"); ?>
            </h4>
        </div>
        <div class="modal-body">
            <span class="help-block"><?= I18N::getExpression("O que deseja fazer agora?"); ?></span>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" ng-click="vm.loadListOfUsuario()">
                <?= I18N::getExpression("Listar usuários"); ?>
            </button>
            <button type="button" class="btn" ng-click="closeMessageDialog()">
                <?= I18N::getExpression("Permanecer na tela"); ?>
            </button>
            <button type="button" class="btn btn-success" ng-click="vm.addNewUsuario()">
                <?= I18N::getExpression("Adicionar usuário"); ?>
            </button>
        </div>
    </script>

    <div class='col-xs-12'>

        <div class='row'>
            <div class='col-sm-12'>
                <div class='page-header'>
                    <h1 class='pull-left'>
                        <i class='icon-edit'></i>
                        <span>
                            <?= I18N::getExpression("Cadastro de Usuário"); ?>
                        </span>
                    </h1>
                    <div class='pull-right close-button-container'>
                        <?= $objForm->renderCloseModalButtonIfExists(); ?>
                    </div>
                </div>
            </div>
        </div>

        <?= $objForm->getFormDebugPanel(); ?>

        <div class='row'>
            <div class='col-sm-12'>
                <div class='box'>
                    <div class='box-header blue-background'>
                        <div class='title'>
                            <div class='icon-edit'></div>
                            <?= I18N::getExpression("Dados do Usuário"); ?>
                        </div>
                        <div class='actions'>
                            <a class="btn btn-xs btn-link " href="javascript: void(0);"
                               ng-click="expandOrShrinkPanels(<?= $objForm->getFormName() ?>)">
                                <i ng-class="getExpandOrShinkPanelsButtonClass(<?= $objForm->getFormName() ?>)"></i>
                            </a>
                        </div>
                    </div>
                    <div class='box-content'>

                        <form name="<?= $objForm->getFormName() ?>" class="form form-horizontal"
                              style="margin-bottom: 0;" novalidate>

                            <?php
                            $objCampo = $objForm->getTextInputInstance("email");
                            $objCampo->setLabel("Email");
                            $objCampo->setDefaultValues();
                            $objCampo->setPlaceHolder("Informe o e-mail.");
                            $objCampo->addValidation(new MaxLengthValidation(255, "O tamanho máximo do e-mail deve ser de {0} caracteres."));
                            $objCampo->addValidation(new RequiredValidation("O e-mail é de preenchimento obrigatório."));
                            //$objCampo->addValidation(new EmailValidation("Informe um endereço de e-mail válido."));
                            ?>

                            <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                 ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                                <?= $objCampo->renderLabel(); ?>
                                <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                    <?= $objCampo->render(); ?>
                                    <?= $objCampo->renderValidationBlock(); ?>

                                </div>

                                <div class='col-md-4'>

                                    <button class='btn btn-primary' ng-click="vm.validarEmail()"
                                            ng-show="!vm.isEdition">
                                        Verificar e-mail
                                    </button>

                                    <button class='btn btn-primary' ng-click="vm.utilizarOutroEmail()"
                                            ng-show="vm.isEdition && !vm.formData.isAdministradorGlobalDaConta">
                                        Trocar e-mail
                                    </button>

                                </div>

                            </div>

                            <?= $objForm->renderHorizontalLine(); ?>

                            <?php
                            $objCampo = $objForm->getTextInputInstance("nome", TextInput::TRANSFORMATION_DIRECTIVE_UPPER);
                            $objCampo->setLabel("Nome");
                            $objCampo->setDefaultValues();
                            $objCampo->setPlaceHolder("Informe o Nome.");
                            $objCampo->addValidation(new MaxLengthValidation(255, "O tamanho máximo do nome deve ser de {0} caracteres."));
                            $objCampo->addValidation(new RequiredValidation("O Nome é de preenchimento obrigatório."));
                            ?>

                            <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                 ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                                <?= $objCampo->renderLabel(); ?>
                                <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                    <?= $objCampo->render(); ?>
                                    <?= $objCampo->renderValidationBlock(); ?>

                                </div>

                            </div>

                            <?= $objForm->renderHorizontalLine(); ?>

                            <?php
                            $objCampo = $objForm->getPasswordInputInstance("senha");
                            $objCampo->setLabel("Senha");
                            $objCampo->setDefaultValues();
                            $objCampo->setPlaceHolder("Informe a Senha.");
                            $objCampo->addValidation(new MaxLengthValidation(40, "O tamanho máximo da senha deve ser de {0} caracteres."));
                            $objCampo->addValidation(new RequiredValidation("A Senha é de preenchimento obrigatório."));
                            ?>

                            <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                 ng-if="!vm.isEdition"
                                 ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                                <?= $objCampo->renderLabel(); ?>
                                <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                    <?= $objCampo->render(); ?>
                                    <?= $objCampo->renderValidationBlock(); ?>

                                </div>

                            </div>

                            <?= $objForm->renderHorizontalLine(); ?>

                            <?php
                            $objCampo = $objForm->getPasswordInputInstance("confirmacaoSenha");
                            $objCampo->setLabel("Confirmação de Senha");
                            $objCampo->setDefaultValues();
                            $objCampo->setPlaceHolder("Informe a Confirmação de Senha.");
                            $objCampo->addValidation(new MaxLengthValidation(40, "O tamanho máximo da confirmação de senha deve ser de {0} caracteres."));
                            $objCampo->addValidation(new RequiredValidation("A confirmação da senha é de preenchimento obrigatório."));
                            ?>

                            <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                 ng-if="!vm.isEdition"
                                 ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                                <?= $objCampo->renderLabel(); ?>
                                <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                    <?= $objCampo->render(); ?>
                                    <?= $objCampo->renderValidationBlock(); ?>

                                </div>

                            </div>

                            <?= $objForm->renderHorizontalLine(); ?>

                            <?php

                            $objCampo = $objForm->getSelectInstance("empresaId");
                            $objCampo->setLabel(I18N::getExpression("Empresa"));
                            $objCampo->setPlaceHolder(I18N::getExpression("Selecione uma empresa da lista..."), false);
                            $objCampo->setMatchExpression("{{nome}}");
                            $objCampo->setCloseOnSelect(true);

                            $propsFilter = new PropsFilter();
                            $propsFilter->addFieldToFilter("nome");

                            $selectChoiceContent = new SelectChoiceContent("empresaId");
                            $selectChoiceContent->setMainString(new AngularString("nome", "div", Select::getSelectHighLightFilter()));

                            $objCampo->setFilter($propsFilter);
                            $objCampo->setSelectChoiceContent($selectChoiceContent);

                            ?>

                            <div class="<?= $objCampo->getFormGroupCssClass() ?>"
                                 ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>"
                                 ng-if="vm.isEdition">

                                <?= $objCampo->renderLabel(); ?>
                                <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                    <?= $objCampo->render(); ?>
                                    <?= $objCampo->renderValidationBlock(); ?>

                                </div>

                                <div class='col-md-2'>

                                    <?= $objCampo->renderAddEntityButton(I18N::getExpression("Adicionar empresa")); ?>

                                </div>

                            </div>

                            <?= $objForm->renderHorizontalLine(); ?>

                            <?php
                            $objCampo = $objForm->getSwitchControlInstance("isAtivo");
                            $objCampo->setLabel(I18N::getExpression("Status"));
                            $objCampo->setOnText(I18N::getExpression("ATIVO"));
                            $objCampo->setOffText(I18N::getExpression("INATIVO"));
                            ?>

                            <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                 ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>"
                                 ng-if="vm.isEdition && !vm.formData.isAdministradorGlobalDaConta">

                                <?= $objCampo->renderLabel(); ?>
                                <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                    <?= $objCampo->render(); ?>
                                    <?= $objCampo->renderValidationBlock(); ?>

                                </div>

                            </div>

                            <?= $objForm->renderHorizontalLine(null, 'hr-normal hr-big'); ?>

                            <?
                            /*
                             *
                             *      Campos referentes ao cadastro da pessoa associada ao usuário.
                             *
                             */
                            ?>

                            <div class="col-sm-12 box box-nomargin box-collapsed omega-form-section-box"
                                 ng-if="vm.isEdition">
                                <div class="box-header small blue-background">
                                    <div class="title">
                                        <div class='icon-book'></div>
                                        <?= I18N::getExpression("Dados Pessoais") ?>
                                    </div>
                                    <div class="actions">
                                        <a class="btn box-collapse btn-xs btn-link" href="javascript: void(0);"><i></i></a>
                                    </div>
                                </div>
                                <div class="box-content">

                                    <?php
                                    $objCampo = $objForm->getSelectInstance("pessoaSexoId");
                                    $objCampo->setLabel(I18N::getExpression("Sexo"));
                                    $objCampo->setPlaceHolder(I18N::getExpression("Selecione o sexo da lista..."), false);
                                    $objCampo->setMatchExpression("{{nome}}");
                                    $objCampo->setCloseOnSelect(true);

                                    $propsFilter = new PropsFilter();
                                    $propsFilter->addFieldToFilter("nome");

                                    $selectChoiceContent = new SelectChoiceContent("pessoaSexoId");
                                    $selectChoiceContent->setMainString(new AngularString("nome", "div", Select::getSelectHighLightFilter()));

                                    $objCampo->setFilter($propsFilter);
                                    $objCampo->setSelectChoiceContent($selectChoiceContent);
                                    $objCampo->addValidation(new RequiredValidation("O Sexo é de preenchimento obrigatório."));
                                    ?>

                                    <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                         ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                                        <?= $objCampo->renderLabel(); ?>
                                        <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                            <?= $objCampo->render(); ?>
                                            <?= $objCampo->renderValidationBlock(); ?>

                                        </div>

                                    </div>

                                    <?= $objForm->renderHorizontalLine(); ?>

                                    <?php
                                    $objCampo = $objForm->getPhoneInputInstance("pessoaTelefone");
                                    $objCampo->setLabel("Telefone");
                                    $objCampo->setDefaultValues();
                                    $objCampo->setPlaceHolder("Informe o Telefone.");
                                    $objCampo->addValidation(new RequiredValidation("O Telefone é de preenchimento obrigatório."));
                                    ?>

                                    <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                         ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                                        <?= $objCampo->renderLabel(); ?>
                                        <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                            <?= $objCampo->render(); ?>
                                            <?= $objCampo->renderValidationBlock(); ?>

                                        </div>


                                    </div>

                                    <?= $objForm->renderHorizontalLine(); ?>

                                    <?php
                                    $objCampo = $objForm->getPhoneInputInstance("pessoaCelular");
                                    $objCampo->setLabel("Celular");
                                    $objCampo->setDefaultValues();
                                    $objCampo->setPlaceHolder("Informe o Celular.");
                                    $objCampo->addValidation(new RequiredValidation("O celular é de preenchimento obrigatório."));
                                    ?>

                                    <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                         ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                                        <?= $objCampo->renderLabel(); ?>
                                        <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                            <?= $objCampo->render(); ?>
                                            <?= $objCampo->renderValidationBlock(); ?>

                                        </div>


                                    </div>

                                    <?= $objForm->renderHorizontalLine(); ?>


                                    <?php
                                    $objCampo = $objForm->getSelectInstance("pessoaTipoDocumentoId");
                                    $objCampo->setLabel(I18N::getExpression("Tipo de documento"));
                                    $objCampo->setPlaceHolder(I18N::getExpression("Selecione um tipo de documento da lista..."), false);
                                    $objCampo->setMatchExpression("{{nome}}");
                                    $objCampo->setCloseOnSelect(true);

                                    $propsFilter = new PropsFilter();
                                    $propsFilter->addFieldToFilter("nome");

                                    $selectChoiceContent = new SelectChoiceContent("pessoaTipoDocumentoId");
                                    $selectChoiceContent->setMainString(new AngularString("nome", "div", Select::getSelectHighLightFilter()));

                                    $objCampo->setFilter($propsFilter);
                                    $objCampo->setSelectChoiceContent($selectChoiceContent);
                                    $objCampo->addValidation(new RequiredValidation("O tipo de documento é de preenchimento obrigatório."));
                                    ?>

                                    <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                         ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                                        <?= $objCampo->renderLabel(); ?>
                                        <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                            <?= $objCampo->render(); ?>
                                            <?= $objCampo->renderValidationBlock(); ?>

                                        </div>

                                        <div class='col-md-2'>

                                            <?= $objCampo->renderAddEntityButton(I18N::getExpression("Adicionar tipo de documento")); ?>

                                        </div>

                                    </div>

                                    <?= $objForm->renderHorizontalLine(); ?>

                                    <?php
                                    $objCampo = $objForm->getTextInputInstance("pessoaNumeroDocumento");
                                    $objCampo->setLabel("Número do documento");
                                    $objCampo->setDefaultValues();
                                    $objCampo->setPlaceHolder("Informe o Número do documento.");
                                    $objCampo->addValidation(new MaxLengthValidation(30, "O tamanho máximo do número do documento deve ser de {0} caracteres."));
                                    $objCampo->addValidation(new RequiredValidation("O Número do documento é de preenchimento obrigatório."));
                                    ?>

                                    <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                         ng-class="<?= $objCampo->getFormGroupAngularClass() ?>">

                                        <?= $objCampo->renderLabel(); ?>
                                        <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                            <?= $objCampo->render(); ?>
                                            <?= $objCampo->renderValidationBlock(); ?>

                                        </div>

                                    </div>

                                </div>

                            </div>

                            <?= $objForm->renderHorizontalLine(null, 'hr-normal hr-big'); ?>

                            <div class="col-sm-12 box box-nomargin box-collapsed omega-form-section-box"
                                 ng-if="vm.isEdition">
                                <div class="box-header small blue-background">
                                    <div class="title">
                                        <div class='icon-map-marker'></div>
                                        <?= I18N::getExpression("Endereço") ?>
                                    </div>
                                    <div class="actions">
                                        <a class="btn box-collapse btn-xs btn-link" href="javascript: void(0);"><i></i></a>
                                    </div>
                                </div>
                                <div class="box-content">

                                    <?php
                                    $objCampo = $objForm->getTextInputInstance("pessoaLogradouro");
                                    $objCampo->setLabel("Logradouro");
                                    $objCampo->setDefaultValues();
                                    $objCampo->setPlaceHolder("Informe o Logradouro.");
                                    $objCampo->addValidation(new MaxLengthValidation(255, "O tamanho mmínimo do logradouro deve ser de {0} caracteres."));
                                    $objCampo->addValidation(new RequiredValidation("O Logradouro é de preenchimento obrigatório."));
                                    ?>

                                    <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                         ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                                        <?= $objCampo->renderLabel(); ?>
                                        <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                            <?= $objCampo->render(); ?>
                                            <?= $objCampo->renderValidationBlock(); ?>

                                        </div>


                                    </div>

                                    <?= $objForm->renderHorizontalLine(); ?>



                                    <?php
                                    $objCampo = $objForm->getTextInputInstance("pessoaNumero");
                                    $objCampo->setLabel("Número");
                                    $objCampo->setDefaultValues();
                                    $objCampo->setPlaceHolder("Informe o Número.");
                                    $objCampo->addValidation(new MaxLengthValidation(30, "O tamanho máximo do número deve ser de {0} caracteres."));
                                    $objCampo->addValidation(new RequiredValidation("O Número é de preenchimento obrigatório."));
                                    ?>

                                    <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                         ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                                        <?= $objCampo->renderLabel(); ?>
                                        <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                            <?= $objCampo->render(); ?>
                                            <?= $objCampo->renderValidationBlock(); ?>

                                        </div>


                                    </div>

                                    <?= $objForm->renderHorizontalLine(); ?>

                                    <?php
                                    $objCampo = $objForm->getTextInputInstance("pessoaComplemento");
                                    $objCampo->setLabel("Complemento");
                                    $objCampo->setDefaultValues();
                                    $objCampo->setPlaceHolder("Informe o Complemento.");
                                    $objCampo->addValidation(new MaxLengthValidation(255, "O tamanho mímimo do complemento deve ser de {0} caracteres."));
                                    ?>

                                    <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                         ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                                        <?= $objCampo->renderLabel(); ?>
                                        <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                            <?= $objCampo->render(); ?>
                                            <?= $objCampo->renderValidationBlock(); ?>

                                        </div>


                                    </div>

                                    <?= $objForm->renderHorizontalLine(); ?>

                                    <?php
                                    $objCampo = $objForm->getSelectInstance("pessoaCidadeId");
                                    $objCampo->setLabel(I18N::getExpression("Cidade"));
                                    $objCampo->setPlaceHolder(I18N::getExpression("Selecione uma cidade da lista..."), false);
                                    $objCampo->setMatchExpression("{{nome}}");
                                    $objCampo->setCloseOnSelect(true);
                                    $objCampo->setRefreshDelay(0);

                                    $propsFilter = new PropsFilter();
                                    $propsFilter->addFieldToFilter("nome");

                                    $selectChoiceContent = new SelectChoiceContent("pessoaCidadeId");
                                    $selectChoiceContent->setMainString(new AngularString("nome", "div", Select::getSelectHighLightFilter()));

                                    $objCampo->setFilter($propsFilter);
                                    $objCampo->setSelectChoiceContent($selectChoiceContent);
                                    $objCampo->addValidation(new RequiredValidation("A Cidade é de preenchimento obrigatório."));
                                    ?>

                                    <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                         ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                                        <?= $objCampo->renderLabel(); ?>
                                        <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                            <?= $objCampo->render(); ?>
                                            <?= $objCampo->renderValidationBlock(); ?>

                                        </div>

                                        <div class='col-md-2'>

                                            <?= $objCampo->renderAddEntityButton(I18N::getExpression("Adicionar Cidade")); ?>

                                        </div>

                                    </div>

                                    <?= $objForm->renderHorizontalLine(); ?>


                                    <?php

                                    $objCampo = $objForm->getSelectInstance("pessoaBairroId");
                                    $objCampo->setLabel(I18N::getExpression("Bairro"));
                                    $objCampo->setPlaceHolder(I18N::getExpression("Selecione um bairro da lista..."), false);
                                    $objCampo->setMatchExpression("{{nome}}");
                                    $objCampo->setCloseOnSelect(true);
                                    $objCampo->setRefreshDelay(0);

                                    $propsFilter = new PropsFilter();
                                    $propsFilter->addFieldToFilter("nome");

                                    $selectChoiceContent = new SelectChoiceContent("pessoaBairroId");
                                    $selectChoiceContent->setMainString(new AngularString("nome", "div", Select::getSelectHighLightFilter()));

                                    $objCampo->setFilter($propsFilter);
                                    $objCampo->setSelectChoiceContent($selectChoiceContent);
                                    $objCampo->addValidation(new RequiredValidation("O Bairro é de preenchimento obrigatório."));

                                    ?>

                                    <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                         ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                                        <?= $objCampo->renderLabel(); ?>
                                        <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                            <?= $objCampo->render(); ?>
                                            <?= $objCampo->renderValidationBlock(); ?>

                                        </div>

                                        <div class='col-md-2'>

                                            <?= $objCampo->renderAddEntityButton(I18N::getExpression("Adicionar Bairro")); ?>

                                        </div>

                                    </div>

                                </div>

                            </div>

                            <?= $objForm->renderHorizontalLine(null, 'hr-normal hr-big'); ?>

                            <!--
                            <div class="col-sm-12 box box-nomargin box-collapsed omega-form-section-box">
                                <div class="box-header small blue-background">
                                    <div class="title">
                                        <div class='icon-android'></div>
                                        <?= I18N::getExpression("Permissões do Ponto Eletrônico Mobile (Android)") ?>
                                    </div>
                                    <div class="actions">
                                        <a class="btn box-collapse btn-xs btn-link" href="javascript: void(0);"><i></i></a>
                                    </div>
                                </div>
                                <div class="box-content">

                                    <?php
                            $objCampo = $objForm->getSwitchControlInstance("isAdministradorAndroid");
                            $objCampo->setLabel(I18N::getExpression("Administrador Android"));
                            $objCampo->setOnText(I18N::getExpression("SIM"));
                            $objCampo->setOffText(I18N::getExpression("NÃO"));
                            ?>

                                    <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                         ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                                        <?= $objCampo->renderLabel(); ?>
                                        <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                            <?= $objCampo->render(); ?>
                                            <?= $objCampo->renderValidationBlock(); ?>

                                        </div>

                                    </div>

                                    <?php

                            $objCampo = $objForm->getSelectInstance("categoriaPermissaoAndroidId");
                            $objCampo->setLabel(I18N::getExpression("Categoria de Permissão Android"));
                            $objCampo->setPlaceHolder(I18N::getExpression("Selecione uma Categoria de Permissão Android da lista..."), false);
                            $objCampo->setMatchExpression("{{nome}}");
                            $objCampo->setCloseOnSelect(true);
                            $objCampo->setRefreshDelay(0);

                            $propsFilter = new PropsFilter();
                            $propsFilter->addFieldToFilter("nome");

                            $selectChoiceContent = new SelectChoiceContent("categoriaPermissaoAndroidId");
                            $selectChoiceContent->setMainString(new AngularString("nome", "div", Select::getSelectHighLightFilter()));

                            $objCampo->setFilter($propsFilter);
                            $objCampo->setSelectChoiceContent($selectChoiceContent);
                            $objCampo->addValidation(new RequiredValidation("A categoria de Permissão Android é de preenchimento obrigatório."));

                            ?>

                                    <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                         ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                                        <?= $objCampo->renderLabel(); ?>
                                        <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                            <?= $objCampo->render(); ?>
                                            <?= $objCampo->renderValidationBlock(); ?>

                                        </div>

                                        <div class='col-md-2'>

                                            <?= $objCampo->renderAddEntityButton(I18N::getExpression("Adicionar Categoria de Permissão")); ?>

                                        </div>

                                    </div>

                                    <div class='form-group'>

                                        <label class='col-md-2 control-label'>Serviços Disponíveis</label>
                                        <div class='col-md-10'>

                                            <div class="col-sm-4" ng-repeat="servicoAndroid in vm.listaServicosAndroid">

                                                <label>
                                                    <input checklist-value="servicoAndroid"
                                                           type="checkbox"
                                                           checklist-model="vm.formData.servicosAndroidSelecionados"/>
                                                    {{servicoAndroid.servicoNome}}
                                                </label>

                                            </div>

                                        </div>

                                    </div>

                                </div>

                            </div>

                            <?= $objForm->renderHorizontalLine(null, 'hr-normal hr-big'); ?>
                            -->

                            <div class="col-sm-12 box box-nomargin box-collapsed omega-form-section-box"
                                 ng-if="vm.isEdition && !vm.formData.isAdministradorGlobalDaConta">
                                <div class="box-header small blue-background">
                                    <div class="title">
                                        <div class='icon-globe'></div>
                                        <?= I18N::getExpression("Permissões da Web") ?>
                                    </div>
                                    <div class="actions">
                                        <a class="btn box-collapse btn-xs btn-link" href="javascript: void(0);"><i></i></a>
                                    </div>
                                </div>
                                <div class="box-content">

                                    <?php
                                    $objCampo = $objForm->getSwitchControlInstance("isAdministradorWeb");
                                    $objCampo->setLabel(I18N::getExpression("Administrador Web"));
                                    $objCampo->setOnText(I18N::getExpression("SIM"));
                                    $objCampo->setOffText(I18N::getExpression("NÃO"));
                                    ?>

                                    <!--
                                    <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                         ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                                        <?= $objCampo->renderLabel(); ?>
                                        <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                            <?= $objCampo->render(); ?>
                                            <?= $objCampo->renderValidationBlock(); ?>

                                        </div>

                                    </div>
                                    -->

                                    <?php

                                    $objCampo = $objForm->getSelectInstance("usuarioTipoId");
                                    $objCampo->setLabel(I18N::getExpression("Categoria de Permissão Web"));
                                    $objCampo->setPlaceHolder(I18N::getExpression("Selecione uma categoria de permissão da lista..."), false);
                                    $objCampo->setMatchExpression("{{nome}}");
                                    $objCampo->setCloseOnSelect(true);

                                    $propsFilter = new PropsFilter();
                                    $propsFilter->addFieldToFilter("nome");

                                    $selectChoiceContent = new SelectChoiceContent("usuarioTipoId");
                                    $selectChoiceContent->setMainString(new AngularString("nome", "div", Select::getSelectHighLightFilter()));

                                    $objCampo->setFilter($propsFilter);
                                    $objCampo->setSelectChoiceContent($selectChoiceContent);
                                    $objCampo->addValidation(new RequiredValidation("O tipo de usuário é de preenchimento obrigatório."));

                                    ?>

                                    <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                         ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                                        <?= $objCampo->renderLabel(); ?>
                                        <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                            <?= $objCampo->render(); ?>
                                            <?= $objCampo->renderValidationBlock(); ?>

                                        </div>

                                        <div class='col-md-2' ng-if="false">

                                            <?= $objCampo->renderAddEntityButton(I18N::getExpression("Adicionar tipo de usuário")); ?>

                                        </div>

                                    </div>

                                    <?php

                                    $objListaEsquerda = $objForm->getCheckboxListInstance("itensMenuSelecionados", "vm.listaItensMenuWebEsquerda", "itemMenuWeb", "descricaoItem");
                                    $objListaEsquerda->setLabel(I18N::getExpression("Permissões do Menu"));
                                    $objListaEsquerda->setItemLabelAngularClass("::vm.getMenuWebItemClass(itemMenuWeb.nivel)");
                                    $objListaEsquerda->setChangeListener("changeMenuItem(itemMenuWeb, checked)");
                                    $objListaEsquerda->setContainerCssClass("col-md-5");
                                    $objListaEsquerda->setItemContainerCssClass("col-sm-12");

                                    $objListaDireita = $objForm->getCheckboxListInstance("itensMenuSelecionados", "vm.listaItensMenuWebDireita", "itemMenuWeb", "descricaoItem");
                                    $objListaDireita->setItemLabelAngularClass("::vm.getMenuWebItemClass(itemMenuWeb.nivel)");
                                    $objListaDireita->setChangeListener("changeMenuItem(itemMenuWeb, checked)");
                                    $objListaDireita->setContainerCssClass("col-md-5");
                                    $objListaDireita->setItemContainerCssClass("col-sm-12");
                                    ?>


                                    <div class='<?= $objListaEsquerda->getFormGroupCssClass() ?>'
                                         ng-class="<?= $objListaEsquerda->getFormGroupAngularClass() ?>">

                                        <?= $objListaEsquerda->renderLabel(); ?>
                                        <div class='<?= $objListaEsquerda->getContainerCssClass(); ?>'>

                                            <?= $objListaEsquerda->render(); ?>
                                            <?= $objListaEsquerda->renderValidationBlock(); ?>

                                        </div>

                                        <div class='<?= $objListaDireita->getContainerCssClass(); ?>'>

                                            <?= $objListaDireita->render(); ?>
                                            <?= $objListaDireita->renderValidationBlock(); ?>

                                        </div>

                                    </div>


                                </div>

                            </div>

                            <div class='form-actions form-actions-padding-sm'>
                                <div class='row'>
                                    <div class='col-md-10 action-buttons-bar'>

                                        <? $objForm->setSubmitButtonLabel("{{ vm.getSubmitButtonLabel() }}"); ?>
                                        <?= $objForm->renderSubmitButton(); ?>

                                    </div>

                                </div>

                            </div>

                        </form>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>

<?php

if (!Helper::isAngularModal())
{
    $arrDataServicesRelacionados = array('forms/usuarioTrocarEmail', 'forms/usuarioTipo', 'forms/empresa', 'forms/sexo', 'forms/tipoDocumento', 'forms/operadora', 'forms/bairro', 'forms/cidade', 'forms/uf', 'forms/pais', 'forms/categoriaPermissao');
    $arrControllersRelacionados = array('forms/usuarioTipo', 'forms/empresa', 'forms/sexo', 'forms/tipoDocumento', 'forms/operadora', 'forms/bairro', 'forms/cidade', 'forms/uf', 'forms/pais', 'forms/categoriaPermissao');
    ?>

    <!--Definição do Controller Angular JS -->
    <script type="text/javascript">

        <?=Helper::incluirConteudoDataServices(true, $arrDataServicesRelacionados); ?>

    </script>

    <?= Helper::incluirControllerAngularJS(true, $arrControllersRelacionados); ?>
    <?= Helper::incluirControlllerCustomizadoAngularJS('forms/usuarioTrocarEmail'); ?>

<?php } ?>
