<?php

/*

Arquivo gerado atravï¿½s de gerador de cï¿½digo em 24/05/2017 as 10:16:52.
Para que o arquivo nï¿½o seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

Tabela correspondente: servico
Sobrescrita de dados: ---SOBRESCRITA_NAO_AUTORIZADA---

*/
?>

<?php

class EXTDAO_Servico extends DAO_Servico
{
    public function __construct($configDAO = null)
    {
        parent::__construct($configDAO);
        $this->nomeClasse = "EXTDAO_Servico";
    }

    public function factory()
    {
        return new EXTDAO_Servico();
    }

    public static function getServicos($objBanco = null)
    {
        if ($objBanco == null)
        {
            $objBanco = new Database();
        }
        else
        {
            $objBanco = $objBanco;
        }
        $query = "SELECT s.id id
            FROM servico s";

        $objBanco->query($query);
        $ids = Helper::getResultSetToArrayDeUmCampo($objBanco->result);

        return $ids;
    }
}

?>
