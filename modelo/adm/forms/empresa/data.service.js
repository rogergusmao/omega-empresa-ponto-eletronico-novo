/*

 Arquivo gerado atrav�s de gerador de c�digo em 07/01/2018 as 16:14:45.
 Para que o arquivo n�o seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

 Tabela correspondente: empresa
 Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

 */

returnObject.getEmpresa = function (filterParameters)
{

    var parameters = {
        filterParameters: filterParameters,
        loadComboBoxesData: true
    };

    return $http({
        url: baseURLAdm + "/webservice.php?class=EXTDAO_Empresa&action=__getRecord",
        method: "POST",
        data: parameters,
        headers: {
            'Content-Type': 'application/json'
        }
    });

};

returnObject.addEmpresa = function (formData)
{

    return $http({
        url: baseURLAdm + "/webservice.php?class=EXTDAO_Empresa&jsonOutput=true&action=add",
        method: "POST",
        data: formData,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    });

};

returnObject.editEmpresa = function (formData)
{

    return $http({
        url: baseURLAdm + "/webservice.php?class=EXTDAO_Empresa&jsonOutput=true&action=edit",
        method: "POST",
        data: formData,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    });

};

returnObject.getAllComboBoxesDataForEmpresaForm = function ()
{
    return $http({
        url: baseURLAdm + "/webservice.php?class=EXTDAO_Empresa&jsonOutput=true&action=__getFormComboBoxesData",
        method: "POST",
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    });

};

returnObject.getListOfOperadoraForEmpresaForm = function ()
{
    var params = {filterParameters: null, listMappingType: Constants.ListMappingType.COMBOBOX, hideLoading: true};
    return $http.post(baseURLAdm + "/webservice.php?class=EXTDAO_Operadora&action=__getList", params);
};
returnObject.getListOfTipoDocumentoForEmpresaForm = function ()
{
    var params = {filterParameters: null, listMappingType: Constants.ListMappingType.COMBOBOX, hideLoading: true};
    return $http.post(baseURLAdm + "/webservice.php?class=EXTDAO_Tipo_documento&action=__getList", params);
};
returnObject.getListOfTipoEmpresaForEmpresaForm = function ()
{
    var params = {filterParameters: null, listMappingType: Constants.ListMappingType.COMBOBOX, hideLoading: true};
    return $http.post(baseURLAdm + "/webservice.php?class=EXTDAO_Tipo_empresa&action=__getList", params);
};
returnObject.getListOfBairroForEmpresaForm = function ()
{
    var params = {filterParameters: null, listMappingType: Constants.ListMappingType.COMBOBOX, hideLoading: true};
    return $http.post(baseURLAdm + "/webservice.php?class=EXTDAO_Bairro&action=__getList", params);
};
returnObject.getListOfCidadeForEmpresaForm = function ()
{
    var params = {filterParameters: null, listMappingType: Constants.ListMappingType.COMBOBOX, hideLoading: true};
    return $http.post(baseURLAdm + "/webservice.php?class=EXTDAO_Cidade&action=__getList", params);
};
                

