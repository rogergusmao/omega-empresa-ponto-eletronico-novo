import { RemoteRequest } from 'lib/ajax';
import { ContextoGenerico } from 'lib/contexto';

export class RemoteRequestEmContexto<T extends ContextoGenerico> extends RemoteRequest
{
    public contexto: T;
    public constructor(contexto: T, url: string, requestMethod: RemoteRequest.RequestMethod = RemoteRequest.RequestMethod.POST, data: any, async: boolean, background: boolean, requestOptions: JQueryAjaxSettings)
    {
        super(url, data, requestMethod, async);
        this.contexto = contexto;
        this.background = background || true;

        if (!this.background)
        {
            this.requestNumber = ++this.contexto.ajaxRequestCounter;
        }

        if (!this.background)
        {
            //adiciona requsicao na lista de requisicoes em processamento
            this.contexto.ajaxProcessingRequests.push(this.requestNumber);
        }

    }

    public addOptions(options: any)
    {
        for (var attrname in options)
        {
            this.options[attrname] = options[attrname];
        }
    }

    protected async execute<T>(): Promise<T>
    {
        if(!this.background)
        {
            this.contexto.addLoadMask();
        }

        var result = await super.execute<T>();

        if(!this.background)
        {
            this.contexto.removeLoadMask();
        }

        return result;

    }

}