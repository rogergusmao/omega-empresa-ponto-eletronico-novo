<?php
/**
 * Created by PhpStorm.
 * User: W10
 * Date: 16/01/2018
 * Time: 23:05
 */




class BO_Log_app
{
    public static function uploadLogErro($corporacao ,$mobileIdentificador){
        $db = null;
        try{
            if(!strlen($corporacao))
                $corporacao="__publica";

            if(!strlen($mobileIdentificador))
                $mobileIdentificador = "__publico";

            $path = Helper::acharRaiz() . "log/log_android/{$corporacao}/{$mobileIdentificador}/";
            if(!file_exists($path)){
                Helper::mkdir($path, 0777, true);
            }
            if(isset($_FILES["logfile"])){

                $nomeArquivo = $corporacao."_".$mobileIdentificador."_".Helper::getDiaEHoraNomeArquivo().".zip";
                $objUpload = new Upload();
                $objUpload->arrPermitido = "";
                $objUpload->tamanhoMax = "";
                $objUpload->file = $_FILES["logfile"];
                $objUpload->nome = $nomeArquivo;
                $objUpload->uploadPath = $path;
                $success = $objUpload->uploadFile();
                if (!$success) {
                    return new Mensagem(
                        PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR,
                        I18N::getExpression( "Ocorreu uma falha durante o envio de dados do seu celular. Talvez por falha de conex�o a internet."));
                }
            }

            return new Mensagem_token(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO, null);

        }  catch(DatabaseException $dbex){
            if($db !=null)$db->rollbackTransacao();
            return new Mensagem(PROTOCOLO_SISTEMA::BANCO_FORA_DO_AR,null, $dbex);
        }   catch (Exception $exc) {
            if($db !=null)$db->rollbackTransacao();
            return new Mensagem(null,null,$exc);
        }
    }
}