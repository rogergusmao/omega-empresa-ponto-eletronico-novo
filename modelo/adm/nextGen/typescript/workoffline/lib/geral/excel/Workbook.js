"use strict";
exports.__esModule = true;
var XLSX = require("xlsx");
var geral_1 = require("lib/geral");
var Workbook = (function () {
    function Workbook(nomeArquivoExportacao) {
        this.nomeArquivoWorkbook = nomeArquivoExportacao;
        this.celulaInicial = new geral_1.Cell(1, 1);
        this.sheets = new Array();
    }
    Workbook.prototype.addSheet = function (sheet) {
        this.sheets.push(sheet);
    };
    Workbook.prototype.createNewSheet = function (nomeSheet) {
        if (!nomeSheet) {
            nomeSheet = "Planilha " + (this.sheets.length + 1);
        }
        var sheet = new geral_1.Sheet(nomeSheet);
        this.sheets.push(sheet);
        return sheet;
    };
    Workbook.prototype.getSheetNamesArray = function () {
        var sheetNames = new Array();
        for (var _i = 0, _a = this.sheets; _i < _a.length; _i++) {
            var sheet = _a[_i];
            sheetNames.push(sheet.getSheetName());
        }
        return sheetNames;
    };
    Workbook.prototype.getSheetsObject = function () {
        var sheetsObject = {};
        for (var _i = 0, _a = this.sheets; _i < _a.length; _i++) {
            var sheet = _a[_i];
            sheetsObject[sheet.getSheetName()] = sheet.getConteudo();
        }
        return sheetsObject;
    };
    Workbook.prototype.exportar = function () {
        XLSX.writeFile({
            SheetNames: this.getSheetNamesArray(),
            Sheets: this.getSheetsObject()
        }, this.nomeArquivoWorkbook, { cellStyles: true, cellDates: true, bookType: 'xlsx' });
    };
    return Workbook;
}());
exports.Workbook = Workbook;
