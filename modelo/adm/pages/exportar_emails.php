<?php

include '../../recursos/languages/pt-br.php';
include '../../recursos/php/funcoes.php';
include '../../recursos/php/constants.php';
include '../../recursos/php/database_config.php';

$nomeScript = Helper::getNomeDoScriptAtual();

if ($nomeScript == "index.php")
{

    echo "<center>";
    Helper::imprimirMensagem("A janela de download abrirá em alguns segundos.\nCaso não abra automaticamente, <a class='link_padrao' href='forms/exportar_celulares.php'>clique aqui</a>.");
    echo "</center>";

    Helper::mudarLocation("pages/exportar_emails.php");
    exit();
}

$objBanco = new Database();
$objBanco->Query("SELECT DISTINCT primeiro_nome, sobrenomes, email FROM pessoa WHERE email IS NOT NULL");

$stringRetorno = "";

if ($objBanco->rows > 0)
{

    $stringRetorno .= "Name;Email;\r\n";

    while ($dados = $objBanco->fetchArray())
    {

        $nomeCompleto = "{$dados[0]} {$dados[1]}";
        $email = $dados[2];

        if (strlen($email) && Helper::verificarEmail($email))
        {

            $stringRetorno .= "{$nomeCompleto};{$email};\r\n";
        }
    }

    $objDownload = new Download("emails.csv");
    print $objDownload->ds_download($stringRetorno);
}

?>



