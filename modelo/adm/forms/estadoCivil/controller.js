/*

Arquivo gerado atrav�s de gerador de c�digo em 20/01/2018 as 22:16:54.
Para que o arquivo n�o seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

Tabela correspondente: estado_civil
Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

*/

omegaApp.controller('formEstadoCivilController',

    ['$scope', '$rootScope', '$element', '$window', '$interval', '$timeout', 'dataService', 'Upload',

     function ($scope, $rootScope, $element, $window, $interval, $timeout, dataService, Upload)
     {

         var vm = this;
         var generatedTasks = omegaApp.generatedFormEstadoCivilController($scope, $rootScope, $element, $window, $interval, $timeout, dataService, Upload, vm);

         try
         {
             //remover chamada caso queira sobrescrever carregamento de dados inicial  
             ApplicationUtil.runGeneratedTasks(generatedTasks);

             //implementar regras de neg�cio n�o-geradas

         }
         catch (ex)
         {
             ApplicationUtil.showErrorMessage(ex.message, 'Erro no controller formEstadoCivilController', vm);
         }

     }]
);