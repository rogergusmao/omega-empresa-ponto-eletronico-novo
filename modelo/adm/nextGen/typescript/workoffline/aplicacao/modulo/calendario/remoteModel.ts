import { DadosIniciais } from './remoteModel/DadosIniciais';
import { PontoEletronico } from './remoteModel/PontoEletronico';

export
{
    DadosIniciais,
    PontoEletronico
};

