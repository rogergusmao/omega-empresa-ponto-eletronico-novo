"use strict";
exports.__esModule = true;
var DataCarregada = (function () {
    function DataCarregada(data) {
        this.data = data;
        this.periodosCarregados = new Array();
    }
    DataCarregada.prototype.addPeriodoCarregado = function (periodo) {
        this.periodosCarregados.push(periodo);
    };
    return DataCarregada;
}());
exports.DataCarregada = DataCarregada;
