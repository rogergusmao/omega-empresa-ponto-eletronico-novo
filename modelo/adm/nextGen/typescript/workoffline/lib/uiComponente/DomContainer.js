"use strict";
exports.__esModule = true;
var DomContainer = (function () {
    function DomContainer(contexto, domSelector) {
        this.contexto = contexto;
        this.domSelector = domSelector;
    }
    DomContainer.prototype.$ = function (selector) {
        return $(this.contexto.getDomRootSelector() + ' ' + this.domSelector + (typeof selector == 'undefined' ? "" : " " + selector));
    };
    DomContainer.prototype.$root = function (selector) {
        return $(this.contexto.getDomRootSelector() + (typeof selector == 'undefined' ? "" : " " + selector));
    };
    DomContainer.$documentBody = function (selector) {
        return $('body' + (typeof selector == 'undefined' ? "" : " " + selector));
    };
    DomContainer.getMaxAllowedHeight = function () {
        var maxHeight = $(window).height();
        DomContainer.$documentBody('.navbar').each(function () {
            maxHeight -= $(this).outerHeight();
        });
        return maxHeight;
    };
    DomContainer.getMaxAllowedWidth = function () {
        var maxWidth = $(window).width();
        this.$documentBody('#main-nav-bg').each(function () {
            maxWidth -= $(this).outerWidth();
        });
        return maxWidth;
    };
    return DomContainer;
}());
exports.DomContainer = DomContainer;
