<?php

/*

Arquivo gerado atravï¿½s de gerador de cï¿½digo em 24/05/2017 as 10:01:42.
Para que o arquivo nï¿½o seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

Tabela correspondente: pessoa_usuario
Sobrescrita de dados: ---SOBRESCRITA_NAO_AUTORIZADA---

*/
?>

<?php

class EXTDAO_Pessoa_usuario extends DAO_Pessoa_usuario
{
    public function __construct($configDAO = null)
    {
        parent::__construct($configDAO);
        $this->nomeClasse = "EXTDAO_Pessoa_usuario";
    }

    public function factory()
    {
        return new EXTDAO_Pessoa_usuario();
    }

    public static function existeRelacionamentoComAPessoa($pIdPessoa, $objBanco = null)
    {

        if ($objBanco == null)
        {
            $objBanco = new Database();
        }

        //checa a existencia do funcionario
        $query = "SELECT id
                      FROM pessoa_usuario
                      WHERE pessoa_id_INT = " . $pIdPessoa . " ";

        $objBanco->query($query);

        //o usuario ja eh existente
        if ($objBanco->rows() > 1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static function getObjDoRelacionamentoComAPessoa($pIdPessoa, $objBanco = null, $idCorporacao = null)
    {

        if ($objBanco == null)
        {
            $objBanco = new Database();
        }
        if ($idCorporacao == null)
        {
            $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
        }
        //checa a existencia do funcionario
        $query = "SELECT pu.pessoa_id_INT pessoa_id_INT,
                        pu.usuario_id_INT usuario_id_INT, 
                        u.email email_usuario,
                        pu.id pessoa_usuario_id_INT
                      FROM pessoa_usuario pu 
                        JOIN usuario u 
                            ON pu.usuario_id_INT = u.id
                      WHERE pu.pessoa_id_INT = $pIdPessoa AND pu.corporacao_id_INT = $idCorporacao ";

        $objBanco->query($query);
        $objs = Helper::getResultSetToArrayDeObjetos($objBanco->result);
        if (count($objs))
        {
            return $objs[0];
        }
        else
        {
            return null;
        }
    }

    public static function getObjDoRelacionamentoComOUsuario($pIdUsuario, $objBanco = null)
    {

        if ($objBanco == null)
        {
            $objBanco = new Database();
        }

        //checa a existencia do funcionario
        $query = "SELECT pu.id id, pu.pessoa_id_INT pessoa_id_INT, p.nome nome, p.email email_usuario
                      FROM pessoa_usuario pu 
                        JOIN pessoa p 
                            ON pu.pessoa_id_INT = p.id
                      WHERE pu.usuario_id_INT = " . $pIdUsuario . " ";

        $objBanco->query($query);
        $objs = Helper::getResultSetToArrayDeObjetos($objBanco->result);
        if (count($objs))
        {
            return $objs[0];
        }
        else
        {
            return null;
        }
    }

    public static function atualizaEmailDoUsuario(
        $idCorporacao, $idUsuario, $nomeNovo, $emailNovo, $db = null,
        $idUsuarioResponsavel = null)
    {
        if ($db == null)
        {
            $db = new Database();
        }
        $objPessoa = null;
        $objPessoaUsuario = null;
        $objPU = EXTDAO_Pessoa_usuario::getObjDoRelacionamentoComOUsuario($idUsuario, $db);
        //se ja existe uma pessoa atrelada ao usuario
        if ($objPU != null)
        {
            //procura alguma outra pessoa da base que possui o email novo do usuario
            $idPessoaDoEmailNovo = EXTDAO_Pessoa::getPessoaDoEmail($emailNovo, $db);
            if (!empty($idPessoaDoEmailNovo))
            {
                //Caso JÃ¡ exista uma pessoa na base com o email novo:

                //Verifica se essa pessoa jÃ¡ estÃ¡ atrelada a outro usuario
                $idRelacionamentoComAPessoaDoEmailNovo = EXTDAO_Pessoa_usuario::getObjDoRelacionamentoComAPessoa(
                    $idPessoaDoEmailNovo, $db, $idCorporacao);
                //Se a pessoa nÃ£o estÃ¡ vinculada a nenhum usuÃ¡rio
                //entÃ£o iremos vincular
                if ($idRelacionamentoComAPessoaDoEmailNovo == null)
                {
                    //Atualiza o identificador da pessoa do atual relacionamento
                    //do usuÃ¡rio
                    $objPessoa = new EXTDAO_Pessoa($db);
                    $objPessoa->select($idPessoaDoEmailNovo);
                    $objPessoa->setNome($nomeNovo);
                    $objPessoa->formatarParaSQL();
                    $msg = $objPessoa->update($idPessoaDoEmailNovo, true, $idCorporacao, $idUsuarioResponsavel);
                    if ($msg != null && !$msg->ok())
                    {
                        return $msg;
                    }
                    $objPU->setPessoa_id_INT($idPessoaDoEmailNovo);
                    $objPU->formatarParaSQL();

                    $msg = $objPU->update($objPU->getId(), true, $idCorporacao, $idUsuarioResponsavel);
                    if ($msg != null && !$msg->ok())
                    {
                        return $msg;
                    }
                    return new Mensagem(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO);
                }
                else
                {
                    //Se jÃ¡ existe uma pessoa com o email novo, e jÃ¡ estÃ¡ atrelada a um usuÃ¡rio da ]
                    //base, iremos resgatar o email desse usuario relacionado a ela
                    //e iremos atualizar o email dela, para liberar para a ediÃ§Ã£o em andamento

                    //atualiza o email da pessoa com o usuario dela, ou seja, do usuÃ¡rio que atualmente jÃ¡ estÃ¡
                    // vinculado a ela, liberando o email para a ediÃ§Ã£o da pessoa do contexto
                    $objPessoa = new EXTDAO_Pessoa($db);
                    $objPessoa->select($idPessoaDoEmailNovo);
                    $objPessoa->setEmail($objPU->email_usuario);
                    $objPessoa->formatarParaSQL();
                    $msg = $objPessoa->update($idPessoaDoEmailNovo, true, $idCorporacao, $idUsuarioResponsavel);
                    if ($msg != null && !$msg->ok())
                    {
                        return $msg;
                    }
                    //continua abaixo...
                }
            }
            //Nesse ponto o email novo esta liberado para o cadastramento da pessoa
            //iremos atualiza-lo
            $idPessoa = $objPU->pessoa_id_INT;
            if ($objPessoa == null)
            {
                $objPessoa = new EXTDAO_Pessoa($db);
            }
            //selecionamos a pessoa do relacionamento atual, e iremos atualizar o emai dela
            $objPessoa->select($idPessoa);
            $objPessoa->setEmail($emailNovo);
            $objPessoa->setNome($nomeNovo);
            $objPessoa->formatarParaSQL();

            $msg = $objPessoa->update($idPessoa, true, $idCorporacao, $idUsuarioResponsavel);
            if ($msg != null && !$msg->ok())
            {
                return $msg;
            }
            return new Mensagem(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO);
        }
        else
        {
            //se nÃ£o existir relacionamento do usuÃ¡rio da ediÃ§Ã£o com nenhuma pessoa
            //verificamos se existe alguma pessoa com esse novo email
            $idPessoaDoEmailNovo = EXTDAO_Pessoa::getPessoaDoEmail($emailNovo, $db);
            if (!empty($idPessoaDoEmailNovo))
            {
                //se existir uma pessoa com o email desejado para alteracao
                $idRelacionamentoComAPessoaDoEmailNovo = EXTDAO_Pessoa_usuario::getObjDoRelacionamentoComAPessoa(
                    $idPessoaDoEmailNovo, $db, $idCorporacao);
                //Se a pessoa nÃ£o estÃ¡ vinculada a nenhum usuÃ¡rio
                if ($idRelacionamentoComAPessoaDoEmailNovo == null)
                {
                    $objPessoa = new EXTDAO_Pessoa($db);
                    $objPessoa->select($idPessoaDoEmailNovo);
                    $objPessoa->setNome($nomeNovo);
                    $objPessoa->formatarParaSQL();
                    $msg = $objPessoa->update($idPessoaDoEmailNovo, true, $idCorporacao, $idUsuarioResponsavel);
                    if ($msg != null && !$msg->ok())
                    {
                        return $msg;
                    }
                    $objPessoaUsuario = new EXTDAO_Pessoa_usuario($db);
                    //deleta o atual relacionamento do usuario
                    $objPessoaUsuario->delete($objPU['id'], true, $idCorporacao, $idUsuarioResponsavel);
                    //insere um novo relacionamento com a pessoa do email jÃ¡ existente
                    //que nÃ£o estÃ¡ vinculada a ninguÃ©m
                    $objPessoaUsuario->setPessoa_id_INT($idPessoaDoEmailNovo);
                    $objPessoaUsuario->setUsuario_id_INT($idUsuario);
                    $objPessoaUsuario->setCorporacao_id_INT($idCorporacao);
                    $objPessoaUsuario->formatarParaSQL();
                    $msg = $objPessoaUsuario->insert(true, $idCorporacao, $idUsuarioResponsavel);
                    if ($msg != null && !$msg->ok())
                    {
                        return $msg;
                    }
                    return new Mensagem(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO);
                }
                else
                {
                    //atualiza o email da pessoa com o usuario dela, liberando o email para a
                    //pessoa atualmente vinculada ao usuÃ¡rio editado
                    $objPessoa = new EXTDAO_Pessoa($db);
                    $objPessoa->select($idPessoaDoEmailNovo);
                    $objPessoa->setEmail($objPU['email_usuario']);
                    $objPessoa->formatarParaSQL();
                    $msg = $objPessoa->update($idPessoaDoEmailNovo, true, $idCorporacao, $idUsuarioResponsavel);
                    if ($msg != null && !$msg->ok())
                    {
                        return $msg;
                    }
                    //continua abaixo ...
                }
            }
            if ($objPessoa == null)
            {
                $objPessoa = new EXTDAO_Pessoa($db);
            }
            $objPessoa->setNome($nomeNovo);
            $objPessoa->setEmail($emailNovo);
            $objPessoa->formatarParaSQL();
            $msg = $objPessoa->insert(true, $idCorporacao, $idUsuarioResponsavel);
            if ($msg != null && !$msg->ok())
            {
                return $msg;
            }

            $idPessoa = $objPessoa->getIdDoUltimoRegistroInserido();
            if ($objPessoaUsuario == null)
            {
                $objPessoaUsuario = new EXTDAO_Pessoa_usuario($db);
            }
            $objPessoaUsuario->setPessoa_id_INT($idPessoa);
            $objPessoaUsuario->setUsuario_id_INT($idUsuario);
            $objPessoaUsuario->setCorporacao_id_INT($idCorporacao);
            $objPessoaUsuario->formatarParaSQL();
            $msg = $objPessoaUsuario->insert(true, $idCorporacao, $idUsuarioResponsavel);
            if ($msg != null && !$msg->ok())
            {
                return $msg;
            }
            return new Mensagem(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO);
        }
    }

    public static function atualizaDadosDaPessoaDoUsuario($idUsuario, $nome, $db = null,
                                                          $idCorporacao = null, $idUsuarioResponsavel = null)
    {
        if ($db == null)
        {
            $db = new Database();
        }
        $obj = EXTDAO_Pessoa_usuario::getObjDoRelacionamentoComOUsuario($idUsuario, $db);
        if ($obj != null)
        {
            $idPessoa = $obj['pessoa_id_INT'];
            $objPessoa = new EXTDAO_Pessoa();
            $objPessoa->select($idPessoa);
            $objPessoa->setNome($nome);
            $objPessoa->formatarParaSQL();
            $objPessoa->update($idPessoa, true, $idCorporacao, $idUsuarioResponsavel);
        }

        return new Mensagem(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO);
    }

    public static function vinculaUsuarioAPessoa(
        $idCorporacao, $nome, $email, $idUsuario, $db = null, $idUsuarioOperacao = null, &$operacoes = null)
    {
        if ($db == null)
        {
            $db = new Database();
        }
        $idPessoa = EXTDAO_Pessoa::getPessoaDoEmail($email, $db);
        if (!empty($idPessoa))
        {
            //Se existir uma pessoa com o email

            $auxPessoaUsuario = EXTDAO_Pessoa_usuario::getObjDoRelacionamentoComAPessoa(
                $idPessoa, $db, $idCorporacao);
            if ($auxPessoaUsuario->usuario_id_INT == $idUsuario)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO);
            }

            //verifica se existe relacionamento
            if ($auxPessoaUsuario != null)
            {
                //Caso positivo o usuario relacionado deve ter tido o email alterado
                //e ocorreu uma falha no fluxo, para corrigi-la basta atualizar
                //o email da pessoa
                $idPessoaDoUsuario = $auxPessoaUsuario->pessoa_id_INT;
                $emailDoUsuarioDoRelacionamento = $auxPessoaUsuario->email_usuario;
                $objPessoaDoRelacionamento = new EXTDAO_Pessoa($db);
                $objPessoaDoRelacionamento->select($idPessoaDoUsuario);
                $objPessoaDoRelacionamento->setEmail($emailDoUsuarioDoRelacionamento);
                $objPessoaDoRelacionamento->formatarParaSQL();
                $msg = $objPessoaDoRelacionamento->update(
                    $idPessoaDoUsuario, true, $idCorporacao, $idUsuarioOperacao);

                if ($msg != null && !$msg->ok())
                {
                    return $msg;
                }
                //O email da pessoa foi liberado para cadastramento do email de entrada, logo abaixo
                //sera criada a pessoa, e embaixo sera realacionada essa pessoa ao usuario
                $idPessoa = null;

            }
            else
            {
                //logo abaixo
                //embaixo sera realacionada essa pessoa ao usuario
            }
        }
        if (empty($idPessoa))
        {
            //Uma vez atualizado o email da pessoa do relacionamento, serÃ¡ possÃ­vel
            //inserir uma nova pessoa com o email em questÃ£o
            $objPessoa = new EXTDAO_Pessoa($db);
            //$objPessoa->select($id);
            $objPessoa->setNome($nome);
            $objPessoa->setEmail($email);
            $objPessoa->setCorporacao_id_INT($idCorporacao);

            $objPessoa->setCadastro_SEC(Helper::getUTCNowSec());
            $objPessoa->setCadastro_OFFSEC(Helper::getTimezoneOffsetSec());
            $objPessoa->formatarParaSQL();
            $msg = $objPessoa->insert(true, $idCorporacao, $idUsuarioOperacao);
            if ($msg != null && !$msg->ok())
            {
                return $msg;
            }
            $idPessoa = $msg->mValor;
            if ($operacoes != null)
            {
                $operacoes["pessoa"] = $idPessoa;
            }
        }
//        echo "ENTROU: $idPessoa";
//        exit();

        $objPessoaUsuario = new EXTDAO_Pessoa_usuario($db);
        $objPessoaUsuario->setPessoa_id_INT($idPessoa);
        $objPessoaUsuario->setUsuario_id_INT($idUsuario);
        $objPessoaUsuario->formatarParaSQL();
        $msg = $objPessoaUsuario->insert(true, $idCorporacao, $idUsuarioOperacao);
        if ($msg != null && !$msg->ok())
        {
            return $msg;
        }

        $idPessoaUsuario = $msg->mValor;

        EXTDAO_Pessoa_empresa::adicionaProfissaoIndefinidaAPessoa($db, $idCorporacao, $idUsuarioOperacao, $idPessoa);

        $operacoes["pessoa_usuario"] = $idPessoaUsuario;

        return new Mensagem_token(
            PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO,
            null,
            $idPessoaUsuario);
    }
}
