export class Time
{
    public hora: number;
    public minuto: number;
    public segundo: number;

    public constructor(segundos :number);
    public constructor(hora: number, minuto: number, segundo: number);
    public constructor(horaOrSegundos: number, minuto?: number, segundo?: number)
    {
        if (horaOrSegundos && typeof minuto == 'undefined' && typeof segundo == 'undefined')
        {
            var date = new Date(horaOrSegundos);
            this.hora = date.getHours();
            this.minuto = date.getMinutes();
            this.segundo = date.getSeconds();
        }
        else
        {
            this.hora = horaOrSegundos;
            this.minuto = minuto;
            this.segundo = segundo;
        }

    }

}