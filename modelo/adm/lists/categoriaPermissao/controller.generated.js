/*

Arquivo gerado atrav�s de gerador de c�digo em 21/01/2018 as 18:27:38.
Para que o arquivo n�o seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

Tabela correspondente: categoria_permissao
Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

*/

omegaApp.generatedListCategoriaPermissaoController = function ($scope, $rootScope, $element, $window, $interval, $timeout, dataService, Upload, vm)
{
    try
    {
        vm.rootControllerElement = $element;
        vm.fieldsTypes = {};
        vm.isModalView = false;

        vm.messages = {
            error: [],
            warning: [],
            info: [],
            success: []
        };

        vm.disabledFilterFields = {
            fieldId: false,
            fieldNome: false
        };

        vm.requiredFilterFields = {
            fieldId: false,
            fieldNome: false
        };

        vm.filterFieldsParameters = {
            fieldId: null,
            fieldNome: null
        };

        vm.filterData = {
            id: null,
            nome: null
        };

        vm.filterOperators = {
            id: Constants.FilterOperator.EQUALS,
            nome: Constants.FilterOperator.CONTAINS
        };

        vm.sortingClass = {
            id: Constants.ListSortingClass.IDLE,
            nome: Constants.ListSortingClass.IDLE
        };

        vm.paginationParameters =
            {
                currentPage: 1,
                recordsPerPage: Constants.LIST_DEFAULT_NUMBER_OF_RECORDS_PER_PAGE,
                lastPage: null,
                visibleButtonsArray: []
            };

        vm.sortingParameters = [];

        var relationshipObject = null;

        vm.isInitialLoadFinished = false;
        var initialLoading = function ()
        {
            $timeout(function ()
            {
                dataService.getAllComboBoxesDataForCategoriaPermissaoFilter().then(function (returnContent)
                {
                    if (RemoteDataUtil.validateRemoteResponse(returnContent))
                    {
                        var responseObject = RemoteDataUtil.getResponseObject(returnContent);
                        RemoteDataUtil.refactorInitialFilterLoadRemoteResponseData(vm, responseObject, relationshipObject);
                    }
                    else
                    {
                        LogUtil.logRemoteResponseError(returnContent);
                    }

                    vm.isInitialLoadFinished = true;
                    vm.getRemoteListDataFromQueryString();

                });
            }, 200);
        }

        $rootScope.filterListWatch(vm);

        vm.getRemoteListDataFromQueryString = function ()
        {
            var listParametersObject = OmegaListUtil.getListParametersFromBrowseUrlQueryString();

            if (!GeneralUtil.isNullOrEmptyObject(listParametersObject.preservedFilterData))
            {
                vm.filterData = listParametersObject.preservedFilterData;
            }

            var parametersObject = {};
            if (!GeneralUtil.isNullOrEmptyObject(listParametersObject.filterParameters))
            {
                parametersObject.filterParameters = listParametersObject.filterParameters;
            }
            else
            {
                parametersObject.filterParameters = GeneralUtil.getFilterParameters({}, vm.filterOperators)
            }

            if (!GeneralUtil.isNullOrEmptyObject(listParametersObject.sortingParameters))
            {
                parametersObject.sortingParameters = listParametersObject.sortingParameters;
            }
            else
            {
                parametersObject.sortingParameters = vm.sortingParameters;
            }

            if (!GeneralUtil.isNullOrEmptyObject(listParametersObject.paginationParameters))
            {
                parametersObject.paginationParameters = listParametersObject.paginationParameters;
            }
            else
            {
                parametersObject.paginationParameters = vm.paginationParameters;
            }

            vm.applyFilterSortingAndPaginationParameters(parametersObject);

        };
        vm.getRemoteListData = function ()
        {
            //limpa o filtro de resultados renderizado na lista
            vm.listFilter = null;

            //limpa checkboxes selecionados da lista
            vm.selectedRecords = [];

            var filterData = RemoteDataUtil.refactorRemoteRequestForFilter(vm.filterData);
            var parametersObject =
                {
                    filterParameters: GeneralUtil.getFilterParameters(filterData, vm.filterOperators),
                    sortingParameters: vm.sortingParameters,
                    paginationParameters: vm.paginationParameters
                };

            vm.applyFilterSortingAndPaginationParameters(parametersObject);

        };
        vm.applyFilterSortingAndPaginationParameters = function (parametersObject)
        {
            OmegaListUtil.addListParametersToBrowserUrlQueryString(parametersObject, vm.filterData);

            dataService.getCategoriaPermissaoList(parametersObject).then(function (returnContent)
            {
                if (RemoteDataUtil.validateRemoteResponseForList(returnContent))
                {
                    var responseObject = RemoteDataUtil.getResponseObject(returnContent);

                    vm.originalDataSet = responseObject.dataSet;
                    vm.dataSet = angular.copy(vm.originalDataSet);
                    vm.isFiltered = true;

                    OmegaListUtil.doActionsAfterRemoteListDataReceived(vm, responseObject);
                }
                else
                {
                    RemoteDataUtil.defaultRemoteErrorHandling(returnContent, vm);
                }
            });

        };

        vm.getListOfCorporacaoId = function ()
        {

            dataService.getListOfCorporacaoIdForCategoriaPermissaoList().then(function (returnContent)
            {
                if (RemoteDataUtil.validateRemoteResponseForList(returnContent))
                {
                    var remoteDataSet = RemoteDataUtil.getResponseObjectDataSetOrEmptyArray(returnContent);
                    vm.filterFieldsParameters.fieldCorporacaoId.remoteData = remoteDataSet;
                }
                else
                {
                    LogUtil.logRemoteResponseError(returnContent);
                }

            });

        };

        vm.selectedRecords = [];
        vm.selectedRecordsForRemoval = [];

        vm.showRemoveMessageDialog = function (records)
        {
            records = records || vm.selectedRecords;
            vm.selectedRecordsForRemoval = angular.copy(records);

            $rootScope.showDialog(vm, 'before-remove-template.html');
        };

        vm.closeRemoveMessageDialog = function ()
        {
            vm.selectedRecordsForRemoval = [];
            $rootScope.closeMessageDialog();
        };

        vm.removeSelectedRecords = function ()
        {
            var parameters = vm.selectedRecordsForRemoval;
            dataService.removeRecords(parameters).then(function (returnContent)
            {
                vm.closeRemoveMessageDialog();
                if (RemoteDataUtil.validateRemoteResponse(returnContent))
                {
                    vm.getRemoteListData();
                }
                else
                {
                    RemoteDataUtil.defaultRemoteErrorHandling(returnContent, vm);
                }
            });
        };

        vm.editRecord = function (primaryKey)
        {
            var parameters = $.param(primaryKey);
            document.location = 'index.php?tipo=forms&page=categoriaPermissao&' + parameters;
        };

        return {

            scope: this,
            initFunction: initialLoading

        };

    }
    catch (ex)
    {
        ApplicationUtil.showErrorMessage(ex.message, 'Erro no controller generatedListCategoriaPermissaoController', vm);
    }

}