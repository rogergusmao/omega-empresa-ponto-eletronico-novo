"use strict";
exports.__esModule = true;
var ElementoVisualizacao = (function () {
    function ElementoVisualizacao(tipo, descricao) {
        this.tipo = tipo;
        this.descricao = descricao;
    }
    ElementoVisualizacao.getLista = function () {
        return [
            new ElementoVisualizacao('empresa', "Empresas"),
            new ElementoVisualizacao('ponto', "Pontos Eletrônicos"),
            new ElementoVisualizacao('usuario', "Usuários"),
            new ElementoVisualizacao('rua', "Ruas"),
            new ElementoVisualizacao('estrada', "Estradas"),
            new ElementoVisualizacao('estabelecimento', "Estabelecimentos")
        ];
    };
    return ElementoVisualizacao;
}());
exports.ElementoVisualizacao = ElementoVisualizacao;
