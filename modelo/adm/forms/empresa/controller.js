/*

 Arquivo gerado atrav�s de gerador de c�digo em 07/01/2018 as 16:14:45.
 Para que o arquivo n�o seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

 Tabela correspondente: empresa
 Sobrescrita de dados: ---SOBRESCRITA_NAO_AUTORIZADA---

 */

omegaApp.controller('formEmpresaController',

    ['$scope', '$rootScope', '$element', '$window', '$interval', '$timeout', 'dataService', 'Upload',

     function ($scope, $rootScope, $element, $window, $interval, $timeout, dataService, Upload)
     {

         var vm = this;
         var generatedTasks = omegaApp.generatedFormEmpresaController($scope, $rootScope, $element, $window, $interval, $timeout, dataService, Upload, vm);

         try
         {
             //remover chamada caso queira sobrescrever carregamento de dados inicial
             ApplicationUtil.runGeneratedTasks(generatedTasks);

             vm.requiredFields = {
                 fieldNome: false,
                 fieldTelefone1: false,
                 fieldTelefone2: false,
                 fieldFax: false,
                 fieldCelular: false,
                 fieldOperadoraId: false,
                 fieldCelularSms: false,
                 fieldEmail: true,
                 fieldTipoDocumentoId: false,
                 fieldNumeroDocumento: false,
                 fieldTipoEmpresaId: false,
                 fieldLogradouro: false,
                 fieldNumero: false,
                 fieldComplemento: false,
                 fieldBairroId: false,
                 fieldCidadeId: false
             };

             vm.openBairroIdModal = function ()
             {
                 var modalIndex = $rootScope.getNextModalIndex();
                 $rootScope.registerModalInstance($rootScope.showModalDialog('forms', 'bairro', modalIndex));

                 //atualizando valores da lista
                 var unregisterModal = $rootScope.$watch(
                     function ()
                     {
                         return $rootScope.uibModalInstance[modalIndex]
                     },
                     function ()
                     {
                         if ($rootScope.uibModalInstance[modalIndex] == null)
                         {
                             if (!GeneralUtil.isNullOrEmpty(vm.formData.cidadeId))
                             {
                                 vm.getListOfBairroId(vm.formData.cidadeId.id);
                             }

                             vm.formData.bairroId = null;
                             unregisterModal();
                         }

                     });

             };

             vm.getListOfBairroId = function (cidadeId)
             {
                 if (!vm.isInitialLoadFinished)
                 {
                     return;
                 }

                 dataService.getListOfBairroForEmpresaForm(cidadeId).then(function (returnContent)
                 {
                     if (RemoteDataUtil.validateRemoteResponse(returnContent))
                     {
                         var remoteDataSet = RemoteDataUtil.getResponseObjectDataSetOrEmptyArray(returnContent);
                         vm.fieldsParameters.fieldBairroId.remoteData = remoteDataSet;
                     }
                     else
                     {
                         LogUtil.logRemoteResponseError(returnContent);
                     }
                 });
             };

             $scope.$watch(
                 //express�o do Watch
                 function ()
                 {
                     return vm.formData.cidadeId
                 },
                 //callback em caso de altera��o
                 function ()
                 {
                     if (!vm.isInitialLoadFinished)
                     {
                         return;
                     }

                     if (!GeneralUtil.isNullOrEmpty(vm.formData.cidadeId))
                     {
                         vm.getListOfBairroId(vm.formData.cidadeId.id);
                     }
                     vm.formData.bairroId = null;
                 }
             );

         }
         catch (ex)
         {
             ApplicationUtil.showErrorMessage(ex.message, 'Erro no controller formEmpresaController', vm);
         }

     }]
);