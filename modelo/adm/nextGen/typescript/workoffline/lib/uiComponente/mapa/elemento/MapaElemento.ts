import { ContextoMapa } from 'lib/contexto';
import { ElementoVisualizacao } from 'lib/uiComponente';

export abstract class MapaElemento
{
    public static CurrentIdentifierCounter :number = 0;

    protected contexto :ContextoMapa;
    public elementsLatLngs :Array<google.maps.LatLng>;
    public htmlElement :HTMLElement;
    protected tipoElemento :ElementoVisualizacao.Tipo;

    protected constructor(contexto :ContextoMapa, latLngs :Array<google.maps.LatLng>, tipoElemento :ElementoVisualizacao.Tipo)
    {
        this.contexto = contexto;
        this.contexto.mapObject.addComponente(this);

        this.elementsLatLngs = new Array<google.maps.LatLng>();
        this.elementsLatLngs = this.elementsLatLngs.concat(latLngs);
        
        this.tipoElemento = tipoElemento;
        this.htmlElement = null;
    }

    public static getNextIdentifier() :string
    {
        return "workoffline-element-" + (++MapaElemento.CurrentIdentifierCounter);
    }

    public getTipoElemento() :ElementoVisualizacao.Tipo
    {
        return this.tipoElemento;
    }

    public getGoogleMapInstance()
    {
        return this.contexto.mapObject.getGoogleMapInstance();
    }

    public abstract setVisible(visible :boolean) :void;
    public abstract tryToSetHtmlElement() :void;
    public abstract render() :void;
    public abstract isRendered() :boolean;
    public abstract removeFromGoogleMap() :void;

}