type Point = google.maps.Point;
type Size = google.maps.Size;

export class Icone implements google.maps.Icon
{
    public anchor?: Point;
    public labelOrigin?: Point;
    public origin?: Point;
    public scaledSize?: Size;
    public size?: Size;
    public url?: string;

    public constructor(url :string)
    {
        this.url = url;
        this.setDefaultValues();
    }

    public setDefaultValues()
    {
        this.origin = new google.maps.Point(0, 0);
        //this.labelOrigin = new google.maps.Point(0, -5);
    }

}