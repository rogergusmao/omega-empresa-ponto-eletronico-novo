<?php

$pathFuncoes = acharRaizWorkspace() . DIRETORIO_BIBLIOTECAS_COMPARTILHADAS . '/classes/FuncoesCorporacao.php';

require_once $pathFuncoes;

$singletonFuncoes = FuncoesCorporacao::getSingleton();
if (!isset($raizWorkspace))
{
    $raizWorkspace = Helper::acharRaizWorkspace();
}
$raiz = Helper::acharRaiz();

$classe = Helper::POSTGET("class");
$singletonFuncoes->setDiretorios(array(
                                     array($classe, Helper::acharRaiz() . "recursos/classes/", array("class/", "EXTDAO/", "DAO/", "BO/")),
                                     array($classe, Helper::acharRaiz() . "web_service_pontoeletronico/", array('/')),
                                     array($classe, Helper::acharRaiz() . "web_service_sihop/", array('/')),
                                     array($classe, Helper::acharRaiz() . "recursos/protocolo/", array("in/", "out/")),
                                     array($classe, $raizWorkspace . DIRETORIO_BIBLIOTECAS_COMPARTILHADAS,
                                         array(
                                             "classes/",
                                             "php/",
                                             "adm_flatty/",
                                             "adm_padrao/",
                                             "imports/",
                                             "UI/Menu/",
                                             "UI/Controls/",
                                             "UI/Controls/Auxiliares/",
                                             "UI/Environment/",
                                             "UI/I18N/",
                                             "UI/Validation/",
                                             "pipeline_sync/",
                                             "messenger/")),
                                     array($classe, $raizWorkspace . DIRETORIO_BIBLIOTECAS_COMPARTILHADAS . "classes/", array("protocolo/")),
                                     array($classe, $raizWorkspace . DIRETORIO_BIBLIOTECAS_COMPARTILHADAS . "adm_flatty/", array("imports/")),
                                     array($classe, $raizWorkspace . DIRETORIO_BIBLIOTECAS_COMPARTILHADAS . "adm_padrao/", array("imports/")),

                                 ));

require_once $raizWorkspace . DIRETORIO_BIBLIOTECAS_COMPARTILHADAS . '/imports/instancias.php';
require_once $raiz. PATH_RELATIVO_PROJETO . 'imports/instancias.php';
