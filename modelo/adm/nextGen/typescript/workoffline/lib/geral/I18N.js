"use strict";
exports.__esModule = true;
var Moment = require("moment");
var I18N = (function () {
    function I18N() {
    }
    I18N.getMomentInstance = function (dateObject) {
        return Moment(dateObject);
    };
    return I18N;
}());
exports.I18N = I18N;
