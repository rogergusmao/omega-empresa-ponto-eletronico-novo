"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var React = require("react");
var uiComponente_1 = require("lib/uiComponente");
var uiTemplate_1 = require("lib/uiTemplate");
var BarraInferior = (function (_super) {
    __extends(BarraInferior, _super);
    function BarraInferior(props, reactContext) {
        var _this = _super.call(this, props || {}, reactContext) || this;
        var arrTipos = new Array();
        for (var _i = 0, _a = _this.getListaDeElementos(); _i < _a.length; _i++) {
            var elemento = _a[_i];
            arrTipos.push(elemento.tipo);
        }
        _this.state = { elementosSelecionados: arrTipos };
        return _this;
    }
    BarraInferior.prototype.getListaDeTemplates = function () {
        return [
            new uiTemplate_1.Default(),
            new uiTemplate_1.Light()
        ];
    };
    BarraInferior.prototype.getListaDeElementos = function () {
        return uiComponente_1.ElementoVisualizacao.getLista();
    };
    BarraInferior.prototype.onElementoVisualizacaoClick = function (evento, elemento) {
        setTimeout(function () { return $('#botao-lista-elementos').addClass('open'); }, 0);
        var indiceElemento = this.state.elementosSelecionados.indexOf(elemento.tipo);
        var novoEstado;
        if (indiceElemento > -1) {
            this.state.elementosSelecionados.splice(indiceElemento, 1);
            novoEstado = this.state.elementosSelecionados.slice();
        }
        else {
            novoEstado = [elemento.tipo].concat(this.state.elementosSelecionados);
        }
        this.setState({ elementosSelecionados: novoEstado });
        if (this.props.onElementosVisiveisChange != null) {
            this.props.onElementosVisiveisChange(novoEstado);
        }
    };
    BarraInferior.prototype.render = function () {
        var _this = this;
        var listaTemplates = this.getListaDeTemplates();
        var listaElementos = this.getListaDeElementos();
        return (<div className='row'>
                <div className='col-sm-12 box'>
                  <div className='box-content'>
                    <nav className='navbar navbar-default' role='navigation'>
                      <div className='navbar-header'>                        
                      </div>
                      <div className='collapse navbar-collapse navbar-ex1-collapse'>
                        <ul className='nav navbar-nav'>                          
                          <li className='dropup'>
                            <a className='dropdown-toggle' data-toggle='dropdown' href='#'>
                              Tema
                              <b className='caret'></b>
                            </a>
                            <ul className='dropdown-menu'>
                              {listaTemplates.map(function (template) { return <li key={template.nomeTemplate}><a onClick={function () { return _this.props.onTemaChange(template); }}>{template.nomeTemplate}</a></li>; })}                           
                            </ul>
                          </li>

                          <li className='dropup' id="botao-lista-elementos">
                            <a className='dropdown-toggle' data-toggle='dropdown' href='#'>
                              Elementos
                              <b className='caret'></b>
                            </a>
                            <ul className='dropdown-menu lista-checkboxes'>
                              {listaElementos.map(function (elemento) { return (<li key={elemento.tipo}>
                                  <a onClick={function (evento) { return _this.onElementoVisualizacaoClick(evento, elemento); }}>
                                    <input type="checkbox" checked={_this.state.elementosSelecionados.indexOf(elemento.tipo) > -1}/>&nbsp;{elemento.descricao}
                                  </a>
                                </li>); })}                                                
                            </ul>
                          </li>

                        </ul>                        
                        <ul className='nav navbar-nav navbar-right'>
                          <li>
                            <a><uiComponente_1.Relogio /></a>
                          </li>                          
                        </ul>
                      </div>
                    </nav>                   
                  </div>
                </div>
            </div>);
    };
    return BarraInferior;
}(uiComponente_1.ReactComponent));
exports.BarraInferior = BarraInferior;
