import { RemoteModel } from 'lib/model';

export class Usuario extends RemoteModel
{
    public latitude :string;
    public longitude :string;
    public velocidade :string;
    public cadastroSec :string;
    public cadastroOffsec :string;
    public nomeUsuario :string;
    public idUsuario :string;
    public idSexoUsuario :string;
    public fonteInformacao :string;
}