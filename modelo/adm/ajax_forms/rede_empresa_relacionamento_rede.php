<?php
/*
 *
 * -------------------------------------------------------
 * NOME DO FORMUL�RIO: rede_empresa
 * DATA DE GERA��O:    21.10.2012
 * ARQUIVO:            rede_empresa.php
 * TABELA MYSQL:       rede_empresa
 * BANCO DE DADOS:     ponto_eletronico
 * -------------------------------------------------------
 *
 */

if (isset($_GET["contador"]))
{

    $numeroRegistroInterno = Helper::GET("contador");
}

if (isset($identificadorRelacionamento) && is_numeric($identificadorRelacionamento))
{

    $objRede_empresa = new EXTDAO_Rede_empresa();
    $objRede_empresa->select($identificadorRelacionamento);
}
else
{

    $objRede_empresa = new EXTDAO_Rede_empresa();
}

$objArgRede_empresa = new Generic_Argument();
$objRede_empresa->formatarParaExibicao();
?>

<input type="hidden" name="rede_empresa_id_<?= $numeroRegistroInterno ?>"
       id="rede_empresa_id_<?= $numeroRegistroInterno ?>" value="<?= $objRede_empresa->getId(); ?>">

<table class="tabela_form">

    <tr class="tr_form">


        <?
        $objArgRede_empresa = new Generic_Argument();

        $objArgRede_empresa->numeroDoRegistro = "";
        $objArgRede_empresa->label = $objRede_empresa->label_empresa_id_INT;
        $objArgRede_empresa->valor = $objRede_empresa->getEmpresa_id_INT();
        $objArgRede_empresa->classeCss = "input_text";
        $objArgRede_empresa->classeCssFocus = "focus_text";
        $objArgRede_empresa->obrigatorio = true;
        $objArgRede_empresa->largura = 400;
        $objArgRede_empresa->nome = "rede_empresa_empresa_id_INT_{$numeroRegistroInterno}";
        $objArgRede_empresa->id = "rede_empresa_empresa_id_INT_{$numeroRegistroInterno}";
        $objArgRede_empresa->funcaoGetOptions = "getOptionsClientesPJ";

        $objRede_empresa->addInfoCampos("empresa_id_INT", $objArgRede_empresa->label, "TEXTO", $objArgRede_empresa->obrigatorio);
        ?>

        <td class="td_form_label" style="width: 100px;"><?= $objArgRede_empresa->getLabel() ?></td>
        <td class="td_form_campo">
            <?= $objRede_empresa->objEmpresa->getComboBox($objArgRede_empresa); ?>
        </td>


    <tr>
        <td colspan="4" class="td_botao_remover_da_lista"><input class="botoes_form" type="button"
                                                                 value="Remover Empresa"
                                                                 onclick="javascript:removerDivAjaxEmLista(this);"></td>
    </tr>
</table><br/>

