export class Helper
{
    protected constructor()
    {
        throw new Error("Não é possível instanciar esta classe");
    }

    static async sleep(miliseconds :number)
    {
        return new Promise(resolve => setTimeout(resolve, miliseconds));
    }

    static isNullOrEmpty(object :any)
    {
        return (typeof object == 'undefined' || object === null || object === '');
    }

    static parseInt(stringValue :string) :number
    {
        return stringValue == null ? null : parseInt(stringValue, 10);
    }

    static getRandomColor(iDarkLuma: number, iLightLuma: number): string
    {
        for (var i = 0; i < 20; i++)
        {
            var sColour = ('ffffff' + Math.floor(Math.random() * 0xFFFFFF).toString(16)).substr(-6);

            var rgb = parseInt(sColour, 16); // convert rrggbb to decimal
            var r = (rgb >> 16) & 0xff;  // extract red
            var g = (rgb >> 8) & 0xff;  // extract green
            var b = (rgb >> 0) & 0xff;  // extract blue

            var iLuma = 0.2126 * r + 0.7152 * g + 0.0722 * b; // per ITU-R BT.709

            if (iLuma > iDarkLuma && iLuma < iLightLuma)
            {
                return sColour;
            }

        }

        return '#' + sColour;

    }

    static getDateSeconds(date?: Date): number
    {
        date = date || new Date();
        return date.getHours() * 3600 + date.getMinutes() * 60 + date.getSeconds();
    }

}