<?php
/*
 *
 * -------------------------------------------------------
 * NOME DA LIST:       empresa
 * NOME DA CLASSE DAO: DAO_Empresa
 * DATA DE GERA��O:    05.07.2012
 * ARQUIVO:            EXTDAO_Empresa.php
 * TABELA MYSQL:       empresa
 * BANCO DE DADOS:     ponto_eletronico
 * -------------------------------------------------------
 *
 */

$email = Helper::POSTGET("email");

$mensagemHospedagens = BO_SIHOP::consultaHospedagensDoUsuario($email, ID_SISTEMA_SIHOP);

?>


    <fieldset class="fieldset_list">
        <legend class="legend_list"><?= I18N::getExpression("Lista de Empresas"); ?></legend>

        <table class="tabela_list">
            <colgroup>
                <col width="30%"/>
                <col width="70%"/>
            </colgroup>
            <thead>
            <tr class="tr_list_titulos">
                <td class="td_list_titulos">Nome</td>
                <td class="td_list_titulos">Dom�nio</td>
            </tr>
            </thead>
            <tbody>

            <?
            if ($mensagemHospedagens->mCodRetorno != PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO)
            {
                ?>

                <tr class="tr_list_conteudo_impar">
                    <td colspan="4">
                        <?

                        Helper::imprimirMensagem($mensagemHospedagens->mMensagem, MENSAGEM_WARNING);

                        ?>
                    </td>
                </tr>

                <?
            }
            else
            {
                $vetor = $mensagemHospedagens->mVetorObj;

                $protocolo = new Protocolo_dados_hospedagem();
                for ($i = 0; $i < count($vetor); $i++)
                {
                    $protocolo = $vetor[$i];
                    ?>
                    <tr class="tr_list_conteudo_impar">
                        <td>
                            <?
                            echo $protocolo->nomeSite;
                            ?>
                        </td>
                        <td>
                            <?

                            $objLink = new Link();
                            $objLink->alturaGreyBox = null;
                            $objLink->larguraGreyBox = null;
                            $objLink->tituloGreyBox = null;
                            $objLink->url = $protocolo->dominio;
                            $objLink->demaisAtributos = array("onmouseover" => "tip('Clique aqui para acessar o sistema.', this);", "onmouseout" => "notip();");
                            //$objLink->cssClass = "link_padrao";
                            $objLink->label = $protocolo->dominio;

                            echo $objLink->montarLink();

                            ?>
                        </td>
                    </tr>
                    <?
                }
            }

            ?>

            </tbody>
        </table>

    </fieldset>

    <br/>
    <br/>

<?
