<?php
/*

Arquivo gerado através de gerador de código em 11/12/2017 as 22:44:38.
Para que o arquivo não seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

Tabela correspondente: ponto
Sobrescrita de dados: ---SOBRESCRITA_NAO_AUTORIZADA---

*/

?>

<?
global $objFilter;
?>

<?= $objFilter->getFormDebugPanel(); ?>
<div class='row'>
    <div class='col-sm-12'>
        <div class='box omega-filter-box'>
            <div class='box-header blue-background'>
                <div class='title'>
                    <div class='icon-edit'></div>
                    <?= I18N::getExpression("Filtro"); ?>
                </div>
                <div class='actions'>
                    <a class="btn box-collapse btn-xs btn-link" href="#"><i></i></a>
                </div>
            </div>
            <div class='box-content'>

                <form name="<?= $objFilter->getFormName() ?>" class="form form-horizontal" style="margin-bottom: 0;">

                    <fieldset>

                        <?
                        $objCampo = $objFilter->getSelectInstance("pessoaId");
                        $objCampo->setLabel(I18N::getExpression("Pessoa"));
                        $objCampo->setPlaceHolder(I18N::getExpression("Selecione uma pessoa da lista..."), false);
                        $objCampo->setMatchExpression("{{nome}}");
                        $objCampo->setCloseOnSelect(true);
                        $propsFilter = new PropsFilter();
                        $propsFilter->addFieldToFilter("nome");
                        $selectChoiceContent = new SelectChoiceContent("pessoaId");
                        $selectChoiceContent->setMainString(new AngularString("nome", "div", Select::getSelectHighLightFilter()));
                        $objCampo->setFilter($propsFilter);
                        $objCampo->setSelectChoiceContent($selectChoiceContent);
                        $objCampo->addValidation(new RequiredValidation(I18N::getExpression("A pessoa é de preenchimento obrigatório.")));
                        ?>

                        <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                             ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                            <?= $objCampo->renderLabel(); ?>
                            <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                <?= $objCampo->render(); ?>
                                <?= $objCampo->renderValidationBlock(); ?>

                            </div>
                        </div>

                        <?= $objFilter->renderHorizontalLine(); ?>



                        <?
                        $objCampo = $objFilter->getSelectInstance("empresaId");
                        $objCampo->setLabel(I18N::getExpression("Empresa"));
                        $objCampo->setPlaceHolder(I18N::getExpression("Selecione uma empresa da lista..."), false);
                        $objCampo->setMatchExpression("{{nome}}");
                        $objCampo->setCloseOnSelect(true);
                        $propsFilter = new PropsFilter();
                        $propsFilter->addFieldToFilter("nome");
                        $selectChoiceContent = new SelectChoiceContent("empresaId");
                        $selectChoiceContent->setMainString(new AngularString("nome", "div", Select::getSelectHighLightFilter()));
                        $objCampo->setFilter($propsFilter);
                        $objCampo->setSelectChoiceContent($selectChoiceContent);
                        $objCampo->addValidation(new RequiredValidation(I18N::getExpression("A Empresa é de preenchimento obrigatório.")));
                        ?>

                        <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                             ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                            <?= $objCampo->renderLabel(); ?>
                            <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                <?= $objCampo->render(); ?>
                                <?= $objCampo->renderValidationBlock(); ?>

                            </div>
                        </div>

                        <?= $objFilter->renderHorizontalLine(); ?>


                        <?
                        $objCampo = $objFilter->getSelectForBooleanInstance("isEntradaBoolean");
                        $objCampo->setLabel(I18N::getExpression("É de entrada?"));
                        $objCampo->addValidation(new RequiredValidation(I18N::getExpression("A é de entrada? é de preenchimento obrigatório.")));
                        ?>

                        <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                             ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                            <?= $objCampo->renderLabel(); ?>
                            <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                <?= $objCampo->render(); ?>
                                <?= $objCampo->renderValidationBlock(); ?>

                            </div>
                        </div>

                        <?= $objFilter->renderHorizontalLine(); ?>

                        <?
                        $objCampoDe = $objFilter->getDateInputInstance("dataSecDe");
                        $objCampoDe->setLabel(I18N::getExpression("Data do ponto - De"));
                        $objCampoDe->setDefaultValues();
                        $objCampoDe->setPlaceHolder("");

                        $objCampoAte = $objFilter->getDateInputInstance("dataSecAte");
                        $objCampoAte->setLabel(I18N::getExpression("Data do ponto - Até"));
                        $objCampoAte->setDefaultValues();
                        $objCampoAte->setPlaceHolder("");

                        $objFilter->defineAttributesForDateRangeFields($objCampoDe, $objCampoAte);

                        ?>

                        <div class='<?= $objCampoDe->getFormGroupCssClass() ?>'
                             ng-class="<?= $objCampoDe->getFormGroupAngularClass(); ?>">

                            <?= $objCampoDe->renderLabel(); ?>
                            <div class='<?= $objCampoDe->getContainerCssClass(); ?>'>

                                <?= $objCampoDe->render(); ?>
                                <?= $objCampoDe->renderValidationBlock(); ?>

                            </div>
                        </div>

                        <?= $objFilter->renderHorizontalLine(); ?>


                        <?
                        $objCampo = $objFilter->getDateInputInstance("dataSecAte");
                        $objCampo->setLabel(I18N::getExpression("Data do ponto - Até"));
                        $objCampo->setDefaultValues();
                        $objCampo->setPlaceHolder("");
                        ?>

                        <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                             ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                            <?= $objCampo->renderLabel(); ?>
                            <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                <?= $objCampo->render(); ?>
                                <?= $objCampo->renderValidationBlock(); ?>

                            </div>
                        </div>

                        <?= $objFilter->renderHorizontalLine(); ?>

                    </fieldset>

                    <div class='form-actions form-actions-padding-sm'>
                        <div class='row'>
                            <div class='col-md-11 action-buttons-bar'>

                                <?= $objFilter->renderSubmitButton(); ?>
                                <?= $objFilter->renderClearButton(); ?>

                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
            
