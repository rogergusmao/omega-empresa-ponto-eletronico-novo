<?php

/*

Arquivo gerado atrav?s de gerador de c?digo em 24/05/2017 as 10:04:44.
Para que o arquivo n?o seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

Tabela correspondente: profissao
Sobrescrita de dados: ---SOBRESCRITA_NAO_AUTORIZADA---

*/
?>

<?php

class EXTDAO_Profissao extends DAO_Profissao
{
    public function __construct($configDAO = null)
    {
        parent::__construct($configDAO);
        $this->nomeClasse = "EXTDAO_Profissao";
    }

    public function factory()
    {
        return new EXTDAO_Profissao();
    }

    public static function getIdProfissaoIndefinida(Database $db, $idCorporacao, $idUsuarioLogado)
    {
        $str = I18N::getExpression("PROFISSAO");
        $q = "SELECT id 
              FROM profissao 
              WHERE nome = " . Helper::formatarStringParaComandoSQL($str, $db)
            . " AND corporacao_id_INT = $idCorporacao";

        $db->queryMensagem($q);
        $idProfissao = $db->getPrimeiraTuplaDoResultSet(0);
        if (!strlen($idProfissao))
        {
            $objProfissao = new EXTDAO_Profissao($db);
            $objProfissao->setNome($str);
            $objProfissao->formatarParaSQL();
            $msg = $objProfissao->insert(true, $idCorporacao, $idUsuarioLogado);

            if (Interface_mensagem::checkOk($msg))
            {
                return $msg->mValor;
            }
            else
            {
                HelperLog::logErro(null, $msg);
                return null;
            }
        }
        else
        {
            return $idProfissao;
        }
    }
}

?>
