import * as ReactDOM from 'react-dom';
import * as React from 'react';

import { ContextoGenerico } from 'lib/contexto';
import { Usuario, Empresa, PontoEletronico } from 'aplicacao/modulo/mapa/model';
import { DetalhesUsuario, DetalhesEmpresa, DetalhesPontoEletronico } from 'aplicacao/modulo/mapa/uiComponente';

export class ModalFactory
{
    public static getModalDetalhesUsuario(contexto: ContextoGenerico, usuario: Usuario) :DetalhesUsuario
    {
        const modalWindow = <DetalhesUsuario
                                modelUsuario={usuario}   
                                setDefaultBody 
                                setDefaultHeader />;
            
        return contexto.renderReactComponent<DetalhesUsuario>(modalWindow, '#modal-container');
    }

    public static getModalDetalhesEmpresa(contexto: ContextoGenerico, empresa: Empresa) :DetalhesEmpresa
    {
        const modalWindow = <DetalhesEmpresa            
                                modelEmpresa={empresa}                                      
                                setDefaultBody
                                setDefaultHeader />;

        return contexto.renderReactComponent<DetalhesEmpresa>(modalWindow, '#modal-container');
    }

    public static getModalDetalhesPontoEletronico(contexto: ContextoGenerico, pontoEletronico: PontoEletronico) :DetalhesPontoEletronico
    {
        const modalWindow = <DetalhesPontoEletronico    
                                modelPontoEletronico={pontoEletronico}                                      
                                setDefaultBody
                                setDefaultHeader />;

        return contexto.renderReactComponent<DetalhesPontoEletronico>(modalWindow, '#modal-container');
    }

}
