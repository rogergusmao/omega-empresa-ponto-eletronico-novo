<?php

/*

Arquivo gerado através de gerador de código em 24/05/2017 as 07:53:28.
Para que o arquivo não seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

Tabela correspondente: tipo_documento
Sobrescrita de dados: ---SOBRESCRITA_NAO_AUTORIZADA---

*/

?>

<?php

class EXTDAO_Tipo_documento extends DAO_Tipo_documento
{
    public function __construct($configDAO = null)
    {
        parent::__construct($configDAO);
        $this->nomeClasse = "EXTDAO_Tipo_documento";
    }

    public function factory()
    {
        return new EXTDAO_Tipo_documento();
    }

    public static function getIdTipoDocumento($nome, $idCorporacao, $db = null)
    {
        $q = "SELECT id "
            . " FROM tipo_documento "
            . " WHERE nome LIKE '{$nome}' "
            . " AND corporacao_id_INT = {$idCorporacao}";

        if ($db == null)
        {
            $db = new Database();
        }

        $db->query($q);
        return $db->getPrimeiraTuplaDoResultSet(0);
    }

    public static function cadastraTiposDeDocumento($idUsuarioAdm, $idCorporacao,
                                             $idUsuario, $imei, $idPrograma, $db = null, $vObjSincronizador = null)
    {
        if ($db == null)
        {
            $db = new Database();
        }

        $idTipoDocumentoSistemaTabela = EXTDAO_Sistema_tabela::getIdDaSistemaTabela("tipo_documento", $db);
        if (strlen($idUsuario) &&
            strlen($idTipoDocumentoSistemaTabela) &&
            strlen($idCorporacao) &&
            strlen($idUsuarioAdm)
        )
        {

            $vetorNomeTipoDocumento = array("CNPJ", "CPF", "RG");
            $objTipoDocumento = new EXTDAO_Tipo_documento($db);

            if (count($vetorNomeTipoDocumento) > 0)
            {
                if ($vObjSincronizador == null)
                {
                    $vObjSincronizador = new EXTDAO_Sistema_registro_sincronizador();
                }
                for ($index = 0; $index < count($vetorNomeTipoDocumento); $index++)
                {
                    $nomeTipoDocumento = $vetorNomeTipoDocumento[$index];
                    if (!strlen($nomeTipoDocumento))
                    {
                        continue;
                    }
                    else
                    {
                        $idTipoDocumento = EXTDAO_Tipo_documento::getIdTipoDocumento($nomeTipoDocumento, $idCorporacao, $db);
                        if (!strlen($idTipoDocumento))
                        {
                            $objTipoDocumento->clear();
                            $objTipoDocumento->setNome($nomeTipoDocumento);
                            $objTipoDocumento->formatarParaSQL();
                            $msg = $objTipoDocumento->insert(false, $idCorporacao);
                            if (!$msg->ok())
                            {
                                return $msg;
                            }
                            $idTipoDocumento = $msg->mValor;

                            $vObjSincronizador->inserirRegistro(
                                $idTipoDocumentoSistemaTabela, $idUsuarioAdm, $idCorporacao,
                                '0', DatabaseSincronizador::$INDEX_OPERACAO_EDIT,
                                $imei, $idPrograma, $idTipoDocumento);
                        }
                    }
                }

                return new Mensagem(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO);
            }
            else
            {
                return new Mensagem(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO);
            }
        }
        else
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_PARAMETRO_INVALIDO);
        }
    }
}

?>
