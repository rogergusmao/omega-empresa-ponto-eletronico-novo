import { RemoteModel } from 'lib/model';

export class PontoEletronico extends RemoteModel
{
    public idPonto :string;
    public nomePessoa :string;
    public idPessoa :string;
    public dataSec :string;
    public dataOffsec : string;
    public isEntrada :string;
}