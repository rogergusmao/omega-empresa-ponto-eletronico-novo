<?php


        /*

        Arquivo gerado através de gerador de código em 12/05/2018 as 15:01:34.
        Para que o arquivo não seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

        Tabela correspondente: sistema_tabela
        Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

        */
        


class DAO_Sistema_tabela extends Generic_DAO
                        {
                            const NOME_ENTIDADE = "sistema_tabela";

                            public static $nomeTabela = null;
                            public static $idSistemaTabela = false;
                            public static $databaseFieldsRelatedAttributes = null;
	                        public static $databaseFieldTypes = null;
	                        public static $databaseFieldNames = null;
	                        public static $listAliasTypes = null;
	                        public static $listAliasRelatedAttributes = null;
	                        
	                        public $id;
public $nome;
public $nomeExibicao;
public $dataModificacaoEstruturaDatetime;
public $frequenciaSincronizadorInt;
public $bancoMobileBoolean;
public $bancoWebBoolean;
public $transmissaoWebParaMobileBoolean;
public $transmissaoMobileParaWebBoolean;
public $tabelaSistemaBoolean;
public $excluidaBoolean;
public $atributosChaveUnica;

public $labelId;
public $labelNome;
public $labelNomeExibicao;
public $labelDataModificacaoEstruturaDatetime;
public $labelFrequenciaSincronizadorInt;
public $labelBancoMobileBoolean;
public $labelBancoWebBoolean;
public $labelTransmissaoWebParaMobileBoolean;
public $labelTransmissaoMobileParaWebBoolean;
public $labelTabelaSistemaBoolean;
public $labelExcluidaBoolean;
public $labelAtributosChaveUnica;

public static $arrPK = array("id");

public function __construct($configDAO = null)
        {
            parent::__construct($configDAO);
            static::__setStaticValues($this->database);           
        }

public static function idSistemaTabela($databaseObject = null){
            if(!(static::$idSistemaTabela))
            {
                static::$idSistemaTabela = EXTDAO_Sistema_tabela::getIdDaSistemaTabela(static::nomeTabela(), $databaseObject);
            }
            return static::$idSistemaTabela;
        }

public function getIdSistemaTabela(){
            return static::idSistemaTabela();
        }

public static function nomeTabela(){
            if(is_null(static::$nomeTabela))
            {
                static::$nomeTabela = "sistema_tabela";
            }
            return static::$nomeTabela;
        }

public function getNomeTabela(){
            return static::nomeTabela();
        }

public static function __setStaticValues($databaseObject = null)
                   {    
                        static::nomeTabela();
                        
                        static::idSistemaTabela($databaseObject);
                   
                        static::setDatabaseFieldNames();
                        static::setDatabaseFieldTypes();
                        static::setDatabaseFieldsRelatedAttributeNames();
                        
                        static::setListAliasTypes();
                        static::setListAliasRelatedAttributes();
                   }

public function getExpressaoLabel()
                   {
                        return "{id}";
                   }

public static function setDatabaseFieldNames()
        {
            if(is_null(static::$databaseFieldNames))
            {
                static::$databaseFieldNames = new stdClass();
                static::$databaseFieldNames->id = "id";
static::$databaseFieldNames->nome = "nome";
static::$databaseFieldNames->nomeExibicao = "nome_exibicao";
static::$databaseFieldNames->dataModificacaoEstruturaDatetime = "data_modificacao_estrutura_DATETIME";
static::$databaseFieldNames->frequenciaSincronizadorInt = "frequencia_sincronizador_INT";
static::$databaseFieldNames->bancoMobileBoolean = "banco_mobile_BOOLEAN";
static::$databaseFieldNames->bancoWebBoolean = "banco_web_BOOLEAN";
static::$databaseFieldNames->transmissaoWebParaMobileBoolean = "transmissao_web_para_mobile_BOOLEAN";
static::$databaseFieldNames->transmissaoMobileParaWebBoolean = "transmissao_mobile_para_web_BOOLEAN";
static::$databaseFieldNames->tabelaSistemaBoolean = "tabela_sistema_BOOLEAN";
static::$databaseFieldNames->excluidaBoolean = "excluida_BOOLEAN";
static::$databaseFieldNames->atributosChaveUnica = "atributos_chave_unica";

            }
                      
        }

public static function setDatabaseFieldTypes()
        {
            if(is_null(static::$databaseFieldTypes))
            {
                static::$databaseFieldTypes = new stdClass();
                static::$databaseFieldTypes->id = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->nome = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->nomeExibicao = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->dataModificacaoEstruturaDatetime = static::TIPO_VARIAVEL_DATE;
static::$databaseFieldTypes->frequenciaSincronizadorInt = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->bancoMobileBoolean = static::TIPO_VARIAVEL_BOOLEAN;
static::$databaseFieldTypes->bancoWebBoolean = static::TIPO_VARIAVEL_BOOLEAN;
static::$databaseFieldTypes->transmissaoWebParaMobileBoolean = static::TIPO_VARIAVEL_BOOLEAN;
static::$databaseFieldTypes->transmissaoMobileParaWebBoolean = static::TIPO_VARIAVEL_BOOLEAN;
static::$databaseFieldTypes->tabelaSistemaBoolean = static::TIPO_VARIAVEL_BOOLEAN;
static::$databaseFieldTypes->excluidaBoolean = static::TIPO_VARIAVEL_BOOLEAN;
static::$databaseFieldTypes->atributosChaveUnica = static::TIPO_VARIAVEL_TEXT;

            }
            
        }

public static function setDatabaseFieldsRelatedAttributeNames()
        {
            if(is_null(static::$databaseFieldsRelatedAttributes))
            {
                static::$databaseFieldsRelatedAttributes = new stdClass();
                static::$databaseFieldsRelatedAttributes->id = "id";
static::$databaseFieldsRelatedAttributes->nome = "nome";
static::$databaseFieldsRelatedAttributes->nome_exibicao = "nomeExibicao";
static::$databaseFieldsRelatedAttributes->data_modificacao_estrutura_DATETIME = "dataModificacaoEstruturaDatetime";
static::$databaseFieldsRelatedAttributes->frequencia_sincronizador_INT = "frequenciaSincronizadorInt";
static::$databaseFieldsRelatedAttributes->banco_mobile_BOOLEAN = "bancoMobileBoolean";
static::$databaseFieldsRelatedAttributes->banco_web_BOOLEAN = "bancoWebBoolean";
static::$databaseFieldsRelatedAttributes->transmissao_web_para_mobile_BOOLEAN = "transmissaoWebParaMobileBoolean";
static::$databaseFieldsRelatedAttributes->transmissao_mobile_para_web_BOOLEAN = "transmissaoMobileParaWebBoolean";
static::$databaseFieldsRelatedAttributes->tabela_sistema_BOOLEAN = "tabelaSistemaBoolean";
static::$databaseFieldsRelatedAttributes->excluida_BOOLEAN = "excluidaBoolean";
static::$databaseFieldsRelatedAttributes->atributos_chave_unica = "atributosChaveUnica";

            }           
        }

public function setAllFieldsToHumanFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["nome"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["nome_exibicao"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["data_modificacao_estrutura_DATETIME"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["frequencia_sincronizador_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["banco_mobile_BOOLEAN"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["banco_web_BOOLEAN"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["transmissao_web_para_mobile_BOOLEAN"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["transmissao_mobile_para_web_BOOLEAN"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["tabela_sistema_BOOLEAN"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["excluida_BOOLEAN"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["atributos_chave_unica"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
}

public function setAllFieldsToDatabaseFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["nome"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["nome_exibicao"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["data_modificacao_estrutura_DATETIME"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["frequencia_sincronizador_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["banco_mobile_BOOLEAN"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["banco_web_BOOLEAN"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["transmissao_web_para_mobile_BOOLEAN"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["transmissao_mobile_para_web_BOOLEAN"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["tabela_sistema_BOOLEAN"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["excluida_BOOLEAN"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["atributos_chave_unica"] = static::ESTADO_FORMATACAO_CAMPO_DB;
}



public function setLabels()
        {$this->labelId = "id";
$this->labelNome = "nome";
$this->labelNomeExibicao = "nomeexibicao";
$this->labelDataModificacaoEstruturaDatetime = "datamodificacaoestruturaDATETIME";
$this->labelFrequenciaSincronizadorInt = "frequenciasincronizadorINT";
$this->labelBancoMobileBoolean = "bancomobileBOOLEAN";
$this->labelBancoWebBoolean = "bancowebBOOLEAN";
$this->labelTransmissaoWebParaMobileBoolean = "transmissaowebparamobileBOOLEAN";
$this->labelTransmissaoMobileParaWebBoolean = "transmissaomobileparawebBOOLEAN";
$this->labelTabelaSistemaBoolean = "tabelasistemaBOOLEAN";
$this->labelExcluidaBoolean = "excluidaBOOLEAN";
$this->labelAtributosChaveUnica = "atributoschaveunica";
}

public function __actionAdd($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                $this->setByObject($parameters);
                $this->setDataCadastroAndOffsetInSecondsFromUser($parameters);
                
                $this->formatarParaSQL();
                $msg = $this->insert(true);
                
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                return new Mensagem(null, I18N::getExpression("Sistema tabela adicionado com sucesso."));
                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionEdit($parameters = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try {

                $this->setByObject($parameters);
                $this->setAllFieldsToHumanFormat();
                
                $this->formatarParaSQL();
                $msg = $this->updateByInstanceUsingParameters($this->id, $parameters, true);
                if($msg != null && $msg->erro())
                {
                     return $msg;
                }
        
                return new Mensagem(null, I18N::getExpression("Sistema tabela editado com sucesso."));
            }
            catch(Exception $ex)
            {                
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionRemove($parameters = null)
        {
            $successulRemovalCount = 0;
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                foreach($parameters as $record)
                {
                    if(is_numeric($record->id))
                    {
                        $msg = $this->delete($record->id, true);
                        if($msg == null || $msg->ok())
                        {
                            $successulRemovalCount++;
                        }
                    }
                }
                
                if($successulRemovalCount > 0)
                {
                    $successMessage = count($successulRemovalCount) > 1 ? I18N::getExpression("{0} Sistema tabela foram removidos com sucesso.", $successulRemovalCount) : I18N::getExpression("Sistema tabela removido com sucesso.");
                    return new Mensagem(null, $successMessage);
                }
                else
                {
                    $errorMessage = count($parameters) > 1 ? I18N::getExpression("Falha ao remover Sistema tabela.") : I18N::getExpression("Falha ao remover Sistema tabela.");
                    return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, $errorMessage);                    
                }
            }
            catch (Exception $ex) 
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }

public function __getRecord($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }
            
            try
            {            
                $returnObject = new stdClass();            
                $whereClause = static::getWhereClauseForFilter($parameters->filterParameters);
                $query = "SELECT  id, nome, nome_exibicao, data_modificacao_estrutura_DATETIME, frequencia_sincronizador_INT, banco_mobile_BOOLEAN, banco_web_BOOLEAN, transmissao_web_para_mobile_BOOLEAN, transmissao_mobile_para_web_BOOLEAN, tabela_sistema_BOOLEAN, excluida_BOOLEAN, atributos_chave_unica FROM sistema_tabela {$whereClause}";
    
                $msg = $this->database->queryMensagem($query);                
                if ($msg == null)
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Não foi encontrado nenhum registro"));
                    }                    
                    $resultSet = $this->database->result;
                    $resultObject = static::mapResultSetDataToRecord($resultSet);
                    
                    $returnObject->formData = $resultObject;
                    $returnObject->comboBoxesData = null;
                    if($parameters->loadComboBoxesData)
                    {
                        $this->setByObject($resultObject);
                        $returnObject->comboBoxesData =  $this->getFormComboBoxesData();
                    }                    
    
                    
    
                    return new Mensagem_generica($returnObject);
                } 
                else 
                {
                    return $msg;
                }
                
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }


        
        public function __getFilterComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
                
        public function __getFormComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
        
        protected function getFormComboBoxesData()
        {
            $comboBoxesData = new stdClass();
            $listParameters = new stdClass();
            $listParameters->listMappingType = static::LIST_MAPPING_TYPE_COMBOBOX;
            
            

            return $comboBoxesData;
        }
        
        


        
        public static function setListAliasTypes()
        {
            if(is_null(static::$listAliasTypes))
            {
                static::$listAliasTypes = new stdClass();
                static::$listAliasTypes->sistema_tabela__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->sistema_tabela__nome = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->sistema_tabela__nome_exibicao = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->sistema_tabela__data_modificacao_estrutura_DATETIME = static::TIPO_VARIAVEL_DATE;
static::$listAliasTypes->sistema_tabela__frequencia_sincronizador_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->sistema_tabela__banco_mobile_BOOLEAN = static::TIPO_VARIAVEL_BOOLEAN;
static::$listAliasTypes->sistema_tabela__banco_web_BOOLEAN = static::TIPO_VARIAVEL_BOOLEAN;
static::$listAliasTypes->sistema_tabela__transmissao_web_para_mobile_BOOLEAN = static::TIPO_VARIAVEL_BOOLEAN;
static::$listAliasTypes->sistema_tabela__transmissao_mobile_para_web_BOOLEAN = static::TIPO_VARIAVEL_BOOLEAN;
static::$listAliasTypes->sistema_tabela__tabela_sistema_BOOLEAN = static::TIPO_VARIAVEL_BOOLEAN;
static::$listAliasTypes->sistema_tabela__excluida_BOOLEAN = static::TIPO_VARIAVEL_BOOLEAN;
static::$listAliasTypes->sistema_tabela__atributos_chave_unica = static::TIPO_VARIAVEL_TEXT;
            }        
        }
        
        public static function setListAliasRelatedAttributes()
        {
            if(is_null(static::$listAliasRelatedAttributes))
            {
                static::$listAliasRelatedAttributes = new stdClass();
                static::$listAliasRelatedAttributes->sistema_tabela__id = "id";
static::$listAliasRelatedAttributes->sistema_tabela__nome = "nome";
static::$listAliasRelatedAttributes->sistema_tabela__nome_exibicao = "nomeExibicao";
static::$listAliasRelatedAttributes->sistema_tabela__data_modificacao_estrutura_DATETIME = "dataModificacaoEstruturaDatetime";
static::$listAliasRelatedAttributes->sistema_tabela__frequencia_sincronizador_INT = "frequenciaSincronizadorInt";
static::$listAliasRelatedAttributes->sistema_tabela__banco_mobile_BOOLEAN = "bancoMobileBoolean";
static::$listAliasRelatedAttributes->sistema_tabela__banco_web_BOOLEAN = "bancoWebBoolean";
static::$listAliasRelatedAttributes->sistema_tabela__transmissao_web_para_mobile_BOOLEAN = "transmissaoWebParaMobileBoolean";
static::$listAliasRelatedAttributes->sistema_tabela__transmissao_mobile_para_web_BOOLEAN = "transmissaoMobileParaWebBoolean";
static::$listAliasRelatedAttributes->sistema_tabela__tabela_sistema_BOOLEAN = "tabelaSistemaBoolean";
static::$listAliasRelatedAttributes->sistema_tabela__excluida_BOOLEAN = "excluidaBoolean";
static::$listAliasRelatedAttributes->sistema_tabela__atributos_chave_unica = "atributosChaveUnica";
            }         
        }

        public function __getList($parameters = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try
            {
                $filterParameters = null;
                if(isset($parameters->filterParameters))
                {
                    $filterParameters = $parameters->filterParameters;
                }
                                    
                    
                
                $sortingParameters = null;
                if(isset($parameters->sortingParameters))
                {
                    $sortingParameters = $parameters->sortingParameters;
                }
                
                $paginationParameters = null;
                if(isset($parameters->paginationParameters))
                {
                    $paginationParameters = $parameters->paginationParameters;
                }
                
                $listMappingType = static::getListMappingType($parameters->listMappingType);
                $mainTableAlias = "st";

                $whereClause = static::getWhereClauseForFilter($filterParameters, $mainTableAlias);
                $orderByClause = static::getOrderByClauseForFilter($sortingParameters, $mainTableAlias);
                $limitClause = static::getLimitClauseForFilter($paginationParameters);

                $queryWithoutLimit = "SELECT st.id FROM sistema_tabela st {$whereClause}";
                $query = "SELECT st.id AS sistema_tabela__id, st.nome AS sistema_tabela__nome, st.nome_exibicao AS sistema_tabela__nome_exibicao, st.data_modificacao_estrutura_DATETIME AS sistema_tabela__data_modificacao_estrutura_DATETIME, st.frequencia_sincronizador_INT AS sistema_tabela__frequencia_sincronizador_INT, st.banco_mobile_BOOLEAN AS sistema_tabela__banco_mobile_BOOLEAN, st.banco_web_BOOLEAN AS sistema_tabela__banco_web_BOOLEAN, st.transmissao_web_para_mobile_BOOLEAN AS sistema_tabela__transmissao_web_para_mobile_BOOLEAN, st.transmissao_mobile_para_web_BOOLEAN AS sistema_tabela__transmissao_mobile_para_web_BOOLEAN, st.tabela_sistema_BOOLEAN AS sistema_tabela__tabela_sistema_BOOLEAN, st.excluida_BOOLEAN AS sistema_tabela__excluida_BOOLEAN, st.atributos_chave_unica AS sistema_tabela__atributos_chave_unica FROM sistema_tabela st  {$whereClause} {$orderByClause} {$limitClause}";

                $msg = $this->database->queryMensagem($queryWithoutLimit);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                
                $totalNumberOfRecords = $this->database->rows();
                if($totalNumberOfRecords == 0)
                {
                    return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                }                        
                        
                $msg = $this->database->queryMensagem($query);
                
                //operacao realizaca com sucesso
                if ($msg == null) 
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                    }                        
                
                    $resultSet = $this->database->result;

                    $objReturn = new stdClass();
                    $objReturn->dataSet = static::mapResultSetDataToList($resultSet, $listMappingType, static::$listAliasTypes);

                    if(!Helper::isNullOrEmpty($paginationParameters->recordsPerPage) && !Helper::isNullOrEmpty($paginationParameters->currentPage))
                    {
                        $lastPage = ceil($totalNumberOfRecords / $paginationParameters->recordsPerPage);
                        $objReturn->paginationParameters = new PaginationParams($paginationParameters->recordsPerPage, $paginationParameters->currentPage, $lastPage);
                    }
                    
                    return new Mensagem_generica($objReturn);
                } 
                else
                {
                    return $msg;
                }                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }



public function getId()
            {
                return $this->id;
            }

public function getNome()
            {
                return $this->nome;
            }

public function getNomeExibicao()
            {
                return $this->nomeExibicao;
            }

public function getNome_exibicao()
                {
                    return $this->nomeExibicao;
                }

public function getDataModificacaoEstruturaDatetime()
            {
                return $this->dataModificacaoEstruturaDatetime;
            }

public function getData_modificacao_estrutura_DATETIME()
                {
                    return $this->dataModificacaoEstruturaDatetime;
                }

public function getFrequenciaSincronizadorInt()
            {
                return $this->frequenciaSincronizadorInt;
            }

public function getFrequencia_sincronizador_INT()
                {
                    return $this->frequenciaSincronizadorInt;
                }

public function getBancoMobileBoolean()
            {
                return $this->bancoMobileBoolean;
            }

public function getBanco_mobile_BOOLEAN()
                {
                    return $this->bancoMobileBoolean;
                }

public function getBancoWebBoolean()
            {
                return $this->bancoWebBoolean;
            }

public function getBanco_web_BOOLEAN()
                {
                    return $this->bancoWebBoolean;
                }

public function getTransmissaoWebParaMobileBoolean()
            {
                return $this->transmissaoWebParaMobileBoolean;
            }

public function getTransmissao_web_para_mobile_BOOLEAN()
                {
                    return $this->transmissaoWebParaMobileBoolean;
                }

public function getTransmissaoMobileParaWebBoolean()
            {
                return $this->transmissaoMobileParaWebBoolean;
            }

public function getTransmissao_mobile_para_web_BOOLEAN()
                {
                    return $this->transmissaoMobileParaWebBoolean;
                }

public function getTabelaSistemaBoolean()
            {
                return $this->tabelaSistemaBoolean;
            }

public function getTabela_sistema_BOOLEAN()
                {
                    return $this->tabelaSistemaBoolean;
                }

public function getExcluidaBoolean()
            {
                return $this->excluidaBoolean;
            }

public function getExcluida_BOOLEAN()
                {
                    return $this->excluidaBoolean;
                }

public function getAtributosChaveUnica()
            {
                return $this->atributosChaveUnica;
            }

public function getAtributos_chave_unica()
                {
                    return $this->atributosChaveUnica;
                }



function setId($value)
            {
                $this->id = $value;
            }

function setNome($value)
            {
                $this->nome = $value;
            }

function setNomeExibicao($value)
            {
                $this->nomeExibicao = $value;
            }

public function setNome_exibicao($value)
                { 
                    $this->nomeExibicao = $value; 
                }

function setDataModificacaoEstruturaDatetime($value)
            {
                $this->dataModificacaoEstruturaDatetime = $value;
            }

public function setData_modificacao_estrutura_DATETIME($value)
                { 
                    $this->dataModificacaoEstruturaDatetime = $value; 
                }

function setFrequenciaSincronizadorInt($value)
            {
                $this->frequenciaSincronizadorInt = $value;
            }

public function setFrequencia_sincronizador_INT($value)
                { 
                    $this->frequenciaSincronizadorInt = $value; 
                }

function setBancoMobileBoolean($value)
            {
                $this->bancoMobileBoolean = $value;
            }

public function setBanco_mobile_BOOLEAN($value)
                { 
                    $this->bancoMobileBoolean = $value; 
                }

function setBancoWebBoolean($value)
            {
                $this->bancoWebBoolean = $value;
            }

public function setBanco_web_BOOLEAN($value)
                { 
                    $this->bancoWebBoolean = $value; 
                }

function setTransmissaoWebParaMobileBoolean($value)
            {
                $this->transmissaoWebParaMobileBoolean = $value;
            }

public function setTransmissao_web_para_mobile_BOOLEAN($value)
                { 
                    $this->transmissaoWebParaMobileBoolean = $value; 
                }

function setTransmissaoMobileParaWebBoolean($value)
            {
                $this->transmissaoMobileParaWebBoolean = $value;
            }

public function setTransmissao_mobile_para_web_BOOLEAN($value)
                { 
                    $this->transmissaoMobileParaWebBoolean = $value; 
                }

function setTabelaSistemaBoolean($value)
            {
                $this->tabelaSistemaBoolean = $value;
            }

public function setTabela_sistema_BOOLEAN($value)
                { 
                    $this->tabelaSistemaBoolean = $value; 
                }

function setExcluidaBoolean($value)
            {
                $this->excluidaBoolean = $value;
            }

public function setExcluida_BOOLEAN($value)
                { 
                    $this->excluidaBoolean = $value; 
                }

function setAtributosChaveUnica($value)
            {
                $this->atributosChaveUnica = $value;
            }

public function setAtributos_chave_unica($value)
                { 
                    $this->atributosChaveUnica = $value; 
                }



public function clear()
        {$this->id = null;
$this->nome = null;
$this->nomeExibicao = null;
$this->dataModificacaoEstruturaDatetime = null;
$this->frequenciaSincronizadorInt = null;
$this->bancoMobileBoolean = null;
$this->bancoWebBoolean = null;
$this->transmissaoWebParaMobileBoolean = null;
$this->transmissaoMobileParaWebBoolean = null;
$this->tabelaSistemaBoolean = null;
$this->excluidaBoolean = null;
$this->atributosChaveUnica = null;
if(is_array($this->arrEstadosCampos)){
                        unset($this->arrEstadosCampos);
                     $this->arrEstadosCampos = array();
                    }
                }

public function formatarParaSQL()
        {
if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->id)){
$this->id = $this->formatarIntegerParaComandoSQL($this->id);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->id);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->nome)){
$this->nome = $this->formatarStringParaComandoSQL($this->nome);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->nome);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->nomeExibicao)){
$this->nomeExibicao = $this->formatarStringParaComandoSQL($this->nomeExibicao);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->nomeExibicao);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->dataModificacaoEstruturaDatetime)){
$this->dataModificacaoEstruturaDatetime = $this->formatarDataParaExibicao($this->dataModificacaoEstruturaDatetime);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->dataModificacaoEstruturaDatetime);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->frequenciaSincronizadorInt)){
$this->frequenciaSincronizadorInt = $this->formatarIntegerParaComandoSQL($this->frequenciaSincronizadorInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->frequenciaSincronizadorInt);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->bancoMobileBoolean)){
$this->bancoMobileBoolean = $this->formatarBooleanParaComandoSQL($this->bancoMobileBoolean);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->bancoMobileBoolean);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->bancoWebBoolean)){
$this->bancoWebBoolean = $this->formatarBooleanParaComandoSQL($this->bancoWebBoolean);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->bancoWebBoolean);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->transmissaoWebParaMobileBoolean)){
$this->transmissaoWebParaMobileBoolean = $this->formatarBooleanParaComandoSQL($this->transmissaoWebParaMobileBoolean);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->transmissaoWebParaMobileBoolean);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->transmissaoMobileParaWebBoolean)){
$this->transmissaoMobileParaWebBoolean = $this->formatarBooleanParaComandoSQL($this->transmissaoMobileParaWebBoolean);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->transmissaoMobileParaWebBoolean);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->tabelaSistemaBoolean)){
$this->tabelaSistemaBoolean = $this->formatarBooleanParaComandoSQL($this->tabelaSistemaBoolean);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->tabelaSistemaBoolean);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->excluidaBoolean)){
$this->excluidaBoolean = $this->formatarBooleanParaComandoSQL($this->excluidaBoolean);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->excluidaBoolean);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->atributosChaveUnica)){
$this->atributosChaveUnica = $this->formatarStringParaComandoSQL($this->atributosChaveUnica);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->atributosChaveUnica);
}

}

public function formatarParaExibicao()
        {if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->nome)){
$this->nome = $this->formatarStringParaExibicao($this->nome);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->nome);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->nomeExibicao)){
$this->nomeExibicao = $this->formatarStringParaExibicao($this->nomeExibicao);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->nomeExibicao);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->dataModificacaoEstruturaDatetime)){
$this->dataModificacaoEstruturaDatetime = $this->formatarDataParaExibicao($this->dataModificacaoEstruturaDatetime);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->dataModificacaoEstruturaDatetime);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->bancoMobileBoolean)){
$this->bancoMobileBoolean = $this->formatarBooleanParaExibicao($this->bancoMobileBoolean);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->bancoMobileBoolean);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->bancoWebBoolean)){
$this->bancoWebBoolean = $this->formatarBooleanParaExibicao($this->bancoWebBoolean);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->bancoWebBoolean);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->transmissaoWebParaMobileBoolean)){
$this->transmissaoWebParaMobileBoolean = $this->formatarBooleanParaExibicao($this->transmissaoWebParaMobileBoolean);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->transmissaoWebParaMobileBoolean);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->transmissaoMobileParaWebBoolean)){
$this->transmissaoMobileParaWebBoolean = $this->formatarBooleanParaExibicao($this->transmissaoMobileParaWebBoolean);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->transmissaoMobileParaWebBoolean);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->tabelaSistemaBoolean)){
$this->tabelaSistemaBoolean = $this->formatarBooleanParaExibicao($this->tabelaSistemaBoolean);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->tabelaSistemaBoolean);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->excluidaBoolean)){
$this->excluidaBoolean = $this->formatarBooleanParaExibicao($this->excluidaBoolean);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->excluidaBoolean);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->atributosChaveUnica)){
$this->atributosChaveUnica = $this->formatarStringParaExibicao($this->atributosChaveUnica);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->atributosChaveUnica);
}

}

function select($id, $idCorporacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }   

            $sql = "SELECT id, nome, nome_exibicao, data_modificacao_estrutura_DATETIME, frequencia_sincronizador_INT, banco_mobile_BOOLEAN, banco_web_BOOLEAN, transmissao_web_para_mobile_BOOLEAN, transmissao_mobile_para_web_BOOLEAN, tabela_sistema_BOOLEAN, excluida_BOOLEAN, atributos_chave_unica FROM sistema_tabela WHERE id = $id";

            $msg = $this->database->queryMensagem($sql);
            $result = $this->database->result;

            if($msg != null && $msg->erro() )
            { 
            		$this->clear();
            		return $msg;
        	} 
        	else if($this->database->rows() == 0)
        	{
                return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nenhum registro foi encontrado."));
            }
        

            $row = $this->database->fetchArray(MYSQLI_NUM);
            
            		$this->id = $row[0];
		$this->nome = $row[1];
		$this->nomeExibicao = $row[2];
		$this->dataModificacaoEstruturaDatetime = $row[3];
		$this->frequenciaSincronizadorInt = $row[4];
		$this->bancoMobileBoolean = $row[5];
		$this->bancoWebBoolean = $row[6];
		$this->transmissaoWebParaMobileBoolean = $row[7];
		$this->transmissaoMobileParaWebBoolean = $row[8];
		$this->tabelaSistemaBoolean = $row[9];
		$this->excluidaBoolean = $row[10];
		$this->atributosChaveUnica = $row[11];

            
        }

public function delete($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (is_null($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }

            $sql = "DELETE FROM sistema_tabela WHERE id= $id";
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                    && $sincronizar
                        && static::idSistemaTabela() != null
                            && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_REMOVE,
                                null,
                                null,
                                $id,
                                $this->database);
            }

            return $msg;
        }

public function insert($sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null, $opcoes = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            
            
            if (is_null($opcoes))
            {
                $opcoes = $this->opcoesDAO;
            }

            if (!is_numeric($this->id))
            {
                $this->id = "null";
                if ($opcoes == null || $opcoes[Database::OPCAO_GERAR_ID] != false) 
                {
                    $this->id = EXTDAO_Sistema_sequencia::gerarId(static::nomeTabela());                    
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }    
                    else if($msg == null)
                    {
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
                else 
                {
                    $this->id = null;
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao));
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }
                    else if($msg == null)
                    {
                        $this->id = $this->database->getLastInsertId();
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
            }
            else 
            {
                $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }                        
                else if($msg == null)
                {
                   $msg = new Mensagem_token();
                   $msg->mValor = $this->id;
                }
            }
            if($msg == null || $msg->ok())
            {                
                if($sincronizar
                    && static::idSistemaTabela() != null
                    && isset($this->id)){
                    
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_INSERT,
                                null,
                                null,
                                $this->id,
                                $this->database); 
                	}
            } 
            else
            {
                $this->id = null;
            }
            
            return $msg;
            
        }

public function getSQLInsert($idCorporacao){ 

return "INSERT INTO sistema_tabela (id, nome, nome_exibicao, data_modificacao_estrutura_DATETIME, frequencia_sincronizador_INT, banco_mobile_BOOLEAN, banco_web_BOOLEAN, transmissao_web_para_mobile_BOOLEAN, transmissao_mobile_para_web_BOOLEAN, tabela_sistema_BOOLEAN, excluida_BOOLEAN, atributos_chave_unica) VALUES ( $this->id ,  $this->nome ,  $this->nomeExibicao ,  $this->dataModificacaoEstruturaDatetime ,  $this->frequenciaSincronizadorInt ,  $this->bancoMobileBoolean ,  $this->bancoWebBoolean ,  $this->transmissaoWebParaMobileBoolean ,  $this->transmissaoMobileParaWebBoolean ,  $this->tabelaSistemaBoolean ,  $this->excluidaBoolean ,  $this->atributosChaveUnica ) "; 

}




        
        public function updateByParameters($id, $objParametros, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao))
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();

            
                
                if (isset($objParametros->nome)) 
                {
                    $arrUpdateFields[] = " nome = {$objParametros->nome} ";
                }


                
                if (isset($objParametros->nomeExibicao)) 
                {
                    $arrUpdateFields[] = " nome_exibicao = {$objParametros->nomeExibicao} ";
                }


                
                if (isset($objParametros->dataModificacaoEstruturaDatetime)) 
                {
                    $arrUpdateFields[] = " data_modificacao_estrutura_DATETIME = {$objParametros->dataModificacaoEstruturaDatetime} ";
                }


                
                if (isset($objParametros->frequenciaSincronizadorInt)) 
                {
                    $arrUpdateFields[] = " frequencia_sincronizador_INT = {$objParametros->frequenciaSincronizadorInt} ";
                }


                
                if (isset($objParametros->bancoMobileBoolean)) 
                {
                    $arrUpdateFields[] = " banco_mobile_BOOLEAN = {$objParametros->bancoMobileBoolean} ";
                }


                
                if (isset($objParametros->bancoWebBoolean)) 
                {
                    $arrUpdateFields[] = " banco_web_BOOLEAN = {$objParametros->bancoWebBoolean} ";
                }


                
                if (isset($objParametros->transmissaoWebParaMobileBoolean)) 
                {
                    $arrUpdateFields[] = " transmissao_web_para_mobile_BOOLEAN = {$objParametros->transmissaoWebParaMobileBoolean} ";
                }


                
                if (isset($objParametros->transmissaoMobileParaWebBoolean)) 
                {
                    $arrUpdateFields[] = " transmissao_mobile_para_web_BOOLEAN = {$objParametros->transmissaoMobileParaWebBoolean} ";
                }


                
                if (isset($objParametros->tabelaSistemaBoolean)) 
                {
                    $arrUpdateFields[] = " tabela_sistema_BOOLEAN = {$objParametros->tabelaSistemaBoolean} ";
                }


                
                if (isset($objParametros->excluidaBoolean)) 
                {
                    $arrUpdateFields[] = " excluida_BOOLEAN = {$objParametros->excluidaBoolean} ";
                }


                
                if (isset($objParametros->atributosChaveUnica)) 
                {
                    $arrUpdateFields[] = " atributos_chave_unica = {$objParametros->atributosChaveUnica} ";
                }



            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE sistema_tabela SET {$strUpdateFields} WHERE id = {$id}";

            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null)
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function update($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($this->nome)) 
                {                                      
                    $arrUpdateFields[] = " nome = {$this->nome} ";
                }


                
                if (isset($this->nomeExibicao)) 
                {                                      
                    $arrUpdateFields[] = " nome_exibicao = {$this->nomeExibicao} ";
                }


                
                if (isset($this->dataModificacaoEstruturaDatetime)) 
                {                                      
                    $arrUpdateFields[] = " data_modificacao_estrutura_DATETIME = {$this->dataModificacaoEstruturaDatetime} ";
                }


                
                if (isset($this->frequenciaSincronizadorInt)) 
                {                                      
                    $arrUpdateFields[] = " frequencia_sincronizador_INT = {$this->frequenciaSincronizadorInt} ";
                }


                
                if (isset($this->bancoMobileBoolean)) 
                {                                      
                    $arrUpdateFields[] = " banco_mobile_BOOLEAN = {$this->bancoMobileBoolean} ";
                }


                
                if (isset($this->bancoWebBoolean)) 
                {                                      
                    $arrUpdateFields[] = " banco_web_BOOLEAN = {$this->bancoWebBoolean} ";
                }


                
                if (isset($this->transmissaoWebParaMobileBoolean)) 
                {                                      
                    $arrUpdateFields[] = " transmissao_web_para_mobile_BOOLEAN = {$this->transmissaoWebParaMobileBoolean} ";
                }


                
                if (isset($this->transmissaoMobileParaWebBoolean)) 
                {                                      
                    $arrUpdateFields[] = " transmissao_mobile_para_web_BOOLEAN = {$this->transmissaoMobileParaWebBoolean} ";
                }


                
                if (isset($this->tabelaSistemaBoolean)) 
                {                                      
                    $arrUpdateFields[] = " tabela_sistema_BOOLEAN = {$this->tabelaSistemaBoolean} ";
                }


                
                if (isset($this->excluidaBoolean)) 
                {                                      
                    $arrUpdateFields[] = " excluida_BOOLEAN = {$this->excluidaBoolean} ";
                }


                
                if (isset($this->atributosChaveUnica)) 
                {                                      
                    $arrUpdateFields[] = " atributos_chave_unica = {$this->atributosChaveUnica} ";
                }


            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE sistema_tabela SET {$strUpdateFields} WHERE id = {$id}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function updateByInstanceUsingParameters($id, $parameters, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($parameters->nome)) 
                {                                      
                    $arrUpdateFields[] = " nome = {$this->nome} ";
                }
                
                if (isset($parameters->nomeExibicao)) 
                {                                      
                    $arrUpdateFields[] = " nome_exibicao = {$this->nomeExibicao} ";
                }
                
                if (isset($parameters->dataModificacaoEstruturaDatetime)) 
                {                                      
                    $arrUpdateFields[] = " data_modificacao_estrutura_DATETIME = {$this->dataModificacaoEstruturaDatetime} ";
                }
                
                if (isset($parameters->frequenciaSincronizadorInt)) 
                {                                      
                    $arrUpdateFields[] = " frequencia_sincronizador_INT = {$this->frequenciaSincronizadorInt} ";
                }
                
                if (isset($parameters->bancoMobileBoolean)) 
                {                                      
                    $arrUpdateFields[] = " banco_mobile_BOOLEAN = {$this->bancoMobileBoolean} ";
                }
                
                if (isset($parameters->bancoWebBoolean)) 
                {                                      
                    $arrUpdateFields[] = " banco_web_BOOLEAN = {$this->bancoWebBoolean} ";
                }
                
                if (isset($parameters->transmissaoWebParaMobileBoolean)) 
                {                                      
                    $arrUpdateFields[] = " transmissao_web_para_mobile_BOOLEAN = {$this->transmissaoWebParaMobileBoolean} ";
                }
                
                if (isset($parameters->transmissaoMobileParaWebBoolean)) 
                {                                      
                    $arrUpdateFields[] = " transmissao_mobile_para_web_BOOLEAN = {$this->transmissaoMobileParaWebBoolean} ";
                }
                
                if (isset($parameters->tabelaSistemaBoolean)) 
                {                                      
                    $arrUpdateFields[] = " tabela_sistema_BOOLEAN = {$this->tabelaSistemaBoolean} ";
                }
                
                if (isset($parameters->excluidaBoolean)) 
                {                                      
                    $arrUpdateFields[] = " excluida_BOOLEAN = {$this->excluidaBoolean} ";
                }
                
                if (isset($parameters->atributosChaveUnica)) 
                {                                      
                    $arrUpdateFields[] = " atributos_chave_unica = {$this->atributosChaveUnica} ";
                }
            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE sistema_tabela SET {$strUpdateFields} WHERE id = {$id}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }

}

?>
