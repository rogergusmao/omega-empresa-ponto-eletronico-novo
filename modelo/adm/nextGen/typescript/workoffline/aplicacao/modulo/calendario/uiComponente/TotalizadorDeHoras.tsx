import * as React from 'react';
import { Label, Alert } from 'react-bootstrap';

import * as FullCalendar from 'fullcalendar';
import * as FullCalendarInputTypes from 'fullcalendar/src/types/input-types';

import { ReactProps } from 'lib/geral';
import { ReactComponent, DomContainer } from 'lib/uiComponente';
import { Pessoa } from 'aplicacao/modulo/calendario/model';

export class TotalizadorDeHoras extends ReactComponent<TotalizadorDeHoras.TotalizadorDeHorasProps, TotalizadorDeHoras.TotalizadorDeHorasState>
{
    private dadosTotalizador :Array<Pessoa>;

    public constructor(props: TotalizadorDeHoras.TotalizadorDeHorasProps, reactContext: any)
    { 
        super(props, reactContext);  
        this.dadosTotalizador = new Array<Pessoa>();
    }

    public fixLayout()
    {
        var maxHeight: number = DomContainer.getMaxAllowedHeight();            
    }

    public setData(pessoas :Array<Pessoa>)
    {
        this.dadosTotalizador = pessoas;
        this.setState({ dadosAtualizados: true });
    }

    public render()
    {
        if(this.dadosTotalizador.length > 0)
        {
            return (                     
                <div className="row">
                    <div className="col-sm-12">  
                        {this.dadosTotalizador.map((value) => (  
                            <div key={value.nomePessoa} className="box-content box-statistic">
                                <h3 className="title" style={ { color: value.corBackground.getHexValue() } }>{ value.getTempoTotalFormatado() }</h3>
                                <small>{value.nomePessoa}</small>
                                <div className="icon-user align-right" style={ { color: value.corBackground.getHexValue() } }></div>
                            </div> 
                        )) }  
                    </div>
                </div>                 
            );
        }
        else
        {
            return (<Alert bsStyle="warning" className="mb-0">
                        <strong></strong> Nenhum ponto encontrado no período
                    </Alert>);
        }
        
        
    }

}

export namespace TotalizadorDeHoras
{
    export interface TotalizadorDeHorasProps extends ReactProps
    {
        
    }

    export interface TotalizadorDeHorasState
    {
        dadosAtualizados :boolean;
    }

}