<?php

/*

Arquivo gerado atravÃ©s de gerador de cÃ³digo em 02/03/2017 as 19:48:07.
Para que o arquivo nÃ£o seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

Tabela correspondente: sistema_tabela
Sobrescrita de dados: ---SOBRESCRITA_NAO_AUTORIZADA---

*/

class EXTDAO_Sistema_tabela extends DAO_Sistema_tabela
{
    static $IDS_TABELAS = array();

    public function __construct($configDAO = null)
    {
        parent::__construct($configDAO);
        $this->nomeClasse = "EXTDAO_Sistema_tabela";
    }

    public function factory()
    {
        return new EXTDAO_Sistema_tabela();
    }

    public static function getTabelasDownload($db = null)
    {
        $q = "SELECT DISTINCT(st.id)"
            . " FROM sistema_tabela st"
            . " WHERE st.transmissao_web_para_mobile_BOOLEAN = '1' "
            . " AND st.frequencia_sincronizador_INT = '-1' ";

        if ($db == null)
        {
            $db = new Database();
        }

        $db->query($q);
        $ids = Helper::getResultSetToArrayDeUmCampo($db->result);

        return $ids;
    }

    public static function getTabelasUpload($db = null)
    {
        $q = "SELECT DISTINCT(st.id)"
            . " FROM sistema_tabela st"
            . " WHERE st.transmissao_mobile_para_web_BOOLEAN = '1' "
            . " AND st.frequencia_sincronizador_INT = '-1' ";

        if ($db == null)
        {
            $db = new Database();
        }

        $db->query($q);
        $ids = Helper::getResultSetToArrayDeUmCampo($db->result);

        return $ids;
    }

    public static function getNomeDaTabela($idSistemaTabela, $db = null)
    {
        $db = new Database();
        $db->query("SELECT nome "
                   . " FROM sistema_tabela"
                   . " WHERE id=$idSistemaTabela");
        $id = $db->getPrimeiraTuplaDoResultSet(0);

        return $id;
    }

    public static function getTabelasASeremSincronizadas($idUltimoRegistroIncluso, $db = null)
    {
        $q = "SELECT DISTINCT(sistema_tabela_id_INT) "
            . " FROM sistema_registro_sincronizador ";
        if ($idUltimoRegistroIncluso != null)
        {
            $q .= " WHERE id <= $idUltimoRegistroIncluso ";
        }
        if ($db == null)
        {
            $db = new Database();
        }
        $db->query($q);
        $ids = Helper::getResultSetToArrayDeUmCampo($db->result);

        return $ids;
    }

    public static function getIdDaSistemaTabela($pNomeTabela, $db = null)
    {

        if (!empty($pNomeTabela))
        {
            if (isset($IDS_TABELAS[$pNomeTabela]))
            {
                return $IDS_TABELAS[$pNomeTabela];
            }
            if ($db == null)
            {
                $objBanco = new Database();
            }
            else
            {
                $objBanco = $db;
            }

            //checa a existencia do funcionario
            $query = "SELECT ts.id as id
                              FROM sistema_tabela ts
                              WHERE ts.nome = '" . Helper::formatarCampoTextoHTMLParaSQL($pNomeTabela) . "' ";

            $objBanco->query($query);
            $id = $objBanco->getPrimeiraTuplaDoResultSet("id");

            //o usuario ja eh existente
            if ($db == null)
            {
                $objBanco->close();
            }

            if (strlen($id))
            {
                $IDS_TABELAS[$pNomeTabela] = $id;

                return $id;
            }
            else
            {
                $IDS_TABELAS[$pNomeTabela] = null;

                return null;
            }
        }
    }
}

?>
