export class Dimensoes
{
    public largura: string;
    public altura: string;

    constructor(largura: string, altura: string);
    constructor(largura: number, altura: number);
    constructor(largura?: any, altura?: any)
    {
        this.largura = (typeof largura == 'string') ? largura : largura + 'px';
        this.altura = (typeof altura == 'string') ? altura : altura + 'px';
    }

}