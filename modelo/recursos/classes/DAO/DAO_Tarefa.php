<?php


        /*

        Arquivo gerado através de gerador de código em 12/05/2018 as 14:18:07.
        Para que o arquivo não seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

        Tabela correspondente: tarefa
        Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

        */
        


class DAO_Tarefa extends Generic_DAO
                        {
                            const NOME_ENTIDADE = "tarefa";

                            public static $nomeTabela = null;
                            public static $idSistemaTabela = false;
                            public static $databaseFieldsRelatedAttributes = null;
	                        public static $databaseFieldTypes = null;
	                        public static $databaseFieldNames = null;
	                        public static $listAliasTypes = null;
	                        public static $listAliasRelatedAttributes = null;
	                        
	                        public $id;
public $criadoPeloUsuarioId;
public $categoriaPermissaoId;
public $veiculoId;
public $usuarioId;
public $empresaEquipeId;
public $origemPessoaId;
public $origemEmpresaId;
public $origemLogradouro;
public $origemNumero;
public $origemCidadeId;
public $origemLatitudeInt;
public $origemLongitudeInt;
public $origemLatitudeRealInt;
public $origemLongitudeRealInt;
public $destinoPessoaId;
public $destinoEmpresaId;
public $destinoLogradouro;
public $destinoNumero;
public $destinoCidadeId;
public $destinoLatitudeInt;
public $destinoLongitudeInt;
public $destinoLatitudeRealInt;
public $destinoLongitudeRealInt;
public $tempoEstimadoCarroInt;
public $tempoEstimadoAPeInt;
public $distanciaEstimadaCarroInt;
public $distanciaEstimadaAPeInt;
public $inicioHoraProgramadaSec;
public $inicioHoraProgramadaOffsec;
public $dataExibirSec;
public $dataExibirOffsec;
public $inicioSec;
public $inicioOffsec;
public $fimSec;
public $fimOffsec;
public $cadastroSec;
public $cadastroOffsec;
public $titulo;
public $tituloNormalizado;
public $descricao;
public $empresaAtividadeId;
public $tipoTarefaId;
public $empresaVendaId;
public $idPrioridadeInt;
public $registroEstadoId;
public $registroEstadoCorporacaoId;
public $corporacaoId;
public $percentualCompletoInt;
public $prazoSec;
public $prazoOffsec;
public $atividadeId;
public $protocoloEmpresaVendaInt;
public $protocoloEmpresaAtividadeVendaInt;

public $labelId;
public $labelCriadoPeloUsuarioId;
public $labelCategoriaPermissaoId;
public $labelVeiculoId;
public $labelUsuarioId;
public $labelEmpresaEquipeId;
public $labelOrigemPessoaId;
public $labelOrigemEmpresaId;
public $labelOrigemLogradouro;
public $labelOrigemNumero;
public $labelOrigemCidadeId;
public $labelOrigemLatitudeInt;
public $labelOrigemLongitudeInt;
public $labelOrigemLatitudeRealInt;
public $labelOrigemLongitudeRealInt;
public $labelDestinoPessoaId;
public $labelDestinoEmpresaId;
public $labelDestinoLogradouro;
public $labelDestinoNumero;
public $labelDestinoCidadeId;
public $labelDestinoLatitudeInt;
public $labelDestinoLongitudeInt;
public $labelDestinoLatitudeRealInt;
public $labelDestinoLongitudeRealInt;
public $labelTempoEstimadoCarroInt;
public $labelTempoEstimadoAPeInt;
public $labelDistanciaEstimadaCarroInt;
public $labelDistanciaEstimadaAPeInt;
public $labelInicioHoraProgramadaSec;
public $labelInicioHoraProgramadaOffsec;
public $labelDataExibirSec;
public $labelDataExibirOffsec;
public $labelInicioSec;
public $labelInicioOffsec;
public $labelFimSec;
public $labelFimOffsec;
public $labelCadastroSec;
public $labelCadastroOffsec;
public $labelTitulo;
public $labelTituloNormalizado;
public $labelDescricao;
public $labelEmpresaAtividadeId;
public $labelTipoTarefaId;
public $labelEmpresaVendaId;
public $labelIdPrioridadeInt;
public $labelRegistroEstadoId;
public $labelRegistroEstadoCorporacaoId;
public $labelCorporacaoId;
public $labelPercentualCompletoInt;
public $labelPrazoSec;
public $labelPrazoOffsec;
public $labelAtividadeId;
public $labelProtocoloEmpresaVendaInt;
public $labelProtocoloEmpresaAtividadeVendaInt;

public static $arrPK = array("id");

public function __construct($configDAO = null)
        {
            parent::__construct($configDAO);
            static::__setStaticValues($this->database);           
        }

public static function idSistemaTabela($databaseObject = null){
            if(!(static::$idSistemaTabela))
            {
                static::$idSistemaTabela = EXTDAO_Sistema_tabela::getIdDaSistemaTabela(static::nomeTabela(), $databaseObject);
            }
            return static::$idSistemaTabela;
        }

public function getIdSistemaTabela(){
            return static::idSistemaTabela();
        }

public static function nomeTabela(){
            if(is_null(static::$nomeTabela))
            {
                static::$nomeTabela = "tarefa";
            }
            return static::$nomeTabela;
        }

public function getNomeTabela(){
            return static::nomeTabela();
        }

public static function __setStaticValues($databaseObject = null)
                   {    
                        static::nomeTabela();
                        
                        static::idSistemaTabela($databaseObject);
                   
                        static::setDatabaseFieldNames();
                        static::setDatabaseFieldTypes();
                        static::setDatabaseFieldsRelatedAttributeNames();
                        
                        static::setListAliasTypes();
                        static::setListAliasRelatedAttributes();
                   }

public function getExpressaoLabel()
                   {
                        return "{id}";
                   }

public static function setDatabaseFieldNames()
        {
            if(is_null(static::$databaseFieldNames))
            {
                static::$databaseFieldNames = new stdClass();
                static::$databaseFieldNames->id = "id";
static::$databaseFieldNames->criadoPeloUsuarioId = "criado_pelo_usuario_id_INT";
static::$databaseFieldNames->categoriaPermissaoId = "categoria_permissao_id_INT";
static::$databaseFieldNames->veiculoId = "veiculo_id_INT";
static::$databaseFieldNames->usuarioId = "usuario_id_INT";
static::$databaseFieldNames->empresaEquipeId = "empresa_equipe_id_INT";
static::$databaseFieldNames->origemPessoaId = "origem_pessoa_id_INT";
static::$databaseFieldNames->origemEmpresaId = "origem_empresa_id_INT";
static::$databaseFieldNames->origemLogradouro = "origem_logradouro";
static::$databaseFieldNames->origemNumero = "origem_numero";
static::$databaseFieldNames->origemCidadeId = "origem_cidade_id_INT";
static::$databaseFieldNames->origemLatitudeInt = "origem_latitude_INT";
static::$databaseFieldNames->origemLongitudeInt = "origem_longitude_INT";
static::$databaseFieldNames->origemLatitudeRealInt = "origem_latitude_real_INT";
static::$databaseFieldNames->origemLongitudeRealInt = "origem_longitude_real_INT";
static::$databaseFieldNames->destinoPessoaId = "destino_pessoa_id_INT";
static::$databaseFieldNames->destinoEmpresaId = "destino_empresa_id_INT";
static::$databaseFieldNames->destinoLogradouro = "destino_logradouro";
static::$databaseFieldNames->destinoNumero = "destino_numero";
static::$databaseFieldNames->destinoCidadeId = "destino_cidade_id_INT";
static::$databaseFieldNames->destinoLatitudeInt = "destino_latitude_INT";
static::$databaseFieldNames->destinoLongitudeInt = "destino_longitude_INT";
static::$databaseFieldNames->destinoLatitudeRealInt = "destino_latitude_real_INT";
static::$databaseFieldNames->destinoLongitudeRealInt = "destino_longitude_real_INT";
static::$databaseFieldNames->tempoEstimadoCarroInt = "tempo_estimado_carro_INT";
static::$databaseFieldNames->tempoEstimadoAPeInt = "tempo_estimado_a_pe_INT";
static::$databaseFieldNames->distanciaEstimadaCarroInt = "distancia_estimada_carro_INT";
static::$databaseFieldNames->distanciaEstimadaAPeInt = "distancia_estimada_a_pe_INT";
static::$databaseFieldNames->inicioHoraProgramadaSec = "inicio_hora_programada_SEC";
static::$databaseFieldNames->inicioHoraProgramadaOffsec = "inicio_hora_programada_OFFSEC";
static::$databaseFieldNames->dataExibirSec = "data_exibir_SEC";
static::$databaseFieldNames->dataExibirOffsec = "data_exibir_OFFSEC";
static::$databaseFieldNames->inicioSec = "inicio_SEC";
static::$databaseFieldNames->inicioOffsec = "inicio_OFFSEC";
static::$databaseFieldNames->fimSec = "fim_SEC";
static::$databaseFieldNames->fimOffsec = "fim_OFFSEC";
static::$databaseFieldNames->cadastroSec = "cadastro_SEC";
static::$databaseFieldNames->cadastroOffsec = "cadastro_OFFSEC";
static::$databaseFieldNames->titulo = "titulo";
static::$databaseFieldNames->tituloNormalizado = "titulo_normalizado";
static::$databaseFieldNames->descricao = "descricao";
static::$databaseFieldNames->empresaAtividadeId = "empresa_atividade_id_INT";
static::$databaseFieldNames->tipoTarefaId = "tipo_tarefa_id_INT";
static::$databaseFieldNames->empresaVendaId = "empresa_venda_id_INT";
static::$databaseFieldNames->idPrioridadeInt = "id_prioridade_INT";
static::$databaseFieldNames->registroEstadoId = "registro_estado_id_INT";
static::$databaseFieldNames->registroEstadoCorporacaoId = "registro_estado_corporacao_id_INT";
static::$databaseFieldNames->corporacaoId = "corporacao_id_INT";
static::$databaseFieldNames->percentualCompletoInt = "percentual_completo_INT";
static::$databaseFieldNames->prazoSec = "prazo_SEC";
static::$databaseFieldNames->prazoOffsec = "prazo_OFFSEC";
static::$databaseFieldNames->atividadeId = "atividade_id_INT";
static::$databaseFieldNames->protocoloEmpresaVendaInt = "protocolo_empresa_venda_INT";
static::$databaseFieldNames->protocoloEmpresaAtividadeVendaInt = "protocolo_empresa_atividade_venda_INT";

            }
                      
        }

public static function setDatabaseFieldTypes()
        {
            if(is_null(static::$databaseFieldTypes))
            {
                static::$databaseFieldTypes = new stdClass();
                static::$databaseFieldTypes->id = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->criadoPeloUsuarioId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->categoriaPermissaoId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->veiculoId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->usuarioId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->empresaEquipeId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->origemPessoaId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->origemEmpresaId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->origemLogradouro = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->origemNumero = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->origemCidadeId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->origemLatitudeInt = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->origemLongitudeInt = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->origemLatitudeRealInt = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->origemLongitudeRealInt = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->destinoPessoaId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->destinoEmpresaId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->destinoLogradouro = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->destinoNumero = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->destinoCidadeId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->destinoLatitudeInt = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->destinoLongitudeInt = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->destinoLatitudeRealInt = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->destinoLongitudeRealInt = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->tempoEstimadoCarroInt = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->tempoEstimadoAPeInt = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->distanciaEstimadaCarroInt = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->distanciaEstimadaAPeInt = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->inicioHoraProgramadaSec = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$databaseFieldTypes->inicioHoraProgramadaOffsec = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$databaseFieldTypes->dataExibirSec = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$databaseFieldTypes->dataExibirOffsec = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$databaseFieldTypes->inicioSec = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$databaseFieldTypes->inicioOffsec = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$databaseFieldTypes->fimSec = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$databaseFieldTypes->fimOffsec = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$databaseFieldTypes->cadastroSec = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$databaseFieldTypes->cadastroOffsec = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$databaseFieldTypes->titulo = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->tituloNormalizado = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->descricao = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->empresaAtividadeId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->tipoTarefaId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->empresaVendaId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->idPrioridadeInt = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->registroEstadoId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->registroEstadoCorporacaoId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->corporacaoId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->percentualCompletoInt = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->prazoSec = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$databaseFieldTypes->prazoOffsec = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$databaseFieldTypes->atividadeId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->protocoloEmpresaVendaInt = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->protocoloEmpresaAtividadeVendaInt = static::TIPO_VARIAVEL_INTEGER;

            }
            
        }

public static function setDatabaseFieldsRelatedAttributeNames()
        {
            if(is_null(static::$databaseFieldsRelatedAttributes))
            {
                static::$databaseFieldsRelatedAttributes = new stdClass();
                static::$databaseFieldsRelatedAttributes->id = "id";
static::$databaseFieldsRelatedAttributes->criado_pelo_usuario_id_INT = "criadoPeloUsuarioId";
static::$databaseFieldsRelatedAttributes->categoria_permissao_id_INT = "categoriaPermissaoId";
static::$databaseFieldsRelatedAttributes->veiculo_id_INT = "veiculoId";
static::$databaseFieldsRelatedAttributes->usuario_id_INT = "usuarioId";
static::$databaseFieldsRelatedAttributes->empresa_equipe_id_INT = "empresaEquipeId";
static::$databaseFieldsRelatedAttributes->origem_pessoa_id_INT = "origemPessoaId";
static::$databaseFieldsRelatedAttributes->origem_empresa_id_INT = "origemEmpresaId";
static::$databaseFieldsRelatedAttributes->origem_logradouro = "origemLogradouro";
static::$databaseFieldsRelatedAttributes->origem_numero = "origemNumero";
static::$databaseFieldsRelatedAttributes->origem_cidade_id_INT = "origemCidadeId";
static::$databaseFieldsRelatedAttributes->origem_latitude_INT = "origemLatitudeInt";
static::$databaseFieldsRelatedAttributes->origem_longitude_INT = "origemLongitudeInt";
static::$databaseFieldsRelatedAttributes->origem_latitude_real_INT = "origemLatitudeRealInt";
static::$databaseFieldsRelatedAttributes->origem_longitude_real_INT = "origemLongitudeRealInt";
static::$databaseFieldsRelatedAttributes->destino_pessoa_id_INT = "destinoPessoaId";
static::$databaseFieldsRelatedAttributes->destino_empresa_id_INT = "destinoEmpresaId";
static::$databaseFieldsRelatedAttributes->destino_logradouro = "destinoLogradouro";
static::$databaseFieldsRelatedAttributes->destino_numero = "destinoNumero";
static::$databaseFieldsRelatedAttributes->destino_cidade_id_INT = "destinoCidadeId";
static::$databaseFieldsRelatedAttributes->destino_latitude_INT = "destinoLatitudeInt";
static::$databaseFieldsRelatedAttributes->destino_longitude_INT = "destinoLongitudeInt";
static::$databaseFieldsRelatedAttributes->destino_latitude_real_INT = "destinoLatitudeRealInt";
static::$databaseFieldsRelatedAttributes->destino_longitude_real_INT = "destinoLongitudeRealInt";
static::$databaseFieldsRelatedAttributes->tempo_estimado_carro_INT = "tempoEstimadoCarroInt";
static::$databaseFieldsRelatedAttributes->tempo_estimado_a_pe_INT = "tempoEstimadoAPeInt";
static::$databaseFieldsRelatedAttributes->distancia_estimada_carro_INT = "distanciaEstimadaCarroInt";
static::$databaseFieldsRelatedAttributes->distancia_estimada_a_pe_INT = "distanciaEstimadaAPeInt";
static::$databaseFieldsRelatedAttributes->inicio_hora_programada_SEC = "inicioHoraProgramadaSec";
static::$databaseFieldsRelatedAttributes->inicio_hora_programada_OFFSEC = "inicioHoraProgramadaOffsec";
static::$databaseFieldsRelatedAttributes->data_exibir_SEC = "dataExibirSec";
static::$databaseFieldsRelatedAttributes->data_exibir_OFFSEC = "dataExibirOffsec";
static::$databaseFieldsRelatedAttributes->inicio_SEC = "inicioSec";
static::$databaseFieldsRelatedAttributes->inicio_OFFSEC = "inicioOffsec";
static::$databaseFieldsRelatedAttributes->fim_SEC = "fimSec";
static::$databaseFieldsRelatedAttributes->fim_OFFSEC = "fimOffsec";
static::$databaseFieldsRelatedAttributes->cadastro_SEC = "cadastroSec";
static::$databaseFieldsRelatedAttributes->cadastro_OFFSEC = "cadastroOffsec";
static::$databaseFieldsRelatedAttributes->titulo = "titulo";
static::$databaseFieldsRelatedAttributes->titulo_normalizado = "tituloNormalizado";
static::$databaseFieldsRelatedAttributes->descricao = "descricao";
static::$databaseFieldsRelatedAttributes->empresa_atividade_id_INT = "empresaAtividadeId";
static::$databaseFieldsRelatedAttributes->tipo_tarefa_id_INT = "tipoTarefaId";
static::$databaseFieldsRelatedAttributes->empresa_venda_id_INT = "empresaVendaId";
static::$databaseFieldsRelatedAttributes->id_prioridade_INT = "idPrioridadeInt";
static::$databaseFieldsRelatedAttributes->registro_estado_id_INT = "registroEstadoId";
static::$databaseFieldsRelatedAttributes->registro_estado_corporacao_id_INT = "registroEstadoCorporacaoId";
static::$databaseFieldsRelatedAttributes->corporacao_id_INT = "corporacaoId";
static::$databaseFieldsRelatedAttributes->percentual_completo_INT = "percentualCompletoInt";
static::$databaseFieldsRelatedAttributes->prazo_SEC = "prazoSec";
static::$databaseFieldsRelatedAttributes->prazo_OFFSEC = "prazoOffsec";
static::$databaseFieldsRelatedAttributes->atividade_id_INT = "atividadeId";
static::$databaseFieldsRelatedAttributes->protocolo_empresa_venda_INT = "protocoloEmpresaVendaInt";
static::$databaseFieldsRelatedAttributes->protocolo_empresa_atividade_venda_INT = "protocoloEmpresaAtividadeVendaInt";

            }           
        }

public function setAllFieldsToHumanFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["criado_pelo_usuario_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["categoria_permissao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["veiculo_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["usuario_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["empresa_equipe_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["origem_pessoa_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["origem_empresa_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["origem_logradouro"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["origem_numero"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["origem_cidade_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["origem_latitude_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["origem_longitude_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["origem_latitude_real_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["origem_longitude_real_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["destino_pessoa_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["destino_empresa_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["destino_logradouro"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["destino_numero"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["destino_cidade_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["destino_latitude_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["destino_longitude_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["destino_latitude_real_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["destino_longitude_real_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["tempo_estimado_carro_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["tempo_estimado_a_pe_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["distancia_estimada_carro_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["distancia_estimada_a_pe_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["inicio_hora_programada_SEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["inicio_hora_programada_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["data_exibir_SEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["data_exibir_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["inicio_SEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["inicio_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["fim_SEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["fim_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["cadastro_SEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["cadastro_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["titulo"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["titulo_normalizado"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["descricao"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["empresa_atividade_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["tipo_tarefa_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["empresa_venda_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["id_prioridade_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["registro_estado_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["registro_estado_corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["percentual_completo_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["prazo_SEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["prazo_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["atividade_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["protocolo_empresa_venda_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["protocolo_empresa_atividade_venda_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
}

public function setAllFieldsToDatabaseFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["criado_pelo_usuario_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["categoria_permissao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["veiculo_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["usuario_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["empresa_equipe_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["origem_pessoa_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["origem_empresa_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["origem_logradouro"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["origem_numero"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["origem_cidade_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["origem_latitude_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["origem_longitude_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["origem_latitude_real_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["origem_longitude_real_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["destino_pessoa_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["destino_empresa_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["destino_logradouro"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["destino_numero"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["destino_cidade_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["destino_latitude_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["destino_longitude_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["destino_latitude_real_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["destino_longitude_real_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["tempo_estimado_carro_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["tempo_estimado_a_pe_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["distancia_estimada_carro_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["distancia_estimada_a_pe_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["inicio_hora_programada_SEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["inicio_hora_programada_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["data_exibir_SEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["data_exibir_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["inicio_SEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["inicio_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["fim_SEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["fim_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["cadastro_SEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["cadastro_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["titulo"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["titulo_normalizado"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["descricao"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["empresa_atividade_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["tipo_tarefa_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["empresa_venda_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["id_prioridade_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["registro_estado_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["registro_estado_corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["percentual_completo_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["prazo_SEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["prazo_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["atividade_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["protocolo_empresa_venda_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["protocolo_empresa_atividade_venda_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
}

  public function getFkObjCriado_pelo_usuario() 
                {
                    if($this->objCriado_pelo_usuario == null)
                    {                        
                        $this->objCriado_pelo_usuario = new EXTDAO_Criado_pelo_usuario_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getCriado_pelo_usuario_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objCriado_pelo_usuario->clear();
                    }
                    elseif($this->objCriado_pelo_usuario->getId() != $idFK)
                    {
                        $this->objCriado_pelo_usuario->select($idFK);
                    }
                    return $this->objCriado_pelo_usuario;
                }
  public function getFkObjCategoria_permissao() 
                {
                    if($this->objCategoria_permissao == null)
                    {                        
                        $this->objCategoria_permissao = new EXTDAO_Categoria_permissao_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getCategoria_permissao_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objCategoria_permissao->clear();
                    }
                    elseif($this->objCategoria_permissao->getId() != $idFK)
                    {
                        $this->objCategoria_permissao->select($idFK);
                    }
                    return $this->objCategoria_permissao;
                }
  public function getFkObjVeiculo() 
                {
                    if($this->objVeiculo == null)
                    {                        
                        $this->objVeiculo = new EXTDAO_Veiculo_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getVeiculo_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objVeiculo->clear();
                    }
                    elseif($this->objVeiculo->getId() != $idFK)
                    {
                        $this->objVeiculo->select($idFK);
                    }
                    return $this->objVeiculo;
                }
  public function getFkObjUsuario() 
                {
                    if($this->objUsuario == null)
                    {                        
                        $this->objUsuario = new EXTDAO_Usuario_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getUsuario_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objUsuario->clear();
                    }
                    elseif($this->objUsuario->getId() != $idFK)
                    {
                        $this->objUsuario->select($idFK);
                    }
                    return $this->objUsuario;
                }
  public function getFkObjEmpresa_equipe() 
                {
                    if($this->objEmpresa_equipe == null)
                    {                        
                        $this->objEmpresa_equipe = new EXTDAO_Empresa_equipe_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getEmpresa_equipe_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objEmpresa_equipe->clear();
                    }
                    elseif($this->objEmpresa_equipe->getId() != $idFK)
                    {
                        $this->objEmpresa_equipe->select($idFK);
                    }
                    return $this->objEmpresa_equipe;
                }
  public function getFkObjOrigem_pessoa() 
                {
                    if($this->objOrigem_pessoa == null)
                    {                        
                        $this->objOrigem_pessoa = new EXTDAO_Origem_pessoa_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getOrigem_pessoa_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objOrigem_pessoa->clear();
                    }
                    elseif($this->objOrigem_pessoa->getId() != $idFK)
                    {
                        $this->objOrigem_pessoa->select($idFK);
                    }
                    return $this->objOrigem_pessoa;
                }
  public function getFkObjOrigem_empresa() 
                {
                    if($this->objOrigem_empresa == null)
                    {                        
                        $this->objOrigem_empresa = new EXTDAO_Origem_empresa_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getOrigem_empresa_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objOrigem_empresa->clear();
                    }
                    elseif($this->objOrigem_empresa->getId() != $idFK)
                    {
                        $this->objOrigem_empresa->select($idFK);
                    }
                    return $this->objOrigem_empresa;
                }
  public function getFkObjOrigem_cidade() 
                {
                    if($this->objOrigem_cidade == null)
                    {                        
                        $this->objOrigem_cidade = new EXTDAO_Origem_cidade_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getOrigem_cidade_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objOrigem_cidade->clear();
                    }
                    elseif($this->objOrigem_cidade->getId() != $idFK)
                    {
                        $this->objOrigem_cidade->select($idFK);
                    }
                    return $this->objOrigem_cidade;
                }
  public function getFkObjDestino_pessoa() 
                {
                    if($this->objDestino_pessoa == null)
                    {                        
                        $this->objDestino_pessoa = new EXTDAO_Destino_pessoa_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getDestino_pessoa_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objDestino_pessoa->clear();
                    }
                    elseif($this->objDestino_pessoa->getId() != $idFK)
                    {
                        $this->objDestino_pessoa->select($idFK);
                    }
                    return $this->objDestino_pessoa;
                }
  public function getFkObjDestino_empresa() 
                {
                    if($this->objDestino_empresa == null)
                    {                        
                        $this->objDestino_empresa = new EXTDAO_Destino_empresa_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getDestino_empresa_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objDestino_empresa->clear();
                    }
                    elseif($this->objDestino_empresa->getId() != $idFK)
                    {
                        $this->objDestino_empresa->select($idFK);
                    }
                    return $this->objDestino_empresa;
                }
  public function getFkObjDestino_cidade() 
                {
                    if($this->objDestino_cidade == null)
                    {                        
                        $this->objDestino_cidade = new EXTDAO_Destino_cidade_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getDestino_cidade_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objDestino_cidade->clear();
                    }
                    elseif($this->objDestino_cidade->getId() != $idFK)
                    {
                        $this->objDestino_cidade->select($idFK);
                    }
                    return $this->objDestino_cidade;
                }
  public function getFkObjEmpresa_atividade() 
                {
                    if($this->objEmpresa_atividade == null)
                    {                        
                        $this->objEmpresa_atividade = new EXTDAO_Empresa_atividade_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getEmpresa_atividade_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objEmpresa_atividade->clear();
                    }
                    elseif($this->objEmpresa_atividade->getId() != $idFK)
                    {
                        $this->objEmpresa_atividade->select($idFK);
                    }
                    return $this->objEmpresa_atividade;
                }
  public function getFkObjTipo_tarefa() 
                {
                    if($this->objTipo_tarefa == null)
                    {                        
                        $this->objTipo_tarefa = new EXTDAO_Tipo_tarefa_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getTipo_tarefa_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objTipo_tarefa->clear();
                    }
                    elseif($this->objTipo_tarefa->getId() != $idFK)
                    {
                        $this->objTipo_tarefa->select($idFK);
                    }
                    return $this->objTipo_tarefa;
                }
  public function getFkObjEmpresa_venda() 
                {
                    if($this->objEmpresa_venda == null)
                    {                        
                        $this->objEmpresa_venda = new EXTDAO_Empresa_venda_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getEmpresa_venda_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objEmpresa_venda->clear();
                    }
                    elseif($this->objEmpresa_venda->getId() != $idFK)
                    {
                        $this->objEmpresa_venda->select($idFK);
                    }
                    return $this->objEmpresa_venda;
                }
  public function getFkObjRegistro_estado() 
                {
                    if($this->objRegistro_estado == null)
                    {                        
                        $this->objRegistro_estado = new EXTDAO_Registro_estado_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getRegistro_estado_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objRegistro_estado->clear();
                    }
                    elseif($this->objRegistro_estado->getId() != $idFK)
                    {
                        $this->objRegistro_estado->select($idFK);
                    }
                    return $this->objRegistro_estado;
                }
  public function getFkObjRegistro_estado_corporacao() 
                {
                    if($this->objRegistro_estado_corporacao == null)
                    {                        
                        $this->objRegistro_estado_corporacao = new EXTDAO_Registro_estado_corporacao_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getRegistro_estado_corporacao_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objRegistro_estado_corporacao->clear();
                    }
                    elseif($this->objRegistro_estado_corporacao->getId() != $idFK)
                    {
                        $this->objRegistro_estado_corporacao->select($idFK);
                    }
                    return $this->objRegistro_estado_corporacao;
                }
  public function getFkObjCorporacao() 
                {
                    if($this->objCorporacao == null)
                    {                        
                        $this->objCorporacao = new EXTDAO_Corporacao_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getCorporacao_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objCorporacao->clear();
                    }
                    elseif($this->objCorporacao->getId() != $idFK)
                    {
                        $this->objCorporacao->select($idFK);
                    }
                    return $this->objCorporacao;
                }
  public function getFkObjAtividade() 
                {
                    if($this->objAtividade == null)
                    {                        
                        $this->objAtividade = new EXTDAO_Atividade_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getAtividade_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objAtividade->clear();
                    }
                    elseif($this->objAtividade->getId() != $idFK)
                    {
                        $this->objAtividade->select($idFK);
                    }
                    return $this->objAtividade;
                }


public function setLabels()
        {$this->labelId = "id";
$this->labelCriadoPeloUsuarioId = "criadopelousuarioidINT";
$this->labelCategoriaPermissaoId = "categoriapermissaoidINT";
$this->labelVeiculoId = "veiculoidINT";
$this->labelUsuarioId = "usuarioidINT";
$this->labelEmpresaEquipeId = "";
$this->labelOrigemPessoaId = "origempessoaidINT";
$this->labelOrigemEmpresaId = "origemempresaidINT";
$this->labelOrigemLogradouro = "origemlogradouro";
$this->labelOrigemNumero = "origemnumero";
$this->labelOrigemCidadeId = "origemcidadeidINT";
$this->labelOrigemLatitudeInt = "origemlatitudeINT";
$this->labelOrigemLongitudeInt = "origemlongitudeINT";
$this->labelOrigemLatitudeRealInt = "origemlatituderealINT";
$this->labelOrigemLongitudeRealInt = "origemlongituderealINT";
$this->labelDestinoPessoaId = "destinopessoaidINT";
$this->labelDestinoEmpresaId = "destinoempresaidINT";
$this->labelDestinoLogradouro = "destinologradouro";
$this->labelDestinoNumero = "destinonumero";
$this->labelDestinoCidadeId = "destinocidadeidINT";
$this->labelDestinoLatitudeInt = "destinolatitudeINT";
$this->labelDestinoLongitudeInt = "destinolongitudeINT";
$this->labelDestinoLatitudeRealInt = "destinolatituderealINT";
$this->labelDestinoLongitudeRealInt = "destinolongituderealINT";
$this->labelTempoEstimadoCarroInt = "tempoestimadocarroINT";
$this->labelTempoEstimadoAPeInt = "tempoestimadoapeINT";
$this->labelDistanciaEstimadaCarroInt = "distanciaestimadacarroINT";
$this->labelDistanciaEstimadaAPeInt = "distanciaestimadaapeINT";
$this->labelInicioHoraProgramadaSec = "";
$this->labelInicioHoraProgramadaOffsec = "";
$this->labelDataExibirSec = "";
$this->labelDataExibirOffsec = "";
$this->labelInicioSec = "";
$this->labelInicioOffsec = "";
$this->labelFimSec = "";
$this->labelFimOffsec = "";
$this->labelCadastroSec = "";
$this->labelCadastroOffsec = "";
$this->labelTitulo = "titulo";
$this->labelTituloNormalizado = "titulonormalizado";
$this->labelDescricao = "descricao";
$this->labelEmpresaAtividadeId = "";
$this->labelTipoTarefaId = "";
$this->labelEmpresaVendaId = "";
$this->labelIdPrioridadeInt = "";
$this->labelRegistroEstadoId = "";
$this->labelRegistroEstadoCorporacaoId = "";
$this->labelCorporacaoId = "corporacaoidINT";
$this->labelPercentualCompletoInt = "";
$this->labelPrazoSec = "";
$this->labelPrazoOffsec = "";
$this->labelAtividadeId = "";
$this->labelProtocoloEmpresaVendaInt = "";
$this->labelProtocoloEmpresaAtividadeVendaInt = "";
}

public function __actionAdd($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                $this->setByObject($parameters);
                $this->setDataCadastroAndOffsetInSecondsFromUser($parameters);
                
                $this->formatarParaSQL();
                $msg = $this->insert(true);
                
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                return new Mensagem(null, I18N::getExpression("Tarefa adicionado com sucesso."));
                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionEdit($parameters = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try {

                $this->setByObject($parameters);
                $this->setAllFieldsToHumanFormat();
                
                $this->formatarParaSQL();
                $msg = $this->updateByInstanceUsingParameters($this->id, $parameters, true);
                if($msg != null && $msg->erro())
                {
                     return $msg;
                }
        
                return new Mensagem(null, I18N::getExpression("Tarefa editado com sucesso."));
            }
            catch(Exception $ex)
            {                
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionRemove($parameters = null)
        {
            $successulRemovalCount = 0;
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                foreach($parameters as $record)
                {
                    if(is_numeric($record->id))
                    {
                        $msg = $this->delete($record->id, true);
                        if($msg == null || $msg->ok())
                        {
                            $successulRemovalCount++;
                        }
                    }
                }
                
                if($successulRemovalCount > 0)
                {
                    $successMessage = count($successulRemovalCount) > 1 ? I18N::getExpression("{0} Tarefa foram removidos com sucesso.", $successulRemovalCount) : I18N::getExpression("Tarefa removido com sucesso.");
                    return new Mensagem(null, $successMessage);
                }
                else
                {
                    $errorMessage = count($parameters) > 1 ? I18N::getExpression("Falha ao remover Tarefa.") : I18N::getExpression("Falha ao remover Tarefa.");
                    return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, $errorMessage);                    
                }
            }
            catch (Exception $ex) 
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }

public function __getRecord($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }
            
            try
            {            
                $returnObject = new stdClass();            
                $whereClause = static::getWhereClauseForFilter($parameters->filterParameters);
                $query = "SELECT  id, criado_pelo_usuario_id_INT, categoria_permissao_id_INT, veiculo_id_INT, usuario_id_INT, empresa_equipe_id_INT, origem_pessoa_id_INT, origem_empresa_id_INT, origem_logradouro, origem_numero, origem_cidade_id_INT, origem_latitude_INT, origem_longitude_INT, origem_latitude_real_INT, origem_longitude_real_INT, destino_pessoa_id_INT, destino_empresa_id_INT, destino_logradouro, destino_numero, destino_cidade_id_INT, destino_latitude_INT, destino_longitude_INT, destino_latitude_real_INT, destino_longitude_real_INT, tempo_estimado_carro_INT, tempo_estimado_a_pe_INT, distancia_estimada_carro_INT, distancia_estimada_a_pe_INT, inicio_hora_programada_SEC, inicio_hora_programada_OFFSEC, data_exibir_SEC, data_exibir_OFFSEC, inicio_SEC, inicio_OFFSEC, fim_SEC, fim_OFFSEC, cadastro_SEC, cadastro_OFFSEC, titulo, titulo_normalizado, descricao, empresa_atividade_id_INT, tipo_tarefa_id_INT, empresa_venda_id_INT, id_prioridade_INT, registro_estado_id_INT, registro_estado_corporacao_id_INT, corporacao_id_INT, percentual_completo_INT, prazo_SEC, prazo_OFFSEC, atividade_id_INT, protocolo_empresa_venda_INT, protocolo_empresa_atividade_venda_INT FROM tarefa {$whereClause}";
    
                $msg = $this->database->queryMensagem($query);                
                if ($msg == null)
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Não foi encontrado nenhum registro"));
                    }                    
                    $resultSet = $this->database->result;
                    $resultObject = static::mapResultSetDataToRecord($resultSet);
                    
                    $returnObject->formData = $resultObject;
                    $returnObject->comboBoxesData = null;
                    if($parameters->loadComboBoxesData)
                    {
                        $this->setByObject($resultObject);
                        $returnObject->comboBoxesData =  $this->getFormComboBoxesData();
                    }                    
    
                    
    
                    return new Mensagem_generica($returnObject);
                } 
                else 
                {
                    return $msg;
                }
                
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }


        
        public function __getFilterComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
                
        public function __getFormComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
        
        protected function getFormComboBoxesData()
        {
            $comboBoxesData = new stdClass();
            $listParameters = new stdClass();
            $listParameters->listMappingType = static::LIST_MAPPING_TYPE_COMBOBOX;
            
            
                    
                    $objCategoriaPermissao = new EXTDAO_Categoria_permissao();
                    $comboBoxesData->fieldCategoriaPermissaoId = $objCategoriaPermissao->__getList($listParameters);
                    
                    $objVeiculo = new EXTDAO_Veiculo();
                    $comboBoxesData->fieldVeiculoId = $objVeiculo->__getList($listParameters);
                    
                    $objUsuario = new EXTDAO_Usuario();
                    $comboBoxesData->fieldUsuarioId = $objUsuario->__getList($listParameters);
                    
                    $objEmpresaEquipe = new EXTDAO_Empresa_equipe();
                    $comboBoxesData->fieldEmpresaEquipeId = $objEmpresaEquipe->__getList($listParameters);

            return $comboBoxesData;
        }
        
        


        
        public static function setListAliasTypes()
        {
            if(is_null(static::$listAliasTypes))
            {
                static::$listAliasTypes = new stdClass();
                static::$listAliasTypes->categoria_permissao__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->veiculo__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->usuario__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->empresa_equipe__nome = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->tarefa__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->tarefa__criado_pelo_usuario_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->tarefa__corporacao_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->tarefa__corporacao_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->tarefa__cadastro_OFFSEC = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$listAliasTypes->tarefa__corporacao_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->tarefa__origem_pessoa_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->tarefa__origem_empresa_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->tarefa__origem_logradouro = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->tarefa__origem_numero = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->tarefa__origem_cidade_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->tarefa__origem_latitude_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->tarefa__origem_longitude_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->tarefa__origem_latitude_real_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->tarefa__origem_longitude_real_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->tarefa__destino_pessoa_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->tarefa__destino_empresa_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->tarefa__destino_logradouro = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->tarefa__destino_numero = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->tarefa__destino_cidade_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->tarefa__destino_latitude_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->tarefa__destino_longitude_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->tarefa__destino_latitude_real_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->tarefa__destino_longitude_real_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->tarefa__tempo_estimado_carro_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->tarefa__tempo_estimado_a_pe_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->tarefa__distancia_estimada_carro_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->tarefa__distancia_estimada_a_pe_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->tarefa__inicio_hora_programada_SEC = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$listAliasTypes->tarefa__inicio_hora_programada_OFFSEC = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$listAliasTypes->tarefa__data_exibir_SEC = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$listAliasTypes->tarefa__data_exibir_OFFSEC = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$listAliasTypes->tarefa__inicio_SEC = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$listAliasTypes->tarefa__inicio_OFFSEC = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$listAliasTypes->tarefa__fim_SEC = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$listAliasTypes->tarefa__fim_OFFSEC = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$listAliasTypes->tarefa__cadastro_SEC = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$listAliasTypes->tarefa__cadastro_OFFSEC = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$listAliasTypes->tarefa__titulo = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->tarefa__descricao = static::TIPO_VARIAVEL_TEXT;
            }        
        }
        
        public static function setListAliasRelatedAttributes()
        {
            if(is_null(static::$listAliasRelatedAttributes))
            {
                static::$listAliasRelatedAttributes = new stdClass();
                static::$listAliasRelatedAttributes->categoria_permissao__id = "categoriaPermissaoId";
static::$listAliasRelatedAttributes->veiculo__id = "veiculoId";
static::$listAliasRelatedAttributes->usuario__id = "usuarioId";
static::$listAliasRelatedAttributes->empresa_equipe__nome = "empresaEquipeNome";
static::$listAliasRelatedAttributes->tarefa__id = "id";
static::$listAliasRelatedAttributes->tarefa__criado_pelo_usuario_id_INT = "criadoPeloUsuarioId";
static::$listAliasRelatedAttributes->tarefa__corporacao_id_INT = "corporacaoId";
static::$listAliasRelatedAttributes->tarefa__corporacao_id_INT = "corporacaoId";
static::$listAliasRelatedAttributes->tarefa__cadastro_OFFSEC = "cadastroOffsec";
static::$listAliasRelatedAttributes->tarefa__corporacao_id_INT = "corporacaoId";
static::$listAliasRelatedAttributes->tarefa__origem_pessoa_id_INT = "origemPessoaId";
static::$listAliasRelatedAttributes->tarefa__origem_empresa_id_INT = "origemEmpresaId";
static::$listAliasRelatedAttributes->tarefa__origem_logradouro = "origemLogradouro";
static::$listAliasRelatedAttributes->tarefa__origem_numero = "origemNumero";
static::$listAliasRelatedAttributes->tarefa__origem_cidade_id_INT = "origemCidadeId";
static::$listAliasRelatedAttributes->tarefa__origem_latitude_INT = "origemLatitudeInt";
static::$listAliasRelatedAttributes->tarefa__origem_longitude_INT = "origemLongitudeInt";
static::$listAliasRelatedAttributes->tarefa__origem_latitude_real_INT = "origemLatitudeRealInt";
static::$listAliasRelatedAttributes->tarefa__origem_longitude_real_INT = "origemLongitudeRealInt";
static::$listAliasRelatedAttributes->tarefa__destino_pessoa_id_INT = "destinoPessoaId";
static::$listAliasRelatedAttributes->tarefa__destino_empresa_id_INT = "destinoEmpresaId";
static::$listAliasRelatedAttributes->tarefa__destino_logradouro = "destinoLogradouro";
static::$listAliasRelatedAttributes->tarefa__destino_numero = "destinoNumero";
static::$listAliasRelatedAttributes->tarefa__destino_cidade_id_INT = "destinoCidadeId";
static::$listAliasRelatedAttributes->tarefa__destino_latitude_INT = "destinoLatitudeInt";
static::$listAliasRelatedAttributes->tarefa__destino_longitude_INT = "destinoLongitudeInt";
static::$listAliasRelatedAttributes->tarefa__destino_latitude_real_INT = "destinoLatitudeRealInt";
static::$listAliasRelatedAttributes->tarefa__destino_longitude_real_INT = "destinoLongitudeRealInt";
static::$listAliasRelatedAttributes->tarefa__tempo_estimado_carro_INT = "tempoEstimadoCarroInt";
static::$listAliasRelatedAttributes->tarefa__tempo_estimado_a_pe_INT = "tempoEstimadoAPeInt";
static::$listAliasRelatedAttributes->tarefa__distancia_estimada_carro_INT = "distanciaEstimadaCarroInt";
static::$listAliasRelatedAttributes->tarefa__distancia_estimada_a_pe_INT = "distanciaEstimadaAPeInt";
static::$listAliasRelatedAttributes->tarefa__inicio_hora_programada_SEC = "inicioHoraProgramadaSec";
static::$listAliasRelatedAttributes->tarefa__inicio_hora_programada_OFFSEC = "inicioHoraProgramadaOffsec";
static::$listAliasRelatedAttributes->tarefa__data_exibir_SEC = "dataExibirSec";
static::$listAliasRelatedAttributes->tarefa__data_exibir_OFFSEC = "dataExibirOffsec";
static::$listAliasRelatedAttributes->tarefa__inicio_SEC = "inicioSec";
static::$listAliasRelatedAttributes->tarefa__inicio_OFFSEC = "inicioOffsec";
static::$listAliasRelatedAttributes->tarefa__fim_SEC = "fimSec";
static::$listAliasRelatedAttributes->tarefa__fim_OFFSEC = "fimOffsec";
static::$listAliasRelatedAttributes->tarefa__cadastro_SEC = "cadastroSec";
static::$listAliasRelatedAttributes->tarefa__cadastro_OFFSEC = "cadastroOffsec";
static::$listAliasRelatedAttributes->tarefa__titulo = "titulo";
static::$listAliasRelatedAttributes->tarefa__descricao = "descricao";
            }         
        }

        public function __getList($parameters = null, $idCorporacao = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try
            {
                $filterParameters = new stdClass();
                if(isset($parameters->filterParameters))
                {
                    $filterParameters = $parameters->filterParameters;
                }
                                    
                            
            if(is_null($idCorporacao))
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            ListParameters::addFilterParameterToObject($filterParameters, "corporacaoId", $idCorporacao);           
                
                
                $sortingParameters = null;
                if(isset($parameters->sortingParameters))
                {
                    $sortingParameters = $parameters->sortingParameters;
                }
                
                $paginationParameters = null;
                if(isset($parameters->paginationParameters))
                {
                    $paginationParameters = $parameters->paginationParameters;
                }
                
                $listMappingType = static::getListMappingType($parameters->listMappingType);
                $mainTableAlias = "t";

                $whereClause = static::getWhereClauseForFilter($filterParameters, $mainTableAlias);
                $orderByClause = static::getOrderByClauseForFilter($sortingParameters, $mainTableAlias);
                $limitClause = static::getLimitClauseForFilter($paginationParameters);

                $queryWithoutLimit = "SELECT t.id FROM tarefa t {$whereClause}";
                $query = "SELECT cp.id AS categoria_permissao__id, v.id AS veiculo__id, u.id AS usuario__id, ee.nome AS empresa_equipe__nome, t.id AS tarefa__id, t.criado_pelo_usuario_id_INT AS tarefa__criado_pelo_usuario_id_INT, t.corporacao_id_INT AS tarefa__corporacao_id_INT, t.corporacao_id_INT AS tarefa__corporacao_id_INT, t.cadastro_OFFSEC AS tarefa__cadastro_OFFSEC, t.corporacao_id_INT AS tarefa__corporacao_id_INT, t.origem_pessoa_id_INT AS tarefa__origem_pessoa_id_INT, t.origem_empresa_id_INT AS tarefa__origem_empresa_id_INT, t.origem_logradouro AS tarefa__origem_logradouro, t.origem_numero AS tarefa__origem_numero, t.origem_cidade_id_INT AS tarefa__origem_cidade_id_INT, t.origem_latitude_INT AS tarefa__origem_latitude_INT, t.origem_longitude_INT AS tarefa__origem_longitude_INT, t.origem_latitude_real_INT AS tarefa__origem_latitude_real_INT, t.origem_longitude_real_INT AS tarefa__origem_longitude_real_INT, t.destino_pessoa_id_INT AS tarefa__destino_pessoa_id_INT, t.destino_empresa_id_INT AS tarefa__destino_empresa_id_INT, t.destino_logradouro AS tarefa__destino_logradouro, t.destino_numero AS tarefa__destino_numero, t.destino_cidade_id_INT AS tarefa__destino_cidade_id_INT, t.destino_latitude_INT AS tarefa__destino_latitude_INT, t.destino_longitude_INT AS tarefa__destino_longitude_INT, t.destino_latitude_real_INT AS tarefa__destino_latitude_real_INT, t.destino_longitude_real_INT AS tarefa__destino_longitude_real_INT, t.tempo_estimado_carro_INT AS tarefa__tempo_estimado_carro_INT, t.tempo_estimado_a_pe_INT AS tarefa__tempo_estimado_a_pe_INT, t.distancia_estimada_carro_INT AS tarefa__distancia_estimada_carro_INT, t.distancia_estimada_a_pe_INT AS tarefa__distancia_estimada_a_pe_INT, t.inicio_hora_programada_SEC AS tarefa__inicio_hora_programada_SEC, t.inicio_hora_programada_OFFSEC AS tarefa__inicio_hora_programada_OFFSEC, t.data_exibir_SEC AS tarefa__data_exibir_SEC, t.data_exibir_OFFSEC AS tarefa__data_exibir_OFFSEC, t.inicio_SEC AS tarefa__inicio_SEC, t.inicio_OFFSEC AS tarefa__inicio_OFFSEC, t.fim_SEC AS tarefa__fim_SEC, t.fim_OFFSEC AS tarefa__fim_OFFSEC, t.cadastro_SEC AS tarefa__cadastro_SEC, t.cadastro_OFFSEC AS tarefa__cadastro_OFFSEC, t.titulo AS tarefa__titulo, t.descricao AS tarefa__descricao FROM tarefa t LEFT JOIN categoria_permissao cp ON cp.id = t.categoria_permissao_id_INT LEFT JOIN veiculo v ON v.id = t.veiculo_id_INT LEFT JOIN usuario u ON u.id = t.usuario_id_INT LEFT JOIN empresa_equipe ee ON ee.id = t.empresa_equipe_id_INT {$whereClause} {$orderByClause} {$limitClause}";

                $msg = $this->database->queryMensagem($queryWithoutLimit);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                
                $totalNumberOfRecords = $this->database->rows();
                if($totalNumberOfRecords == 0)
                {
                    return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                }                        
                        
                $msg = $this->database->queryMensagem($query);
                
                //operacao realizaca com sucesso
                if ($msg == null) 
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                    }                        
                
                    $resultSet = $this->database->result;

                    $objReturn = new stdClass();
                    $objReturn->dataSet = static::mapResultSetDataToList($resultSet, $listMappingType, static::$listAliasTypes);

                    if(!Helper::isNullOrEmpty($paginationParameters->recordsPerPage) && !Helper::isNullOrEmpty($paginationParameters->currentPage))
                    {
                        $lastPage = ceil($totalNumberOfRecords / $paginationParameters->recordsPerPage);
                        $objReturn->paginationParameters = new PaginationParams($paginationParameters->recordsPerPage, $paginationParameters->currentPage, $lastPage);
                    }
                    
                    return new Mensagem_generica($objReturn);
                } 
                else
                {
                    return $msg;
                }                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }



public function getId()
            {
                return $this->id;
            }

public function getCriadoPeloUsuarioId()
            {
                return $this->criadoPeloUsuarioId;
            }

public function getCriado_pelo_usuario_id_INT()
                {
                    return $this->criadoPeloUsuarioId;
                }

public function getCategoriaPermissaoId()
            {
                return $this->categoriaPermissaoId;
            }

public function getCategoria_permissao_id_INT()
                {
                    return $this->categoriaPermissaoId;
                }

public function getVeiculoId()
            {
                return $this->veiculoId;
            }

public function getVeiculo_id_INT()
                {
                    return $this->veiculoId;
                }

public function getUsuarioId()
            {
                return $this->usuarioId;
            }

public function getUsuario_id_INT()
                {
                    return $this->usuarioId;
                }

public function getEmpresaEquipeId()
            {
                return $this->empresaEquipeId;
            }

public function getEmpresa_equipe_id_INT()
                {
                    return $this->empresaEquipeId;
                }

public function getOrigemPessoaId()
            {
                return $this->origemPessoaId;
            }

public function getOrigem_pessoa_id_INT()
                {
                    return $this->origemPessoaId;
                }

public function getOrigemEmpresaId()
            {
                return $this->origemEmpresaId;
            }

public function getOrigem_empresa_id_INT()
                {
                    return $this->origemEmpresaId;
                }

public function getOrigemLogradouro()
            {
                return $this->origemLogradouro;
            }

public function getOrigem_logradouro()
                {
                    return $this->origemLogradouro;
                }

public function getOrigemNumero()
            {
                return $this->origemNumero;
            }

public function getOrigem_numero()
                {
                    return $this->origemNumero;
                }

public function getOrigemCidadeId()
            {
                return $this->origemCidadeId;
            }

public function getOrigem_cidade_id_INT()
                {
                    return $this->origemCidadeId;
                }

public function getOrigemLatitudeInt()
            {
                return $this->origemLatitudeInt;
            }

public function getOrigem_latitude_INT()
                {
                    return $this->origemLatitudeInt;
                }

public function getOrigemLongitudeInt()
            {
                return $this->origemLongitudeInt;
            }

public function getOrigem_longitude_INT()
                {
                    return $this->origemLongitudeInt;
                }

public function getOrigemLatitudeRealInt()
            {
                return $this->origemLatitudeRealInt;
            }

public function getOrigem_latitude_real_INT()
                {
                    return $this->origemLatitudeRealInt;
                }

public function getOrigemLongitudeRealInt()
            {
                return $this->origemLongitudeRealInt;
            }

public function getOrigem_longitude_real_INT()
                {
                    return $this->origemLongitudeRealInt;
                }

public function getDestinoPessoaId()
            {
                return $this->destinoPessoaId;
            }

public function getDestino_pessoa_id_INT()
                {
                    return $this->destinoPessoaId;
                }

public function getDestinoEmpresaId()
            {
                return $this->destinoEmpresaId;
            }

public function getDestino_empresa_id_INT()
                {
                    return $this->destinoEmpresaId;
                }

public function getDestinoLogradouro()
            {
                return $this->destinoLogradouro;
            }

public function getDestino_logradouro()
                {
                    return $this->destinoLogradouro;
                }

public function getDestinoNumero()
            {
                return $this->destinoNumero;
            }

public function getDestino_numero()
                {
                    return $this->destinoNumero;
                }

public function getDestinoCidadeId()
            {
                return $this->destinoCidadeId;
            }

public function getDestino_cidade_id_INT()
                {
                    return $this->destinoCidadeId;
                }

public function getDestinoLatitudeInt()
            {
                return $this->destinoLatitudeInt;
            }

public function getDestino_latitude_INT()
                {
                    return $this->destinoLatitudeInt;
                }

public function getDestinoLongitudeInt()
            {
                return $this->destinoLongitudeInt;
            }

public function getDestino_longitude_INT()
                {
                    return $this->destinoLongitudeInt;
                }

public function getDestinoLatitudeRealInt()
            {
                return $this->destinoLatitudeRealInt;
            }

public function getDestino_latitude_real_INT()
                {
                    return $this->destinoLatitudeRealInt;
                }

public function getDestinoLongitudeRealInt()
            {
                return $this->destinoLongitudeRealInt;
            }

public function getDestino_longitude_real_INT()
                {
                    return $this->destinoLongitudeRealInt;
                }

public function getTempoEstimadoCarroInt()
            {
                return $this->tempoEstimadoCarroInt;
            }

public function getTempo_estimado_carro_INT()
                {
                    return $this->tempoEstimadoCarroInt;
                }

public function getTempoEstimadoAPeInt()
            {
                return $this->tempoEstimadoAPeInt;
            }

public function getTempo_estimado_a_pe_INT()
                {
                    return $this->tempoEstimadoAPeInt;
                }

public function getDistanciaEstimadaCarroInt()
            {
                return $this->distanciaEstimadaCarroInt;
            }

public function getDistancia_estimada_carro_INT()
                {
                    return $this->distanciaEstimadaCarroInt;
                }

public function getDistanciaEstimadaAPeInt()
            {
                return $this->distanciaEstimadaAPeInt;
            }

public function getDistancia_estimada_a_pe_INT()
                {
                    return $this->distanciaEstimadaAPeInt;
                }

public function getInicioHoraProgramadaSec()
            {
                return $this->inicioHoraProgramadaSec;
            }

public function getInicio_hora_programada_SEC()
                {
                    return $this->inicioHoraProgramadaSec;
                }

public function getInicioHoraProgramadaOffsec()
            {
                return $this->inicioHoraProgramadaOffsec;
            }

public function getInicio_hora_programada_OFFSEC()
                {
                    return $this->inicioHoraProgramadaOffsec;
                }

public function getDataExibirSec()
            {
                return $this->dataExibirSec;
            }

public function getData_exibir_SEC()
                {
                    return $this->dataExibirSec;
                }

public function getDataExibirOffsec()
            {
                return $this->dataExibirOffsec;
            }

public function getData_exibir_OFFSEC()
                {
                    return $this->dataExibirOffsec;
                }

public function getInicioSec()
            {
                return $this->inicioSec;
            }

public function getInicio_SEC()
                {
                    return $this->inicioSec;
                }

public function getInicioOffsec()
            {
                return $this->inicioOffsec;
            }

public function getInicio_OFFSEC()
                {
                    return $this->inicioOffsec;
                }

public function getFimSec()
            {
                return $this->fimSec;
            }

public function getFim_SEC()
                {
                    return $this->fimSec;
                }

public function getFimOffsec()
            {
                return $this->fimOffsec;
            }

public function getFim_OFFSEC()
                {
                    return $this->fimOffsec;
                }

public function getCadastroSec()
            {
                return $this->cadastroSec;
            }

public function getCadastro_SEC()
                {
                    return $this->cadastroSec;
                }

public function getCadastroOffsec()
            {
                return $this->cadastroOffsec;
            }

public function getCadastro_OFFSEC()
                {
                    return $this->cadastroOffsec;
                }

public function getTitulo()
            {
                return $this->titulo;
            }

public function getTituloNormalizado()
            {
                return $this->tituloNormalizado;
            }

public function getTitulo_normalizado()
                {
                    return $this->tituloNormalizado;
                }

public function getDescricao()
            {
                return $this->descricao;
            }

public function getEmpresaAtividadeId()
            {
                return $this->empresaAtividadeId;
            }

public function getEmpresa_atividade_id_INT()
                {
                    return $this->empresaAtividadeId;
                }

public function getTipoTarefaId()
            {
                return $this->tipoTarefaId;
            }

public function getTipo_tarefa_id_INT()
                {
                    return $this->tipoTarefaId;
                }

public function getEmpresaVendaId()
            {
                return $this->empresaVendaId;
            }

public function getEmpresa_venda_id_INT()
                {
                    return $this->empresaVendaId;
                }

public function getIdPrioridadeInt()
            {
                return $this->idPrioridadeInt;
            }

public function getId_prioridade_INT()
                {
                    return $this->idPrioridadeInt;
                }

public function getRegistroEstadoId()
            {
                return $this->registroEstadoId;
            }

public function getRegistro_estado_id_INT()
                {
                    return $this->registroEstadoId;
                }

public function getRegistroEstadoCorporacaoId()
            {
                return $this->registroEstadoCorporacaoId;
            }

public function getRegistro_estado_corporacao_id_INT()
                {
                    return $this->registroEstadoCorporacaoId;
                }

public function getCorporacaoId()
            {
                return $this->corporacaoId;
            }

public function getCorporacao_id_INT()
                {
                    return $this->corporacaoId;
                }

public function getPercentualCompletoInt()
            {
                return $this->percentualCompletoInt;
            }

public function getPercentual_completo_INT()
                {
                    return $this->percentualCompletoInt;
                }

public function getPrazoSec()
            {
                return $this->prazoSec;
            }

public function getPrazo_SEC()
                {
                    return $this->prazoSec;
                }

public function getPrazoOffsec()
            {
                return $this->prazoOffsec;
            }

public function getPrazo_OFFSEC()
                {
                    return $this->prazoOffsec;
                }

public function getAtividadeId()
            {
                return $this->atividadeId;
            }

public function getAtividade_id_INT()
                {
                    return $this->atividadeId;
                }

public function getProtocoloEmpresaVendaInt()
            {
                return $this->protocoloEmpresaVendaInt;
            }

public function getProtocolo_empresa_venda_INT()
                {
                    return $this->protocoloEmpresaVendaInt;
                }

public function getProtocoloEmpresaAtividadeVendaInt()
            {
                return $this->protocoloEmpresaAtividadeVendaInt;
            }

public function getProtocolo_empresa_atividade_venda_INT()
                {
                    return $this->protocoloEmpresaAtividadeVendaInt;
                }



function setId($value)
            {
                $this->id = $value;
            }

function setCriadoPeloUsuarioId($value)
            {
                $this->criadoPeloUsuarioId = $value;
            }

public function setCriado_pelo_usuario_id_INT($value)
                { 
                    $this->criadoPeloUsuarioId = $value; 
                }

function setCategoriaPermissaoId($value)
            {
                $this->categoriaPermissaoId = $value;
            }

public function setCategoria_permissao_id_INT($value)
                { 
                    $this->categoriaPermissaoId = $value; 
                }

function setVeiculoId($value)
            {
                $this->veiculoId = $value;
            }

public function setVeiculo_id_INT($value)
                { 
                    $this->veiculoId = $value; 
                }

function setUsuarioId($value)
            {
                $this->usuarioId = $value;
            }

public function setUsuario_id_INT($value)
                { 
                    $this->usuarioId = $value; 
                }

function setEmpresaEquipeId($value)
            {
                $this->empresaEquipeId = $value;
            }

public function setEmpresa_equipe_id_INT($value)
                { 
                    $this->empresaEquipeId = $value; 
                }

function setOrigemPessoaId($value)
            {
                $this->origemPessoaId = $value;
            }

public function setOrigem_pessoa_id_INT($value)
                { 
                    $this->origemPessoaId = $value; 
                }

function setOrigemEmpresaId($value)
            {
                $this->origemEmpresaId = $value;
            }

public function setOrigem_empresa_id_INT($value)
                { 
                    $this->origemEmpresaId = $value; 
                }

function setOrigemLogradouro($value)
            {
                $this->origemLogradouro = $value;
            }

public function setOrigem_logradouro($value)
                { 
                    $this->origemLogradouro = $value; 
                }

function setOrigemNumero($value)
            {
                $this->origemNumero = $value;
            }

public function setOrigem_numero($value)
                { 
                    $this->origemNumero = $value; 
                }

function setOrigemCidadeId($value)
            {
                $this->origemCidadeId = $value;
            }

public function setOrigem_cidade_id_INT($value)
                { 
                    $this->origemCidadeId = $value; 
                }

function setOrigemLatitudeInt($value)
            {
                $this->origemLatitudeInt = $value;
            }

public function setOrigem_latitude_INT($value)
                { 
                    $this->origemLatitudeInt = $value; 
                }

function setOrigemLongitudeInt($value)
            {
                $this->origemLongitudeInt = $value;
            }

public function setOrigem_longitude_INT($value)
                { 
                    $this->origemLongitudeInt = $value; 
                }

function setOrigemLatitudeRealInt($value)
            {
                $this->origemLatitudeRealInt = $value;
            }

public function setOrigem_latitude_real_INT($value)
                { 
                    $this->origemLatitudeRealInt = $value; 
                }

function setOrigemLongitudeRealInt($value)
            {
                $this->origemLongitudeRealInt = $value;
            }

public function setOrigem_longitude_real_INT($value)
                { 
                    $this->origemLongitudeRealInt = $value; 
                }

function setDestinoPessoaId($value)
            {
                $this->destinoPessoaId = $value;
            }

public function setDestino_pessoa_id_INT($value)
                { 
                    $this->destinoPessoaId = $value; 
                }

function setDestinoEmpresaId($value)
            {
                $this->destinoEmpresaId = $value;
            }

public function setDestino_empresa_id_INT($value)
                { 
                    $this->destinoEmpresaId = $value; 
                }

function setDestinoLogradouro($value)
            {
                $this->destinoLogradouro = $value;
            }

public function setDestino_logradouro($value)
                { 
                    $this->destinoLogradouro = $value; 
                }

function setDestinoNumero($value)
            {
                $this->destinoNumero = $value;
            }

public function setDestino_numero($value)
                { 
                    $this->destinoNumero = $value; 
                }

function setDestinoCidadeId($value)
            {
                $this->destinoCidadeId = $value;
            }

public function setDestino_cidade_id_INT($value)
                { 
                    $this->destinoCidadeId = $value; 
                }

function setDestinoLatitudeInt($value)
            {
                $this->destinoLatitudeInt = $value;
            }

public function setDestino_latitude_INT($value)
                { 
                    $this->destinoLatitudeInt = $value; 
                }

function setDestinoLongitudeInt($value)
            {
                $this->destinoLongitudeInt = $value;
            }

public function setDestino_longitude_INT($value)
                { 
                    $this->destinoLongitudeInt = $value; 
                }

function setDestinoLatitudeRealInt($value)
            {
                $this->destinoLatitudeRealInt = $value;
            }

public function setDestino_latitude_real_INT($value)
                { 
                    $this->destinoLatitudeRealInt = $value; 
                }

function setDestinoLongitudeRealInt($value)
            {
                $this->destinoLongitudeRealInt = $value;
            }

public function setDestino_longitude_real_INT($value)
                { 
                    $this->destinoLongitudeRealInt = $value; 
                }

function setTempoEstimadoCarroInt($value)
            {
                $this->tempoEstimadoCarroInt = $value;
            }

public function setTempo_estimado_carro_INT($value)
                { 
                    $this->tempoEstimadoCarroInt = $value; 
                }

function setTempoEstimadoAPeInt($value)
            {
                $this->tempoEstimadoAPeInt = $value;
            }

public function setTempo_estimado_a_pe_INT($value)
                { 
                    $this->tempoEstimadoAPeInt = $value; 
                }

function setDistanciaEstimadaCarroInt($value)
            {
                $this->distanciaEstimadaCarroInt = $value;
            }

public function setDistancia_estimada_carro_INT($value)
                { 
                    $this->distanciaEstimadaCarroInt = $value; 
                }

function setDistanciaEstimadaAPeInt($value)
            {
                $this->distanciaEstimadaAPeInt = $value;
            }

public function setDistancia_estimada_a_pe_INT($value)
                { 
                    $this->distanciaEstimadaAPeInt = $value; 
                }

function setInicioHoraProgramadaSec($value)
            {
                $this->inicioHoraProgramadaSec = $value;
            }

public function setInicio_hora_programada_SEC($value)
                { 
                    $this->inicioHoraProgramadaSec = $value; 
                }

function setInicioHoraProgramadaOffsec($value)
            {
                $this->inicioHoraProgramadaOffsec = $value;
            }

public function setInicio_hora_programada_OFFSEC($value)
                { 
                    $this->inicioHoraProgramadaOffsec = $value; 
                }

function setDataExibirSec($value)
            {
                $this->dataExibirSec = $value;
            }

public function setData_exibir_SEC($value)
                { 
                    $this->dataExibirSec = $value; 
                }

function setDataExibirOffsec($value)
            {
                $this->dataExibirOffsec = $value;
            }

public function setData_exibir_OFFSEC($value)
                { 
                    $this->dataExibirOffsec = $value; 
                }

function setInicioSec($value)
            {
                $this->inicioSec = $value;
            }

public function setInicio_SEC($value)
                { 
                    $this->inicioSec = $value; 
                }

function setInicioOffsec($value)
            {
                $this->inicioOffsec = $value;
            }

public function setInicio_OFFSEC($value)
                { 
                    $this->inicioOffsec = $value; 
                }

function setFimSec($value)
            {
                $this->fimSec = $value;
            }

public function setFim_SEC($value)
                { 
                    $this->fimSec = $value; 
                }

function setFimOffsec($value)
            {
                $this->fimOffsec = $value;
            }

public function setFim_OFFSEC($value)
                { 
                    $this->fimOffsec = $value; 
                }

function setCadastroSec($value)
            {
                $this->cadastroSec = $value;
            }

public function setCadastro_SEC($value)
                { 
                    $this->cadastroSec = $value; 
                }

function setCadastroOffsec($value)
            {
                $this->cadastroOffsec = $value;
            }

public function setCadastro_OFFSEC($value)
                { 
                    $this->cadastroOffsec = $value; 
                }

function setTitulo($value)
            {
                $this->titulo = $value;
            }

function setTituloNormalizado($value)
            {
                $this->tituloNormalizado = $value;
            }

public function setTitulo_normalizado($value)
                { 
                    $this->tituloNormalizado = $value; 
                }

function setDescricao($value)
            {
                $this->descricao = $value;
            }

function setEmpresaAtividadeId($value)
            {
                $this->empresaAtividadeId = $value;
            }

public function setEmpresa_atividade_id_INT($value)
                { 
                    $this->empresaAtividadeId = $value; 
                }

function setTipoTarefaId($value)
            {
                $this->tipoTarefaId = $value;
            }

public function setTipo_tarefa_id_INT($value)
                { 
                    $this->tipoTarefaId = $value; 
                }

function setEmpresaVendaId($value)
            {
                $this->empresaVendaId = $value;
            }

public function setEmpresa_venda_id_INT($value)
                { 
                    $this->empresaVendaId = $value; 
                }

function setIdPrioridadeInt($value)
            {
                $this->idPrioridadeInt = $value;
            }

public function setId_prioridade_INT($value)
                { 
                    $this->idPrioridadeInt = $value; 
                }

function setRegistroEstadoId($value)
            {
                $this->registroEstadoId = $value;
            }

public function setRegistro_estado_id_INT($value)
                { 
                    $this->registroEstadoId = $value; 
                }

function setRegistroEstadoCorporacaoId($value)
            {
                $this->registroEstadoCorporacaoId = $value;
            }

public function setRegistro_estado_corporacao_id_INT($value)
                { 
                    $this->registroEstadoCorporacaoId = $value; 
                }

function setCorporacaoId($value)
            {
                $this->corporacaoId = $value;
            }

public function setCorporacao_id_INT($value)
                { 
                    $this->corporacaoId = $value; 
                }

function setPercentualCompletoInt($value)
            {
                $this->percentualCompletoInt = $value;
            }

public function setPercentual_completo_INT($value)
                { 
                    $this->percentualCompletoInt = $value; 
                }

function setPrazoSec($value)
            {
                $this->prazoSec = $value;
            }

public function setPrazo_SEC($value)
                { 
                    $this->prazoSec = $value; 
                }

function setPrazoOffsec($value)
            {
                $this->prazoOffsec = $value;
            }

public function setPrazo_OFFSEC($value)
                { 
                    $this->prazoOffsec = $value; 
                }

function setAtividadeId($value)
            {
                $this->atividadeId = $value;
            }

public function setAtividade_id_INT($value)
                { 
                    $this->atividadeId = $value; 
                }

function setProtocoloEmpresaVendaInt($value)
            {
                $this->protocoloEmpresaVendaInt = $value;
            }

public function setProtocolo_empresa_venda_INT($value)
                { 
                    $this->protocoloEmpresaVendaInt = $value; 
                }

function setProtocoloEmpresaAtividadeVendaInt($value)
            {
                $this->protocoloEmpresaAtividadeVendaInt = $value;
            }

public function setProtocolo_empresa_atividade_venda_INT($value)
                { 
                    $this->protocoloEmpresaAtividadeVendaInt = $value; 
                }



public function clear()
        {$this->id = null;
$this->criadoPeloUsuarioId = null;
if($this->objCriado_pelo_usuario != null) unset($this->objCriado_pelo_usuario);
$this->categoriaPermissaoId = null;
if($this->objCategoria_permissao != null) unset($this->objCategoria_permissao);
$this->veiculoId = null;
if($this->objVeiculo != null) unset($this->objVeiculo);
$this->usuarioId = null;
if($this->objUsuario != null) unset($this->objUsuario);
$this->empresaEquipeId = null;
if($this->objEmpresa_equipe != null) unset($this->objEmpresa_equipe);
$this->origemPessoaId = null;
if($this->objOrigem_pessoa != null) unset($this->objOrigem_pessoa);
$this->origemEmpresaId = null;
if($this->objOrigem_empresa != null) unset($this->objOrigem_empresa);
$this->origemLogradouro = null;
$this->origemNumero = null;
$this->origemCidadeId = null;
if($this->objOrigem_cidade != null) unset($this->objOrigem_cidade);
$this->origemLatitudeInt = null;
$this->origemLongitudeInt = null;
$this->origemLatitudeRealInt = null;
$this->origemLongitudeRealInt = null;
$this->destinoPessoaId = null;
if($this->objDestino_pessoa != null) unset($this->objDestino_pessoa);
$this->destinoEmpresaId = null;
if($this->objDestino_empresa != null) unset($this->objDestino_empresa);
$this->destinoLogradouro = null;
$this->destinoNumero = null;
$this->destinoCidadeId = null;
if($this->objDestino_cidade != null) unset($this->objDestino_cidade);
$this->destinoLatitudeInt = null;
$this->destinoLongitudeInt = null;
$this->destinoLatitudeRealInt = null;
$this->destinoLongitudeRealInt = null;
$this->tempoEstimadoCarroInt = null;
$this->tempoEstimadoAPeInt = null;
$this->distanciaEstimadaCarroInt = null;
$this->distanciaEstimadaAPeInt = null;
$this->inicioHoraProgramadaSec = null;
$this->inicioHoraProgramadaOffsec = null;
$this->dataExibirSec = null;
$this->dataExibirOffsec = null;
$this->inicioSec = null;
$this->inicioOffsec = null;
$this->fimSec = null;
$this->fimOffsec = null;
$this->cadastroSec = null;
$this->cadastroOffsec = null;
$this->titulo = null;
$this->tituloNormalizado = null;
$this->descricao = null;
$this->empresaAtividadeId = null;
if($this->objEmpresa_atividade != null) unset($this->objEmpresa_atividade);
$this->tipoTarefaId = null;
if($this->objTipo_tarefa != null) unset($this->objTipo_tarefa);
$this->empresaVendaId = null;
if($this->objEmpresa_venda != null) unset($this->objEmpresa_venda);
$this->idPrioridadeInt = null;
$this->registroEstadoId = null;
if($this->objRegistro_estado != null) unset($this->objRegistro_estado);
$this->registroEstadoCorporacaoId = null;
if($this->objRegistro_estado_corporacao != null) unset($this->objRegistro_estado_corporacao);
$this->corporacaoId = null;
if($this->objCorporacao != null) unset($this->objCorporacao);
$this->percentualCompletoInt = null;
$this->prazoSec = null;
$this->prazoOffsec = null;
$this->atividadeId = null;
if($this->objAtividade != null) unset($this->objAtividade);
$this->protocoloEmpresaVendaInt = null;
$this->protocoloEmpresaAtividadeVendaInt = null;
if(is_array($this->arrEstadosCampos)){
                        unset($this->arrEstadosCampos);
                     $this->arrEstadosCampos = array();
                    }
                }

public function formatarParaSQL()
        {
$this->tituloNormalizado = $this->formatarStringNormalizadaParaComandoSQL($this->titulo);
if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->id)){
$this->id = $this->formatarIntegerParaComandoSQL($this->id);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->id);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->criadoPeloUsuarioId)){
$this->criadoPeloUsuarioId = $this->formatarIntegerParaComandoSQL($this->criadoPeloUsuarioId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->criadoPeloUsuarioId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->categoriaPermissaoId)){
$this->categoriaPermissaoId = $this->formatarIntegerParaComandoSQL($this->categoriaPermissaoId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->categoriaPermissaoId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->veiculoId)){
$this->veiculoId = $this->formatarIntegerParaComandoSQL($this->veiculoId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->veiculoId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->usuarioId)){
$this->usuarioId = $this->formatarIntegerParaComandoSQL($this->usuarioId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->usuarioId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->empresaEquipeId)){
$this->empresaEquipeId = $this->formatarIntegerParaComandoSQL($this->empresaEquipeId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->empresaEquipeId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->origemPessoaId)){
$this->origemPessoaId = $this->formatarIntegerParaComandoSQL($this->origemPessoaId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->origemPessoaId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->origemEmpresaId)){
$this->origemEmpresaId = $this->formatarIntegerParaComandoSQL($this->origemEmpresaId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->origemEmpresaId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->origemLogradouro)){
$this->origemLogradouro = $this->formatarStringParaComandoSQL($this->origemLogradouro);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->origemLogradouro);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->origemNumero)){
$this->origemNumero = $this->formatarStringParaComandoSQL($this->origemNumero);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->origemNumero);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->origemCidadeId)){
$this->origemCidadeId = $this->formatarIntegerParaComandoSQL($this->origemCidadeId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->origemCidadeId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->origemLatitudeInt)){
$this->origemLatitudeInt = $this->formatarIntegerParaComandoSQL($this->origemLatitudeInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->origemLatitudeInt);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->origemLongitudeInt)){
$this->origemLongitudeInt = $this->formatarIntegerParaComandoSQL($this->origemLongitudeInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->origemLongitudeInt);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->origemLatitudeRealInt)){
$this->origemLatitudeRealInt = $this->formatarIntegerParaComandoSQL($this->origemLatitudeRealInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->origemLatitudeRealInt);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->origemLongitudeRealInt)){
$this->origemLongitudeRealInt = $this->formatarIntegerParaComandoSQL($this->origemLongitudeRealInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->origemLongitudeRealInt);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->destinoPessoaId)){
$this->destinoPessoaId = $this->formatarIntegerParaComandoSQL($this->destinoPessoaId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->destinoPessoaId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->destinoEmpresaId)){
$this->destinoEmpresaId = $this->formatarIntegerParaComandoSQL($this->destinoEmpresaId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->destinoEmpresaId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->destinoLogradouro)){
$this->destinoLogradouro = $this->formatarStringParaComandoSQL($this->destinoLogradouro);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->destinoLogradouro);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->destinoNumero)){
$this->destinoNumero = $this->formatarStringParaComandoSQL($this->destinoNumero);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->destinoNumero);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->destinoCidadeId)){
$this->destinoCidadeId = $this->formatarIntegerParaComandoSQL($this->destinoCidadeId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->destinoCidadeId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->destinoLatitudeInt)){
$this->destinoLatitudeInt = $this->formatarIntegerParaComandoSQL($this->destinoLatitudeInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->destinoLatitudeInt);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->destinoLongitudeInt)){
$this->destinoLongitudeInt = $this->formatarIntegerParaComandoSQL($this->destinoLongitudeInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->destinoLongitudeInt);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->destinoLatitudeRealInt)){
$this->destinoLatitudeRealInt = $this->formatarIntegerParaComandoSQL($this->destinoLatitudeRealInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->destinoLatitudeRealInt);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->destinoLongitudeRealInt)){
$this->destinoLongitudeRealInt = $this->formatarIntegerParaComandoSQL($this->destinoLongitudeRealInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->destinoLongitudeRealInt);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->tempoEstimadoCarroInt)){
$this->tempoEstimadoCarroInt = $this->formatarIntegerParaComandoSQL($this->tempoEstimadoCarroInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->tempoEstimadoCarroInt);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->tempoEstimadoAPeInt)){
$this->tempoEstimadoAPeInt = $this->formatarIntegerParaComandoSQL($this->tempoEstimadoAPeInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->tempoEstimadoAPeInt);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->distanciaEstimadaCarroInt)){
$this->distanciaEstimadaCarroInt = $this->formatarIntegerParaComandoSQL($this->distanciaEstimadaCarroInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->distanciaEstimadaCarroInt);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->distanciaEstimadaAPeInt)){
$this->distanciaEstimadaAPeInt = $this->formatarIntegerParaComandoSQL($this->distanciaEstimadaAPeInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->distanciaEstimadaAPeInt);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->inicioHoraProgramadaSec)){
$this->inicioHoraProgramadaSec = $this->formatarIntegerParaComandoSQL($this->inicioHoraProgramadaSec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->inicioHoraProgramadaSec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->inicioHoraProgramadaOffsec)){
$this->inicioHoraProgramadaOffsec = $this->formatarIntegerParaComandoSQL($this->inicioHoraProgramadaOffsec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->inicioHoraProgramadaOffsec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->dataExibirSec)){
$this->dataExibirSec = $this->formatarIntegerParaComandoSQL($this->dataExibirSec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->dataExibirSec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->dataExibirOffsec)){
$this->dataExibirOffsec = $this->formatarIntegerParaComandoSQL($this->dataExibirOffsec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->dataExibirOffsec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->inicioSec)){
$this->inicioSec = $this->formatarIntegerParaComandoSQL($this->inicioSec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->inicioSec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->inicioOffsec)){
$this->inicioOffsec = $this->formatarIntegerParaComandoSQL($this->inicioOffsec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->inicioOffsec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->fimSec)){
$this->fimSec = $this->formatarIntegerParaComandoSQL($this->fimSec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->fimSec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->fimOffsec)){
$this->fimOffsec = $this->formatarIntegerParaComandoSQL($this->fimOffsec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->fimOffsec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->cadastroSec)){
$this->cadastroSec = $this->formatarIntegerParaComandoSQL($this->cadastroSec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->cadastroSec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->cadastroOffsec)){
$this->cadastroOffsec = $this->formatarIntegerParaComandoSQL($this->cadastroOffsec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->cadastroOffsec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->titulo)){
$this->titulo = $this->formatarStringParaComandoSQL($this->titulo);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->titulo);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->descricao)){
$this->descricao = $this->formatarStringParaComandoSQL($this->descricao);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->descricao);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->empresaAtividadeId)){
$this->empresaAtividadeId = $this->formatarIntegerParaComandoSQL($this->empresaAtividadeId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->empresaAtividadeId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->tipoTarefaId)){
$this->tipoTarefaId = $this->formatarIntegerParaComandoSQL($this->tipoTarefaId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->tipoTarefaId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->empresaVendaId)){
$this->empresaVendaId = $this->formatarIntegerParaComandoSQL($this->empresaVendaId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->empresaVendaId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->idPrioridadeInt)){
$this->idPrioridadeInt = $this->formatarIntegerParaComandoSQL($this->idPrioridadeInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->idPrioridadeInt);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->registroEstadoId)){
$this->registroEstadoId = $this->formatarIntegerParaComandoSQL($this->registroEstadoId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->registroEstadoId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->registroEstadoCorporacaoId)){
$this->registroEstadoCorporacaoId = $this->formatarIntegerParaComandoSQL($this->registroEstadoCorporacaoId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->registroEstadoCorporacaoId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->corporacaoId)){
$this->corporacaoId = $this->formatarIntegerParaComandoSQL($this->corporacaoId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->corporacaoId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->percentualCompletoInt)){
$this->percentualCompletoInt = $this->formatarIntegerParaComandoSQL($this->percentualCompletoInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->percentualCompletoInt);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->prazoSec)){
$this->prazoSec = $this->formatarIntegerParaComandoSQL($this->prazoSec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->prazoSec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->prazoOffsec)){
$this->prazoOffsec = $this->formatarIntegerParaComandoSQL($this->prazoOffsec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->prazoOffsec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->atividadeId)){
$this->atividadeId = $this->formatarIntegerParaComandoSQL($this->atividadeId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->atividadeId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->protocoloEmpresaVendaInt)){
$this->protocoloEmpresaVendaInt = $this->formatarIntegerParaComandoSQL($this->protocoloEmpresaVendaInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->protocoloEmpresaVendaInt);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->protocoloEmpresaAtividadeVendaInt)){
$this->protocoloEmpresaAtividadeVendaInt = $this->formatarIntegerParaComandoSQL($this->protocoloEmpresaAtividadeVendaInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->protocoloEmpresaAtividadeVendaInt);
}

}

public function formatarParaExibicao()
        {if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->origemLogradouro)){
$this->origemLogradouro = $this->formatarStringParaExibicao($this->origemLogradouro);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->origemLogradouro);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->origemNumero)){
$this->origemNumero = $this->formatarStringParaExibicao($this->origemNumero);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->origemNumero);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->destinoLogradouro)){
$this->destinoLogradouro = $this->formatarStringParaExibicao($this->destinoLogradouro);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->destinoLogradouro);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->destinoNumero)){
$this->destinoNumero = $this->formatarStringParaExibicao($this->destinoNumero);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->destinoNumero);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->titulo)){
$this->titulo = $this->formatarStringParaExibicao($this->titulo);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->titulo);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->tituloNormalizado)){
$this->tituloNormalizado = $this->formatarStringParaExibicao($this->tituloNormalizado);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->tituloNormalizado);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->descricao)){
$this->descricao = $this->formatarStringParaExibicao($this->descricao);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->descricao);
}

}

function select($id, $idCorporacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }   

            $sql = "SELECT id, criado_pelo_usuario_id_INT, categoria_permissao_id_INT, veiculo_id_INT, usuario_id_INT, empresa_equipe_id_INT, origem_pessoa_id_INT, origem_empresa_id_INT, origem_logradouro, origem_numero, origem_cidade_id_INT, origem_latitude_INT, origem_longitude_INT, origem_latitude_real_INT, origem_longitude_real_INT, destino_pessoa_id_INT, destino_empresa_id_INT, destino_logradouro, destino_numero, destino_cidade_id_INT, destino_latitude_INT, destino_longitude_INT, destino_latitude_real_INT, destino_longitude_real_INT, tempo_estimado_carro_INT, tempo_estimado_a_pe_INT, distancia_estimada_carro_INT, distancia_estimada_a_pe_INT, inicio_hora_programada_SEC, inicio_hora_programada_OFFSEC, data_exibir_SEC, data_exibir_OFFSEC, inicio_SEC, inicio_OFFSEC, fim_SEC, fim_OFFSEC, cadastro_SEC, cadastro_OFFSEC, titulo, titulo_normalizado, descricao, empresa_atividade_id_INT, tipo_tarefa_id_INT, empresa_venda_id_INT, id_prioridade_INT, registro_estado_id_INT, registro_estado_corporacao_id_INT, corporacao_id_INT, percentual_completo_INT, prazo_SEC, prazo_OFFSEC, atividade_id_INT, protocolo_empresa_venda_INT, protocolo_empresa_atividade_venda_INT FROM tarefa WHERE id = $id AND corporacao_id_INT = $idCorporacao";

            $msg = $this->database->queryMensagem($sql);
            $result = $this->database->result;

            if($msg != null && $msg->erro() )
            { 
            		$this->clear();
            		return $msg;
        	} 
        	else if($this->database->rows() == 0)
        	{
                return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nenhum registro foi encontrado."));
            }
        

            $row = $this->database->fetchArray(MYSQLI_NUM);
            
            		$this->id = $row[0];
		$this->criadoPeloUsuarioId = $row[1];
		$this->categoriaPermissaoId = $row[2];
		$this->veiculoId = $row[3];
		$this->usuarioId = $row[4];
		$this->empresaEquipeId = $row[5];
		$this->origemPessoaId = $row[6];
		$this->origemEmpresaId = $row[7];
		$this->origemLogradouro = $row[8];
		$this->origemNumero = $row[9];
		$this->origemCidadeId = $row[10];
		$this->origemLatitudeInt = $row[11];
		$this->origemLongitudeInt = $row[12];
		$this->origemLatitudeRealInt = $row[13];
		$this->origemLongitudeRealInt = $row[14];
		$this->destinoPessoaId = $row[15];
		$this->destinoEmpresaId = $row[16];
		$this->destinoLogradouro = $row[17];
		$this->destinoNumero = $row[18];
		$this->destinoCidadeId = $row[19];
		$this->destinoLatitudeInt = $row[20];
		$this->destinoLongitudeInt = $row[21];
		$this->destinoLatitudeRealInt = $row[22];
		$this->destinoLongitudeRealInt = $row[23];
		$this->tempoEstimadoCarroInt = $row[24];
		$this->tempoEstimadoAPeInt = $row[25];
		$this->distanciaEstimadaCarroInt = $row[26];
		$this->distanciaEstimadaAPeInt = $row[27];
		$this->inicioHoraProgramadaSec = $row[28];
		$this->inicioHoraProgramadaOffsec = $row[29];
		$this->dataExibirSec = $row[30];
		$this->dataExibirOffsec = $row[31];
		$this->inicioSec = $row[32];
		$this->inicioOffsec = $row[33];
		$this->fimSec = $row[34];
		$this->fimOffsec = $row[35];
		$this->cadastroSec = $row[36];
		$this->cadastroOffsec = $row[37];
		$this->titulo = $row[38];
		$this->tituloNormalizado = $row[39];
		$this->descricao = $row[40];
		$this->empresaAtividadeId = $row[41];
		$this->tipoTarefaId = $row[42];
		$this->empresaVendaId = $row[43];
		$this->idPrioridadeInt = $row[44];
		$this->registroEstadoId = $row[45];
		$this->registroEstadoCorporacaoId = $row[46];
		$this->percentualCompletoInt = $row[48];
		$this->prazoSec = $row[49];
		$this->prazoOffsec = $row[50];
		$this->atividadeId = $row[51];
		$this->protocoloEmpresaVendaInt = $row[52];
		$this->protocoloEmpresaAtividadeVendaInt = $row[53];

            
        }

public function delete($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (is_null($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }

            $sql = "DELETE FROM tarefa WHERE id= $id AND corporacao_id_INT = $idCorporacao";
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                    && $sincronizar
                        && static::idSistemaTabela() != null
                            && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_REMOVE,
                                null,
                                null,
                                $id,
                                $this->database);
            }

            return $msg;
        }

public function insert($sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null, $opcoes = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $this->defineDataCadastroInSecondsIfNotDefined();
$this->defineDataCadastroOffsetInSecondsIfNotDefined();

            
            if (is_null($opcoes))
            {
                $opcoes = $this->opcoesDAO;
            }

            if (!is_numeric($this->id))
            {
                $this->id = "null";
                if ($opcoes == null || $opcoes[Database::OPCAO_GERAR_ID] != false) 
                {
                    $this->id = EXTDAO_Sistema_sequencia::gerarId(static::nomeTabela());                    
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }    
                    else if($msg == null)
                    {
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
                else 
                {
                    $this->id = null;
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao));
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }
                    else if($msg == null)
                    {
                        $this->id = $this->database->getLastInsertId();
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
            }
            else 
            {
                $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }                        
                else if($msg == null)
                {
                   $msg = new Mensagem_token();
                   $msg->mValor = $this->id;
                }
            }
            if($msg == null || $msg->ok())
            {                
                if($sincronizar
                    && static::idSistemaTabela() != null
                    && isset($this->id)){
                    
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_INSERT,
                                null,
                                null,
                                $this->id,
                                $this->database); 
                	}
            } 
            else
            {
                $this->id = null;
            }
            
            return $msg;
            
        }

public function getSQLInsert($idCorporacao){ 

return "INSERT INTO tarefa (id, criado_pelo_usuario_id_INT, categoria_permissao_id_INT, veiculo_id_INT, usuario_id_INT, empresa_equipe_id_INT, origem_pessoa_id_INT, origem_empresa_id_INT, origem_logradouro, origem_numero, origem_cidade_id_INT, origem_latitude_INT, origem_longitude_INT, origem_latitude_real_INT, origem_longitude_real_INT, destino_pessoa_id_INT, destino_empresa_id_INT, destino_logradouro, destino_numero, destino_cidade_id_INT, destino_latitude_INT, destino_longitude_INT, destino_latitude_real_INT, destino_longitude_real_INT, tempo_estimado_carro_INT, tempo_estimado_a_pe_INT, distancia_estimada_carro_INT, distancia_estimada_a_pe_INT, inicio_hora_programada_SEC, inicio_hora_programada_OFFSEC, data_exibir_SEC, data_exibir_OFFSEC, inicio_SEC, inicio_OFFSEC, fim_SEC, fim_OFFSEC, cadastro_SEC, cadastro_OFFSEC, titulo, titulo_normalizado, descricao, empresa_atividade_id_INT, tipo_tarefa_id_INT, empresa_venda_id_INT, id_prioridade_INT, registro_estado_id_INT, registro_estado_corporacao_id_INT, corporacao_id_INT, percentual_completo_INT, prazo_SEC, prazo_OFFSEC, atividade_id_INT, protocolo_empresa_venda_INT, protocolo_empresa_atividade_venda_INT) VALUES ( $this->id ,  $this->criadoPeloUsuarioId ,  $this->categoriaPermissaoId ,  $this->veiculoId ,  $this->usuarioId ,  $this->empresaEquipeId ,  $this->origemPessoaId ,  $this->origemEmpresaId ,  $this->origemLogradouro ,  $this->origemNumero ,  $this->origemCidadeId ,  $this->origemLatitudeInt ,  $this->origemLongitudeInt ,  $this->origemLatitudeRealInt ,  $this->origemLongitudeRealInt ,  $this->destinoPessoaId ,  $this->destinoEmpresaId ,  $this->destinoLogradouro ,  $this->destinoNumero ,  $this->destinoCidadeId ,  $this->destinoLatitudeInt ,  $this->destinoLongitudeInt ,  $this->destinoLatitudeRealInt ,  $this->destinoLongitudeRealInt ,  $this->tempoEstimadoCarroInt ,  $this->tempoEstimadoAPeInt ,  $this->distanciaEstimadaCarroInt ,  $this->distanciaEstimadaAPeInt ,  $this->inicioHoraProgramadaSec ,  $this->inicioHoraProgramadaOffsec ,  $this->dataExibirSec ,  $this->dataExibirOffsec ,  $this->inicioSec ,  $this->inicioOffsec ,  $this->fimSec ,  $this->fimOffsec ,  $this->cadastroSec ,  $this->cadastroOffsec ,  $this->titulo ,  $this->tituloNormalizado ,  $this->descricao ,  $this->empresaAtividadeId ,  $this->tipoTarefaId ,  $this->empresaVendaId ,  $this->idPrioridadeInt ,  $this->registroEstadoId ,  $this->registroEstadoCorporacaoId ,  $idCorporacao ,  $this->percentualCompletoInt ,  $this->prazoSec ,  $this->prazoOffsec ,  $this->atividadeId ,  $this->protocoloEmpresaVendaInt ,  $this->protocoloEmpresaAtividadeVendaInt ) "; 

}




        
        public function updateByParameters($id, $objParametros, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao))
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();

            
                
                if (isset($objParametros->criadoPeloUsuarioId)) 
                {
                    $arrUpdateFields[] = " criado_pelo_usuario_id_INT = {$objParametros->criadoPeloUsuarioId} ";
                }


                
                if (isset($objParametros->categoriaPermissaoId)) 
                {
                    $arrUpdateFields[] = " categoria_permissao_id_INT = {$objParametros->categoriaPermissaoId} ";
                }


                
                if (isset($objParametros->veiculoId)) 
                {
                    $arrUpdateFields[] = " veiculo_id_INT = {$objParametros->veiculoId} ";
                }


                
                if (isset($objParametros->usuarioId)) 
                {
                    $arrUpdateFields[] = " usuario_id_INT = {$objParametros->usuarioId} ";
                }


                
                if (isset($objParametros->empresaEquipeId)) 
                {
                    $arrUpdateFields[] = " empresa_equipe_id_INT = {$objParametros->empresaEquipeId} ";
                }


                
                if (isset($objParametros->origemPessoaId)) 
                {
                    $arrUpdateFields[] = " origem_pessoa_id_INT = {$objParametros->origemPessoaId} ";
                }


                
                if (isset($objParametros->origemEmpresaId)) 
                {
                    $arrUpdateFields[] = " origem_empresa_id_INT = {$objParametros->origemEmpresaId} ";
                }


                
                if (isset($objParametros->origemLogradouro)) 
                {
                    $arrUpdateFields[] = " origem_logradouro = {$objParametros->origemLogradouro} ";
                }


                
                if (isset($objParametros->origemNumero)) 
                {
                    $arrUpdateFields[] = " origem_numero = {$objParametros->origemNumero} ";
                }


                
                if (isset($objParametros->origemCidadeId)) 
                {
                    $arrUpdateFields[] = " origem_cidade_id_INT = {$objParametros->origemCidadeId} ";
                }


                
                if (isset($objParametros->origemLatitudeInt)) 
                {
                    $arrUpdateFields[] = " origem_latitude_INT = {$objParametros->origemLatitudeInt} ";
                }


                
                if (isset($objParametros->origemLongitudeInt)) 
                {
                    $arrUpdateFields[] = " origem_longitude_INT = {$objParametros->origemLongitudeInt} ";
                }


                
                if (isset($objParametros->origemLatitudeRealInt)) 
                {
                    $arrUpdateFields[] = " origem_latitude_real_INT = {$objParametros->origemLatitudeRealInt} ";
                }


                
                if (isset($objParametros->origemLongitudeRealInt)) 
                {
                    $arrUpdateFields[] = " origem_longitude_real_INT = {$objParametros->origemLongitudeRealInt} ";
                }


                
                if (isset($objParametros->destinoPessoaId)) 
                {
                    $arrUpdateFields[] = " destino_pessoa_id_INT = {$objParametros->destinoPessoaId} ";
                }


                
                if (isset($objParametros->destinoEmpresaId)) 
                {
                    $arrUpdateFields[] = " destino_empresa_id_INT = {$objParametros->destinoEmpresaId} ";
                }


                
                if (isset($objParametros->destinoLogradouro)) 
                {
                    $arrUpdateFields[] = " destino_logradouro = {$objParametros->destinoLogradouro} ";
                }


                
                if (isset($objParametros->destinoNumero)) 
                {
                    $arrUpdateFields[] = " destino_numero = {$objParametros->destinoNumero} ";
                }


                
                if (isset($objParametros->destinoCidadeId)) 
                {
                    $arrUpdateFields[] = " destino_cidade_id_INT = {$objParametros->destinoCidadeId} ";
                }


                
                if (isset($objParametros->destinoLatitudeInt)) 
                {
                    $arrUpdateFields[] = " destino_latitude_INT = {$objParametros->destinoLatitudeInt} ";
                }


                
                if (isset($objParametros->destinoLongitudeInt)) 
                {
                    $arrUpdateFields[] = " destino_longitude_INT = {$objParametros->destinoLongitudeInt} ";
                }


                
                if (isset($objParametros->destinoLatitudeRealInt)) 
                {
                    $arrUpdateFields[] = " destino_latitude_real_INT = {$objParametros->destinoLatitudeRealInt} ";
                }


                
                if (isset($objParametros->destinoLongitudeRealInt)) 
                {
                    $arrUpdateFields[] = " destino_longitude_real_INT = {$objParametros->destinoLongitudeRealInt} ";
                }


                
                if (isset($objParametros->tempoEstimadoCarroInt)) 
                {
                    $arrUpdateFields[] = " tempo_estimado_carro_INT = {$objParametros->tempoEstimadoCarroInt} ";
                }


                
                if (isset($objParametros->tempoEstimadoAPeInt)) 
                {
                    $arrUpdateFields[] = " tempo_estimado_a_pe_INT = {$objParametros->tempoEstimadoAPeInt} ";
                }


                
                if (isset($objParametros->distanciaEstimadaCarroInt)) 
                {
                    $arrUpdateFields[] = " distancia_estimada_carro_INT = {$objParametros->distanciaEstimadaCarroInt} ";
                }


                
                if (isset($objParametros->distanciaEstimadaAPeInt)) 
                {
                    $arrUpdateFields[] = " distancia_estimada_a_pe_INT = {$objParametros->distanciaEstimadaAPeInt} ";
                }


                
                if (isset($objParametros->inicioHoraProgramadaSec)) 
                {
                    $arrUpdateFields[] = " inicio_hora_programada_SEC = {$objParametros->inicioHoraProgramadaSec} ";
                }


                
                if (isset($objParametros->inicioHoraProgramadaOffsec)) 
                {
                    $arrUpdateFields[] = " inicio_hora_programada_OFFSEC = {$objParametros->inicioHoraProgramadaOffsec} ";
                }


                
                if (isset($objParametros->dataExibirSec)) 
                {
                    $arrUpdateFields[] = " data_exibir_SEC = {$objParametros->dataExibirSec} ";
                }


                
                if (isset($objParametros->dataExibirOffsec)) 
                {
                    $arrUpdateFields[] = " data_exibir_OFFSEC = {$objParametros->dataExibirOffsec} ";
                }


                
                if (isset($objParametros->inicioSec)) 
                {
                    $arrUpdateFields[] = " inicio_SEC = {$objParametros->inicioSec} ";
                }


                
                if (isset($objParametros->inicioOffsec)) 
                {
                    $arrUpdateFields[] = " inicio_OFFSEC = {$objParametros->inicioOffsec} ";
                }


                
                if (isset($objParametros->fimSec)) 
                {
                    $arrUpdateFields[] = " fim_SEC = {$objParametros->fimSec} ";
                }


                
                if (isset($objParametros->fimOffsec)) 
                {
                    $arrUpdateFields[] = " fim_OFFSEC = {$objParametros->fimOffsec} ";
                }


                
                if (isset($objParametros->cadastroSec)) 
                {
                    $arrUpdateFields[] = " cadastro_SEC = {$objParametros->cadastroSec} ";
                }


                
                if (isset($objParametros->cadastroOffsec)) 
                {
                    $arrUpdateFields[] = " cadastro_OFFSEC = {$objParametros->cadastroOffsec} ";
                }


                
                if (isset($objParametros->titulo)) 
                {
                    $arrUpdateFields[] = " titulo = {$objParametros->titulo} ";
                }


                
                if (isset($objParametros->tituloNormalizado)) 
                {
                    $arrUpdateFields[] = " titulo_normalizado = {$objParametros->tituloNormalizado} ";
                }


                
                if (isset($objParametros->descricao)) 
                {
                    $arrUpdateFields[] = " descricao = {$objParametros->descricao} ";
                }


                
                if (isset($objParametros->empresaAtividadeId)) 
                {
                    $arrUpdateFields[] = " empresa_atividade_id_INT = {$objParametros->empresaAtividadeId} ";
                }


                
                if (isset($objParametros->tipoTarefaId)) 
                {
                    $arrUpdateFields[] = " tipo_tarefa_id_INT = {$objParametros->tipoTarefaId} ";
                }


                
                if (isset($objParametros->empresaVendaId)) 
                {
                    $arrUpdateFields[] = " empresa_venda_id_INT = {$objParametros->empresaVendaId} ";
                }


                
                if (isset($objParametros->idPrioridadeInt)) 
                {
                    $arrUpdateFields[] = " id_prioridade_INT = {$objParametros->idPrioridadeInt} ";
                }


                
                if (isset($objParametros->registroEstadoId)) 
                {
                    $arrUpdateFields[] = " registro_estado_id_INT = {$objParametros->registroEstadoId} ";
                }


                
                if (isset($objParametros->registroEstadoCorporacaoId)) 
                {
                    $arrUpdateFields[] = " registro_estado_corporacao_id_INT = {$objParametros->registroEstadoCorporacaoId} ";
                }


                
                if (isset($objParametros->percentualCompletoInt)) 
                {
                    $arrUpdateFields[] = " percentual_completo_INT = {$objParametros->percentualCompletoInt} ";
                }


                
                if (isset($objParametros->prazoSec)) 
                {
                    $arrUpdateFields[] = " prazo_SEC = {$objParametros->prazoSec} ";
                }


                
                if (isset($objParametros->prazoOffsec)) 
                {
                    $arrUpdateFields[] = " prazo_OFFSEC = {$objParametros->prazoOffsec} ";
                }


                
                if (isset($objParametros->atividadeId)) 
                {
                    $arrUpdateFields[] = " atividade_id_INT = {$objParametros->atividadeId} ";
                }


                
                if (isset($objParametros->protocoloEmpresaVendaInt)) 
                {
                    $arrUpdateFields[] = " protocolo_empresa_venda_INT = {$objParametros->protocoloEmpresaVendaInt} ";
                }


                
                if (isset($objParametros->protocoloEmpresaAtividadeVendaInt)) 
                {
                    $arrUpdateFields[] = " protocolo_empresa_atividade_venda_INT = {$objParametros->protocoloEmpresaAtividadeVendaInt} ";
                }



            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE tarefa SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";

            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null)
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function update($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($this->criadoPeloUsuarioId)) 
                {                                      
                    $arrUpdateFields[] = " criado_pelo_usuario_id_INT = {$this->criadoPeloUsuarioId} ";
                }


                
                if (isset($this->categoriaPermissaoId)) 
                {                                      
                    $arrUpdateFields[] = " categoria_permissao_id_INT = {$this->categoriaPermissaoId} ";
                }


                
                if (isset($this->veiculoId)) 
                {                                      
                    $arrUpdateFields[] = " veiculo_id_INT = {$this->veiculoId} ";
                }


                
                if (isset($this->usuarioId)) 
                {                                      
                    $arrUpdateFields[] = " usuario_id_INT = {$this->usuarioId} ";
                }


                
                if (isset($this->empresaEquipeId)) 
                {                                      
                    $arrUpdateFields[] = " empresa_equipe_id_INT = {$this->empresaEquipeId} ";
                }


                
                if (isset($this->origemPessoaId)) 
                {                                      
                    $arrUpdateFields[] = " origem_pessoa_id_INT = {$this->origemPessoaId} ";
                }


                
                if (isset($this->origemEmpresaId)) 
                {                                      
                    $arrUpdateFields[] = " origem_empresa_id_INT = {$this->origemEmpresaId} ";
                }


                
                if (isset($this->origemLogradouro)) 
                {                                      
                    $arrUpdateFields[] = " origem_logradouro = {$this->origemLogradouro} ";
                }


                
                if (isset($this->origemNumero)) 
                {                                      
                    $arrUpdateFields[] = " origem_numero = {$this->origemNumero} ";
                }


                
                if (isset($this->origemCidadeId)) 
                {                                      
                    $arrUpdateFields[] = " origem_cidade_id_INT = {$this->origemCidadeId} ";
                }


                
                if (isset($this->origemLatitudeInt)) 
                {                                      
                    $arrUpdateFields[] = " origem_latitude_INT = {$this->origemLatitudeInt} ";
                }


                
                if (isset($this->origemLongitudeInt)) 
                {                                      
                    $arrUpdateFields[] = " origem_longitude_INT = {$this->origemLongitudeInt} ";
                }


                
                if (isset($this->origemLatitudeRealInt)) 
                {                                      
                    $arrUpdateFields[] = " origem_latitude_real_INT = {$this->origemLatitudeRealInt} ";
                }


                
                if (isset($this->origemLongitudeRealInt)) 
                {                                      
                    $arrUpdateFields[] = " origem_longitude_real_INT = {$this->origemLongitudeRealInt} ";
                }


                
                if (isset($this->destinoPessoaId)) 
                {                                      
                    $arrUpdateFields[] = " destino_pessoa_id_INT = {$this->destinoPessoaId} ";
                }


                
                if (isset($this->destinoEmpresaId)) 
                {                                      
                    $arrUpdateFields[] = " destino_empresa_id_INT = {$this->destinoEmpresaId} ";
                }


                
                if (isset($this->destinoLogradouro)) 
                {                                      
                    $arrUpdateFields[] = " destino_logradouro = {$this->destinoLogradouro} ";
                }


                
                if (isset($this->destinoNumero)) 
                {                                      
                    $arrUpdateFields[] = " destino_numero = {$this->destinoNumero} ";
                }


                
                if (isset($this->destinoCidadeId)) 
                {                                      
                    $arrUpdateFields[] = " destino_cidade_id_INT = {$this->destinoCidadeId} ";
                }


                
                if (isset($this->destinoLatitudeInt)) 
                {                                      
                    $arrUpdateFields[] = " destino_latitude_INT = {$this->destinoLatitudeInt} ";
                }


                
                if (isset($this->destinoLongitudeInt)) 
                {                                      
                    $arrUpdateFields[] = " destino_longitude_INT = {$this->destinoLongitudeInt} ";
                }


                
                if (isset($this->destinoLatitudeRealInt)) 
                {                                      
                    $arrUpdateFields[] = " destino_latitude_real_INT = {$this->destinoLatitudeRealInt} ";
                }


                
                if (isset($this->destinoLongitudeRealInt)) 
                {                                      
                    $arrUpdateFields[] = " destino_longitude_real_INT = {$this->destinoLongitudeRealInt} ";
                }


                
                if (isset($this->tempoEstimadoCarroInt)) 
                {                                      
                    $arrUpdateFields[] = " tempo_estimado_carro_INT = {$this->tempoEstimadoCarroInt} ";
                }


                
                if (isset($this->tempoEstimadoAPeInt)) 
                {                                      
                    $arrUpdateFields[] = " tempo_estimado_a_pe_INT = {$this->tempoEstimadoAPeInt} ";
                }


                
                if (isset($this->distanciaEstimadaCarroInt)) 
                {                                      
                    $arrUpdateFields[] = " distancia_estimada_carro_INT = {$this->distanciaEstimadaCarroInt} ";
                }


                
                if (isset($this->distanciaEstimadaAPeInt)) 
                {                                      
                    $arrUpdateFields[] = " distancia_estimada_a_pe_INT = {$this->distanciaEstimadaAPeInt} ";
                }


                
                if (isset($this->inicioHoraProgramadaSec)) 
                {                                      
                    $arrUpdateFields[] = " inicio_hora_programada_SEC = {$this->inicioHoraProgramadaSec} ";
                }


                
                if (isset($this->inicioHoraProgramadaOffsec)) 
                {                                      
                    $arrUpdateFields[] = " inicio_hora_programada_OFFSEC = {$this->inicioHoraProgramadaOffsec} ";
                }


                
                if (isset($this->dataExibirSec)) 
                {                                      
                    $arrUpdateFields[] = " data_exibir_SEC = {$this->dataExibirSec} ";
                }


                
                if (isset($this->dataExibirOffsec)) 
                {                                      
                    $arrUpdateFields[] = " data_exibir_OFFSEC = {$this->dataExibirOffsec} ";
                }


                
                if (isset($this->inicioSec)) 
                {                                      
                    $arrUpdateFields[] = " inicio_SEC = {$this->inicioSec} ";
                }


                
                if (isset($this->inicioOffsec)) 
                {                                      
                    $arrUpdateFields[] = " inicio_OFFSEC = {$this->inicioOffsec} ";
                }


                
                if (isset($this->fimSec)) 
                {                                      
                    $arrUpdateFields[] = " fim_SEC = {$this->fimSec} ";
                }


                
                if (isset($this->fimOffsec)) 
                {                                      
                    $arrUpdateFields[] = " fim_OFFSEC = {$this->fimOffsec} ";
                }


                
                if (isset($this->cadastroSec)) 
                {                                      
                    $arrUpdateFields[] = " cadastro_SEC = {$this->cadastroSec} ";
                }


                
                if (isset($this->cadastroOffsec)) 
                {                                      
                    $arrUpdateFields[] = " cadastro_OFFSEC = {$this->cadastroOffsec} ";
                }


                
                if (isset($this->titulo)) 
                {                                      
                    $arrUpdateFields[] = " titulo = {$this->titulo} ";
                }


                
                if (isset($this->tituloNormalizado)) 
                {                                      
                    $arrUpdateFields[] = " titulo_normalizado = {$this->tituloNormalizado} ";
                }


                
                if (isset($this->descricao)) 
                {                                      
                    $arrUpdateFields[] = " descricao = {$this->descricao} ";
                }


                
                if (isset($this->empresaAtividadeId)) 
                {                                      
                    $arrUpdateFields[] = " empresa_atividade_id_INT = {$this->empresaAtividadeId} ";
                }


                
                if (isset($this->tipoTarefaId)) 
                {                                      
                    $arrUpdateFields[] = " tipo_tarefa_id_INT = {$this->tipoTarefaId} ";
                }


                
                if (isset($this->empresaVendaId)) 
                {                                      
                    $arrUpdateFields[] = " empresa_venda_id_INT = {$this->empresaVendaId} ";
                }


                
                if (isset($this->idPrioridadeInt)) 
                {                                      
                    $arrUpdateFields[] = " id_prioridade_INT = {$this->idPrioridadeInt} ";
                }


                
                if (isset($this->registroEstadoId)) 
                {                                      
                    $arrUpdateFields[] = " registro_estado_id_INT = {$this->registroEstadoId} ";
                }


                
                if (isset($this->registroEstadoCorporacaoId)) 
                {                                      
                    $arrUpdateFields[] = " registro_estado_corporacao_id_INT = {$this->registroEstadoCorporacaoId} ";
                }


                
                if (isset($this->percentualCompletoInt)) 
                {                                      
                    $arrUpdateFields[] = " percentual_completo_INT = {$this->percentualCompletoInt} ";
                }


                
                if (isset($this->prazoSec)) 
                {                                      
                    $arrUpdateFields[] = " prazo_SEC = {$this->prazoSec} ";
                }


                
                if (isset($this->prazoOffsec)) 
                {                                      
                    $arrUpdateFields[] = " prazo_OFFSEC = {$this->prazoOffsec} ";
                }


                
                if (isset($this->atividadeId)) 
                {                                      
                    $arrUpdateFields[] = " atividade_id_INT = {$this->atividadeId} ";
                }


                
                if (isset($this->protocoloEmpresaVendaInt)) 
                {                                      
                    $arrUpdateFields[] = " protocolo_empresa_venda_INT = {$this->protocoloEmpresaVendaInt} ";
                }


                
                if (isset($this->protocoloEmpresaAtividadeVendaInt)) 
                {                                      
                    $arrUpdateFields[] = " protocolo_empresa_atividade_venda_INT = {$this->protocoloEmpresaAtividadeVendaInt} ";
                }


            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE tarefa SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function updateByInstanceUsingParameters($id, $parameters, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($parameters->criadoPeloUsuarioId)) 
                {                                      
                    $arrUpdateFields[] = " criado_pelo_usuario_id_INT = {$this->criadoPeloUsuarioId} ";
                }
                
                if (isset($parameters->categoriaPermissaoId)) 
                {                                      
                    $arrUpdateFields[] = " categoria_permissao_id_INT = {$this->categoriaPermissaoId} ";
                }
                
                if (isset($parameters->veiculoId)) 
                {                                      
                    $arrUpdateFields[] = " veiculo_id_INT = {$this->veiculoId} ";
                }
                
                if (isset($parameters->usuarioId)) 
                {                                      
                    $arrUpdateFields[] = " usuario_id_INT = {$this->usuarioId} ";
                }
                
                if (isset($parameters->empresaEquipeId)) 
                {                                      
                    $arrUpdateFields[] = " empresa_equipe_id_INT = {$this->empresaEquipeId} ";
                }
                
                if (isset($parameters->origemPessoaId)) 
                {                                      
                    $arrUpdateFields[] = " origem_pessoa_id_INT = {$this->origemPessoaId} ";
                }
                
                if (isset($parameters->origemEmpresaId)) 
                {                                      
                    $arrUpdateFields[] = " origem_empresa_id_INT = {$this->origemEmpresaId} ";
                }
                
                if (isset($parameters->origemLogradouro)) 
                {                                      
                    $arrUpdateFields[] = " origem_logradouro = {$this->origemLogradouro} ";
                }
                
                if (isset($parameters->origemNumero)) 
                {                                      
                    $arrUpdateFields[] = " origem_numero = {$this->origemNumero} ";
                }
                
                if (isset($parameters->origemCidadeId)) 
                {                                      
                    $arrUpdateFields[] = " origem_cidade_id_INT = {$this->origemCidadeId} ";
                }
                
                if (isset($parameters->origemLatitudeInt)) 
                {                                      
                    $arrUpdateFields[] = " origem_latitude_INT = {$this->origemLatitudeInt} ";
                }
                
                if (isset($parameters->origemLongitudeInt)) 
                {                                      
                    $arrUpdateFields[] = " origem_longitude_INT = {$this->origemLongitudeInt} ";
                }
                
                if (isset($parameters->origemLatitudeRealInt)) 
                {                                      
                    $arrUpdateFields[] = " origem_latitude_real_INT = {$this->origemLatitudeRealInt} ";
                }
                
                if (isset($parameters->origemLongitudeRealInt)) 
                {                                      
                    $arrUpdateFields[] = " origem_longitude_real_INT = {$this->origemLongitudeRealInt} ";
                }
                
                if (isset($parameters->destinoPessoaId)) 
                {                                      
                    $arrUpdateFields[] = " destino_pessoa_id_INT = {$this->destinoPessoaId} ";
                }
                
                if (isset($parameters->destinoEmpresaId)) 
                {                                      
                    $arrUpdateFields[] = " destino_empresa_id_INT = {$this->destinoEmpresaId} ";
                }
                
                if (isset($parameters->destinoLogradouro)) 
                {                                      
                    $arrUpdateFields[] = " destino_logradouro = {$this->destinoLogradouro} ";
                }
                
                if (isset($parameters->destinoNumero)) 
                {                                      
                    $arrUpdateFields[] = " destino_numero = {$this->destinoNumero} ";
                }
                
                if (isset($parameters->destinoCidadeId)) 
                {                                      
                    $arrUpdateFields[] = " destino_cidade_id_INT = {$this->destinoCidadeId} ";
                }
                
                if (isset($parameters->destinoLatitudeInt)) 
                {                                      
                    $arrUpdateFields[] = " destino_latitude_INT = {$this->destinoLatitudeInt} ";
                }
                
                if (isset($parameters->destinoLongitudeInt)) 
                {                                      
                    $arrUpdateFields[] = " destino_longitude_INT = {$this->destinoLongitudeInt} ";
                }
                
                if (isset($parameters->destinoLatitudeRealInt)) 
                {                                      
                    $arrUpdateFields[] = " destino_latitude_real_INT = {$this->destinoLatitudeRealInt} ";
                }
                
                if (isset($parameters->destinoLongitudeRealInt)) 
                {                                      
                    $arrUpdateFields[] = " destino_longitude_real_INT = {$this->destinoLongitudeRealInt} ";
                }
                
                if (isset($parameters->tempoEstimadoCarroInt)) 
                {                                      
                    $arrUpdateFields[] = " tempo_estimado_carro_INT = {$this->tempoEstimadoCarroInt} ";
                }
                
                if (isset($parameters->tempoEstimadoAPeInt)) 
                {                                      
                    $arrUpdateFields[] = " tempo_estimado_a_pe_INT = {$this->tempoEstimadoAPeInt} ";
                }
                
                if (isset($parameters->distanciaEstimadaCarroInt)) 
                {                                      
                    $arrUpdateFields[] = " distancia_estimada_carro_INT = {$this->distanciaEstimadaCarroInt} ";
                }
                
                if (isset($parameters->distanciaEstimadaAPeInt)) 
                {                                      
                    $arrUpdateFields[] = " distancia_estimada_a_pe_INT = {$this->distanciaEstimadaAPeInt} ";
                }
                
                if (isset($parameters->inicioHoraProgramadaSec)) 
                {                                      
                    $arrUpdateFields[] = " inicio_hora_programada_SEC = {$this->inicioHoraProgramadaSec} ";
                }
                
                if (isset($parameters->inicioHoraProgramadaOffsec)) 
                {                                      
                    $arrUpdateFields[] = " inicio_hora_programada_OFFSEC = {$this->inicioHoraProgramadaOffsec} ";
                }
                
                if (isset($parameters->dataExibirSec)) 
                {                                      
                    $arrUpdateFields[] = " data_exibir_SEC = {$this->dataExibirSec} ";
                }
                
                if (isset($parameters->dataExibirOffsec)) 
                {                                      
                    $arrUpdateFields[] = " data_exibir_OFFSEC = {$this->dataExibirOffsec} ";
                }
                
                if (isset($parameters->inicioSec)) 
                {                                      
                    $arrUpdateFields[] = " inicio_SEC = {$this->inicioSec} ";
                }
                
                if (isset($parameters->inicioOffsec)) 
                {                                      
                    $arrUpdateFields[] = " inicio_OFFSEC = {$this->inicioOffsec} ";
                }
                
                if (isset($parameters->fimSec)) 
                {                                      
                    $arrUpdateFields[] = " fim_SEC = {$this->fimSec} ";
                }
                
                if (isset($parameters->fimOffsec)) 
                {                                      
                    $arrUpdateFields[] = " fim_OFFSEC = {$this->fimOffsec} ";
                }
                
                if (isset($parameters->cadastroSec)) 
                {                                      
                    $arrUpdateFields[] = " cadastro_SEC = {$this->cadastroSec} ";
                }
                
                if (isset($parameters->cadastroOffsec)) 
                {                                      
                    $arrUpdateFields[] = " cadastro_OFFSEC = {$this->cadastroOffsec} ";
                }
                
                if (isset($parameters->titulo)) 
                {                                      
                    $arrUpdateFields[] = " titulo = {$this->titulo} ";
                }
                
                if (isset($parameters->tituloNormalizado)) 
                {                                      
                    $arrUpdateFields[] = " titulo_normalizado = {$this->tituloNormalizado} ";
                }
                
                if (isset($parameters->descricao)) 
                {                                      
                    $arrUpdateFields[] = " descricao = {$this->descricao} ";
                }
                
                if (isset($parameters->empresaAtividadeId)) 
                {                                      
                    $arrUpdateFields[] = " empresa_atividade_id_INT = {$this->empresaAtividadeId} ";
                }
                
                if (isset($parameters->tipoTarefaId)) 
                {                                      
                    $arrUpdateFields[] = " tipo_tarefa_id_INT = {$this->tipoTarefaId} ";
                }
                
                if (isset($parameters->empresaVendaId)) 
                {                                      
                    $arrUpdateFields[] = " empresa_venda_id_INT = {$this->empresaVendaId} ";
                }
                
                if (isset($parameters->idPrioridadeInt)) 
                {                                      
                    $arrUpdateFields[] = " id_prioridade_INT = {$this->idPrioridadeInt} ";
                }
                
                if (isset($parameters->registroEstadoId)) 
                {                                      
                    $arrUpdateFields[] = " registro_estado_id_INT = {$this->registroEstadoId} ";
                }
                
                if (isset($parameters->registroEstadoCorporacaoId)) 
                {                                      
                    $arrUpdateFields[] = " registro_estado_corporacao_id_INT = {$this->registroEstadoCorporacaoId} ";
                }
                
                if (isset($parameters->percentualCompletoInt)) 
                {                                      
                    $arrUpdateFields[] = " percentual_completo_INT = {$this->percentualCompletoInt} ";
                }
                
                if (isset($parameters->prazoSec)) 
                {                                      
                    $arrUpdateFields[] = " prazo_SEC = {$this->prazoSec} ";
                }
                
                if (isset($parameters->prazoOffsec)) 
                {                                      
                    $arrUpdateFields[] = " prazo_OFFSEC = {$this->prazoOffsec} ";
                }
                
                if (isset($parameters->atividadeId)) 
                {                                      
                    $arrUpdateFields[] = " atividade_id_INT = {$this->atividadeId} ";
                }
                
                if (isset($parameters->protocoloEmpresaVendaInt)) 
                {                                      
                    $arrUpdateFields[] = " protocolo_empresa_venda_INT = {$this->protocoloEmpresaVendaInt} ";
                }
                
                if (isset($parameters->protocoloEmpresaAtividadeVendaInt)) 
                {                                      
                    $arrUpdateFields[] = " protocolo_empresa_atividade_venda_INT = {$this->protocoloEmpresaAtividadeVendaInt} ";
                }
            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE tarefa SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }

}

?>
