import { Marcador, ElementoVisualizacao } from "lib/uiComponente";
import { Empresa as ModelEmpresa } from 'aplicacao/modulo/mapa/model'
import { ContextoMapa } from "lib/contexto";
import { ModalFactory } from "aplicacao/modulo/mapa/uiFactory";

export class Empresa extends Marcador
{
    public modelEmpresa :ModelEmpresa;
    public constructor(contexto :ContextoMapa, modelEmpresa: ModelEmpresa)
    {
        super(contexto, modelEmpresa.latitude, modelEmpresa.longitude, 'empresa');
        this.modelEmpresa = modelEmpresa;
        this.setDefaultLabelObject(modelEmpresa.nomeEmpresa);
    }

    public setDefaultIconeObject()
    {
        if(this.modelEmpresa.wifiNetworks.length > 0)
        {
            super.setDefaultIconeObject(this.contexto.getDefaultMarkerFolder() + "map_wifi.png");
        }
        else
        {
            super.setDefaultIconeObject(this.contexto.getDefaultMarkerFolder() + "map_company.png");
        }

    }

    public addDefaultClickListener()
    {
        super.addClickListener(() => 
        {
            var janelaModal = ModalFactory.getModalDetalhesEmpresa(this.contexto, this.modelEmpresa);
            janelaModal.handleShow();
        });
    }

}