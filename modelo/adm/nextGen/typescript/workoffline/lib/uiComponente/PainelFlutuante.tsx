import * as React from "react";
import { Modal } from 'react-bootstrap';

import { ReactComponent } from "lib/uiComponente";
import { ReactProps } from 'lib/geral';

export namespace PainelFlutuante
{
    export interface PainelFlutuanteProps extends ReactProps
    {
        setDefaultBody: boolean;
        setDefaultHeader: boolean;
    }
    
}

export class PainelFlutuante<P extends PainelFlutuante.PainelFlutuanteProps> extends ReactComponent<P, any>
{
    protected headerContent :JSX.Element | JSX.Element[];
    protected bodyContent :JSX.Element | JSX.Element[];
    protected footerContent :JSX.Element | JSX.Element[];

    protected constructor(props: P, reactContext: any)
    {
        super(props || {} as P, reactContext);

        this.handleShow = this.handleShow.bind(this);
        this.handleClose = this.handleClose.bind(this);            
        this.state = { show : false };                 
    }

    public setDefaultHeader(): void
    {

    }

    public setDefaultBody() :void
    {

    }

    handleClose() :void 
    {
        this.setState({ show: false });
    }
    
    public handleShow() :void
    {
        this.setState({ show: true });
    }
    
    public render()
    {
        if(this.props.setDefaultBody)
        {
            this.setDefaultBody();
        }

        if(this.props.setDefaultHeader)
        {
            this.setDefaultHeader();
        }

        return (
            <Modal
                show={this.state.show} 
                onHide={this.handleClose} 
            >
                <Modal.Header closeButton>
                    {this.headerContent}
                </Modal.Header>
                <Modal.Body>
                    {this.bodyContent}
                </Modal.Body>
                <Modal.Footer>
                    {this.footerContent}
                </Modal.Footer>
            </Modal>
        );
    }

}
