<?php

include_once '../recursos/php/constants.php';
include_once '../recursos/php/database_config.php';
include_once '../adm/imports/header.php';
include_once '../recursos/php/funcoes.php';
include_once '../adm/imports/instancias.php';
include_once '../adm/imports/lingua.php';

session_name(IDENTIFICADOR_SESSAO_CLIENTE);
session_start();

$objSeguranca = new Seguranca();
$objSeguranca->matarSessao();

?>

<html>
<head>
    <title><?= TITULO_PAGINAS_CLIENTE ?></title>

    <?= Helper::getTagDoFavicon(); ?>

    <?= Helper::carregarArquivoCss(1, "adm/css/", "padrao") ?>
    <?= Helper::carregarArquivoCss(1, "adm/css/", "login") ?>

    <?= Javascript::importarTodasAsBibliotecas(); ?>

</head>
<body>

<?

Javascript::imprimirCorpoDoTooltip();

?>

<div class="externalcontainer">

    <div id="content_wrapper">

        <div class="with_arm">

            <form action="actions.php?class=Seguranca_Pagamento&action=login" class="login" method="post"
                  name="formulario_login" id="formulario_login" onsubmit="return true;">

                <?

                if (is_numeric(Helper::GET("promocao_referencia"))) {

                    echo Helper::getCampoHidden("promocao_referencia", Helper::GET("promocao_referencia"));

                }

                echo Helper::getCampoHidden("next_action", "pagina_inicial");

                ?>

                <h1>DADOS <span>DE ACESSO</span> <strong><?= "Área do Cliente - Estética Solution" ?></strong></h1>
                <h5>Área restrita, só é permitido o acesso a clientes registrados.<br/>
                    Entre com seus dados de acesso:</h5>
                <span style="color:#FF0000;"></span>
                <label>Email: <input name="txtLogin" id="txtLogin" class="usepws" value="<?= $emailAutocompletar ?>"
                                     onfocus="this.value='';" type="text"></label>

                <label>Senha: <input name="txtSenha" id="txtSenha" class="usepws" value="" onfocus="this.value='';"
                                     type="password"></label>

                <label style="text-align: right; width: 100%;">
                    <input name="Submit" value="Submit" src="../adm/imgs/padrao/login_btn.gif" type="image">
                </label>
                <br class="specer">

                <p> Se você esqueceu sua senha,

                    <?

                    $objLink = new Link();
                    $objLink->alturaGreyBox = 500;
                    $objLink->larguraGreyBox = 500;
                    $objLink->tituloGreyBox = "Lembrar Senha";
                    $objLink->url = "popup.php?tipo=paginas&page=lembrar_senha&navegacao=false";
                    $objLink->demaisAtributos = array("onmouseover" => "tip('Clique aqui para receber a senha em seu email.', this);", "onmouseout" => "notip();");
                    //$objLink->cssClass = "link_padrao";
                    $objLink->label = "clique aqui";

                    ?>

                    <?= $objLink->montarLink(); ?>

                    para recuperá-la.</p>

                <p> Se você deseja voltar para o site da Estética Solution, <a href="../site/">clique aqui</a>.</p>

                <p> Se você deseja ainda não é cadastrado e deseja se cadastrar, <a
                        href="index.php?tipo=paginas&page=cadastro">clique aqui</a>.</p>

            </form>

        </div>

    </div>

</div>


<?

Helper::includePHP(1, "adm/imports/mensagens.php");

?>

</body>

</html>
