import * as MomentJS from 'moment';

export class MomentHelper
{
    public static readonly FORMATO_DATA_FULLCALENDAR = 'YYYY-MM-DD';
    public static readonly FORMATO_DATAHORA_FULLCALENDAR = 'YYYY-MM-DDTHH:mm:ss';

    public static readonly FORMATO_DATA_MYSQL = 'YYYY-MM-DD';
    public static readonly FORMATO_DATAHORA_MYSQL = 'YYYY-MM-DD HH:mm:ss';

    public static readonly FORMATO_DATA_EXIBICAO = 'DD/MM/YYYY';
    public static readonly FORMATO_DATAHORA_EXIBICAO = 'DD/MM/YYYY HH:mm:ss';

    public static getMomentInstance(dateObject :Date)
    {
        return MomentJS(dateObject);
    }

    public static formatMomentToDatetimeExibicao(momentObject :MomentJS.Moment) :string
    {
        return momentObject.format(MomentHelper.FORMATO_DATAHORA_EXIBICAO);
    }

    public static formatMomentToDateExibicao(momentObject :MomentJS.Moment) :string
    {
        return momentObject.format(MomentHelper.FORMATO_DATA_EXIBICAO);
    }

    public static formatMoment(momentObject :MomentJS.Moment, formato :string) :string
    {
        return momentObject.format(formato);
    }

    public static formatDate(dateObject :Date, formato :string) :string
    {
        return MomentHelper.getMomentInstance(dateObject).format(formato);
    }

    public static getUtcTimestampInSeconds(momentObject :MomentJS.Moment)
    {
        return momentObject.utc().valueOf()/1000;
    }

    public static getTimezoneOffsetInSeconds(momentObject: MomentJS.Moment)
    {
        return momentObject.utcOffset()*60;
    }

}