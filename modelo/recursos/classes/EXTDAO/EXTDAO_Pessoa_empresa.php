<?php

    /*

    Arquivo gerado atravï¿½s de gerador de cï¿½digo em 24/05/2017 as 09:58:41.
    Para que o arquivo nï¿½o seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

    Tabela correspondente: pessoa_empresa
    Sobrescrita de dados: ---SOBRESCRITA_NAO_AUTORIZADA---

    */
?>

<?php

    class EXTDAO_Pessoa_empresa extends DAO_Pessoa_empresa
    {
        public function __construct($configDAO = null)
        {
            parent::__construct($configDAO);
            $this->nomeClasse = "EXTDAO_Pessoa_empresa";
        }

        public function factory()
        {
            return new EXTDAO_Pessoa_empresa();
        }

        public static function adicionaProfissaoIndefinidaAPessoa(
            Database $db, $idCorporacao, $idUsuarioOperacao, $idPessoa){

            $idProfissao = EXTDAO_Profissao::getIdProfissaoIndefinida($db, $idCorporacao, $idUsuarioOperacao);
            if(!strlen($idProfissao))
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, "Profissao default inexistente no DB");
            $idEmpresa = EXTDAO_Empresa::getIdEmpresaIndefinida($db, $idCorporacao, $idUsuarioOperacao);
            if(!strlen($idEmpresa))
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, "Empresa default inexistente no DB");
            $db->query("SELECT 1 
            FROM pessoa_empresa 
            WHERE pessoa_id_INT = $idPessoa 
                AND profissao_id_INT = $idProfissao
                AND empresa_id_INT = $idEmpresa 
                AND corporacao_id_INT = $idCorporacao");

            $existe = $db->getPrimeiraTuplaDoResultSet(0);
            if($existe == null || !$existe){
                $objPessoaEmpresa = new EXTDAO_Pessoa_empresa($db);
                $objPessoaEmpresa->setProfissao_id_INT($idProfissao);
                $objPessoaEmpresa->setEmpresa_id_INT($idEmpresa);
                $objPessoaEmpresa->setPessoa_id_INT($idPessoa);
                $objPessoaEmpresa->formatarParaSQL();
                return $objPessoaEmpresa->insert(true, $idCorporacao, $idUsuarioOperacao);
            } else return null;
        }
        
    }

?>
