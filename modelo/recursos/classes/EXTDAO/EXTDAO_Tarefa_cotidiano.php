<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Tarefa_cotidiano
    * NOME DA CLASSE DAO: 
    * DATA DE GERAÃ‡ÃƒO:    
    * ARQUIVO:            EXTDAO_Tarefa_cotidiano.php
    * TABELA MYSQL:       tarefa_cotidiano
    * BANCO DE DADOS:     
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÃ‡ÃƒO DA CLASSE
    // **********************

    class EXTDAO_Tarefa_cotidiano extends DAO_Tarefa_cotidiano
    {

        public function __construct($configDAO){

            parent::__construct($configDAO);

            	$this->nomeClasse = "EXTDAO_Tarefa_cotidiano";


            $this->setLabels();






        }

        public function setLabels(){

			$this->label_id = "";
			$this->label_criado_pelo_usuario_id_INT = "";
			$this->label_categoria_permissao_id_INT = "";
			$this->label_veiculo_id_INT = "";
			$this->label_usuario_id_INT = "";
			$this->label_empresa_equipe_id_INT = "";
			$this->label_origem_pessoa_id_INT = "";
			$this->label_origem_empresa_id_INT = "";
			$this->label_origem_logradouro = "";
			$this->label_origem_numero = "";
			$this->label_origem_cidade_id_INT = "";
			$this->label_origem_latitude_INT = "";
			$this->label_origem_longitude_INT = "";
			$this->label_origem_latitude_real_INT = "";
			$this->label_origem_longitude_real_INT = "";
			$this->label_destino_pessoa_id_INT = "";
			$this->label_destino_empresa_id_INT = "";
			$this->label_destino_logradouro = "";
			$this->label_destino_numero = "";
			$this->label_destino_cidade_id_INT = "";
			$this->label_destino_latitude_INT = "";
			$this->label_destino_longitude_INT = "";
			$this->label_destino_latitude_real_INT = "";
			$this->label_destino_longitude_real_INT = "";
			$this->label_tempo_estimado_carro_INT = "";
			$this->label_tempo_estimado_a_pe_INT = "";
			$this->label_distancia_estimada_carro_INT = "";
			$this->label_distancia_estimada_a_pe_INT = "";
			$this->label_is_realizada_a_pe_BOOLEAN = "";
			$this->label_inicio_hora_programada_SEC = "";
			$this->label_inicio_hora_programada_OFFSEC = "";
			$this->label_inicio_SEC = "";
			$this->label_inicio_OFFSEC = "";
			$this->label_fim_SEC = "";
			$this->label_fim_OFFSEC = "";
			$this->label_cadastro_SEC = "";
			$this->label_cadastro_OFFSEC = "";
			$this->label_titulo = "";
			$this->label_titulo_normalizado = "";
			$this->label_descricao = "";
			$this->label_corporacao_id_INT = "";
			$this->label_servico_id_INT = "";
			$this->label_id_estado_INT = "";
			$this->label_venda_id_INT = "";
			$this->label_id_prioridade_INT = "";
			$this->label_parametro_SEC = "";
			$this->label_parametro_OFFSEC = "";
			$this->label_parametro_json = "";
			$this->label_data_limite_cotidiano_SEC = "";
			$this->label_data_limite_cotidiano_OFFSEC = "";


        }





        public function factory(){

            return new EXTDAO_Tarefa_cotidiano();

        }

	}

    
