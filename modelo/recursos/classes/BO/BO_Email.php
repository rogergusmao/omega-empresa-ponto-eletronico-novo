<?php
/**
 * Created by PhpStorm.
 * User: W10
 * Date: 04/09/2017
 * Time: 07:47
 */




class BO_Email
{
    public function __construct()
    {
    }

    public function factory(){
        return new BO_Email();
    }
    public static function enviaEmailRelembrarSenha(
        $emailCadastrado,
        $assunto,
        $conteudoAssunto)
    {
        try{
            $db = new Database();
            $db->query("SELECT id, nome, email, senha
                            FROM usuario
                            WHERE email='{$emailCadastrado}'");

            if ($db->rows() == 1) {

                $idUsuario = $db->getPrimeiraTuplaDoResultSet(0);
                $nome = ucwords($db->getPrimeiraTuplaDoResultSet(1));
                $emailDestino = $db->getPrimeiraTuplaDoResultSet(2);
                $senha = $db->getPrimeiraTuplaDoResultSet(3);

                $objCrypt = new Crypt();
                $senhaDescriptografada = $objCrypt->decrypt($senha);

                $mensagem = " Sr. " . ucwords($nome) . "
                            $conteudoAssunto
                            --- Lembrete de senha de acesso ao " . TITULO_PAGINAS . " ---\n
                                                    Endere�o para Acesso: " . ENDERECO_DE_ACESSO . "
                                                    Usu�rio: {$emailDestino}
                                                    Senha  : {$senhaDescriptografada}
                                                    ";


                $mensagemUsuarioCorporacao = DatabaseSincronizador::getMensagemUsuarioCorporacao($idUsuario);

                $objEmail = new Email_Sender(EMAIL_PADRAO_REMETENTE_MENSAGENS, $emailDestino);
                $objEmail->setAssunto($assunto);
                $objEmail->setConteudo($mensagem . $mensagemUsuarioCorporacao, false);
                if ($objEmail->enviarEmail())
                    return true;
                else
                    return false;
            } else
                return false;
        }catch(Exception $ex){
            HelperLog::logErro($ex);
            return false;
        }

    }

    public static function enviaEmailSenhaAlterada(
        $emailCadastrado,
        $assunto,
        $conteudoAssunto)
    {
        try{
            $db = new Database();
            $db->query("SELECT id, nome, email, senha
                            FROM usuario
                            WHERE email='{$emailCadastrado}'");

            if ($db->rows() == 1) {

                $idUsuario = $db->getPrimeiraTuplaDoResultSet(0);
                $nome = ucwords($db->getPrimeiraTuplaDoResultSet(1));
                $emailDestino = $db->getPrimeiraTuplaDoResultSet(2);
                $senha = $db->getPrimeiraTuplaDoResultSet(3);

                $objCrypt = new Crypt();
                $senhaDescriptografada = $objCrypt->decrypt($senha);

                $mensagem = " Sr. " . ucwords($nome) . "
                            $conteudoAssunto
                            --- Lembrete de senha de acesso ao " . TITULO_PAGINAS . " ---\n
                                                    Endere�o para Acesso: " . ENDERECO_DE_ACESSO . "
                                                    Usu�rio: {$emailDestino}
                                                    Senha  : {$senhaDescriptografada}
                                                    ";


                $mensagemUsuarioCorporacao = DatabaseSincronizador::getMensagemUsuarioCorporacao($idUsuario);

                $objEmail = new Email_Sender(EMAIL_PADRAO_REMETENTE_MENSAGENS, $emailDestino);
                $objEmail->setAssunto($assunto);
                $objEmail->setConteudo($mensagem . $mensagemUsuarioCorporacao, false);
                if ($objEmail->enviarEmail())
                    return true;
                else
                    return false;
            } else
                return false;
        }catch(Exception $ex){
            HelperLog::logErro($ex);
            return false;
        }

    }

}