import { Mapa } from "lib/uiComponente";
import { ContextoTempo } from "lib/contexto";
import { Tempo } from "lib/geral";

export class MapaComTimeline extends ContextoTempo
{
    public constructor(domRootSelector: string, mapContainerSelector: string)
    {
        //chama o construtor do pai
        super(domRootSelector, mapContainerSelector, new Tempo());

        //define o root selector
        this.domRootSelector = domRootSelector;

        //inicializa mapa
        this.initializeMap();
    }

    public initializeMap(): void
    {
        this.mapObject = new Mapa(this);
        this.mapObject.addResizeListener();
    }

    getDefaultMarkerFolder(): string
    {
        return undefined;
    }

}