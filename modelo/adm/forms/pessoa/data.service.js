/*

 Arquivo gerado atrav�s de gerador de c�digo em 26/09/2017 as 22:39:24.
 Para que o arquivo n�o seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

 Tabela correspondente: pessoa
 Sobrescrita de dados: ---SOBRESCRITA_NAO_AUTORIZADA---

 */
returnObject.getPessoa = function (filterParameters)
{
    var parameters = {
        filterParameters: filterParameters,
        loadComboBoxesData: true
    };
    return $http({
        url: baseURLAdm + "/webservice.php?class=EXTDAO_Pessoa&action=__getRecord",
        method: "POST",
        data: parameters,
        headers: {
            'Content-Type': 'application/json'
        }
    });
};
returnObject.addPessoa = function (formData)
{
    return $http({
        url: baseURLAdm + "/webservice.php?class=EXTDAO_Pessoa&jsonOutput=true&action=add",
        method: "POST",
        data: formData,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    });
};
returnObject.editPessoa = function (formData)
{
    return $http({
        url: baseURLAdm + "/webservice.php?class=EXTDAO_Pessoa&jsonOutput=true&action=edit",
        method: "POST",
        data: formData,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    });
};
returnObject.getAllComboBoxesDataForPessoaForm = function ()
{
    return $http({
        url: baseURLAdm + "/webservice.php?class=EXTDAO_Pessoa&jsonOutput=true&action=__getFormComboBoxesData",
        method: "POST",
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    });
};
returnObject.getListOfTipoDocumentoForPessoaForm = function ()
{
    var params = {
        filterParameters: null,
        listMappingType: Constants.ListMappingType.COMBOBOX,
        hideLoading: true
    };
    return $http.post(baseURLAdm + "/webservice.php?class=EXTDAO_Tipo_documento&action=__getList", params);
};
returnObject.getListOfSexoForPessoaForm = function ()
{
    var params = {
        filterParameters: null,
        listMappingType: Constants.ListMappingType.COMBOBOX,
        hideLoading: true
    };
    return $http.post(baseURLAdm + "/webservice.php?class=EXTDAO_Sexo&action=__getList", params);
};
returnObject.getListOfOperadoraForPessoaForm = function ()
{
    var params = {
        filterParameters: null,
        listMappingType: Constants.ListMappingType.COMBOBOX,
        hideLoading: true
    };
    return $http.post(baseURLAdm + "/webservice.php?class=EXTDAO_Operadora&action=__getList", params);
};
returnObject.getListOfCidadeForPessoaForm = function ()
{
    var params = {
        filterParameters: null,
        listMappingType: Constants.ListMappingType.COMBOBOX,
        hideLoading: true
    };
    return $http.post(baseURLAdm + "/webservice.php?class=EXTDAO_Cidade&action=__getList", params);
};
returnObject.getListOfBairroForPessoaForm = function (cidadeId)
{
    var params = {
        filterParameters: {cidadeId: cidadeId},
        listMappingType: Constants.ListMappingType.COMBOBOX,
        hideLoading: true
    };
    return $http.post(baseURLAdm + "/webservice.php?class=EXTDAO_Bairro&action=__getList", params);
};
                

