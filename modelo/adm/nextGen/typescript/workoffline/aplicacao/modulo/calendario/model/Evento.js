"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var Moment = require("moment");
var model_1 = require("lib/model");
var geral_1 = require("lib/geral");
var Evento = (function (_super) {
    __extends(Evento, _super);
    function Evento() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Evento.prototype.getEventoFullCalendar = function () {
        if (this.pontoFinal != null) {
            return {
                id: this.pontoInicial.idPonto + "," + this.pontoFinal.idPonto,
                idPessoa: this.idPessoa,
                idPontoInicial: this.pontoInicial.idPonto,
                title: this.titulo,
                start: geral_1.MomentHelper.formatMoment(this.pontoInicial.dataPonto, geral_1.MomentHelper.FORMATO_DATAHORA_FULLCALENDAR),
                end: geral_1.MomentHelper.formatMoment(this.pontoFinal.dataPonto, geral_1.MomentHelper.FORMATO_DATAHORA_FULLCALENDAR),
                backgroundColor: this.corBackgroundEvento.getHexValue(),
                textColor: this.corForegroudEvento.getHexValue(),
                borderColor: this.corBackgroundEvento.getHexValue(),
                className: 'event-ok',
                eventOverlap: false
            };
        }
        else {
            var dataPonto = Moment(this.pontoInicial.dataPonto);
            return {
                id: "" + this.pontoInicial.idPonto,
                idPessoa: this.idPessoa,
                idPontoInicial: this.pontoInicial.idPonto,
                title: this.titulo,
                start: geral_1.MomentHelper.formatMoment(this.pontoInicial.dataPonto, geral_1.MomentHelper.FORMATO_DATAHORA_FULLCALENDAR),
                end: dataPonto.add(1, "hour").format(geral_1.MomentHelper.FORMATO_DATAHORA_FULLCALENDAR),
                backgroundColor: this.corBackgroundEvento.getHexValue(),
                textColor: this.corForegroudEvento.getHexValue(),
                borderColor: this.corBackgroundEvento.getHexValue(),
                className: 'event-error',
                eventOverlap: false
            };
        }
    };
    return Evento;
}(model_1.Model));
exports.Evento = Evento;
