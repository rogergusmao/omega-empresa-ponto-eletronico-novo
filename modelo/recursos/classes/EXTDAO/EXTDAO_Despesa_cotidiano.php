<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Despesa_cotidiano
    * NOME DA CLASSE DAO: 
    * DATA DE GERAÃ‡ÃƒO:    
    * ARQUIVO:            EXTDAO_Despesa_cotidiano.php
    * TABELA MYSQL:       despesa_cotidiano
    * BANCO DE DADOS:     
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÃ‡ÃƒO DA CLASSE
    // **********************

    class EXTDAO_Despesa_cotidiano extends DAO_Despesa_cotidiano
    {

        public function __construct($configDAO){

            parent::__construct($configDAO);

            	$this->nomeClasse = "EXTDAO_Despesa_cotidiano";


            $this->setLabels();






        }

        public function setLabels(){

			$this->label_id = "";
			$this->label_despesa_da_empresa_id_INT = "";
			$this->label_despesa_do_usuario_id_INT = "";
			$this->label_cadastro_usuario_id_INT = "";
			$this->label_id_tipo_despesa_cotidiano_INT = "";
			$this->label_parametro_INT = "";
			$this->label_parametro_OFFSEC = "";
			$this->label_parametro_SEC = "";
			$this->label_parametro_json = "";
			$this->label_valor_FLOAT = "";
			$this->label_data_limite_cotidiano_SEC = "";
			$this->label_data_limite_cotidiano_OFFSEC = "";
			$this->label_corporacao_id_INT = "";


        }





        public function factory(){

            return new EXTDAO_Despesa_cotidiano();

        }

	}

    
