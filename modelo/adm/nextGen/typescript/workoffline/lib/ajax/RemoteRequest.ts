import { RemoteResponse } from "lib/ajax";

export class RemoteRequest
{
    public url: string;
    public data: any;
    public async: boolean;
    public background: boolean;
    public options: any;
    public requestMethod: RemoteRequest.RequestMethod;
    public requestNumber: number;

    public constructor(url: string);
    public constructor(url: string, data: any);
    public constructor(url: string, data: any, requestMethod: RemoteRequest.RequestMethod);
    public constructor(url: string, data: any, requestMethod: RemoteRequest.RequestMethod, async: boolean);
    public constructor(url: string, data?: any, requestMethod: RemoteRequest.RequestMethod = RemoteRequest.RequestMethod.POST, async?: boolean, requestOptions?: JQueryAjaxSettings)
    {
        this.url = url;
        this.data = data || {};
        this.async = async || true;
        this.options = requestOptions;
    } 

    public addOptions(options: any)
    {
        for (var attrname in options)
        {
            this.options[attrname] = options[attrname];
        }
    }

    public getRequestTypeString(): string
    {
        switch (this.requestMethod)
        {
            case RemoteRequest.RequestMethod.GET:
                return "GET";

            case RemoteRequest.RequestMethod.POST:
                return "POST";

            case RemoteRequest.RequestMethod.PUT:
                return "PUT";

            case RemoteRequest.RequestMethod.DELETE:
                return "DELETE";

            default:
                return "POST";
        }
    }

    public async getItem<T>(): Promise<RemoteResponse<T>>
    {
        return await this.execute<RemoteResponse<T>>();
    }

    public async getList<T>(): Promise<RemoteResponse<Array<T>>>
    {
        return await this.execute<RemoteResponse<Array<T>>>();
    }

    protected async execute<T>(): Promise<T>
    {
        this.data.jsonOutput = 'true';
        var result = await Promise.resolve<T>($.ajax( {
            url: this.url,
            data: this.data,
            dataType: "json",
            method: this.getRequestTypeString(),
            headers: {
                'Accept': 'application/json; charset=iso-8859-1',
                'Content-Type': 'application/x-www-form-urlencoded',
                'X-Application-Type': 'AngularJS'
            }
        }));

        return result;

    }

}

export namespace RemoteRequest
{
    export enum RequestMethod { GET, POST, PUT, DELETE }   
}