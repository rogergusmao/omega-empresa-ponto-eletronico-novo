"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var Mapper_1 = require("../../../../mapper/Mapper");
var Tag_1 = require("../Tag/Tag");
var Cache_Backend_LocalStorage_TagTree = (function (_super) {
    __extends(Cache_Backend_LocalStorage_TagTree, _super);
    function Cache_Backend_LocalStorage_TagTree(tags) {
        if (tags === void 0) { tags = {}; }
        var _this = _super.call(this) || this;
        _this._tags = tags;
        return _this;
    }
    Cache_Backend_LocalStorage_TagTree.prototype.add = function (tag) {
        this._tags[tag.tag] = tag;
    };
    Object.defineProperty(Cache_Backend_LocalStorage_TagTree.prototype, "tags", {
        get: function () {
            return this._tags;
        },
        enumerable: true,
        configurable: true
    });
    Cache_Backend_LocalStorage_TagTree.prototype.get = function (tagKey) {
        // Get the tag
        var tag = this._tags[tagKey];
        if (tag == undefined) {
            return null;
        }
        return Tag_1.Cache_Backend_LocalStorage_Tag.fromObject(tag);
    };
    Cache_Backend_LocalStorage_TagTree.prototype.addKeyToTag = function (tagKey, key) {
        // Get the tag object
        var tag = this.get(tagKey);
        // Check if the tag exists in the tag three
        if (tag == undefined) {
            tag = new Tag_1.Cache_Backend_LocalStorage_Tag(tagKey);
            this.add(tag);
        }
        tag.add(key);
    };
    Cache_Backend_LocalStorage_TagTree.prototype.removeKeyFromTag = function (key, tagKey) {
        // Get the tag
        var tag = this.get(tagKey);
        // Check if we found the tag index
        if (tag == null) {
            return false;
        }
        // Remove the key from the tag
        return tag.remove(key);
    };
    Cache_Backend_LocalStorage_TagTree.prototype.clear = function (tagKey) {
        delete this._tags[tagKey];
    };
    Cache_Backend_LocalStorage_TagTree.prototype.isValidKeyAndTagKeyCombination = function (key, tagKey) {
        // Get the tag
        var tag = this.get(tagKey);
        // Check if the tag exists in the tag three
        if (tag == null) {
            return false;
        }
        // Check if the key exists within the tag
        if (!tag.keyExists(key)) {
            return false;
        }
        return true;
    };
    Cache_Backend_LocalStorage_TagTree.prototype.tagExists = function (tagKey) {
        // Get the tag
        var tag = this.get(tagKey);
        // Check if the tag exists in the tag three
        if (tag == null) {
            return false;
        }
        return true;
    };
    Cache_Backend_LocalStorage_TagTree.prototype.tagEmpty = function (tagKey) {
        // Get the tag
        var tag = this.get(tagKey);
        // Check if the tag exists in the tag three
        if (tag == null) {
            return true;
        }
        return tag.tagEmpty();
    };
    Cache_Backend_LocalStorage_TagTree.fromObject = function (object) {
        _super.fromObject.call(this, object);
        return new Cache_Backend_LocalStorage_TagTree(object._tags);
    };
    return Cache_Backend_LocalStorage_TagTree;
}(Mapper_1.Mapper));
exports.Cache_Backend_LocalStorage_TagTree = Cache_Backend_LocalStorage_TagTree;
