declare module HtmlUtil
{
    export function mostrarLoading() : void;
    export function esconderLoading() : void;
}

declare module DateUtil
{
    export function differenceInDays(date1 :Date, date2 :Date) :number;
    export function getMysqlDate(dateObjectOrString :any) :string;
    export function getFormattedDate(dateObjectOrString :any) :string;
    export function isDateObject(dateObject :any) :boolean;
    export function getDateObject(dateStringOrTimestamp :any) :Date;
    export function getMysqlDateTime(dateObjectOrString :any) :string;
    export function getFormattedDateTime(dateObjectOrString :any) :string;
    export function getUserTimezoneOffsetInSeconds() :number;
    export function getUtcDate() :Date;
    export function getUtcTimestampInSeconds(dateObject :Date) :number;
}

