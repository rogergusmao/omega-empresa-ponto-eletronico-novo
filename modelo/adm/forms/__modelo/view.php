<!--Definição da View Angular JS -->
<?php
$objForm = new FormEnvironment("mainForm", "formClienteController", "vm");
$objForm->setLayoutOneColumn();
?>

<div class='row' id='content-wrapper' ng-controller="<?= $objForm->getAngularControllerAttributeValue() ?>">

    <script type="text/ng-template" id="after-insert-template.html">
        <div class="modal-header">
            <h4 class="modal-title">
                <span class="glyphicon"></span>
                Cliente adicionado com sucesso!
            </h4>
        </div>
        <div class="modal-body">
            <span class="help-block">O que deseja fazer agora?</span>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" ng-click="vm.loadClientesList()">Listar Clientes</button>
            <button type="button" class="btn btn-default" ng-click="closeMessageDialog()">Permanecer na tela</button>
            <button type="button" class="btn btn-default" ng-click="vm.addNewCliente()">Adicionar novo Cliente</button>
        </div>
    </script>

    <script type="text/ng-template" id="after-update-template.html">
        <div class="modal-header">
            <h4 class="modal-title">
                <span class="glyphicon"></span>
                Cliente alterado com sucesso!
            </h4>
        </div>
        <div class="modal-body">
            <span class="help-block">O que deseja fazer agora?</span>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" ng-click="vm.loadClientesList()">Listar Clientes</button>
            <button type="button" class="btn btn-default" ng-click="closeMessageDialog()">Permanecer na tela</button>
            <button type="button" class="btn btn-default" ng-click="vm.addNewCliente()">Adicionar novo Cliente</button>
        </div>
    </script>


    <div class='col-xs-12'>

        <div class='row'>
            <div class='col-sm-12'>
                <div class='page-header'>
                    <h1 class='pull-left'>
                        <i class='icon-edit'></i>
                        <span>
                                <?= I18N::getExpression("Cadastro de Cliente"); ?>
                            </span>
                    </h1>

                    <div class='pull-right close-button-container'>
                        <?= $objForm->renderCloseModalButtonIfExists(); ?>
                    </div>
                </div>
            </div>
        </div>

        <?= $objForm->getFormDebugPanel(); ?>

        <div class='row'>
            <div class='col-sm-12'>
                <div class='box'>
                    <div class='box-header blue-background'>
                        <div class='title'>
                            <div class='icon-edit'></div>
                            Dados do Cliente
                        </div>
                        <div class='actions'>
                            <a class="btn box-collapse btn-xs btn-link" href="#"><i></i></a>
                        </div>
                    </div>
                    <div class='box-content'>

                        <form name="<?= $objForm->getFormName() ?>" class="form form-horizontal"
                              style="margin-bottom: 0;">

                            <?
                            $objCampo = $objForm->getTextInputInstance("nome");
                            $objCampo->setLabel("Nome");
                            $objCampo->setDefaultValues();
                            $objCampo->setPlaceHolder("Informe o nome.");
                            $objCampo->addValidation(new MinLengthValidation(10, "O tamanho mínimo do nome deve ser de {0} caracteres."));
                            ?>

                            <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                 ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                                <?= $objCampo->renderLabel(); ?>
                                <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                    <?= $objCampo->render(); ?>
                                    <?= $objCampo->renderValidationBlock(); ?>

                                </div>
                            </div>

                            <?= $objForm->renderHorizontalLine(); ?>

                            <?
                            $objCampo = $objForm->getPasswordInputInstance("senha");
                            $objCampo->setLabel("Senha");
                            $objCampo->setDefaultValues();
                            $objCampo->setPlaceHolder("Defina sua senha de acesso.");
                            $objCampo->addValidation(new MinLengthValidation(8, "O tamanho mínimo da senha é de {0} caracteres."));
                            $objCampo->addValidation(new MaxLengthValidation(20, "O tamanho máximo da senha é de {0} caracteres."));
                            ?>

                            <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                 ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                                <?= $objCampo->renderLabel(); ?>
                                <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                    <?= $objCampo->render(); ?>
                                    <?= $objCampo->renderValidationBlock(); ?>

                                </div>
                            </div>

                            <?= $objForm->renderHorizontalLine(); ?>

                            <?
                            $objCampo = $objForm->getTextareaInstance("comentarios");
                            $objCampo->setLabel(I18N::getExpression("Comentários"));
                            $objCampo->setDefaultValues();
                            $objCampo->setPlaceHolder(I18N::getExpression("Digite comentários adicionais."));
                            ?>

                            <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                 ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                                <?= $objCampo->renderLabel(); ?>
                                <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                    <?= $objCampo->render(); ?>
                                    <?= $objCampo->renderValidationBlock(); ?>

                                </div>
                            </div>

                            <?= $objForm->renderHorizontalLine(); ?>

                            <?
                            $objCampo = $objForm->getTextInputInstance("desativado");
                            $objCampo->setLabel(I18N::getExpression("Desativado"));
                            $objCampo->setDefaultValues();
                            $objCampo->setPlaceHolder(I18N::getExpression("Campo Desativado."));
                            $objCampo->addValidation(new MinLengthValidation(8, I18N::getExpression("O tamanho mínimo do identificador do passaporte é de {0} caracteres.")));
                            ?>

                            <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                 ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                                <?= $objCampo->renderLabel(); ?>
                                <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                    <?= $objCampo->render(); ?>
                                    <?= $objCampo->renderValidationBlock(); ?>

                                </div>
                            </div>

                            <?= $objForm->renderHorizontalLine(); ?>

                            <div class='<?= $objCampo->getFormGroupCssClass() ?>'>
                                <label class='col-md-2 control-label'>Checkboxes</label>

                                <div class='col-md-10'>
                                    <div class='checkbox'>
                                        <label>
                                            <input type='checkbox' name="fieldPreferencias" value=''>
                                            sit totam labore sunt porro possimus cum officia earum deserunt
                                        </label>
                                    </div>
                                    <div class='checkbox'>
                                        <label>
                                            <input type='checkbox' value=''>
                                            sit culpa sequi rem dolorem
                                        </label>
                                    </div>
                                    <div class='checkbox'>
                                        <label>
                                            <input type='checkbox' value=''>
                                            autem mollitia nulla aliquam
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <?= $objForm->renderHorizontalLine(); ?>

                            <?
                            $objCampo = $objForm->getSelectInstance("pessoa");
                            $objCampo->setDefaultValues();
                            $objCampo->setLabel(I18N::getExpression("Pessoa"));
                            $objCampo->setPlaceHolder(I18N::getExpression("Selecione uma pessoa da lista..."), false);
                            $objCampo->setMatchExpression("{{nome}} {{idade}}");
                            $objCampo->setCloseOnSelect(true);
                            $objCampo->setRefreshDelay(0);
                            $propsFilter = new PropsFilter();
                            $propsFilter->addFieldToFilter("nome");
                            $propsFilter->addFieldToFilter("email");
                            $selectChoiceContent = new SelectChoiceContent("pessoa");
                            $selectChoiceContent->setMainString(new AngularString("nome", "div", Select::getSelectHighLightFilter()));
                            $selectChoiceContent->addSecondaryString(I18N::getExpression("E-mail") . ": ", new AngularString("email"));
                            $selectChoiceContent->addSecondaryString(I18N::getExpression("Idade") . ": ", new AngularString("idade", "span", Select::getSelectHighLightFilter()));
                            $objCampo->setFilter($propsFilter);
                            $objCampo->setSelectChoiceContent($selectChoiceContent);
                            ?>

                            <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                 ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                                <?= $objCampo->renderLabel(); ?>
                                <div class='<?= $objCampo->getContainerCssClass() ?>'>

                                    <?= $objCampo->render(); ?>
                                    <?= $objCampo->renderValidationBlock(); ?>

                                </div>
                                <div class='col-md-2'>

                                    <?= $objCampo->renderAddEntityButton(I18N::getExpression("Adicionar Pessoa")); ?>

                                </div>

                            </div>

                            <?= $objForm->renderHorizontalLine(); ?>

                            <?
                            $objCampo = $objForm->getMultipleSelectInstance("paises");
                            $objCampo->setDefaultValues();
                            $objCampo->setLabel("Paises");
                            $objCampo->setPlaceHolder("Selecione 2 países ou mais...", false);
                            $objCampo->setMatchExpression("{{nome}} {{codigo}}");
                            $objCampo->setCloseOnSelect(true);
                            $objCampo->setRefreshDelay(0);
                            $propsFilter = new PropsFilter();
                            $propsFilter->addFieldToFilter("nome");
                            $propsFilter->addFieldToFilter("codigo");
                            $selectChoiceContent = new SelectChoiceContent("paises");
                            $selectChoiceContent->setMainString(new AngularString("nome", "div", Select::getSelectHighLightFilter()));
                            $selectChoiceContent->addSecondaryString("Código: ", new AngularString("codigo", "span", Select::getSelectHighLightFilter()));
                            $objCampo->setFilter($propsFilter);
                            $objCampo->setSelectChoiceContent($selectChoiceContent);
                            $objCampo->addValidation(new MinChoicesValidation(2, I18N::getExpression("Você deve escolher pelo menos {0} países.")));
                            ?>

                            <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                 ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                                <?= $objCampo->renderLabel(); ?>
                                <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                    <?= $objCampo->render(); ?>
                                    <?= $objCampo->renderValidationBlock(); ?>

                                </div>

                            </div>

                            <!--<pre>Componente: {{mainForm.fieldPaises.$error | json}}</pre>-->

                            <?= $objForm->renderHorizontalLine(); ?>

                            <?
                            $objCampoFrom = $objForm->getDateInputInstance("periodoDe");
                            $objCampoFrom->setDefaultValues();
                            $objCampoFrom->setLabel(I18N::getExpression("Período - De"));
                            $objCampoFrom->setPlaceHolder(I18N::getExpression("Inicio do Período"));
                            $objCampoFrom->addValidation(new RequiredValidation("O início do período é de preenchimento obrigatório."));
                            $objCampoTo = $objForm->getDateInputInstance("pediodoAte");
                            $objCampoTo->setDefaultValues();
                            $objCampoTo->setLabel(I18N::getExpression("Período - Até"));
                            $objCampoTo->setPlaceHolder(I18N::getExpression("Término do Período"));
                            $objCampoTo->addValidation(new RequiredValidation("O término do período é de preenchimento obrigatório."));
                            $objRange = new DateRangeInput($objCampoFrom, $objCampoTo);
                            ?>

                            <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                 ng-class="<?= $objCampoFrom->getFormGroupAngularClass(); ?>">

                                <?= $objCampoFrom->renderLabel(); ?>
                                <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                    <?= $objRange->renderDateFromField(); ?>
                                    <?= $objCampoFrom->renderValidationBlock(); ?>

                                </div>
                            </div>

                            <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                 ng-class="<?= $objCampoTo->getFormGroupAngularClass(); ?>">

                                <?= $objCampoTo->renderLabel(); ?>
                                <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                    <?= $objRange->renderDateToField(); ?>
                                    <?= $objCampoTo->renderValidationBlock(); ?>

                                </div>
                            </div>

                            <?= $objForm->renderHorizontalLine(); ?>

                            <?
                            $objCampo = $objForm->getTimeInputInstance("hora");
                            $objCampo->setLabel(I18N::getExpression("Hora"));
                            $objCampo->setDefaultValues();
                            $objCampo->setHourStep(1);
                            $objCampo->setMinutesStep(2);
                            $objCampo->setPlaceHolder("");
                            ?>

                            <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                 ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                                <?= $objCampo->renderLabel(); ?>
                                <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                    <?= $objCampo->render(); ?>
                                    <?= $objCampo->renderValidationBlock(); ?>

                                </div>
                            </div>

                            <?= $objForm->renderHorizontalLine(); ?>

                            <?
                            $objCampo = $objForm->getCurrencyInputInstance("preco");
                            $objCampo->setLabel(I18N::getExpression("Preço"));
                            $objCampo->setDefaultValues();
                            $objCampo->setDecimalPlaces(2);
                            $objCampo->setPlaceHolder(I18N::getExpression("Informe o valor em R$"));
                            $objCampo->addValidation(new MaxLengthValidation(1500, I18N::getExpression("O valor máximo permitido é de R$ {f0}.")));
                            ?>

                            <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                 ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                                <?= $objCampo->renderLabel(); ?>
                                <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                    <?= $objCampo->render(); ?>
                                    <?= $objCampo->renderValidationBlock(); ?>

                                </div>
                            </div>

                            <?= $objForm->renderHorizontalLine(); ?>

                            <?
                            $objCampo = $objForm->getCepInputInstance("cep");
                            $objCampo->setLabel("CEP");
                            $objCampo->setDefaultValues();
                            $objCampo->setPlaceHolder("Informe o CEP");
                            ?>

                            <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                 ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                                <?= $objCampo->renderLabel(); ?>
                                <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                    <?= $objCampo->render(); ?>
                                    <?= $objCampo->renderValidationBlock(); ?>

                                </div>
                            </div>

                            <?= $objForm->renderHorizontalLine(); ?>

                            <?
                            $objCampo = $objForm->getCpfInputInstance("cpf");
                            $objCampo->setLabel("CPF");
                            $objCampo->setDefaultValues();
                            $objCampo->setPlaceHolder("Informe o CPF");
                            ?>

                            <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                 ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                                <?= $objCampo->renderLabel(); ?>
                                <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                    <?= $objCampo->render(); ?>
                                    <?= $objCampo->renderValidationBlock(); ?>

                                </div>
                            </div>

                            <?= $objForm->renderHorizontalLine(); ?>

                            <?
                            $objCampo = $objForm->getCnpjInputInstance("cnpj");
                            $objCampo->setLabel(I18N::getExpression("CNPJ"));
                            $objCampo->setDefaultValues();
                            $objCampo->setPlaceHolder(I18N::getExpression("Informe o CNPJ"));
                            ?>

                            <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                 ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                                <?= $objCampo->renderLabel(); ?>
                                <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                    <?= $objCampo->render(); ?>
                                    <?= $objCampo->renderValidationBlock(); ?>

                                </div>
                            </div>

                            <?= $objForm->renderHorizontalLine(); ?>

                            <?
                            $objCampo = $objForm->getPhoneInputInstance("telefone");
                            $objCampo->setLabel(I18N::getExpression("Telefone"));
                            $objCampo->setDefaultValues();
                            $objCampo->setPlaceHolder(I18N::getExpression("Telefone em padrão brasileiro"));
                            ?>

                            <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                 ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                                <?= $objCampo->renderLabel(); ?>
                                <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                    <?= $objCampo->render(); ?>
                                    <?= $objCampo->renderValidationBlock(); ?>

                                </div>
                            </div>

                            <?= $objForm->renderHorizontalLine(); ?>

                            <?
                            $objCampo = $objForm->getPercentageInputInstance("porcentagem");
                            $objCampo->setLabel(I18N::getExpression("Porcentagem"));
                            $objCampo->setDefaultValues();
                            $objCampo->setPlaceHolder(I18N::getExpression("Porcentagem"));
                            ?>

                            <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                 ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                                <?= $objCampo->renderLabel(); ?>
                                <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                    <?= $objCampo->render(); ?>
                                    <?= $objCampo->renderValidationBlock(); ?>

                                </div>
                            </div>

                            <?= $objForm->renderHorizontalLine(); ?>

                            <?
                            $objCampo = $objForm->getFileUploadArea("upload");
                            $objCampo->setLabel(I18N::getExpression("Upload"));
                            $objCampo->setPlaceHolder(I18N::getExpression("Selecione ou arraste os arquivos"));
                            $objCampo->setDefaultValues();
                            ?>

                            <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                 ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                                <?= $objCampo->renderLabel(); ?>
                                <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                    <?= $objCampo->render(); ?>
                                    <?= $objCampo->renderValidationBlock(); ?>

                                    <?= $objCampo->renderProgressPanel(); ?>

                                </div>
                            </div>

                            <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                 ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                                <?= $objCampo->renderLabel(); ?>
                                <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                    <div ngf-drop ngf-select ng-model="vm.files" class="drop-box"
                                         ngf-drag-over-class="'dragover'" ngf-multiple="true" ngf-allow-dir="true"
                                         accept="image/*,application/pdf"
                                         ngf-pattern="'image/*,application/pdf'">Arraste imagens ou documentos PDF ou
                                        clique para fazer o upload
                                    </div>
                                    <div ngf-no-file-drop>Funcionalidade de arraste-e-solte não disponível em seu
                                        navegador.
                                    </div>

                                    <div class='row upload-file-list' style="margin-top: 15px;"
                                         ng-show="vm.formData.documentosUpload != null && vm.formData.documentosUpload.length > 0">
                                        <div class='col-sm-12'>
                                            <div class='box bordered-box blue-border' style='margin-bottom:0;'>
                                                <div class='box-content box-no-padding'>
                                                    <div class='responsive-table'>
                                                        <div>
                                                            <table class='table' style='margin-bottom:0;'>
                                                                <tbody>
                                                                <tr ng-repeat="f in vm.formData.documentosUpload">
                                                                    <td>{{f.name}}</td>
                                                                    <td>{{f.fileUniqueId}}</td>
                                                                    <td>
                                                                        <div class='text-right'>
                                                                            <a class='btn btn-danger btn-xs'
                                                                               ng-click="vm.removeUploadedFile(f.fileUniqueId)">
                                                                                <i class='icon-remove'></i>
                                                                            </a>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="upload-progress progress" ng-show="vm.isUploading">
                                        <div class="progress-bar progress-bar-primary"
                                             style="width:{{vm.uploadPercentage}};">{{vm.uploadPercentage}}
                                        </div>
                                    </div>

                                    <pre class="debug-box">Upload Log: {{ vm.fieldsParameters.fieldDocumentoUpload.uploadLog | json}}</pre>

                                </div>

                            </div>

                            <?= $objForm->renderHorizontalLine(); ?>

                            <div class='row'>
                                <div class='col-sm-12'>
                                    <div class='box'>
                                        <div class='box-header blue-background'>
                                            <div class='title'>
                                                <div class='icon-edit'></div>
                                                Cargos
                                            </div>
                                            <div class='actions'>
                                                <a class="btn btn-xs plus-icon" href="javascript: void(0);"
                                                   ng-click="vm.addCargo();"><i></i></a>
                                                <a class="btn box-collapse btn-xs btn-link" href="javascript: void(0);"><i></i></a>
                                            </div>
                                        </div>
                                        <div class='box-content'>

                                            <?
                                            $objCargoForm = new RelatedEntityEnvironment("vm.relatedEntities.cargos", $objForm->getAngularControllerName());
                                            $objCargoForm->setLayoutOneColumn();
                                            $objCargoForm->setModelIteratorVar("cargo");
                                            $objCargoForm->setFormName($objForm->getFormName());
                                            ?>

                                            <div ng-repeat="<?= $objCargoForm->getAngularRepeatExpression() ?>">

                                                <div class="related-entity-container" ng-if="cargo.removed == false">

                                                    <?
                                                    $objCampo = $objCargoForm->getTextInputInstance("nome");
                                                    $objCampo->setLabel("Nome");
                                                    $objCampo->setDynamicNameAndIdPrefix("'cargo' + \$index");
                                                    $objCampo->setPlaceHolder("Informe o nome.");
                                                    $objCampo->addValidation(new MinLengthValidation(10, "O tamanho mínimo do nome deve ser de {0} caracteres."));
                                                    ?>

                                                    <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                                         ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                                                        <?= $objCampo->renderLabel(); ?>
                                                        <div class='<?= $objCampo->getContainerCssClass(); ?>'>
                                                            <?= $objCampo->render(); ?>
                                                            <?= $objCampo->renderValidationBlock(); ?>
                                                        </div>
                                                    </div>

                                                    <?= $objCargoForm->renderHorizontalLine(); ?>

                                                    <?
                                                    $objCampo = $objCargoForm->getTextInputInstance("descricao");
                                                    $objCampo->setLabel("Descrição");
                                                    $objCampo->setDefaultValues();
                                                    $objCampo->setPlaceHolder("Informe a descrição.");
                                                    $objCampo->addValidation(new MinLengthValidation(50, "O tamanho mínimo do nome deve ser de {0} caracteres."));
                                                    ?>

                                                    <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                                         ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                                                        <?= $objCampo->renderLabel(); ?>
                                                        <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                                            <?= $objCampo->render(); ?>
                                                            <?= $objCampo->renderValidationBlock(); ?>

                                                        </div>
                                                    </div>

                                                    <?= $objCargoForm->renderHorizontalLine(); ?>

                                                    <div class='form-actions form-actions-padding-sm'>
                                                        <div class='row'>
                                                            <div class='col-md-10 col-md-offset-2'>

                                                                <button class='btn' ng-click="vm.removeCargo($index)">
                                                                    Remover
                                                                </button>

                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                </div>

                            </div>

                            <div class='form-actions form-actions-padding-sm'>
                                <div class='row'>
                                    <div class='col-md-10 col-md-offset-2'>

                                        <?= $objForm->renderSubmitButton(); ?>
                                        <button class='btn' type='submit'>Cancelar</button>

                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--Definição do Controller Angular JS -->
<script type="text/javascript">

    <?=Helper::incluirConteudoDataServices(); ?>

</script>

<?= Helper::incluirControllerAngularJS(); ?>

