"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var React = require("react");
var react_bootstrap_1 = require("react-bootstrap");
var uiComponente_1 = require("lib/uiComponente");
var geral_1 = require("aplicacao/comum/geral");
var DetalhesUsuario = (function (_super) {
    __extends(DetalhesUsuario, _super);
    function DetalhesUsuario() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    DetalhesUsuario.prototype.setDefaultBody = function () {
        var modelUsuario = this.props.modelUsuario;
        this.bodyContent = (<div className="corpo-modal-detalhes">   
                <p><b>Nome do Usuário:</b>{modelUsuario.nomeUsuario}</p>                  
                <p><b>Identificador do Usuário:</b> {modelUsuario.idUsuario}</p>
                <p><b>Sexo:</b> {geral_1.Helper.getSexoAsString(modelUsuario.sexoUsuario)}</p>
                <p><b>Posição em:</b> {DateUtil.getFormattedDateTime(modelUsuario.dataPosicao)}</p>    
            </div>);
    };
    DetalhesUsuario.prototype.setDefaultHeader = function () {
        var modelUsuario = this.props.modelUsuario;
        this.headerContent = (<react_bootstrap_1.Modal.Title id="contained-modal-title-lg">
                {modelUsuario.nomeUsuario}
            </react_bootstrap_1.Modal.Title>);
    };
    return DetalhesUsuario;
}(uiComponente_1.PainelFlutuante));
exports.DetalhesUsuario = DetalhesUsuario;
