<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BO_Assinatura
 *
 * @author home
 */
class BO_Usuario
{
    public static function getTotalDeUsuarios($idCorporacao)
    {
        $totalDeUsuarios = EXTDAO_Usuario::getNumeroDeUsuariosCadastrados($idCorporacao);
        return new Mensagem_token(
            PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO,
            null,
            $totalDeUsuarios);
    }

    public static function consultaUsuario($email)
    {
        $obj = new EXTDAO_Usuario();
        $id = $obj->getIdDoUsuarioSeExistente($email);
        if (strlen($id))
        {
            return new Mensagem(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO, $id);
        }
        else
        {
            return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, "Usu?rio inexistente.");
        }
    }

    public static function editaUsuarioPeloSICOB($email, $senhaDecrypt, $nome)
    {
        $objCrypt = new Crypt();

        $idUsuario = EXTDAO_Usuario::getIdUsuario($email);
        if (strlen($idUsuario))
        {
            $objUsuario = new EXTDAO_Usuario();
            $objUsuario->select($idUsuario);
            $objUsuario->setNome($nome);
            $objUsuario->setEmail($email);
            $objUsuario->setSenha($objCrypt->crypt($senhaDecrypt));
            $objUsuario->formatarParaSQL();
            $objUsuario->update($idUsuario);
        }
        else
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO,
                                "O cliente n?o foi encontrado na nossa base de dados.");
        }
    }

    public static function trocarUsuario(
        $emailResponsavel,
        $senhaResponsavel,
        $corporacao,
        $emailAntigo,
        $senhaAntiga,
        $nomeNovo,
        $emailNovo,
        $senhaNova,
        $imei,
        $idPrograma)
    {
        $bkpEmail = null;
        $bkpNome = null;
        $bkpSenha = null;
        try
        {
            $db = new Database();
            $idCorporacao = EXTDAO_Corporacao::getIdCorporacao($corporacao, $db);
            if (!strlen($idCorporacao))
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_PARAMETRO_INVALIDO,
                                    "Parametro invalido.");
            }

            $idUsuarioSistemaTabela = EXTDAO_Sistema_tabela::getIdDaSistemaTabela("usuario", $db);
            if (!strlen($idUsuarioSistemaTabela))
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR,
                                    "Sistema tabela n?o encontrado.");
            }

            $idUsuarioResponsavel = EXTDAO_Usuario::getIdUsuario($emailResponsavel, $db);

            if (!strlen($idUsuarioResponsavel))
            {
                return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO,
                                    "Usu?rio $emailResponsavel inexistente.");
            }

            $seguranca = new Seguranca($db);

            $msg = $seguranca->verificaSenha(
                array("id" => $idUsuarioResponsavel, "id_corporacao" => $idCorporacao),
                $senhaResponsavel,
                $db,
                false);
            if ($msg != null && !$msg->ok())
            {
                return $msg;
            }
            $idUsuarioAntigo = EXTDAO_Usuario::getIdUsuario($emailAntigo, $db);

            if (!strlen($idUsuarioAntigo))
            {
                return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO,
                                    "Usu?rio $emailAntigo inexistente.");
            }

            $objUsuario = new EXTDAO_Usuario($db);
            $objUsuario->select($idUsuarioAntigo);
            $bkpNome = $objUsuario->getNome();
            $objUsuario->setNome($nomeNovo);
            $bkpEmail = $objUsuario->setEmail($emailNovo);
            $crypt = new Crypt();

            $senhaC = $crypt->crypt($senhaNova);
            $objUsuario->setSenha($senhaC);
            $objUsuario->setNome(Helper::strtoupperlatin1($nomeNovo));
            $objUsuario->formatarParaSQL();
            $db->iniciarTransacao();
            $msg = $objUsuario->update($idUsuarioAntigo, false);
            if ($msg != null && !$msg->ok())
            {
                $db->rollbackTransacao();
                return $msg;
            }

            $msg = EXTDAO_Sistema_registro_sincronizador::insertTupla(
                $idUsuarioSistemaTabela, $idUsuarioResponsavel, $idCorporacao, '0',
                DatabaseSincronizador::$INDEX_OPERACAO_EDIT, $imei,
                $idPrograma, $idUsuarioAntigo);
            if ($msg != null && !$msg->ok())
            {
                $db->rollbackTransacao();
                return $msg;
            }

            $msg = EXTDAO_Pessoa_usuario::atualizaEmailDoUsuario(
                $idCorporacao, $idUsuarioAntigo, $nomeNovo, $emailNovo, $db, $idUsuarioResponsavel);
            if ($msg != null && !$msg->ok())
            {
                $db->rollbackTransacao();
                return $msg;
            }

            $db->commitTransacao();
            return new Mensagem(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO,
                                "Dados atualizados com sucesso.");
        }
        catch (Exception $ex)
        {
            $db->rollbackTransacao();
            return new Mensagem(null, null, $ex);
        }
    }

    public static function editarUsuario(
        $emailResponsavel,
        $senhaResponsavel,
        $email,
        $nome,
        $corporacao,
        $idPrograma,
        $imei)
    {
        try
        {
            $db = new Database();
            $objSeguranca = new Seguranca($db);

            $msg = $objSeguranca->verificaSenha(
                array("email" => $emailResponsavel, "corporacao" => $corporacao),
                $senhaResponsavel,
                $db,
                false);
            if ($msg != null && !$msg->ok())
            {
                return $msg;
            }

//            echo "PASSOU 2: $email<br/>";
            $idCorporacao = EXTDAO_Corporacao::getIdCorporacao($corporacao, $db);
            if (!strlen($idCorporacao))
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_PARAMETRO_INVALIDO,
                                    "Parametro invalido.");
            }
//            echo "PASSOU 3<br/>";
            $idUsuarioSistemaTabela = EXTDAO_Sistema_tabela::getIdDaSistemaTabela("usuario", $db);
            if (!strlen($idUsuarioSistemaTabela))
            {
                throw new Exception("N?o enconstrou no sistema_tabela a 'usuario'");
            }
            $idUsuarioResponsavel = EXTDAO_Usuario::getIdUsuario($emailResponsavel, $db);
            $idUsuario = EXTDAO_Usuario::getIdUsuario($email, $db);
//            echo "PASSOU 4: $idUsuario<br/>";

//            echo "PASSOU 4.1: $idUsuarioResp<br/>";
            if (!strlen($idUsuario))
            {
                return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO,
                                    "Usu?rio $email inexistente.");
            }
//            echo "PASSOU 6: $idCorporacao<br/>";
            $objUsuario = new EXTDAO_Usuario();
            $objUsuario->select($idUsuario);
//            $objUsuario->setEmail($email);
            $objUsuario->setNome(Helper::strtoupperlatin1($nome));
            $objUsuario->formatarParaSQL();
            $objUsuario->update($idUsuario);

            EXTDAO_Sistema_registro_sincronizador::insertTupla(
                $idUsuarioSistemaTabela, $idUsuario, $idCorporacao, '0',
                DatabaseSincronizador::$INDEX_OPERACAO_EDIT, $imei,
                $idPrograma, $idUsuario);

            EXTDAO_Pessoa_usuario::atualizaDadosDaPessoaDoUsuario(
                $idUsuario, $nome, $db, $idCorporacao, $idUsuarioResponsavel);

            return new Mensagem(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO,
                                "Dados atualizados com sucesso.");
        }
        catch (Exception $ex)
        {
            return new Mensagem(null, null, $ex);
        }
    }

    public static function deletarUsuario($emailResponsavel,
                                          $senhaResponsavel,
                                          $email,
                                          $corporacao,
                                          $idPrograma,
                                          $imei)
    {
        try
        {
            $objSeguranca = new Seguranca();
            $db = new Database();
//            $nomeBanco = $db->getNome(); 
//            echo "PASSOU 1: $nomeBanco<br/>";

            $msg = $objSeguranca->verificaSenha(
                array("email" => $emailResponsavel, "corporacao" => $corporacao),
                $senhaResponsavel,
                $db,
                false);
            if ($msg != null && !$msg->ok())
            {
                return $msg;
            }

//            echo "PASSOU 2: $email<br/>";
            $idCorporacao = EXTDAO_Corporacao::getIdCorporacao($corporacao, $db);
            if (empty($idCorporacao))
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_PARAMETRO_INVALIDO,
                                    "Parametro invalido.");
            }
//            echo "PASSOU 3<br/>";

            $idUsuario = EXTDAO_Usuario::getIdUsuario($email);
//            echo "PASSOU 4: $idUsuario<br/>";
            $idUsuarioResponsavel = EXTDAO_Usuario::getIdUsuario($emailResponsavel);
//            echo "PASSOU 4.1: $idUsuarioResponsavel<br/>";
            if (empty($idUsuario))
            {
                return new Mensagem(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO,
                                    "O usu?rio $email foi removido com sucesso.");
            }

            EXTDAO_Sistema_registro_sincronizador::deleteMultiplo(
                $db,
                "usuario_tipo_corporacao",
                $idUsuarioResponsavel,
                $idCorporacao,
                $idPrograma,
                $imei,
                "SELECT utc.id 
                      FROM usuario_tipo_corporacao 
                      WHERE utc.corporacao_id_INT = $idCorporacao
                            AND utc.usuario_id_INT = $idUsuario");

            EXTDAO_Sistema_registro_sincronizador::deleteMultiplo(
                $db, "usuario_privilegio",
                $idUsuarioResponsavel,
                $idCorporacao,
                $idPrograma,
                $imei,
                "SELECT utp.id 
                  FROM usuario_privilegio utp
                  WHERE utp.corporacao_id_INT = $idCorporacao
                        AND utp.usuario_id_INT = $idUsuario");

            EXTDAO_Sistema_registro_sincronizador::deleteMultiplo(
                $db,
                "usuario_servico",
                $idUsuarioResponsavel,
                $idCorporacao,
                $idPrograma,
                $imei,

                "SELECT us.id 
                      FROM usuario_servico us
                      WHERE us.corporacao_id_INT = $idCorporacao
                            AND us.usuario_id_INT = $idUsuario");

            EXTDAO_Sistema_registro_sincronizador::deleteMultiplo(
                $db,
                "usuario_categoria_permissao",
                $idUsuarioResponsavel,
                $idCorporacao,
                $idPrograma,
                $imei,
                "SELECT ucp.id 
                      FROM usuario_categoria_permissao ucp
                      WHERE ucp.corporacao_id_INT = $idCorporacao
                            AND ucp.usuario_id_INT = $idUsuario");

            EXTDAO_Sistema_registro_sincronizador::deleteMultiplo(
                $db,
                "usuario_corporacao",
                $idUsuarioResponsavel,
                $idCorporacao,
                $idPrograma,
                $imei,
                "SELECT uc.id 
                      FROM usuario_corporacao uc
                      WHERE uc.corporacao_id_INT = $idCorporacao
                            AND uc.usuario_id_INT = $idUsuario");

            EXTDAO_Sistema_registro_sincronizador::deleteMultiplo(
                $db,
                "usuario_pessoa",
                $idUsuarioResponsavel,
                $idCorporacao,
                $idPrograma,
                $imei,
                "SELECT up.id 
                      FROM usuario_pessoa up
                      WHERE up.corporacao_id_INT = $idCorporacao
                            AND up.usuario_id_INT = $idUsuario");

            EXTDAO_Sistema_registro_sincronizador::deleteMultiplo(
                $db,
                "usuario_empresa",
                $idUsuarioResponsavel,
                $idCorporacao,
                $idPrograma,
                $imei,
                "SELECT ue.id 
                      FROM usuario_empresa ue
                      WHERE ue.corporacao_id_INT = $idCorporacao
                            AND ue.usuario_id_INT = $idUsuario");

            EXTDAO_Sistema_registro_sincronizador::deleteMultiplo(
                $db,
                "usuario",
                $idUsuarioResponsavel,
                $idCorporacao,
                $idPrograma,
                $imei,
                "SELECT u.id 
                      FROM usuario u 
                      WHERE u.id = $idUsuario
                        AND (
                          SELECT COUNT(uc.id) 
                          FROM usuario_corporacao uc 
                          WHERE uc.corporacao_id_INT != $idCorporacao 
                            AND uc.usuario_id_INT= $idUsuario
                        ) == 0");

            return new Mensagem(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO,
                                "O usu?rio $email foi removido com sucesso.");
        }
        catch (Exception $ex)
        {
            return new Mensagem(null, null, $ex);
        }
    }

    private static function rollbackCadastroUsuario(
        Interface_mensagem $msg,
        $operacoes,
        $idUsuarioResponsavel,
        $idCorporacao,
        $imei,
        $idPrograma)
    {
        if ($msg != null && !Interface_mensagem::checkOk($msg))
        {
            if (count($operacoes) > 0)
            {
                //nao sera feito em transacao devido a alta concorrencia da base
                $db = new Database();

                foreach ($operacoes as $tabela => $idOuIdsRegistro)
                {
                    $strIn = "";
                    if (is_array($idOuIdsRegistro))
                    {
                        foreach ($idOuIdsRegistro as $id)
                        {
                            if (!empty($strIn))
                            {
                                $strIn .= ", ";
                            }
                            $strIn .= $id;
                        }
                    }
                    else
                    {
                        $strIn = $idOuIdsRegistro;
                    }
                    $db->queryMensagem("DELETE FROM $tabela WHERE id IN ($strIn)");

                    EXTDAO_Sistema_registro_sincronizador::inserirMultiplosRegistros(
                        $db, $tabela, $idUsuarioResponsavel,
                        $idCorporacao, '0',
                        DatabaseSincronizador::$INDEX_OPERACAO_REMOVE, $imei, $idPrograma,
                        $idOuIdsRegistro
                    );
                }
            }
        }
        return $msg;
    }

    public static function cadastrarUsuario(
        $emailResponsavel,
        $senhaResponsavel,
        $nome,
        $email,
        $corporacao,
        $senha,
        $idPrograma,
        $imei)
    {
        $operacoes = array();
        //nome do usuario
        try
        {
            if (!strlen($emailResponsavel)
                || !strlen($senhaResponsavel)
                || !strlen($nome)
                || !strlen($email)
                || !strlen($email)
                || !strlen($corporacao)
                || !strlen($senha)
            )
            {
                return Mensagem::factoryParametroInvalido();
            }
            $objSeguranca = new Seguranca();

            $msg = $objSeguranca->verificaSenha(
                array("email" => $emailResponsavel, "corporacao" => $corporacao),
                $senhaResponsavel,
                null,
                false);
            if ($msg != null && !$msg->ok())
            {
                return $msg;
            }
            $db = new Database();

            //CHECAR SE ELE ? O DONO, OU ENVIAR UM DADO CRIPTOGRAFADO
            $nome = Helper::strtoupperlatin1($nome);
            $corporacao = Helper::strtoupperlatin1($corporacao);
            $nomeSemAcento = Helper::retiraAcento($nome);

            $email = Helper::strtolowerlatin1($email);

            if (strlen($nome) > 0 &&
                strlen($email) > 0 &&
                strlen($corporacao) > 0 &&
                strlen($senha) > 0 &&
                strlen($nomeSemAcento)
            )
            {
                $idCorporacao = EXTDAO_Corporacao::getIdCorporacao($corporacao, $db);
                if (empty($idCorporacao))
                {
                    return new Mensagem(PROTOCOLO_SISTEMA::ACESSO_NEGADO,
                                        I18N::getExpression("O grupo {0} n�o existe! Voc� pode cadastra-lo se quiser!", $corporacao));
                }
                $idUsuario = EXTDAO_Usuario::getIdUsuario($email, $db);

                if (!empty($idUsuario))
                {
                    $idUC = EXTDAO_Usuario_corporacao::getIdUsuarioCorporacao($idUsuario, $idCorporacao, $db);
                    if (!empty($idUC))
                    {
                        return new Mensagem(
                            PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO,
                            I18N::getExpression("Usu�rio j� estava adicionado ao seu grupo. Aguarde que a sincroniza��o ser� realizada."));
                    }
                }

                $idUsuarioResponsavel = EXTDAO_Usuario::getIdUsuario($emailResponsavel, $db);
                $idUsuarioSistemaTabela = EXTDAO_Sistema_tabela::getIdDaSistemaTabela("usuario", $db);
                $idUsuarioCorporacaoSistemaTabela = EXTDAO_Sistema_tabela::getIdDaSistemaTabela("usuario_corporacao", $db);

                $usuarioNovo = false;
                $objSincronizador = new EXTDAO_Sistema_registro_sincronizador($db);
                //cadastrando usuario ainda nao existente 
                if ($idUsuario == null || !strlen($idUsuario))
                {
                    $usuarioNovo = true;
                    $objUsuario = new EXTDAO_Usuario($db);
                    $objUsuario->setEmail($email);

                    $objUsuario->setNome($nome);
                    $objUsuario->setNome_normalizado($nomeSemAcento);
                    $objCriptografia = new Crypt();
                    $objUsuario->setSenha($objCriptografia->crypt($senha));
                    $objUsuario->setStatus_BOOLEAN("1");
                    $objUsuario->setCadastro_SEC(Helper::getUTCNowSec());
                    $objUsuario->setCadastro_OFFSEC(Helper::getTimezoneOffsetSec());
                    $objUsuario->formatarParaSQL();
                    $msg = $objUsuario->insert(false, $idCorporacao);
                    if ($msg != null && !Interface_mensagem::checkOk($msg))
                    {
                        return $msg;
                    }
                    $idUsuario = $objUsuario->getId();

                    $operacoes["usuario"] = array($idUsuario, $objSincronizador->getId());
                }

                $objSincronizador->inserirRegistro(
                    $idUsuarioSistemaTabela, $idUsuarioResponsavel, $idCorporacao, '0',
                    DatabaseSincronizador::$INDEX_OPERACAO_INSERT, $imei,
                    $idPrograma, $idUsuario);

                $msg = EXTDAO_Pessoa_usuario::vinculaUsuarioAPessoa(
                    $idCorporacao, $nome, $email,
                    $idUsuario, $db, $idUsuarioResponsavel, $operacoes);

                if ($msg != null && !Interface_mensagem::checkOk($msg))
                {
                    return self::rollbackCadastroUsuario($msg, $operacoes,
                                                         $idUsuarioResponsavel, $idCorporacao, $imei, $idPrograma);
                }

                $idPessoa = $msg->mValor;
                HelperLog::logCadastro("adicionaProfissaoIndefinidaAPessoa ...");
                $msg = EXTDAO_Pessoa_empresa::adicionaProfissaoIndefinidaAPessoa(
                    $db, $idCorporacao, $idUsuarioResponsavel, $idPessoa);

                HelperLog::logCadastro("adicionaProfissaoIndefinidaAPessoa: " . print_r($msg, true));
                $msg = EXTDAO_Usuario_tipo::cadastraTipoDoUsuarioAdm(
                    $idUsuario, $idCorporacao, $idUsuarioResponsavel, $imei, $idPrograma, $db,
                    $objSincronizador, $operacoes);

                if ($msg != null && $msg->mCodRetorno != PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO)
                {
                    return self::rollbackCadastroUsuario($msg, $operacoes,
                                                         $idUsuarioResponsavel, $idCorporacao, $imei, $idPrograma);
                }

                $msg = EXTDAO_Usuario_servico::cadastraServicosDoUsuario(
                    $idUsuarioResponsavel, $idCorporacao, $idUsuario, $imei,
                    $idPrograma, $db, $objSincronizador, $operacoes);

                if ($msg != null && $msg->mCodRetorno != PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO)
                {
                    return self::rollbackCadastroUsuario($msg, $operacoes,
                                                         $idUsuarioResponsavel, $idCorporacao, $imei, $idPrograma);
                }

                $idUsuarioCorporacao = EXTDAO_Usuario_corporacao::getIdUsuarioCorporacao(
                    $idUsuario, $idCorporacao, $db);

                if (empty($idUsuarioCorporacao))
                {
                    //cadastra a tupla usuario corporacao
                    $objUsuarioCorporacao = new EXTDAO_Usuario_corporacao($db);
                    $objUsuarioCorporacao->setUsuario_id_INT($idUsuario);
                    $objUsuarioCorporacao->setStatus_BOOLEAN("1");
                    $objUsuarioCorporacao->setIs_adm_BOOLEAN("1");
                    $objUsuarioCorporacao->setCadastro_SEC(Helper::getUTCNowSec());
                    $objUsuarioCorporacao->setCadastro_OFFSEC(Helper::getTimezoneOffsetSec());
                    $objUsuarioCorporacao->formatarParaSQL();
                    $objUsuarioCorporacao->setCorporacao_id_INT($idCorporacao);
                    $msg = $objUsuarioCorporacao->insert(
                        false,
                        $idCorporacao,
                        $idUsuarioResponsavel);

                    if ($msg != null && !Interface_mensagem::checkOk($msg))
                    {
                        return self::rollbackCadastroUsuario($msg, $operacoes,
                                                             $idUsuarioResponsavel, $idCorporacao, $imei, $idPrograma);
                    }
                    //$idUsuarioCorporacao = $objUsuarioCorporacao->getIdDoUltimoRegistroInserido($idCorporacao);
                    $idUsuarioCorporacao = $msg->mValor;

                    $objSincronizador->inserirRegistro(
                        $idUsuarioCorporacaoSistemaTabela, $idUsuarioResponsavel, $idCorporacao,
                        '0', DatabaseSincronizador::$INDEX_OPERACAO_INSERT, $imei,
                        $idPrograma, $idUsuarioCorporacao);
                }

//                EXTDAO_Usuario::enviaEmailParaOUsuario(
//                    $email,
//                    ASSUNTO_CADASTRO_USUARIO,
//                    CONTEUDO_ASSUNTO_CADASTRO_USUARIO,
//                    $db);
                self::enviarEmailBoasVindas($email, $nome, $corporacao);

                return new Mensagem(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO,
                                    I18N::getExpression("O usu�rio $email foi cadastrado com sucesso!"));
            }
            else
            {
                return Mensagem::factoryParametroInvalido();
            }
        }
        catch (Exception $ex)
        {
            $msg = new Mensagem(null, null, $ex);
            self::rollbackCadastroUsuario($msg, $operacoes,
                                          $idUsuarioResponsavel, $idCorporacao, $imei, $idPrograma);

            return $msg;
        }
    }

    public static function cadastraUsuarioECorporacao(
        $nome,
        $email,
        $corporacao,
        $senha,
        $idPrograma = null,
        $imei = null,
        $idCorporacaoNoSICOB = null)
    {
        //nome do usuario

        $nome = Helper::strtoupperlatin1($nome);
        $corporacao = Helper::strtoupperlatin1($corporacao);
        $nomeSemAcento = Helper::retiraAcento($nome);
        $corporacaoSemAcento = Helper::retiraAcento($corporacao);
        $email = Helper::strtolowerlatin1($email);

        if (strlen($nome) > 0 &&
            strlen($email) > 0 &&
            strlen($corporacao) > 0 &&
            strlen($senha) > 0 &&
            strlen($corporacaoSemAcento) &&
            strlen($nomeSemAcento)
        )
        {
            $db = new Database();
            $db->iniciarTransacao();
            try
            {
                $idUsuarioSistemaTabela = EXTDAO_Sistema_tabela::getIdDaSistemaTabela("usuario", $db);
                $idUsuarioCorporacaoSistemaTabela = EXTDAO_Sistema_tabela::getIdDaSistemaTabela("usuario_corporacao", $db);
                $usuarioNovo = false;
                $corporacaoNova = false;
                $msg = null;
                $idCorporacao = EXTDAO_Corporacao::getIdCorporacao(
                    $corporacao, $db);

                $objSincronizador = new EXTDAO_Sistema_registro_sincronizador($db);
                if (!strlen($idCorporacao))
                {
                    $objCorporacao = new EXTDAO_Corporacao($db);
                    $objCorporacao->setId($idCorporacaoNoSICOB);
                    $objCorporacao->setNome($corporacao);
                    $objCorporacao->setNome_normalizado($corporacaoSemAcento);
//                    $objCorporacao->setCadastro_SEC(Helper::getUTCNowSec());
//                    $objCorporacao->setCadastro_OFFSEC(Helper::getTimezoneOffsetSec());
                    $objCorporacao->formatarParaSQL();
//                    $db->iniciarTransacao();
                    if ($idCorporacaoNoSICOB != null)
                    {
                        $msg = $objCorporacao->insert(true, $idCorporacaoNoSICOB);
                    }
                    else
                    {
                        $msg = $objCorporacao->insert(true);
                    }
                    if ($msg != null && $msg->erro())
                    {
                        return $msg;
                    }
//                    $db->commitTransacao();
//                    $objCorporacao->database->close();
                    //$idCorporacao = $objCorporacao->getIdDoUltimoRegistroInserido();
                    $idCorporacao = $objCorporacao->getIdDoUltimoRegistroInserido();
                    $corporacaoNova = true;
                }

                //echo "CORPORACAO: $idCorporacao";

                $idUsuario = EXTDAO_Usuario::getIdUsuario($email, $db);

                if ($idUsuario != null)
                {
                    $objSeguranca = new Seguranca($db);
                    if (!$objSeguranca->verificaUsuarioESenha($idUsuario, $senha))
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::SENHA_INCORRETA,
                                            "Senha incorreta do usu?rio j? existente.");
                    }
                }

                //cadastrando usuario ainda nao existente 
                if ($idUsuario == null)
                {
                    $usuarioNovo = true;
                    $objUsuario = new EXTDAO_Usuario($db);
                    $objUsuario->setEmail($email);
                    $objUsuario->setNome($nome);
                    $objUsuario->setNome_normalizado($nomeSemAcento);
                    $objCriptografia = new Crypt();
                    $cr = $objCriptografia->crypt($senha);
                    $objUsuario->setSenha($cr);
                    $objUsuario->setStatus_BOOLEAN("1");

                    $objUsuario->setCadastro_SEC(Helper::getUTCNowSec());
                    $objUsuario->setCadastro_OFFSEC(Helper::getTimezoneOffsetSec());

                    $objUsuario->formatarParaSQL();
                    $db->iniciarTransacao();
                    $msg = $objUsuario->insert(true, $idCorporacao);

                    if ($msg != null && $msg->erro())
                    {
                        return $msg;
                    }
//                    $db->commitTransacao();
                    $idUsuario = $objUsuario->getIdDoUltimoRegistroInserido();

                    $objSincronizador->inserirRegistro(
                        $idUsuarioSistemaTabela, $idUsuario, $idCorporacao, '0',
                        DatabaseSincronizador::$INDEX_OPERACAO_INSERT, $imei,
                        $idPrograma, $idUsuario);
                }

//                $db->iniciarTransacao();

                $msg = EXTDAO_Usuario_tipo::cadastraTipoDoUsuarioAdm(
                    $idUsuario, $idCorporacao, $idUsuario,
                    $imei, $idPrograma, $db, $objSincronizador);
                if ($msg != null && !$msg->ok())
                {
                    return $msg;
                }

                $msg = EXTDAO_Usuario_servico::cadastraServicosDoUsuario(
                    $idUsuario, $idCorporacao, $idUsuario, $imei, $idPrograma, $db, $objSincronizador);
                if ($msg != null && !$msg->ok())
                {
                    return $msg;
                }

                $msg = EXTDAO_Tipo_documento::cadastraTiposDeDocumento(
                    $idUsuario, $idCorporacao, $idUsuario, $imei, $idPrograma, $db, $objSincronizador);
                if ($msg != null && !$msg->ok())
                {
                    return $msg;
                }

                $idUsuarioCorporacao = EXTDAO_Usuario_corporacao::getIdUsuarioCorporacao(
                    $idUsuario, $idCorporacao, $db);

                if (!strlen($idUsuarioCorporacao))
                {
                    //cadastra a tupla usuario corporacao
                    $objUsuarioCorporacao = new EXTDAO_Usuario_corporacao($db);
                    $objUsuarioCorporacao->setUsuario_id_INT($idUsuario);
                    $objUsuarioCorporacao->setStatus_BOOLEAN("1");
                    $objUsuarioCorporacao->setIs_adm_BOOLEAN("1");
                    $objUsuarioCorporacao->setCadastro_SEC(Helper::getUTCNowSec());
                    $objUsuarioCorporacao->setCadastro_OFFSEC(Helper::getTimezoneOffsetSec());
                    $objUsuarioCorporacao->formatarParaSQL();
                    $objUsuarioCorporacao->setCorporacao_id_INT($idCorporacao);
                    $msg = $objUsuarioCorporacao->insert(false, $idCorporacao);
                    if (!$msg->ok())
                    {
                        return $msg;
                    }
                    //$idUsuarioCorporacao = $objUsuarioCorporacao->getIdDoUltimoRegistroInserido($idCorporacao);
                    $idUsuarioCorporacao = $objUsuarioCorporacao->getIdDoUltimoRegistroInserido();

                    $objSincronizador->inserirRegistro(
                        $idUsuarioCorporacaoSistemaTabela, $idUsuario, $idCorporacao,
                        '0', DatabaseSincronizador::$INDEX_OPERACAO_INSERT, $imei,
                        $idPrograma, $idUsuarioCorporacao);
                }

                $msg = EXTDAO_Pessoa_usuario::vinculaUsuarioAPessoa(
                    $idCorporacao, $nome, $email,
                    $idUsuario, $db, $idUsuarioCorporacao);

                if ($msg != null && !Interface_mensagem::checkOk($msg))
                {
                    return $msg;
                }

                $idPessoa = $msg->mValor;
                HelperLog::logCadastro("adicionaProfissaoIndefinidaAPessoa ... Msg: " . print_r($msg, true));
                $msg = EXTDAO_Pessoa_empresa::adicionaProfissaoIndefinidaAPessoa(
                    $db, $idCorporacao, $idUsuario, $idPessoa);
                HelperLog::logCadastro("adicionaProfissaoIndefinidaAPessoa: " . print_r($msg, true));

                $db->commitTransacao();
//                EXTDAO_Usuario::enviaEmailParaOUsuario($email,
//                    ASSUNTO_CADASTRO_USUARIO,
//                    CONTEUDO_ASSUNTO_CADASTRO_USUARIO,
//                    $db);
                self::enviarEmailBoasVindas($email, $nome, $corporacao);

                return new Mensagem(
                    PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO,
                    "A corpora��o e o usu�rio foram criados com sucesso.");
            }
            catch (Exception $exc)
            {
                if ($db != null)
                {
                    $db->rollbackTransacao();
                }
                return new Mensagem(null, null, $exc);
            }
        }
        else
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_PARAMETRO_INVALIDO,
                                "Parametro invalido.");
        }
    }

    public static function cadastraUsuarioECorporacaoNaReserva(
        $corporacaoReserva,
        $nome,
        $email,
        $corporacao,
        $senha,
        $idPrograma = null,
        $imei = null,
        $idCorporacaoNoSICOB = null)
    {
        $nome = Helper::strtoupperlatin1($nome);
        $corporacao = Helper::strtoupperlatin1($corporacao);
        $nomeSemAcento = Helper::retiraAcento($nome);
        $corporacaoSemAcento = Helper::retiraAcento($corporacao);
        $email = Helper::strtolowerlatin1($email);

        if (strlen($nome) > 0 &&
            strlen($email) > 0 &&
            strlen($corporacao) > 0 &&
            strlen($senha) > 0 &&
            strlen($corporacaoSemAcento) &&
            strlen($nomeSemAcento)
        )
        {
            ConfiguracaoCorporacaoDinamica::clearConfiguracaoSiteFromCache($corporacao);
            $db = new Database();

            $idUsuario = EXTDAO_Usuario::getIdUsuario($corporacaoReserva, $db);

            $idCorporacao = EXTDAO_Corporacao::getIdCorporacao($corporacaoReserva, $db);
            if (empty($idCorporacao))
            {
                $idCorporacao = EXTDAO_Corporacao::getIdCorporacao($corporacao, $db);
                if (empty($idCorporacao))
                {
                    $db->query("SELECT 1 FROM corporacao WHERE id = $idCorporacaoNoSICOB");
                    $res = $db->getPrimeiraTuplaDoResultSet(0);
                    if ($res == 1)
                    {
                        $idCorporacao = $idCorporacaoNoSICOB;
                    }
                    if (empty($idCorporacao))
                    {
                        throw new Exception("Corporacao $idCorporacaoNoSICOB reserva inexistente na base.");
                    }
                }
            }
            else
            {
                if ($idCorporacao != $idCorporacaoNoSICOB)
                {
                    throw  new Exception("J� existia a corporacao cadastrada porem em outra reserva: $idCorporacao != $idCorporacaoNoSICOB");
                }
            }

            if (empty($idUsuario))
            {
                $idUsuario = EXTDAO_Usuario::getIdUsuario($email, $db);
                if (empty($idUsuario))
                {
                    $db->query("SELECT usuario_id_INT FROM usuario_corporacao WHERE corporacao_id_INT = $idCorporacao");
                    $idUsuario = $db->getPrimeiraTuplaDoResultSet(0);
                    if (empty($idUsuario))
                    {
                        throw new Exception("UsuarioModel da reserva nao encontrado. Corporacao: $corporacaoReserva");
                    }
                }
            }

            $idUsuarioSistemaTabela = EXTDAO_Sistema_tabela::getIdDaSistemaTabela("usuario", $db);
            $idCorporacaoSistemaTabela = EXTDAO_Sistema_tabela::getIdDaSistemaTabela("corporacao", $db);
            $msg = null;

            $db->iniciarTransacao();
            try
            {
                $objSincronizador = new EXTDAO_Sistema_registro_sincronizador($db);

                $msg = EXTDAO_Corporacao::updateNome($db, $idCorporacaoNoSICOB, $corporacao);
                if ($msg != null && !$msg->ok())
                {
                    if ($db != null)
                    {
                        $db->rollbackTransacao();
                    }
                    return $msg;
                }
                $objSincronizador->inserirRegistro(
                    $idCorporacaoSistemaTabela, $idUsuario, $idCorporacao,
                    '0', DatabaseSincronizador::$INDEX_OPERACAO_EDIT, $imei,
                    $idPrograma, $idCorporacaoNoSICOB);

                //Durante a criacao da reserva, a corporacao eh setada no email do usuario
                //atualizando dados do usuario
                $objUsuario = new EXTDAO_Usuario($db);
                $objUsuario->select($idUsuario);
                $objUsuario->setEmail($email);
                $objUsuario->setNome($nome);

                $objUsuario->setStatus_BOOLEAN("1");
                $objUsuario->formatarParaSQL();

                $msg = $objUsuario->update($idUsuario, false, $idCorporacao);
                if ($msg == null ||
                    $msg->ok())
                {
                    $msg2 = $objUsuario->alterarSenhaAvulso($idUsuario, $senha);
                    if ($msg2 != null && $msg2->erro())
                    {
                        if ($db != null)
                        {
                            $db->rollbackTransacao();
                        }
                        return $msg2;
                    }
                }

                $objSincronizador->inserirRegistro(
                    $idUsuarioSistemaTabela, $idUsuario, $idCorporacao,
                    '0', DatabaseSincronizador::$INDEX_OPERACAO_EDIT, $imei,
                    $idPrograma, $idUsuario);
                if ($msg != null &&
                    (!$msg->ok() && !$msg->erroSqlChaveDuplicada()))
                {
                    if ($db != null)
                    {
                        $db->rollbackTransacao();
                    }
                    return $msg;
                }
                $idPessoa = $objUsuario->getIdDaPessoaAssociada();
                if ($idPessoa != null)
                {
                    $objPessoa = new EXTDAO_Pessoa($db);

                    $objPessoa->setNome($nome);
                    $objPessoa->formatarParaSQL();
                    $objPessoa->update($idPessoa, true, $idCorporacao, $idUsuario);
                }

                $db->commitTransacao();

                $msg = new Mensagem(
                    PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO,
                    "A corpora��o e o usu�rio foram criados com sucesso.");
            }
            catch (Exception $exc)
            {
                if ($db != null)
                {
                    $db->rollbackTransacao();
                }
                $msg = new Mensagem(null, null, $exc);
            }
            if ($msg->ok())
            {
                self::enviarEmailBoasVindas($email, $nome, $corporacao);
            }
            return $msg;
        }
        else
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_PARAMETRO_INVALIDO,
                                "Parametro inv�lido.");
        }
    }

    public static function enviarEmailBoasVindas($email, $nome, $corporacao)
    {
        Email_Sender::sendEmailTemplate(
            Email_Sender::TIPO_BEM_VINDO,
            I18N::getExpression("Bem vindo a WorkOffline.com.br"),
            $email,
            array(
                "TITULO" => I18N::getExpression("Bem vindo."),
                "TEXTO" => I18N::getExpression(
                    "Ol� $nome, bem vindo a workoffline.com.br. "
                    . "O grupo $corporacao acaba de ser criado. Voc� pode adicionar mais usu�rios ao seu grupo para "
                    . "trabalhar em equipe! Acesso o App Android Ponto Eletr�nico"),
            )
        );
    }

    public static function getInformacoesDoUsuario(
        Database $db,
        $email,
        $corporacao,
        $senha,
        $carregarPermissao = false)
    {
        if (!empty($email) > 0 &&
            !empty($corporacao) > 0 &&
            !empty($senha)
        )
        {
            $query = "SELECT u.id id, 
                u.nome nome, 
                 c.id id_corporacao,
                c.nome corporacao,  
                u.status_BOOLEAN status,
                uc.id id_usuario_corporacao,
                uc.status_BOOLEAN status_corporacao,
                uc.is_adm_BOOLEAN is_adm,
                ucp.categoria_permissao_id_INT id_categoria_permissao
             FROM usuario u 
                JOIN usuario_corporacao uc ON u.id = uc.usuario_id_INT
                JOIN corporacao c ON uc.corporacao_id_INT = c.id 
                LEFT JOIN usuario_categoria_permissao ucp ON u.id = ucp.usuario_id_INT AND ucp.corporacao_id_INT = c.id
             WHERE u.email = '$email' AND
                c.nome='$corporacao' ";

            $msg = $db->queryMensagem($query);
            if ($msg != null && !$msg->ok())
            {
                return $msg;
            }
            if ($db->rows() == 0)
            {
                return new Mensagem(
                    PROTOCOLO_SISTEMA::RESULTADO_VAZIO,
                    "Dados do usu�rio n�o encontrado");
            }
            else
            {
                $obj = Helper::getPrimeiroObjeto($db->result, 1, 0);

                // se o usuario n�o for administrador
                if ($carregarPermissao && is_numeric($obj["id_categoria_permissao"]))
                {
                    $vetorObjPermissao = EXTDAO_Permissao_categoria_permissao::getListaPermissaoDaCategoriaDePermissao(
                        $obj["id_categoria_permissao"],
                        $db);

                    if (!empty($vetorObjPermissao))
                    {
                        $obj["permissoes"] = $vetorObjPermissao;
                    }
                }

                $configuracaoSite = Registry::get('ConfiguracaoSite');
                $obj["id_sistema_sihop"] = $configuracaoSite->ID_SISTEMA_SIHOP;
                return new Mensagem_generica($obj);
            }
        }
        else
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_PARAMETRO_INVALIDO, "Parametros invalidos. $email::$corporacao::$senha");
        }
    }

    public static function loginEGetInformacoesDoUsuario(
        $email,
        $corporacao,
        $senha,
        $carregarPermissoes,
        $carregarInformacoesUsuario)
    {
        $db = new Database();
        $objSeguranca = new Seguranca();

        $msg = $objSeguranca->login(
            array("email" => $email, "corporacao" => $corporacao),
            $senha,
            $db,
            true);
//        $msg = $objSeguranca->verificaSenha(
//            array("email" => $email, "corporacao" => $corporacao),
//            $senha,
//            $db,
//            false);

        if ($msg != null && $msg->ok() && $carregarInformacoesUsuario)
        {
            return BO_Usuario::getInformacoesDoUsuario(
                $db,
                $email,
                $corporacao,
                $senha,
                $carregarPermissoes);
        }
        return $msg;
    }

    public static function isUsuarioAdministradorDaCorporacao($pIdUsuario, $pIdCorporacao)
    {
        $db = Registry::get('Database', false);
        if ($db == null)
        {
            $db = new Database();
        }
        //checa a existencia do funcionario
        $query = "SELECT uc.id as id
             FROM usuario_corporacao uc
             WHERE uc.usuario_id_INT = '" . $pIdUsuario . "' AND 
                 uc.corporacao_id_INT = '" . $pIdCorporacao . "' AND
                     uc.is_adm_BOOLEAN = '1'";

        $msg = $db->queryMensagem($query);

        if ($msg == null || $msg->ok())
        {
            if ($db->rows() > 0)
            {
                $idUC = $db->getPrimeiraTuplaDoResultSet("id");
                if (strlen($idUC))
                {
                    return new Mensagem_token(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO,
                                              null,
                                              $idUC);
                }
            }
            return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, "Nao tem usuario");
        }
        else
        {
            return $msg;
        }
    }

    public static function trocarEmailDoClientePeloSistema($idUsuario, $nomeNovo, $emailNovo, $senhaDecrypt)
    {
        return BO_SICOB::trocarEmailDoClientePeloSistema($idUsuario, $nomeNovo, $emailNovo, $senhaDecrypt);
    }

    public function alterarSenhaPeloSicobPorEmail($email, $idCorporacao, $senhaAntiga, $senhaNova)
    {
        $db = new Database();
        $idUsuario = EXTDAO_Usuario::getIdUsuario($email, $db);
        if (!strlen($idUsuario))
        {
            return new Mensagem(PROTOCOLO_SISTEMA::USUARIO_NAO_CADASTRADO,
                                I18N::getExpression("N�o existe nenhum usu�rio com email $email"));
        }
        return $this->alterarSenhaPeloSicob($idUsuario, $idCorporacao, $senhaAntiga, $senhaNova);
    }

    public function alterarSenhaPeloSicob($idUsuario, $idCorporacao, $senhaAntiga, $senhaNova, $db = null)
    {
        $objSeguranca = new Seguranca();
        $msg = $objSeguranca->verificaSenha(
            array("id" => $idUsuario, "id_corporacao" => $idCorporacao),
            $senhaAntiga,
            null,
            false);
        if ($msg != null && !$msg->ok())
        {
            return Mensagem::factoryAcessoNegado();
        }
        if ($db == null)
        {
            $db = new Database();
        }

        $objUsuario = new EXTDAO_Usuario($db);
        $objUsuario->select($idUsuario);
        $msgCorp = EXTDAO_Corporacao::getCorporacaoNome($db, $idCorporacao);
        if ($msgCorp == null)
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR,
                                I18N::getExpression("Grupo do id $idCorporacao n�o encontrado"));
        }
        else
        {
            if (!Interface_mensagem::checkOk($msgCorp))
            {
                return $msgCorp;
            }
        }
        $corporacao = $msgCorp->mValor;
        $msg = BO_SICOB::alterarSenha(
            $objUsuario->getEmail(),
            $idCorporacao,
            $corporacao,
            $senhaAntiga,
            $senhaNova);
        if (Interface_mensagem::checkOk($msg))
        {
            $seguranca = new Seguranca();
            $seguranca->loginWeb($objUsuario->getEmail(), $corporacao, $senhaNova, $db);
        }
        return $msg;
    }

    //Chamado pelo SICOB apenas, � onde realiza a altera��o na base de dados do ponto eletr�nico
    public static function alterarSenhaDoUsuario($pCampoC)
    {
        try
        {
            $crypt = new Crypt();
            $retorno = $crypt->decryptGet($pCampoC);

            if ($retorno == null)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_PARAMETRO_INVALIDO, "Par?metro inv?lido 1");
            }

            $valores = $retorno['valores'];
            $email = $valores['email'];
            $senha = $valores['senha'];
            $senhaNova = $valores['senha_nova'];
            $corporacao = $valores['corporacao'];
            $idPrograma = $valores['id_programa'];
            $imei = $valores['imei'];

            if (!strlen($email)
                || !strlen($corporacao)
                || !strlen($senhaNova)
            )
            {
                return Mensagem::factoryParametroInvalido();
            }

            $idCorporacao = EXTDAO_Corporacao::getIdCorporacao($corporacao);
            if (!strlen($idCorporacao))
            {
                return Mensagem::factoryParametroInvalido();
            }
            $idUsuarioSistemaTabela = EXTDAO_Sistema_tabela::getIdDaSistemaTabela("usuario");
            if (!strlen($idUsuarioSistemaTabela))
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR,
                                    "Sistema tabela n?o encontrado.");
            }
            $db = new Database();
            $objUsuario = new EXTDAO_Usuario($db);
            $idUsuario = EXTDAO_Usuario::getIdUsuario($email, $db);

            if (!strlen($idUsuario))
            {
                return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO,
                                    I18N::getExpression("N�o existe usu�rio cadastrado com o email $email"));
            }

            $msg = $objUsuario->alterarSenhaAvulso($idUsuario, $senhaNova);

            EXTDAO_Sistema_registro_sincronizador::insertTupla(
                $idUsuarioSistemaTabela, $idUsuario, $idCorporacao, '0',
                DatabaseSincronizador::$INDEX_OPERACAO_EDIT, $imei,
                $idPrograma, $idUsuario);

            if (Interface_mensagem::checkOk($msg))
            {
                Email_Sender::sendEmailTemplate(
                    Email_Sender::TIPO_SENHA_ALTERADA,
                    I18N::getExpression("Altera��o de Cadastro - WorkOffline.com.br"),
                    $objUsuario->getEmail(),
                    array(
                        ":TITULO" => I18N::getExpression("Registro de atividade - WorkOffline.com.br"),
                        ":TEXTO" => I18N::getExpression("Ol�, a senha do seu usu�rio {$objUsuario->getEmail()}, foi alterada nesse instante!")));

                return new Mensagem(
                    PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO,
                    I18N::getExpression("Senha alterada com sucesso!"));
            }
            else
            {
                return $msg;
            }
        }
        catch (Exception $ex)
        {
            return new Mensagem(null, null, $ex);
        }
    }
}
