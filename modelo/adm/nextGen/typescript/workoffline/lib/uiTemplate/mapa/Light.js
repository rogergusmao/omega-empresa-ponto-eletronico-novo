"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var MapaTemplate_1 = require("./MapaTemplate");
var Light = (function (_super) {
    __extends(Light, _super);
    function Light() {
        return _super.call(this, "Light") || this;
    }
    Light.prototype.getStyles = function () {
        return [{
                "featureType": "landscape.man_made",
                "elementType": "geometry",
                "stylers": [{ "color": "#f7f1df" }]
            },
            {
                "featureType": "landscape.natural",
                "elementType": "geometry",
                "stylers": [{ "color": "#d0e3b4" }]
            },
            {
                "featureType": "landscape.natural.terrain",
                "elementType": "geometry",
                "stylers": [{ "visibility": "off" }]
            },
            {
                "featureType": "poi",
                "stylers": [{ "hue": "#FF0300" }, { "saturation": -100 }, { "lightness": 25 }, { "gamma": 1 }]
            },
            {
                "featureType": "road",
                "elementType": "geometry.stroke",
                "stylers": [{ "visibility": "off" }]
            },
            {
                "featureType": "road",
                "elementType": "labels",
                "stylers": [{ "visibility": "off" }]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry.fill",
                "stylers": [{ "color": "#ffe15f" }]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry.stroke",
                "stylers": [{ "color": "#efd151" }]
            },
            {
                "featureType": "road.arterial",
                "elementType": "geometry.fill",
                "stylers": [{ "color": "#ffffff" }]
            },
            {
                "featureType": "road.local",
                "elementType": "geometry.fill",
                "stylers": [{ "color": "black" }]
            },
            {
                "featureType": "transit.station.airport",
                "elementType": "geometry.fill",
                "stylers": [{ "color": "#cfb2db" }]
            }, { "featureType": "water", "elementType": "geometry", "stylers": [{ "color": "#a2daf2" }] }];
    };
    return Light;
}(MapaTemplate_1.MapaTemplate));
exports.Light = Light;
