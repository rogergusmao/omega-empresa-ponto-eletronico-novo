"use strict";
exports.__esModule = true;
var Unserializer_JSON = (function () {
    function Unserializer_JSON() {
    }
    Unserializer_JSON.prototype.unserialize = function (data) {
        return JSON.parse(data);
    };
    return Unserializer_JSON;
}());
exports.Unserializer_JSON = Unserializer_JSON;
