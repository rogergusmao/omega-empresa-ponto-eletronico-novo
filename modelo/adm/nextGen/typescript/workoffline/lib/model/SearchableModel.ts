import { SearchableAttribute } from "lib/model";

export class SearchableModel
{
    static readonly DEFAULT_SELECTABLE_ITEM_CLASS = 'selectable';
    static readonly DEFAULT_SELECTED_ITEM_CLASS = 'selected';

    public nomeEntidade: string;
    private primaryKeyString: string;
    private searchableString: string;
    private hasChanged: boolean;

    private isVisible: boolean;
    private isSelectable: boolean;
    private selectableCssClass: string;
    private selectedCssClass: string;

    public constructor(nomeEntidade: string, isVisible: boolean = true, isSelectable: boolean = true,
                        selectableCssClass: string = SearchableModel.DEFAULT_SELECTABLE_ITEM_CLASS,
                        selectedCssClass: string = SearchableModel.DEFAULT_SELECTED_ITEM_CLASS)
    {
        //nome da entidade
        this.nomeEntidade = nomeEntidade;
        this.hasChanged = false;
        this.isVisible = isVisible;
        this.isSelectable = isSelectable;
        this.selectableCssClass = selectableCssClass;
        this.selectedCssClass = selectedCssClass;
    }

    public getIsVisible(): boolean
    {
        return this.isVisible;
    }

    public getIsSelectable(): boolean
    {
        return this.isSelectable;
    }

    public setIsSelectable(isSelectable: boolean): void
    {
        this.isSelectable = isSelectable;
    }

    public getSelectableCssClass(): string
    {
        return this.selectableCssClass;
    }

    public setSelectableCssClass(selectableCssClass: string): void
    {
        this.selectableCssClass = selectableCssClass;
    }

    public getSelectedCssClass(): string
    {
        return this.selectedCssClass;
    }

    public setSelectedCssClass(selectedCssClass: string): void
    {
        this.selectedCssClass = selectedCssClass;
    }

    public getPrimaryKeyString(): string;
    public getPrimaryKeyString(forceReload?: boolean): string
    {
        forceReload = (typeof forceReload == 'undefined') ? false : forceReload;

        if (forceReload || this.primaryKeyString == null)
        {
            this.primaryKeyString = '';
            var attributes: Array<keyof SearchableModel> = Object.getOwnPropertyNames(this) as Array<keyof SearchableModel>;
            for (var i: number = 0; i < attributes.length; i++)
            {
                /*
                if (this[attributes[i]] instanceof IBaseAttribute && this[attributes[i]].getPrimaryKey())
                {
                    this.primaryKeyString += this[attributes[i]].getValue() + ",";
                }
                */
            }

            if (this.primaryKeyString.length > 0)
            {
                this.primaryKeyString = this.primaryKeyString.substr(0, this.primaryKeyString.length - 1)
            }

        }

        return this.primaryKeyString;

    }

    public setVisible(): void
    {
        this.isVisible = true;
    }

    public setInvisible(): void
    {
        this.isVisible = false;
    }

    public doActionsAfterEdit(): void
    {
        this.setSearchableString();
    }

    public getSearchableString(): string
    {
        return this.searchableString;
    }

    public setSearchableString(): void
    {
        this.searchableString = '';
        var attributes: Array<any> = Object.getOwnPropertyNames(this);
        for (var i: number = 0; i < attributes.length; i++)
        {
            /*
            if (this[attributes[i]] instanceof IBaseAttribute && this[attributes[i]].getSearchable())
            {
                this.searchableString += this[attributes[i]].getValue() + "|";
            }
            */
        }
    }

    public setAttributeValue<T>(attr: SearchableAttribute<T>, value: T)
    {
        attr.setValue(value);
        this.hasChanged = true;
    }

    public markAsChanged()
    {
        this.hasChanged = true;
    }

    static dynamicSort(property: string)
    {
        var sortOrder = 1;
        if (property[0] === "-")
        {
            sortOrder = -1;
            property = property.substr(1);
        }

        return function (a :any, b :any)
        {
            var result = (a[property].getValue() < b[property].getValue()) ? -1 : (a[property].getValue() > b[property].getValue()) ? 1 : 0;
            return result * sortOrder;
        }
    }

    static dynamicSortMultiple(...params :any[])
    {
        var props = arguments[0];
        return function (obj1 :any, obj2 :any)
        {
            var i = 0, result = 0, numberOfProperties = props.length;
            while (result === 0 && i < numberOfProperties)
            {
                result = SearchableModel.dynamicSort(props[i])(obj1, obj2);
                i++;
            }

            return result;
        }

    }

}