"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var Moment = require("moment");
var model_1 = require("lib/model");
var geral_1 = require("lib/geral");
var model_2 = require("aplicacao/modulo/calendario/model");
var Pessoa = (function (_super) {
    __extends(Pessoa, _super);
    function Pessoa() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Pessoa.prototype.addEvento = function (pontoInicial, pontoFinal) {
        var evento = new model_2.Evento();
        evento.titulo = pontoInicial.nomePessoa;
        evento.idPessoa = pontoInicial.idPessoa;
        evento.pontoInicial = pontoInicial;
        evento.pontoFinal = pontoFinal;
        evento.corBackgroundEvento = this.corBackground;
        evento.corForegroudEvento = this.corForeground;
        this.eventos.push(evento);
    };
    Pessoa.prototype.getTempoTotal = function () {
        var totalTempoPonto = Moment.duration(0, 'second');
        for (var _i = 0, _a = this.eventos; _i < _a.length; _i++) {
            var evento = _a[_i];
            if (evento.pontoFinal != null) {
                var diferenca = Moment(evento.pontoFinal.dataPonto).diff(Moment(evento.pontoInicial.dataPonto));
                totalTempoPonto = totalTempoPonto.add(diferenca);
            }
        }
        return totalTempoPonto;
    };
    Pessoa.prototype.getTempoTotalFormatado = function () {
        var duracao = this.getTempoTotal();
        return "" + duracao.format('hh:mm');
    };
    Pessoa.factory = function (pontoInicial, pontoFinal) {
        var instance = new Pessoa();
        var corAleatoria = new geral_1.RandomColor();
        instance.nomePessoa = pontoInicial.nomePessoa;
        instance.idPessoa = pontoInicial.idPessoa;
        instance.corBackground = corAleatoria.mainColor;
        instance.corForeground = corAleatoria.foregroundColor;
        instance.eventos = Array();
        return instance;
    };
    return Pessoa;
}(model_1.Model));
exports.Pessoa = Pessoa;
