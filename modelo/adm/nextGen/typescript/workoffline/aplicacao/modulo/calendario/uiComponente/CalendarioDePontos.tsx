import * as FullCalendar from 'fullcalendar';
import * as FullCalendarInputTypes from 'fullcalendar/src/types/input-types';
import * as React from 'react';

import { Calendario, DomContainer } from 'lib/uiComponente';

export class CalendarioDePontos extends Calendario<Calendario.CalendarioProps, Calendario.CalendarioState>
{
    public constructor(props: Calendario.CalendarioProps, reactContext: any)
    { 
        super(props, reactContext);  
    }

    public fixLayout()
    {
        var maxHeight: number = DomContainer.getMaxAllowedHeight();
        $(this.jQuerySelector).height(maxHeight-145);
        $(this.jQuerySelector).parent().height(maxHeight-115);
    }

}