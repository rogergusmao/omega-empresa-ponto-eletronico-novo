"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var uiComponente_1 = require("lib/uiComponente");
var CalendarioDePontos = (function (_super) {
    __extends(CalendarioDePontos, _super);
    function CalendarioDePontos(props, reactContext) {
        return _super.call(this, props, reactContext) || this;
    }
    CalendarioDePontos.prototype.fixLayout = function () {
        var maxHeight = uiComponente_1.DomContainer.getMaxAllowedHeight();
        $(this.jQuerySelector).height(maxHeight - 145);
        $(this.jQuerySelector).parent().height(maxHeight - 115);
    };
    return CalendarioDePontos;
}(uiComponente_1.Calendario));
exports.CalendarioDePontos = CalendarioDePontos;
