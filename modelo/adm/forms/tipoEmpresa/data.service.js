/*

 Arquivo gerado atrav�s de gerador de c�digo em 11/01/2018 as 18:55:36.
 Para que o arquivo n�o seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

 Tabela correspondente: tipo_empresa
 Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

 */

returnObject.getTipoEmpresa = function (filterParameters)
{

    var parameters = {
        filterParameters: filterParameters,
        loadComboBoxesData: true
    };

    return $http({
        url: baseURLAdm + "/webservice.php?class=EXTDAO_Tipo_empresa&action=__getRecord",
        method: "POST",
        data: parameters,
        headers: {
            'Content-Type': 'application/json'
        }
    });

};

returnObject.addTipoEmpresa = function (formData)
{

    return $http({
        url: baseURLAdm + "/webservice.php?class=EXTDAO_Tipo_empresa&jsonOutput=true&action=add",
        method: "POST",
        data: formData,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    });

};

returnObject.editTipoEmpresa = function (formData)
{

    return $http({
        url: baseURLAdm + "/webservice.php?class=EXTDAO_Tipo_empresa&jsonOutput=true&action=edit",
        method: "POST",
        data: formData,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    });

};

returnObject.getAllComboBoxesDataForTipoEmpresaForm = function ()
{
    return $http({
        url: baseURLAdm + "/webservice.php?class=EXTDAO_Tipo_empresa&jsonOutput=true&action=__getFormComboBoxesData",
        method: "POST",
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    });

};



