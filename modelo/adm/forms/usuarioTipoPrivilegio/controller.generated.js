

        /*

        Arquivo gerado atrav�s de gerador de c�digo em 07/09/2017 as 01:32:16.
        Para que o arquivo n�o seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

        Tabela correspondente: usuario_tipo_privilegio
        Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

        */

omegaApp.generatedFormUsuarioTipoPrivilegioController = function($scope, $rootScope, $window, $interval, $timeout, dataService, Upload, vm) {try { vm.fieldsTypes = {};

vm.formData = {
        usuarioTipoId: null,
identificadorFuncionalidade: null,
};

vm.disabledFields = {
        fieldUsuarioTipoId: false,
fieldIdentificadorFuncionalidade: false,
};

vm.requiredFields = {
        fieldUsuarioTipoId: true,
fieldIdentificadorFuncionalidade: true,
};

vm.relatedEntities =
        {
        };

vm.fieldsParameters = {
        fieldUsuarioTipoId: {remoteData: null},
fieldIdentificadorFuncionalidade: null,
};

vm.submitForm = function(formInstance)
        {
            if($rootScope.validateForm(formInstance))
            {
                var formData = angular.merge({}, vm.formData, GeneralUtil.getRelatedEntitiesData(vm.relatedEntities));
                if (!GeneralUtil.isNullOrEmpty(vm.formData.id))
                {
                    dataService.editUsuarioTipoPrivilegio(formData).then(function(returnContent)
                    {
                        if (GeneralUtil.validateRemoteResponse(returnContent))
                        {
                            $rootScope.showDialog(vm, 'after-usuarioTipoPrivilegio-update-template.html');
                        }
                        else
                        {
                            $rootScope.instantErrorMessages.push(returnContent.data.mMensagem);
                        }
                    });
                }
                else
                {
                    dataService.addUsuarioTipoPrivilegio(formData).then(function(returnContent)
                    {
                        if (GeneralUtil.validateRemoteResponse(returnContent))
                        {
                            $rootScope.showDialog(vm, 'after-usuarioTipoPrivilegio-insert-template.html');
                        }
                        else
                        {
                            $rootScope.instantErrorMessages.push(returnContent.data.mMensagem);
                        }
                    });
                }
            }

        };

if (!GeneralUtil.isNullOrEmpty(__GET) && !GeneralUtil.isNullOrEmpty(__GET.id))
        {

            var filterData = { id: __GET.id };
            var filterOperators = { id: Constants.FilterOperator.EQUALS };
            var parameters = GeneralUtil.getFilterParameters(filterData, filterOperators);

            dataService.getUsuarioTipoPrivilegio(parameters).then(function (returnContent)
            {
                if (GeneralUtil.validateRemoteResponse(returnContent))
                {
                    vm.formData = returnContent.data.mObj;
                }
                else
                {
                    $rootScope.instantErrorMessages.push(returnContent.data.mMensagem);
                }

            });
        }

vm.openUsuarioTipoIdModal = function()
                {
                    $rootScope.uibModalInstance = $rootScope.showModalDialog('forms', 'UsuarioTipo');
                    
                    //atualizando valores da lista
                    var unregisterModal = $rootScope.$watch(function() { return $rootScope.uibModalInstance }, function () {
    
                        if($rootScope.uibModalInstance == null)
                        {
                            vm.getListOfUsuarioTipoId();
                            vm.formData.usuarioTipoId = null;
                            unregisterModal();
                        }
    
                    });
                    
                };
                

vm.getListOfUsuarioTipoId = function()
                {
                    dataService.getListOfUsuarioTipo().then(function (returnContent)
                    {
                        if (GeneralUtil.validateRemoteResponse(returnContent))
                        {
                            vm.fieldsParameters.fieldUsuarioTipoId.remoteData = returnContent.data.mObj.gridData;
                        }
                        else
                        {
                            $rootScope.instantErrorMessages.push(returnContent.data.mMensagem);
                        }

                    });

                };
                

vm.loadListOfUsuarioTipoPrivilegio = function()
        {
            document.location.href = 'index.php?tipo=lists&page=usuarioTipoPrivilegio';
        };

        vm.addNewUsuarioTipoPrivilegio = function()
        {
            document.location.href = document.location.href;
        };

}
                        catch(ex)
                        {
                            $rootScope.instantErrorMessages.push(new Message('Erro no controller generatedFormUsuarioTipoPrivilegioController', ex.message));
                        }

                }