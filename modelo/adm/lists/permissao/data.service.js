/*

 Arquivo gerado atrav�s de gerador de c�digo em 07/01/2018 as 15:54:11.
 Para que o arquivo n�o seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

 Tabela correspondente: permissao
 Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

 */

returnObject.getAllComboBoxesDataForPermissaoFilter = function ()
{
    return $http({
        url: baseURLAdm + "/webservice.php?class=EXTDAO_Permissao&jsonOutput=true&action=__getFilterComboBoxesData",
        method: "POST",
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    });

};

returnObject.getPermissaoList = function (listParams)
{
    listParams.listMappingType = Constants.ListMappingType.GRID;
    return $http.post(baseURLAdm + "/webservice.php?class=EXTDAO_Permissao&action=__getList", listParams);
};

returnObject.removeRecords = function (recordsPKs)
{
    return $http.post(baseURLAdm + "/webservice.php?class=EXTDAO_Permissao&action=remove", recordsPKs);
};

returnObject.getListOfPaiPermissaoIdForPermissaoList = function ()
{
    var params = {filterData: null, listMappingType: Constants.ListMappingType.COMBOBOX};
    return $http.post(baseURLAdm + "/webservice.php?class=EXTDAO_Permissao&action=__getList", params);
};
                

