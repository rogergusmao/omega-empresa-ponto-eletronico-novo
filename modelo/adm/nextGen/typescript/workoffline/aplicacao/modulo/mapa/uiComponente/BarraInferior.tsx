import * as React from "react";

import { ReactComponent, Relogio, ElementoVisualizacao } from "lib/uiComponente";
import { ReactProps } from 'lib/geral';
import { MapaTemplate, Default, Light } from "lib/uiTemplate";

export class BarraInferior extends ReactComponent<BarraInferior.BarraInferiorProps, BarraInferior.BarraInferiorState>
{
    protected constructor(props: BarraInferior.BarraInferiorProps, reactContext: any)
    {
        super(props || {} as BarraInferior.BarraInferiorProps, reactContext);

        let arrTipos = new Array<ElementoVisualizacao.Tipo>();
        for(let elemento of this.getListaDeElementos())
        {
          arrTipos.push(elemento.tipo);
        }

        this.state = { elementosSelecionados: arrTipos }

    }

    public getListaDeTemplates() :Array<MapaTemplate>
    {
      return [
        new Default(), 
        new Light()
      ];
    }

    public getListaDeElementos() :Array<ElementoVisualizacao>
    {
      return ElementoVisualizacao.getLista();
    }

    public onElementoVisualizacaoClick(evento: React.MouseEvent<HTMLAnchorElement>, elemento: ElementoVisualizacao)
    {
      setTimeout(() => $('#botao-lista-elementos').addClass('open'), 0);

      let indiceElemento = this.state.elementosSelecionados.indexOf(elemento.tipo);
      let novoEstado :Array<ElementoVisualizacao.Tipo>;
      if(indiceElemento > -1)
      {
        this.state.elementosSelecionados.splice(indiceElemento, 1);
        novoEstado = [...this.state.elementosSelecionados];
      }
      else
      {
        novoEstado = [elemento.tipo, ...this.state.elementosSelecionados];        
      }

      this.setState({elementosSelecionados: novoEstado});
      if(this.props.onElementosVisiveisChange != null)
      {
        this.props.onElementosVisiveisChange(novoEstado);
      }
      
    }

    public render()
    {
      
        let listaTemplates = this.getListaDeTemplates();    
        let listaElementos = this.getListaDeElementos();

        return (
            <div className='row'>
                <div className='col-sm-12 box'>
                  <div className='box-content'>
                    <nav className='navbar navbar-default' role='navigation'>
                      <div className='navbar-header'>                        
                      </div>
                      <div className='collapse navbar-collapse navbar-ex1-collapse'>
                        <ul className='nav navbar-nav'>                          
                          <li className='dropup'>
                            <a className='dropdown-toggle' data-toggle='dropdown' href='#'>
                              Tema
                              <b className='caret'></b>
                            </a>
                            <ul className='dropdown-menu'>
                              {listaTemplates.map((template) => <li key={template.nomeTemplate}><a onClick={() => this.props.onTemaChange(template)}>{template.nomeTemplate}</a></li>)}                           
                            </ul>
                          </li>

                          <li className='dropup' id="botao-lista-elementos">
                            <a className='dropdown-toggle' data-toggle='dropdown' href='#'>
                              Elementos
                              <b className='caret'></b>
                            </a>
                            <ul className='dropdown-menu lista-checkboxes'>
                              {listaElementos.map((elemento) => (
                                <li key={elemento.tipo}>
                                  <a onClick={(evento) => this.onElementoVisualizacaoClick(evento, elemento)}>
                                    <input type="checkbox" checked={this.state.elementosSelecionados.indexOf(elemento.tipo) > -1}/>&nbsp;{elemento.descricao}
                                  </a>
                                </li>
                                ))}                                                
                            </ul>
                          </li>

                        </ul>                        
                        <ul className='nav navbar-nav navbar-right'>
                          <li>
                            <a><Relogio /></a>
                          </li>                          
                        </ul>
                      </div>
                    </nav>                   
                  </div>
                </div>
            </div>        
              
        );
    }

}

export namespace BarraInferior
{
    export interface BarraInferiorProps extends ReactProps
    {
        onTemaChange: (template: MapaTemplate) => void;
        onElementosVisiveisChange?: (elementosVisiveis: Array<ElementoVisualizacao.Tipo>) => void;
    }    

    export interface BarraInferiorState
    {
        elementosSelecionados: Array<ElementoVisualizacao.Tipo>;
    }   
}

