var isProduction = (process.env.NODE_ENV === "production");

var TsConfigPathsPlugin = require('awesome-typescript-loader').TsConfigPathsPlugin;
var EncodingPlugin = require('webpack-encoding-plugin');
var nodeExternals = require('webpack-node-externals');
var UglifyJsPlugin = require('uglifyjs-webpack-plugin');
var Visualizer = require('webpack-visualizer-plugin');
var packages = require('./package.json');
var webpack = require('webpack');

module.exports = {
    entry: {        
        MapaIntegradoLoader: "./workoffline/MapaIntegradoLoader.ts",
        CalendarioDePontosLoader: "./workoffline/CalendarioDePontosLoader.ts",        
        //vendor: Object.keys(packages.dependencies)        
    },
    output: {
        filename: "[name].js",
        path: __dirname + "/../js"
    },
    plugins: [
        new webpack.DefinePlugin({
            'process.env': {
                'NODE_ENV': JSON.stringify('production')
            }
        }),     
        new webpack.ProvidePlugin({
            $: "jquery",
            "window.jQuery": "jquery",
            "moment": "moment"
        }),                
        new webpack.optimize.CommonsChunkPlugin({
            name: 'vendor.MapaIntegradoLoader',
            chunks: ['MapaIntegradoLoader'],      
            minChunks: ({ resource }) => /node_modules/.test(resource)
        }),
        new webpack.optimize.CommonsChunkPlugin({
            name: 'vendor.CalendarioDePontosLoader',
            chunks: ['CalendarioDePontosLoader'],         
            minChunks: ({ resource }) => /node_modules/.test(resource)
        }),
        new UglifyJsPlugin({
            test: /\.js($|\?)/i
        }),
        new EncodingPlugin({ encoding: 'iso-8859-1' }),
        new Visualizer({ filename: 'webpack.statistics.html' })        
    ],

    // Enable sourcemaps for debugging webpack's output.
    devtool: "source-map",

    resolve: {        
        // Add '.ts' and '.tsx' as resolvable extensions.
        //modulesDirectories: ["workoffline", "node_modules"],
        extensions: [".ts", ".tsx", ".js", ".json"],
        plugins: [
            new TsConfigPathsPlugin(/* { tsconfig, compiler } */ )
        ],
    },

    module: {        
        rules: [

            // All files with a '.ts' or '.tsx' extension will be handled by 'awesome-typescript-loader'.
            { 
                test: /\.ts$/,
                loader: "awesome-typescript-loader"             
            },
            { 
                test: /\.tsx$/,
                loader: "awesome-typescript-loader"           
             },
             {
                test: /\.css$/,
                use: [ 'style-loader', 'css-loader' ]
             },

            // All output '.js' files will have any sourcemaps re-processed by 'source-map-loader'.
            { 
                enforce: "pre", 
                test: /\.js$/, 
                use: ["source-map-loader"] 
            }
        ]
    },

    // When importing a module whose path matches one of the following, just
    // assume a corresponding global variable exists and use that instead.
    // This is important because it allows us to avoid bundling all of our
    // dependencies, which allows browsers to cache those libraries between builds.
    //externals: [nodeExternals()]    
    externals: {
        "moment": 'moment',
        "jquery": 'jQuery'
        //"react": "React",
        //"react-dom": "ReactDOM"
    },

};