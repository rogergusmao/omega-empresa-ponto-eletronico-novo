import 'react-datetime/css/react-datetime.css';

import * as React from "react";
import * as Datetime from 'react-datetime';

import { Modal, FormGroup, ControlLabel, FormControl, HelpBlock, Button, Alert } from 'react-bootstrap';
import { DateRangePicker, FocusedInputShape, SingleDatePicker } from 'react-dates';
import * as MomentJS from 'moment';

import { ContextoGenerico } from 'lib/contexto';
import { ReactComponent } from 'lib/uiComponente';
import { PontoEletronico, Evento } from 'aplicacao/modulo/calendario/model';
import { TipoPontoEletronico, Helper } from 'aplicacao/comum/geral';
import { ReactProps, MomentHelper } from 'lib/geral';

export class CorrigirPonto extends ReactComponent<CorrigirPonto.CorrigirPontoProps, CorrigirPonto.CorrigirPontoState>
{
    protected constructor(props: CorrigirPonto.CorrigirPontoProps, reactContext: any)
    {
        super(props || {} as CorrigirPonto.CorrigirPontoProps, reactContext);

        this.show = this.show.bind(this);
        this.close = this.close.bind(this);            
        this.state = { show : false };                 
    }

    public close() :void 
    {
        this.setState({ show: false, dataPonto: null });
    }
    
    public show() :void
    {
        this.setState({ show: true, dataPonto: null });
    }

    public onSalvarDadosClick() :void
    {
        let dataHoraPonto = this.state.dataPonto;
        if(Helper.isNullOrEmpty(dataHoraPonto))
        {
            this.setState({dataEmBranco: true});
        }  
        else if((this.props.dataMinimaPermitida != null && dataHoraPonto.isBefore(this.props.dataMinimaPermitida))
                || (this.props.dataMaximaPermitida != null && dataHoraPonto.isAfter(this.props.dataMaximaPermitida)))
        {
            this.setState({dataInvalida: true});    
        }
        else
        {
            let novoPonto = new PontoEletronico();
            //grava no modelo o id do ponto de referencia
            novoPonto.idPonto = this.props.evento.pontoInicial.idPonto;
            novoPonto.idPessoa = this.props.evento.idPessoa;
            novoPonto.dataPonto = this.state.dataPonto;
            novoPonto.tipoPonto = this.props.evento.pontoInicial.tipoPonto == TipoPontoEletronico.PONTO_ENTRADA ? 
                                    TipoPontoEletronico.PONTO_SAIDA : TipoPontoEletronico.PONTO_ENTRADA;

            //fecha a modal
            this.close();

            //chama função no contexto da aplicação para persistir os dados
            this.props.onSalvarDadosClick(novoPonto);
        }                       
        
    }

    private isDayValid(date :MomentJS.Moment)
    {
        if(this.props.dataMinimaPermitida != null && date.isBefore(this.props.dataMinimaPermitida.clone().startOf('day')))
        {
            return false;
        }

        if(this.props.dataMaximaPermitida != null && date.isAfter(this.props.dataMaximaPermitida.clone().endOf('day')))
        {
            return false;
        }

        return true;
    }

    private corrigirControles() :void
    {
        setTimeout(function(){

            $('.rdtNext span').html('&gt;');
            $('.rdtPrev span').html('&lt;');
            $('.rdtCounters .rdtBtn:first-child').html('&xwedge;');
            $('.rdtCounters .rdtBtn:last-child').html('&xvee;');

        });        
    }
    
    public render()
    {
        //corrige caracteres do controle de data
        this.corrigirControles();

        debugger;
        let eventoCorrente = this.props.evento;
        let conteudoPontoExistente: JSX.Element[] | JSX.Element;
        let conteudoPontoASerCriado: JSX.Element[] | JSX.Element;

        let labelPontoExistente :string = null;
        let labelPontoASerCriado :string = null;
        if(eventoCorrente.pontoInicial.tipoPonto == TipoPontoEletronico.PONTO_ENTRADA)
        {
            labelPontoExistente = "Ponto de Entrada";
            labelPontoASerCriado = "Ponto de Saida";
        }
        else if(eventoCorrente.pontoInicial.tipoPonto == TipoPontoEletronico.PONTO_SAIDA)
        {
            labelPontoExistente = "Ponto de Saida";
            labelPontoASerCriado = "Ponto de Entrada";
        }

        let mensagemDatasMinimasMaximasPermtidas :JSX.Element[] = [];
        if(this.props.dataMinimaPermitida)
        {
            mensagemDatasMinimasMaximasPermtidas.push(<p className="mb-0">Menor data permitida: {MomentHelper.formatMomentToDatetimeExibicao(this.props.dataMinimaPermitida)}</p>)
        }

        if(this.props.dataMaximaPermitida)
        {
            mensagemDatasMinimasMaximasPermtidas.push(<p className="mb-0">Maior data permitida: {MomentHelper.formatMomentToDatetimeExibicao(this.props.dataMaximaPermitida)}</p>)
        }

        conteudoPontoExistente = (
            <div className="mb-3" key="ponto-existente">
                <div className="lead text-contrast mb-1">{labelPontoExistente}:</div>

                <address>
                    <strong>{eventoCorrente.pontoInicial.nomePessoa}</strong><br />              
                    Identificador do Ponto Eletrônico: {eventoCorrente.pontoInicial.idPonto}<br />              
                    Data: {eventoCorrente.pontoInicial.dataPonto.toDate().toLocaleDateString()}<br />
                    Hora: {eventoCorrente.pontoInicial.dataPonto.toDate().toLocaleTimeString()}<br /> 
                </address>

            </div>
        );

        conteudoPontoASerCriado = (
            <div key="ponto-a-ser-criado">
                <div className="lead text-contrast mb-1">{labelPontoASerCriado}:</div>

                <strong>{eventoCorrente.pontoInicial.nomePessoa}</strong><br />  
                Definir data e hora:
                <Datetime           
                    className="mb-1 datetime-picker"                
                    isValidDate={ this.isDayValid.bind(this) }
                    value={this.state.dataPonto}                    
                    onViewModeChange={() => {this.corrigirControles()}}
                    onFocus={() => {this.corrigirControles()}}
                    onChange={(selectedDate) => this.setState({ dataPonto: selectedDate as MomentJS.Moment })  }                       
                />
                {mensagemDatasMinimasMaximasPermtidas}
            </div>

        );

        let conteudoCorpo :JSX.Element[];
        if(eventoCorrente.pontoInicial.tipoPonto == TipoPontoEletronico.PONTO_ENTRADA)
        {
            conteudoCorpo = [conteudoPontoExistente, conteudoPontoASerCriado];
        }
        else if(eventoCorrente.pontoInicial.tipoPonto == TipoPontoEletronico.PONTO_SAIDA)
        {
            conteudoCorpo = [conteudoPontoASerCriado, conteudoPontoExistente];
        }        

        let mensagemValidacao :string = null;
        let mensagemValidacaoJSX :JSX.Element = null;
        if(this.state.dataEmBranco)
        {
            mensagemValidacao = "A data do ponto deve ser informada."
        }
        else if(this.state.dataInvalida)
        {
            if(this.props.dataMinimaPermitida != null && this.props.dataMaximaPermitida != null)
            {
                mensagemValidacao = `A data do ponto deve estar compreendida entre ${MomentHelper.formatMomentToDatetimeExibicao(this.props.dataMinimaPermitida)} e ${MomentHelper.formatMomentToDatetimeExibicao(this.props.dataMaximaPermitida)}.`;
            }
            else if(this.props.dataMinimaPermitida != null)
            {
                mensagemValidacao = `A data do ponto deve ser maior que ${MomentHelper.formatMomentToDatetimeExibicao(this.props.dataMinimaPermitida)}`;
            }
            else if(this.props.dataMaximaPermitida != null)
            {
                mensagemValidacao = `A data do ponto deve ser menor que ${MomentHelper.formatMomentToDatetimeExibicao(this.props.dataMaximaPermitida)}`;
            }
        }

        if(mensagemValidacao != null)
        {
            mensagemValidacaoJSX = (
                <Alert bsStyle="warning" className="mb-2">
                    {mensagemValidacao}
                </Alert>)
        }        

        return (
            <Modal
                show={this.state.show} 
                onHide={this.close} 
            >
                <Modal.Header closeButton>
                    Corrigir Ponto Eletrônico
                </Modal.Header>
                <Modal.Body>
                    {mensagemValidacaoJSX}
                    {conteudoCorpo}                
                </Modal.Body>
                <Modal.Footer>
                    <Button 
                        bsStyle="success"
                        onClick={this.onSalvarDadosClick.bind(this)}>
                        Salvar Dados
                    </Button>
                    <Button 
                        bsStyle="danger"
                        onClick={this.close.bind(this)}>
                        Cancelar
                    </Button>                    
                </Modal.Footer>
            </Modal>
        );
    }

}

export namespace CorrigirPonto
{
    export interface CorrigirPontoProps extends ReactProps
    {
        evento: Evento;
        dataMinimaPermitida?: MomentJS.Moment;
        dataMaximaPermitida?: MomentJS.Moment;
        onSalvarDadosClick: (pontoEletronico :PontoEletronico) => any;
    }

    export interface CorrigirPontoState
    {
        show: boolean,
        dataPonto?: MomentJS.Moment,
        dataEmBranco? : boolean | null;
        dataInvalida? : boolean | null;
    }

}
