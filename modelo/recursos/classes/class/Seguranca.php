<?php

    class Seguranca extends InterfaceSegurancaUsuario
    {

    //lista de paginas que nao precisam de autenticacao
    public static $paginasExcecao = array(
        "lembrar_senha", 
        "actions", 
        "lembrar_corporacao", 
        "corporacoes_do_usuario",
        "teste",
        "online",
        "gerar_id_avulso");
    
    //actions que nao precisam de autenticacao
    public static $actionsExcecao = array(
        "cadastrarUsuarioPontoEletronico",
        "cadastrarUsuarioECorporacaoPontoEletronico",
        "statusSincronizacaoBancoSqliteDaCorporacao",
        "procedimentoGerarBancoSqlite",
        "trocarUsuario",
        "alterarSenhaDoUsuario",
        "editarUsuario",
        "deletarUsuario",
        "cadastrarUsuario",
        "loginSICOB",
        "checaIntegridadeDeServicosDoUsuario",
        "loginUsuarioSICOB",
        "cadastraUsuarioECorporacao",
        "lembrarSenhaEGrupo", 
        "login", 
        "efetuarLogout", 
        "loginMobile",
        "marcaPontoAndroidXML", 
        "existeRelacionamentoComAPessoa",
        "removeCorporacao",
        "downloadBanco",
        "updateOperadoraEmpresa",
        "updateOperadoraPessoa",
        "receiveListaMensagemSMSPessoa",
        "receiveListaMensage",
        "receiveListaMensagemSMSPessoaComFalhaDeEnvio",
        "isAdmnistradorDaCorporacao",
        "sendSenhaToEmail",
        "isServerOnline",
        "getStrQueryInsertUsuarioMultipleAndroidDatabase",
        "sendMelhoriaToEmail",
        "sendErroToEmail",
        "receiveRemocaoUsuario",
        "receiveAlterarSenha",
        "isCorporacaoExistente",
        "receiveCadastroCorporacao",
        "updateStatusServicoDoUsuario",
        "disponibilizaVeiculoPorOutroUsuario",
        "disponibilizaTodoVeiculoOcupadoPeloUsuario",
        "sendUltimoIdSincronizadorAndroid",
        "receiveListUpdateDatabase",
        "receiveListRemoveDatabase",
        "receiveListInsertAndReturnIdDatabase",
        "receiveVetorQuerySelectAndReturnListIdTabelaDatabase",
        "sendListaInformacaoSincronizador",
        "getTotalTuplasRastrearRotaUsuarioAndroidDatabase",
        "getStrQueryInsertMultipleAndroidDatabase",
        "receiveEdicaoUsuario",
        "sendListQueryOfListIdSincronizador",
        "sincronizaTabelaAndroidDatabase", 
        "sincronizaTabelaCorporacaoAndroidDatabase",
        "receiveCadastroUsuarioECorporacao",
        "receiveTabelaAndroidDatabase", 
        "receiveVetorQueryInsertAndReturnListIdTabelaDatabase",
        "exportarImagemUsuarioFoto",
        "receiveLastTuplaUsuarioFoto",
        "receiveFoto",
        "receiveListaTuplaUsuarioFoto",
        "isEmailDoUsuarioExistente",
        "receiveListaTuplaUsuarioPosicao",
        "getListIdUsuarioFotoAndroidDatabase",
        "receiveQueryAndReturnIdDatabase",
        "receiveTabelaTarefaAndroidDatabase",
        "receiveListaIdServicoAtivo",
        "disponibilizaVeiculo",
        "removeAllPermissaoCategoriaPermissao",
        "receiveZipFileQuery", 
        "receiveTabelaVeiculoPosicaoAndroidDatabase",
        "receiveFileZipperAndroidDatabase",
        "getListIdOfReceiveZipFileQueryInsert",
        "getListIdOfReceiveTabelaAndroidDatabase",
        "receiveVetorQueryTabelaAndroidDatabase", 
        "receiveInformacaoUsuairo", 
        "receiveCadastroUsuario", 
        "isVeiculoDisponivel",
        "checkVeiculoUsuarioDaTarefa",
        "setDataDeInicioDeExcecucaoDaTarefa",
        "rastrearRotaUsuarioAndroidDatabase",
        "setDataDeFimDeExcecucaoDaTarefa",
        "exportarUltimaImagemDoUsuario",
        "editaUsuarioPeloSICOB",
        "getTotalDeUsuarios",
        "getEspacoOcupadoPeloBanco",
        "consultaUsuario",
        "lembrarCorporacoes",
        "loginCorporacao",
        "insertData",
        "getTesteInsert",
        "releaseApcLock",
        "getIdDaCorporacao",
        "cadastraUsuarioECorporacaoNaReserva",
        "cadastraAssinaturaDoClienteReservado",
        "clearCacheBanco",
        "autoLogin",
        "sugestaoDeMelhoria",
        "relatoDeErro",
        "teste",
        "getDashboard",
        "uploadLogErro");

        public static function getNomeCorporacao()
        {
            //TODO-EDUARDO: Remover Linha abaixo
            //return "TESTE201709083";

            return Helper::SESSION("usuario_nome_corporacao");
        }

        public static function setDonoDaCorporacao($isDonoDaCorporacao)
        {
            Helper::setSession("usuario_dono_da_corporacao", $isDonoDaCorporacao);
        }

        public static function isDonoDaCorporacao()
        {
            return Helper::SESSION("dono_da_corporacao");
        }

        public static function getIdDaCorporacaoLogada()
        {
            return Helper::SESSION("corporacao_id");
        }

        public static function getIdDaEmpresaLogada()
        {
            return Helper::SESSION("empresa_id");
        }

        public static function getNomeDaEmpresaLogada()
        {
            return Helper::SESSION("empresa");
        }

        public static function getNomeDoUsuarioLogado()
        {
            return Helper::SESSION("usuario_nome");
        }

        public static function isAutenticado()
        {
            $obj = Registry::get('Seguranca');

            return $obj->__isAutenticado();
        }

        public function getAcoesLiberadasParaUsoAutenticado()
        {
            return array('autoLogin');
        }

        public function __actionLoginSICOB($pCampo = null)
        {
            $campoCrypt = null;
            if ($pCampo == null)
            {
                $campoCrypt = Helper::POSTGET("pCampo");
            }
            else
            {
                $campoCrypt = $pCampo;
            }
            $objCrypt = Registry::get('Crypt');
            $campo = $objCrypt->decrypt($campoCrypt);

            $prefixo = "SICOB:";
            if (Helper::startsWith($campo, $prefixo))
            {
                $campo = str_replace($prefixo, "", $campo);
                $vetor = Helper::explode("/:/", $campo);

                if (count($vetor) != 2)
                {
                    $msgErro = urlencode("[erg3e4r5t] Criptografia inválida");

                    return array(DOMINIO_DE_ACESSO_SICOB . "index.php?msgErro=$msgErro");
                }
                else
                {
                    $email = $vetor[0];
                    $corporacao = $vetor[1];

                    $db = new Database();
                    $idUsuario = EXTDAO_Usuario::getIdUsuario($email, $db);

                    if (empty($idUsuario))
                    {
                        $msgErro = urlencode("[345456]Email n?o cadastrado do usu?rio '$email'.");
                        Helper::mudarLocation(DOMINIO_DE_ACESSO_SICOB . "index.php?msgErro=$msgErro");

                        return;
                    }
                    else
                    {
                        $objUsuario = new EXTDAO_Usuario();
                        $objUsuario->select($idUsuario);
                        $senhaCrypt = $objUsuario->getSenha();
                        $senha = $objCrypt->decrypt($senhaCrypt);

                        $msg = $this->login(
                            array(
                                "email" => $email,
                                "corporacao" => $corporacao
                            ),
                            $senha,
                            $db,
                            true);

                        if ($msg != null && !$msg->ok())
                        {
                            return array(DOMINIO_DE_ACESSO_SICOB . "index.php?msgErro=" . $msg->getMensagemUrl());
                        }
                        else
                        {
                            return array("index.php?corporacao=".urlencode( $corporacao));
                        }
                    }
                }
            }
            throw new Exception("Criptografia inválida");
        }

        public function __actionLogin($dados, $senha, $db = null)
        {
            try
            {
                if ($db == null)
                {
                    $db = new Database();
                }

                HelperLog::verbose("validarLogin::verificaSenha");
                $nextAction = Helper::POST("next_action");

                $mensagem = $this->login($dados, $senha, $db, true);
                if ($mensagem != null && $mensagem->ok())
                {
                    if (Helper::POST("next_action") == "fecharEAtualizar")
                    {
                        Helper::imprimirCabecalhoParaFormatarAction();
                        Helper::imprimirMensagem("Login realizado com sucesso, aguarde redirecionamento...", MENSAGEM_OK);
                        Helper::imprimirComandoJavascript("window.opener.location.reload(true);");
                        Helper::imprimirComandoJavascriptComTimer("window.close();", 3);
                        exit();
                    }
                    else
                    {
                        return array("index.php?msgSucesso=Login realizado com sucesso");
                    }
                }
                else
                {
                    $complementoGET = "";
                    if (Helper::POST("next_action") == "fecharEAtualizar")
                    {
                        $complementoGET = "next_action=fecharEAtualizar&";
                    }

                    return array("login.php?{$complementoGET}&msgErro=" . $mensagem->mMensagem);
                }
            }
            catch (Exception $exc)
            {
                return array("login.php?msgErro=" . urlencode($exc->getMessage()));
            }
        }

        public function __actionLogout($mensagemUsuario = null)
        {

            $this->logout();

            return array(DOMINIO_DE_ACESSO_SICOB . "login.php");
        }

        public function factory()
        {

            return new Seguranca();
        }

        public function isPaginaRestrita($pagina)
        {
            return !in_array($pagina, Seguranca::$paginasExcecao);
        }

        public function verificaSenha(
            $dados,
            $senha,
            $db = null,
            $retornarIdentificadorSessao = false)
        {
            try
            {
                $email = isset($dados["email"]) ? $dados["email"] : null;
                $corporacao = isset($dados["corporacao"]) ? $dados["corporacao"] : null;
                $idUsuario = isset($dados["id"]) ? $dados["id"] : null;
                $idCorporacao = isset($dados["id_corporacao"]) ? $dados["id_corporacao"] : null;

                if (!(strlen($email) || strlen($idUsuario))
                    || !strlen($senha)
                    || !(strlen($corporacao) || strlen($idCorporacao))
                )
                {
                    return new Mensagem(
                        PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR,
                        "Parâmetro invalido. $email ou $idUsuario, $senha, $corporacao ou $idCorporacao");
                }

                $criptografia = Registry::get('Crypt');
                $senhaC = $criptografia->crypt($senha);

                if ($db == null)
                {
                    $db = new Database();
                }

                $where = "";

                if (!empty($email))
                {
                    $email = Helper::strtolowerlatin1($email);
                    $where .= " u.email = '$email'";
                }
                else
                {
                    $where .= " u.id = '$idUsuario'";
                }
                $where2 = "";
                if (!empty($corporacao))
                {
                    $corporacao = Helper::strtoupperlatin1($corporacao);
                    $where2 = " c.nome = '$corporacao' ";
                }
                else
                {
                    $where2 = " c.id = '$idCorporacao' ";
                }

                $q = "SELECT u.senha AS senha,
                u.status_BOOLEAN AS status,
                uc.status_BOOLEAN AS statusC
                FROM usuario AS u 
                    LEFT JOIN usuario_corporacao uc ON u.id = uc.usuario_id_INT
                    LEFT JOIN corporacao c ON c.id = uc.corporacao_id_INT
                WHERE $where AND $where2";

                $msg = $db->queryMensagem($q);
                if ($msg != null && $msg->erro())
                {
                    return $msg;
                }
                $acessoLiberado = false;
                if ($db->rows() > 0)
                {
                    $objUsuario = Helper::getPrimeiroObjeto($db->result, 0, 1);
                    $senhaUsuario = $objUsuario[0];

                    $senhaDec = $criptografia->decrypt($senhaUsuario);

                    $acessoLiberado = $senhaDec == $senha;
//                    $senhaDec2 = $criptografia->decrypt($senhaC);
                    HelperLog::logDatabase("SenhaDecript [$senhaDec==$senha]");
//                    HelperLog::logDatabase("Senha $senhaUsuario == $senhaC");
                    if ($objUsuario[2] != 1)
                    {
                        return new Mensagem(
                            PROTOCOLO_SISTEMA::ACESSO_NEGADO,
                            "O usuário {0} está bloqueado para o grupo. Acesse o Gerenciamento de usuário na área restrita com outro usuário do grupo para libera-lo!",
                            $email);
                    }
                }
                else
                {
                    return new Mensagem(
                        PROTOCOLO_SISTEMA::ACESSO_NEGADO,
                        "Não existe nenhum usuário com o {0}. Você pode cadastra-lo!",
                        $email);
                }

                if ($acessoLiberado)
                {
                    //verifica status do usuario - ativo/inativo
                    if ($objUsuario[1] == 1)
                    {

                        //Verificando se a corporacao eh pertinente ao usuario

                        $q = "SELECT DISTINCT u.id as usuario_id,
                            u.nome as usuario_nome,
                            c.id as usuario_id_corporacao,
                            u.email as usuario_email,
                            t.nome AS usuario_tipo,
                            t.nome_visivel AS usuario_tipo_visivel,
                            t.id AS usuario_id_tipo,
                            t.pagina_inicial AS usuario_tipo_pagina_inicial,
                            c.nome AS usuario_nome_corporacao,
                            e.id AS usuario_id_empresa,
                            e.nome AS usuario_empresa,
                            c.id corporacao_id,
                            uc.status_BOOLEAN ucstatus
                        FROM usuario u
                          JOIN usuario_corporacao uc ON u.id = uc.usuario_id_INT
                          JOIN usuario_tipo_corporacao utc ON utc.usuario_id_INT = u.id
                          JOIN corporacao c ON c.id = uc.corporacao_id_INT
                          JOIN usuario_tipo t ON t.id = utc.usuario_tipo_id_INT
                          LEFT OUTER JOIN usuario_empresa AS ue ON ue.usuario_id_INT=u.id
                          LEFT OUTER JOIN empresa AS e ON e.id=ue.empresa_id_INT
                             
                        WHERE $where
                            AND $where2";

                        $msg = $db->queryMensagem($q);
                        if ($msg != null && $msg->erro())
                        {
                            return $msg;
                        }
                        //se o resultado nao encontrou a tupla em questao, entoa o login nao eh validado
                        if ($db->rows() == 0)
                        {
                            return new Mensagem(
                                PROTOCOLO_SISTEMA::ACESSO_NEGADO,
                                "O usuário {0} não é membro do grupo {1}. Se quiser adiciona-lo acesse a área administrativa do seu grupo e cadastre.",
                                $email,
                                $corporacao);
                        }
                        if ($retornarIdentificadorSessao)
                        {
                            $objUsuario = $objsCliente = Helper::getPrimeiroObjeto($db->result, 1, 0);

                            $objUsuario["usuario_permissoes"] = $this->getArrayDeFuncionalidadesDoUsuario(
                                $objUsuario["usuario_id"],
                                $db,
                                array("corporacao_id" => $objUsuario["corporacao_id"]));

                            $objMenu = new Menu();
                            $objUsuario["usuario_menu"] = $objMenu->getArrayDoMenuDoUsuarioLogado(
                                $objUsuario["usuario_id"],
                                $objUsuario["usuario_id_tipo"],
                                $db);

                            return new Mensagem_protocolo(
                                $objUsuario,
                                PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO,
                                "Login realizado com suceso.");
                        }
                        else
                        {
                            return new Mensagem(
                                PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO,
                                "Login realizado com suceso.");
                        }
                    }
                    else
                    {
                        return new Mensagem(
                            PROTOCOLO_SISTEMA::ACESSO_NEGADO,
                            "O usuário com email {0} está bloqueado. Acesse a área administrativa com outro usuário do grupo para desbloquea-lo!",
                            $email);
                    }
                }
                else
                {
                    return new Mensagem(PROTOCOLO_SISTEMA::ACESSO_NEGADO,
                                        "Senha inválida do usuário");
                }
            }
            catch (Exception $exc)
            {
                return new Mensagem(null, null, $exc);
            }
        }

        public function isAcaoRestrita($classe, $acao)
        {
            return false;
            if (!in_array($acao, Seguranca::$actionsExcecao))
            {
                return true;
            }

            return false;
        }

        public function getParametrosLogin()
        {
            return array(
                "email" => Helper::POST("txtLogin"),
                "corporacao" => Helper::POST("corporacao")
            );
        }
        public function loginWeb($email, $corporacao, $senha, $db=null){
            if($db==null)$db= new  Database();
            $msg = $this->login(
                array(
                    "email" => $email,
                    "corporacao" => $corporacao
                ),
                $senha,
                $db,
                true);
            return $msg;
        }
    }

