import { RemoteRequest, RemoteResponse } from "lib/ajax";

export class RemoteDataService
{
    protected scriptName : string;
    protected remoteDefaultClassName: string;

    public constructor();
    public constructor(scriptName? :string)
    {
        this.scriptName = scriptName || "webservice.php";
    }

    public getServiceUrl(methodName :string, className? :string) :string
    {
        className = className || this.remoteDefaultClassName;
        return this.scriptName + "?" + "class=" + className + "&action=" + methodName;
    }

    public static async getAsyncItem<T>(url :string, parameters?: any): Promise<T>
    {
        var request = new RemoteRequest(url, parameters);
        const remoteResponse = await request.getItem<T>();

        if(RemoteDataService.isValidResponse(remoteResponse))
        {
            var promise = new Promise<T>((resolve, reject) =>
            {
                resolve(remoteResponse.mObj);
            });

            return promise;
        }
        else
        {
            throw new Error(remoteResponse && remoteResponse.mMensagem  ? remoteResponse.mMensagem : "Erro ao recuperar dados remotos.");
        }
    }

    public static async getAsyncList<T>(url :string, parameters?: any): Promise<Array<T>>
    {
        var request = new RemoteRequest(url, parameters);
        const remoteResponse = await request.getList<T>();

        if(RemoteDataService.isValidResponse(remoteResponse))
        {
            return new Promise<Array<T>>((resolve, reject) =>
            {
                resolve(remoteResponse.mObj);
            });
        }
        else
        {
            throw new Error(remoteResponse && remoteResponse.mMensagem  ? remoteResponse.mMensagem : "Erro ao recuperar dados remotos.");
        }
    }

    public static getDefaultValidResponseCodRetorno() : Array<number>
    {
        return [0, -1];
    }

    public static isValidResponse<T>(response: RemoteResponse<T>, validReturnCodes? : Array<number>) :boolean
    {
        validReturnCodes = validReturnCodes || RemoteDataService.getDefaultValidResponseCodRetorno();
        if(validReturnCodes.indexOf(response.mCodRetorno) > -1)
        {
            return true;
        }
        else
        {
            return false;
        }

    }

}