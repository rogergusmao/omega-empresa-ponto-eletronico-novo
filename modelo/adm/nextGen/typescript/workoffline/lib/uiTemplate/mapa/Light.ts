import {MapaTemplate} from './MapaTemplate';

export class Light extends MapaTemplate
{
    public constructor()
    {
        super("Light");
    }

    public getStyles(): any
    {
        return [{
            "featureType": "landscape.man_made",
            "elementType": "geometry",
            "stylers": [{"color": "#f7f1df"}]
        }, 
        {
            "featureType": "landscape.natural",
            "elementType": "geometry",
            "stylers": [{"color": "#d0e3b4"}]
        }, 
        {
            "featureType": "landscape.natural.terrain",
            "elementType": "geometry",
            "stylers": [{"visibility": "off"}]
        }, 
        {
            "featureType": "poi",
            "stylers": [{"hue": "#FF0300"}, {"saturation": -100}, {"lightness": 25}, {"gamma": 1}]
        },        
        {
            "featureType": "road",
            "elementType": "geometry.stroke",
            "stylers": [{"visibility": "off"}]
        }, 
        {
            "featureType": "road",
            "elementType": "labels",
            "stylers": [{"visibility": "off"}]
        }, 
        {
            "featureType": "road.highway",
            "elementType": "geometry.fill",
            "stylers": [{"color": "#ffe15f"}]
        }, 
        {
            "featureType": "road.highway",
            "elementType": "geometry.stroke",
            "stylers": [{"color": "#efd151"}]
        }, 
        {
            "featureType": "road.arterial",
            "elementType": "geometry.fill",
            "stylers": [{"color": "#ffffff"}]
        }, 
        {
            "featureType": "road.local",
            "elementType": "geometry.fill",
            "stylers": [{"color": "black"}]
        }, 
        {
            "featureType": "transit.station.airport",
            "elementType": "geometry.fill",
            "stylers": [{"color": "#cfb2db"}]
        }, {"featureType": "water", "elementType": "geometry", "stylers": [{"color": "#a2daf2"}]}]
    }
}