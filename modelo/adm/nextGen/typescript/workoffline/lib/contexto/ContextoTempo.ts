import { Tempo } from "lib/geral";
import { ContextoMapa } from 'lib/contexto';

export abstract class ContextoTempo extends ContextoMapa
{
    public currentDateTime: Tempo;
    public constructor(domRootSelector: string, mapContainerSelector :string, currentDateTime: Tempo)
    {
        super(domRootSelector, mapContainerSelector);
        this.currentDateTime = currentDateTime;
    }

}