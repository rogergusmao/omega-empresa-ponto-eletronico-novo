<?php

/*

Arquivo gerado atravï¿½s de gerador de cï¿½digo em 30/05/2017 as 02:18:02.
Para que o arquivo nï¿½o seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

Tabela correspondente: usuario_tipo_corporacao
Sobrescrita de dados: ---SOBRESCRITA_NAO_AUTORIZADA---

*/
?>

<?php

class EXTDAO_Usuario_tipo_corporacao extends DAO_Usuario_tipo_corporacao
{
    public function __construct($configDAO = null)
    {
        parent::__construct($configDAO);
        $this->nomeClasse = "EXTDAO_Usuario_tipo_corporacao";
    }

    public function factory()
    {
        return new EXTDAO_Usuario_tipo_corporacao();
    }

    public static function getIdUsuarioTipoCorporacao($idUsuario, $idCorporacao, $idUsuarioTipo = null, $db = null)
    {
        $q = "SELECT id "
            . " FROM usuario_tipo_corporacao "
            . " WHERE usuario_id_INT = $idUsuario"
            . " AND corporacao_id_INT = $idCorporacao ";
        if ($idUsuarioTipo != null)
        {
            $idUsuarioTipo .= " AND usuario_tipo_id_INT = $idUsuarioTipo ";
        }
        if ($db == null)
        {
            $db = new Database();
        }
        $db->query($q);

        return $db->getPrimeiraTuplaDoResultSet(0);
    }

    public static function getIdUsuarioTipo($idUsuario, $idCorporacao, $db = null)
    {
        $q = "SELECT usuario_tipo_id_INT "
            . " FROM usuario_tipo_corporacao "
            . " WHERE usuario_id_INT = $idUsuario"
            . " AND corporacao_id_INT = $idCorporacao ";

        if ($db == null)
        {
            $db = new Database();
        }
        $db->query($q);

        return $db->getPrimeiraTuplaDoResultSet(0);
    }
}

?>
