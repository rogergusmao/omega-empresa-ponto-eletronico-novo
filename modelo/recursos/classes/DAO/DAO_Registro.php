<?php


        /*

        Arquivo gerado através de gerador de código em 12/05/2018 as 14:49:59.
        Para que o arquivo não seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

        Tabela correspondente: registro
        Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

        */
        


class DAO_Registro extends Generic_DAO
                        {
                            const NOME_ENTIDADE = "registro";

                            public static $nomeTabela = null;
                            public static $idSistemaTabela = false;
                            public static $databaseFieldsRelatedAttributes = null;
	                        public static $databaseFieldTypes = null;
	                        public static $databaseFieldNames = null;
	                        public static $listAliasTypes = null;
	                        public static $listAliasRelatedAttributes = null;
	                        
	                        public $id;
public $protocoloInt;
public $cadastroSec;
public $cadastroOffsec;
public $responsavelUsuarioId;
public $responsavelCategoriaPermissaoId;
public $descricao;
public $tipoRegistroId;
public $registroEstadoId;
public $registroEstadoCorporacaoId;
public $idOrigemChamadaInt;
public $corporacaoId;

public $labelId;
public $labelProtocoloInt;
public $labelCadastroSec;
public $labelCadastroOffsec;
public $labelResponsavelUsuarioId;
public $labelResponsavelCategoriaPermissaoId;
public $labelDescricao;
public $labelTipoRegistroId;
public $labelRegistroEstadoId;
public $labelRegistroEstadoCorporacaoId;
public $labelIdOrigemChamadaInt;
public $labelCorporacaoId;

public static $arrPK = array("id");

public function __construct($configDAO = null)
        {
            parent::__construct($configDAO);
            static::__setStaticValues($this->database);           
        }

public static function idSistemaTabela($databaseObject = null){
            if(!(static::$idSistemaTabela))
            {
                static::$idSistemaTabela = EXTDAO_Sistema_tabela::getIdDaSistemaTabela(static::nomeTabela(), $databaseObject);
            }
            return static::$idSistemaTabela;
        }

public function getIdSistemaTabela(){
            return static::idSistemaTabela();
        }

public static function nomeTabela(){
            if(is_null(static::$nomeTabela))
            {
                static::$nomeTabela = "registro";
            }
            return static::$nomeTabela;
        }

public function getNomeTabela(){
            return static::nomeTabela();
        }

public static function __setStaticValues($databaseObject = null)
                   {    
                        static::nomeTabela();
                        
                        static::idSistemaTabela($databaseObject);
                   
                        static::setDatabaseFieldNames();
                        static::setDatabaseFieldTypes();
                        static::setDatabaseFieldsRelatedAttributeNames();
                        
                        static::setListAliasTypes();
                        static::setListAliasRelatedAttributes();
                   }

public function getExpressaoLabel()
                   {
                        return "{id}";
                   }

public static function setDatabaseFieldNames()
        {
            if(is_null(static::$databaseFieldNames))
            {
                static::$databaseFieldNames = new stdClass();
                static::$databaseFieldNames->id = "id";
static::$databaseFieldNames->protocoloInt = "protocolo_INT";
static::$databaseFieldNames->cadastroSec = "cadastro_SEC";
static::$databaseFieldNames->cadastroOffsec = "cadastro_OFFSEC";
static::$databaseFieldNames->responsavelUsuarioId = "responsavel_usuario_id_INT";
static::$databaseFieldNames->responsavelCategoriaPermissaoId = "responsavel_categoria_permissao_id_INT";
static::$databaseFieldNames->descricao = "descricao";
static::$databaseFieldNames->tipoRegistroId = "tipo_registro_id_INT";
static::$databaseFieldNames->registroEstadoId = "registro_estado_id_INT";
static::$databaseFieldNames->registroEstadoCorporacaoId = "registro_estado_corporacao_id_INT";
static::$databaseFieldNames->idOrigemChamadaInt = "id_origem_chamada_INT";
static::$databaseFieldNames->corporacaoId = "corporacao_id_INT";

            }
                      
        }

public static function setDatabaseFieldTypes()
        {
            if(is_null(static::$databaseFieldTypes))
            {
                static::$databaseFieldTypes = new stdClass();
                static::$databaseFieldTypes->id = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->protocoloInt = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->cadastroSec = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$databaseFieldTypes->cadastroOffsec = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$databaseFieldTypes->responsavelUsuarioId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->responsavelCategoriaPermissaoId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->descricao = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->tipoRegistroId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->registroEstadoId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->registroEstadoCorporacaoId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->idOrigemChamadaInt = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->corporacaoId = static::TIPO_VARIAVEL_INTEGER;

            }
            
        }

public static function setDatabaseFieldsRelatedAttributeNames()
        {
            if(is_null(static::$databaseFieldsRelatedAttributes))
            {
                static::$databaseFieldsRelatedAttributes = new stdClass();
                static::$databaseFieldsRelatedAttributes->id = "id";
static::$databaseFieldsRelatedAttributes->protocolo_INT = "protocoloInt";
static::$databaseFieldsRelatedAttributes->cadastro_SEC = "cadastroSec";
static::$databaseFieldsRelatedAttributes->cadastro_OFFSEC = "cadastroOffsec";
static::$databaseFieldsRelatedAttributes->responsavel_usuario_id_INT = "responsavelUsuarioId";
static::$databaseFieldsRelatedAttributes->responsavel_categoria_permissao_id_INT = "responsavelCategoriaPermissaoId";
static::$databaseFieldsRelatedAttributes->descricao = "descricao";
static::$databaseFieldsRelatedAttributes->tipo_registro_id_INT = "tipoRegistroId";
static::$databaseFieldsRelatedAttributes->registro_estado_id_INT = "registroEstadoId";
static::$databaseFieldsRelatedAttributes->registro_estado_corporacao_id_INT = "registroEstadoCorporacaoId";
static::$databaseFieldsRelatedAttributes->id_origem_chamada_INT = "idOrigemChamadaInt";
static::$databaseFieldsRelatedAttributes->corporacao_id_INT = "corporacaoId";

            }           
        }

public function setAllFieldsToHumanFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["protocolo_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["cadastro_SEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["cadastro_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["responsavel_usuario_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["responsavel_categoria_permissao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["descricao"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["tipo_registro_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["registro_estado_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["registro_estado_corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["id_origem_chamada_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
}

public function setAllFieldsToDatabaseFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["protocolo_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["cadastro_SEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["cadastro_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["responsavel_usuario_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["responsavel_categoria_permissao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["descricao"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["tipo_registro_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["registro_estado_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["registro_estado_corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["id_origem_chamada_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
}

  public function getFkObjResponsavel_usuario() 
                {
                    if($this->objResponsavel_usuario == null)
                    {                        
                        $this->objResponsavel_usuario = new EXTDAO_Responsavel_usuario_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getResponsavel_usuario_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objResponsavel_usuario->clear();
                    }
                    elseif($this->objResponsavel_usuario->getId() != $idFK)
                    {
                        $this->objResponsavel_usuario->select($idFK);
                    }
                    return $this->objResponsavel_usuario;
                }
  public function getFkObjResponsavel_categoria_permissao() 
                {
                    if($this->objResponsavel_categoria_permissao == null)
                    {                        
                        $this->objResponsavel_categoria_permissao = new EXTDAO_Responsavel_categoria_permissao_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getResponsavel_categoria_permissao_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objResponsavel_categoria_permissao->clear();
                    }
                    elseif($this->objResponsavel_categoria_permissao->getId() != $idFK)
                    {
                        $this->objResponsavel_categoria_permissao->select($idFK);
                    }
                    return $this->objResponsavel_categoria_permissao;
                }
  public function getFkObjTipo_registro() 
                {
                    if($this->objTipo_registro == null)
                    {                        
                        $this->objTipo_registro = new EXTDAO_Tipo_registro_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getTipo_registro_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objTipo_registro->clear();
                    }
                    elseif($this->objTipo_registro->getId() != $idFK)
                    {
                        $this->objTipo_registro->select($idFK);
                    }
                    return $this->objTipo_registro;
                }
  public function getFkObjRegistro_estado() 
                {
                    if($this->objRegistro_estado == null)
                    {                        
                        $this->objRegistro_estado = new EXTDAO_Registro_estado_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getRegistro_estado_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objRegistro_estado->clear();
                    }
                    elseif($this->objRegistro_estado->getId() != $idFK)
                    {
                        $this->objRegistro_estado->select($idFK);
                    }
                    return $this->objRegistro_estado;
                }
  public function getFkObjRegistro_estado_corporacao() 
                {
                    if($this->objRegistro_estado_corporacao == null)
                    {                        
                        $this->objRegistro_estado_corporacao = new EXTDAO_Registro_estado_corporacao_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getRegistro_estado_corporacao_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objRegistro_estado_corporacao->clear();
                    }
                    elseif($this->objRegistro_estado_corporacao->getId() != $idFK)
                    {
                        $this->objRegistro_estado_corporacao->select($idFK);
                    }
                    return $this->objRegistro_estado_corporacao;
                }
  public function getFkObjCorporacao() 
                {
                    if($this->objCorporacao == null)
                    {                        
                        $this->objCorporacao = new EXTDAO_Corporacao_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getCorporacao_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objCorporacao->clear();
                    }
                    elseif($this->objCorporacao->getId() != $idFK)
                    {
                        $this->objCorporacao->select($idFK);
                    }
                    return $this->objCorporacao;
                }


public function setLabels()
        {$this->labelId = "";
$this->labelProtocoloInt = "";
$this->labelCadastroSec = "";
$this->labelCadastroOffsec = "";
$this->labelResponsavelUsuarioId = "";
$this->labelResponsavelCategoriaPermissaoId = "";
$this->labelDescricao = "";
$this->labelTipoRegistroId = "";
$this->labelRegistroEstadoId = "";
$this->labelRegistroEstadoCorporacaoId = "";
$this->labelIdOrigemChamadaInt = "";
$this->labelCorporacaoId = "";
}

public function __actionAdd($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                $this->setByObject($parameters);
                $this->setDataCadastroAndOffsetInSecondsFromUser($parameters);
                
                $this->formatarParaSQL();
                $msg = $this->insert(true);
                
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                return new Mensagem(null, I18N::getExpression("Registro adicionado com sucesso."));
                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionEdit($parameters = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try {

                $this->setByObject($parameters);
                $this->setAllFieldsToHumanFormat();
                
                $this->formatarParaSQL();
                $msg = $this->updateByInstanceUsingParameters($this->id, $parameters, true);
                if($msg != null && $msg->erro())
                {
                     return $msg;
                }
        
                return new Mensagem(null, I18N::getExpression("Registro editado com sucesso."));
            }
            catch(Exception $ex)
            {                
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionRemove($parameters = null)
        {
            $successulRemovalCount = 0;
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                foreach($parameters as $record)
                {
                    if(is_numeric($record->id))
                    {
                        $msg = $this->delete($record->id, true);
                        if($msg == null || $msg->ok())
                        {
                            $successulRemovalCount++;
                        }
                    }
                }
                
                if($successulRemovalCount > 0)
                {
                    $successMessage = count($successulRemovalCount) > 1 ? I18N::getExpression("{0} Registro foram removidos com sucesso.", $successulRemovalCount) : I18N::getExpression("Registro removido com sucesso.");
                    return new Mensagem(null, $successMessage);
                }
                else
                {
                    $errorMessage = count($parameters) > 1 ? I18N::getExpression("Falha ao remover Registro.") : I18N::getExpression("Falha ao remover Registro.");
                    return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, $errorMessage);                    
                }
            }
            catch (Exception $ex) 
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }

public function __getRecord($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }
            
            try
            {            
                $returnObject = new stdClass();            
                $whereClause = static::getWhereClauseForFilter($parameters->filterParameters);
                $query = "SELECT  id, protocolo_INT, cadastro_SEC, cadastro_OFFSEC, responsavel_usuario_id_INT, responsavel_categoria_permissao_id_INT, descricao, tipo_registro_id_INT, registro_estado_id_INT, registro_estado_corporacao_id_INT, id_origem_chamada_INT, corporacao_id_INT FROM registro {$whereClause}";
    
                $msg = $this->database->queryMensagem($query);                
                if ($msg == null)
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Não foi encontrado nenhum registro"));
                    }                    
                    $resultSet = $this->database->result;
                    $resultObject = static::mapResultSetDataToRecord($resultSet);
                    
                    $returnObject->formData = $resultObject;
                    $returnObject->comboBoxesData = null;
                    if($parameters->loadComboBoxesData)
                    {
                        $this->setByObject($resultObject);
                        $returnObject->comboBoxesData =  $this->getFormComboBoxesData();
                    }                    
    
                    
    
                    return new Mensagem_generica($returnObject);
                } 
                else 
                {
                    return $msg;
                }
                
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }


        
        public function __getFilterComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
                
        public function __getFormComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
        
        protected function getFormComboBoxesData()
        {
            $comboBoxesData = new stdClass();
            $listParameters = new stdClass();
            $listParameters->listMappingType = static::LIST_MAPPING_TYPE_COMBOBOX;
            
            
                    
                    $objTipoRegistro = new EXTDAO_Tipo_registro();
                    $comboBoxesData->fieldTipoRegistroId = $objTipoRegistro->__getList($listParameters);
                    
                    $objRegistroEstado = new EXTDAO_Registro_estado();
                    $comboBoxesData->fieldRegistroEstadoId = $objRegistroEstado->__getList($listParameters);
                    
                    $objRegistroEstadoCorporacao = new EXTDAO_Registro_estado_corporacao();
                    $comboBoxesData->fieldRegistroEstadoCorporacaoId = $objRegistroEstadoCorporacao->__getList($listParameters);

            return $comboBoxesData;
        }
        
        


        
        public static function setListAliasTypes()
        {
            if(is_null(static::$listAliasTypes))
            {
                static::$listAliasTypes = new stdClass();
                static::$listAliasTypes->tipo_registro__nome = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->registro_estado__nome = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->registro_estado_corporacao__nome = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->registro__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->registro__protocolo_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->registro__cadastro_SEC = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$listAliasTypes->registro__responsavel_usuario_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->registro__responsavel_categoria_permissao_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->registro__descricao = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->registro__nome = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->registro__tipo_registro_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->registro__corporacao_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->registro__id_origem_chamada_INT = static::TIPO_VARIAVEL_INTEGER;
            }        
        }
        
        public static function setListAliasRelatedAttributes()
        {
            if(is_null(static::$listAliasRelatedAttributes))
            {
                static::$listAliasRelatedAttributes = new stdClass();
                static::$listAliasRelatedAttributes->tipo_registro__nome = "tipoRegistroNome";
static::$listAliasRelatedAttributes->registro_estado__nome = "registroEstadoNome";
static::$listAliasRelatedAttributes->registro_estado_corporacao__nome = "registroEstadoCorporacaoNome";
static::$listAliasRelatedAttributes->registro__id = "id";
static::$listAliasRelatedAttributes->registro__protocolo_INT = "protocoloInt";
static::$listAliasRelatedAttributes->registro__cadastro_SEC = "cadastroSec";
static::$listAliasRelatedAttributes->registro__responsavel_usuario_id_INT = "responsavelUsuarioId";
static::$listAliasRelatedAttributes->registro__responsavel_categoria_permissao_id_INT = "responsavelCategoriaPermissaoId";
static::$listAliasRelatedAttributes->registro__descricao = "descricao";
static::$listAliasRelatedAttributes->registro__nome = "nome";
static::$listAliasRelatedAttributes->registro__tipo_registro_id_INT = "tipoRegistroId";
static::$listAliasRelatedAttributes->registro__corporacao_id_INT = "corporacaoId";
static::$listAliasRelatedAttributes->registro__id_origem_chamada_INT = "idOrigemChamadaInt";
            }         
        }

        public function __getList($parameters = null, $idCorporacao = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try
            {
                $filterParameters = new stdClass();
                if(isset($parameters->filterParameters))
                {
                    $filterParameters = $parameters->filterParameters;
                }
                                    
                            
            if(is_null($idCorporacao))
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            ListParameters::addFilterParameterToObject($filterParameters, "corporacaoId", $idCorporacao);           
                
                
                $sortingParameters = null;
                if(isset($parameters->sortingParameters))
                {
                    $sortingParameters = $parameters->sortingParameters;
                }
                
                $paginationParameters = null;
                if(isset($parameters->paginationParameters))
                {
                    $paginationParameters = $parameters->paginationParameters;
                }
                
                $listMappingType = static::getListMappingType($parameters->listMappingType);
                $mainTableAlias = "r";

                $whereClause = static::getWhereClauseForFilter($filterParameters, $mainTableAlias);
                $orderByClause = static::getOrderByClauseForFilter($sortingParameters, $mainTableAlias);
                $limitClause = static::getLimitClauseForFilter($paginationParameters);

                $queryWithoutLimit = "SELECT r.id FROM registro r {$whereClause}";
                $query = "SELECT tr.nome AS tipo_registro__nome, re.nome AS registro_estado__nome, rec.nome AS registro_estado_corporacao__nome, r.id AS registro__id, r.protocolo_INT AS registro__protocolo_INT, r.cadastro_SEC AS registro__cadastro_SEC, r.responsavel_usuario_id_INT AS registro__responsavel_usuario_id_INT, r.responsavel_categoria_permissao_id_INT AS registro__responsavel_categoria_permissao_id_INT, r.descricao AS registro__descricao, r.nome AS registro__nome, r.tipo_registro_id_INT AS registro__tipo_registro_id_INT, r.corporacao_id_INT AS registro__corporacao_id_INT, r.id_origem_chamada_INT AS registro__id_origem_chamada_INT FROM registro r LEFT JOIN tipo_registro tr ON tr.id = r.tipo_registro_id_INT LEFT JOIN registro_estado re ON re.id = r.registro_estado_id_INT LEFT JOIN registro_estado_corporacao rec ON rec.id = r.registro_estado_corporacao_id_INT {$whereClause} {$orderByClause} {$limitClause}";

                $msg = $this->database->queryMensagem($queryWithoutLimit);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                
                $totalNumberOfRecords = $this->database->rows();
                if($totalNumberOfRecords == 0)
                {
                    return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                }                        
                        
                $msg = $this->database->queryMensagem($query);
                
                //operacao realizaca com sucesso
                if ($msg == null) 
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                    }                        
                
                    $resultSet = $this->database->result;

                    $objReturn = new stdClass();
                    $objReturn->dataSet = static::mapResultSetDataToList($resultSet, $listMappingType, static::$listAliasTypes);

                    if(!Helper::isNullOrEmpty($paginationParameters->recordsPerPage) && !Helper::isNullOrEmpty($paginationParameters->currentPage))
                    {
                        $lastPage = ceil($totalNumberOfRecords / $paginationParameters->recordsPerPage);
                        $objReturn->paginationParameters = new PaginationParams($paginationParameters->recordsPerPage, $paginationParameters->currentPage, $lastPage);
                    }
                    
                    return new Mensagem_generica($objReturn);
                } 
                else
                {
                    return $msg;
                }                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }



public function getId()
            {
                return $this->id;
            }

public function getProtocoloInt()
            {
                return $this->protocoloInt;
            }

public function getProtocolo_INT()
                {
                    return $this->protocoloInt;
                }

public function getCadastroSec()
            {
                return $this->cadastroSec;
            }

public function getCadastro_SEC()
                {
                    return $this->cadastroSec;
                }

public function getCadastroOffsec()
            {
                return $this->cadastroOffsec;
            }

public function getCadastro_OFFSEC()
                {
                    return $this->cadastroOffsec;
                }

public function getResponsavelUsuarioId()
            {
                return $this->responsavelUsuarioId;
            }

public function getResponsavel_usuario_id_INT()
                {
                    return $this->responsavelUsuarioId;
                }

public function getResponsavelCategoriaPermissaoId()
            {
                return $this->responsavelCategoriaPermissaoId;
            }

public function getResponsavel_categoria_permissao_id_INT()
                {
                    return $this->responsavelCategoriaPermissaoId;
                }

public function getDescricao()
            {
                return $this->descricao;
            }

public function getTipoRegistroId()
            {
                return $this->tipoRegistroId;
            }

public function getTipo_registro_id_INT()
                {
                    return $this->tipoRegistroId;
                }

public function getRegistroEstadoId()
            {
                return $this->registroEstadoId;
            }

public function getRegistro_estado_id_INT()
                {
                    return $this->registroEstadoId;
                }

public function getRegistroEstadoCorporacaoId()
            {
                return $this->registroEstadoCorporacaoId;
            }

public function getRegistro_estado_corporacao_id_INT()
                {
                    return $this->registroEstadoCorporacaoId;
                }

public function getIdOrigemChamadaInt()
            {
                return $this->idOrigemChamadaInt;
            }

public function getId_origem_chamada_INT()
                {
                    return $this->idOrigemChamadaInt;
                }

public function getCorporacaoId()
            {
                return $this->corporacaoId;
            }

public function getCorporacao_id_INT()
                {
                    return $this->corporacaoId;
                }



function setId($value)
            {
                $this->id = $value;
            }

function setProtocoloInt($value)
            {
                $this->protocoloInt = $value;
            }

public function setProtocolo_INT($value)
                { 
                    $this->protocoloInt = $value; 
                }

function setCadastroSec($value)
            {
                $this->cadastroSec = $value;
            }

public function setCadastro_SEC($value)
                { 
                    $this->cadastroSec = $value; 
                }

function setCadastroOffsec($value)
            {
                $this->cadastroOffsec = $value;
            }

public function setCadastro_OFFSEC($value)
                { 
                    $this->cadastroOffsec = $value; 
                }

function setResponsavelUsuarioId($value)
            {
                $this->responsavelUsuarioId = $value;
            }

public function setResponsavel_usuario_id_INT($value)
                { 
                    $this->responsavelUsuarioId = $value; 
                }

function setResponsavelCategoriaPermissaoId($value)
            {
                $this->responsavelCategoriaPermissaoId = $value;
            }

public function setResponsavel_categoria_permissao_id_INT($value)
                { 
                    $this->responsavelCategoriaPermissaoId = $value; 
                }

function setDescricao($value)
            {
                $this->descricao = $value;
            }

function setTipoRegistroId($value)
            {
                $this->tipoRegistroId = $value;
            }

public function setTipo_registro_id_INT($value)
                { 
                    $this->tipoRegistroId = $value; 
                }

function setRegistroEstadoId($value)
            {
                $this->registroEstadoId = $value;
            }

public function setRegistro_estado_id_INT($value)
                { 
                    $this->registroEstadoId = $value; 
                }

function setRegistroEstadoCorporacaoId($value)
            {
                $this->registroEstadoCorporacaoId = $value;
            }

public function setRegistro_estado_corporacao_id_INT($value)
                { 
                    $this->registroEstadoCorporacaoId = $value; 
                }

function setIdOrigemChamadaInt($value)
            {
                $this->idOrigemChamadaInt = $value;
            }

public function setId_origem_chamada_INT($value)
                { 
                    $this->idOrigemChamadaInt = $value; 
                }

function setCorporacaoId($value)
            {
                $this->corporacaoId = $value;
            }

public function setCorporacao_id_INT($value)
                { 
                    $this->corporacaoId = $value; 
                }



public function clear()
        {$this->id = null;
$this->protocoloInt = null;
$this->cadastroSec = null;
$this->cadastroOffsec = null;
$this->responsavelUsuarioId = null;
if($this->objResponsavel_usuario != null) unset($this->objResponsavel_usuario);
$this->responsavelCategoriaPermissaoId = null;
if($this->objResponsavel_categoria_permissao != null) unset($this->objResponsavel_categoria_permissao);
$this->descricao = null;
$this->tipoRegistroId = null;
if($this->objTipo_registro != null) unset($this->objTipo_registro);
$this->registroEstadoId = null;
if($this->objRegistro_estado != null) unset($this->objRegistro_estado);
$this->registroEstadoCorporacaoId = null;
if($this->objRegistro_estado_corporacao != null) unset($this->objRegistro_estado_corporacao);
$this->idOrigemChamadaInt = null;
$this->corporacaoId = null;
if($this->objCorporacao != null) unset($this->objCorporacao);
if(is_array($this->arrEstadosCampos)){
                        unset($this->arrEstadosCampos);
                     $this->arrEstadosCampos = array();
                    }
                }

public function formatarParaSQL()
        {
if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->id)){
$this->id = $this->formatarIntegerParaComandoSQL($this->id);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->id);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->protocoloInt)){
$this->protocoloInt = $this->formatarIntegerParaComandoSQL($this->protocoloInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->protocoloInt);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->cadastroSec)){
$this->cadastroSec = $this->formatarIntegerParaComandoSQL($this->cadastroSec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->cadastroSec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->cadastroOffsec)){
$this->cadastroOffsec = $this->formatarIntegerParaComandoSQL($this->cadastroOffsec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->cadastroOffsec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->responsavelUsuarioId)){
$this->responsavelUsuarioId = $this->formatarIntegerParaComandoSQL($this->responsavelUsuarioId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->responsavelUsuarioId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->responsavelCategoriaPermissaoId)){
$this->responsavelCategoriaPermissaoId = $this->formatarIntegerParaComandoSQL($this->responsavelCategoriaPermissaoId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->responsavelCategoriaPermissaoId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->descricao)){
$this->descricao = $this->formatarStringParaComandoSQL($this->descricao);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->descricao);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->tipoRegistroId)){
$this->tipoRegistroId = $this->formatarIntegerParaComandoSQL($this->tipoRegistroId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->tipoRegistroId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->registroEstadoId)){
$this->registroEstadoId = $this->formatarIntegerParaComandoSQL($this->registroEstadoId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->registroEstadoId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->registroEstadoCorporacaoId)){
$this->registroEstadoCorporacaoId = $this->formatarIntegerParaComandoSQL($this->registroEstadoCorporacaoId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->registroEstadoCorporacaoId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->idOrigemChamadaInt)){
$this->idOrigemChamadaInt = $this->formatarIntegerParaComandoSQL($this->idOrigemChamadaInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->idOrigemChamadaInt);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->corporacaoId)){
$this->corporacaoId = $this->formatarIntegerParaComandoSQL($this->corporacaoId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->corporacaoId);
}

}

public function formatarParaExibicao()
        {if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->descricao)){
$this->descricao = $this->formatarStringParaExibicao($this->descricao);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->descricao);
}

}

function select($id, $idCorporacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }   

            $sql = "SELECT id, protocolo_INT, cadastro_SEC, cadastro_OFFSEC, responsavel_usuario_id_INT, responsavel_categoria_permissao_id_INT, descricao, tipo_registro_id_INT, registro_estado_id_INT, registro_estado_corporacao_id_INT, id_origem_chamada_INT, corporacao_id_INT FROM registro WHERE id = $id AND corporacao_id_INT = $idCorporacao";

            $msg = $this->database->queryMensagem($sql);
            $result = $this->database->result;

            if($msg != null && $msg->erro() )
            { 
            		$this->clear();
            		return $msg;
        	} 
        	else if($this->database->rows() == 0)
        	{
                return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nenhum registro foi encontrado."));
            }
        

            $row = $this->database->fetchArray(MYSQLI_NUM);
            
            		$this->id = $row[0];
		$this->protocoloInt = $row[1];
		$this->cadastroSec = $row[2];
		$this->cadastroOffsec = $row[3];
		$this->responsavelUsuarioId = $row[4];
		$this->responsavelCategoriaPermissaoId = $row[5];
		$this->descricao = $row[6];
		$this->tipoRegistroId = $row[7];
		$this->registroEstadoId = $row[8];
		$this->registroEstadoCorporacaoId = $row[9];
		$this->idOrigemChamadaInt = $row[10];

            
        }

public function delete($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (is_null($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }

            $sql = "DELETE FROM registro WHERE id= $id AND corporacao_id_INT = $idCorporacao";
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                    && $sincronizar
                        && static::idSistemaTabela() != null
                            && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_REMOVE,
                                null,
                                null,
                                $id,
                                $this->database);
            }

            return $msg;
        }

public function insert($sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null, $opcoes = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $this->defineDataCadastroInSecondsIfNotDefined();
$this->defineDataCadastroOffsetInSecondsIfNotDefined();

            
            if (is_null($opcoes))
            {
                $opcoes = $this->opcoesDAO;
            }

            if (!is_numeric($this->id))
            {
                $this->id = "null";
                if ($opcoes == null || $opcoes[Database::OPCAO_GERAR_ID] != false) 
                {
                    $this->id = EXTDAO_Sistema_sequencia::gerarId(static::nomeTabela());                    
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }    
                    else if($msg == null)
                    {
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
                else 
                {
                    $this->id = null;
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao));
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }
                    else if($msg == null)
                    {
                        $this->id = $this->database->getLastInsertId();
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
            }
            else 
            {
                $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }                        
                else if($msg == null)
                {
                   $msg = new Mensagem_token();
                   $msg->mValor = $this->id;
                }
            }
            if($msg == null || $msg->ok())
            {                
                if($sincronizar
                    && static::idSistemaTabela() != null
                    && isset($this->id)){
                    
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_INSERT,
                                null,
                                null,
                                $this->id,
                                $this->database); 
                	}
            } 
            else
            {
                $this->id = null;
            }
            
            return $msg;
            
        }

public function getSQLInsert($idCorporacao){ 

return "INSERT INTO registro (id, protocolo_INT, cadastro_SEC, cadastro_OFFSEC, responsavel_usuario_id_INT, responsavel_categoria_permissao_id_INT, descricao, tipo_registro_id_INT, registro_estado_id_INT, registro_estado_corporacao_id_INT, id_origem_chamada_INT, corporacao_id_INT) VALUES ( $this->id ,  $this->protocoloInt ,  $this->cadastroSec ,  $this->cadastroOffsec ,  $this->responsavelUsuarioId ,  $this->responsavelCategoriaPermissaoId ,  $this->descricao ,  $this->tipoRegistroId ,  $this->registroEstadoId ,  $this->registroEstadoCorporacaoId ,  $this->idOrigemChamadaInt ,  $idCorporacao ) "; 

}




        
        public function updateByParameters($id, $objParametros, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao))
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();

            
                
                if (isset($objParametros->protocoloInt)) 
                {
                    $arrUpdateFields[] = " protocolo_INT = {$objParametros->protocoloInt} ";
                }


                
                if (isset($objParametros->cadastroSec)) 
                {
                    $arrUpdateFields[] = " cadastro_SEC = {$objParametros->cadastroSec} ";
                }


                
                if (isset($objParametros->cadastroOffsec)) 
                {
                    $arrUpdateFields[] = " cadastro_OFFSEC = {$objParametros->cadastroOffsec} ";
                }


                
                if (isset($objParametros->responsavelUsuarioId)) 
                {
                    $arrUpdateFields[] = " responsavel_usuario_id_INT = {$objParametros->responsavelUsuarioId} ";
                }


                
                if (isset($objParametros->responsavelCategoriaPermissaoId)) 
                {
                    $arrUpdateFields[] = " responsavel_categoria_permissao_id_INT = {$objParametros->responsavelCategoriaPermissaoId} ";
                }


                
                if (isset($objParametros->descricao)) 
                {
                    $arrUpdateFields[] = " descricao = {$objParametros->descricao} ";
                }


                
                if (isset($objParametros->tipoRegistroId)) 
                {
                    $arrUpdateFields[] = " tipo_registro_id_INT = {$objParametros->tipoRegistroId} ";
                }


                
                if (isset($objParametros->registroEstadoId)) 
                {
                    $arrUpdateFields[] = " registro_estado_id_INT = {$objParametros->registroEstadoId} ";
                }


                
                if (isset($objParametros->registroEstadoCorporacaoId)) 
                {
                    $arrUpdateFields[] = " registro_estado_corporacao_id_INT = {$objParametros->registroEstadoCorporacaoId} ";
                }


                
                if (isset($objParametros->idOrigemChamadaInt)) 
                {
                    $arrUpdateFields[] = " id_origem_chamada_INT = {$objParametros->idOrigemChamadaInt} ";
                }



            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE registro SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";

            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null)
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function update($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($this->protocoloInt)) 
                {                                      
                    $arrUpdateFields[] = " protocolo_INT = {$this->protocoloInt} ";
                }


                
                if (isset($this->cadastroSec)) 
                {                                      
                    $arrUpdateFields[] = " cadastro_SEC = {$this->cadastroSec} ";
                }


                
                if (isset($this->cadastroOffsec)) 
                {                                      
                    $arrUpdateFields[] = " cadastro_OFFSEC = {$this->cadastroOffsec} ";
                }


                
                if (isset($this->responsavelUsuarioId)) 
                {                                      
                    $arrUpdateFields[] = " responsavel_usuario_id_INT = {$this->responsavelUsuarioId} ";
                }


                
                if (isset($this->responsavelCategoriaPermissaoId)) 
                {                                      
                    $arrUpdateFields[] = " responsavel_categoria_permissao_id_INT = {$this->responsavelCategoriaPermissaoId} ";
                }


                
                if (isset($this->descricao)) 
                {                                      
                    $arrUpdateFields[] = " descricao = {$this->descricao} ";
                }


                
                if (isset($this->tipoRegistroId)) 
                {                                      
                    $arrUpdateFields[] = " tipo_registro_id_INT = {$this->tipoRegistroId} ";
                }


                
                if (isset($this->registroEstadoId)) 
                {                                      
                    $arrUpdateFields[] = " registro_estado_id_INT = {$this->registroEstadoId} ";
                }


                
                if (isset($this->registroEstadoCorporacaoId)) 
                {                                      
                    $arrUpdateFields[] = " registro_estado_corporacao_id_INT = {$this->registroEstadoCorporacaoId} ";
                }


                
                if (isset($this->idOrigemChamadaInt)) 
                {                                      
                    $arrUpdateFields[] = " id_origem_chamada_INT = {$this->idOrigemChamadaInt} ";
                }


            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE registro SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function updateByInstanceUsingParameters($id, $parameters, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($parameters->protocoloInt)) 
                {                                      
                    $arrUpdateFields[] = " protocolo_INT = {$this->protocoloInt} ";
                }
                
                if (isset($parameters->cadastroSec)) 
                {                                      
                    $arrUpdateFields[] = " cadastro_SEC = {$this->cadastroSec} ";
                }
                
                if (isset($parameters->cadastroOffsec)) 
                {                                      
                    $arrUpdateFields[] = " cadastro_OFFSEC = {$this->cadastroOffsec} ";
                }
                
                if (isset($parameters->responsavelUsuarioId)) 
                {                                      
                    $arrUpdateFields[] = " responsavel_usuario_id_INT = {$this->responsavelUsuarioId} ";
                }
                
                if (isset($parameters->responsavelCategoriaPermissaoId)) 
                {                                      
                    $arrUpdateFields[] = " responsavel_categoria_permissao_id_INT = {$this->responsavelCategoriaPermissaoId} ";
                }
                
                if (isset($parameters->descricao)) 
                {                                      
                    $arrUpdateFields[] = " descricao = {$this->descricao} ";
                }
                
                if (isset($parameters->tipoRegistroId)) 
                {                                      
                    $arrUpdateFields[] = " tipo_registro_id_INT = {$this->tipoRegistroId} ";
                }
                
                if (isset($parameters->registroEstadoId)) 
                {                                      
                    $arrUpdateFields[] = " registro_estado_id_INT = {$this->registroEstadoId} ";
                }
                
                if (isset($parameters->registroEstadoCorporacaoId)) 
                {                                      
                    $arrUpdateFields[] = " registro_estado_corporacao_id_INT = {$this->registroEstadoCorporacaoId} ";
                }
                
                if (isset($parameters->idOrigemChamadaInt)) 
                {                                      
                    $arrUpdateFields[] = " id_origem_chamada_INT = {$this->idOrigemChamadaInt} ";
                }
            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE registro SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }

}

?>
