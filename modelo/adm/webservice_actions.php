<?php
require_once '../recursos/php/configuracao_biblioteca_compartilhada.php';

require_once $workspaceRaiz . DIRETORIO_BIBLIOTECAS_COMPARTILHADAS . '/imports/header_json.php';

ConfiguracaoCorporacaoDinamica::init(getWebservicesSemCache());

require_once '../adm/imports/header.php';
require_once '../recursos/php/funcoes.php';

require_once '../adm/imports/lingua.php';

$script = new WebserviceActionsPadrao();
$script->render();
