/*

Arquivo gerado atrav�s de gerador de c�digo em 21/01/2018 as 18:27:38.
Para que o arquivo n�o seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

Tabela correspondente: cidade
Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

*/

omegaApp.generatedFormCidadeController = function ($scope, $rootScope, $element, $window, $interval, $timeout, dataService, Upload, vm)
{
    try
    {
        vm.rootControllerElement = $element;
        vm.fieldsTypes = {};

        vm.isModalView = false;
        vm.isEdition = false;
        vm.entityIdForEdition = null;

        vm.messages = {
            error: [],
            warning: [],
            info: [],
            success: []
        };

        vm.formData = {
            nome: null,
            ufId: null
        };

        vm.disabledFields = {
            fieldNome: false,
            fieldUfId: false
        };

        vm.requiredFields = {
            fieldNome: true,
            fieldUfId: true
        };

        vm.relatedEntities =
            {};

        vm.fieldsParameters = {
            fieldNome: null,
            fieldUfId: {remoteData: null}
        };

        vm.arrForeignKeys = ['ufId', 'corporacaoId'];

        var relationshipObject = {ufId: 'fieldUfId'};

        vm.isInitialLoadFinished = false;
        var initialLoading = function ()
        {
            $timeout(function ()
            {
                if (AngularControllerUtil.isEditionMode(vm))
                {

                    var filterParameters = AngularControllerUtil.getFilterParametersForEdition(vm);
                    dataService.getCidade(filterParameters).then(function (responseContent)
                    {
                        if (RemoteDataUtil.validateRemoteResponse(responseContent, [PROTOCOLO_SISTEMA.OPERACAO_REALIZADA_COM_SUCESSO]))
                        {
                            var responseObject = RemoteDataUtil.getResponseObject(responseContent);
                            RemoteDataUtil.refactorInitialFormLoadRemoteResponseData(vm, responseObject, relationshipObject);
                            OmegaFormUtil.expandSectionPanels(vm);
                        }
                        else
                        {
                            RemoteDataUtil.defaultRemoteErrorHandling(responseContent, vm);
                        }

                        $timeout(function ()
                        {
                            vm.isInitialLoadFinished = true;
                        }, 100);

                    });
                }
                else
                {
                    dataService.getAllComboBoxesDataForCidadeForm().then(function (returnContent)
                    {
                        if (RemoteDataUtil.validateRemoteResponse(returnContent))
                        {
                            var responseObject = RemoteDataUtil.getResponseObject(returnContent);
                            RemoteDataUtil.refactorInitialFormLoadRemoteResponseData(vm, responseObject, relationshipObject);
                        }
                        else
                        {
                            LogUtil.logRemoteResponseError(returnContent);
                        }

                        $timeout(function ()
                        {
                            vm.isInitialLoadFinished = true;
                        }, 100);

                    });
                }

            }, 200);

        };

        vm.submitForm = function (formInstance)
        {
            if ($rootScope.validateForm(formInstance, vm))
            {
                var postData = RemoteDataUtil.refactorRemoteRequest(vm.formData, vm.relatedEntities);
                if (AngularControllerUtil.isEditionMode(vm))
                {
                    dataService.editCidade(postData).then(function (returnContent)
                    {
                        if (RemoteDataUtil.validateRemoteResponse(returnContent))
                        {
                            var successMessage = RemoteDataUtil.getResponseMessageString(returnContent);
                            $rootScope.defaultAfterUpdateAction(vm, 'after-cidade-update-template.html', successMessage);
                        }
                        else
                        {
                            RemoteDataUtil.defaultRemoteErrorHandling(returnContent, vm);
                        }
                    });
                }
                else
                {
                    dataService.addCidade(postData).then(function (returnContent)
                    {
                        if (RemoteDataUtil.validateRemoteResponse(returnContent))
                        {
                            var successMessage = RemoteDataUtil.getResponseMessageString(returnContent);
                            $rootScope.defaultAfterInsertAction(vm, 'after-cidade-insert-template.html', successMessage);
                        }
                        else
                        {
                            RemoteDataUtil.defaultRemoteErrorHandling(returnContent, vm);
                        }
                    });
                }
            }

        };

        vm.openUfIdModal = function ()
        {
            var modalIndex = $rootScope.getNextModalIndex();
            $rootScope.registerModalInstance($rootScope.showModalDialog('forms', 'uf', modalIndex));

            //atualizando valores da lista
            var unregisterModal = $rootScope.$watch(function ()
            {
                return $rootScope.uibModalInstance[modalIndex]
            }, function ()
            {

                if ($rootScope.uibModalInstance[modalIndex] == null)
                {
                    vm.getListOfUfId();
                    vm.formData.ufId = null;
                    unregisterModal();
                }

            });

        };

        vm.getListOfUfId = function ()
        {
            if (!vm.isInitialLoadFinished)
            {
                return;
            }

            dataService.getListOfUfForCidadeForm().then(function (returnContent)
            {
                if (RemoteDataUtil.validateRemoteResponse(returnContent))
                {
                    var remoteDataSet = RemoteDataUtil.getResponseObjectDataSetOrEmptyArray(returnContent);
                    vm.fieldsParameters.fieldUfId.remoteData = remoteDataSet;
                }
                else
                {
                    LogUtil.logRemoteResponseError(returnContent);
                }

            });

        };

        vm.loadListOfCidade = function ()
        {
            document.location.href = 'index.php?tipo=lists&page=cidade';
        };

        vm.addNewCidade = function ()
        {
            document.location.href = document.location.href;
        };

        return {

            scope: this,
            initFunction: initialLoading

        };

    }
    catch (ex)
    {
        ApplicationUtil.showErrorMessage(ex.message, 'Erro no controller generatedFormCidadeController', vm);
    }

}