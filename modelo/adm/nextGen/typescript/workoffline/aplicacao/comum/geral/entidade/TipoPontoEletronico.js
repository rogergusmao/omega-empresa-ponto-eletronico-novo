"use strict";
exports.__esModule = true;
var TipoPontoEletronico;
(function (TipoPontoEletronico) {
    TipoPontoEletronico[TipoPontoEletronico["PONTO_ENTRADA"] = 0] = "PONTO_ENTRADA";
    TipoPontoEletronico[TipoPontoEletronico["PONTO_SAIDA"] = 1] = "PONTO_SAIDA";
})(TipoPontoEletronico = exports.TipoPontoEletronico || (exports.TipoPontoEletronico = {}));
