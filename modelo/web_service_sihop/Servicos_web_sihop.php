<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Servicos_web
 *
 * @author home
 */
class Servicos_web_sihop
{
    //Como chamar algum webservice da classe:
    //http://127.0.0.1/hospedagem/10001/adm/actions.php?class=Servicos_web&action=

    public function __construct()
    {
    }

    public static function factory()
    {
        return new Servicos_web_sihop();
    }

    public static function acessarDetalhesAssinatura()
    {
        $acaoAoCarregarAPagina = Helper::POSTGET("acao");
        $emailUsuarioDesejado = Helper::POSTGET("email_usuario");

        return BO_SICOB::acessarDetalhesAssinatura($acaoAoCarregarAPagina, $emailUsuarioDesejado);
    }

    public static function trocarPacote()
    {
        return BO_SICOB::trocarPacote();
    }

    public static function logarPeloSistema()
    {
        return BO_SICOB::logarPeloSistema();
    }

    public static function getCaracteristicasDoPacoteDaCorporacao()
    {
        return BO_SICOB::getCaracteristicasDoPacoteDaCorporacao();
    }

    public static function isServidorOnline()
    {
        echo "TRUE";
    }

    public static function editaUsuarioPeloSICOB()
    {
        $campo = Helper::POSTGET("pCampo");

        $mensagemRet = BO_Usuario::editaUsuarioPeloSICOB($campo);

        return $mensagemRet;
    }

    //http://127.0.0.1/PontoEletronico/10001/public_html/adm/actions.php?class=Servicos_web_sihop&action=getTotalDeUsuarios
    public static function getTotalDeUsuarios()
    {
        $idCorporacao = Helper::POSTGET("id_corporacao");
        $mensagem = null;
        if (!empty($idCorporacao))
        {
            $obj = new BO_Usuario();
            $mensagem = $obj->getTotalDeUsuarios($idCorporacao);
        }
        else
        {
            $mensagem = Mensagem::factoryParametroInvalido();
        }

        return $mensagem;
    }

    //http://127.0.0.1/PontoEletronico/10001/public_html/adm/actions.php?class=Servicos_web_sihop&action=getEspacoOcupadoPeloBanco
    public static function getEspacoOcupadoPeloBanco()
    {
        $idCorporacao = Helper::POSTGET("id_corporacao");
        $mensagem = null;
        if (!empty($idCorporacao))
        {
            $obj = new BO_Database();
            $mensagem = $obj->getEspacoOcupadoPeloBanco($idCorporacao);
        }
        else
        {
            $mensagem = Mensagem::factoryParametroInvalido();
        }

        return $mensagem;
    }

//        http://127.0.0.1/PontoEletronico/10001/public_html/adm/actions.php?class=Servicos_web_sihop&action=consultaUsuario&email=roger%40omegasoftware.com.br
    public static function consultaUsuario()
    {
        $email = Helper::POSTGET("email");

        $email = urldecode($email);

        $obj = new BO_Usuario();
        $mensagemRet = $obj->consultaUsuario($email);

        return $mensagemRet;
    }

    //http://127.0.0.1/PontoEletronico/10001/public_html/adm/actions.php?class=Servicos_web_sihop&action=loginCorporacao&email=roger%40omegasoftware.com.br
    public static function loginCorporacao()
    {
        $usuario = Helper::POSTGET("txtLogin");
        $corporacao = Helper::POSTGET("txtCorporacao");
        $senha = Helper::POSTGET("txtSenha");
        $idSistema = ID_SISTEMA_SIHOP;

        $mensagem = BO_SIHOP::loginCorporacao(
            $usuario,
            $corporacao,
            $senha,
            $idSistema);

        return $mensagem;
    }

    public static function loginUsuarioSICOB()
    {
        $campo = Helper::POSTGET("pCampo");

        if (!empty($campo))
        {
            $objSeguranca = new Seguranca();
            $msg = $objSeguranca->__actionLoginSICOB($campo);
            return $msg;
        }
        else
        {
            $msg = new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, "Par�metro inv�lido");
            return $msg;
        }
    }

    public static function cadastrarUsuario()
    {
        //http://127.0.0.1/PontoEletronico/10001/public_html/adm/actions.php?class=Servicos_web_sihop&action=cadastraUsuarioECorporacao&email=teste%40teste.com.br&corporacao=teste&senha=123456&nome=teste&
        $campoC = Helper::POSTGET("pCampo");
        $crypt = new Crypt();
        $retorno = $crypt->decryptGet($campoC);

        if ($retorno == null)
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, "Par�metro inv�lido 1");
        }
        //$chaves = $retorno[0];
        $valores = $retorno['valores'];

        $emailResponsavel = $valores['email_responsavel'];
        $senhaResponsavel = $valores['senha_responsavel'];
        $nome = $valores['nome'];
        $email = $valores['email'];
        $corporacao = $valores['corporacao'];
        $senha = $valores['senha'];
        $idPrograma = $valores['idPrograma'];
        $imei = $valores['imei'];

        $obj = new Seguranca();
        $mensagem = $obj->verificaSenha(
            array("email" => $emailResponsavel, "corporacao" => $corporacao),
            $senhaResponsavel,
            null,
            true);

        if ($mensagem == null || $mensagem->ok())
        {
            $mensagem = BO_Usuario::cadastrarUsuario(
                $emailResponsavel,
                $senhaResponsavel,
                $nome,
                $email,
                $corporacao,
                $senha,
                $idPrograma,
                $imei);
        }

        return $mensagem;
    }

    public static function deletarUsuario()
    {
        //http://127.0.0.1/PontoEletronico/10001/public_html/adm/actions.php?class=Servicos_web_sihop&action=cadastraUsuarioECorporacao&email=teste%40teste.com.br&corporacao=teste&senha=123456&nome=teste&
        $campoC = Helper::POSTGET("pCampo");
        $crypt = new Crypt();
        $retorno = $crypt->decryptGet($campoC);

        if ($retorno == null)
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, "Par�metro inv�lido 1");
        }

        $valores = $retorno['valores'];

        $emailResponsavel = $valores['email_responsavel'];
        $senhaResponsavel = $valores['senha_responsavel'];
        $email = $valores['email'];
        $corporacao = $valores['corporacao'];
        $idPrograma = $valores['idPrograma'];
        $imei = $valores['imei'];

        $obj = new Seguranca();
        $mensagem = $obj->verificaSenha(
            array("email" => $emailResponsavel, "corporacao" => $corporacao),
            $senhaResponsavel,
            null,
            true);
        if ($mensagem == null || $mensagem->ok())
        {
            $mensagem = BO_Usuario::deletarUsuario(
                $emailResponsavel,
                $senhaResponsavel,
                $email,
                $corporacao,
                $idPrograma,
                $imei);
        }

        return $mensagem;
    }

    public static function editarUsuario()
    {
        //http://127.0.0.1/PontoEletronico/10001/public_html/adm/actions.php?class=Servicos_web_sihop&action=cadastraUsuarioECorporacao&email=teste%40teste.com.br&corporacao=teste&senha=123456&nome=teste&
        $campoC = Helper::POSTGET("pCampo");
        $crypt = new Crypt();
        $retorno = $crypt->decryptGet($campoC);

        if ($retorno == null)
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, "Par�metro inv�lido 1");
        }

        $valores = $retorno['valores'];
        $emailResponsavel = $valores['email_responsavel'];
        $senhaResponsavel = $valores['senha_responsavel'];
        $email = $valores['email'];
        $nome = $valores['nome'];
        $corporacao = $valores['corporacao'];
        $idPrograma = $valores['idPrograma'];
        $imei = $valores['imei'];

        $obj = new Seguranca();
        $mensagem = $obj->verificaSenha(
            array("email" => $emailResponsavel, "corporacao" => $corporacao),
            $senhaResponsavel,
            null,
            true);
        if ($mensagem == null || $mensagem->ok())
        {
            $mensagem = BO_Usuario::editarUsuario(
                $emailResponsavel,
                $senhaResponsavel,
                $email,
                $nome,
                $corporacao,
                $idPrograma,
                $imei);
        }

        return $mensagem;
    }

    public static function trocarUsuario()
    {
        $campoC = Helper::POSTGET("pCampo");
        $crypt = new Crypt();
        $retorno = $crypt->decryptGet($campoC);

        if ($retorno == null)
        {
            $mensagem = new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, "Par�metro inv�lido 1");
        }
        $valores = $retorno['valores'];
        $corporacao = $valores['corporacao'];
        $emailAntigo = $valores['email_antigo'];
        $senhaAntiga = $valores['senha_antiga'];
        $nomeNovo = $valores['nome_novo'];
        $emailNovo = $valores['email_novo'];
        $senhaNova = $valores['senha_nova'];
        $imei = $valores['imei'];
        $idPrograma = $valores['id_programa'];
        $emailResponsavel = $valores['email_responsavel'];
        $senhaResponsavel = $valores['senha_responsavel'];
        if (!strlen($emailNovo)
            || !strlen($emailResponsavel)
            || !strlen($senhaResponsavel)
            || !strlen($corporacao)
            || !strlen($senhaNova)
            || !strlen($emailAntigo)
            || !strlen($senhaAntiga)
            || !strlen($nomeNovo)
            || !strlen($senhaNova))
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, "Par�metros inv�lidos");
        }

        $obj = new Seguranca();

        $mensagem = $obj->verificaSenha(
            array("email" => $emailResponsavel, "corporacao" => $corporacao),
            $senhaResponsavel,
            null,
            true);
        if ($mensagem == null || $mensagem->ok())
        {
            $mensagem = BO_Usuario::trocarUsuario(
                $emailResponsavel,
                $senhaResponsavel,
                $corporacao,
                $emailAntigo,
                $senhaAntiga,
                $nomeNovo,
                $emailNovo,
                $senhaNova,
                $imei,
                $idPrograma);
        }

        return $mensagem;
    }

    public static function alterarSenhaDoUsuario()
    {
//            http://127.0.0.1/PontoEletronico/10001/public_html/adm/actions.php?class=Servicos_web_sihop&action=cadastraUsuarioECorporacao&email=teste%40teste.com.br&corporacao=teste&senha=123456&nome=teste&

        $campoC = Helper::POSTGET("pCampo");
        $mensagem = BO_Usuario::alterarSenhaDoUsuario(
            $campoC);

        return $mensagem;
    }

    public static function cadastraUsuarioECorporacao()
    {
        $campoC = Helper::POSTGET("pCampo");
        //TODO otimizar a funcao e remover a linha abaixo
        set_time_limit(0);
        $crypt = new Crypt();
        $retorno = $crypt->decryptGet($campoC);

        if ($retorno == null)
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, "Par�metro inv�lido 1");
        }

        $valores = $retorno['valores'];

        $emailResponsavel = $valores['email_responsavel'];
        $senhaResponsavel = $valores['senha_responsavel'];
        $nome = $valores['nome'];
        $email = $valores['email'];
        $corporacao = $valores['corporacao'];
        $idCorporacao = $valores['id_corporacao'];
        $senha = $valores['senha'];
        $idPrograma = isset($valores['idPrograma']) ? $valores['idPrograma'] : null;
        $imei = $valores['imei'];

        $mensagem = null;

        if ($mensagem == null
            || $mensagem->ok())
        {
            $mensagem = BO_Usuario::cadastraUsuarioECorporacao(
                $nome,
                $email,
                $corporacao,
                $senha,
                $idPrograma,
                $imei,
                $idCorporacao);
        }
        return $mensagem;
    }

    public function gerarIdInterna()
    {
        try
        {
            $tabela = Helper::POSTGET("tabela");
            $totGerar = Helper::POSTGET("tot_gerar");
            $id = EXTDAO_Sistema_sequencia::gerarIdInterna($tabela, $totGerar);
            return new Mensagem_token(null, null, $id);
        }
        catch (Exception $exc)
        {
            return new Mensagem(null, null, $exc);
        }
    }

    public static function cadastraUsuarioECorporacaoNaReserva()
    {
        $campoC = Helper::POSTGET("pCampo");

        //TODO otimizar a funcao e remover a linha abaixo
        set_time_limit(0);
        $crypt = new Crypt();
        $retorno = $crypt->decryptGet($campoC);

        if ($retorno == null)
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, "Par�metro inv�lido 1");
        }

        $valores = $retorno['valores'];

        $emailResponsavel = $valores['email_responsavel'];
        $senhaResponsavel = $valores['senha_responsavel'];
        $nome = $valores['nome'];
        $email = $valores['email'];
        $corporacao = $valores['corporacao'];
        $idCorporacao = $valores['id_corporacao'];
        $senha = $valores['senha'];
        $idPrograma = $valores['idPrograma'];
        $imei = $valores['imei'];
        $corporacaoReserva = $valores['corporacao_reserva'];

        $mensagem = null;

        if ($mensagem == null
            || $mensagem->ok())
        {
            $mensagem = BO_Usuario::cadastraUsuarioECorporacaoNaReserva(
                $corporacaoReserva,
                $nome,
                $email,
                $corporacao,
                $senha,
                $idPrograma,
                $imei,
                $idCorporacao);
        }
        return $mensagem;
    }

    public static function clearCacheBanco()
    {
        $idCorporacao = Helper::POSTGET("id_corporacao");
        $corporacao = Helper::POSTGET("corporacao");

        if (!empty($corporacao))
        {
            return ConfiguracaoCorporacaoDinamica::clearConfiguracaoSiteFromCache($corporacao);
        }
        else
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_PARAMETRO_INVALIDO, "Par�metro inv�lido");
        }
    }

    public static function autoLogin()
    {
        //se passar pelo sess�o � porque foi poss�vel o autologin
        return new Mensagem(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO);
    }

    public static function teste()
    {
        EXTDAO_Pessoa_empresa::adicionaProfissaoIndefinidaAPessoa(
            new Database(),
            1726,
            2,
            1
        );
    }

}
