import { RemoteModel } from 'lib/model';
import { PontoEletronico, Empresa, Usuario, WifiNetwork } from 'aplicacao/modulo/mapa/remoteModel';

export class DadosIniciais extends RemoteModel
{
    public pontos :Array<PontoEletronico>;
    public empresas :Array<Empresa>;
    public usuarios :Array<Usuario>;
    public wifi :Array<WifiNetwork>;
}