<?php

require_once '../recursos/php/configuracao_biblioteca_compartilhada.php';

require_once $workspaceRaiz . DIRETORIO_BIBLIOTECAS_COMPARTILHADAS . '/imports/header.php';

ConfiguracaoCorporacaoDinamica::init(getWebservicesSemCache());

require_once '../recursos/php/funcoes.php';

$corporacao = ConfiguracaoCorporacaoDinamica::getParametroCorporacao();
$script = new LoginPadrao(array('corporacao' => $corporacao));
$script->render(array());

require_once '../recursos/php/on_finish.php';
