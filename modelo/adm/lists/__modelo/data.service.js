returnObject.filterClienteList = function (filterData)
{
    return $http({
        url: "actions.php?class=EXTDAO_Acesso&jsonOutput=true&action=filterClientes",
        method: "POST",
        data: filterData,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    });
};
returnObject.getListOfPessoa = function ()
{
    var params = {};
    return $http.get(baseURLAdm + "/actions.php?class=EXTDAO_Acesso&action=getListaPessoas", {params: params});
};
returnObject.getListOfPaises = function ()
{
    var params = {};
    return $http.get(baseURLAdm + "/actions.php?class=EXTDAO_Acesso&action=getListaPaises", {params: params});
};


