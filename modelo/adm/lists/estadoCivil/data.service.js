/*

 Arquivo gerado atrav�s de gerador de c�digo em 07/01/2018 as 15:54:10.
 Para que o arquivo n�o seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

 Tabela correspondente: estado_civil
 Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

 */

returnObject.getAllComboBoxesDataForEstadoCivilFilter = function ()
{
    return $http({
        url: baseURLAdm + "/webservice.php?class=EXTDAO_Estado_civil&jsonOutput=true&action=__getFilterComboBoxesData",
        method: "POST",
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    });

};

returnObject.getEstadoCivilList = function (listParams)
{
    listParams.listMappingType = Constants.ListMappingType.GRID;
    return $http.post(baseURLAdm + "/webservice.php?class=EXTDAO_Estado_civil&action=__getList", listParams);
};

returnObject.removeRecords = function (recordsPKs)
{
    return $http.post(baseURLAdm + "/webservice.php?class=EXTDAO_Estado_civil&action=remove", recordsPKs);
};



