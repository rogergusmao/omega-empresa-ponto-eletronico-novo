

        /*

        Arquivo gerado atrav�s de gerador de c�digo em 07/09/2017 as 01:26:41.
        Para que o arquivo n�o seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

        Tabela correspondente: usuario_servico
        Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

        */

omegaApp.generatedListUsuarioServicoController = function($scope, $rootScope, $window, $interval, $timeout, dataService, Upload, vm) {try { vm.fieldsTypes = {};

vm.disabledFilterFields = {
        fieldId: false,
fieldStatusBoolean: false,
fieldServicoId: false,
fieldUsuarioId: false,
};

vm.requiredFilterFields = {
        fieldId: false,
fieldStatusBoolean: false,
fieldServicoId: false,
fieldUsuarioId: false,
};

vm.filterFieldsParameters = {
        fieldId: null,
fieldStatusBoolean: null,
fieldServicoId: {remoteData: null},
fieldUsuarioId: {remoteData: null},
};

vm.filterData = {
        id: null,
statusBoolean: null,
servicoId: null,
usuarioId: null,
};

vm.filterOperators = {
        id: Constants.FilterOperator.EQUALS,
statusBoolean: Constants.FilterOperator.EQUALS,
servicoId: Constants.FilterOperator.EQUALS,
usuarioId: Constants.FilterOperator.EQUALS,
};

vm.sortingClass = {
        id: Constants.ListSortingClass.IDLE,
statusBoolean: Constants.ListSortingClass.IDLE,
servicoId: Constants.ListSortingClass.IDLE,
usuarioId: Constants.ListSortingClass.IDLE,
};

vm.paginationParameters =
        {
            currentPage: 1,
            recordsPerPage: Constants.LIST_DEFAULT_NUMBER_OF_RECORDS_PER_PAGE,
            lastPage: null,
            visibleButtonsArray: []
        };

vm.sortingParameters = [];

$rootScope.filterListWatch(vm);

vm.getRemoteFilterData = function()
        {
            var parameters =
            {
                filterParameters: GeneralUtil.getFilterParameters(vm.preservedFilterData, vm.filterOperators),
                sortingParameters: vm.sortingParameters,
                paginationParameters: vm.paginationParameters
            };

            dataService.getList(parameters).then(function(returnContent)
            {
                if (GeneralUtil.validateRemoteResponse(returnContent))
                {
                    var remoteContent = returnContent.data.mObj;

                    vm.originalGridData = remoteContent.gridData;
                    vm.gridData = angular.copy(vm.originalGridData);
                    vm.paginationParameters = remoteContent.paginationParameters;
                    vm.isFiltered = true;

                    PaginationUtil.updateVisiblePaginationButtonsArray(vm);
                }
                else
                {
                    $rootScope.instantErrorMessages.push(returnContent.data.mMensagem);
                }
            });

        };

vm.getListOfServicoId = function()
                {
                    

                        

                        dataService.getListOfServicoId().then(function (returnContent)
                        {
                            if (GeneralUtil.validateRemoteResponse(returnContent))
                            {
                                var remoteContent = GeneralUtil.isNullOrEmpty(returnContent.data.mObj) ? [] : returnContent.data.mObj;
                                vm.filterFieldsParameters.fieldServicoId.remoteData = remoteContent.gridData;
                            }
                            else
                            {
                                $rootScope.instantErrorMessages.push(returnContent.data.mMensagem);
                            }

                        });

                    

                };vm.getListOfUsuarioId = function()
                {
                    

                        

                        dataService.getListOfUsuarioId().then(function (returnContent)
                        {
                            if (GeneralUtil.validateRemoteResponse(returnContent))
                            {
                                var remoteContent = GeneralUtil.isNullOrEmpty(returnContent.data.mObj) ? [] : returnContent.data.mObj;
                                vm.filterFieldsParameters.fieldUsuarioId.remoteData = remoteContent.gridData;
                            }
                            else
                            {
                                $rootScope.instantErrorMessages.push(returnContent.data.mMensagem);
                            }

                        });

                    

                };vm.getListOfCorporacaoId = function()
                {
                    

                        

                        dataService.getListOfCorporacaoId().then(function (returnContent)
                        {
                            if (GeneralUtil.validateRemoteResponse(returnContent))
                            {
                                var remoteContent = GeneralUtil.isNullOrEmpty(returnContent.data.mObj) ? [] : returnContent.data.mObj;
                                vm.filterFieldsParameters.fieldCorporacaoId.remoteData = remoteContent.gridData;
                            }
                            else
                            {
                                $rootScope.instantErrorMessages.push(returnContent.data.mMensagem);
                            }

                        });

                    

                };

vm.selectedRecords = [];
        vm.selectedRecordsForRemoval = [];

        vm.showRemoveMessageDialog = function(records)
        {
            records = records || vm.selectedRecords;
            vm.selectedRecordsForRemoval = angular.copy(records);

            $rootScope.showDialog(vm, 'before-remove-template.html');
        };

        vm.closeRemoveMessageDialog = function()
        {
            vm.selectedRecordsForRemoval = [];
            $rootScope.closeMessageDialog();
        };

        vm.removeSelectedRecords = function()
        {
            var parameters = vm.selectedRecordsForRemoval;
            dataService.removeRecords(parameters).then(function (returnContent)
            {
                vm.closeRemoveMessageDialog();
                if (GeneralUtil.validateRemoteResponse(returnContent))
                {
                    vm.getRemoteFilterData(function(){ $rootScope.showSuccessMessage(returnContent.data.mMensagem) });
                }
                else
                {
                    $rootScope.instantErrorMessages.push(returnContent.data.mMensagem);
                }
            });
        };

vm.editRecord = function(primaryKey)
        {
            var parameters = $.param(primaryKey);
            document.location = 'index.php?tipo=forms&page=usuarioServico&' + parameters;
        };

}
                        catch(ex)
                        {
                            $rootScope.instantErrorMessages.push(new Message('Erro no controller generatedListUsuarioServicoController', ex.message));
                        }

                }