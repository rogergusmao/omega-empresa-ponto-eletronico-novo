export class Tempo
{
    public data: Date;
    public tempoEmSegundos: number;

    public constructor();
    public constructor(data: Date);
    public constructor(data: Date, tempoEmSegundos: number);
    public constructor(data?: Date, tempoEmSegundos?: number)
    {
        this.data = data || new Date();
        this.tempoEmSegundos = tempoEmSegundos;
    }

    public updateDate(data: Date): void
    {
        this.data = data;
    }

    public updateValue(value: number): void
    {
        this.tempoEmSegundos = value;
    }

    public toString(): string
    {
        var sec_num: any = this.tempoEmSegundos;
        var hours: any = Math.floor(sec_num / 3600);
        var minutes: any = Math.floor((sec_num - (hours * 3600)) / 60);
        var seconds: any = sec_num - (hours * 3600) - (minutes * 60);


        if (hours < 10)
        {
            hours = "0" + hours;
        }
        if (minutes < 10)
        {
            minutes = "0" + minutes;
        }
        if (seconds < 10)
        {
            seconds = "0" + seconds;
        }

        var time = hours + ':' + minutes + ':' + seconds;
        return time;

    }

}