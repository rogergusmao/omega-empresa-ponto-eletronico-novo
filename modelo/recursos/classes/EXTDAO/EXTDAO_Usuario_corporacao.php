<?php

/*

Arquivo gerado atravï¿½s de gerador de cï¿½digo em 24/05/2017 as 07:53:29.
Para que o arquivo nï¿½o seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

Tabela correspondente: usuario_corporacao
Sobrescrita de dados: ---SOBRESCRITA_NAO_AUTORIZADA---

*/
?>

<?php

class EXTDAO_Usuario_corporacao extends DAO_Usuario_corporacao
{
    public function __construct($configDAO = null)
    {
        parent::__construct($configDAO);
        $this->nomeClasse = "EXTDAO_Usuario_corporacao";
    }

    public function factory()
    {
        return new EXTDAO_Usuario_corporacao();
    }

    public static function getIdUsuarioCorporacao($idUsuario, $idCorporacao, $db = null)
    {
        $q = "SELECT id "
            . " FROM usuario_corporacao "
            . " WHERE usuario_id_INT = $idUsuario "
            . " AND corporacao_id_INT = $idCorporacao ";
        if ($db == null)
        {
            $db = new Database();
        }

        $db->query($q);

        return $db->getPrimeiraTuplaDoResultSet(0);
    }
}

?>
