omegaApp.generatedListClienteController = function ($scope, $rootScope, $window, $interval, $timeout, dataService, Upload, vm)
{
    try
    {
        vm.fieldsTypes = {};
        vm.disabledFilterFields =
            {
                fieldNome: false,
                fieldSenha: false,
                fieldComentarios: false,
                fieldPassaporte: true,
                fieldPreferencias: false,
                fieldPessoa: false,
                fieldPaises: false,
                fieldDataNascimento: false,
                fieldPeriodoDe: false,
                fieldPeriodoAte: false,
                fieldHora: false,
                fieldPreco: false,
                fieldCep: false,
                fieldCpf: false,
                fieldCnpj: false,
                fieldTelefone: false,
                fieldPorcentagem: false,
                fieldArquivo: false
            };
        vm.requiredFilterFields =
            {
                fieldNome: false,
                fieldSenha: false,
                fieldComentarios: false,
                fieldPassaporte: false,
                fieldPreferencias: false,
                fieldPessoa: false,
                fieldPaises: false,
                fieldDataNascimento: false,
                fieldPeriodoDe: false,
                fieldPeriodoAte: false,
                fieldHora: false,
                fieldPreco: false,
                fieldCep: false,
                fieldCpf: false,
                fieldCnpj: false,
                fieldTelefone: false,
                fieldPorcentagem: false,
                fieldArquivo: false
            };
        vm.filterFieldsParameters =
            {
                fieldNome: null,
                fieldSenha: null,
                fieldComentarios: null,
                fieldPassaporte: null,
                fieldPreferencias: null,
                fieldPessoa: {remoteData: null},
                fieldPaises: {remoteData: null},
                fieldDataNascimento: {
                    popupAberto: false,
                    datePickerParams: angular.copy(FieldsParametersUtil.defaultDatePickerParams)
                },
                fieldPeriodoDe: {
                    isPopupOpened: false,
                    datePickerParams: angular.copy(FieldsParametersUtil.defaultDatePickerParams)
                },
                fieldPeriodoAte: {
                    isPopupOpened: false,
                    datePickerParams: angular.copy(FieldsParametersUtil.defaultDatePickerParams)
                },
                fieldHora: {timePickerParams: angular.copy(FieldsParametersUtil.defaultTimePickerParams)},
                fieldUpload: {uploadAreaParams: angular.copy(FieldsParametersUtil.defaultUploadAreaParams)},
                fieldPreco: null,
                fieldCep: null,
                fieldCpf: null,
                fieldCnpj: null,
                fieldTelefone: null,
                fieldPorcentagem: null,
                fieldArquivo: null
            };
        vm.filterData =
            {
                nome: null,
                senha: null,
                comentarios: null,
                passaporte: null,
                preferencias: null,
                pessoa: null,
                paises: null,
                dataNascimento: null,
                periodoDe: null,
                periodoAte: null
            };
        vm.sortingClass =
            {
                id: Constants.ListSortingClass.IDLE,
                nome: Constants.ListSortingClass.IDLE,
                senha: Constants.ListSortingClass.IDLE,
                comentarios: Constants.ListSortingClass.IDLE,
                passaporte: Constants.ListSortingClass.IDLE,
                preferencias: Constants.ListSortingClass.IDLE,
                pessoa: Constants.ListSortingClass.IDLE,
                paises: Constants.ListSortingClass.IDLE,
                dataNascimento: Constants.ListSortingClass.IDLE,
                periodoDe: Constants.ListSortingClass.IDLE,
                periodoAte: Constants.ListSortingClass.IDLE
            };
        vm.paginationParameters =
            {
                currentPage: 1,
                recordsPerPage: Constants.LIST_DEFAULT_NUMBER_OF_RECORDS_PER_PAGE,
                lastPage: null,
                visibleButtonsArray: []
            };
        vm.sortingParameters = [];
        vm.gridData = null;
        vm.isFiltered = false;
        vm.selectedRecords = [];
        $rootScope.filterListWatch(vm);
        vm.getRemoteListData = function ()
        {
            var parameters = {
                filterData: vm.preservedFilterData,
                sortingParameters: vm.sortingParameters,
                paginationParameters: vm.paginationParameters
            };
            dataService.filterClienteList(parameters).then(function (returnContent)
            {
                if (GeneralUtil.validateRemoteResponse(returnContent))
                {
                    var remoteContent = returnContent.data.content;
                    vm.originalGridData = remoteContent.gridData;
                    vm.gridData = angular.copy(vm.originalGridData);
                    vm.paginationParameters = remoteContent.paginationParameters;
                    vm.isFiltered = true;
                    PaginationUtil.updateVisiblePaginationButtonsArray(vm);
                }
                else
                {
                    $rootScope.instantErrorMessages.push(returnContent.data.message);
                }
            });
        };
        vm.getListOfPessoa = function ()
        {
            dataService.getListOfPessoa().then(function (returnContent)
            {
                if (GeneralUtil.validateRemoteResponse(returnContent))
                {
                    vm.filterFieldsParameters.fieldPessoa.remoteData = returnContent.data.content;
                }
                else
                {
                    $rootScope.instantErrorMessages.push(returnContent.data.message);
                }
            });
        };
        vm.getListOfPaises = function ()
        {
            dataService.getListOfPaises().then(function (returnContent)
            {
                if (GeneralUtil.validateRemoteResponse(returnContent))
                {
                    vm.filterFieldsParameters.fieldPaises.remoteData = returnContent.data.content;
                }
                else
                {
                    $rootScope.instantErrorMessages.push(returnContent.data.message);
                }
            });
        };
    }
    catch (ex)
    {
        $rootScope.instantErrorMessages.push(new Message('Erro no controller gerado', ex.message));
    }
}