import { Cache_Backend_LocalStorage_Tag } from "lib/cache/Cache/Backend/LocalStorage/Tag/Tag";

export interface Cache_Backend_LocalStorage_TagTree_Interface {

	_tags : { [tag: string] : Cache_Backend_LocalStorage_Tag; };

}