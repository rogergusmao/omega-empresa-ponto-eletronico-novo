"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var React = require("react");
var react_bootstrap_1 = require("react-bootstrap");
var uiComponente_1 = require("lib/uiComponente");
var model_1 = require("aplicacao/modulo/calendario/model");
var ProblemasEncontrados = (function (_super) {
    __extends(ProblemasEncontrados, _super);
    function ProblemasEncontrados(props, reactContext) {
        var _this = _super.call(this, props, reactContext) || this;
        _this.dadosCalendario = new Array();
        return _this;
    }
    ProblemasEncontrados.prototype.fixLayout = function () {
        var maxHeight = uiComponente_1.DomContainer.getMaxAllowedHeight();
    };
    ProblemasEncontrados.prototype.setData = function (pessoas) {
        this.dadosCalendario = pessoas;
        this.setState({ dadosAtualizados: true });
    };
    ProblemasEncontrados.prototype.render = function () {
        var _this = this;
        var problemasEncontrados = new Array();
        if (this.dadosCalendario.length > 0) {
            for (var _i = 0, _a = this.dadosCalendario; _i < _a.length; _i++) {
                var pessoa = _a[_i];
                for (var _b = 0, _c = pessoa.eventos; _b < _c.length; _b++) {
                    var evento = _c[_b];
                    //caso o evento não tenha ponto final, adiciona como problema
                    if (evento.pontoFinal == null) {
                        var problemaEncontrado = new model_1.ProblemaEncontrado();
                        problemaEncontrado.id = evento.pontoInicial.idPonto;
                        problemaEncontrado.nomePessoa = evento.pontoInicial.nomePessoa;
                        problemaEncontrado.cor = evento.corBackgroundEvento;
                        problemaEncontrado.eventoAssociado = evento;
                        problemasEncontrados.push(problemaEncontrado);
                    }
                }
            }
        }
        if (problemasEncontrados.length > 0) {
            return (<div className="row">
                    <div className="col-sm-12">  
                        {problemasEncontrados.map(function (value) { return (<div key={value.id} className="box-content box-statistic problema-encontrado-item" onClick={function () { return _this.props.onProblemaClick(value.eventoAssociado); }}>
                                <h3 className="title" style={{ color: value.cor.getHexValue() }}>{value.getTituloProblema()}</h3>
                                <small>{value.nomePessoa}</small><br />
                                <small>{value.getDescricaoProblema()}</small>
                                <div className="icon-warning-sign align-right" style={{ color: value.cor.getHexValue() }}></div>
                            </div>); })}  
                    </div>
                </div>);
        }
        else {
            return (<react_bootstrap_1.Alert bsStyle="warning" className="mb-0">
                        <strong></strong> Nenhum problema encontrado no período
                    </react_bootstrap_1.Alert>);
        }
    };
    return ProblemasEncontrados;
}(uiComponente_1.ReactComponent));
exports.ProblemasEncontrados = ProblemasEncontrados;
