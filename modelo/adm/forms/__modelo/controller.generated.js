omegaApp.generatedFormClienteController = function ($scope, $rootScope, $window, $interval, $timeout, dataService, Upload, vm)
{
    try
    {
        vm.fieldsTypes = {};
        vm.formData =
            {
                nome: null,
                senha: null,
                comentarios: null,
                passaporte: null,
                preferencias: null,
                pessoa: null,
                paises: null,
                dataNascimento: null,
                periodoDe: null,
                periodoAte: null,
                documentosUpload: []
            };
        vm.relatedEntities =
            {
                cargos: {
                    formData: [
                        {
                            id: 1,
                            nome: 'Teste',
                            descricao: 'Cargo de Teste',
                            removed: false
                        },
                        {
                            id: 2,
                            nome: 'Teste 2',
                            descricao: 'Cargo de Teste 2',
                            removed: false
                        }
                    ],
                    disabledFields: {
                        fieldNome: false,
                        fieldDescricao: false
                    },
                    requiredFields: {
                        fieldNome: true,
                        fieldDescricao: true
                    },
                    fieldsParameters: {
                        fieldNome: null,
                        fieldDescricao: null
                    }
                }
            };
        var defaultCargoObject =
            {
                id: null,
                nome: null,
                descricao: null,
                removed: false
            };
        vm.addCargo = function ()
        {
            vm.relatedEntities.cargos.formData.push(angular.copy(defaultCargoObject));
        };
        vm.removeCargo = function (index)
        {
            vm.relatedEntities.cargos.formData[index].removed = true;
        };
        vm.disabledFields =
            {
                fieldNome: false,
                fieldSenha: false,
                fieldComentarios: false,
                fieldPassaporte: true,
                fieldPreferencias: false,
                fieldPessoa: false,
                fieldPaises: false,
                fieldDataNascimento: false,
                fieldPeriodoDe: false,
                fieldPeriodoAte: false,
                fieldHora: false,
                fieldPreco: false,
                fieldCep: false,
                fieldCpf: false,
                fieldCnpj: false,
                fieldTelefone: false,
                fieldPorcentagem: false,
                fieldDocumentoUpload: false
            };
        vm.requiredFields =
            {
                fieldNome: true,
                fieldSenha: true,
                fieldComentarios: true,
                fieldPassaporte: false,
                fieldPreferencias: false,
                fieldPessoa: true,
                fieldPaises: true,
                fieldDataNascimento: true,
                fieldPeriodoDe: true,
                fieldPeriodoAte: true,
                fieldHora: true,
                fieldPreco: false,
                fieldCep: true,
                fieldCpf: true,
                fieldCnpj: false,
                fieldTelefone: true,
                fieldPorcentagem: false,
                fieldDocumentoUpload: false
            };
        vm.fieldsParameters =
            {
                fieldNome: null,
                fieldSenha: null,
                fieldComentarios: null,
                fieldPassaporte: null,
                fieldPreferencias: null,
                fieldPessoa: {remoteData: null},
                fieldPaises: {remoteData: null},
                fieldDataNascimento: {
                    popupAberto: false,
                    datePickerParams: angular.copy(FieldsParametersUtil.defaultDatePickerParams)
                },
                fieldPeriodoDe: {
                    isPopupOpened: false,
                    datePickerParams: angular.copy(FieldsParametersUtil.defaultDatePickerParams)
                },
                fieldPeriodoAte: {
                    isPopupOpened: false,
                    datePickerParams: angular.copy(FieldsParametersUtil.defaultDatePickerParams)
                },
                fieldHora: {timePickerParams: angular.copy(FieldsParametersUtil.defaultTimePickerParams)},
                fieldDocumentoUpload: angular.copy(FieldsParametersUtil.defaultUploadAreaParams),
                fieldPreco: null,
                fieldCep: null,
                fieldCpf: null,
                fieldCnpj: null,
                fieldTelefone: null,
                fieldPorcentagem: null
            };
        vm.openPessoaModal = function ()
        {
            $rootScope.uibModalInstance = $rootScope.showModalDialog('forms', 'cliente');
        };
        vm.submitForm = function (formInstance)
        {
            if ($rootScope.validateForm(formInstance))
            {
                var formData = angular.merge({}, vm.formData, GeneralUtil.getRelatedEntitiesData(vm.relatedEntities));
                if (!GeneralUtil.isNullOrEmpty(vm.formData.id))
                {
                    dataService.addCliente(formData).then(function ()
                    {
                        $rootScope.showDialog(vm, 'after-insert-template.html');
                    });
                }
                else
                {
                    dataService.editCliente(formData).then(function ()
                    {
                        $rootScope.showDialog(vm, 'after-update-template.html');
                    });
                }
            }
        };
        if (!GeneralUtil.isNullOrEmpty(__GET) && !GeneralUtil.isNullOrEmpty(__GET.id))
        {
            dataService.getCliente(__GET.id).then(function (returnContent)
            {
                if (GeneralUtil.validateRemoteResponse(returnContent))
                {
                    vm.formData = returnContent.data.content;
                }
                else
                {
                    $rootScope.instantErrorMessages.push(returnContent.data.message);
                }
            });
            dataService.getCargos(__GET.id).then(function (returnContent)
            {
                if (GeneralUtil.validateRemoteResponse(returnContent))
                {
                    vm.relatedEntities.cargos.formData = returnContent.data.content;
                }
                else
                {
                    $rootScope.instantErrorMessages.push(returnContent.data.message);
                }
            });
        }
        vm.getListOfPessoa = function ()
        {
            dataService.getListOfPessoa().then(function (returnContent)
            {
                if (GeneralUtil.validateRemoteResponse(returnContent))
                {
                    vm.fieldsParameters.fieldPessoa.remoteData = returnContent.data.content;
                }
                else
                {
                    $rootScope.instantErrorMessages.push(returnContent.data.message);
                }
            });
        };
        vm.getListOfPaises = function ()
        {
            dataService.getListOfPaises().then(function (returnContent)
            {
                if (GeneralUtil.validateRemoteResponse(returnContent))
                {
                    vm.fieldsParameters.fieldPaises.remoteData = returnContent.data.content;
                }
                else
                {
                    $rootScope.instantErrorMessages.push(returnContent.data.message);
                }
            });
        };
        $scope.$watch(function ()
        {
            return vm.files
        }, function (oldVal, newVal)
        {
            if (!ArrayUtil.isNullOrEmpty(vm.files))
            {
                vm.uploadFiles(vm.files);
            }
        });
        vm.loadClientesList = function ()
        {
            document.location.href = 'index.php?tipo=lists&page=cliente';
        };
        vm.addNewCliente = function ()
        {
            document.location.href = document.location.href;
        };
        vm.removeUploadedFile = function (fileUniqueId)
        {
            dataService.removeDocumentoUploadFile(fileUniqueId).then(function (response)
            {
                for (var i = 0; i < vm.formData.documentosUpload.length; i++)
                {
                    if (vm.formData.documentosUpload[i].fileUniqueId == fileUniqueId)
                    {
                        vm.formData.documentosUpload.splice(i, 1);
                    }
                }
            });
        };
        vm.uploadFiles = function (files)
        {

            //se for um unico arquivo, transforma em array
            if (!files instanceof Array && typeof files == 'object')
            {
                files = [files];
            }
            vm.isUploading = true;
            if (files && files.length)
            {
                for (var i = 0; i < files.length; i++)
                {
                    var file = files[i];
                    if (!file.$error)
                    {
                        Upload.upload({
                            url: baseURLAdm + "/actions.php?class=EXTDAO_Acesso&action=uploadDocumentoUploadFile",
                            data: {
                                file: file,
                                teste: 'Eduardo'
                            }
                        })
                            .then(function (resp)
                                {
                                    $timeout(function ()
                                    {

                                        //copiando para o log
                                        vm.fieldsParameters.fieldDocumentoUpload.uploadLog.push({
                                            success: true,
                                            messsage: 'Upload successfully',
                                            responseData: resp.data
                                        });
                                        //setando unique id retornado pelo servidor
                                        file.fileUniqueId = resp.data.content.fileUniqueId;
                                        //copiar para estrutura fixa
                                        vm.formData.documentosUpload.push(file);
                                    });
                                    $timeout(function ()
                                    {
                                        vm.isUploading = false;
                                    }, 3000);
                                },
                                function (resp)
                                {
                                    $timeout(function ()
                                    {

                                        //copiando para o log
                                        vm.fieldsParameters.fieldDocumentoUpload.uploadLog.push({
                                            success: false,
                                            messsage: 'Server error',
                                            response: resp
                                        });
                                        vm.isUploading = false;
                                    }, 3000);
                                },
                                function (evt)
                                {
                                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                                    vm.uploadPercentage = progressPercentage + "%";
                                });
                    }
                    else
                    {
                        vm.fieldsParameters.fieldDocumentoUpload.uploadLog.push({
                            success: false,
                            message: 'Upload blocked by client',
                            errorObject: file.$error
                        });
                    }
                }
            }
        };
    }
    catch (ex)
    {
        $rootScope.instantErrorMessages.push(new Message('Erro no controller gerado', ex.message));
    }
}