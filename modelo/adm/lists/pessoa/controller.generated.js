/*

Arquivo gerado atrav�s de gerador de c�digo em 21/01/2018 as 18:27:39.
Para que o arquivo n�o seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

Tabela correspondente: pessoa
Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

*/

omegaApp.generatedListPessoaController = function ($scope, $rootScope, $element, $window, $interval, $timeout, dataService, Upload, vm)
{
    try
    {
        vm.rootControllerElement = $element;
        vm.fieldsTypes = {};
        vm.isModalView = false;

        vm.messages = {
            error: [],
            warning: [],
            info: [],
            success: []
        };

        vm.disabledFilterFields = {
            fieldId: false,
            fieldIdentificador: false,
            fieldNome: false,
            fieldTipoDocumentoId: false,
            fieldNumeroDocumento: false,
            fieldIsConsumidorBoolean: false,
            fieldSexoId: false,
            fieldTelefone: false,
            fieldCelular: false,
            fieldEmail: false,
            fieldLogradouro: false,
            fieldNumero: false,
            fieldComplemento: false,
            fieldCidadeId: false
        };

        vm.requiredFilterFields = {
            fieldId: false,
            fieldIdentificador: false,
            fieldNome: false,
            fieldTipoDocumentoId: false,
            fieldNumeroDocumento: false,
            fieldIsConsumidorBoolean: false,
            fieldSexoId: false,
            fieldTelefone: false,
            fieldCelular: false,
            fieldEmail: false,
            fieldLogradouro: false,
            fieldNumero: false,
            fieldComplemento: false,
            fieldCidadeId: false
        };

        vm.filterFieldsParameters = {
            fieldId: null,
            fieldIdentificador: null,
            fieldNome: null,
            fieldTipoDocumentoId: {remoteData: null},
            fieldNumeroDocumento: null,
            fieldIsConsumidorBoolean: {options: angular.copy(FieldsParametersUtil.getDefaultSelectForBooleanOptions())},
            fieldSexoId: {remoteData: null},
            fieldTelefone: null,
            fieldCelular: null,
            fieldEmail: null,
            fieldLogradouro: null,
            fieldNumero: null,
            fieldComplemento: null,
            fieldCidadeId: {remoteData: null}
        };

        vm.filterData = {
            id: null,
            identificador: null,
            nome: null,
            tipoDocumentoId: null,
            numeroDocumento: null,
            isConsumidorBoolean: null,
            sexoId: null,
            telefone: null,
            celular: null,
            email: null,
            logradouro: null,
            numero: null,
            complemento: null,
            cidadeId: null
        };

        vm.filterOperators = {
            id: Constants.FilterOperator.EQUALS,
            identificador: Constants.FilterOperator.CONTAINS,
            nome: Constants.FilterOperator.CONTAINS,
            tipoDocumentoId: Constants.FilterOperator.EQUALS,
            numeroDocumento: Constants.FilterOperator.CONTAINS,
            isConsumidorBoolean: Constants.FilterOperator.EQUALS,
            sexoId: Constants.FilterOperator.EQUALS,
            telefone: Constants.FilterOperator.EQUALS,
            celular: Constants.FilterOperator.EQUALS,
            email: Constants.FilterOperator.CONTAINS,
            logradouro: Constants.FilterOperator.CONTAINS,
            numero: Constants.FilterOperator.CONTAINS,
            complemento: Constants.FilterOperator.CONTAINS,
            cidadeId: Constants.FilterOperator.EQUALS
        };

        vm.sortingClass = {
            id: Constants.ListSortingClass.IDLE,
            identificador: Constants.ListSortingClass.IDLE,
            nome: Constants.ListSortingClass.IDLE,
            tipoDocumentoId: Constants.ListSortingClass.IDLE,
            numeroDocumento: Constants.ListSortingClass.IDLE,
            sexoId: Constants.ListSortingClass.IDLE,
            email: Constants.ListSortingClass.IDLE,
            cidadeId: Constants.ListSortingClass.IDLE
        };

        vm.paginationParameters =
            {
                currentPage: 1,
                recordsPerPage: Constants.LIST_DEFAULT_NUMBER_OF_RECORDS_PER_PAGE,
                lastPage: null,
                visibleButtonsArray: []
            };

        vm.sortingParameters = [];

        var relationshipObject = {
            tipoDocumentoId: 'fieldTipoDocumentoId',
            sexoId: 'fieldSexoId',
            operadoraId: 'fieldOperadoraId',
            cidadeId: 'fieldCidadeId',
            bairroId: 'fieldBairroId'
        };

        vm.isInitialLoadFinished = false;
        var initialLoading = function ()
        {
            $timeout(function ()
            {
                dataService.getAllComboBoxesDataForPessoaFilter().then(function (returnContent)
                {
                    if (RemoteDataUtil.validateRemoteResponse(returnContent))
                    {
                        var responseObject = RemoteDataUtil.getResponseObject(returnContent);
                        RemoteDataUtil.refactorInitialFilterLoadRemoteResponseData(vm, responseObject, relationshipObject);
                    }
                    else
                    {
                        LogUtil.logRemoteResponseError(returnContent);
                    }

                    vm.isInitialLoadFinished = true;
                    vm.getRemoteListDataFromQueryString();

                });
            }, 200);
        }

        $rootScope.filterListWatch(vm);

        vm.getRemoteListDataFromQueryString = function ()
        {
            var listParametersObject = OmegaListUtil.getListParametersFromBrowseUrlQueryString();

            if (!GeneralUtil.isNullOrEmptyObject(listParametersObject.preservedFilterData))
            {
                vm.filterData = listParametersObject.preservedFilterData;
            }

            var parametersObject = {};
            if (!GeneralUtil.isNullOrEmptyObject(listParametersObject.filterParameters))
            {
                parametersObject.filterParameters = listParametersObject.filterParameters;
            }
            else
            {
                parametersObject.filterParameters = GeneralUtil.getFilterParameters({}, vm.filterOperators)
            }

            if (!GeneralUtil.isNullOrEmptyObject(listParametersObject.sortingParameters))
            {
                parametersObject.sortingParameters = listParametersObject.sortingParameters;
            }
            else
            {
                parametersObject.sortingParameters = vm.sortingParameters;
            }

            if (!GeneralUtil.isNullOrEmptyObject(listParametersObject.paginationParameters))
            {
                parametersObject.paginationParameters = listParametersObject.paginationParameters;
            }
            else
            {
                parametersObject.paginationParameters = vm.paginationParameters;
            }

            vm.applyFilterSortingAndPaginationParameters(parametersObject);

        };
        vm.getRemoteListData = function ()
        {
            //limpa o filtro de resultados renderizado na lista
            vm.listFilter = null;

            //limpa checkboxes selecionados da lista
            vm.selectedRecords = [];

            var filterData = RemoteDataUtil.refactorRemoteRequestForFilter(vm.filterData);
            var parametersObject =
                {
                    filterParameters: GeneralUtil.getFilterParameters(filterData, vm.filterOperators),
                    sortingParameters: vm.sortingParameters,
                    paginationParameters: vm.paginationParameters
                };

            vm.applyFilterSortingAndPaginationParameters(parametersObject);

        };
        vm.applyFilterSortingAndPaginationParameters = function (parametersObject)
        {
            OmegaListUtil.addListParametersToBrowserUrlQueryString(parametersObject, vm.filterData);

            dataService.getPessoaList(parametersObject).then(function (returnContent)
            {
                if (RemoteDataUtil.validateRemoteResponseForList(returnContent))
                {
                    var responseObject = RemoteDataUtil.getResponseObject(returnContent);

                    vm.originalDataSet = responseObject.dataSet;
                    vm.dataSet = angular.copy(vm.originalDataSet);
                    vm.isFiltered = true;

                    OmegaListUtil.doActionsAfterRemoteListDataReceived(vm, responseObject);
                }
                else
                {
                    RemoteDataUtil.defaultRemoteErrorHandling(returnContent, vm);
                }
            });

        };

        vm.getListOfTipoDocumentoId = function ()
        {

            dataService.getListOfTipoDocumentoIdForPessoaList().then(function (returnContent)
            {
                if (RemoteDataUtil.validateRemoteResponseForList(returnContent))
                {
                    var remoteDataSet = RemoteDataUtil.getResponseObjectDataSetOrEmptyArray(returnContent);
                    vm.filterFieldsParameters.fieldTipoDocumentoId.remoteData = remoteDataSet;
                }
                else
                {
                    LogUtil.logRemoteResponseError(returnContent);
                }

            });

        };
        vm.getListOfSexoId = function ()
        {

            dataService.getListOfSexoIdForPessoaList().then(function (returnContent)
            {
                if (RemoteDataUtil.validateRemoteResponseForList(returnContent))
                {
                    var remoteDataSet = RemoteDataUtil.getResponseObjectDataSetOrEmptyArray(returnContent);
                    vm.filterFieldsParameters.fieldSexoId.remoteData = remoteDataSet;
                }
                else
                {
                    LogUtil.logRemoteResponseError(returnContent);
                }

            });

        };
        vm.getListOfOperadoraId = function ()
        {

            dataService.getListOfOperadoraIdForPessoaList().then(function (returnContent)
            {
                if (RemoteDataUtil.validateRemoteResponseForList(returnContent))
                {
                    var remoteDataSet = RemoteDataUtil.getResponseObjectDataSetOrEmptyArray(returnContent);
                    vm.filterFieldsParameters.fieldOperadoraId.remoteData = remoteDataSet;
                }
                else
                {
                    LogUtil.logRemoteResponseError(returnContent);
                }

            });

        };
        vm.getListOfCidadeId = function ()
        {

            dataService.getListOfCidadeIdForPessoaList().then(function (returnContent)
            {
                if (RemoteDataUtil.validateRemoteResponseForList(returnContent))
                {
                    var remoteDataSet = RemoteDataUtil.getResponseObjectDataSetOrEmptyArray(returnContent);
                    vm.filterFieldsParameters.fieldCidadeId.remoteData = remoteDataSet;
                }
                else
                {
                    LogUtil.logRemoteResponseError(returnContent);
                }

            });

        };
        vm.getListOfBairroId = function ()
        {

            dataService.getListOfBairroIdForPessoaList().then(function (returnContent)
            {
                if (RemoteDataUtil.validateRemoteResponseForList(returnContent))
                {
                    var remoteDataSet = RemoteDataUtil.getResponseObjectDataSetOrEmptyArray(returnContent);
                    vm.filterFieldsParameters.fieldBairroId.remoteData = remoteDataSet;
                }
                else
                {
                    LogUtil.logRemoteResponseError(returnContent);
                }

            });

        };
        vm.getListOfCorporacaoId = function ()
        {

            dataService.getListOfCorporacaoIdForPessoaList().then(function (returnContent)
            {
                if (RemoteDataUtil.validateRemoteResponseForList(returnContent))
                {
                    var remoteDataSet = RemoteDataUtil.getResponseObjectDataSetOrEmptyArray(returnContent);
                    vm.filterFieldsParameters.fieldCorporacaoId.remoteData = remoteDataSet;
                }
                else
                {
                    LogUtil.logRemoteResponseError(returnContent);
                }

            });

        };

        vm.selectedRecords = [];
        vm.selectedRecordsForRemoval = [];

        vm.showRemoveMessageDialog = function (records)
        {
            records = records || vm.selectedRecords;
            vm.selectedRecordsForRemoval = angular.copy(records);

            $rootScope.showDialog(vm, 'before-remove-template.html');
        };

        vm.closeRemoveMessageDialog = function ()
        {
            vm.selectedRecordsForRemoval = [];
            $rootScope.closeMessageDialog();
        };

        vm.removeSelectedRecords = function ()
        {
            var parameters = vm.selectedRecordsForRemoval;
            dataService.removeRecords(parameters).then(function (returnContent)
            {
                vm.closeRemoveMessageDialog();
                if (RemoteDataUtil.validateRemoteResponse(returnContent))
                {
                    vm.getRemoteListData();
                }
                else
                {
                    RemoteDataUtil.defaultRemoteErrorHandling(returnContent, vm);
                }
            });
        };

        vm.editRecord = function (primaryKey)
        {
            var parameters = $.param(primaryKey);
            document.location = 'index.php?tipo=forms&page=pessoa&' + parameters;
        };

        return {

            scope: this,
            initFunction: initialLoading

        };

    }
    catch (ex)
    {
        ApplicationUtil.showErrorMessage(ex.message, 'Erro no controller generatedListPessoaController', vm);
    }

}