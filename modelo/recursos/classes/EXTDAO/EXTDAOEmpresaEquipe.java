

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  EXTDAOEmpresaEquipe
        * DATA DE GERA��O: 21.08.2017
        * ARQUIVO:         EXTDAOEmpresaEquipe.java
        * TABELA MYSQL:    empresa_equipe
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronicofactu.database.EXTDAO;

        import app.omegasoftware.pontoeletronicofactu.database.DAO.DAOEmpresaEquipe;
        import app.omegasoftware.pontoeletronicofactu.database.Attribute;
        import app.omegasoftware.pontoeletronicofactu.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronicofactu.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronicofactu.database.Database;
        import app.omegasoftware.pontoeletronicofactu.database.Table;


        // **********************
        // DECLARA��O DA CLASSE
        // **********************


    
        

        public class EXTDAOEmpresaEquipe extends DAOEmpresaEquipe
        {


        // *************************
        // DECLARA��O DE ATRIBUTOS
        // *************************
        
        // *************************
        // CONSTRUTOR
        // *************************
        public EXTDAOEmpresaEquipe(Database database){
            super(database);

        }
        
        // *************************
        // FACTORY
        // *************************
        public Table factory(){
            return new EXTDAOEmpresaEquipe(this.getDatabase());

        }
        

        } // classe: fim


        