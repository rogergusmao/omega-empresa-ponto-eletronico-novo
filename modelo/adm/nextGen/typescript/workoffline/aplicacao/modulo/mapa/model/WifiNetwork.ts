import { Model } from 'lib/model';
import { Helper } from 'aplicacao/comum/geral';
import { WifiNetwork as WifiNetworkRemoteModel } from "aplicacao/modulo/mapa/remoteModel";

export class WifiNetwork extends Model
{
    public ssid :string;
    public dataInicio :Date;
    public dataFim :Date;
    public idEmpresa :number;
    public idUsuario :number;

    public static parseFromRemoteObject(remoteObject: WifiNetworkRemoteModel)
    {
        let instance = new WifiNetwork();

        instance.ssid = remoteObject.ssid;
        instance.idEmpresa = Helper.parseInt(remoteObject.idEmpresa);
        instance.idUsuario = Helper.parseInt(remoteObject.idUsuario);
        instance.dataInicio = DateUtil.getDateObject(Helper.parseInt(remoteObject.dataInicioSec) + Helper.parseInt(remoteObject.dataInicioOffsec));
        instance.dataFim = DateUtil.getDateObject(Helper.parseInt(remoteObject.dataFimSec) + Helper.parseInt(remoteObject.dataFimOffsec));

        return instance;
    }

}