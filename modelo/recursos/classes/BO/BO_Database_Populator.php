<?php
    
class BO_Database_Populator {

    private $arrChavesProibidasNoCrud = array(

        "usuario" => array(1, 2, 3, 4),
        "empresa" => array(2, 5),
        "pessoa" => array(4, 7, 10)

    );

    public $tempJsonStructure;

    public $objBanco;
    public $arrTableObjList;
    public $arrOperations;
    public $arrAlreadyProcessedTables = array();
    public $tabelasIgnorarAlteracoes = array("corporacao", "usuario");
    public $isToGenerateJson = false;
    public $arrJsonOperations = array();
    public $results;
    public $doNotOperateOnFKTableIfNotInList = false;

    public function releaseApcLock(){

        $tabela = $_GET["tabela"];
        $chaveLock = "dblock_{$tabela}";

        apc_fetch($chaveLock, $success);

        if($success)
            echo "a chave existia....<br>";

        apc_delete($chaveLock);
        echo "lock da tabela {$tabela} desativado...";

    }

    public function getTesteInsert(){

        session_write_close();
        $inicio = date("d/M/Y H:i:s");

        $objBanco = new Database();
        $objBanco->iniciarTransacao();

        for($i=0; $i < 100; $i++) {

            $proximoId =  EXTDAO_Sistema_sequencia::getInstance()->gerarId("teste");
            if ($proximoId == EXTDAO_Sistema_sequencia::$VALOR_INVALIDO) {

                echo "Lock não foi liberado...";
                exit();

            }

            $nome = "Teste!! " . rand(0, 1000);
            $objBanco->query("INSERT INTO teste VALUES({$proximoId}, '{$nome}')");

        }


        sleep(10);

        if(isset($_GET["operacao"]))
            $operacao = $_GET["operacao"];
        else
            $operacao = "commit";

        if($operacao == "commit")
            $objBanco->commitTransacao();
        else
            $objBanco->rollbackTransacao();

        $termino = date("d/M/Y H:i:s");

        echo "Concluído {$operacao} {$proximoId}...<br>";
        echo "Tempo total de semáforo: " . EXTDAO_Sistema_sequencia::$tempoTotalSemaforo . "ms<br>";
        echo "Inicio: $inicio<br>";
        echo "Termino: $termino<br>";

    }

    public function __construct(){

        $this->objBanco = new Database();
        $this->objBanco->printErrorMessage = false;
        $this->objBanco->performTransationOperations = false;

        $objPost = json_decode(file_get_contents("php://input"));

        if(is_numeric($objPost->corporacaoId))
             Helper::setSession("corporacao_id",  $objPost->corporacaoId) ;

        $this->arrTableObjList = $objPost->tableList;

        if($objPost->operations->insert)
            $this->arrOperations[] = Operations::INSERT;

        if($objPost->operations->update)
            $this->arrOperations[] = Operations::UPDATE;

        if($objPost->operations->delete)
            $this->arrOperations[] = Operations::DELETE;

        $this->doNotOperateOnFKTableIfNotInList = $objPost->desativarOperacoesChaveEstrangeira;

        //parametro para saber se é para criar o arquivo JSON com os registros inseridos/modificados
        $this->isToGenerateJson = $objPost->isToGenerateJson;

        if($this->isToGenerateJson) {

            $this->urlDoProduto = $objPost->urlDoProduto;

            if ($objPost->jsonOperations)
                $this->arrJsonOperations[] = Operations::INSERT;

            if ($objPost->jsonOperations)
                $this->arrJsonOperations[] = Operations::UPDATE;

            if ($objPost->jsonOperations)
                $this->arrJsonOperations[] = Operations::DELETE;

        }

        $this->results = new GenerationResult();

    }

    public function checkIfTableHasValidUpdatableIds($tableName){

        if(array_key_exists($tableName, $this->arrChavesProibidasNoCrud)) {

            $primaryKeyColumn = $pkField = $this->getPrimaryKey($tableName);
            $strForbiddenPKs = implode(",", $this->arrChavesProibidasNoCrud[$tableName]);

            $this->objBanco->query("SELECT COUNT(DISTINCT {$primaryKeyColumn}) FROM {$tableName} WHERE {$primaryKeyColumn} NOT IN({$strForbiddenPKs})");
            $numeroLinhas = $this->objBanco->getPrimeiraTuplaDoResultSet(0);

            return ($numeroLinhas > 0);

        }
        else{

            return true;

        }

    }

    public function getRandomAllowedOperation(){

        $randomOperation = $this->arrOperations[rand(0, count($this->arrOperations)-1)];
        return $randomOperation;

    }

    public function generateJson(){

        $uniqueFileName = date("Ymd-His");
        $raiz= Helper::acharRaiz();
        $pathSemRaiz= "conteudo/jsons/";
        $file = "{$uniqueFileName}.json";
        if(!is_dir($raiz.$pathSemRaiz)) Helper::mkdir($raiz.$pathSemRaiz, 0777, true);
        //cria arquivo em disco
        file_put_contents($raiz.$pathSemRaiz.$file, Helper::jsonEncode($this->tempJsonStructure));
        $url = $this->urlDoProduto . $pathSemRaiz.$file;
//        print_r($url);
        return $url;

    }

    public function insertData(){
        set_time_limit(30 * 60);
        foreach($this->arrTableObjList as $tableObj){

            $tableName = $tableObj->tableName;
            $recordCount = $tableObj->recordCount;

            $this->performCrudOperations($tableName, $recordCount);

        }

        $this->results->success = true;
        $this->results->message = utf8_encode("Operações de CRUD realizadas em " . count($this->results->arrTables) . " tabelas.");

        if($this->isToGenerateJson) {

            $this->results->jsonFileUrl = $this->generateJson();

        }

        return $this->results;

    }

    public function getNewDatabaseInstance(){

        $newDatabase = new Database();
        return $newDatabase;

    }

    public function getRecordCount($tableName){

        foreach($this->arrTableObjList as $tableObj){

            if($tableObj->tableName == $tableName){

                return $tableObj->recordCount;

            }

        }

        return null;

    }

    public function getFieldList($tableName){

        $arrRetorno = array();
        $objBanco = $this->getNewDatabaseInstance();
        $objBanco->query("SHOW COLUMNS FROM {$tableName}");

        $primaryKey = $this->getPrimaryKey($tableName);
        while($dados = $objBanco->fetchArray(MYSQL_NUM)){

            if($dados[0] != $primaryKey) {

                $type = $dados[1];
                if (strpos($type, "(") > -1) {

                    $type = substr($type, 0, strpos($type, "("));

                }

                $maxLength = $dados[1];
                if (strpos($maxLength, "(") > -1) {

                    $pos1 =  strpos($maxLength, "(")+1;
                    $pos2 = strrpos($maxLength, ")");
                    $maxLength = substr($maxLength, $pos1, $pos2-$pos1);

                }
                else{

                    $maxLength = null;

                }

                $objField = new FieldStructure();
                $objField->fieldName = $dados[0];
                $objField->type = $type;
                $objField->maxLength = $maxLength;
                $objField->nullable = ($dados[2] == "YES") ? true : false;

                $arrRetorno[] = $objField;

            }

        }

        return $arrRetorno;

    }

    public function getPrimaryKey($tableName){

        $primaryKey = "";
        $objBanco = $this->getNewDatabaseInstance();
        $objBanco->query("SHOW KEYS FROM {$tableName} WHERE Key_name = 'PRIMARY'");

        if($objBanco->rows() > 0){

            $primaryKey = $objBanco->getPrimeiraTuplaDoResultSet("Column_name");

        }

        return $primaryKey;

    }

    public function getForeignKeys($tableName){

        $arrRetorno = array();
        $objBanco = $this->getNewDatabaseInstance();
        $objBanco->query("SELECT DISTINCT
                              TABLE_NAME, COLUMN_NAME, REFERENCED_TABLE_NAME, REFERENCED_COLUMN_NAME
                          FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE
                          WHERE
                              TABLE_NAME = '{$tableName}'
                              AND CONSTRAINT_NAME <> 'PRIMARY'
                              AND REFERENCED_TABLE_NAME IS NOT NULL
                              AND CONSTRAINT_SCHEMA LIKE '{$objBanco->getDBName()}'");

        while($dadosFK = $objBanco->fetchArray()){

            $objFK = new ForeignKeyStructure();
            $objFK->tableName = $dadosFK[0];
            $objFK->fieldName = $dadosFK[1];
            $objFK->refTableName = $dadosFK[2];
            $objFK->refFieldName = $dadosFK[3];

            $arrRetorno[] = $objFK;

        }

        return $arrRetorno;

    }

    public function isTableInList($searchTable){

        foreach($this->arrTableObjList as $tableObj){

            if($tableObj->tableName == $searchTable)
                return true;

        }

        return false;

    }

    public function getRandomPrimaryKey($tableName){

        $strForbiddenPKs = "";
        if(array_key_exists($tableName, $this->arrChavesProibidasNoCrud)) {

            $strForbiddenPKs = implode(",", $this->arrChavesProibidasNoCrud[$tableName]);

        }

        $pkField = $this->getPrimaryKey($tableName);

        if(strlen($strForbiddenPKs) > 0){

            $whereClause = "WHERE {$pkField} NOT IN ({$strForbiddenPKs})";

        }

        $this->objBanco->query("SELECT {$pkField} FROM {$tableName} {$whereClause} ORDER BY RAND() LIMIT 1");
        while($dados = $this->objBanco->fetchArray()){

            return $dados[0];

        }

        return null;

    }

    public function performCrudOperations($tableName, $recordCount, $isFKTable=false){
        
        $tempJsonRecords = array();
        if(in_array($tableName, $this->arrAlreadyProcessedTables))
            return;

        //colca mesmo a tabela de FK que não será processada, para evitar verificações futuras
        $this->arrAlreadyProcessedTables[] = $tableName;

        //historico de operações, para retorno
        $objTableHistory = new TableOperationHistory($tableName);

        if($isFKTable && $this->doNotOperateOnFKTableIfNotInList && !$this->isTableInList($tableName)) {

            $objTableHistory->addHistoryMessage("Operações na tabela {$tableName} nao permitidas, pois as operações em tabelas de chave estrangeira estão desativadas. Ignorando operações...");
            $this->results->addTable($objTableHistory);
            return;

        }

        $forceCurrentOperation = null;
        //checa se existe ID válido para edição/remoção
        if(!$this->checkIfTableHasValidUpdatableIds($tableName)){

            if(in_array(Operations::INSERT, $this->arrOperations)) {

                $forceCurrentOperation = Operations::INSERT;

            }
            else {

                $objTableHistory->addHistoryMessage("Não existe nenhuma chave fora da blacklist para a tabela {$tableName}. Ignorando operações...");
                $this->results->addTable($objTableHistory);
                return;

            }

        }


        $arrFKs = $this->getForeignKeys($tableName);
        foreach($arrFKs as $objFK){

            $refTableName = $objFK->refTableName;
            $refRecordCount = $this->getRecordCount($refTableName);

            if($refRecordCount == null || $refRecordCount == 0)
                $refRecordCount = ceil($recordCount / 10);

            if(!in_array($refTableName, $this->arrAlreadyProcessedTables)) {

                $this->performCrudOperations($refTableName, $refRecordCount, true);

            }

        }

        if(!in_array($tableName, $this->tabelasIgnorarAlteracoes)) {

            $arrFields = $this->getFieldList($tableName);
            $className = "EXTDAO_" . ucfirst($tableName);
            $idUsuario = EXTDAO_Usuario::getFirstIdUsuario($this->objBanco);

            for ($i = 0; $i < $recordCount; $i++) {

                //operacao aleatoria
                $currentOperation = ($forceCurrentOperation != null ) ? $forceCurrentOperation: $this->getRandomAllowedOperation();

                $objEntidade = call_user_func_array(array($className, "factory"), array());
                
//                print_r($objEntidade);
//                exit();
                //PK aleatoria
                $PK = $this->getRandomPrimaryKey($tableName);

                //operacao aleatoria
                $currentOperation = ($PK == null) ? Operations::INSERT : $currentOperation;

                //se for fazer insert e o insert não for permitido...
                if(!in_array($currentOperation, $this->arrOperations))
                    return;

                if($currentOperation == Operations::DELETE){

                    if($PK != null) {

                        if($this->isToGenerateJson) {

                            //estrutura para o JSON
                            $objJsonOperacao = new JsonRecordsStructure($PK, JsonOperation::DELETE);
                            $tempJsonRecords[] = $objJsonOperacao;

                        }

                        $this->objBanco->iniciarTransacao();
                        $objEntidade->delete($PK, true,  Helper::SESSION("corporacao_id") , $idUsuario);
                        $this->objBanco->commitTransacao();

                        if(!$this->objBanco->hasErrors)
                            $objTableHistory->addHistoryItem($PK, Operations::DELETE);
                        else
                            $objTableHistory->addHistoryMessage("Falha ao remover registro de chave {$PK}.");

                        continue;

                    }
                    else{

                        if(in_array(Operations::INSERT, $this->arrOperations))
                            $currentOperation = Operations::INSERT;
                        else
                            return;

                    }

                }

                $arrCamposUpdate = array();
                foreach ($arrFields as $objField) {

                    //flag, padrao para prosseguir com a operação
                    $naoProsseguir = false;

                    $fieldType = $objField->type;
                    $nullable = $objField->nullable;
                    $maxLength = $objField->maxLength;
                    $fieldName = $objField->fieldName;

                    if($currentOperation == Operations::UPDATE){

                        //probabilidade de 30% de fazer o update no campo corrente - desativado
                        if(rand(1, 10) > 3 && false) {

                            $objEntidade->$fieldName = null;
                            continue;

                        }

                        $arrCamposUpdate[$objField->fieldName] = true;

                    }

                    $objFK = $this->getObjFK($arrFKs, $fieldName);

                    //FK
                    if ($objFK != null) {

                        $referencePK = $this->getRandomPrimaryKey($objFK->refTableName);

                        if ($referencePK != null) {

                            $randomValue = $referencePK;

                        }
                        elseif($nullable) {

                            $randomValue = null;

                        }
                        else {

                            $objTableHistory->addHistoryMessage("Não foi possível fazer operação devido a falta de entidades na tabela {$objFK->refTableName}, e existe chave estrangeira não-nula para esta tabela.");

                            $naoProsseguir = true;
                            continue;

                        }

                    }
                    //campo convencional
                    else {

                        $randomValue = $this->getRandomValue($fieldType, $maxLength);

                    }

                    if ($nullable)
                        $objEntidade->$fieldName = $this->getRandomValueOrNull($randomValue);
                    else
                        $objEntidade->$fieldName = $randomValue;

                }

                if(!$naoProsseguir) {

                    $objEntidade->formatarParaSQL();

                    if ($currentOperation == Operations::INSERT) {
                        $this->objBanco->iniciarTransacao();
                        $objEntidade->insert(true,  Helper::SESSION("corporacao_id") , $idUsuario);
//                        echo "entrou";
                        $this->objBanco->commitTransacao();

                        //$idEntidade = $objEntidade->getIdDoUltimoRegistroInserido();
                        $idEntidade = $objEntidade->getId();
                        
                        if($this->isToGenerateJson) {

                            //estrutura para o JSON
                            $objJsonOperacao = new JsonRecordsStructure($idEntidade, JsonOperation::INSERT);
                            $objJsonOperacao->setArrFields($objEntidade, $arrFields);
                            $tempJsonRecords[] = $objJsonOperacao;

                        }

                        if(!$this->objBanco->hasErrors)
                            $objTableHistory->addHistoryItem($idEntidade, Operations::INSERT);
                        else
                            $objTableHistory->addHistoryMessage("Falha ao atualizar registro.");


                    }
                    elseif ($currentOperation == Operations::UPDATE && count($arrCamposUpdate) > 0) {

                        $randId = $this->getRandomPrimaryKey($tableName);

                        if($randId != null) {
                            $this->objBanco->iniciarTransacao();
                            $objEntidade->update($randId, null, "", true,  Helper::SESSION("corporacao_id") , $idUsuario);
                            $this->objBanco->commitTransacao();

                            if($this->isToGenerateJson) {

                                //estrutura para o JSON
                                $objJsonOperacao = new JsonRecordsStructure($PK, JsonOperation::UPDATE);
                                $objJsonOperacao->setArrFields($objEntidade, $arrFields);
                                $tempJsonRecords[] = $objJsonOperacao;

                            }

                            if(!$this->objBanco->hasErrors)
                                $objTableHistory->addHistoryItem($randId, Operations::UPDATE);
                            else
                                $objTableHistory->addHistoryMessage("Falha ao atualizar registro de íncide {$randId}");
                        }

                    }

                }

            }

            $this->tempJsonStructure->$tableName = $tempJsonRecords;
            $this->results->addTable($objTableHistory);

        }
        return new Mensagem(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO);
    }

    public function getObjFK($arrFKs, $fieldName){

        foreach($arrFKs as $objFK){

            if($objFK->fieldName == $fieldName){

                return $objFK;

            }

        }

        return null;

    }

    public function getRandomValueOrNull($randomValue){

        $ref = rand(1, 100);
        return ($ref < 90) ? $randomValue : null;

    }

    public function getRandomValue($fieldType, $maxLength){

        switch(strtolower($fieldType)){

            case "int":
            case "tinyint":
                return $this->getRandomInteger($maxLength);
                break;
            case "float":
            case "double":
            case "decimal":
                return $this->getRandomFloat($maxLength);
                break;
            case "varchar":
            case "char":
                return $this->getRandomString($maxLength);
                break;
            case "text":
            case "tinytext":
            case "mediumtext":
                return $this->getRandomString(200);
                break;
            case "date":
                return $this->getRandomDate();
                break;
            case "time":
                return $this->getRandomTime();
                break;
            case "datetime":
                return $this->getRandomDateTime();
                break;

        }

        return null;

    }

    public function getRandomString($maxLength){

        if($maxLength > 200) $maxLength = 150;
        if($maxLength < 1) $maxLength = 1;

        $length = rand(1, $maxLength);
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';

        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return $randomString;

    }

    public function getRandomInteger($maxLength){

        return rand(0, pow(10, $maxLength)-1);

    }

    public function getRandomFloat($maxLength){

        $partes = explode(",", $maxLength);
        $maxInteiro = $partes[0];
        $maxDecimal = $partes[1];

        return $this->getRandomInteger($maxInteiro) . "." . $this->getRandomInteger($maxDecimal);

    }

    public function getRandomDate(){

        return date("Y-m-d", $this->getRandomTimestamp());

    }

    public function getRandomTime(){

        return date("H:i:s", $this->getRandomTimestamp());

    }

    public function getRandomDateTime(){

        return date("Y-m-d H:i:s", $this->getRandomTimestamp());

    }

    public function getRandomTimestamp(){

        $timestampNow = time();
        $segundosEmUmDia = 24*60*60;
        return rand($timestampNow-(100*$segundosEmUmDia), $timestampNow+(100*$segundosEmUmDia));

    }

    public function factory(){

        return new BO_Database_Populator();

    }

}

class FieldStructure {

    public $fieldName;
    public $type;
    public $maxLength;
    public $nullable;

}

class ForeignKeyStructure {

    public $tableName;
    public $fieldName;
    public $refTableName;
    public $refFieldName;

}

class JsonRecordsStructure {

    public $operation;
    public $primaryKey;
    public $arrFields;

    public function __construct($primaryKey, $operation){

        $this->operation = $operation;
        $this->primaryKey = $primaryKey;

    }

    public function setArrFields($objEntidade, $arrFields){

        $obj = null;
        if ($this->operation != JsonOperation::DELETE) {

            if(is_array($arrFields)) {

                $objEntidade->select($this->primaryKey);
                $obj = new stdClass();

                foreach($arrFields as $field) {

                    $fieldName = $field->fieldName;
                    $valor = $objEntidade->$fieldName;
                    $obj->$fieldName = $valor;

                }

            }

        }

        $this->fields = $obj;

    }

}

class JsonOperation {

    const UPDATE = "U";
    const DELETE = "D";
    const INSERT = "I";

}

class Operations {

    const UPDATE = "update";
    const DELETE = "delete";
    const INSERT = "insert";

}

class GenerationResult {

    public $success = true;
    public $message = "";
    public $arrTables = array();
    public $jsonFileUrl = "";

    public function addTable(TableOperationHistory $table){

        $this->arrTables[] = $table;

    }

}

class TableOperationHistory {

    public $tableName;
    public $history;

    public function __construct($tableName){

        $this->tableName = $tableName;
        $this->history = array();
    }

    public function addHistoryMessage($message){

        $this->history[] = utf8_encode($message);

    }

    public function addHistoryItem($pk, $operation){

        $currentDatetime = Helper::getDiaEHoraAtual();
        $strItem = "";

        switch ($operation) {

            case Operations::INSERT:
                $strItem = "Adição do registro de ID {$pk} em {$currentDatetime}.";
                break;

            case Operations::UPDATE:
                $strItem = "Edição do registro de ID {$pk} em {$currentDatetime}.";
                break;

            case Operations::DELETE:
                $strItem = "Remoção do registro de ID {$pk} em {$currentDatetime}.";
                break;

        }

        $this->history[] = utf8_encode($strItem);

    }

}

?>
