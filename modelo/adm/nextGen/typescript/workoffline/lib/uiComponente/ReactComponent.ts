import * as React from 'react';

import { ReactProps } from 'lib/geral';

export class ReactComponent<P extends ReactProps, S> extends React.Component<P, S>
{
    public constructor(reactProps? : P, reactContext? : any)
    {
        super(reactProps || {} as P, reactContext || null);                 
    }

}