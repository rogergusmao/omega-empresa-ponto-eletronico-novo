/*

Arquivo gerado atrav�s de gerador de c�digo em 19/01/2018 as 19:58:43.
Para que o arquivo n�o seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

Tabela correspondente: usuario_tipo
Sobrescrita de dados: ---SOBRESCRITA_NAO_AUTORIZADA---

*/

omegaApp.controller('formUsuarioTipoController',

    ['$scope', '$rootScope', '$element', '$window', '$interval', '$timeout', 'dataService', 'Upload',

     function ($scope, $rootScope, $element, $window, $interval, $timeout, dataService, Upload)
     {
         var vm = this;
         var generatedTasks = omegaApp.generatedFormUsuarioTipoController($scope, $rootScope, $element, $window, $interval, $timeout, dataService, Upload, vm);

         try
         {
             vm.rootContollerElement = $element;
             vm.fieldsTypes = {};

             vm.isModalView = false;
             vm.isEdition = false;
             vm.entityIdForEdition = null;

             vm.formData = {
                 nome: null,
                 nomeVisivel: null,
                 statusBoolean: true,
                 itensMenuSelecionados: []
             };

             vm.disabledFields = {
                 fieldNome: false,
                 fieldNomeVisivel: false,
                 fieldStatusBoolean: false
             };

             vm.requiredFields = {
                 fieldNome: true,
                 fieldNomeVisivel: true,
                 fieldStatusBoolean: false
             };

             vm.relatedEntities =
                 {};

             vm.fieldsParameters = {
                 fieldNome: null,
                 fieldNomeVisivel: null,
                 fieldStatusBoolean: null
             };

             vm.arrForeignKeys = ['corporacaoId'];

             vm.submitForm = function (formInstance)
             {
                 if ($rootScope.validateForm(formInstance, vm))
                 {
                     var postData = RemoteDataUtil.refactorRemoteRequest(vm.formData, vm.relatedEntities);
                     if (AngularControllerUtil.isEditionMode(vm))
                     {
                         dataService.editUsuarioTipo(postData).then(function (returnContent)
                         {
                             if (RemoteDataUtil.validateRemoteResponse(returnContent))
                             {
                                 var successMessage = RemoteDataUtil.getResponseMessageString(returnContent);
                                 $rootScope.defaultAfterUpdateAction(vm, 'after-usuarioTipo-update-template.html', successMessage);
                             }
                             else
                             {
                                 RemoteDataUtil.defaultRemoteErrorHandling(returnContent, vm);
                             }
                         });
                     }
                     else
                     {
                         dataService.addUsuarioTipo(postData).then(function (returnContent)
                         {
                             if (RemoteDataUtil.validateRemoteResponse(returnContent))
                             {
                                 var successMessage = RemoteDataUtil.getResponseMessageString(returnContent);
                                 $rootScope.defaultAfterInsertAction(vm, 'after-usuarioTipo-insert-template.html', successMessage);
                             }
                             else
                             {
                                 RemoteDataUtil.defaultRemoteErrorHandling(returnContent, vm);
                             }
                         });
                     }
                 }

             };

             var relationshipObject = null;

             vm.isInitialLoadFinished = false;
             $timeout(function ()
             {
                 if (AngularControllerUtil.isEditionMode(vm))
                 {
                     var filterParameters = AngularControllerUtil.getFilterParametersForEdition(vm);
                     dataService.getUsuarioTipo(filterParameters).then(function (responseContent)
                     {
                         if (RemoteDataUtil.validateRemoteResponse(responseContent, [PROTOCOLO_SISTEMA.OPERACAO_REALIZADA_COM_SUCESSO]))
                         {
                             var responseObject = RemoteDataUtil.getResponseObject(responseContent);
                             RemoteDataUtil.refactorInitialFormLoadRemoteResponseData(vm, responseObject, relationshipObject);

                             vm.listaItensMenuWeb = responseObject.listaDeItensDoMenuSistemaWeb;
                             if (!GeneralUtil.isNullOrEmptyArray(responseObject.listaDeItensDoMenuSistemaWebDoTipoDeUsuario))
                             {
                                 vm.formData.itensMenuSelecionados = responseObject.listaDeItensDoMenuSistemaWebDoTipoDeUsuario;
                             }
                             else
                             {
                                 vm.formData.itensMenuSelecionados = [];
                             }

                             gerarListaDivididaDeItensDoMenuSistemaWeb(vm.listaItensMenuWeb);
                             OmegaFormUtil.expandSectionPanels(vm);
                         }
                         else
                         {
                             RemoteDataUtil.defaultRemoteErrorHandling(responseContent);
                         }

                         $timeout(function ()
                         {
                             vm.isInitialLoadFinished = true;
                         }, 100);

                     });
                 }
                 else
                 {
                     dataService.getAllComboBoxesDataForUsuarioTipoForm().then(function (returnContent)
                     {
                         if (RemoteDataUtil.validateRemoteResponse(returnContent))
                         {
                             var responseObject = RemoteDataUtil.getResponseObject(returnContent);
                             RemoteDataUtil.refactorInitialFormLoadRemoteResponseData(vm, responseObject, relationshipObject);

                             vm.formData.statusBoolean = true;
                             vm.listaItensMenuWeb = responseObject.listaDeItensDoMenuSistemaWeb;
                             gerarListaDivididaDeItensDoMenuSistemaWeb(vm.listaItensMenuWeb);
                         }
                         else
                         {
                             LogUtil.logRemoteResponseError(returnContent);
                         }

                         $timeout(function ()
                         {
                             vm.isInitialLoadFinished = true;
                         }, 100);

                     });
                 }

             }, 200);

             vm.getListOfItemMenuSistemaWeb = function ()
             {
                 if (!vm.isInitialLoadFinished)
                 {
                     return;
                 }

                 dataService.getListOfItemMenuSistemaWebForUsuarioTipoForm().then(function (returnContent)
                 {
                     if (RemoteDataUtil.validateRemoteResponse(returnContent))
                     {
                         vm.listaItensMenuWeb = RemoteDataUtil.getResponseObject(returnContent);
                         gerarListaDivididaDeItensDoMenuSistemaWeb(vm.listaItensMenuWeb);
                     }
                     else
                     {
                         LogUtil.logRemoteResponseError(returnContent);
                     }
                 });
             };

             var gerarListaDivididaDeItensDoMenuSistemaWeb = function ()
             {
                 if (ArrayUtil.isArray(vm.listaItensMenuWeb))
                 {
                     vm.listaItensMenuWebEsquerda = vm.listaItensMenuWeb.slice(0, Math.floor(vm.listaItensMenuWeb.length / 2));
                     vm.listaItensMenuWebDireita = vm.listaItensMenuWeb.slice(Math.floor(vm.listaItensMenuWeb.length / 2), vm.listaItensMenuWeb.length);
                 }
             };

             vm.getMenuWebItemClass = function (nivelItemMenu)
             {
                 return "menu-item-nivel-" + nivelItemMenu;
             };

             var deselecionarItemDoMenu = function (itemMenu)
             {
                 var listaDeItensSelecionados = vm.formData.itensMenuSelecionados;
                 for (var i = 0; i < listaDeItensSelecionados.length; i++)
                 {
                     if (listaDeItensSelecionados[i].chaveItem == itemMenu.chaveItem)
                     {
                         listaDeItensSelecionados.splice(i, 1);
                     }
                 }

                 vm.formData.isAdministradorWeb = (vm.formData.itensMenuSelecionados.length == vm.listaItensMenuWeb);

             };

             var selecionarItemDoMenu = function (itemMenu)
             {
                 var listaDeItensSelecionados = vm.formData.itensMenuSelecionados;
                 for (var i = 0; i < listaDeItensSelecionados.length; i++)
                 {
                     if (listaDeItensSelecionados[i].chaveItem == itemMenu.chaveItem)
                     {
                         return;
                     }
                 }
                 vm.formData.itensMenuSelecionados.push(angular.copy(itemMenu));
                 vm.formData.isAdministradorWeb = (vm.formData.itensMenuSelecionados.length == vm.listaItensMenuWeb);

             };

             vm.changeMenuItem = function (menuItem, isChecked)
             {
                 var chaveItem = menuItem.chaveItem;
                 if (isChecked)
                 {
                     selecionarItemDoMenu(menuItem);
                     for (var i = 0; i < vm.listaItensMenuWeb.length; i++)
                     {
                         var itemAtual = vm.listaItensMenuWeb[i];
                         if (itemAtual.itemPai == chaveItem)
                         {
                             selecionarItemDoMenu(itemAtual);
                             vm.changeMenuItem(itemAtual, isChecked);
                         }
                     }
                 }
                 else
                 {
                     deselecionarItemDoMenu(menuItem);
                     for (var i = 0; i < vm.listaItensMenuWeb.length; i++)
                     {
                         var itemAtual = vm.listaItensMenuWeb[i];
                         if (itemAtual.itemPai == chaveItem)
                         {
                             deselecionarItemDoMenu(itemAtual);
                             vm.changeMenuItem(itemAtual, isChecked);
                         }
                     }
                 }
             };

             vm.loadListOfUsuarioTipo = function ()
             {
                 document.location.href = 'index.php?tipo=lists&page=usuarioTipo';
             };

             vm.addNewUsuarioTipo = function ()
             {
                 document.location.href = document.location.href;
             };

         }
         catch (ex)
         {
             ApplicationUtil.showErrorMessage(ex.message, 'Erro no controller formUsuarioController', vm);
         }

     }]
);
