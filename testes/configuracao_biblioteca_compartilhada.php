<?php

class SingletonRaizWorkspace
{
    public static $raizWorkspace = null;
}

function acharRaizWorkspace()
{
    if (SingletonRaizWorkspace::$raizWorkspace != null)
    {
        return SingletonRaizWorkspace::$raizWorkspace;
    }
    $pathDir = '';
    $niveis = 0;

    if (php_sapi_name() != 'cli')
    {

        $pathDir = $_SERVER["SCRIPT_FILENAME"];
        $niveis = substr_count($pathDir, "/");
    }
    else
    {

        $pathDir = getcwd();
        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN')
        {
            $niveis = substr_count($pathDir, "\\");
        }
        else
        {
            $niveis = substr_count($pathDir, "/");
        }

    }

    $root = "";

    for ($i = 0; $i < $niveis; $i++)
    {
        if (is_dir("{$root}__workspaceRoot"))
        {

            SingletonRaizWorkspace::$raizWorkspace = $root;
            return SingletonRaizWorkspace::$raizWorkspace;

        }
        else
        {
            $root .= "../";
        }
    }
    SingletonRaizWorkspace::$raizWorkspace = "";
    return SingletonRaizWorkspace::$raizWorkspace;
}

function getWebservicesSemCache()
{
    return array();
}

define ('CORPORACAO_TESTE_PHPUNIT', 'WORK');
define('PATH_CURL_CMD', 'C:\Program Files\curl_7_53_1_openssl_nghttp2_x64');

require_once '../modelo/recursos/php/constants.php';

$workspaceRaiz = acharRaizWorkspace();

require_once $workspaceRaiz . DIRETORIO_BIBLIOTECAS_COMPARTILHADAS . '/classes/Registry.php';
Registry::add(CORPORACAO_TESTE_PHPUNIT,'corporacao');

include_once $workspaceRaiz . DIRETORIO_BIBLIOTECAS_COMPARTILHADAS . '/classes/constants.php';
require_once $workspaceRaiz . DIRETORIO_BIBLIOTECAS_COMPARTILHADAS . '/php/configuracao_corporacao_dinamica.php';
