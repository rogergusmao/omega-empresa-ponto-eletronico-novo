<?php

class Database_Comparator_Helper
{
    public function __construct()
    {
    }

    public function getTableList()
    {
        $objDatabase = new Database();
        $arrDefaultTables = array("empresa", "bairro", "cidade", "pessoa", "pais");

        $arrRetorno = array();
        foreach ($objDatabase->getTablesArray() as $table)
        {
            $obj = new stdClass();
            $obj->name = $table;
            $obj->isSelected = in_array($table, $arrDefaultTables);

            $arrRetorno[] = $obj;
        }

        echo json_encode($arrRetorno);
    }

    public function getSQLiteFiles()
    {
        $fileList = Helper::getTodosOsArquivosDoDiretorio(Helper::acharRaiz() . "database_comparator/__sqlite_files/");
        echo json_encode($fileList);
    }

    public function getSQLiteTables()
    {
        $POSTData = json_decode(file_get_contents('php://input'));
        $sqliteFilePath = $POSTData->sqliteFile;

        if (is_file($sqliteFilePath))
        {
            $objDatabase = new Database_SQLite($sqliteFilePath);
            $arrTables = $objDatabase->getTablesArray();

            echo json_encode($arrTables);
        }
        else
        {
            echo "null";
        }
    }

    public function getFilteredResults()
    {
        $POSTData = json_decode(file_get_contents('php://input'));
        $corporacaId = $POSTData->corporacaId;
        $comparedDatabaseFileName = $POSTData->comparisonDatabase;
        $arrTables = $POSTData->selectedTables;

        $objMainDatabase = new Database();
        $arrSecondaryDatabases = array(new Database_SQLite($comparedDatabaseFileName, true));

        $objComparator = new Database_Comparator($objMainDatabase, $arrSecondaryDatabases, $arrTables, $corporacaId);

        for ($i = 0; $i < count($arrSecondaryDatabases); $i++)
        {
            $comparisonResults[] = $objComparator->compareDatabases($i);
        }

        echo json_encode($comparisonResults);
    }

    public static function factory()
    {
        return new Database_Comparator_Helper();
    }
}

?>