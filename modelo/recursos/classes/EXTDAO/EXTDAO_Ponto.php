<?php

/*

Arquivo gerado atravÃ©s de gerador de cÃ³digo em 24/05/2017 as 10:02:56.
Para que o arquivo nÃ£o seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

Tabela correspondente: ponto
Sobrescrita de dados: ---SOBRESCRITA_NAO_AUTORIZADA---

*/

?>

<?php

class EXTDAO_Ponto extends DAO_Ponto
{
    public function __construct($configDAO = null)
    {
        parent::__construct($configDAO);
        $this->nomeClasse = "EXTDAO_Ponto";
    }

    protected function getFormComboBoxesData()
    {
        $comboBoxesData = new stdClass();
        $listParameters = new stdClass();
        $listParameters->listMappingType = static::LIST_MAPPING_TYPE_COMBOBOX;

        $objPessoa = new EXTDAO_Pessoa();
        $comboBoxesData->fieldPessoaId = $objPessoa->__getList($listParameters);

        $objEmpresa = new EXTDAO_Empresa();
        $comboBoxesData->fieldEmpresaId = $objEmpresa->__getList($listParameters);

        $objTipoPonto = new EXTDAO_Tipo_ponto();
        $comboBoxesData->fieldTipoPontoId = $objTipoPonto->__getList($listParameters);

        return $comboBoxesData;
    }

    public static function setDatabaseFieldNames()
    {
        if (is_null(static::$databaseFieldNames))
        {
            static::$databaseFieldNames = new stdClass();
            static::$databaseFieldNames->id = "id";
            static::$databaseFieldNames->pessoaId = "pessoa_id_INT";
            static::$databaseFieldNames->foto = "foto";
            static::$databaseFieldNames->empresaId = "empresa_id_INT";
            static::$databaseFieldNames->profissaoId = "profissao_id_INT";
            static::$databaseFieldNames->usuarioId = "usuario_id_INT";
            static::$databaseFieldNames->isEntradaBoolean = "is_entrada_BOOLEAN";
            static::$databaseFieldNames->latitudeInt = "latitude_INT";
            static::$databaseFieldNames->longitudeInt = "longitude_INT";
            static::$databaseFieldNames->tipoPontoId = "tipo_ponto_id_INT";
            static::$databaseFieldNames->corporacaoId = "corporacao_id_INT";
            static::$databaseFieldNames->dataSec = "data_SEC";
            static::$databaseFieldNames->dataOffsec = "data_OFFSEC";

            static::$databaseFieldNames->dataSecDe = "data_SEC";
            static::$databaseFieldNames->dataSecAte = "data_SEC";

        }
    }

    public static function setDatabaseFieldTypes()
    {
        if (is_null(static::$databaseFieldTypes))
        {
            static::$databaseFieldTypes = new stdClass();
            static::$databaseFieldTypes->id = static::TIPO_VARIAVEL_INTEGER;
            static::$databaseFieldTypes->pessoaId = static::TIPO_VARIAVEL_INTEGER;
            static::$databaseFieldTypes->foto = static::TIPO_VARIAVEL_TEXT;
            static::$databaseFieldTypes->empresaId = static::TIPO_VARIAVEL_INTEGER;
            static::$databaseFieldTypes->profissaoId = static::TIPO_VARIAVEL_INTEGER;
            static::$databaseFieldTypes->usuarioId = static::TIPO_VARIAVEL_INTEGER;
            static::$databaseFieldTypes->isEntradaBoolean = static::TIPO_VARIAVEL_BOOLEAN;
            static::$databaseFieldTypes->latitudeInt = static::TIPO_VARIAVEL_INTEGER;
            static::$databaseFieldTypes->longitudeInt = static::TIPO_VARIAVEL_INTEGER;
            static::$databaseFieldTypes->tipoPontoId = static::TIPO_VARIAVEL_INTEGER;
            static::$databaseFieldTypes->corporacaoId = static::TIPO_VARIAVEL_INTEGER;
            static::$databaseFieldTypes->dataSec = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
            static::$databaseFieldTypes->dataOffsec = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;

            static::$databaseFieldTypes->dataSecDe = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
            static::$databaseFieldTypes->dataSecAte = static::TIPO_VARIAVEL_TIME_IN_SECONDS;

        }
    }

    public static function setListAliasTypes()
    {
        if (is_null(static::$listAliasTypes))
        {
            static::$listAliasTypes = new stdClass();
            static::$listAliasTypes->pessoa__nome = static::TIPO_VARIAVEL_TEXT;
            static::$listAliasTypes->empresa__nome = static::TIPO_VARIAVEL_TEXT;
            static::$listAliasTypes->profissao__nome = static::TIPO_VARIAVEL_TEXT;
            static::$listAliasTypes->usuario__nome = static::TIPO_VARIAVEL_TEXT;
            static::$listAliasTypes->tipo_ponto__nome = static::TIPO_VARIAVEL_TEXT;
            static::$listAliasTypes->ponto__id = static::TIPO_VARIAVEL_INTEGER;
            static::$listAliasTypes->ponto__corporacao_id_INT = static::TIPO_VARIAVEL_INTEGER;
            static::$listAliasTypes->ponto__is_entrada_BOOLEAN = static::TIPO_VARIAVEL_BOOLEAN;
            static::$listAliasTypes->ponto__nome = static::TIPO_VARIAVEL_TEXT;
            static::$listAliasTypes->ponto__data_SEC = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
            static::$listAliasTypes->ponto__data_OFFSEC = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
        }
    }

    public static function setListAliasRelatedAttributes()
    {
        if (is_null(static::$listAliasRelatedAttributes))
        {
            static::$listAliasRelatedAttributes = new stdClass();
            static::$listAliasRelatedAttributes->pessoa__nome = "pessoaNome";
            static::$listAliasRelatedAttributes->empresa__nome = "empresaNome";
            static::$listAliasRelatedAttributes->profissao__nome = "profissaoNome";
            static::$listAliasRelatedAttributes->usuario__nome = "nomeUsuario";
            static::$listAliasRelatedAttributes->tipo_ponto__nome = "tipoPontoNome";
            static::$listAliasRelatedAttributes->ponto__id = "id";
            static::$listAliasRelatedAttributes->ponto__cadastro_OFFSEC = "cadastroOffsec";
            static::$listAliasRelatedAttributes->ponto__cadastro_OFFSEC = "cadastroOffsec";
            static::$listAliasRelatedAttributes->ponto__corporacao_id_INT = "corporacaoId";
            static::$listAliasRelatedAttributes->ponto__cadastro_OFFSEC = "cadastroOffsec";
            static::$listAliasRelatedAttributes->ponto__is_entrada_BOOLEAN = "isEntradaBoolean";
            static::$listAliasRelatedAttributes->ponto__nome = "nome";
            static::$listAliasRelatedAttributes->ponto__data_SEC = "dataSec";
            static::$listAliasTypes->ponto__data_OFFSEC = "dataOffSec";
        }
    }

    public function __getList($parameters = null, $idCorporacao = null)
    {
        if (is_null($parameters))
        {
            $parameters = Helper::getPhpInputObject();
        }

        try
        {
            $filterParameters = new stdClass();
            if (isset($parameters->filterParameters))
            {
                $filterParameters = $parameters->filterParameters;
            }

            if (is_null($idCorporacao))
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            ListParameters::addFilterParameterToObject($filterParameters, "corporacaoId", $idCorporacao);

            $sortingParameters = null;
            if (isset($parameters->sortingParameters))
            {
                $sortingParameters = $parameters->sortingParameters;
            }

            $paginationParameters = null;
            if (isset($parameters->paginationParameters))
            {
                $paginationParameters = $parameters->paginationParameters;
            }

            $listMappingType = static::getListMappingType($parameters->listMappingType);
            $mainTableAlias = "p";

            $whereClause = static::getWhereClauseForFilter($filterParameters, $mainTableAlias);
            $orderByClause = static::getOrderByClauseForFilter($sortingParameters, $mainTableAlias);
            $limitClause = static::getLimitClauseForFilter($paginationParameters);

            $queryWithoutLimit = "SELECT p.id FROM ponto p {$whereClause}";
            $query = "SELECT p1.nome AS pessoa__nome, 
                      e.nome AS empresa__nome, 
                      p2.nome AS profissao__nome, 
                      u.nome AS usuario__nome, 
                      tp.nome AS tipo_ponto__nome, 
                      p.id AS ponto__id, 
                      p.corporacao_id_INT AS ponto__corporacao_id_INT,
                      IF (p.is_entrada_BOOLEAN IS NULL, 0, p.is_entrada_BOOLEAN) AS ponto__is_entrada_BOOLEAN,                      
                      p.data_SEC AS ponto__data_SEC,
                      p.data_OFFSEC AS ponto__data_OFFSEC
                      FROM ponto p 
                        LEFT JOIN pessoa p1 ON p1.id = p.pessoa_id_INT 
                        LEFT JOIN empresa e ON e.id = p.empresa_id_INT 
                        LEFT JOIN profissao p2 ON p2.id = p.profissao_id_INT 
                        LEFT JOIN usuario u ON u.id = p.usuario_id_INT 
                        LEFT JOIN tipo_ponto tp ON tp.id = p.tipo_ponto_id_INT 
                      {$whereClause} {$orderByClause} {$limitClause}";

            $msg = $this->database->queryMensagem($queryWithoutLimit);
            if ($msg != null && $msg->erro())
            {
                return $msg;
            }

            $totalNumberOfRecords = $this->database->rows();
            if ($totalNumberOfRecords == 0)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
            }

            $msg = $this->database->queryMensagem($query);

            //operacao realizaca com sucesso
            if ($msg == null)
            {
                if ($this->database->rows() == 0)
                {
                    return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                }

                $resultSet = $this->database->result;

                $objReturn = new stdClass();
                $objReturn->dataSet = static::mapResultSetDataToList($resultSet, $listMappingType, static::$listAliasTypes);

                if (!Helper::isNullOrEmpty($paginationParameters->recordsPerPage) && !Helper::isNullOrEmpty($paginationParameters->currentPage))
                {
                    $lastPage = ceil($totalNumberOfRecords / $paginationParameters->recordsPerPage);
                    $objReturn->paginationParameters = new PaginationParams($paginationParameters->recordsPerPage, $paginationParameters->currentPage, $lastPage);
                }

                return new Mensagem_generica($objReturn);
            }
            else
            {
                return $msg;
            }
        }
        catch (Exception $ex)
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
        }
    }

    public function factory()
    {
        return new EXTDAO_Ponto();
    }
}

?>
