/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function alertaRemover(el, id, email) {
    var $el = $(el);

    bootbox.confirm(
        "Tem certeza que deseja remover o usu�rio "+email+"?", 
        function (result) {
            if (result) {
                removeUsuario($el, id);
            }
        });
}
    
    
function removeUsuario($el, id) {
    
    adicionarLoading($el, 'removendo');
    
    var post = {
        id_usuario: id
    };
    $el.css('display', 'none');
    var funcaoChamada = function () {

        try {

            var arg = arguments[0];
            var resultado = JSON.parse(arg);

            if (resultado['mCodRetorno'] == PROTOCOLO_SISTEMA.OPERACAO_REALIZADA_COM_SUCESSO) {

                bootbox.alert({
                    message: resultado['mMensagem']
                });
                $el.siblings('.editar').css('display', 'none');
                $el.siblings('.detalhes').css('display', 'none');
                $el.after('<span>Removido</span>');
                return;

            }
            else if (resultado['mCodRetorno'] != PROTOCOLO_SISTEMA.ERRO_COM_SERVIDOR) {

                bootbox.alert({
                    message: resultado['mMensagem']
                });
                
                $el.css('display', 'inline-block');

            }
            else {

                bootbox.alert({
                    message: MSG_SUPORTE
                });
                
                $el.css('display', 'inline-block');
                return;

            }

        }

        catch (exception) {
            bootbox.alert({
                message: MSG_SUPORTE
            });
            $el.css('display', 'inline-block');
            
        } finally{
            removerLoading($el);
        }

    };

    carregarValorRemotoAsyncPost(
    'webservice.php?class=BO_SICOB&action=retirarClienteDaAssinatura',
    null,
    funcaoChamada,
    post);

}