<?php


        /*

        Arquivo gerado através de gerador de código em 12/05/2018 as 14:14:33.
        Para que o arquivo não seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

        Tabela correspondente: pessoa
        Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

        */
        


class DAO_Pessoa extends Generic_DAO
                        {
                            const NOME_ENTIDADE = "pessoa";

                            public static $nomeTabela = null;
                            public static $idSistemaTabela = false;
                            public static $databaseFieldsRelatedAttributes = null;
	                        public static $databaseFieldTypes = null;
	                        public static $databaseFieldNames = null;
	                        public static $listAliasTypes = null;
	                        public static $listAliasRelatedAttributes = null;
	                        
	                        public $id;
public $identificador;
public $nome;
public $nomeNormalizado;
public $tipoDocumentoId;
public $numeroDocumento;
public $isConsumidorBoolean;
public $sexoId;
public $telefone;
public $celular;
public $operadoraId;
public $celularSms;
public $email;
public $logradouro;
public $numero;
public $complemento;
public $cidadeId;
public $bairroId;
public $latitudeInt;
public $longitudeInt;
public $foto;
public $cadastroSec;
public $cadastroOffsec;
public $corporacaoId;
public $dataNascimentoSec;
public $dataNascimentoOffsec;
public $relatorioId;
public $indEmailValidoBoolean;
public $indCelularValidoBoolean;

public $labelId;
public $labelIdentificador;
public $labelNome;
public $labelNomeNormalizado;
public $labelTipoDocumentoId;
public $labelNumeroDocumento;
public $labelIsConsumidorBoolean;
public $labelSexoId;
public $labelTelefone;
public $labelCelular;
public $labelOperadoraId;
public $labelCelularSms;
public $labelEmail;
public $labelLogradouro;
public $labelNumero;
public $labelComplemento;
public $labelCidadeId;
public $labelBairroId;
public $labelLatitudeInt;
public $labelLongitudeInt;
public $labelFoto;
public $labelCadastroSec;
public $labelCadastroOffsec;
public $labelCorporacaoId;
public $labelDataNascimentoSec;
public $labelDataNascimentoOffsec;
public $labelRelatorioId;
public $labelIndEmailValidoBoolean;
public $labelIndCelularValidoBoolean;

public static $arrPK = array("id");

public function __construct($configDAO = null)
        {
            parent::__construct($configDAO);
            static::__setStaticValues($this->database);           
        }

public static function idSistemaTabela($databaseObject = null){
            if(!(static::$idSistemaTabela))
            {
                static::$idSistemaTabela = EXTDAO_Sistema_tabela::getIdDaSistemaTabela(static::nomeTabela(), $databaseObject);
            }
            return static::$idSistemaTabela;
        }

public function getIdSistemaTabela(){
            return static::idSistemaTabela();
        }

public static function nomeTabela(){
            if(is_null(static::$nomeTabela))
            {
                static::$nomeTabela = "pessoa";
            }
            return static::$nomeTabela;
        }

public function getNomeTabela(){
            return static::nomeTabela();
        }

public static function __setStaticValues($databaseObject = null)
                   {    
                        static::nomeTabela();
                        
                        static::idSistemaTabela($databaseObject);
                   
                        static::setDatabaseFieldNames();
                        static::setDatabaseFieldTypes();
                        static::setDatabaseFieldsRelatedAttributeNames();
                        
                        static::setListAliasTypes();
                        static::setListAliasRelatedAttributes();
                   }

public function getExpressaoLabel()
                   {
                        return "{id}";
                   }

public static function setDatabaseFieldNames()
        {
            if(is_null(static::$databaseFieldNames))
            {
                static::$databaseFieldNames = new stdClass();
                static::$databaseFieldNames->id = "id";
static::$databaseFieldNames->identificador = "identificador";
static::$databaseFieldNames->nome = "nome";
static::$databaseFieldNames->nomeNormalizado = "nome_normalizado";
static::$databaseFieldNames->tipoDocumentoId = "tipo_documento_id_INT";
static::$databaseFieldNames->numeroDocumento = "numero_documento";
static::$databaseFieldNames->isConsumidorBoolean = "is_consumidor_BOOLEAN";
static::$databaseFieldNames->sexoId = "sexo_id_INT";
static::$databaseFieldNames->telefone = "telefone";
static::$databaseFieldNames->celular = "celular";
static::$databaseFieldNames->operadoraId = "operadora_id_INT";
static::$databaseFieldNames->celularSms = "celular_sms";
static::$databaseFieldNames->email = "email";
static::$databaseFieldNames->logradouro = "logradouro";
static::$databaseFieldNames->numero = "numero";
static::$databaseFieldNames->complemento = "complemento";
static::$databaseFieldNames->cidadeId = "cidade_id_INT";
static::$databaseFieldNames->bairroId = "bairro_id_INT";
static::$databaseFieldNames->latitudeInt = "latitude_INT";
static::$databaseFieldNames->longitudeInt = "longitude_INT";
static::$databaseFieldNames->foto = "foto";
static::$databaseFieldNames->cadastroSec = "cadastro_SEC";
static::$databaseFieldNames->cadastroOffsec = "cadastro_OFFSEC";
static::$databaseFieldNames->corporacaoId = "corporacao_id_INT";
static::$databaseFieldNames->dataNascimentoSec = "data_nascimento_SEC";
static::$databaseFieldNames->dataNascimentoOffsec = "data_nascimento_OFFSEC";
static::$databaseFieldNames->relatorioId = "relatorio_id_INT";
static::$databaseFieldNames->indEmailValidoBoolean = "ind_email_valido_BOOLEAN";
static::$databaseFieldNames->indCelularValidoBoolean = "ind_celular_valido_BOOLEAN";

            }
                      
        }

public static function setDatabaseFieldTypes()
        {
            if(is_null(static::$databaseFieldTypes))
            {
                static::$databaseFieldTypes = new stdClass();
                static::$databaseFieldTypes->id = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->identificador = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->nome = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->nomeNormalizado = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->tipoDocumentoId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->numeroDocumento = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->isConsumidorBoolean = static::TIPO_VARIAVEL_BOOLEAN;
static::$databaseFieldTypes->sexoId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->telefone = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->celular = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->operadoraId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->celularSms = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->email = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->logradouro = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->numero = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->complemento = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->cidadeId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->bairroId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->latitudeInt = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->longitudeInt = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->foto = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->cadastroSec = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$databaseFieldTypes->cadastroOffsec = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$databaseFieldTypes->corporacaoId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->dataNascimentoSec = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$databaseFieldTypes->dataNascimentoOffsec = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$databaseFieldTypes->relatorioId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->indEmailValidoBoolean = static::TIPO_VARIAVEL_BOOLEAN;
static::$databaseFieldTypes->indCelularValidoBoolean = static::TIPO_VARIAVEL_BOOLEAN;

            }
            
        }

public static function setDatabaseFieldsRelatedAttributeNames()
        {
            if(is_null(static::$databaseFieldsRelatedAttributes))
            {
                static::$databaseFieldsRelatedAttributes = new stdClass();
                static::$databaseFieldsRelatedAttributes->id = "id";
static::$databaseFieldsRelatedAttributes->identificador = "identificador";
static::$databaseFieldsRelatedAttributes->nome = "nome";
static::$databaseFieldsRelatedAttributes->nome_normalizado = "nomeNormalizado";
static::$databaseFieldsRelatedAttributes->tipo_documento_id_INT = "tipoDocumentoId";
static::$databaseFieldsRelatedAttributes->numero_documento = "numeroDocumento";
static::$databaseFieldsRelatedAttributes->is_consumidor_BOOLEAN = "isConsumidorBoolean";
static::$databaseFieldsRelatedAttributes->sexo_id_INT = "sexoId";
static::$databaseFieldsRelatedAttributes->telefone = "telefone";
static::$databaseFieldsRelatedAttributes->celular = "celular";
static::$databaseFieldsRelatedAttributes->operadora_id_INT = "operadoraId";
static::$databaseFieldsRelatedAttributes->celular_sms = "celularSms";
static::$databaseFieldsRelatedAttributes->email = "email";
static::$databaseFieldsRelatedAttributes->logradouro = "logradouro";
static::$databaseFieldsRelatedAttributes->numero = "numero";
static::$databaseFieldsRelatedAttributes->complemento = "complemento";
static::$databaseFieldsRelatedAttributes->cidade_id_INT = "cidadeId";
static::$databaseFieldsRelatedAttributes->bairro_id_INT = "bairroId";
static::$databaseFieldsRelatedAttributes->latitude_INT = "latitudeInt";
static::$databaseFieldsRelatedAttributes->longitude_INT = "longitudeInt";
static::$databaseFieldsRelatedAttributes->foto = "foto";
static::$databaseFieldsRelatedAttributes->cadastro_SEC = "cadastroSec";
static::$databaseFieldsRelatedAttributes->cadastro_OFFSEC = "cadastroOffsec";
static::$databaseFieldsRelatedAttributes->corporacao_id_INT = "corporacaoId";
static::$databaseFieldsRelatedAttributes->data_nascimento_SEC = "dataNascimentoSec";
static::$databaseFieldsRelatedAttributes->data_nascimento_OFFSEC = "dataNascimentoOffsec";
static::$databaseFieldsRelatedAttributes->relatorio_id_INT = "relatorioId";
static::$databaseFieldsRelatedAttributes->ind_email_valido_BOOLEAN = "indEmailValidoBoolean";
static::$databaseFieldsRelatedAttributes->ind_celular_valido_BOOLEAN = "indCelularValidoBoolean";

            }           
        }

public function setAllFieldsToHumanFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["identificador"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["nome"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["nome_normalizado"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["tipo_documento_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["numero_documento"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["is_consumidor_BOOLEAN"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["sexo_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["telefone"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["celular"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["operadora_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["celular_sms"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["email"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["logradouro"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["numero"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["complemento"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["cidade_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["bairro_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["latitude_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["longitude_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["foto"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["cadastro_SEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["cadastro_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["data_nascimento_SEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["data_nascimento_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["relatorio_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["ind_email_valido_BOOLEAN"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["ind_celular_valido_BOOLEAN"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
}

public function setAllFieldsToDatabaseFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["identificador"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["nome"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["nome_normalizado"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["tipo_documento_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["numero_documento"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["is_consumidor_BOOLEAN"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["sexo_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["telefone"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["celular"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["operadora_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["celular_sms"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["email"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["logradouro"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["numero"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["complemento"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["cidade_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["bairro_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["latitude_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["longitude_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["foto"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["cadastro_SEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["cadastro_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["data_nascimento_SEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["data_nascimento_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["relatorio_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["ind_email_valido_BOOLEAN"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["ind_celular_valido_BOOLEAN"] = static::ESTADO_FORMATACAO_CAMPO_DB;
}

  public function getFkObjTipo_documento() 
                {
                    if($this->objTipo_documento == null)
                    {                        
                        $this->objTipo_documento = new EXTDAO_Tipo_documento_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getTipo_documento_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objTipo_documento->clear();
                    }
                    elseif($this->objTipo_documento->getId() != $idFK)
                    {
                        $this->objTipo_documento->select($idFK);
                    }
                    return $this->objTipo_documento;
                }
  public function getFkObjSexo() 
                {
                    if($this->objSexo == null)
                    {                        
                        $this->objSexo = new EXTDAO_Sexo_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getSexo_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objSexo->clear();
                    }
                    elseif($this->objSexo->getId() != $idFK)
                    {
                        $this->objSexo->select($idFK);
                    }
                    return $this->objSexo;
                }
  public function getFkObjOperadora() 
                {
                    if($this->objOperadora == null)
                    {                        
                        $this->objOperadora = new EXTDAO_Operadora_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getOperadora_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objOperadora->clear();
                    }
                    elseif($this->objOperadora->getId() != $idFK)
                    {
                        $this->objOperadora->select($idFK);
                    }
                    return $this->objOperadora;
                }
  public function getFkObjCidade() 
                {
                    if($this->objCidade == null)
                    {                        
                        $this->objCidade = new EXTDAO_Cidade_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getCidade_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objCidade->clear();
                    }
                    elseif($this->objCidade->getId() != $idFK)
                    {
                        $this->objCidade->select($idFK);
                    }
                    return $this->objCidade;
                }
  public function getFkObjBairro() 
                {
                    if($this->objBairro == null)
                    {                        
                        $this->objBairro = new EXTDAO_Bairro_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getBairro_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objBairro->clear();
                    }
                    elseif($this->objBairro->getId() != $idFK)
                    {
                        $this->objBairro->select($idFK);
                    }
                    return $this->objBairro;
                }
  public function getFkObjCorporacao() 
                {
                    if($this->objCorporacao == null)
                    {                        
                        $this->objCorporacao = new EXTDAO_Corporacao_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getCorporacao_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objCorporacao->clear();
                    }
                    elseif($this->objCorporacao->getId() != $idFK)
                    {
                        $this->objCorporacao->select($idFK);
                    }
                    return $this->objCorporacao;
                }
  public function getFkObjRelatorio() 
                {
                    if($this->objRelatorio == null)
                    {                        
                        $this->objRelatorio = new EXTDAO_Relatorio_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getRelatorio_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objRelatorio->clear();
                    }
                    elseif($this->objRelatorio->getId() != $idFK)
                    {
                        $this->objRelatorio->select($idFK);
                    }
                    return $this->objRelatorio;
                }


public function setLabels()
        {$this->labelId = "id";
$this->labelIdentificador = "identificador";
$this->labelNome = "nome";
$this->labelNomeNormalizado = "nomenormalizado";
$this->labelTipoDocumentoId = "tipodocumentoidINT";
$this->labelNumeroDocumento = "numerodocumento";
$this->labelIsConsumidorBoolean = "isconsumidorBOOLEAN";
$this->labelSexoId = "sexoidINT";
$this->labelTelefone = "telefone";
$this->labelCelular = "celular";
$this->labelOperadoraId = "operadoraidINT";
$this->labelCelularSms = "celularsms";
$this->labelEmail = "email";
$this->labelLogradouro = "logradouro";
$this->labelNumero = "numero";
$this->labelComplemento = "complemento";
$this->labelCidadeId = "cidadeidINT";
$this->labelBairroId = "bairroidINT";
$this->labelLatitudeInt = "latitudeINT";
$this->labelLongitudeInt = "longitudeINT";
$this->labelFoto = "foto";
$this->labelCadastroSec = "";
$this->labelCadastroOffsec = "";
$this->labelCorporacaoId = "corporacaoidINT";
$this->labelDataNascimentoSec = "";
$this->labelDataNascimentoOffsec = "";
$this->labelRelatorioId = "";
$this->labelIndEmailValidoBoolean = "";
$this->labelIndCelularValidoBoolean = "";
}

public function __actionAdd($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                $this->setByObject($parameters);
                $this->setDataCadastroAndOffsetInSecondsFromUser($parameters);
                
                $this->formatarParaSQL();
                $msg = $this->insert(true);
                
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                return new Mensagem(null, I18N::getExpression("Pessoa adicionado com sucesso."));
                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionEdit($parameters = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try {

                $this->setByObject($parameters);
                $this->setAllFieldsToHumanFormat();
                
                $this->formatarParaSQL();
                $msg = $this->updateByInstanceUsingParameters($this->id, $parameters, true);
                if($msg != null && $msg->erro())
                {
                     return $msg;
                }
        
                return new Mensagem(null, I18N::getExpression("Pessoa editado com sucesso."));
            }
            catch(Exception $ex)
            {                
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionRemove($parameters = null)
        {
            $successulRemovalCount = 0;
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                foreach($parameters as $record)
                {
                    if(is_numeric($record->id))
                    {
                        $msg = $this->delete($record->id, true);
                        if($msg == null || $msg->ok())
                        {
                            $successulRemovalCount++;
                        }
                    }
                }
                
                if($successulRemovalCount > 0)
                {
                    $successMessage = count($successulRemovalCount) > 1 ? I18N::getExpression("{0} Pessoa foram removidos com sucesso.", $successulRemovalCount) : I18N::getExpression("Pessoa removido com sucesso.");
                    return new Mensagem(null, $successMessage);
                }
                else
                {
                    $errorMessage = count($parameters) > 1 ? I18N::getExpression("Falha ao remover Pessoa.") : I18N::getExpression("Falha ao remover Pessoa.");
                    return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, $errorMessage);                    
                }
            }
            catch (Exception $ex) 
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }

public function __getRecord($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }
            
            try
            {            
                $returnObject = new stdClass();            
                $whereClause = static::getWhereClauseForFilter($parameters->filterParameters);
                $query = "SELECT  id, identificador, nome, nome_normalizado, tipo_documento_id_INT, numero_documento, is_consumidor_BOOLEAN, sexo_id_INT, telefone, celular, operadora_id_INT, celular_sms, email, logradouro, numero, complemento, cidade_id_INT, bairro_id_INT, latitude_INT, longitude_INT, foto, cadastro_SEC, cadastro_OFFSEC, corporacao_id_INT, data_nascimento_SEC, data_nascimento_OFFSEC, relatorio_id_INT, ind_email_valido_BOOLEAN, ind_celular_valido_BOOLEAN FROM pessoa {$whereClause}";
    
                $msg = $this->database->queryMensagem($query);                
                if ($msg == null)
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Não foi encontrado nenhum registro"));
                    }                    
                    $resultSet = $this->database->result;
                    $resultObject = static::mapResultSetDataToRecord($resultSet);
                    
                    $returnObject->formData = $resultObject;
                    $returnObject->comboBoxesData = null;
                    if($parameters->loadComboBoxesData)
                    {
                        $this->setByObject($resultObject);
                        $returnObject->comboBoxesData =  $this->getFormComboBoxesData();
                    }                    
    
                    
    
                    return new Mensagem_generica($returnObject);
                } 
                else 
                {
                    return $msg;
                }
                
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }


        
        public function __getFilterComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
                
        public function __getFormComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
        
        protected function getFormComboBoxesData()
        {
            $comboBoxesData = new stdClass();
            $listParameters = new stdClass();
            $listParameters->listMappingType = static::LIST_MAPPING_TYPE_COMBOBOX;
            
            
                    
                    $objTipoDocumento = new EXTDAO_Tipo_documento();
                    $comboBoxesData->fieldTipoDocumentoId = $objTipoDocumento->__getList($listParameters);
                    
                    $objSexo = new EXTDAO_Sexo();
                    $comboBoxesData->fieldSexoId = $objSexo->__getList($listParameters);
                    
                    $objOperadora = new EXTDAO_Operadora();
                    $comboBoxesData->fieldOperadoraId = $objOperadora->__getList($listParameters);
                    
                    $objCidade = new EXTDAO_Cidade();
                    $comboBoxesData->fieldCidadeId = $objCidade->__getList($listParameters);
                    
                    $objBairro = new EXTDAO_Bairro();
                    $comboBoxesData->fieldBairroId = $objBairro->__getList($listParameters);

            return $comboBoxesData;
        }
        
        


        
        public static function setListAliasTypes()
        {
            if(is_null(static::$listAliasTypes))
            {
                static::$listAliasTypes = new stdClass();
                static::$listAliasTypes->tipo_documento__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->sexo__nome = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->operadora__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->cidade__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->bairro__nome = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->pessoa__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->pessoa__identificador = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->pessoa__nome = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->pessoa__corporacao_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->pessoa__numero_documento = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->pessoa__is_consumidor_BOOLEAN = static::TIPO_VARIAVEL_BOOLEAN;
static::$listAliasTypes->pessoa__nome = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->pessoa__telefone = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->pessoa__celular = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->pessoa__nome_normalizado = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->pessoa__celular_sms = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->pessoa__email = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->pessoa__logradouro = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->pessoa__numero = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->pessoa__complemento = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->pessoa__corporacao_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->pessoa__corporacao_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->pessoa__latitude_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->pessoa__longitude_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->pessoa__foto = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->pessoa__cadastro_SEC = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$listAliasTypes->pessoa__cadastro_OFFSEC = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
            }        
        }
        
        public static function setListAliasRelatedAttributes()
        {
            if(is_null(static::$listAliasRelatedAttributes))
            {
                static::$listAliasRelatedAttributes = new stdClass();
                static::$listAliasRelatedAttributes->tipo_documento__id = "tipoDocumentoId";
static::$listAliasRelatedAttributes->sexo__nome = "sexoNome";
static::$listAliasRelatedAttributes->operadora__id = "operadoraId";
static::$listAliasRelatedAttributes->cidade__id = "cidadeId";
static::$listAliasRelatedAttributes->bairro__nome = "bairroNome";
static::$listAliasRelatedAttributes->pessoa__id = "id";
static::$listAliasRelatedAttributes->pessoa__identificador = "identificador";
static::$listAliasRelatedAttributes->pessoa__nome = "nome";
static::$listAliasRelatedAttributes->pessoa__corporacao_id_INT = "corporacaoId";
static::$listAliasRelatedAttributes->pessoa__numero_documento = "numeroDocumento";
static::$listAliasRelatedAttributes->pessoa__is_consumidor_BOOLEAN = "isConsumidorBoolean";
static::$listAliasRelatedAttributes->pessoa__nome = "nome";
static::$listAliasRelatedAttributes->pessoa__telefone = "telefone";
static::$listAliasRelatedAttributes->pessoa__celular = "celular";
static::$listAliasRelatedAttributes->pessoa__nome_normalizado = "nomeNormalizado";
static::$listAliasRelatedAttributes->pessoa__celular_sms = "celularSms";
static::$listAliasRelatedAttributes->pessoa__email = "email";
static::$listAliasRelatedAttributes->pessoa__logradouro = "logradouro";
static::$listAliasRelatedAttributes->pessoa__numero = "numero";
static::$listAliasRelatedAttributes->pessoa__complemento = "complemento";
static::$listAliasRelatedAttributes->pessoa__corporacao_id_INT = "corporacaoId";
static::$listAliasRelatedAttributes->pessoa__corporacao_id_INT = "corporacaoId";
static::$listAliasRelatedAttributes->pessoa__latitude_INT = "latitudeInt";
static::$listAliasRelatedAttributes->pessoa__longitude_INT = "longitudeInt";
static::$listAliasRelatedAttributes->pessoa__foto = "foto";
static::$listAliasRelatedAttributes->pessoa__cadastro_SEC = "cadastroSec";
static::$listAliasRelatedAttributes->pessoa__cadastro_OFFSEC = "cadastroOffsec";
            }         
        }

        public function __getList($parameters = null, $idCorporacao = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try
            {
                $filterParameters = new stdClass();
                if(isset($parameters->filterParameters))
                {
                    $filterParameters = $parameters->filterParameters;
                }
                                    
                            
            if(is_null($idCorporacao))
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            ListParameters::addFilterParameterToObject($filterParameters, "corporacaoId", $idCorporacao);           
                
                
                $sortingParameters = null;
                if(isset($parameters->sortingParameters))
                {
                    $sortingParameters = $parameters->sortingParameters;
                }
                
                $paginationParameters = null;
                if(isset($parameters->paginationParameters))
                {
                    $paginationParameters = $parameters->paginationParameters;
                }
                
                $listMappingType = static::getListMappingType($parameters->listMappingType);
                $mainTableAlias = "p";

                $whereClause = static::getWhereClauseForFilter($filterParameters, $mainTableAlias);
                $orderByClause = static::getOrderByClauseForFilter($sortingParameters, $mainTableAlias);
                $limitClause = static::getLimitClauseForFilter($paginationParameters);

                $queryWithoutLimit = "SELECT p.id FROM pessoa p {$whereClause}";
                $query = "SELECT td.id AS tipo_documento__id, s.nome AS sexo__nome, o.id AS operadora__id, c.id AS cidade__id, b.nome AS bairro__nome, p.id AS pessoa__id, p.identificador AS pessoa__identificador, p.nome AS pessoa__nome, p.corporacao_id_INT AS pessoa__corporacao_id_INT, p.numero_documento AS pessoa__numero_documento, p.is_consumidor_BOOLEAN AS pessoa__is_consumidor_BOOLEAN, p.nome AS pessoa__nome, p.telefone AS pessoa__telefone, p.celular AS pessoa__celular, p.nome_normalizado AS pessoa__nome_normalizado, p.celular_sms AS pessoa__celular_sms, p.email AS pessoa__email, p.logradouro AS pessoa__logradouro, p.numero AS pessoa__numero, p.complemento AS pessoa__complemento, p.corporacao_id_INT AS pessoa__corporacao_id_INT, p.corporacao_id_INT AS pessoa__corporacao_id_INT, p.latitude_INT AS pessoa__latitude_INT, p.longitude_INT AS pessoa__longitude_INT, p.foto AS pessoa__foto, p.cadastro_SEC AS pessoa__cadastro_SEC, p.cadastro_OFFSEC AS pessoa__cadastro_OFFSEC FROM pessoa p LEFT JOIN tipo_documento td ON td.id = p.tipo_documento_id_INT LEFT JOIN sexo s ON s.id = p.sexo_id_INT LEFT JOIN operadora o ON o.id = p.operadora_id_INT LEFT JOIN cidade c ON c.id = p.cidade_id_INT LEFT JOIN bairro b ON b.id = p.bairro_id_INT {$whereClause} {$orderByClause} {$limitClause}";

                $msg = $this->database->queryMensagem($queryWithoutLimit);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                
                $totalNumberOfRecords = $this->database->rows();
                if($totalNumberOfRecords == 0)
                {
                    return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                }                        
                        
                $msg = $this->database->queryMensagem($query);
                
                //operacao realizaca com sucesso
                if ($msg == null) 
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                    }                        
                
                    $resultSet = $this->database->result;

                    $objReturn = new stdClass();
                    $objReturn->dataSet = static::mapResultSetDataToList($resultSet, $listMappingType, static::$listAliasTypes);

                    if(!Helper::isNullOrEmpty($paginationParameters->recordsPerPage) && !Helper::isNullOrEmpty($paginationParameters->currentPage))
                    {
                        $lastPage = ceil($totalNumberOfRecords / $paginationParameters->recordsPerPage);
                        $objReturn->paginationParameters = new PaginationParams($paginationParameters->recordsPerPage, $paginationParameters->currentPage, $lastPage);
                    }
                    
                    return new Mensagem_generica($objReturn);
                } 
                else
                {
                    return $msg;
                }                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }



public function getId()
            {
                return $this->id;
            }

public function getIdentificador()
            {
                return $this->identificador;
            }

public function getNome()
            {
                return $this->nome;
            }

public function getNomeNormalizado()
            {
                return $this->nomeNormalizado;
            }

public function getNome_normalizado()
                {
                    return $this->nomeNormalizado;
                }

public function getTipoDocumentoId()
            {
                return $this->tipoDocumentoId;
            }

public function getTipo_documento_id_INT()
                {
                    return $this->tipoDocumentoId;
                }

public function getNumeroDocumento()
            {
                return $this->numeroDocumento;
            }

public function getNumero_documento()
                {
                    return $this->numeroDocumento;
                }

public function getIsConsumidorBoolean()
            {
                return $this->isConsumidorBoolean;
            }

public function getIs_consumidor_BOOLEAN()
                {
                    return $this->isConsumidorBoolean;
                }

public function getSexoId()
            {
                return $this->sexoId;
            }

public function getSexo_id_INT()
                {
                    return $this->sexoId;
                }

public function getTelefone()
            {
                return $this->telefone;
            }

public function getCelular()
            {
                return $this->celular;
            }

public function getOperadoraId()
            {
                return $this->operadoraId;
            }

public function getOperadora_id_INT()
                {
                    return $this->operadoraId;
                }

public function getCelularSms()
            {
                return $this->celularSms;
            }

public function getCelular_sms()
                {
                    return $this->celularSms;
                }

public function getEmail()
            {
                return $this->email;
            }

public function getLogradouro()
            {
                return $this->logradouro;
            }

public function getNumero()
            {
                return $this->numero;
            }

public function getComplemento()
            {
                return $this->complemento;
            }

public function getCidadeId()
            {
                return $this->cidadeId;
            }

public function getCidade_id_INT()
                {
                    return $this->cidadeId;
                }

public function getBairroId()
            {
                return $this->bairroId;
            }

public function getBairro_id_INT()
                {
                    return $this->bairroId;
                }

public function getLatitudeInt()
            {
                return $this->latitudeInt;
            }

public function getLatitude_INT()
                {
                    return $this->latitudeInt;
                }

public function getLongitudeInt()
            {
                return $this->longitudeInt;
            }

public function getLongitude_INT()
                {
                    return $this->longitudeInt;
                }

public function getFoto()
            {
                return $this->foto;
            }

public function getCadastroSec()
            {
                return $this->cadastroSec;
            }

public function getCadastro_SEC()
                {
                    return $this->cadastroSec;
                }

public function getCadastroOffsec()
            {
                return $this->cadastroOffsec;
            }

public function getCadastro_OFFSEC()
                {
                    return $this->cadastroOffsec;
                }

public function getCorporacaoId()
            {
                return $this->corporacaoId;
            }

public function getCorporacao_id_INT()
                {
                    return $this->corporacaoId;
                }

public function getDataNascimentoSec()
            {
                return $this->dataNascimentoSec;
            }

public function getData_nascimento_SEC()
                {
                    return $this->dataNascimentoSec;
                }

public function getDataNascimentoOffsec()
            {
                return $this->dataNascimentoOffsec;
            }

public function getData_nascimento_OFFSEC()
                {
                    return $this->dataNascimentoOffsec;
                }

public function getRelatorioId()
            {
                return $this->relatorioId;
            }

public function getRelatorio_id_INT()
                {
                    return $this->relatorioId;
                }

public function getIndEmailValidoBoolean()
            {
                return $this->indEmailValidoBoolean;
            }

public function getInd_email_valido_BOOLEAN()
                {
                    return $this->indEmailValidoBoolean;
                }

public function getIndCelularValidoBoolean()
            {
                return $this->indCelularValidoBoolean;
            }

public function getInd_celular_valido_BOOLEAN()
                {
                    return $this->indCelularValidoBoolean;
                }



function setId($value)
            {
                $this->id = $value;
            }

function setIdentificador($value)
            {
                $this->identificador = $value;
            }

function setNome($value)
            {
                $this->nome = $value;
            }

function setNomeNormalizado($value)
            {
                $this->nomeNormalizado = $value;
            }

public function setNome_normalizado($value)
                { 
                    $this->nomeNormalizado = $value; 
                }

function setTipoDocumentoId($value)
            {
                $this->tipoDocumentoId = $value;
            }

public function setTipo_documento_id_INT($value)
                { 
                    $this->tipoDocumentoId = $value; 
                }

function setNumeroDocumento($value)
            {
                $this->numeroDocumento = $value;
            }

public function setNumero_documento($value)
                { 
                    $this->numeroDocumento = $value; 
                }

function setIsConsumidorBoolean($value)
            {
                $this->isConsumidorBoolean = $value;
            }

public function setIs_consumidor_BOOLEAN($value)
                { 
                    $this->isConsumidorBoolean = $value; 
                }

function setSexoId($value)
            {
                $this->sexoId = $value;
            }

public function setSexo_id_INT($value)
                { 
                    $this->sexoId = $value; 
                }

function setTelefone($value)
            {
                $this->telefone = $value;
            }

function setCelular($value)
            {
                $this->celular = $value;
            }

function setOperadoraId($value)
            {
                $this->operadoraId = $value;
            }

public function setOperadora_id_INT($value)
                { 
                    $this->operadoraId = $value; 
                }

function setCelularSms($value)
            {
                $this->celularSms = $value;
            }

public function setCelular_sms($value)
                { 
                    $this->celularSms = $value; 
                }

function setEmail($value)
            {
                $this->email = $value;
            }

function setLogradouro($value)
            {
                $this->logradouro = $value;
            }

function setNumero($value)
            {
                $this->numero = $value;
            }

function setComplemento($value)
            {
                $this->complemento = $value;
            }

function setCidadeId($value)
            {
                $this->cidadeId = $value;
            }

public function setCidade_id_INT($value)
                { 
                    $this->cidadeId = $value; 
                }

function setBairroId($value)
            {
                $this->bairroId = $value;
            }

public function setBairro_id_INT($value)
                { 
                    $this->bairroId = $value; 
                }

function setLatitudeInt($value)
            {
                $this->latitudeInt = $value;
            }

public function setLatitude_INT($value)
                { 
                    $this->latitudeInt = $value; 
                }

function setLongitudeInt($value)
            {
                $this->longitudeInt = $value;
            }

public function setLongitude_INT($value)
                { 
                    $this->longitudeInt = $value; 
                }

function setFoto($value)
            {
                $this->foto = $value;
            }

function setCadastroSec($value)
            {
                $this->cadastroSec = $value;
            }

public function setCadastro_SEC($value)
                { 
                    $this->cadastroSec = $value; 
                }

function setCadastroOffsec($value)
            {
                $this->cadastroOffsec = $value;
            }

public function setCadastro_OFFSEC($value)
                { 
                    $this->cadastroOffsec = $value; 
                }

function setCorporacaoId($value)
            {
                $this->corporacaoId = $value;
            }

public function setCorporacao_id_INT($value)
                { 
                    $this->corporacaoId = $value; 
                }

function setDataNascimentoSec($value)
            {
                $this->dataNascimentoSec = $value;
            }

public function setData_nascimento_SEC($value)
                { 
                    $this->dataNascimentoSec = $value; 
                }

function setDataNascimentoOffsec($value)
            {
                $this->dataNascimentoOffsec = $value;
            }

public function setData_nascimento_OFFSEC($value)
                { 
                    $this->dataNascimentoOffsec = $value; 
                }

function setRelatorioId($value)
            {
                $this->relatorioId = $value;
            }

public function setRelatorio_id_INT($value)
                { 
                    $this->relatorioId = $value; 
                }

function setIndEmailValidoBoolean($value)
            {
                $this->indEmailValidoBoolean = $value;
            }

public function setInd_email_valido_BOOLEAN($value)
                { 
                    $this->indEmailValidoBoolean = $value; 
                }

function setIndCelularValidoBoolean($value)
            {
                $this->indCelularValidoBoolean = $value;
            }

public function setInd_celular_valido_BOOLEAN($value)
                { 
                    $this->indCelularValidoBoolean = $value; 
                }



public function clear()
        {$this->id = null;
$this->identificador = null;
$this->nome = null;
$this->nomeNormalizado = null;
$this->tipoDocumentoId = null;
if($this->objTipo_documento != null) unset($this->objTipo_documento);
$this->numeroDocumento = null;
$this->isConsumidorBoolean = null;
$this->sexoId = null;
if($this->objSexo != null) unset($this->objSexo);
$this->telefone = null;
$this->celular = null;
$this->operadoraId = null;
if($this->objOperadora != null) unset($this->objOperadora);
$this->celularSms = null;
$this->email = null;
$this->logradouro = null;
$this->numero = null;
$this->complemento = null;
$this->cidadeId = null;
if($this->objCidade != null) unset($this->objCidade);
$this->bairroId = null;
if($this->objBairro != null) unset($this->objBairro);
$this->latitudeInt = null;
$this->longitudeInt = null;
$this->foto = null;
$this->cadastroSec = null;
$this->cadastroOffsec = null;
$this->corporacaoId = null;
if($this->objCorporacao != null) unset($this->objCorporacao);
$this->dataNascimentoSec = null;
$this->dataNascimentoOffsec = null;
$this->relatorioId = null;
if($this->objRelatorio != null) unset($this->objRelatorio);
$this->indEmailValidoBoolean = null;
$this->indCelularValidoBoolean = null;
if(is_array($this->arrEstadosCampos)){
                        unset($this->arrEstadosCampos);
                     $this->arrEstadosCampos = array();
                    }
                }

public function formatarParaSQL()
        {
$this->nomeNormalizado = $this->formatarStringNormalizadaParaComandoSQL($this->nome);
if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->id)){
$this->id = $this->formatarIntegerParaComandoSQL($this->id);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->id);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->identificador)){
$this->identificador = $this->formatarStringParaComandoSQL($this->identificador);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->identificador);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->nome)){
$this->nome = $this->formatarStringParaComandoSQL($this->nome);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->nome);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->tipoDocumentoId)){
$this->tipoDocumentoId = $this->formatarIntegerParaComandoSQL($this->tipoDocumentoId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->tipoDocumentoId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->numeroDocumento)){
$this->numeroDocumento = $this->formatarStringParaComandoSQL($this->numeroDocumento);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->numeroDocumento);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->isConsumidorBoolean)){
$this->isConsumidorBoolean = $this->formatarBooleanParaComandoSQL($this->isConsumidorBoolean);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->isConsumidorBoolean);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->sexoId)){
$this->sexoId = $this->formatarIntegerParaComandoSQL($this->sexoId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->sexoId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->telefone)){
$this->telefone = $this->formatarStringParaComandoSQL($this->telefone);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->telefone);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->celular)){
$this->celular = $this->formatarStringParaComandoSQL($this->celular);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->celular);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->operadoraId)){
$this->operadoraId = $this->formatarIntegerParaComandoSQL($this->operadoraId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->operadoraId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->celularSms)){
$this->celularSms = $this->formatarStringParaComandoSQL($this->celularSms);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->celularSms);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->email)){
$this->email = $this->formatarStringParaComandoSQL($this->email);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->email);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->logradouro)){
$this->logradouro = $this->formatarStringParaComandoSQL($this->logradouro);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->logradouro);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->numero)){
$this->numero = $this->formatarStringParaComandoSQL($this->numero);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->numero);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->complemento)){
$this->complemento = $this->formatarStringParaComandoSQL($this->complemento);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->complemento);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->cidadeId)){
$this->cidadeId = $this->formatarIntegerParaComandoSQL($this->cidadeId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->cidadeId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->bairroId)){
$this->bairroId = $this->formatarIntegerParaComandoSQL($this->bairroId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->bairroId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->latitudeInt)){
$this->latitudeInt = $this->formatarIntegerParaComandoSQL($this->latitudeInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->latitudeInt);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->longitudeInt)){
$this->longitudeInt = $this->formatarIntegerParaComandoSQL($this->longitudeInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->longitudeInt);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->foto)){
$this->foto = $this->formatarStringParaComandoSQL($this->foto);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->foto);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->cadastroSec)){
$this->cadastroSec = $this->formatarIntegerParaComandoSQL($this->cadastroSec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->cadastroSec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->cadastroOffsec)){
$this->cadastroOffsec = $this->formatarIntegerParaComandoSQL($this->cadastroOffsec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->cadastroOffsec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->corporacaoId)){
$this->corporacaoId = $this->formatarIntegerParaComandoSQL($this->corporacaoId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->corporacaoId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->dataNascimentoSec)){
$this->dataNascimentoSec = $this->formatarIntegerParaComandoSQL($this->dataNascimentoSec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->dataNascimentoSec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->dataNascimentoOffsec)){
$this->dataNascimentoOffsec = $this->formatarIntegerParaComandoSQL($this->dataNascimentoOffsec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->dataNascimentoOffsec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->relatorioId)){
$this->relatorioId = $this->formatarIntegerParaComandoSQL($this->relatorioId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->relatorioId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->indEmailValidoBoolean)){
$this->indEmailValidoBoolean = $this->formatarBooleanParaComandoSQL($this->indEmailValidoBoolean);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->indEmailValidoBoolean);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->indCelularValidoBoolean)){
$this->indCelularValidoBoolean = $this->formatarBooleanParaComandoSQL($this->indCelularValidoBoolean);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->indCelularValidoBoolean);
}

}

public function formatarParaExibicao()
        {if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->identificador)){
$this->identificador = $this->formatarStringParaExibicao($this->identificador);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->identificador);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->nome)){
$this->nome = $this->formatarStringParaExibicao($this->nome);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->nome);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->nomeNormalizado)){
$this->nomeNormalizado = $this->formatarStringParaExibicao($this->nomeNormalizado);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->nomeNormalizado);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->numeroDocumento)){
$this->numeroDocumento = $this->formatarStringParaExibicao($this->numeroDocumento);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->numeroDocumento);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->isConsumidorBoolean)){
$this->isConsumidorBoolean = $this->formatarBooleanParaExibicao($this->isConsumidorBoolean);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->isConsumidorBoolean);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->telefone)){
$this->telefone = $this->formatarStringParaExibicao($this->telefone);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->telefone);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->celular)){
$this->celular = $this->formatarStringParaExibicao($this->celular);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->celular);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->celularSms)){
$this->celularSms = $this->formatarStringParaExibicao($this->celularSms);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->celularSms);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->email)){
$this->email = $this->formatarStringParaExibicao($this->email);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->email);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->logradouro)){
$this->logradouro = $this->formatarStringParaExibicao($this->logradouro);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->logradouro);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->numero)){
$this->numero = $this->formatarStringParaExibicao($this->numero);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->numero);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->complemento)){
$this->complemento = $this->formatarStringParaExibicao($this->complemento);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->complemento);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->foto)){
$this->foto = $this->formatarStringParaExibicao($this->foto);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->foto);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->indEmailValidoBoolean)){
$this->indEmailValidoBoolean = $this->formatarBooleanParaExibicao($this->indEmailValidoBoolean);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->indEmailValidoBoolean);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->indCelularValidoBoolean)){
$this->indCelularValidoBoolean = $this->formatarBooleanParaExibicao($this->indCelularValidoBoolean);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->indCelularValidoBoolean);
}

}

function select($id, $idCorporacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }   

            $sql = "SELECT id, identificador, nome, nome_normalizado, tipo_documento_id_INT, numero_documento, is_consumidor_BOOLEAN, sexo_id_INT, telefone, celular, operadora_id_INT, celular_sms, email, logradouro, numero, complemento, cidade_id_INT, bairro_id_INT, latitude_INT, longitude_INT, foto, cadastro_SEC, cadastro_OFFSEC, corporacao_id_INT, data_nascimento_SEC, data_nascimento_OFFSEC, relatorio_id_INT, ind_email_valido_BOOLEAN, ind_celular_valido_BOOLEAN FROM pessoa WHERE id = $id AND corporacao_id_INT = $idCorporacao";

            $msg = $this->database->queryMensagem($sql);
            $result = $this->database->result;

            if($msg != null && $msg->erro() )
            { 
            		$this->clear();
            		return $msg;
        	} 
        	else if($this->database->rows() == 0)
        	{
                return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nenhum registro foi encontrado."));
            }
        

            $row = $this->database->fetchArray(MYSQLI_NUM);
            
            		$this->id = $row[0];
		$this->identificador = $row[1];
		$this->nome = $row[2];
		$this->nomeNormalizado = $row[3];
		$this->tipoDocumentoId = $row[4];
		$this->numeroDocumento = $row[5];
		$this->isConsumidorBoolean = $row[6];
		$this->sexoId = $row[7];
		$this->telefone = $row[8];
		$this->celular = $row[9];
		$this->operadoraId = $row[10];
		$this->celularSms = $row[11];
		$this->email = $row[12];
		$this->logradouro = $row[13];
		$this->numero = $row[14];
		$this->complemento = $row[15];
		$this->cidadeId = $row[16];
		$this->bairroId = $row[17];
		$this->latitudeInt = $row[18];
		$this->longitudeInt = $row[19];
		$this->foto = $row[20];
		$this->cadastroSec = $row[21];
		$this->cadastroOffsec = $row[22];
		$this->dataNascimentoSec = $row[24];
		$this->dataNascimentoOffsec = $row[25];
		$this->relatorioId = $row[26];
		$this->indEmailValidoBoolean = $row[27];
		$this->indCelularValidoBoolean = $row[28];

            
        }

public function delete($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (is_null($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }

            $sql = "DELETE FROM pessoa WHERE id= $id AND corporacao_id_INT = $idCorporacao";
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                    && $sincronizar
                        && static::idSistemaTabela() != null
                            && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_REMOVE,
                                null,
                                null,
                                $id,
                                $this->database);
            }

            return $msg;
        }

public function insert($sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null, $opcoes = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $this->defineDataCadastroInSecondsIfNotDefined();
$this->defineDataCadastroOffsetInSecondsIfNotDefined();

            
            if (is_null($opcoes))
            {
                $opcoes = $this->opcoesDAO;
            }

            if (!is_numeric($this->id))
            {
                $this->id = "null";
                if ($opcoes == null || $opcoes[Database::OPCAO_GERAR_ID] != false) 
                {
                    $this->id = EXTDAO_Sistema_sequencia::gerarId(static::nomeTabela());                    
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }    
                    else if($msg == null)
                    {
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
                else 
                {
                    $this->id = null;
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao));
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }
                    else if($msg == null)
                    {
                        $this->id = $this->database->getLastInsertId();
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
            }
            else 
            {
                $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }                        
                else if($msg == null)
                {
                   $msg = new Mensagem_token();
                   $msg->mValor = $this->id;
                }
            }
            if($msg == null || $msg->ok())
            {                
                if($sincronizar
                    && static::idSistemaTabela() != null
                    && isset($this->id)){
                    
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_INSERT,
                                null,
                                null,
                                $this->id,
                                $this->database); 
                	}
            } 
            else
            {
                $this->id = null;
            }
            
            return $msg;
            
        }

public function getSQLInsert($idCorporacao){ 

return "INSERT INTO pessoa (id, identificador, nome, nome_normalizado, tipo_documento_id_INT, numero_documento, is_consumidor_BOOLEAN, sexo_id_INT, telefone, celular, operadora_id_INT, celular_sms, email, logradouro, numero, complemento, cidade_id_INT, bairro_id_INT, latitude_INT, longitude_INT, foto, cadastro_SEC, cadastro_OFFSEC, corporacao_id_INT, data_nascimento_SEC, data_nascimento_OFFSEC, relatorio_id_INT, ind_email_valido_BOOLEAN, ind_celular_valido_BOOLEAN) VALUES ( $this->id ,  $this->identificador ,  $this->nome ,  $this->nomeNormalizado ,  $this->tipoDocumentoId ,  $this->numeroDocumento ,  $this->isConsumidorBoolean ,  $this->sexoId ,  $this->telefone ,  $this->celular ,  $this->operadoraId ,  $this->celularSms ,  $this->email ,  $this->logradouro ,  $this->numero ,  $this->complemento ,  $this->cidadeId ,  $this->bairroId ,  $this->latitudeInt ,  $this->longitudeInt ,  $this->foto ,  $this->cadastroSec ,  $this->cadastroOffsec ,  $idCorporacao ,  $this->dataNascimentoSec ,  $this->dataNascimentoOffsec ,  $this->relatorioId ,  $this->indEmailValidoBoolean ,  $this->indCelularValidoBoolean ) "; 

}




        
        public function updateByParameters($id, $objParametros, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao))
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();

            
                
                if (isset($objParametros->identificador)) 
                {
                    $arrUpdateFields[] = " identificador = {$objParametros->identificador} ";
                }


                
                if (isset($objParametros->nome)) 
                {
                    $arrUpdateFields[] = " nome = {$objParametros->nome} ";
                }


                
                if (isset($objParametros->nomeNormalizado)) 
                {
                    $arrUpdateFields[] = " nome_normalizado = {$objParametros->nomeNormalizado} ";
                }


                
                if (isset($objParametros->tipoDocumentoId)) 
                {
                    $arrUpdateFields[] = " tipo_documento_id_INT = {$objParametros->tipoDocumentoId} ";
                }


                
                if (isset($objParametros->numeroDocumento)) 
                {
                    $arrUpdateFields[] = " numero_documento = {$objParametros->numeroDocumento} ";
                }


                
                if (isset($objParametros->isConsumidorBoolean)) 
                {
                    $arrUpdateFields[] = " is_consumidor_BOOLEAN = {$objParametros->isConsumidorBoolean} ";
                }


                
                if (isset($objParametros->sexoId)) 
                {
                    $arrUpdateFields[] = " sexo_id_INT = {$objParametros->sexoId} ";
                }


                
                if (isset($objParametros->telefone)) 
                {
                    $arrUpdateFields[] = " telefone = {$objParametros->telefone} ";
                }


                
                if (isset($objParametros->celular)) 
                {
                    $arrUpdateFields[] = " celular = {$objParametros->celular} ";
                }


                
                if (isset($objParametros->operadoraId)) 
                {
                    $arrUpdateFields[] = " operadora_id_INT = {$objParametros->operadoraId} ";
                }


                
                if (isset($objParametros->celularSms)) 
                {
                    $arrUpdateFields[] = " celular_sms = {$objParametros->celularSms} ";
                }


                
                if (isset($objParametros->email)) 
                {
                    $arrUpdateFields[] = " email = {$objParametros->email} ";
                }


                
                if (isset($objParametros->logradouro)) 
                {
                    $arrUpdateFields[] = " logradouro = {$objParametros->logradouro} ";
                }


                
                if (isset($objParametros->numero)) 
                {
                    $arrUpdateFields[] = " numero = {$objParametros->numero} ";
                }


                
                if (isset($objParametros->complemento)) 
                {
                    $arrUpdateFields[] = " complemento = {$objParametros->complemento} ";
                }


                
                if (isset($objParametros->cidadeId)) 
                {
                    $arrUpdateFields[] = " cidade_id_INT = {$objParametros->cidadeId} ";
                }


                
                if (isset($objParametros->bairroId)) 
                {
                    $arrUpdateFields[] = " bairro_id_INT = {$objParametros->bairroId} ";
                }


                
                if (isset($objParametros->latitudeInt)) 
                {
                    $arrUpdateFields[] = " latitude_INT = {$objParametros->latitudeInt} ";
                }


                
                if (isset($objParametros->longitudeInt)) 
                {
                    $arrUpdateFields[] = " longitude_INT = {$objParametros->longitudeInt} ";
                }


                
                if (isset($objParametros->foto)) 
                {
                    $arrUpdateFields[] = " foto = {$objParametros->foto} ";
                }


                
                if (isset($objParametros->cadastroSec)) 
                {
                    $arrUpdateFields[] = " cadastro_SEC = {$objParametros->cadastroSec} ";
                }


                
                if (isset($objParametros->cadastroOffsec)) 
                {
                    $arrUpdateFields[] = " cadastro_OFFSEC = {$objParametros->cadastroOffsec} ";
                }


                
                if (isset($objParametros->dataNascimentoSec)) 
                {
                    $arrUpdateFields[] = " data_nascimento_SEC = {$objParametros->dataNascimentoSec} ";
                }


                
                if (isset($objParametros->dataNascimentoOffsec)) 
                {
                    $arrUpdateFields[] = " data_nascimento_OFFSEC = {$objParametros->dataNascimentoOffsec} ";
                }


                
                if (isset($objParametros->relatorioId)) 
                {
                    $arrUpdateFields[] = " relatorio_id_INT = {$objParametros->relatorioId} ";
                }


                
                if (isset($objParametros->indEmailValidoBoolean)) 
                {
                    $arrUpdateFields[] = " ind_email_valido_BOOLEAN = {$objParametros->indEmailValidoBoolean} ";
                }


                
                if (isset($objParametros->indCelularValidoBoolean)) 
                {
                    $arrUpdateFields[] = " ind_celular_valido_BOOLEAN = {$objParametros->indCelularValidoBoolean} ";
                }



            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE pessoa SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";

            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null)
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function update($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($this->identificador)) 
                {                                      
                    $arrUpdateFields[] = " identificador = {$this->identificador} ";
                }


                
                if (isset($this->nome)) 
                {                                      
                    $arrUpdateFields[] = " nome = {$this->nome} ";
                }


                
                if (isset($this->nomeNormalizado)) 
                {                                      
                    $arrUpdateFields[] = " nome_normalizado = {$this->nomeNormalizado} ";
                }


                
                if (isset($this->tipoDocumentoId)) 
                {                                      
                    $arrUpdateFields[] = " tipo_documento_id_INT = {$this->tipoDocumentoId} ";
                }


                
                if (isset($this->numeroDocumento)) 
                {                                      
                    $arrUpdateFields[] = " numero_documento = {$this->numeroDocumento} ";
                }


                
                if (isset($this->isConsumidorBoolean)) 
                {                                      
                    $arrUpdateFields[] = " is_consumidor_BOOLEAN = {$this->isConsumidorBoolean} ";
                }


                
                if (isset($this->sexoId)) 
                {                                      
                    $arrUpdateFields[] = " sexo_id_INT = {$this->sexoId} ";
                }


                
                if (isset($this->telefone)) 
                {                                      
                    $arrUpdateFields[] = " telefone = {$this->telefone} ";
                }


                
                if (isset($this->celular)) 
                {                                      
                    $arrUpdateFields[] = " celular = {$this->celular} ";
                }


                
                if (isset($this->operadoraId)) 
                {                                      
                    $arrUpdateFields[] = " operadora_id_INT = {$this->operadoraId} ";
                }


                
                if (isset($this->celularSms)) 
                {                                      
                    $arrUpdateFields[] = " celular_sms = {$this->celularSms} ";
                }


                
                if (isset($this->email)) 
                {                                      
                    $arrUpdateFields[] = " email = {$this->email} ";
                }


                
                if (isset($this->logradouro)) 
                {                                      
                    $arrUpdateFields[] = " logradouro = {$this->logradouro} ";
                }


                
                if (isset($this->numero)) 
                {                                      
                    $arrUpdateFields[] = " numero = {$this->numero} ";
                }


                
                if (isset($this->complemento)) 
                {                                      
                    $arrUpdateFields[] = " complemento = {$this->complemento} ";
                }


                
                if (isset($this->cidadeId)) 
                {                                      
                    $arrUpdateFields[] = " cidade_id_INT = {$this->cidadeId} ";
                }


                
                if (isset($this->bairroId)) 
                {                                      
                    $arrUpdateFields[] = " bairro_id_INT = {$this->bairroId} ";
                }


                
                if (isset($this->latitudeInt)) 
                {                                      
                    $arrUpdateFields[] = " latitude_INT = {$this->latitudeInt} ";
                }


                
                if (isset($this->longitudeInt)) 
                {                                      
                    $arrUpdateFields[] = " longitude_INT = {$this->longitudeInt} ";
                }


                
                if (isset($this->foto)) 
                {                                      
                    $arrUpdateFields[] = " foto = {$this->foto} ";
                }


                
                if (isset($this->cadastroSec)) 
                {                                      
                    $arrUpdateFields[] = " cadastro_SEC = {$this->cadastroSec} ";
                }


                
                if (isset($this->cadastroOffsec)) 
                {                                      
                    $arrUpdateFields[] = " cadastro_OFFSEC = {$this->cadastroOffsec} ";
                }


                
                if (isset($this->dataNascimentoSec)) 
                {                                      
                    $arrUpdateFields[] = " data_nascimento_SEC = {$this->dataNascimentoSec} ";
                }


                
                if (isset($this->dataNascimentoOffsec)) 
                {                                      
                    $arrUpdateFields[] = " data_nascimento_OFFSEC = {$this->dataNascimentoOffsec} ";
                }


                
                if (isset($this->relatorioId)) 
                {                                      
                    $arrUpdateFields[] = " relatorio_id_INT = {$this->relatorioId} ";
                }


                
                if (isset($this->indEmailValidoBoolean)) 
                {                                      
                    $arrUpdateFields[] = " ind_email_valido_BOOLEAN = {$this->indEmailValidoBoolean} ";
                }


                
                if (isset($this->indCelularValidoBoolean)) 
                {                                      
                    $arrUpdateFields[] = " ind_celular_valido_BOOLEAN = {$this->indCelularValidoBoolean} ";
                }


            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE pessoa SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function updateByInstanceUsingParameters($id, $parameters, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($parameters->identificador)) 
                {                                      
                    $arrUpdateFields[] = " identificador = {$this->identificador} ";
                }
                
                if (isset($parameters->nome)) 
                {                                      
                    $arrUpdateFields[] = " nome = {$this->nome} ";
                }
                
                if (isset($parameters->nomeNormalizado)) 
                {                                      
                    $arrUpdateFields[] = " nome_normalizado = {$this->nomeNormalizado} ";
                }
                
                if (isset($parameters->tipoDocumentoId)) 
                {                                      
                    $arrUpdateFields[] = " tipo_documento_id_INT = {$this->tipoDocumentoId} ";
                }
                
                if (isset($parameters->numeroDocumento)) 
                {                                      
                    $arrUpdateFields[] = " numero_documento = {$this->numeroDocumento} ";
                }
                
                if (isset($parameters->isConsumidorBoolean)) 
                {                                      
                    $arrUpdateFields[] = " is_consumidor_BOOLEAN = {$this->isConsumidorBoolean} ";
                }
                
                if (isset($parameters->sexoId)) 
                {                                      
                    $arrUpdateFields[] = " sexo_id_INT = {$this->sexoId} ";
                }
                
                if (isset($parameters->telefone)) 
                {                                      
                    $arrUpdateFields[] = " telefone = {$this->telefone} ";
                }
                
                if (isset($parameters->celular)) 
                {                                      
                    $arrUpdateFields[] = " celular = {$this->celular} ";
                }
                
                if (isset($parameters->operadoraId)) 
                {                                      
                    $arrUpdateFields[] = " operadora_id_INT = {$this->operadoraId} ";
                }
                
                if (isset($parameters->celularSms)) 
                {                                      
                    $arrUpdateFields[] = " celular_sms = {$this->celularSms} ";
                }
                
                if (isset($parameters->email)) 
                {                                      
                    $arrUpdateFields[] = " email = {$this->email} ";
                }
                
                if (isset($parameters->logradouro)) 
                {                                      
                    $arrUpdateFields[] = " logradouro = {$this->logradouro} ";
                }
                
                if (isset($parameters->numero)) 
                {                                      
                    $arrUpdateFields[] = " numero = {$this->numero} ";
                }
                
                if (isset($parameters->complemento)) 
                {                                      
                    $arrUpdateFields[] = " complemento = {$this->complemento} ";
                }
                
                if (isset($parameters->cidadeId)) 
                {                                      
                    $arrUpdateFields[] = " cidade_id_INT = {$this->cidadeId} ";
                }
                
                if (isset($parameters->bairroId)) 
                {                                      
                    $arrUpdateFields[] = " bairro_id_INT = {$this->bairroId} ";
                }
                
                if (isset($parameters->latitudeInt)) 
                {                                      
                    $arrUpdateFields[] = " latitude_INT = {$this->latitudeInt} ";
                }
                
                if (isset($parameters->longitudeInt)) 
                {                                      
                    $arrUpdateFields[] = " longitude_INT = {$this->longitudeInt} ";
                }
                
                if (isset($parameters->foto)) 
                {                                      
                    $arrUpdateFields[] = " foto = {$this->foto} ";
                }
                
                if (isset($parameters->cadastroSec)) 
                {                                      
                    $arrUpdateFields[] = " cadastro_SEC = {$this->cadastroSec} ";
                }
                
                if (isset($parameters->cadastroOffsec)) 
                {                                      
                    $arrUpdateFields[] = " cadastro_OFFSEC = {$this->cadastroOffsec} ";
                }
                
                if (isset($parameters->dataNascimentoSec)) 
                {                                      
                    $arrUpdateFields[] = " data_nascimento_SEC = {$this->dataNascimentoSec} ";
                }
                
                if (isset($parameters->dataNascimentoOffsec)) 
                {                                      
                    $arrUpdateFields[] = " data_nascimento_OFFSEC = {$this->dataNascimentoOffsec} ";
                }
                
                if (isset($parameters->relatorioId)) 
                {                                      
                    $arrUpdateFields[] = " relatorio_id_INT = {$this->relatorioId} ";
                }
                
                if (isset($parameters->indEmailValidoBoolean)) 
                {                                      
                    $arrUpdateFields[] = " ind_email_valido_BOOLEAN = {$this->indEmailValidoBoolean} ";
                }
                
                if (isset($parameters->indCelularValidoBoolean)) 
                {                                      
                    $arrUpdateFields[] = " ind_celular_valido_BOOLEAN = {$this->indCelularValidoBoolean} ";
                }
            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE pessoa SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }

}

?>
