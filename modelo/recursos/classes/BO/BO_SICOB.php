<?php

class BO_SICOB
{
    const PREFIXO = "SICOB:";

    public function factory()
    {
        return new BO_SICOB();
    }

    public static function logarPeloSistema()
    {
        Protocolo_caracteristicas_pacote::apagaSessao();
        $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
        $idUsuario = Seguranca::getId();

        if (strlen($idCorporacao) && strlen($idUsuario))
        {
            $objUsuario = new EXTDAO_Usuario();
            $objUsuario->select($idUsuario);
            $email = $objUsuario->getEmail();

            $objCorporacao = new EXTDAO_Corporacao();
            $objCorporacao->select($idCorporacao);
            $corporacao = $objCorporacao->getNome();

            $identificador = BO_SICOB::PREFIXO . $email . ":" . $corporacao;
            $objCrypt = new Crypt();

            $campoCrypt = $objCrypt->crypt($identificador);

            $url = DOMINIO_DE_ACESSO_SICOB . "client_area/index.php?tipo=paginas&page=logar_pelo_sistema&pCampo=$campoCrypt";

            Helper::mudarLocation($url);
        }
        else
        {
            Helper::mudarLocation(DOMINIO_DE_ACESSO_SICOB);
        }
    }

    public static function getEstadoDaCorporacao()
    {
        $corporacao = NOME_CORPORACAO;
        $url = DOMINIO_DE_ACESSO_SERVICOS_SICOB . "getEstadoDaCorporacao&corporacao=" . urlencode($corporacao);

        $mensagem = Helper::chamadaCurlMensagem($url);
        return $mensagem;
    }

    public static function isUsuarioLogadoDonoDaCorporacao()
    {
        $donoDaCorporacao = Seguranca::isDonoDaCorporacao();
        if ($donoDaCorporacao != null)
        {
            return $donoDaCorporacao;
        }
        $msgProtocolo = BO_SICOB::getCaracteristicasDoPacoteDaCorporacao();

        if ($msgProtocolo == null ||
            $msgProtocolo->mCodRetorno != PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO)
        {
            return null;
        }
        else
        {
            $protocolo = $msgProtocolo->mObj;
        }

        $idUsuario = Seguranca::getId();
        $objUsuario = new EXTDAO_Usuario();
        $objUsuario->select($idUsuario);
        $email = $objUsuario->getEmail();
        $isDonoDaCorporacao = null;
        if (strcmp($email, $protocolo->emailUsuarioPrincipal) == 0)
        {

            $isDonoDaCorporacao = true;
        }
        else
        {
            $isDonoDaCorporacao = false;
        }

        Seguranca::setDonoDaCorporacao($isDonoDaCorporacao);
        return $isDonoDaCorporacao;
    }

    public static function editaClienteSICOB($email, $senhaDecrypt)
    {
        Protocolo_caracteristicas_pacote::apagaSessao();
        $idCorporacao = Seguranca::getIdDaCorporacaoLogada();

        if (!strlen($idCorporacao))
        {
            Helper::mudarLocation(DOMINIO_DE_ACESSO_SICOB);
            return;
        }

        $objCorporacao = new EXTDAO_Corporacao();
        $objCorporacao->select($idCorporacao);
        $corporacao = $objCorporacao->getNome();
        if (strlen($email) && strlen($senhaDecrypt) && strlen($corporacao))
        {
            $identificador = BO_SICOB::PREFIXO . $email . ":" . $senhaDecrypt . ":" . $corporacao;

            $objCrypt = new Crypt();
            $campoCrypt = $objCrypt->crypt($identificador);

            $url = DOMINIO_DE_ACESSO_SERVICOS_SICOB . "editaCliente&pCampo=$campoCrypt";
            $mensagem = Helper::chamadaCurlMensagem($url);
            return $mensagem;
        }
        else
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_PARAMETRO_INVALIDO, "Par�mentro inv�lido");
        }
    }

    public static function getLinkAcessoDetalheAssinatura($acaoAoCarregarAPagina = "cadastrar_usuario", $emailUsuarioDesejado = '')
    {
        $objLink = new Link();
        $objLink->alturaGreyBox = null;
        $objLink->larguraGreyBox = null;
        $objLink->tituloGreyBox = null;
        $objLink->url = "actions.php?class=Servicos_web_sihop&action=acessarDetalhesAssinatura&acao=$acaoAoCarregarAPagina&email_usuario=$emailUsuarioDesejado&";
        $objLink->demaisAtributos = array("onmouseover" => "tip('Clique para gerenciar os seus usu�rios.', this);", "onmouseout" => "notip();");

        $objLink->label = "clique aqui";
        $objLink->numeroNiveisPai = 0;
        return $objLink;
    }

    public static function acessarDetalhesAssinatura($acaoAoCarregarAPagina = "cadastrar_usuario", $emailUsuarioDesejado = "")
    {
        $crypt = new Crypt();
        $seguranca = new Seguranca();
        $senha = $seguranca->getSenhaDoUsuarioLogado();

        $campoC = $crypt->cryptGet(
            array("corporacao", "email_responsavel", "senha_responsavel", "acao", "email_usuario"),
            array(Seguranca::getNomeCorporacao(), Seguranca::getEmail(), $senha, $acaoAoCarregarAPagina, $emailUsuarioDesejado)
        );

        $url = DOMINIO_DE_ACESSO_SICOB . "index.php?tipo=paginas&page=detalhe_assinatura&pCampo=$campoC";

        Helper::mudarLocation($url);
    }

    public static function trocarPacote()
    {
        Protocolo_caracteristicas_pacote::apagaSessao();
        $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
        $idUsuario = Seguranca::getId();

        if (strlen($idCorporacao) && strlen($idUsuario))
        {
            $objUsuario = new EXTDAO_Usuario();
            $objUsuario->select($idUsuario);
            $email = $objUsuario->getEmail();

            $objCorporacao = new EXTDAO_Corporacao();
            $objCorporacao->select($idCorporacao);
            $corporacao = $objCorporacao->getNome();

            $objCrypt = new Crypt();
            $campoCrypt = $objCrypt->cryptGet(array("email", "corporacao"), array($email, $corporacao));

            $url = DOMINIO_DE_ACESSO_SICOB . "index.php?tipo=paginas&page=pacotes_pelo_sistema&pCampo=$campoCrypt";

            Helper::mudarLocation($url);
        }
        else
        {
            Helper::mudarLocation(DOMINIO_DE_ACESSO_SICOB);
        }
    }

    public static function getDominioDaCorporacao($nomeCorporacao)
    {

        $nomeCorporacao = urlencode($nomeCorporacao);
        $url = DOMINIO_DE_ACESSO_SERVICOS_SICOB . "getDominioDaCorporacao&corporacao=$nomeCorporacao";

        return Helper::chamadaCurlMensagemToken($url);

    }

//    public static function verificaEstadoDaAssinatura( 
//                        $nomeCorporacao, 
//                        $idSistema, 
//                        $numeroUsuarios){
//        
//        $nomeCorporacao = urlencode($nomeCorporacao);
//        $url = DOMINIO_DE_ACESSO_SERVICOS_SICOB."verificaEstadoDaAssinatura&sistema=$idSistema&corporacao=$nomeCorporacao&numero_usuario=$numeroUsuarios";
//        
//        $strJson = Helper::chamadaCurlJson($url);
//     
//        $json = json_decode($strJson);
//        $mensagem = new Mensagem();
//        $mensagem->inicializa($json);
//        if($mensagem->mCodRetorno == PROTOCOLO_SISTEMA::BLOQUEADA || 
//               $mensagem->mCodRetorno == PROTOCOLO_SISTEMA::NAO_BLOQUEADA){
//            $mensagem = new Mensagem_token();
//            $mensagem->inicializa($json);
//        } 
//        return $mensagem;
//    }
//        
    public static function getCaracteristicasDoPacoteDaCorporacao()
    {
        $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
        $protocolo = Protocolo_caracteristicas_pacote::carregaSessao($idCorporacao);

        if ($protocolo != null)
        {
            return new Mensagem_protocolo($protocolo,
                                          PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO,
                                          null);
        }

        if (strlen($idCorporacao))
        {
            $objCorporacao = new EXTDAO_Corporacao();
            $objCorporacao->select($idCorporacao);

            $corporacao = $objCorporacao->getNome();
            $nomeCorporacao = urlencode($corporacao);
            $url = DOMINIO_DE_ACESSO_SERVICOS_SICOB . "getCaracteristicasDoPacoteDaCorporacao&corporacao=$nomeCorporacao";

            $json = Helper::chamadaCurlJson($url);

            if (Interface_mensagem::checkOk($json))
            {
                $mensagem = new Mensagem_protocolo(new Protocolo_caracteristicas_pacote());
                $mensagem->inicializa($json);
                return $mensagem;
            }
            else
            {
                return Mensagem::factoryJson($json);
            }
        }
        else
        {
            Helper::mudarLocation(DOMINIO_DE_ACESSO_SICOB);
        }

    }

    public static function trocarEmailDoClientePeloSistema(
        $idUsuario = null,
        $nomeNovo = null,
        $emailNovo = null,
        $senhaDecrypt = null)
    {
        if ($nomeNovo == null)
        {
            $nomeNovo = Helper::POSTGET("nome");
        }

        if ($emailNovo == null)
        {
            $emailNovo = Helper::POSTGET("email");
        }
        if ($idUsuario == null)
        {
            $idUsuario = Helper::POSTGET("id_usuario");
        }

        if ($senhaDecrypt == null)
        {
            $senhaDecrypt = Helper::POSTGET("senha");
        }

        if (strlen($emailNovo) && strlen($idUsuario))
        {
            $objUsuario = new EXTDAO_Usuario();
            $objUsuario->select($idUsuario);
            $emailAntigo = $objUsuario->getEmail();
            if (!strlen($nomeNovo))
            {
                $nomeNovo = $objUsuario->getNome();
            }
            $objCrypt = new Crypt();
            $seguranca = new Seguranca();

            $campoCrypt = $objCrypt->cryptGet(
                array("email_responsavel",
                    "senha_responsavel",
                    "nome",
                    "email_antigo",
                    "email_novo",
                    "senha",
                    "corporacao"),
                array(
                    Seguranca::getEmail(),
                    $seguranca->getSenhaDoUsuarioLogado(),
                    $nomeNovo,
                    $emailAntigo,
                    $emailNovo,
                    $senhaDecrypt,
                    Seguranca::getNomeCorporacao()
                ));

            $url = DOMINIO_DE_ACESSO_SERVICOS_SICOB . "trocarEmailDoClientePeloSistema&pCampo=$campoCrypt";

            return Helper::chamadaCurlMensagem($url);
        }
        else
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_PARAMETRO_INVALIDO, "Par�mentro inv�lido");
        }
    }

    public static function verificaExistenciaDoCliente($email = null)
    {
        if ($email == null)
        {
            $email = Helper::POSTGET("email");
        }

        if (strlen($email))
        {
            $objCrypt = new Crypt();
            $campoCrypt = $objCrypt->cryptGet(
                array("email"),
                array($email));

            $url = DOMINIO_DE_ACESSO_SERVICOS_SICOB . "verificaExistenciaDoCliente&pCampo=$campoCrypt";
            return Helper::chamadaCurlMensagem($url);
        }
        else
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_PARAMETRO_INVALIDO, "Par�mentro inv�lido");
        }
    }

    public static function retirarClienteDaAssinatura($idUsuario = null)
    {
        if ($idUsuario == null)
        {
            $idUsuario = Helper::POSTGET("id_usuario");
        }

        if (strlen($idUsuario))
        {
            $objUsuario = new EXTDAO_Usuario();
            $objUsuario->select($idUsuario);
            $email = $objUsuario->getEmail();
            $objCrypt = new Crypt();
            $seguranca = new Seguranca();
            $senha = $seguranca->getSenhaDoUsuarioLogado();
            $campoCrypt = $objCrypt->cryptGet(
                array(
                    "email_usuario_removido",
                    "corporacao",
                    "email_responsavel",
                    "senha_responsavel"),
                array(
                    $email,
                    Seguranca::getNomeCorporacao(),
                    Seguranca::getEmail(),
                    $senha));

            $url = DOMINIO_DE_ACESSO_SERVICOS_SICOB . "retirarClienteDaAssinatura&pCampo=$campoCrypt";

            return Helper::chamadaCurlMensagem($url);
        }
        else
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_PARAMETRO_INVALIDO, "Par�mentro inv�lido");
        }
    }

    public static function cadastrarClienteNaAssinatura(
        $nome = null,
        $email = null,
        $senhaDecrypt = null,
        $db = null)
    {
        if ($nome == null)
        {
            $nome = Helper::POSTGET("nome");
        }

        if ($email == null)
        {
            $email = Helper::POSTGET("email");
        }

        if ($senhaDecrypt == null)
        {
            $senhaDecrypt = Helper::POSTGET("senha");
        }

        if (strlen($nome) && strlen($email))
        {
            $objCrypt = new Crypt();
            $seguranca = new Seguranca();

            $campoCrypt = $objCrypt->cryptGet(
                array(
                    "email_responsavel",
                    "senha_responsavel",
                    "nome",
                    "email",
                    "senha",
                    "corporacao"),
                array(
                    Seguranca::getEmail(),
                    $seguranca->getSenhaDoUsuarioLogado($db),
                    $nome,
                    $email,
                    $senhaDecrypt,
                    Seguranca::getNomeCorporacao()
                ));

            $url = DOMINIO_DE_ACESSO_SERVICOS_SICOB . "cadastrarClienteNaAssinatura&pCampo=$campoCrypt";

            return Helper::chamadaCurlMensagem($url);
        }
        else
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_PARAMETRO_INVALIDO, "Par�mentro inv�lido");
        }
    }

    public static function alterarSenha(
        $email = null,
        $idCorporacao = null,
        $corporacao = null,
        $senhaAntiga = null,
        $senhaNova = null

    )
    {
        if ($email == null)
        {
            $email = Seguranca::getEmail();
            $senhaAntiga = Helper::POSTGET("senha_antiga");
            $senhaNova = Helper::POSTGET("senha_nova");
        }

        if (strlen($email) && strlen($senhaAntiga) && strlen($senhaNova))
        {
            $objCrypt = new Crypt();
            $seguranca = new Seguranca();
            $msg = $seguranca->verificaSenha(
                array("email" => $email, "id_corporacao" => $idCorporacao),
                $senhaAntiga,
                null,
                false);

            if ($msg != null && !$msg->ok())
            {
                return $msg;
            }
            $campoCrypt = $objCrypt->cryptGet(
                array(
                    "senha_antiga",
                    "senha_nova",
                    "email",
                    "corporacao"),
                array(
                    $senhaAntiga,
                    $senhaNova,
                    $email,
                    $corporacao
                ));

            $url = DOMINIO_DE_ACESSO_SERVICOS_SICOB . "alterarSenha&pCampo={$campoCrypt}";
            return Helper::chamadaCurlMensagem($url);
        }
        else
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_PARAMETRO_INVALIDO, "Par�mentro inv�lido");
        }
    }
}
