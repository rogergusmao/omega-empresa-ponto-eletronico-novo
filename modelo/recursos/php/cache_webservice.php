<?php

require_once Helper::acharRaizWorkspace() . DIRETORIO_BIBLIOTECAS_COMPARTILHADAS . "/classes/SingletonCacheWebservice.php";

$singletonFuncoes = SingletonCacheWebservice::getSingleton();

$singletonFuncoes->setWebservices(
    "Cobranca",
    array("getEstadoDaCorporacao",
        "getEspacoEmMBOcupadoPelaAssinatura",
        "getCaracteristicasDoPacoteDaAssinatura",
        "getDominioDaCorporacaoId",
        "getNomeDaCorporacao",
        "getConfiguracoesDeBancoDaCorporacao"));

$singletonFuncoes->exitIfCached();
$singletonFuncoes->startCachingIfNecessary();

