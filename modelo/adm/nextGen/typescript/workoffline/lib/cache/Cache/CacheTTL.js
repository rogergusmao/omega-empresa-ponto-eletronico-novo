"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var Cache_1 = require("./Cache");
var DSO_1 = require("./DSO/TTL/DSO");
var Date_1 = require("../utility/Date");
var CacheTTL = (function (_super) {
    __extends(CacheTTL, _super);
    function CacheTTL() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    CacheTTL.getInstance = function (type, config) {
        if (CacheTTL._instance === null) {
            CacheTTL._instance = new CacheTTL(type, config);
        }
        return CacheTTL._instance;
    };
    // Add cache record to the backend cache
    CacheTTL.prototype.add = function (key, data, tagKey, lifetime) {
        if (lifetime === void 0) { lifetime = CacheTTL.DEFAULT_TTL; }
        // Get the time in seconds
        var timeInSeconds = Date_1.Utility_Date.getTimeInSeconds();
        // Create the cache DSO object
        var cacheDSO = new DSO_1.Cache_DSO_TTL_DSO(lifetime + timeInSeconds, data);
        return _super.prototype.add.call(this, key, cacheDSO, tagKey);
    };
    // Get the cache record from the backend cache, if available
    CacheTTL.prototype.get = function (key, tagKey) {
        // Get the cache DSO from cache
        var data = _super.prototype.get.call(this, key, tagKey);
        // Check if the cache DSO exists
        if (data == undefined) {
            return null;
        }
        var cacheDSO = DSO_1.Cache_DSO_TTL_DSO.fromObject(data);
        // Check if the cache record has a valid lifetime
        if (!this.isValid(cacheDSO.lifetime)) {
            return null;
        }
        return cacheDSO.data;
    };
    CacheTTL.prototype.isValid = function (lifetime) {
        // Get the time in seconds
        var timeInSeconds = Date_1.Utility_Date.getTimeInSeconds();
        return (lifetime > timeInSeconds);
    };
    return CacheTTL;
}(Cache_1.Cache));
CacheTTL.DEFAULT_TTL = 500;
CacheTTL._instance = null;
exports.CacheTTL = CacheTTL;
