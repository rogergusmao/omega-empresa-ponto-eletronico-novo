<?php

//Pagina Wrapper para requisições Ajax

include_once '../recursos/php/constants.php';
include_once '../recursos/php/database_config.php';
include_once '../promocoes/imports/header.php';
include_once '../recursos/php/funcoes.php';
include_once '../promocoes/imports/sessao.php';

if (Helper::GET("ajax_tipo") && Helper::GET("ajax_page")) {

    $tipoAjax = Helper::GET("ajax_tipo");
    $paginaAjax = Helper::GET("ajax_page");

    include_once "{$tipoAjax}/{$paginaAjax}.php";

} elseif (Helper::GET("class") && Helper::GET("funcao")) {

    $classe = Helper::GET("class");

    $obj = call_user_func_array(array($classe, "factory"), "");

    $funcaoComParametros = Helper::GET("funcao");

    $metodo = Helper::getNomeFuncao($funcaoComParametros);
    $parametros = Helper::getParametrosFuncao($funcaoComParametros);

    $retorno = call_user_func_array(array($obj, $metodo), $parametros);

    print $retorno;

} elseif (Helper::GET("class") && Helper::GET("metodo_estatico")) {

    $classe = Helper::GET("class");
    $funcaoComParametros = Helper::GET("metodo_estatico");

    $metodo = Helper::getNomeFuncao($funcaoComParametros);
    $parametros = Helper::getParametrosFuncao($funcaoComParametros);

    $retorno = call_user_func_array(array($classe, $metodo), $parametros);

    print $retorno;

}

?>
