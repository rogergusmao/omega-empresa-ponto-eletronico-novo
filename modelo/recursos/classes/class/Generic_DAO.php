<?php

    abstract class  Generic_DAO
    {
        const INDEX_OPERACAO_REMOVE = 1;
        const INDEX_OPERACAO_INSERT = 2;
        const INDEX_OPERACAO_EDIT = 3;

        const TIPO_VARIAVEL_INTEGER = "INT";
        const TIPO_VARIAVEL_FLOAT = "FLOAT";
        const TIPO_VARIAVEL_DATE = "DATE";
        const TIPO_VARIAVEL_TIME = "TIME";
        const TIPO_VARIAVEL_DATETIME = "DATETIME";
        const TIPO_VARIAVEL_TEXT = "TEXT";
        const TIPO_VARIAVEL_BOOLEAN = "BOOLEAN";
        const TIPO_VARIAVEL_TIME_IN_SECONDS = "TIMEZONE_IN_SECONDS";
        const TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS = "TIMEZONE_OFFSET_IN_SECONDS";

        const LIST_MAPPING_TYPE_GRID = "GRID";
        const LIST_MAPPING_TYPE_COMBOBOX = "COMBOBOX";
        const LIST_MAPPING_TYPE_RELATED_ENTITY = "RELATED_ENTITY";
        const LIST_MAPPING_TYPE_GENERIC = "GENERIC";

        const ESTADO_FORMATACAO_CAMPO_EXIBICAO = "VAR_STATE_HUMAN";
        const ESTADO_FORMATACAO_CAMPO_DB = "VAR_STATE_DB";

        const OPERATOR_STARTS_WITH = "STARTS_WITH";
        const OPERATOR_ENDS_WITH = "ENDS_WITH";
        const OPERATOR_CONTAINS = "CONTAINS";
        const OPERATOR_EQUALS = "EQUALS";
        const OPERATOR_NOT_EQUALS = "NOT_EQUALS";
        const OPERATOR_GREATER_THAN = "GREATER_THAN";
        const OPERATOR_LESS_THAN = "LESS_THAN";
        const OPERATOR_GREATER_THAN_OR_EQUALS = "GREATER_THAN_OR_EQUALS";
        const OPERATOR_LESS_THAN_OR_EQUALS = "LESS_THAN_OR_EQUALS";

        const SORTING_TYPE_ASC = "ASC";
        const SORTING_TYPE_DESC = "DESC";

        static $TABELAS_COM_CORPORACAO = array();

        public $strToRaiz = null;
        public $nomeClasse;
        public $campoId;
        public $campoLabel;
        public $database;
        public $camposObrigatorios;
        public $labels;
        public $campos;
        public $tipos;
        public $opcoesDAO = null;
        public $arrEstadosCampos;

        public static $nomeTabela;
        public function __construct($configDAO = null)
        {
            $db = null;
            $configDatabase = null;
            if ($configDAO != null)
            {
                $strClassDAO = get_class($configDAO);
                if ($strClassDAO == "ConfiguracaoDAO")
                {
                    $db = $configDAO->db;
                    $configDatabase = $configDAO->configDatabase;
                    $this->opcoesDAO = $configDAO->opcoesDAO;
                }
                else
                {
                    if ($strClassDAO == "Database")
                    {
                        $db = $configDAO;
                    }
                }
            }

            if ($db != null)
            {
                $this->database = $db;
            }
            else
            {
                $this->database = new Database($configDatabase);
            }

            $this->camposObrigatorios = array();
            $this->labels = array();
            $this->campos = array();
            $this->tipos = array();
        }

        public function setDataCadastroAndOffsetInSecondsFromUser($parameters)
        {
            if(is_object($parameters))
            {
                if(!Helper::isNullOrEmpty($parameters->__currentTimeInSeconds))
                {
                    $this->cadastroSec = $parameters->__currentTimeInSeconds;
                }

                if(!Helper::isNullOrEmpty($parameters->__currentTimezoneOffsetInSeconds))
                {
                    $this->cadastroOffsec = $parameters->__currentTimezoneOffsetInSeconds;
                }
            }
        }

        public function defineDataCadastroInSecondsIfNotDefined()
        {
            if(Helper::isNullOrEmpty($this->cadastroSec))
            {
                $this->cadastroSec = Helper::getServerTimestampUtcAtual();
            }
        }

        public function defineDataCadastroOffsetInSecondsIfNotDefined()
        {

            if(Helper::isNullOrEmpty($this->cadastroOffsec))
            {
                $this->cadastroOffsec = Helper::getServerTimezoneOffsetInSeconds();
            }
        }

        public function isCampoFormatadoParaExibicao($nomeCampoMysql)
        {
            return isset($this->arrEstadosCampos[$nomeCampoMysql]) &&  $this->arrEstadosCampos[$nomeCampoMysql] == static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
        }

        public function isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida($nomeCampoMysql)
        {
            return !isset($this->arrEstadosCampos[$nomeCampoMysql]) || static::isCampoFormatadoParaExibicao($nomeCampoMysql);
        }

        public function isCampoFormatadoParaBancoDeDados($nomeCampoMysql)
        {
            return isset($this->arrEstadosCampos[$nomeCampoMysql]) &&  $this->arrEstadosCampos[$nomeCampoMysql] == static::ESTADO_FORMATACAO_CAMPO_DB;
        }

        public function isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida($nomeCampoMysql)
        {
            return !isset($this->arrEstadosCampos[$nomeCampoMysql]) || static::isCampoFormatadoParaBancoDeDados($nomeCampoMysql);
        }

        public function marcarFormatacaoDoCampoComoExibicao($nomeCampoMysql)
        {
            $this->arrEstadosCampos[$nomeCampoMysql] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
        }

        public function marcarFormatacaoDoCampoComoBancoDeDados($nomeCampoMysql)
        {
            $this->arrEstadosCampos[$nomeCampoMysql] = static::ESTADO_FORMATACAO_CAMPO_DB;
        }

        public static function getStrQueryWhereUnique($pObjTable, $pVetorNomeAtributo)
        {
            if (count($pVetorNomeAtributo) == 0 || $pObjTable == null)
            {
                return null;
            }
            $vQuery = "WHERE ";
            $vQueryWhere = "";
            foreach ($pVetorNomeAtributo as $vAttrName)
            {
                $vAttrValue = call_user_func(array($pObjTable, "get_" . ucfirst($vAttrName)), array());
                if (strlen($vAttrValue))
                {
                    if (strlen($vQueryWhere) == 0)
                    {
                        $vQueryWhere .= $vAttrName . " = " . $vAttrValue;
                    }
                    else
                    {
                        $vQueryWhere .= " AND " . $vAttrName . " = '" + $vAttrValue + "' ";
                    }
                }
            }

            return $vQuery + $vQueryWhere;
        }

        public static function formatarTimestampComOffsetParaExibicao($timestampUTC, $offset)
        {
            $timestampReal = $timestampUTC + $offset;

            return I18N::getFormattedDateTime($timestampReal);
        }

        public static function getWhereClauseForFilter($arrParametrosFiltro, $aliasTabelaPrincipal = null)
        {
            //define nome dos campos caso não tenha sido definido anteriormente
            static::setDatabaseFieldNames();

            $strRetorno = "";
            if (!is_array($arrParametrosFiltro))
            {
                //TODO otimizar porque essa funcao é muito cara de converter
                $arrParametrosFiltro = static::convertSimpleFilterParameterObjectToCompleteArray($arrParametrosFiltro);
            }

            if (Helper::isNullOrEmpty($arrParametrosFiltro))
            {
                return $strRetorno;
            }

            $arrCondicoes = array();
            foreach ($arrParametrosFiltro as $parametroFiltro)
            {
                $condicao = static::getWhereClause($parametroFiltro, $aliasTabelaPrincipal);
                if (!is_null($condicao))
                {
                    $arrCondicoes[] = $condicao;
                }
            }

            $strRetorno = implode(" AND ", $arrCondicoes);

            return strlen($strRetorno) > 0 ? "WHERE {$strRetorno}" : "";
        }

        public static function convertSimpleFilterParameterObjectToCompleteArray($filterParameter)
        {
            $arrRetorno = array();
            if(is_object($filterParameter))
            {
                foreach ($filterParameter as $key => $value)
                {
                    $objRetorno = new stdClass();
                    $objRetorno->attribute = $key;
                    $objRetorno->value = $value;
                    $objRetorno->operator = 'EQUALS';

                    $arrRetorno[] = $objRetorno;
                }
            }

            return $arrRetorno;
        }

        public static function getFilterValueFromFilterArray($attribute, $arrParametrosFiltro)
        {
            if(is_null($arrParametrosFiltro))
            {
                return null;
            }
            elseif (is_object($arrParametrosFiltro))
            {
                if(isset($arrParametrosFiltro->$attribute)){
                    return $arrParametrosFiltro->$attribute;
                }
//                $arrParametrosFiltro = static::convertSimpleFilterParameterObjectToCompleteArray($arrParametrosFiltro);
            }

            for($i=0; $i < count($arrParametrosFiltro); $i++)
            {
                if($arrParametrosFiltro[$i]->attribute == $attribute)
                {
                    return $arrParametrosFiltro[$i]->value;
                }
            }

            return null;
        }

        public static function getWhereClause($parametroFiltro, $aliasTabelaPrincipal = null)
        {
            $strRetorno = null;
            $atributoClasse = $parametroFiltro->attribute;
            $valor = $parametroFiltro->value;
            $operador = trim(strtoupper($parametroFiltro->operator));

            if (is_object($valor) && isset($valor->primaryKey))
            {
                $valor = $valor->primaryKey->id;
            }

            if (static::isOperatorValid($operador))
            {
                $campoBanco = isset(static::$databaseFieldNames->$atributoClasse) ? static::$databaseFieldNames->$atributoClasse : $atributoClasse;
                $tipoCampo = static::$databaseFieldTypes->$atributoClasse;
                $strAlias = is_null($aliasTabelaPrincipal) ? "" : "{$aliasTabelaPrincipal}.";

                switch ($tipoCampo)
                {
                    case static::TIPO_VARIAVEL_TEXT:
                    case static::TIPO_VARIAVEL_DATE:
                    case static::TIPO_VARIAVEL_DATETIME:
                    case static::TIPO_VARIAVEL_TIME:

                        if ($valor == null && trim($valor) == '')
                        {
                            $strRetorno = null;
                        }
                        else
                        {
                            $operadorMaisValor = static::getOperatorWithTextValue($valor, $operador);
                            $strRetorno = "{$strAlias}{$campoBanco} {$operadorMaisValor}";
                        }

                        break;

                    case static::TIPO_VARIAVEL_BOOLEAN:
                    case static::TIPO_VARIAVEL_FLOAT:
                    case static::TIPO_VARIAVEL_INTEGER:
                    case static::TIPO_VARIAVEL_TIME_IN_SECONDS:
                    case static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS:

                        if ($valor === null)
                        {
                            $strRetorno = null;
                        }
                        else
                        {
                            if(is_bool($valor))
                            {
                                $valor = ($valor === false) ? 0 : 1;
                            }

                            $operadorMaisValor = static::getOperatorWithNumericValue($valor, $operador);
                            $strRetorno = "{$strAlias}{$campoBanco} {$operadorMaisValor}";
                        }

                        break;

                }

            }

            return $strRetorno;
        }

        public static function isOperatorValid($operador)
        {
            return in_array($operador,
                            array(static::OPERATOR_STARTS_WITH, static::OPERATOR_CONTAINS, static::OPERATOR_ENDS_WITH,
                                    static::OPERATOR_EQUALS, static::OPERATOR_NOT_EQUALS, static::OPERATOR_GREATER_THAN,
                                        static::OPERATOR_LESS_THAN, static::OPERATOR_GREATER_THAN_OR_EQUALS, static::OPERATOR_LESS_THAN_OR_EQUALS));
        }

        public static function getOperatorWithTextValue($valorTexto, $operador)
        {
            switch ($operador)
            {
                case static::OPERATOR_STARTS_WITH:
                    $strRetorno = "LIKE '{$valorTexto}%'";
                    break;

                case static::OPERATOR_CONTAINS:
                    $strRetorno = "LIKE '%{$valorTexto}%'";
                    break;

                case static::OPERATOR_ENDS_WITH:
                    $strRetorno = "LIKE '%{$valorTexto}'";
                    break;

                case static::OPERATOR_EQUALS:
                    $strRetorno = " = '{$valorTexto}'";
                    break;

                case static::OPERATOR_NOT_EQUALS:
                    $strRetorno = " <> '{$valorTexto}'";
                    break;

                case static::OPERATOR_GREATER_THAN:
                    $strRetorno = " > '{$valorTexto}'";
                    break;

                case static::OPERATOR_LESS_THAN:
                    $strRetorno = " < '{$valorTexto}'";
                    break;

                case static::OPERATOR_GREATER_THAN_OR_EQUALS:
                    $strRetorno = " >= '{$valorTexto}'";
                    break;

                case static::OPERATOR_LESS_THAN_OR_EQUALS:
                    $strRetorno = " <= '{$valorTexto}'";
                    break;

                default:
                    $strRetorno = "{$operador} '{$valorTexto}'";
                    break;

            }

            return $strRetorno;
        }

        public static function getOperatorWithNumericValue($valorNumerico, $operador)
        {
            switch ($operador)
            {
                case static::OPERATOR_EQUALS:
                    $strRetorno = " = {$valorNumerico}";
                    break;

                case static::OPERATOR_NOT_EQUALS:
                    $strRetorno = " <> {$valorNumerico}";
                    break;

                case static::OPERATOR_GREATER_THAN:
                    $strRetorno = " > {$valorNumerico}";
                    break;

                case static::OPERATOR_LESS_THAN:
                    $strRetorno = " < {$valorNumerico}";
                    break;

                case static::OPERATOR_GREATER_THAN_OR_EQUALS:
                    $strRetorno = " >= {$valorNumerico}";
                    break;

                case static::OPERATOR_LESS_THAN_OR_EQUALS:
                    $strRetorno = " <= {$valorNumerico}";
                    break;

                default:
                    $strRetorno = "{$operador} {$valorNumerico}";
                    break;

            }

            return $strRetorno;
        }

        public static function getLimitClauseForFilter($paginationParameters)
        {
            $strRetorno = "";
            if (Helper::isNullOrEmpty($paginationParameters))
            {
                return $strRetorno;
            }

            $offset = $paginationParameters->offset;
            $numberOfRecords = $paginationParameters->numberOfRecords;
            if (is_numeric($offset) && is_numeric($numberOfRecords))
            {
                $strRetorno = "LIMIT {$offset} {$numberOfRecords}";
            }

            return $strRetorno;
        }

        public static function getOrderByClauseForFilter($arrSortingParameters)
        {
            $strRetorno = "";
            if (Helper::isNullOrEmpty($arrSortingParameters))
            {
                return $strRetorno;
            }

            usort($arrSortingParameters, function ($a, $b)
            {
                return $a->order - $b->order;
            });

            $arrSQLSorting = array();
            foreach ($arrSortingParameters as $sortingParameter)
            {
                $aliasCampo = $sortingParameter->columnAlias;
                $tipoSort = $sortingParameter->sortingType == static::SORTING_TYPE_ASC ? static::SORTING_TYPE_ASC : static::SORTING_TYPE_DESC;
                $arrSQLSorting[] = "{$aliasCampo} {$tipoSort}";
            }

            $strRetorno = implode(", ", $arrSQLSorting);
            return strlen($strRetorno) > 0 ? "ORDER BY {$strRetorno}" : "";
        }

        public function closeDatabase()
        {
            $this->database->close();
        }

        public function getConfiguracaoDAO()
        {
            $config = new ConfiguracaoDAO();
            $config->db = $this->database;
            return $config;
        }

        public static function isCorporacaoExistenteNaTabela($pNomeTabela, $db = null)
        {
            if (!strlen($pNomeTabela))
            {
                return null;
            }
            if (isset(static::$TABELAS_COM_CORPORACAO[ $pNomeTabela ]))
            {
                if (static::$TABELAS_COM_CORPORACAO[ $pNomeTabela ])
                {
                    return new Mensagem(null, null);
                }
                else
                {
                    return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO);
                }
            }

            if ($db == null)
            {
                $db = new Database();
            }

            $msg = $db->queryMensagem("SHOW COLUMNS FROM {$pNomeTabela}  WHERE Field = 'corporacao_id_INT'");
            if ($msg != null && $msg->erro())
            {
                return $msg;
            }
            else
            {
                if ($msg->rows() > 0)
                {
                    static::$TABELAS_COM_CORPORACAO[ $pNomeTabela ] = true;
                    return new Mensagem(null, null);
                }
                else
                {
                    static::$TABELAS_COM_CORPORACAO[ $pNomeTabela ] = false;
                    return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO);
                }
            }
        }

        function selectUltimoRegistroInserido($pIsCorporacaoSetada = true)
        {
            $queryCorporacao = "";
            $msg = Generic_DAO::isCorporacaoExistenteNaTabela($this->nomeTabela);

            if ($pIsCorporacaoSetada && ($msg == null || $msg->ok()))
            {
                $vIdCorporacao = Seguranca::getIdDaCorporacaoLogada();
                if (strlen($vIdCorporacao))
                {
                    $queryCorporacao .= " WHERE corporacao_id_INT = " . $vIdCorporacao;
                }

            }
            $sql = "SELECT MAX({$this->campoId}) FROM {$this->nomeTabela} $queryCorporacao";
            $result = $this->database->query($sql);

            if ($this->database->rows() > 0)
            {
                $id = Database::mysqli_result($result, 0, 0);
                $this->select($id);

                return;
            }
            else
            {
                return;
            }
        }

		public function getNomeTabela()
        {
            return $this->nomeTabela;
        }

        public function updateCampo($id, $campo, $valor)
        {
            if (is_numeric($id) && strlen($campo))
            {
                $nomeTabela = static::$nomeTabela;
                $this->database->query("UPDATE {$nomeTabela} SET {$campo}='{$valor}' WHERE {$this->campoId}={$id}");
            }
        }

        public function formatarStringNormalizadaParaComandoSQL($string)
        {
            $string = Helper::retiraAcento($string);
            $string = $this->formatarStringParaComandoSQL($string);

            return $string;
        }

        public function formatarStringParaComandoSQL($valor)
        {
            return $valor == null ? "null" : "'" . $this->database->realEscapeString($valor) . "'";
        }

        function getIdDoUltimoRegistroInserido()
        {
            if ($this->id != null)
            {
                return $this->id;
            }

            $msg = $this->database->selectLastId();
            if ($msg != null && $msg->ok())
            {
                return $msg->mValor;
            }
            else
            {
                return null;
            }
        }

        public function mapFromDatabaseFetchObject($databaseFetchObject)
        {
            $fs = static::getDatabaseFieldNames();
            if ($fs == null)
            {
                return null;
            }
            $arrayMapeamento = Helper::objectToArray($fs);
            foreach ($databaseFetchObject as $databaseField => $value)
            {
                $classProperty = array_search($databaseField, $arrayMapeamento);
                if (!Helper::isNullOrEmpty($classProperty))
                {
                    $this->$classProperty = $value;
                }
            }
        }

        public function setByObject($object)
        {
            foreach ($object as $key => $value)
            {
                $this->$key = is_object($value) ? $value->id : $value;
            }
        }

        public static function formatarValorParaExibicao($valor, $tipoCampo)
        {
            switch ($tipoCampo)
            {
                case static::TIPO_VARIAVEL_INTEGER:
                    return $valor;
                    break;

                case static::TIPO_VARIAVEL_FLOAT:
                    return static::formatarFloatParaExibicao($valor);
                    break;

                case static::TIPO_VARIAVEL_DATE:
                    return static::formatarDataParaExibicao($valor);
                    break;

                case static::TIPO_VARIAVEL_TIME:
                    return static::formatarHoraParaExibicao($valor);
                    break;

                case static::TIPO_VARIAVEL_DATETIME:
                    return static::formatarDataTimeParaExibicao($valor);
                    break;

                case static::TIPO_VARIAVEL_BOOLEAN:
                    return static::formatarBooleanParaExibicao($valor);
                    break;

                case static::TIPO_VARIAVEL_TEXT:
                    return static::formatarStringParaExibicao($valor);
                    break;

                case static::TIPO_VARIAVEL_TIME_IN_SECONDS:
                case static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS:
                    return static::formatarIntegerParaExibicao($valor);
                    break;

                default:
                    return $valor;
                    break;
            }
        }

        public static function formatarStringParaExibicao($valor)
        {
            return $valor;
        }

        public static function formatarFloatParaExibicao($numero, $casasDecimais = 2)
        {
            return Helper::formatarFloatParaExibicao($numero, $casasDecimais);
        }

        public static function formatarDataParaExibicao($valor)
        {
            return Helper::formatarDataParaExibicao($valor);
        }

        public static function formatarHoraParaExibicao($valor)
        {
            return Helper::formatarHoraParaExibicao($valor);
        }

        public static function formatarDataTimeParaExibicao($valor)
        {
            return Helper::formatarDataTimeParaExibicao($valor);
        }

        public static function formatarBooleanParaExibicao($valor)
        {
            return Helper::formatarBooleanParaExibicao($valor);
        }

        public static function formatarIntegerParaExibicao($valor)
        {
            return intval($valor);
        }

        public static function formatarFloatParaComandoSQL($numero)
        {
            return Helper::formatarFloatParaComandoSQL($numero);
        }

        public static function formatarDataParaComandoSQL($valor)
        {
            return Helper::formatarDataParaComandoSQL($valor);
        }

        public static function formatarHoraParaComandoSQL($valor)
        {
            return Helper::formatarHoraParaComandoSQL($valor);
        }

        public static function formatarDataTimeParaComandoSQL($valor)
        {
            return Helper::formatarDataTimeParaComandoSQL($valor);
        }

        public static function formatarBooleanParaComandoSQL($valor)
        {
            return Helper::formatarBooleanParaComandoSQL($valor);
        }

        public static function formatarIntegerParaComandoSQL($valor)
        {
            return Helper::formatarIntegerParaComandoSQL($valor);
        }

        public static function getRecordAliasRelatedAttributeName($sqlFieldAlias)
        {
            return static::$databaseFieldsRelatedAttributes->$sqlFieldAlias;
        }

        public static function getListAliasRelatedAttributeName($sqlFieldAlias)
        {
            return static::$listAliasRelatedAttributes->$sqlFieldAlias;
        }

        public static function mapResultSetDataToRecord($resultSetData)
        {
            for ($i = 0; $object = mysqli_fetch_object($resultSetData); $i++)
            {
                $currentObject = new stdClass();
                foreach ($object as $key => $value)
                {
                    $attribute = static::$databaseFieldsRelatedAttributes->$key;
                    $currentObject->$attribute = $value;
                }

                if(!is_null(static::$databaseFieldTypes))
                {
                    static::appendTypesProperty($currentObject, static::$databaseFieldTypes);
                }

                return $currentObject;
            }

            return null;
        }

        public static function mapGenericResultSetDataToRecord($resultSetData, $fieldTypesMappingObject=null)
        {
            for ($i = 0; $object = mysqli_fetch_object($resultSetData); $i++)
            {
                if(!is_null($fieldTypesMappingObject))
                {
                    static::appendTypesProperty($object, $fieldTypesMappingObject);
                }

                return $object;
            }

            return null;
        }

        public static function mapGenericResultSetDataToList($resultSetData)
        {
            $returnData = array();
            for ($i = 0; $object = mysqli_fetch_object($resultSetData); $i++)
            {
                $returnData[] = $object;
            }

            return $returnData;
        }

        public static function mapGenericResultSetDataToListAppendingTypes($resultSetData, $typesMappingObject)
        {
            $returnData = array();
            for ($i = 0; $object = mysqli_fetch_object($resultSetData); $i++)
            {
                if(!is_null($typesMappingObject))
                {
                    static::appendTypesProperty($object, $typesMappingObject);
                }
                $returnData[] = $object;
            }

            return $returnData;
        }

        public function mapGenericResultSetDataToListIncludingHuman($resultSetData, $objAliasTypes)
        {
            $returnData = array();
            for ($i = 0; $object = mysqli_fetch_object($resultSetData); $i++)
            {
                $currentObject = new stdClass();
                foreach ($object as $key => $value)
                {
                    $attributeName = $key;
                    $currentObject->$attributeName = $value;

                    $attributeType = $objAliasTypes->$key;
                    $valueForHuman = static::formatarValorParaExibicao($value, $attributeType);
                    $attributeNameForHuman = "{$attributeName}_forHuman";
                    $currentObject->$attributeNameForHuman = $valueForHuman;
                }

                $returnData[] = $currentObject;
            }

            return $returnData;
        }

        public static function mapResultSetDataToListForComboBox($resultSetData, $aliasRelatedAttributes=null)
        {
            if(is_null($aliasRelatedAttributes))
            {
                $aliasRelatedAttributes = static::$listAliasRelatedAttributes;
            }

            $returnData = array();
            for ($i = 0; $object = mysqli_fetch_object($resultSetData); $i++)
            {
                $currentObject = new stdClass();
                foreach ($object as $key => $value)
                {
                    $attribute = $aliasRelatedAttributes->$key;
                    if(!is_null($attribute))
                    {
                        $currentObject->$attribute = $value;
                    }
                }

                $returnData[] = $currentObject;
            }

            return $returnData;
        }

        public static function mapResultSetDataToListForGrid($resultSetData, $objAliasTypes=null)
        {
            $returnData = array();
            $formatForHuman = !is_null($objAliasTypes);
            for ($i = 0; $object = mysqli_fetch_object($resultSetData); $i++)
            {
                $currentObject = new stdClass();
                $currentObject->allowEdit = true;
                $currentObject->allowRemove = true;

                foreach ($object as $key => $value)
                {
                    $attributeName = $key;
                    $attributeNameForHuman = "{$attributeName}_forHuman";

                    if($formatForHuman)
                    {
                        $attributeType = $objAliasTypes->$key;
                        $valueForHuman = static::formatarValorParaExibicao($value, $attributeType);
                    }
                    else
                    {
                        $valueForHuman = $value;
                    }

                    $currentObject->$attributeName = $value;
                    $currentObject->$attributeNameForHuman = $valueForHuman;
                }

                $currentObject->primaryKey = static::getPrimaryKeyObject($currentObject);
                $returnData[] = $currentObject;
            }

            return $returnData;
        }

        public static function mapResultSetDataToListForRelatedEntity($resultSetData)
        {
            $returnData = array();
            for ($i = 0; $object = mysqli_fetch_object($resultSetData); $i++)
            {
                $currentObject = new stdClass();
                $currentObject->removed = false;

                foreach ($object as $key => $value)
                {
                    $currentObject->$key = $value;
                }

                $returnData[] = $currentObject;
            }

            return $returnData;
        }

        public static function appendTypesProperty(&$responseObject, $typesObject)
        {
            if(is_object($responseObject))
            {
                $responseObject->__types = $typesObject;
            }
        }

        public static function getListMappingType($listMappingType)
        {
            switch ($listMappingType)
            {
                case static::LIST_MAPPING_TYPE_GRID:
                    return static::LIST_MAPPING_TYPE_GRID;
                    break;

                case static::LIST_MAPPING_TYPE_COMBOBOX:
                    return static::LIST_MAPPING_TYPE_COMBOBOX;
                    break;

                case static::LIST_MAPPING_TYPE_RELATED_ENTITY:
                    return static::LIST_MAPPING_TYPE_RELATED_ENTITY;
                    break;

                default:
                    return static::LIST_MAPPING_TYPE_GENERIC;
                    break;
            }

        }

        public static function mapResultSetDataToList($resultSetData, $listMappingType, $objAliasTypes=null, $appendTypesProperty=true)
        {
            $returnData = array();
            switch ($listMappingType)
            {
                case static::LIST_MAPPING_TYPE_GRID:
                    $returnData = static::mapResultSetDataToListForGrid($resultSetData, $objAliasTypes);
                    break;

                case static::LIST_MAPPING_TYPE_COMBOBOX:
                    $returnData = static::mapResultSetDataToListForComboBox($resultSetData);
                    break;

                case static::LIST_MAPPING_TYPE_RELATED_ENTITY:
                    $returnData = static::mapResultSetDataToListForRelatedEntity($resultSetData);
                    break;

                default:
                    $returnData = static::mapGenericResultSetDataToList($resultSetData);
                    break;
            }

            if ($appendTypesProperty && !is_null($objAliasTypes) && is_object($objAliasTypes))
            {
                static::appendTypesProperty($returnData, $objAliasTypes);
            }

            return $returnData;
        }

        public static function getPrimaryKeyObject($fullObject)
        {
            $objRetorno = new stdClass();
            foreach (static::$arrPK as $pkProperty)
            {
                $pkPropertyQuery = static::nomeTabela() . "__{$pkProperty}";
                $objRetorno->$pkProperty = $fullObject->$pkPropertyQuery;
            }

            return $objRetorno;
        }

        public function mapNumericFromDatabaseFetchObject($databaseFetchObject)
        {
            $fs = $this->getDatabaseFieldNames();
            if ($fs == null)
            {
                return null;
            }
            $arrayMapeamento = Helper::objectToArray($fs);
            foreach ($databaseFetchObject as $databaseField => $value)
            {
                $classProperty = array_search($databaseField, $arrayMapeamento);
                if (!Helper::isNullOrEmpty($classProperty))
                {
                    $this->$classProperty = $value;
                }
            }
        }


        public function formatarDados($valor)
        {

            if ($valor == null)
            {
                return "null";
            }
            else
            {
                if (!strlen($valor))
                {
                    return "null";
                }
                else
                {
                    $tratado = "'{$this->database->realEscapeString($valor)}'";

                    return $tratado;
                }
            }
        }


        public function formatarDadosParaSQL($valor)
        {
            return $this->formatarDados($valor);
        }
}
