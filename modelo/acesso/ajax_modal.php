<?php

//Pagina Wrapper para requisições Ajax

include_once '../recursos/php/constants.php';
include_once '../recursos/php/database_config.php';
include_once '../client_area/imports/header.php';
include_once '../recursos/php/funcoes.php';

include_once '../client_area/imports/sessao.php';

$tituloModal = Helper::POSTGET("titulo_modal");
$idModal = Helper::POSTGET("id_modal");
$botaoFechar = Helper::POSTGET('botao_fechar')
?>

<a class='btn btn-info btn-lg' id="<?= $idModal ?>Botao" data-toggle='modal' href='#<?= $idModal; ?>'
   style="visibility: hidden;" role='button'>Launch modal with form</a>
<div class='modal fade in' id='<?= $idModal; ?>' tabindex='-1'>
    <div class='modal-dialog'>
        <div class='modal-content'>
            <div class='modal-header'>
                <?
                if ($botaoFechar) {
                    ?>

                    <button aria-hidden='true' class='close' data-dismiss='modal' type='button'>×</button>
                <?
                }
                if (strlen($tituloModal)) {
                    ?>
                    <h4 class='modal-title' id='myModalLabel'><?= $tituloModal; ?></h4>
                <?
                }
                ?>
            </div>
            <div class='modal-body'>
                <?
                if (Helper::GET("ajax_tipo") && Helper::GET("ajax_page")) {

                    $tipoAjax = Helper::GET("ajax_tipo");
                    $paginaAjax = Helper::GET("ajax_page");

                    include_once "{$tipoAjax}/{$paginaAjax}.php";

                } elseif (Helper::GET("class") && Helper::GET("funcao")) {

                    $classe = Helper::GET("class");

                    $obj = call_user_func_array(array($classe, "factory"), "");

                    $funcaoComParametros = Helper::GET("funcao");

                    $metodo = Helper::getNomeFuncao($funcaoComParametros);
                    $parametros = Helper::getParametrosFuncao($funcaoComParametros);

                    $retorno = call_user_func_array(array($obj, $metodo), $parametros);

                    print $retorno;

                } elseif (Helper::GET("class") && Helper::GET("metodo_estatico")) {

                    $classe = Helper::GET("class");
                    $funcaoComParametros = Helper::GET("metodo_estatico");

                    $metodo = Helper::getNomeFuncao($funcaoComParametros);
                    $parametros = Helper::getParametrosFuncao($funcaoComParametros);

                    $retorno = call_user_func_array(array($classe, $metodo), $parametros);

                    print $retorno;

                }

                ?>
            </div>
            <?
            if ($botaoFechar == 'true') {
                ?>
                <div class='modal-footer'>
                    <button class='btn btn-default' data-dismiss='modal' type='button'>Fechar</button>
                </div>
            <?
            }
            ?>
        </div>
    </div>
</div>
   
