"use strict";
exports.__esModule = true;
var SearchableAttribute = (function () {
    function SearchableAttribute(valor, isSearchable, isPrimaryKey) {
        if (valor === void 0) { valor = null; }
        if (isSearchable === void 0) { isSearchable = true; }
        if (isPrimaryKey === void 0) { isPrimaryKey = false; }
        this.valor = valor;
        this.isSearchable = isSearchable;
        this.isPrimaryKey = isPrimaryKey;
    }
    SearchableAttribute.prototype.getValue = function () {
        return this.valor;
    };
    SearchableAttribute.prototype.setValue = function (valor) {
        this.valor = valor;
    };
    SearchableAttribute.prototype.getSearchable = function () {
        return this.isSearchable;
    };
    SearchableAttribute.prototype.setSearchable = function (isSearchable) {
        this.isSearchable = isSearchable;
    };
    SearchableAttribute.prototype.getPrimaryKey = function () {
        return this.isPrimaryKey;
    };
    SearchableAttribute.prototype.setPrimaryKey = function (isPrimaryKey) {
        this.isPrimaryKey = isPrimaryKey;
    };
    return SearchableAttribute;
}());
exports.SearchableAttribute = SearchableAttribute;
