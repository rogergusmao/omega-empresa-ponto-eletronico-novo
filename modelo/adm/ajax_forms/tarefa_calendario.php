<?php
$dataProgramadaEscolhida = Helper::GET('data_programada_DATE');

$dataCarregarDe = Helper::GET('data_inicio_DATE');
$dataCarregarAte = Helper::GET('data_termino_DATE');

if (strlen($dataCarregarDe) && strlen($dataCarregarAte))
{
    ?>

    <table class="tabela_list" style="margin-top: 20px;">
        <colgroup>
            <col width="12%"/>
            <col width="12%"/>
            <col width="12%"/>
            <col width="12%"/>
            <col width="12%"/>
            <col width="12%"/>
            <col width="12%"/>
        </colgroup>
        <thead>
        <tr class="tr_list_titulos">

            <td class="td_calendario_titulos"><input type="checkbox" id="check_todos"/> Todos os Dias</td>
            <td class="td_calendario_titulos">
                <input type="checkbox" id="check_c1"/>
                <?= "Segunda-Feira" ?>
            </td>
            <td class="td_calendario_titulos">
                <input type="checkbox" id="check_c2"/>
                <?= "Ter�a-Feira" ?>
            </td>
            <td class="td_calendario_titulos">
                <input type="checkbox" id="check_c3"/>
                <?= "Quarta-Feira" ?>
            </td>
            <td class="td_calendario_titulos">
                <input type="checkbox" id="check_c4"/>
                <?= "Quinta-Feira" ?>
            </td>
            <td class="td_calendario_titulos">
                <input type="checkbox" id="check_c5"/>
                <?= "Sexta-Feira" ?>
            </td>
            <td class="td_calendario_titulos">
                <input type="checkbox" id="check_c6"/>
                <?= "S�bado" ?>
            </td>
            <td class="td_calendario_titulos">
                <input type="checkbox" id="check_c7"/>
                <?= "Domingo" ?>
            </td>

        </tr>
        </thead>
        <tbody>

        <?
        $timestampDataCarregarDe = Helper::getUnixTimestamp($dataCarregarDe);
        $timestampDataCarregarAte = Helper::getUnixTimestamp($dataCarregarAte);

        $dataCarregarDeDiaDaSemana = date('N', $timestampDataCarregarDe);
        $dataCarregarAteDiaDaSemana = date('N', $timestampDataCarregarAte);

        $diaInicialCalendario = $timestampDataCarregarDe - ($dataCarregarDeDiaDaSemana - 1) * SEGUNDOS_EM_UM_DIA;
        $diaFinalCalendario = $timestampDataCarregarAte + (7 - $dataCarregarAteDiaDaSemana) * SEGUNDOS_EM_UM_DIA;

        $contadorDias = 0;
        $contadorDiasSemana = 0;
        $semana = 0;
        for ($i = $diaInicialCalendario; $i <= $diaFinalCalendario; $i += SEGUNDOS_EM_UM_DIA)
        {

            if ($contadorDias % 7 == 0)
            {

                $semana++;
                $contadorDiasSemana = 1;

                $classeCss = ($semana % 2 == 0) ? "tr_list_conteudo_impar" : "tr_list_conteudo_par";
                echo "<tr class=\"{$classeCss}\"><td class=\"td_calendario_titulos\"><input type=\"checkbox\" id=\"check_l{$semana}\"> Semana {$semana}</td>";
            }

            $dataExibicao = date("d/m/Y", $i);
            echo "<td class=\"td_tarefa_calendario\">
                <input type=\"checkbox\" id=\"calendario_l{$semana}_c{$contadorDiasSemana}\" name=\"data_repetir_tarefa[]\" value=\"{$dataExibicao}\"/>&nbsp;
                {$dataExibicao}
              </td>";

            if ($contadorDias % 7 == 6)
            {

                echo "</tr>";
            }

            $contadorDias++;
            $contadorDiasSemana++;
        }

        ?>

        </tbody>
    </table>

<? } ?>

<script language="javascript">

    $('input[type="checkbox"][id^="check_"]').click(function ()
    {

        var selecao = $(this).attr('checked') ? true : false;
        var identificador = $(this).attr('id').replace('check_', '');
        $('input[type="checkbox"][id^="calendario"][id*="_' + identificador + '"]').attr('checked', selecao);

    });

    $('#check_todos').click(function ()
    {

        var selecao = $(this).attr('checked') ? true : false;
        $('input[type="checkbox"][id^="calendario"]').attr('checked', selecao);

    });

</script>
