import { CalendarioDePontos } from './uiComponente/CalendarioDePontos';
import { TotalizadorDeHoras } from './uiComponente/TotalizadorDeHoras';
import { DistribuicaoDeHoras } from './uiComponente/DistribuicaoDeHoras';
import { ProblemasEncontrados } from './uiComponente/ProblemasEncontrados';

import { CorrigirPonto } from './uiComponente/modal/CorrigirPonto';
import { ExportarDados } from './uiComponente/modal/ExportarDados';

export
{    
     CalendarioDePontos,
     TotalizadorDeHoras,
     DistribuicaoDeHoras,
     ProblemasEncontrados,

     CorrigirPonto,
     ExportarDados
}

