/*

 Arquivo gerado atrav�s de gerador de c�digo em 07/01/2018 as 15:54:14.
 Para que o arquivo n�o seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

 Tabela correspondente: usuario_tipo
 Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

 */

returnObject.getUsuarioTipo = function (filterParameters)
{

    var parameters = {
        filterParameters: filterParameters,
        loadComboBoxesData: true
    };

    return $http({
        url: baseURLAdm + "/webservice.php?class=EXTDAO_Usuario_tipo&action=__getRecord",
        method: "POST",
        data: parameters,
        headers: {
            'Content-Type': 'application/json'
        }
    });

};

returnObject.addUsuarioTipo = function (formData)
{

    return $http({
        url: baseURLAdm + "/webservice.php?class=EXTDAO_Usuario_tipo&jsonOutput=true&action=add",
        method: "POST",
        data: formData,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    });

};

returnObject.editUsuarioTipo = function (formData)
{

    return $http({
        url: baseURLAdm + "/webservice.php?class=EXTDAO_Usuario_tipo&jsonOutput=true&action=edit",
        method: "POST",
        data: formData,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    });

};

returnObject.getAllComboBoxesDataForUsuarioTipoForm = function ()
{
    return $http({
        url: baseURLAdm + "/webservice.php?class=EXTDAO_Usuario_tipo&jsonOutput=true&action=__getFormComboBoxesData",
        method: "POST",
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    });

};

returnObject.getListOfItemMenuSistemaWebForUsuarioForm = function ()
{
    var params = {
        filterParameters: null,
        isRelatedEntity: true
    };
    return $http.post(baseURLAdm + "/webservice.php?class=EXTDAO_Usuario_tipo&action=getListOfItemMenuSistemaWeb", params);
};



