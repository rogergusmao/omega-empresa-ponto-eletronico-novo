import { PeriodoTempo } from "lib/geral";

export class DataCarregada
{
    public data: Date;
    public periodosCarregados: Array<PeriodoTempo>;

    public constructor(data: Date)
    {
        this.data = data;
        this.periodosCarregados = new Array<PeriodoTempo>();
    }

    public addPeriodoCarregado(periodo: PeriodoTempo)
    {
        this.periodosCarregados.push(periodo);
    }

}