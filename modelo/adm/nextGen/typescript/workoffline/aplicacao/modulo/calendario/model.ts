import { Evento } from './model/Evento';
import { Pessoa } from './model/Pessoa';
import { PontoEletronico } from './model/PontoEletronico';
import { ProblemaEncontrado } from './model/ProblemaEncontrado';

export
{
    Evento,
    Pessoa,
    PontoEletronico,
    ProblemaEncontrado
}



