<?php


        /*

        Arquivo gerado através de gerador de código em 07/09/2017 as 12:49:45.
        Para que o arquivo não seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

        Tabela correspondente: wifi_registro
        Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

        */
?>

<!--Definição da View Angular JS -->
                   <?

                    $objGrid = new GridEnvironment("filterForm", "listWifiRegistroController", "vm");

                   ?><div class='row' id='content-wrapper' ng-controller="<?=$objGrid->getAngularControllerAttributeValue() ?>"><!--Definição do template do dialog exibido para confirmar remoção -->
                    <script type="text/ng-template" id="before-remove-template.html">
                        <div class="modal-header">
                            <h4 class="modal-title">
                                <span class="glyphicon"></span>
                                <?= I18N::getExpression("Atenção"); ?>
                            </h4>
                        </div>
                        <div class="modal-body">
                            <span class="help-block" ng-show="vm.selectedRecordsForRemoval.length > 1"><?= I18N::getExpression("Deseja remover os {0} registros selecionados?", array("{{vm.selectedRecordsForRemoval.length}}")); ?></span>
                            <span class="help-block" ng-show="vm.selectedRecordsForRemoval.length == 1"><?= I18N::getExpression("Deseja remover o registro selecionado?"); ?></span>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" ng-click="vm.closeRemoveMessageDialog()"><?= I18N::getExpression("Não") ?></button>
                            <button type="button" class="btn btn-default" ng-click="vm.removeSelectedRecords()"><?= I18N::getExpression("Sim") ?></button>
                        </div>
                    </script>

<div class='col-xs-12'><div class='row'>
                        <div class='col-sm-12'>
                            <div class='page-header'>
                                <h1 class='pull-left'>
                                    <i class='icon-edit'></i>
                                    <span>
                                        <?=I18N::getExpression("Wifi registro"); ?>
                                    </span>
                                </h1>
                            </div>
                        </div>
                    </div>

<?

            global $objFilter;
            $objFilter = $objGrid->getFilterEnvironment();
            $objFilter->setLayoutTwoColumns();

            ?>

            <?=Helper::includeSubView("filter_view"); ?>


            <div class="row">
                <div class="col-sm-12" ng-if="!isNullOrEmpty(<?=$objGrid->getAngularControllerAs() ?>.gridData)">

                    <div class="box bordered-box blue-border">
                        <div class="box-header blue-background">
                            <div class="title"><?=I18N::getExpression("Lista de Wifi registro") ?></div>
                            <?=$objGrid->renderListActionButtons(); ?>
                        </div>

                        <div class='collapse navbar-collapse navbar-ex1-collapse blue-background'>

                            <?=$objGrid->renderHighlightAndFilterPanel(); ?>

                            <ul class='nav navbar-nav navbar-form navbar-right'>
                                <button class='btn btn-default' ng-show="<?=$objGrid->getAngularControllerAs() ?>.selectedRecords.length > 1" ng-click="removeAllSelectedRecords('<?=I18N::getExpression("Deseja remover todos os registros selecionados?") ?>', vm)">Apagar Selecionados</button>
                            </ul>

                        </div>

                        <div class="box-content box-no-padding">
                            <div class="responsive-table">
                                <div class="scrollable-area dataTables_wrapper">
                                    <table class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>
                                                <?=$objGrid->renderSelectAllRecordsCheckbox(); ?>
                                            </th>

                                            <?=$objGrid->renderColumnHeader('id', 'Id'); ?> <?=$objGrid->renderColumnHeader('wifiId', 'WifiidINT'); ?> <?=$objGrid->renderColumnHeader('usuarioId', 'UsuarioidINT'); ?> <?=$objGrid->renderColumnHeader('dataInicioSec', ''); ?> <?=$objGrid->renderColumnHeader('dataInicioOffsec', ''); ?> <?=$objGrid->renderColumnHeader('dataFimSec', ''); ?> <?=$objGrid->renderColumnHeader('dataFimOffsec', ''); ?> 

                                            <th>
                                                <?=I18N::getExpression("Opções") ?>
                                            </th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr ng-repeat="record in <?=$objGrid->getAngularControllerAs() ?>.gridData track by $index">
                                            <td>
                                                <?=$objGrid->renderRecordCheckbox('record'); ?>
                                            </td><td>
                                                                           <?=$objGrid->renderColumnContent('record', 'wifi_registro', 'id'); ?>
                                                                       </td> <td>
                                                                           <?=$objGrid->renderColumnContent('record', 'wifi', 'id'); ?><br />
                                                                       </td> <td>
                                                                           <?=$objGrid->renderColumnContent('record', 'usuario', 'id'); ?><br />
                                                                       </td> <td>
                                                                           <?=$objGrid->renderColumnContent('record', 'wifi_registro', 'data_inicio_SEC'); ?>
                                                                       </td> <td>
                                                                           <?=$objGrid->renderColumnContent('record', 'wifi_registro', 'data_inicio_OFFSEC'); ?>
                                                                       </td> <td>
                                                                           <?=$objGrid->renderColumnContent('record', 'wifi_registro', 'data_fim_SEC'); ?>
                                                                       </td> <td>
                                                                           <?=$objGrid->renderColumnContent('record', 'wifi_registro', 'data_fim_OFFSEC'); ?>
                                                                       </td> 
                                            <td>
                                                <?=$objGrid->renderEditButton('record'); ?>
                                                <?=$objGrid->renderDeleteButton('record'); ?>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>

                                    <div class="row datatables-bottom">

                                        <?=$objGrid->renderPositionText(); ?>
                                        <?=$objGrid->renderPaginationBar(); ?>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

    </div>
                        </div>

        <!--Definição do Controller Angular JS -->
        <script type="text/javascript">

            <?=Helper::incluirConteudoDataServices(); ?>

        </script>

        <?=Helper::incluirControllerAngularJS(); ?>

        
