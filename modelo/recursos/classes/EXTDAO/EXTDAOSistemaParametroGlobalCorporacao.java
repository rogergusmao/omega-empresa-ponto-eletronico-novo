

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  EXTDAOSistemaParametroGlobalCorporacao
        * DATA DE GERA��O: 24.08.2017
        * ARQUIVO:         EXTDAOSistemaParametroGlobalCorporacao.java
        * TABELA MYSQL:    sistema_parametro_global_corporacao
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronicofactu.database.EXTDAO;

        import app.omegasoftware.pontoeletronicofactu.database.DAO.DAOSistemaParametroGlobalCorporacao;
        import app.omegasoftware.pontoeletronicofactu.database.Attribute;
        import app.omegasoftware.pontoeletronicofactu.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronicofactu.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronicofactu.database.Database;
        import app.omegasoftware.pontoeletronicofactu.database.Table;


        // **********************
        // DECLARA��O DA CLASSE
        // **********************


    
        

        public class EXTDAOSistemaParametroGlobalCorporacao extends DAOSistemaParametroGlobalCorporacao
        {


        // *************************
        // DECLARA��O DE ATRIBUTOS
        // *************************
        
        // *************************
        // CONSTRUTOR
        // *************************
        public EXTDAOSistemaParametroGlobalCorporacao(Database database){
            super(database);

        }
        
        // *************************
        // FACTORY
        // *************************
        public Table factory(){
            return new EXTDAOSistemaParametroGlobalCorporacao(this.getDatabase());

        }
        

        } // classe: fim


        