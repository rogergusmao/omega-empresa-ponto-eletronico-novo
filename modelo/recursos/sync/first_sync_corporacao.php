<?php


include_once '../php/constants.php';
include_once '../php/configuracao_corporacao_dinamica.php';
include_once '../php/funcoes.php';



$imei = Helper::POSTGET("imei");
$corporacao = Helper::POSTGET("corporacao");

$debug = Helper::POSTGET("debug");

if (!strlen($imei))
    $imei = Helper::POSTGET("imei");

if (!strlen($corporacao))
    Helper::GET("corporacao");

if (!strlen($imei))
    $imei = 1;

if (!strlen($corporacao))
    $corporacao = 1;

if (strlen($imei)) {

    $strImei = ".{$imei}";

}

$nomeDoArquivoDB = "database{$strImei}.db";
$nomeDoArquivoZip = "database{$strImei}.zip";
$diretorioApartirDaRaiz = "recursos/sync/databases";
$diretorio = ConfiguracaoSite::getPathConteudo() . $diretorioApartirDaRaiz;
$database = new Database();

chmod($diretorio, 0777);

$database->query("SELECT DISTINCT nome 
                  FROM sistema_tabela
                  WHERE (frequencia_sincronizador_INT = -1 OR frequencia_sincronizador_INT > 0) AND
                        transmissao_web_para_mobile_BOOLEAN = 1 AND
                        banco_mobile_BOOLEAN = 1");


$arrTabelas = Helper::getResultSetToArrayDeUmCampo($database->result);
$arrTabelasUtilizar = array();

$database->query("SHOW TABLES");

while ($dadosTabelasExistentes = $database->fetchArray()) {

    $tabelaExistente = $dadosTabelasExistentes[0];

    if (in_array($tabelaExistente, $arrTabelas)) {

        $arrTabelasUtilizar[] = $tabelaExistente;

    }

}

if (is_array($arrTabelasUtilizar)) {

    $strTabelas = implode(" ", $arrTabelasUtilizar);

} else {

    $strTabelas = "";

}
//$configuracaoDatabasePrincipal = new ConfiguracaoDatabase();
$configuracaoDatabasePrincipal = Registry::get('ConfiguracaoDatabasePrincipal');

$host = $configuracaoDatabasePrincipal->host;
$usuario = $configuracaoDatabasePrincipal->usuario;
$senha = $configuracaoDatabasePrincipal->senha;
$banco = $configuracaoDatabasePrincipal->DBName;

//-d -> dump somente a estrutura do banco
$comando = "mysqldump --compatible=ansi --default-character-set=latin1 --skip-extended-insert --compact --host={$host} --user={$usuario} --password={$senha} {$banco} {$strTabelas} > sql/{$imei}.sql";
Helper::shellExec($comando);

if ($debug) {

    print $comando . "<br>";

}

$database = new Database();

foreach ($arrTabelasUtilizar as $tabela) {

    $database->query("SHOW COLUMNS FROM " . $tabela . ";");

    $vVetorAttr = Helper::getResultSetToArrayDeUmCampo($database->result);
    $vConsultaTuplas = "SELECT *
                FROM {$tabela}";
    $msg = Generic_DAO::isCorporacaoExistenteNaTabela($tabela, $database);
    if ($msg->ok())
        $vConsultaTuplas .= " WHERE corporacao_id_INT = {$corporacao}";


    $database->query($vConsultaTuplas);

    $handleArquivo = fopen(ConfiguracaoSite::getPathConteudo()."sql/{$imei}.sql", "a");

    $primeiraVez = true;
    while ($dadosTabelasExistentes = $database->fetchArray()) {

        if ($primeiraVez) {
            $primeiraVez = false;
        }
        fwrite($handleArquivo, "INSERT INTO `{$tabela}` VALUES (");
        for ($i = 0; $i < count($vVetorAttr); $i++) {
            $vValorAtributo = $dadosTabelasExistentes[$i];
            if (is_null($vValorAtributo)) {
                if ($i == 0)
                    fwrite($handleArquivo, " NULL");
                else fwrite($handleArquivo, ", NULL");
            } else {
                $vValorAtributo = str_replace(array("\r\n", "\r", "\n"), " ", $vValorAtributo);
                if ($i == 0)
                    fwrite($handleArquivo, " '{$vValorAtributo}'");
                else fwrite($handleArquivo, ", '{$vValorAtributo}'");
            }
        }
        fwrite($handleArquivo, " );\n");
    }

    fclose($handleArquivo);
}
if (is_file("{$diretorio}/{$nomeDoArquivoDB}"))
    unlink("{$diretorio}/{$nomeDoArquivoDB}");

$comando = "";
if(Helper::getSistemaOperacionalDoServidor() == Helper::LINUX)
    $comando = "sh ./db_converter.corporacao.sh sql/{$imei}.sql | sqlite3 {$diretorio}/{$nomeDoArquivoDB}";
else
    $comando = PATH_CYGWIN."bash ./db_converter.corporacao.sh sql/{$imei}.sql | sqlite3 {$diretorio}/{$nomeDoArquivoDB}";
Helper::shellExec($comando);

if ($debug) {

    print $comando . "<br>";

}


Helper::criarArquivoZip(array("{$diretorio}/{$nomeDoArquivoDB}"), "{$diretorio}/{$nomeDoArquivoZip}", true);

$tamanhoEmBytes = filesize("{$diretorio}/{$nomeDoArquivoZip}");
$timestamp = filemtime("{$diretorio}/{$nomeDoArquivoZip}");
$urlAcesso = "http://" . DOMINIO_DE_ACESSO . "/" . $diretorioApartirDaRaiz . "/" . $nomeDoArquivoZip;

print "{$tamanhoEmBytes};{$timestamp};{$urlAcesso}";

