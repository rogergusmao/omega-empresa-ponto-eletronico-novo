<?php


        /*

        Arquivo gerado através de gerador de código em 12/05/2018 as 14:47:08.
        Para que o arquivo não seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

        Tabela correspondente: empresa_venda_parcela
        Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

        */
        


class DAO_Empresa_venda_parcela extends Generic_DAO
                        {
                            const NOME_ENTIDADE = "empresa_venda_parcela";

                            public static $nomeTabela = null;
                            public static $idSistemaTabela = false;
                            public static $databaseFieldsRelatedAttributes = null;
	                        public static $databaseFieldTypes = null;
	                        public static $databaseFieldNames = null;
	                        public static $listAliasTypes = null;
	                        public static $listAliasRelatedAttributes = null;
	                        
	                        public $id;
public $empresaVendaId;
public $dataSec;
public $dataOffsec;
public $valorFloat;
public $pagamentoSec;
public $pagamentoOffsec;
public $seqInt;
public $corporacaoId;

public $labelId;
public $labelEmpresaVendaId;
public $labelDataSec;
public $labelDataOffsec;
public $labelValorFloat;
public $labelPagamentoSec;
public $labelPagamentoOffsec;
public $labelSeqInt;
public $labelCorporacaoId;

public static $arrPK = array("id");

public function __construct($configDAO = null)
        {
            parent::__construct($configDAO);
            static::__setStaticValues($this->database);           
        }

public static function idSistemaTabela($databaseObject = null){
            if(!(static::$idSistemaTabela))
            {
                static::$idSistemaTabela = EXTDAO_Sistema_tabela::getIdDaSistemaTabela(static::nomeTabela(), $databaseObject);
            }
            return static::$idSistemaTabela;
        }

public function getIdSistemaTabela(){
            return static::idSistemaTabela();
        }

public static function nomeTabela(){
            if(is_null(static::$nomeTabela))
            {
                static::$nomeTabela = "empresa_venda_parcela";
            }
            return static::$nomeTabela;
        }

public function getNomeTabela(){
            return static::nomeTabela();
        }

public static function __setStaticValues($databaseObject = null)
                   {    
                        static::nomeTabela();
                        
                        static::idSistemaTabela($databaseObject);
                   
                        static::setDatabaseFieldNames();
                        static::setDatabaseFieldTypes();
                        static::setDatabaseFieldsRelatedAttributeNames();
                        
                        static::setListAliasTypes();
                        static::setListAliasRelatedAttributes();
                   }

public function getExpressaoLabel()
                   {
                        return "{id}";
                   }

public static function setDatabaseFieldNames()
        {
            if(is_null(static::$databaseFieldNames))
            {
                static::$databaseFieldNames = new stdClass();
                static::$databaseFieldNames->id = "id";
static::$databaseFieldNames->empresaVendaId = "empresa_venda_id_INT";
static::$databaseFieldNames->dataSec = "data_SEC";
static::$databaseFieldNames->dataOffsec = "data_OFFSEC";
static::$databaseFieldNames->valorFloat = "valor_FLOAT";
static::$databaseFieldNames->pagamentoSec = "pagamento_SEC";
static::$databaseFieldNames->pagamentoOffsec = "pagamento_OFFSEC";
static::$databaseFieldNames->seqInt = "seq_INT";
static::$databaseFieldNames->corporacaoId = "corporacao_id_INT";

            }
                      
        }

public static function setDatabaseFieldTypes()
        {
            if(is_null(static::$databaseFieldTypes))
            {
                static::$databaseFieldTypes = new stdClass();
                static::$databaseFieldTypes->id = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->empresaVendaId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->dataSec = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$databaseFieldTypes->dataOffsec = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$databaseFieldTypes->valorFloat = static::TIPO_VARIAVEL_FLOAT;
static::$databaseFieldTypes->pagamentoSec = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$databaseFieldTypes->pagamentoOffsec = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$databaseFieldTypes->seqInt = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->corporacaoId = static::TIPO_VARIAVEL_INTEGER;

            }
            
        }

public static function setDatabaseFieldsRelatedAttributeNames()
        {
            if(is_null(static::$databaseFieldsRelatedAttributes))
            {
                static::$databaseFieldsRelatedAttributes = new stdClass();
                static::$databaseFieldsRelatedAttributes->id = "id";
static::$databaseFieldsRelatedAttributes->empresa_venda_id_INT = "empresaVendaId";
static::$databaseFieldsRelatedAttributes->data_SEC = "dataSec";
static::$databaseFieldsRelatedAttributes->data_OFFSEC = "dataOffsec";
static::$databaseFieldsRelatedAttributes->valor_FLOAT = "valorFloat";
static::$databaseFieldsRelatedAttributes->pagamento_SEC = "pagamentoSec";
static::$databaseFieldsRelatedAttributes->pagamento_OFFSEC = "pagamentoOffsec";
static::$databaseFieldsRelatedAttributes->seq_INT = "seqInt";
static::$databaseFieldsRelatedAttributes->corporacao_id_INT = "corporacaoId";

            }           
        }

public function setAllFieldsToHumanFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["empresa_venda_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["data_SEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["data_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["valor_FLOAT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["pagamento_SEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["pagamento_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["seq_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
}

public function setAllFieldsToDatabaseFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["empresa_venda_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["data_SEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["data_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["valor_FLOAT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["pagamento_SEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["pagamento_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["seq_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
}

  public function getFkObjEmpresa_venda() 
                {
                    if($this->objEmpresa_venda == null)
                    {                        
                        $this->objEmpresa_venda = new EXTDAO_Empresa_venda_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getEmpresa_venda_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objEmpresa_venda->clear();
                    }
                    elseif($this->objEmpresa_venda->getId() != $idFK)
                    {
                        $this->objEmpresa_venda->select($idFK);
                    }
                    return $this->objEmpresa_venda;
                }
  public function getFkObjCorporacao() 
                {
                    if($this->objCorporacao == null)
                    {                        
                        $this->objCorporacao = new EXTDAO_Corporacao_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getCorporacao_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objCorporacao->clear();
                    }
                    elseif($this->objCorporacao->getId() != $idFK)
                    {
                        $this->objCorporacao->select($idFK);
                    }
                    return $this->objCorporacao;
                }


public function setLabels()
        {$this->labelId = "id";
$this->labelEmpresaVendaId = "empresavendaidINT";
$this->labelDataSec = "";
$this->labelDataOffsec = "";
$this->labelValorFloat = "valorFLOAT";
$this->labelPagamentoSec = "";
$this->labelPagamentoOffsec = "";
$this->labelSeqInt = "";
$this->labelCorporacaoId = "corporacaoidINT";
}

public function __actionAdd($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                $this->setByObject($parameters);
                $this->setDataCadastroAndOffsetInSecondsFromUser($parameters);
                
                $this->formatarParaSQL();
                $msg = $this->insert(true);
                
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                return new Mensagem(null, I18N::getExpression("Empresa venda parcela adicionado com sucesso."));
                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionEdit($parameters = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try {

                $this->setByObject($parameters);
                $this->setAllFieldsToHumanFormat();
                
                $this->formatarParaSQL();
                $msg = $this->updateByInstanceUsingParameters($this->id, $parameters, true);
                if($msg != null && $msg->erro())
                {
                     return $msg;
                }
        
                return new Mensagem(null, I18N::getExpression("Empresa venda parcela editado com sucesso."));
            }
            catch(Exception $ex)
            {                
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionRemove($parameters = null)
        {
            $successulRemovalCount = 0;
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                foreach($parameters as $record)
                {
                    if(is_numeric($record->id))
                    {
                        $msg = $this->delete($record->id, true);
                        if($msg == null || $msg->ok())
                        {
                            $successulRemovalCount++;
                        }
                    }
                }
                
                if($successulRemovalCount > 0)
                {
                    $successMessage = count($successulRemovalCount) > 1 ? I18N::getExpression("{0} Empresa venda parcela foram removidos com sucesso.", $successulRemovalCount) : I18N::getExpression("Empresa venda parcela removido com sucesso.");
                    return new Mensagem(null, $successMessage);
                }
                else
                {
                    $errorMessage = count($parameters) > 1 ? I18N::getExpression("Falha ao remover Empresa venda parcela.") : I18N::getExpression("Falha ao remover Empresa venda parcela.");
                    return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, $errorMessage);                    
                }
            }
            catch (Exception $ex) 
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }

public function __getRecord($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }
            
            try
            {            
                $returnObject = new stdClass();            
                $whereClause = static::getWhereClauseForFilter($parameters->filterParameters);
                $query = "SELECT  id, empresa_venda_id_INT, data_SEC, data_OFFSEC, valor_FLOAT, pagamento_SEC, pagamento_OFFSEC, seq_INT, corporacao_id_INT FROM empresa_venda_parcela {$whereClause}";
    
                $msg = $this->database->queryMensagem($query);                
                if ($msg == null)
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Não foi encontrado nenhum registro"));
                    }                    
                    $resultSet = $this->database->result;
                    $resultObject = static::mapResultSetDataToRecord($resultSet);
                    
                    $returnObject->formData = $resultObject;
                    $returnObject->comboBoxesData = null;
                    if($parameters->loadComboBoxesData)
                    {
                        $this->setByObject($resultObject);
                        $returnObject->comboBoxesData =  $this->getFormComboBoxesData();
                    }                    
    
                    
    
                    return new Mensagem_generica($returnObject);
                } 
                else 
                {
                    return $msg;
                }
                
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }


        
        public function __getFilterComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
                
        public function __getFormComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
        
        protected function getFormComboBoxesData()
        {
            $comboBoxesData = new stdClass();
            $listParameters = new stdClass();
            $listParameters->listMappingType = static::LIST_MAPPING_TYPE_COMBOBOX;
            
            
                    
                    $objEmpresaVenda = new EXTDAO_Empresa_venda();
                    $comboBoxesData->fieldEmpresaVendaId = $objEmpresaVenda->__getList($listParameters);

            return $comboBoxesData;
        }
        
        


        
        public static function setListAliasTypes()
        {
            if(is_null(static::$listAliasTypes))
            {
                static::$listAliasTypes = new stdClass();
                static::$listAliasTypes->empresa_venda__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->empresa_venda_parcela__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->empresa_venda_parcela__corporacao_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->empresa_venda_parcela__data_SEC = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$listAliasTypes->empresa_venda_parcela__data_OFFSEC = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$listAliasTypes->empresa_venda_parcela__valor_FLOAT = static::TIPO_VARIAVEL_FLOAT;
            }        
        }
        
        public static function setListAliasRelatedAttributes()
        {
            if(is_null(static::$listAliasRelatedAttributes))
            {
                static::$listAliasRelatedAttributes = new stdClass();
                static::$listAliasRelatedAttributes->empresa_venda__id = "empresaVendaId";
static::$listAliasRelatedAttributes->empresa_venda_parcela__id = "id";
static::$listAliasRelatedAttributes->empresa_venda_parcela__corporacao_id_INT = "corporacaoId";
static::$listAliasRelatedAttributes->empresa_venda_parcela__data_SEC = "dataSec";
static::$listAliasRelatedAttributes->empresa_venda_parcela__data_OFFSEC = "dataOffsec";
static::$listAliasRelatedAttributes->empresa_venda_parcela__valor_FLOAT = "valorFloat";
            }         
        }

        public function __getList($parameters = null, $idCorporacao = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try
            {
                $filterParameters = new stdClass();
                if(isset($parameters->filterParameters))
                {
                    $filterParameters = $parameters->filterParameters;
                }
                                    
                            
            if(is_null($idCorporacao))
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            ListParameters::addFilterParameterToObject($filterParameters, "corporacaoId", $idCorporacao);           
                
                
                $sortingParameters = null;
                if(isset($parameters->sortingParameters))
                {
                    $sortingParameters = $parameters->sortingParameters;
                }
                
                $paginationParameters = null;
                if(isset($parameters->paginationParameters))
                {
                    $paginationParameters = $parameters->paginationParameters;
                }
                
                $listMappingType = static::getListMappingType($parameters->listMappingType);
                $mainTableAlias = "evp";

                $whereClause = static::getWhereClauseForFilter($filterParameters, $mainTableAlias);
                $orderByClause = static::getOrderByClauseForFilter($sortingParameters, $mainTableAlias);
                $limitClause = static::getLimitClauseForFilter($paginationParameters);

                $queryWithoutLimit = "SELECT evp.id FROM empresa_venda_parcela evp {$whereClause}";
                $query = "SELECT ev.id AS empresa_venda__id, evp.id AS empresa_venda_parcela__id, evp.corporacao_id_INT AS empresa_venda_parcela__corporacao_id_INT, evp.data_SEC AS empresa_venda_parcela__data_SEC, evp.data_OFFSEC AS empresa_venda_parcela__data_OFFSEC, evp.valor_FLOAT AS empresa_venda_parcela__valor_FLOAT FROM empresa_venda_parcela evp LEFT JOIN empresa_venda ev ON ev.id = evp.empresa_venda_id_INT {$whereClause} {$orderByClause} {$limitClause}";

                $msg = $this->database->queryMensagem($queryWithoutLimit);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                
                $totalNumberOfRecords = $this->database->rows();
                if($totalNumberOfRecords == 0)
                {
                    return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                }                        
                        
                $msg = $this->database->queryMensagem($query);
                
                //operacao realizaca com sucesso
                if ($msg == null) 
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                    }                        
                
                    $resultSet = $this->database->result;

                    $objReturn = new stdClass();
                    $objReturn->dataSet = static::mapResultSetDataToList($resultSet, $listMappingType, static::$listAliasTypes);

                    if(!Helper::isNullOrEmpty($paginationParameters->recordsPerPage) && !Helper::isNullOrEmpty($paginationParameters->currentPage))
                    {
                        $lastPage = ceil($totalNumberOfRecords / $paginationParameters->recordsPerPage);
                        $objReturn->paginationParameters = new PaginationParams($paginationParameters->recordsPerPage, $paginationParameters->currentPage, $lastPage);
                    }
                    
                    return new Mensagem_generica($objReturn);
                } 
                else
                {
                    return $msg;
                }                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }



public function getId()
            {
                return $this->id;
            }

public function getEmpresaVendaId()
            {
                return $this->empresaVendaId;
            }

public function getEmpresa_venda_id_INT()
                {
                    return $this->empresaVendaId;
                }

public function getDataSec()
            {
                return $this->dataSec;
            }

public function getData_SEC()
                {
                    return $this->dataSec;
                }

public function getDataOffsec()
            {
                return $this->dataOffsec;
            }

public function getData_OFFSEC()
                {
                    return $this->dataOffsec;
                }

public function getValorFloat()
            {
                return $this->valorFloat;
            }

public function getValor_FLOAT()
                {
                    return $this->valorFloat;
                }

public function getPagamentoSec()
            {
                return $this->pagamentoSec;
            }

public function getPagamento_SEC()
                {
                    return $this->pagamentoSec;
                }

public function getPagamentoOffsec()
            {
                return $this->pagamentoOffsec;
            }

public function getPagamento_OFFSEC()
                {
                    return $this->pagamentoOffsec;
                }

public function getSeqInt()
            {
                return $this->seqInt;
            }

public function getSeq_INT()
                {
                    return $this->seqInt;
                }

public function getCorporacaoId()
            {
                return $this->corporacaoId;
            }

public function getCorporacao_id_INT()
                {
                    return $this->corporacaoId;
                }



function setId($value)
            {
                $this->id = $value;
            }

function setEmpresaVendaId($value)
            {
                $this->empresaVendaId = $value;
            }

public function setEmpresa_venda_id_INT($value)
                { 
                    $this->empresaVendaId = $value; 
                }

function setDataSec($value)
            {
                $this->dataSec = $value;
            }

public function setData_SEC($value)
                { 
                    $this->dataSec = $value; 
                }

function setDataOffsec($value)
            {
                $this->dataOffsec = $value;
            }

public function setData_OFFSEC($value)
                { 
                    $this->dataOffsec = $value; 
                }

function setValorFloat($value)
            {
                $this->valorFloat = $value;
            }

public function setValor_FLOAT($value)
                { 
                    $this->valorFloat = $value; 
                }

function setPagamentoSec($value)
            {
                $this->pagamentoSec = $value;
            }

public function setPagamento_SEC($value)
                { 
                    $this->pagamentoSec = $value; 
                }

function setPagamentoOffsec($value)
            {
                $this->pagamentoOffsec = $value;
            }

public function setPagamento_OFFSEC($value)
                { 
                    $this->pagamentoOffsec = $value; 
                }

function setSeqInt($value)
            {
                $this->seqInt = $value;
            }

public function setSeq_INT($value)
                { 
                    $this->seqInt = $value; 
                }

function setCorporacaoId($value)
            {
                $this->corporacaoId = $value;
            }

public function setCorporacao_id_INT($value)
                { 
                    $this->corporacaoId = $value; 
                }



public function clear()
        {$this->id = null;
$this->empresaVendaId = null;
if($this->objEmpresa_venda != null) unset($this->objEmpresa_venda);
$this->dataSec = null;
$this->dataOffsec = null;
$this->valorFloat = null;
$this->pagamentoSec = null;
$this->pagamentoOffsec = null;
$this->seqInt = null;
$this->corporacaoId = null;
if($this->objCorporacao != null) unset($this->objCorporacao);
if(is_array($this->arrEstadosCampos)){
                        unset($this->arrEstadosCampos);
                     $this->arrEstadosCampos = array();
                    }
                }

public function formatarParaSQL()
        {
if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->id)){
$this->id = $this->formatarIntegerParaComandoSQL($this->id);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->id);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->empresaVendaId)){
$this->empresaVendaId = $this->formatarIntegerParaComandoSQL($this->empresaVendaId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->empresaVendaId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->dataSec)){
$this->dataSec = $this->formatarIntegerParaComandoSQL($this->dataSec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->dataSec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->dataOffsec)){
$this->dataOffsec = $this->formatarIntegerParaComandoSQL($this->dataOffsec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->dataOffsec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->valorFloat)){
$this->valorFloat = $this->formatarFloatParaComandoSQL($this->valorFloat);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->valorFloat);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->pagamentoSec)){
$this->pagamentoSec = $this->formatarIntegerParaComandoSQL($this->pagamentoSec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->pagamentoSec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->pagamentoOffsec)){
$this->pagamentoOffsec = $this->formatarIntegerParaComandoSQL($this->pagamentoOffsec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->pagamentoOffsec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->seqInt)){
$this->seqInt = $this->formatarIntegerParaComandoSQL($this->seqInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->seqInt);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->corporacaoId)){
$this->corporacaoId = $this->formatarIntegerParaComandoSQL($this->corporacaoId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->corporacaoId);
}

}

public function formatarParaExibicao()
        {if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->valorFloat)){
$this->valorFloat = $this->formatarFloatParaExibicao($this->valorFloat);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->valorFloat);
}

}

function select($id, $idCorporacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }   

            $sql = "SELECT id, empresa_venda_id_INT, data_SEC, data_OFFSEC, valor_FLOAT, pagamento_SEC, pagamento_OFFSEC, seq_INT, corporacao_id_INT FROM empresa_venda_parcela WHERE id = $id AND corporacao_id_INT = $idCorporacao";

            $msg = $this->database->queryMensagem($sql);
            $result = $this->database->result;

            if($msg != null && $msg->erro() )
            { 
            		$this->clear();
            		return $msg;
        	} 
        	else if($this->database->rows() == 0)
        	{
                return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nenhum registro foi encontrado."));
            }
        

            $row = $this->database->fetchArray(MYSQLI_NUM);
            
            		$this->id = $row[0];
		$this->empresaVendaId = $row[1];
		$this->dataSec = $row[2];
		$this->dataOffsec = $row[3];
		$this->valorFloat = $row[4];
		$this->pagamentoSec = $row[5];
		$this->pagamentoOffsec = $row[6];
		$this->seqInt = $row[7];

            
        }

public function delete($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (is_null($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }

            $sql = "DELETE FROM empresa_venda_parcela WHERE id= $id AND corporacao_id_INT = $idCorporacao";
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                    && $sincronizar
                        && static::idSistemaTabela() != null
                            && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_REMOVE,
                                null,
                                null,
                                $id,
                                $this->database);
            }

            return $msg;
        }

public function insert($sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null, $opcoes = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            
            
            if (is_null($opcoes))
            {
                $opcoes = $this->opcoesDAO;
            }

            if (!is_numeric($this->id))
            {
                $this->id = "null";
                if ($opcoes == null || $opcoes[Database::OPCAO_GERAR_ID] != false) 
                {
                    $this->id = EXTDAO_Sistema_sequencia::gerarId(static::nomeTabela());                    
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }    
                    else if($msg == null)
                    {
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
                else 
                {
                    $this->id = null;
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao));
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }
                    else if($msg == null)
                    {
                        $this->id = $this->database->getLastInsertId();
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
            }
            else 
            {
                $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }                        
                else if($msg == null)
                {
                   $msg = new Mensagem_token();
                   $msg->mValor = $this->id;
                }
            }
            if($msg == null || $msg->ok())
            {                
                if($sincronizar
                    && static::idSistemaTabela() != null
                    && isset($this->id)){
                    
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_INSERT,
                                null,
                                null,
                                $this->id,
                                $this->database); 
                	}
            } 
            else
            {
                $this->id = null;
            }
            
            return $msg;
            
        }

public function getSQLInsert($idCorporacao){ 

return "INSERT INTO empresa_venda_parcela (id, empresa_venda_id_INT, data_SEC, data_OFFSEC, valor_FLOAT, pagamento_SEC, pagamento_OFFSEC, seq_INT, corporacao_id_INT) VALUES ( $this->id ,  $this->empresaVendaId ,  $this->dataSec ,  $this->dataOffsec ,  $this->valorFloat ,  $this->pagamentoSec ,  $this->pagamentoOffsec ,  $this->seqInt ,  $idCorporacao ) "; 

}




        
        public function updateByParameters($id, $objParametros, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao))
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();

            
                
                if (isset($objParametros->empresaVendaId)) 
                {
                    $arrUpdateFields[] = " empresa_venda_id_INT = {$objParametros->empresaVendaId} ";
                }


                
                if (isset($objParametros->dataSec)) 
                {
                    $arrUpdateFields[] = " data_SEC = {$objParametros->dataSec} ";
                }


                
                if (isset($objParametros->dataOffsec)) 
                {
                    $arrUpdateFields[] = " data_OFFSEC = {$objParametros->dataOffsec} ";
                }


                
                if (isset($objParametros->valorFloat)) 
                {
                    $arrUpdateFields[] = " valor_FLOAT = {$objParametros->valorFloat} ";
                }


                
                if (isset($objParametros->pagamentoSec)) 
                {
                    $arrUpdateFields[] = " pagamento_SEC = {$objParametros->pagamentoSec} ";
                }


                
                if (isset($objParametros->pagamentoOffsec)) 
                {
                    $arrUpdateFields[] = " pagamento_OFFSEC = {$objParametros->pagamentoOffsec} ";
                }


                
                if (isset($objParametros->seqInt)) 
                {
                    $arrUpdateFields[] = " seq_INT = {$objParametros->seqInt} ";
                }



            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE empresa_venda_parcela SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";

            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null)
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function update($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($this->empresaVendaId)) 
                {                                      
                    $arrUpdateFields[] = " empresa_venda_id_INT = {$this->empresaVendaId} ";
                }


                
                if (isset($this->dataSec)) 
                {                                      
                    $arrUpdateFields[] = " data_SEC = {$this->dataSec} ";
                }


                
                if (isset($this->dataOffsec)) 
                {                                      
                    $arrUpdateFields[] = " data_OFFSEC = {$this->dataOffsec} ";
                }


                
                if (isset($this->valorFloat)) 
                {                                      
                    $arrUpdateFields[] = " valor_FLOAT = {$this->valorFloat} ";
                }


                
                if (isset($this->pagamentoSec)) 
                {                                      
                    $arrUpdateFields[] = " pagamento_SEC = {$this->pagamentoSec} ";
                }


                
                if (isset($this->pagamentoOffsec)) 
                {                                      
                    $arrUpdateFields[] = " pagamento_OFFSEC = {$this->pagamentoOffsec} ";
                }


                
                if (isset($this->seqInt)) 
                {                                      
                    $arrUpdateFields[] = " seq_INT = {$this->seqInt} ";
                }


            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE empresa_venda_parcela SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function updateByInstanceUsingParameters($id, $parameters, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($parameters->empresaVendaId)) 
                {                                      
                    $arrUpdateFields[] = " empresa_venda_id_INT = {$this->empresaVendaId} ";
                }
                
                if (isset($parameters->dataSec)) 
                {                                      
                    $arrUpdateFields[] = " data_SEC = {$this->dataSec} ";
                }
                
                if (isset($parameters->dataOffsec)) 
                {                                      
                    $arrUpdateFields[] = " data_OFFSEC = {$this->dataOffsec} ";
                }
                
                if (isset($parameters->valorFloat)) 
                {                                      
                    $arrUpdateFields[] = " valor_FLOAT = {$this->valorFloat} ";
                }
                
                if (isset($parameters->pagamentoSec)) 
                {                                      
                    $arrUpdateFields[] = " pagamento_SEC = {$this->pagamentoSec} ";
                }
                
                if (isset($parameters->pagamentoOffsec)) 
                {                                      
                    $arrUpdateFields[] = " pagamento_OFFSEC = {$this->pagamentoOffsec} ";
                }
                
                if (isset($parameters->seqInt)) 
                {                                      
                    $arrUpdateFields[] = " seq_INT = {$this->seqInt} ";
                }
            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE empresa_venda_parcela SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }

}

?>
