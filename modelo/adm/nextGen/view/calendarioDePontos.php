<?php=NextGenLoader::importarBibliotecasCalendarioDePontos(); ?>

<!-- container de componentes ReactJS -->
<div id="content-wrapper" class="mt-2">
    <div class="col-md-9">
        <div class="col-sm-12 box box-nomargin box-no-padding">
            <div class="box-header blue-background">
                <div class="title">Pontos Eletrônicos</div>
                <div class="actions" id="botao-exportar-dados-container">

                </div>
            </div>
            <div class="box-content" id="calendario-container" style="overflow-y: scroll;">

            </div>
        </div>
    </div>

    <div class="col-md-3">

        <div class="col-sm-12 box box-nomargin box-no-padding">
            <div class="box-header">
                <div class="title">Total de Horas Trabalhadas</div>
                <div class="actions">
                    <a class="btn box-collapse btn-xs btn-link" href="#"><i></i></a>
                </div>
            </div>
            <div class="box-content" id="painel-totalizador-horas-container">

            </div>
        </div>

        <div class="col-sm-12 box box-nomargin box-no-padding">
            <div class="box-header">
                <div class="title">Problemas Encontrados</div>
                <div class="actions">
                    <a class="btn box-collapse btn-xs btn-link" href="#"><i></i></a>
                </div>
            </div>
            <div class="box-content" id="painel-problemas-encontrador-container">

            </div>
        </div>

        <div class="col-sm-12 box box-nomargin box-no-padding">
            <div class="box-header">
                <div class="title">Distribuição de Horas</div>
                <div class="actions">
                    <a class="btn box-collapse btn-xs btn-link" href="#"><i></i></a>
                </div>
            </div>
            <div class="box-content" id="painel-distribuicao-horas-container">

            </div>
        </div>

    </div>

</div>

<!-- container para rendizar modals ReactJS -->
<div id="modal-container"></div>


