<?php

/*

Arquivo gerado através de gerador de código em 02/03/2017 as 19:48:07.
Para que o arquivo não seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

Tabela correspondente: tipo_empresa
Sobrescrita de dados: ---SOBRESCRITA_NAO_AUTORIZADA---

*/
?>

<?

class EXTDAO_Tipo_empresa extends DAO_Tipo_empresa
{
    public function __construct($configDAO = null)
    {
        parent::__construct($configDAO);
        $this->nomeClasse = "EXTDAO_TipoEmpresa";
    }

    public function factory()
    {
        return new EXTDAO_Tipo_empresa();
    }
}

?>
