

        /*

        Arquivo gerado atrav�s de gerador de c�digo em 07/09/2017 as 01:26:41.
        Para que o arquivo n�o seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

        Tabela correspondente: usuario_servico
        Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

        */

returnObject.getUsuarioServico = function(parameters){

            return $http({
                url: baseURLAdm + "/webservice.php?class=EXTDAO_Usuario_servico&action=__getRecord",
                method: "POST",
                data: parameters,
                headers: {
                    'Content-Type': 'application/json'
                }
            });

        };



        returnObject.addUsuarioServico = function(formData){

            return $http({
                url: baseURLAdm + "/webservice.php?class=EXTDAO_Usuario_servico&jsonOutput=true&action=add",
                method: "POST",
                data: formData,
                headers: {
                    'Content-Type':'application/x-www-form-urlencoded'
                }
            });

        };

        returnObject.editUsuarioServico = function(formData){

            return $http({
                url: baseURLAdm + "/webservice.php?class=EXTDAO_Usuario_servico&jsonOutput=true&action=edit",
                method: "POST",
                data: formData,
                headers: {
                    'Content-Type':'application/x-www-form-urlencoded'
                }
            });

        };

returnObject.getListOfServico = function()
                {
                    var params = { filterParameters: null, isRelatedEntity: true };
                    return $http.post(baseURLAdm + "/webservice.php?class=EXTDAO_Servico&action=__getList", params);
                };
                returnObject.getListOfUsuario = function()
                {
                    var params = { filterParameters: null, isRelatedEntity: true };
                    return $http.post(baseURLAdm + "/webservice.php?class=EXTDAO_Usuario&action=__getList", params);
                };
                

