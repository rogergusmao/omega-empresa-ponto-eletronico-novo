<?php

include '../../recursos/languages/pt-br.php';
include '../../recursos/php/funcoes.php';
include '../../recursos/php/constants.php';
include '../../recursos/php/database_config.php';

$nomeScript = Helper::getNomeDoScriptAtual();
$idMensagemSMS = Helper::GET("mensagem_sms_id_INT");

$vIsPrimeiraVez = Helper::GET("is_primeira_vez_BOOLEAN");

if ($nomeScript == "index.php" && !isset($vIsPrimeiraVez))
{
    echo "<center>";
    Helper::imprimirMensagem("A janela de download abrirá em alguns segundos.\nCaso não abra automaticamente, <a class='link_padrao' href='pages/exportar_destinatario_sms.php?is_primeira_vez_BOOLEAN=0&mensagem_sms_id_INT=$idMensagemSMS'>clique aqui</a>.");
    echo "</center>";

    Helper::mudarLocation("pages/exportar_destinatario_sms.php?is_primeira_vez_BOOLEAN=0&mensagem_sms_id_INT=$idMensagemSMS");
    exit();
}

$objBanco = new Database();
$objBanco->query("SELECT DISTINCT CONCAT(p.primeiro_nome,' ', p.sobrenomes) nome, p.celular, msp.foi_enviado_BOOLEAN, msp.data_envio_DATETIME, msp.numero_tentativa_INT
    FROM mensagem_sms ms LEFT JOIN mensagem_sms_pessoa msp ON ms.id = msp.mensagem_sms_id_INT AND ms.id=$idMensagemSMS
        JOIN pessoa p ON p.id = msp.pessoa_id_INT ");

$stringRetorno = "";

if ($objBanco->rows > 0)
{

    $stringRetorno .= "Nome;Celular;Enviado\r\n";

    while ($dados = $objBanco->fetchArray())
    {
        $foiEnviado = $dados[2];
        $vStrEnviado = "";

        if ($foiEnviado)
        {
            $dataDoEnvio = $dados[3];
            $dataDoEnvio = Helper::formatarDataTimeParaExibicao($dataDoEnvio);
            $vStrEnviado = "Sim ({$dataDoEnvio})";
        }
        else
        {
            $vNumeroTentativa = $dados[4];
            if ($vNumeroTentativa >= LIMITE_TENTATICA_ENVIO_SMS)
            {
                $vStrEnviado = "Erro";
            }
            else
            {
                $vStrEnviado = "Nao";
            }
        }

        $stringRetorno .= Helper::getStrLinhaCSVDoVetorDeDados(array($dados[0], $dados[1], $vStrEnviado));
    }
//    echo $stringRetorno;
}

$objDownload = new Download("destinatario_sms.csv");
print $objDownload->ds_download($stringRetorno);
?>
