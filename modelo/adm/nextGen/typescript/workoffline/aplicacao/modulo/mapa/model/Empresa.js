"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var model_1 = require("lib/model");
var geral_1 = require("lib/geral");
var Empresa = (function (_super) {
    __extends(Empresa, _super);
    function Empresa() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Empresa.parseFromRemoteObject = function (remoteObject) {
        var instance = new Empresa();
        instance.latitude = geral_1.GeoLocationHelper.getFloatValue(remoteObject.latitude);
        instance.longitude = geral_1.GeoLocationHelper.getFloatValue(remoteObject.longitude);
        instance.idEmpresa = geral_1.Helper.parseInt(remoteObject.idEmpresa);
        instance.nomeEmpresa = remoteObject.nomeEmpresa;
        instance.wifiNetworks = new Array();
        return instance;
    };
    Empresa.prototype.setValidWifiNetworks = function (wifiNetworks) {
        if (wifiNetworks != null) {
            for (var _i = 0, wifiNetworks_1 = wifiNetworks; _i < wifiNetworks_1.length; _i++) {
                var wifiNetwork = wifiNetworks_1[_i];
                if (this.idEmpresa == wifiNetwork.idEmpresa) {
                    this.wifiNetworks.push(wifiNetwork);
                }
            }
        }
    };
    return Empresa;
}(model_1.Model));
exports.Empresa = Empresa;
