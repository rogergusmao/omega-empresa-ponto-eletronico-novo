<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Mensagem
    * NOME DA CLASSE DAO: 
    * DATA DE GERAÃ‡ÃƒO:    
    * ARQUIVO:            EXTDAO_Mensagem.php
    * TABELA MYSQL:       mensagem
    * BANCO DE DADOS:     
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÃ‡ÃƒO DA CLASSE
    // **********************

    class EXTDAO_Mensagem extends DAO_Mensagem
    {

        public function __construct($configDAO){

            parent::__construct($configDAO);

            	$this->nomeClasse = "EXTDAO_Mensagem";


            $this->setLabels();






        }

        public function setLabels(){

			$this->label_id = "";
			$this->label_titulo = "";
			$this->label_mensagem = "";
			$this->label_remetente_usuario_id_INT = "";
			$this->label_tipo_mensagem_id_INT = "";
			$this->label_destinatario_usuario_id_INT = "";
			$this->label_destinatario_pessoa_id_INT = "";
			$this->label_destinatario_empresa_id_INT = "";
			$this->label_data_min_envio_SEC = "";
			$this->label_data_min_envio_OFFSEC = "";
			$this->label_protocolo_INT = "";
			$this->label_registro_estado_id_INT = "";
			$this->label_empresa_para_cliente_BOOLEAN = "";
			$this->label_corporacao_id_INT = "";


        }





        public function factory(){

            return new EXTDAO_Mensagem();

        }

	}

    
