<?php

/*

Arquivo gerado através de gerador de código em 24/05/2017 as 07:53:29.
Para que o arquivo não seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

Tabela correspondente: usuario_servico
Sobrescrita de dados: ---SOBRESCRITA_NAO_AUTORIZADA---

*/

?>

<?php

class EXTDAO_Usuario_servico extends DAO_Usuario_servico
{
    public function __construct($configDAO = null)
    {
        parent::__construct($configDAO);
        $this->nomeClasse = "EXTDAO_Usuario_servico";
    }

    public function factory()
    {
        return new EXTDAO_Usuario_servico();
    }

    public static function getIdUsuarioServico($idServico, $idUsuario, $idCorporacao, $db = null)
    {


        $q = "SELECT id "
            . " FROM usuario_servico "
            . " WHERE servico_id_INT = $idServico"
            . " AND usuario_id_INT = $idUsuario "
            . " AND corporacao_id_INT = $idCorporacao ";
        if ($db == null)
        {
            $db = new Database();
        }
        $db->query($q);

        return $db->getPrimeiraTuplaDoResultSet(0);
    }

    public static function cadastraServicosDoUsuario(
        $idUsuarioAdm, $idCorporacao, $idUsuario,
        $imei, $idPrograma, $db = null, $objSincronizador = null,
        &$operacoes = null)
    {
        if ($db == null)
        {
            $db = new Database();
        }
        $idUsuarioServicoSistemaTabela = EXTDAO_Sistema_tabela::getIdDaSistemaTabela("usuario_servico", $db);
        if (strlen($idUsuario) &&
            strlen($idUsuarioServicoSistemaTabela) &&
            strlen($idCorporacao) &&
            strlen($idUsuarioAdm)
        )
        {

            $vetorIdServico = EXTDAO_Servico::getServicos($db);
            if (count($vetorIdServico) > 0)
            {
                if ($objSincronizador == null)
                {
                    $objSincronizador = new EXTDAO_Sistema_registro_sincronizador();
                }
                $objUsuarioServico = new EXTDAO_Usuario_servico($db);
                $idsUS = array();
                foreach ($vetorIdServico as $key => $idServico)
                {
                    if (!strlen($idServico))
                    {
                        continue;
                    }
                    else
                    {
                        $idUsuarioServico = EXTDAO_Usuario_servico::getIdUsuarioServico($idServico, $idUsuario, $idCorporacao, $db);
                        if (!strlen($idUsuarioServico))
                        {
                            $objUsuarioServico->clear();
                            $objUsuarioServico->setServico_id_INT($idServico);
                            $objUsuarioServico->setUsuario_id_INT($idUsuario);
                            $objUsuarioServico->setStatus_BOOLEAN("1");
                            $objUsuarioServico->setCorporacao_id_INT($idCorporacao);
                            $objUsuarioServico->formatarParaSQL();
                            $msg = $objUsuarioServico->insert(false, $idCorporacao);
                            if (!$msg->ok())
                            {
                                return $msg;
                            }
                            $idUsuarioServico = $msg->mValor;
                            $idsUS[count($idsUS)] = $idUsuarioServico;
                            $objSincronizador->inserirRegistro(
                                $idUsuarioServicoSistemaTabela, $idUsuarioAdm,
                                $idCorporacao, '0', DatabaseSincronizador::$INDEX_OPERACAO_EDIT,
                                $imei, $idPrograma, $idUsuarioServico);
                        }
                    }
                    if ($operacoes != null)
                    {
                        $operacoes["usuario_servico"] = $idsUS;
                    }
                }

                return new Mensagem(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO);
            }
            else
            {
                return new Mensagem(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO);
            }
        }
        else
        {
            return new Mensagem(
                PROTOCOLO_SISTEMA::ERRO_PARAMETRO_INVALIDO,
                "Parametro invalido cadastraServicosDoUsuario. "
                . $idUsuario . ","
                . $idUsuarioServicoSistemaTabela . ","
                . $idCorporacao . ","
                . $idUsuarioAdm . ". Get Identificador: " . $db->getIdentificador());
        }
    }
}

?>
