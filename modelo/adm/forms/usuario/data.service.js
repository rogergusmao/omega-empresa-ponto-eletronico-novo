/*

 Arquivo gerado através de gerador de código em 04/06/2017 as 23:37:04.
 Para que o arquivo não seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

 Tabela correspondente: usuario
 Sobrescrita de dados: ---SOBRESCRITA_NAO_AUTORIZADA---

 */

//recuperação de dados para edição
returnObject.getDadosCompletosDoUsuario = function (idUsuario)
{
    return $http({
        url: baseURLAdm + "/webservice.php?class=EXTDAO_Usuario&action=getDadosCompletosDoUsuario",
        method: "POST",
        data: {idUsuario: idUsuario},
        headers: {
            'Content-Type': 'application/json'
        }
    });
};

returnObject.getDadosCompletosDoUsuarioAndEstruturasAdicionais = function (filterParameters)
{
    return $http({
        url: baseURLAdm + "/webservice.php?class=EXTDAO_Usuario&action=getDadosCompletosDoUsuarioAndEstruturasAdicionais",
        method: "POST",
        data: filterParameters,
        headers: {
            'Content-Type': 'application/json'
        }
    });
};
//POST do cadastro de usuário
returnObject.addUsuario = function (formData)
{
    return $http({
        url: baseURLAdm + "/webservice.php?class=EXTDAO_Usuario&jsonOutput=true&action=add",
        method: "POST",
        data: formData,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    });
};
//POST da edição de usuário
returnObject.editUsuario = function (formData)
{
    return $http({
        url: baseURLAdm + "/webservice.php?class=EXTDAO_Usuario&jsonOutput=true&action=edit",
        method: "POST",
        data: formData,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    });
};

/*
 Métodos adicionais
 */
returnObject.getCaracteristicasDoPacoteForUsuarioForm = function ()
{
    return $http({
        url: baseURLAdm + "/webservice.php?class=EXTDAO_Usuario&jsonOutput=true&action=getCaracteristicasDoPacote",
        method: "GET"
    });
};

returnObject.getListOfUsuarioTipoForUsuarioForm = function ()
{
    var params = {
        filterParameters: null,
        listMappingType: Constants.ListMappingType.COMBOBOX,
        hideLoading: true
    };
    return $http.post(baseURLAdm + "/webservice.php?class=EXTDAO_Usuario_tipo&action=__getList", params);
};

returnObject.getListOfEmpresaForUsuarioForm = function ()
{
    var params = {
        filterParameters: null,
        listMappingType: Constants.ListMappingType.COMBOBOX,
        hideLoading: true
    };
    return $http.post(baseURLAdm + "/webservice.php?class=EXTDAO_Empresa&action=__getList", params);
};

returnObject.getListOfTipoDocumentoForUsuarioForm = function ()
{
    var params = {
        filterParameters: null,
        listMappingType: Constants.ListMappingType.COMBOBOX,
        hideLoading: true
    };
    return $http.post(baseURLAdm + "/webservice.php?class=EXTDAO_Tipo_documento&action=__getList", params);
};

returnObject.getListOfCidadeForUsuarioForm = function ()
{
    var params = {
        filterParameters: null,
        listMappingType: Constants.ListMappingType.COMBOBOX,
        hideLoading: true
    };
    return $http.post(baseURLAdm + "/webservice.php?class=EXTDAO_Cidade&action=__getList", params);
};

returnObject.getListOfBairroForUsuarioForm = function (cidadeId)
{
    var params = {
        filterParameters: {cidadeId: cidadeId},
        listMappingType: Constants.ListMappingType.COMBOBOX,
        hideLoading: true
    };
    return $http.post(baseURLAdm + "/webservice.php?class=EXTDAO_Bairro&action=__getList", params);
};

returnObject.getListOfCategoriaPermissaoAndroidForUsuarioForm = function ()
{
    var params = {
        filterParameters: null,
        isRelatedEntity: true
    };
    return $http.post(baseURLAdm + "/webservice.php?class=EXTDAO_Usuario&action=getListOfCategoriaPermissaoAndroid", params);
};

returnObject.getListOfServicosAndroidForUsuarioForm = function ()
{
    var params = {
        filterParameters: null,
        isRelatedEntity: true
    };
    return $http.post(baseURLAdm + "/webservice.php?class=EXTDAO_Usuario&action=getListOfServicosAndroid", params);
};

returnObject.getListOfItemMenuSistemaWebForUsuarioForm = function ()
{
    var params = {
        filterParameters: null,
        isRelatedEntity: true
    };
    return $http.post(baseURLAdm + "/webservice.php?class=EXTDAO_Usuario&action=getListOfItemMenuSistemaWeb", params);
};

returnObject.getAllInitialDataForUsuarioForm = function ()
{
    return $http({
        url: baseURLAdm + "/webservice.php?class=EXTDAO_Usuario&jsonOutput=true&action=__getAllInitialDataForUsuarioForm",
        method: "POST",
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    });
};

returnObject.validarEmailUsuario = function (emailUsuario, idUsuario)
{
    var params = {email: emailUsuario, idUsuario: idUsuario};
    return $http.post(baseURLAdm + "/webservice.php?class=EXTDAO_Usuario&action=validarEmailUsuario", params);
};

returnObject.alterarSenha = function (senha, confirmacaoSenha)
{
    var params = {
        senha: senha,
        confirmacaoSenha: confirmacaoSenha
    };
    return $http.post(baseURLAdm + "/webservice.php?class=EXTDAO_Usuario&action=validarEmailUsuario", params);
};

