ALTER TABLE empresa  MODIFY COLUMN complemento  varchar(100) ;
ALTER TABLE empresa  ADD COLUMN relatorio_id_INT  int(11) ;
ALTER TABLE empresa  ADD COLUMN ind_email_valido_BOOLEAN  int(1) ;
ALTER TABLE empresa  ADD COLUMN ind_celular_valido_BOOLEAN  int(1) ;
ALTER TABLE `empresa`
DROP COLUMN `logradouro_normalizado`;
ALTER TABLE `empresa`
DROP COLUMN `complemento_normalizado`;
ALTER TABLE empresa ADD INDEX `empresa_FK_690795899` (relatorio_id_INT) USING BTREE;
ALTER TABLE empresa
	ADD CONSTRAINT empresa_FK_690795899 FOREIGN KEY (`relatorio_id_INT`)
	REFERENCES `relatorio` (`id`)	ON UPDATE CASCADE	ON DELETE SET NULL;
ALTER TABLE empresa_compra  ADD COLUMN vencimento_SEC  int(10) ;
ALTER TABLE empresa_compra  ADD COLUMN vencimento_OFFSEC  int(6) ;
ALTER TABLE empresa_compra  ADD COLUMN relatorio_id_INT  int(11) ;
ALTER TABLE empresa_compra  ADD COLUMN registro_estado_id_INT  int(11) ;
ALTER TABLE empresa_compra  ADD COLUMN registro_estado_corporacao_id_INT  int(11) ;
ALTER TABLE empresa_compra  ADD COLUMN fechamento_SEC  int(10) ;
ALTER TABLE empresa_compra  ADD COLUMN fechamento_OFFSEC  int(6) ;
ALTER TABLE empresa_compra  ADD COLUMN valor_pago_FLOAT  double;
ALTER TABLE empresa_compra  ADD COLUMN desconto_FLOAT  double;
ALTER TABLE empresa_compra  ADD COLUMN protocolo_INT  bigint(20) ;
ALTER TABLE `empresa_compra`
DROP COLUMN `foto`;
ALTER TABLE empresa_compra ADD INDEX `empresa_compra_FK_584411621` (relatorio_id_INT) USING BTREE;
ALTER TABLE empresa_compra ADD INDEX `empresa_compra_FK_608856201` (registro_estado_id_INT) USING BTREE;
ALTER TABLE empresa_compra ADD INDEX `empresa_compra_FK_382690430` (registro_estado_corporacao_id_INT) USING BTREE;
ALTER TABLE empresa_compra
	ADD CONSTRAINT empresa_compra_FK_584411621 FOREIGN KEY (`relatorio_id_INT`)
	REFERENCES `relatorio` (`id`)	ON UPDATE CASCADE	ON DELETE SET NULL;
ALTER TABLE empresa_compra
	ADD CONSTRAINT empresa_compra_FK_608856201 FOREIGN KEY (`registro_estado_id_INT`)
	REFERENCES `registro_estado` (`id`)	ON UPDATE CASCADE	ON DELETE SET NULL;
ALTER TABLE empresa_compra
	ADD CONSTRAINT empresa_compra_FK_382690430 FOREIGN KEY (`registro_estado_corporacao_id_INT`)
	REFERENCES `registro_estado_corporacao` (`id`)	ON UPDATE CASCADE	ON DELETE SET NULL;
ALTER TABLE empresa_compra_parcela  ADD COLUMN pagamento_SEC  int(10) ;
ALTER TABLE empresa_compra_parcela  ADD COLUMN pagamento_OFFSEC  int(6) ;
ALTER TABLE empresa_equipe ADD INDEX `empresa_equipe_FK_652496338` (empresa_id_INT) USING BTREE;
ALTER TABLE empresa_equipe ADD INDEX `empresa_equipe_FK_545715332` (corporacao_id_INT) USING BTREE;
ALTER TABLE empresa_equipe
	ADD CONSTRAINT empresa_equipe_FK_652496338 FOREIGN KEY (`empresa_id_INT`)
	REFERENCES `empresa` (`id`)	ON UPDATE CASCADE	ON DELETE SET NULL;
ALTER TABLE empresa_equipe
	ADD CONSTRAINT empresa_equipe_FK_545715332 FOREIGN KEY (`corporacao_id_INT`)
	REFERENCES `corporacao` (`id`)	ON UPDATE CASCADE	ON DELETE SET NULL;
ALTER TABLE `empresa_produto` DROP FOREIGN KEY `empresa_produto_ibfk_3`;
ALTER TABLE `empresa_produto` DROP FOREIGN KEY `empresa_produto_ibfk_2`;
ALTER TABLE empresa_produto
	DROP KEY empresa_produto_FK_426605225
;
ALTER TABLE empresa_produto
	DROP KEY empresa_produto_FK_329833984
;
ALTER TABLE empresa_produto  ADD COLUMN preco_custo_FLOAT  double;
ALTER TABLE empresa_produto  ADD COLUMN preco_venda_FLOAT  double;
ALTER TABLE empresa_produto  ADD COLUMN estoque_atual_INT  int(11) ;
ALTER TABLE empresa_produto  ADD COLUMN estoque_minimo_INT  int(11) ;
ALTER TABLE empresa_produto  ADD COLUMN prazo_reposicao_estoque_dias_INT  int(6) ;
ALTER TABLE empresa_produto  ADD COLUMN codigo_barra  char(13) ;
ALTER TABLE empresa_produto  ADD COLUMN produto_id_INT  int(11) ;
ALTER TABLE `empresa_produto`
DROP COLUMN `identificador`;
ALTER TABLE `empresa_produto`
DROP COLUMN `nome`;
ALTER TABLE `empresa_produto`
DROP COLUMN `nome_normalizado`;
ALTER TABLE `empresa_produto`
DROP COLUMN `descricao`;
ALTER TABLE `empresa_produto`
DROP COLUMN `empresa_produto_tipo_id_INT`;
ALTER TABLE `empresa_produto`
DROP COLUMN `empresa_produto_unidade_medida_id_INT`;
ALTER TABLE `empresa_produto`
DROP COLUMN `video`;
ALTER TABLE `empresa_produto`
DROP COLUMN `id_omega_INT`;
ALTER TABLE empresa_produto ADD INDEX `empresa_produto_FK_384552002` (produto_id_INT) USING BTREE;
ALTER TABLE empresa_produto
	ADD CONSTRAINT empresa_produto_FK_384552002 FOREIGN KEY (`produto_id_INT`)
	REFERENCES `produto` (`id`)	ON UPDATE CASCADE	ON DELETE SET NULL;
ALTER TABLE `empresa_produto_compra` DROP FOREIGN KEY `empresa_produto_compra_ibfk_2`;
ALTER TABLE empresa_produto_compra
	DROP KEY empresa_produto_compra_FK_285888672
;
ALTER TABLE empresa_produto_compra  ADD COLUMN produto_id_INT  int(11) ;
ALTER TABLE empresa_produto_compra  ADD COLUMN empresa_id_INT  int(11) ;
ALTER TABLE empresa_produto_compra  ADD COLUMN descricao  varchar(255) ;
ALTER TABLE `empresa_produto_compra`
DROP COLUMN `empresa_produto_id_INT`;
ALTER TABLE empresa_produto_compra ADD INDEX `empresa_produto_compra_FK_968383790` (produto_id_INT) USING BTREE;
ALTER TABLE empresa_produto_compra ADD INDEX `empresa_produto_compra_FK_618072510` (empresa_id_INT) USING BTREE;
ALTER TABLE empresa_produto_compra
	ADD CONSTRAINT empresa_produto_compra_FK_968383790 FOREIGN KEY (`produto_id_INT`)
	REFERENCES `produto` (`id`)	ON UPDATE CASCADE	ON DELETE SET NULL;
ALTER TABLE empresa_produto_compra
	ADD CONSTRAINT empresa_produto_compra_FK_618072510 FOREIGN KEY (`empresa_id_INT`)
	REFERENCES `empresa` (`id`)	ON UPDATE CASCADE	ON DELETE SET NULL;
ALTER TABLE `empresa_produto_venda` DROP FOREIGN KEY `empresa_produto_venda_ibfk_2`;
ALTER TABLE empresa_produto_venda
	DROP KEY empresa_produto_venda_FK_771057129
;
ALTER TABLE empresa_produto_venda  ADD COLUMN produto_id_INT  int(11) ;
ALTER TABLE empresa_produto_venda  ADD COLUMN descricao  varchar(255) ;
ALTER TABLE empresa_produto_venda  ADD COLUMN cadastro_SEC  int(10) ;
ALTER TABLE empresa_produto_venda  ADD COLUMN cadastro_OFFSEC  int(6) ;
ALTER TABLE empresa_produto_venda  ADD COLUMN empresa_id_INT  int(11) ;
ALTER TABLE `empresa_produto_venda`
DROP COLUMN `empresa_produto_id_INT`;
ALTER TABLE empresa_produto_venda ADD INDEX `empresa_produto_venda_FK_420928955` (produto_id_INT) USING BTREE;
ALTER TABLE empresa_produto_venda ADD INDEX `empresa_produto_venda_FK_367370605` (empresa_id_INT) USING BTREE;
ALTER TABLE empresa_produto_venda
	ADD CONSTRAINT empresa_produto_venda_FK_420928955 FOREIGN KEY (`produto_id_INT`)
	REFERENCES `produto` (`id`)	ON UPDATE CASCADE	ON DELETE SET NULL;
ALTER TABLE empresa_produto_venda
	ADD CONSTRAINT empresa_produto_venda_FK_367370605 FOREIGN KEY (`empresa_id_INT`)
	REFERENCES `empresa` (`id`)	ON UPDATE CASCADE	ON DELETE SET NULL;
ALTER TABLE empresa_venda  ADD COLUMN registro_estado_id_INT  int(11) ;
ALTER TABLE empresa_venda  ADD COLUMN registro_estado_corporacao_id_INT  int(11) ;
ALTER TABLE empresa_venda  ADD COLUMN relatorio_id_INT  int(11) ;
ALTER TABLE empresa_venda  ADD COLUMN descricao  varchar(255) ;
ALTER TABLE empresa_venda  ADD COLUMN vencimento_SEC  int(10) ;
ALTER TABLE empresa_venda  ADD COLUMN vencimento_OFFSEC  int(6) ;
ALTER TABLE empresa_venda  ADD COLUMN fechamento_SEC  int(10) ;
ALTER TABLE empresa_venda  ADD COLUMN fechamento_OFFSEC  int(6) ;
ALTER TABLE empresa_venda  ADD COLUMN valor_pago_FLOAT  double;
ALTER TABLE empresa_venda  ADD COLUMN mesa_id_INT  int(11) ;
ALTER TABLE empresa_venda  ADD COLUMN identificador  varchar(255) ;
ALTER TABLE empresa_venda  ADD COLUMN protocolo_INT  bigint(20) ;
ALTER TABLE empresa_venda ADD INDEX `empresa_venda_FK_985656739` (registro_estado_id_INT) USING BTREE;
ALTER TABLE empresa_venda ADD INDEX `empresa_venda_FK_407073975` (registro_estado_corporacao_id_INT) USING BTREE;
ALTER TABLE empresa_venda ADD INDEX `empresa_venda_FK_402862549` (relatorio_id_INT) USING BTREE;
ALTER TABLE empresa_venda ADD INDEX `empresa_venda_FK_648132324` (mesa_id_INT) USING BTREE;
ALTER TABLE empresa_venda
	ADD CONSTRAINT empresa_venda_FK_985656739 FOREIGN KEY (`registro_estado_id_INT`)
	REFERENCES `registro_estado` (`id`)	ON UPDATE CASCADE	ON DELETE SET NULL;
ALTER TABLE empresa_venda
	ADD CONSTRAINT empresa_venda_FK_407073975 FOREIGN KEY (`registro_estado_corporacao_id_INT`)
	REFERENCES `registro_estado_corporacao` (`id`)	ON UPDATE CASCADE	ON DELETE SET NULL;
ALTER TABLE empresa_venda
	ADD CONSTRAINT empresa_venda_FK_402862549 FOREIGN KEY (`relatorio_id_INT`)
	REFERENCES `relatorio` (`id`)	ON UPDATE CASCADE	ON DELETE SET NULL;
ALTER TABLE empresa_venda
	ADD CONSTRAINT empresa_venda_FK_648132324 FOREIGN KEY (`mesa_id_INT`)
	REFERENCES `mesa` (`id`)	ON UPDATE CASCADE	ON DELETE SET NULL;
ALTER TABLE empresa_venda_parcela  ADD COLUMN pagamento_SEC  int(10) ;
ALTER TABLE empresa_venda_parcela  ADD COLUMN pagamento_OFFSEC  int(6) ;
ALTER TABLE empresa_venda_parcela  ADD COLUMN seq_INT  int(6) ;
ALTER TABLE `forma_pagamento`
DROP COLUMN `nome_normalizado`;
ALTER TABLE pessoa  MODIFY COLUMN complemento  varchar(100) ;
ALTER TABLE pessoa  ADD COLUMN data_nascimento_SEC  int(10) ;
ALTER TABLE pessoa  ADD COLUMN data_nascimento_OFFSEC  int(6) ;
ALTER TABLE pessoa  ADD COLUMN relatorio_id_INT  int(11) ;
ALTER TABLE pessoa  ADD COLUMN ind_email_valido_BOOLEAN  int(1) ;
ALTER TABLE pessoa  ADD COLUMN ind_celular_valido_BOOLEAN  int(1) ;
ALTER TABLE `pessoa`
DROP COLUMN `logradouro_normalizado`;
ALTER TABLE `pessoa`
DROP COLUMN `complemento_normalizado`;
ALTER TABLE pessoa ADD INDEX `pessoa_FK_594970703` (relatorio_id_INT) USING BTREE;
ALTER TABLE pessoa
	ADD CONSTRAINT pessoa_FK_594970703 FOREIGN KEY (`relatorio_id_INT`)
	REFERENCES `relatorio` (`id`)	ON UPDATE CASCADE	ON DELETE SET NULL;
ALTER TABLE ponto  ADD COLUMN descricao  varchar(100) ;
ALTER TABLE ponto  ADD COLUMN endereco  varchar(255) ;
ALTER TABLE rede  ADD COLUMN cadastro_SEC  int(10) ;
ALTER TABLE rede  ADD COLUMN cadastro_OFFSEC  int(6) ;
ALTER TABLE rede  ADD COLUMN relatorio_id_INT  int(11) ;
ALTER TABLE rede ADD INDEX `rede_FK_704559326` (relatorio_id_INT) USING BTREE;
ALTER TABLE rede
	ADD CONSTRAINT rede_FK_704559326 FOREIGN KEY (`relatorio_id_INT`)
	REFERENCES `relatorio` (`id`)	ON UPDATE CASCADE	ON DELETE SET NULL;
ALTER TABLE rede_empresa  ADD COLUMN cadastro_SEC  int(10) ;
ALTER TABLE rede_empresa  ADD COLUMN cadastro_OFFSEC  int(6) ;
ALTER TABLE `relatorio` DROP FOREIGN KEY `relatorio_ibfk_3`;
ALTER TABLE `relatorio` DROP FOREIGN KEY `relatorio_ibfk_4`;
ALTER TABLE relatorio
	DROP KEY relatorio_FK_584716797
;
ALTER TABLE relatorio
	DROP KEY relatorio_FK_735870362
;
ALTER TABLE relatorio  MODIFY COLUMN titulo  varchar(100) ;
ALTER TABLE relatorio  MODIFY COLUMN descricao  varchar(255) ;
ALTER TABLE relatorio  ADD COLUMN tipo_relatorio_id_INT  int(11) ;
ALTER TABLE `relatorio`
DROP COLUMN `titulo_normalizado`;
ALTER TABLE `relatorio`
DROP COLUMN `descricao_normalizado`;
ALTER TABLE `relatorio`
DROP COLUMN `pessoa_id_INT`;
ALTER TABLE `relatorio`
DROP COLUMN `empresa_id_INT`;
ALTER TABLE relatorio ADD INDEX `relatorio_FK_984588624` (tipo_relatorio_id_INT) USING BTREE;
ALTER TABLE relatorio
	ADD CONSTRAINT relatorio_FK_984588624 FOREIGN KEY (`tipo_relatorio_id_INT`)
	REFERENCES `tipo_relatorio` (`id`)	ON UPDATE CASCADE	ON DELETE SET NULL;
ALTER TABLE relatorio_anexo  MODIFY COLUMN arquivo  varchar(50) ;
ALTER TABLE relatorio_anexo  ADD COLUMN descricao  varchar(100) ;
ALTER TABLE `sistema_parametro_global_corporacao` DROP FOREIGN KEY `spgc_FK_66619873`;
ALTER TABLE sistema_parametro_global_corporacao
	DROP KEY spgc_ibfk_1
;
ALTER TABLE sistema_parametro_global_corporacao ADD INDEX `spgc_FK_66619873` (corporacao_id_INT) USING BTREE;
ALTER TABLE sistema_parametro_global_corporacao
	ADD CONSTRAINT spgc_ibfk_1 FOREIGN KEY (`corporacao_id_INT`)
	REFERENCES `corporacao` (`id`)	ON UPDATE CASCADE	ON DELETE SET NULL;
ALTER TABLE `tarefa` DROP FOREIGN KEY `tarefa_ibfk_1`;
ALTER TABLE `tarefa` DROP FOREIGN KEY `tarefa_ibfk_9`;
ALTER TABLE `tarefa` DROP FOREIGN KEY `tarefa_ibfk_6`;
ALTER TABLE tarefa
	DROP KEY tarefa_FK_748779297
;
ALTER TABLE tarefa
	DROP KEY tarefa_FK_672546387
;
ALTER TABLE tarefa
	DROP KEY tarefa_FK_114227295
;
ALTER TABLE tarefa  ADD COLUMN empresa_atividade_id_INT  int(11) ;
ALTER TABLE tarefa  ADD COLUMN tipo_tarefa_id_INT  int(4) ;
ALTER TABLE tarefa  ADD COLUMN empresa_venda_id_INT  int(11) ;
ALTER TABLE tarefa  ADD COLUMN id_prioridade_INT  int(4) ;
ALTER TABLE tarefa  ADD COLUMN registro_estado_id_INT  int(11) ;
ALTER TABLE tarefa  ADD COLUMN registro_estado_corporacao_id_INT  int(11) ;
ALTER TABLE tarefa  ADD COLUMN percentual_completo_INT  int(2) ;
ALTER TABLE tarefa  ADD COLUMN prazo_SEC  int(10) ;
ALTER TABLE tarefa  ADD COLUMN prazo_OFFSEC  int(6) ;
ALTER TABLE tarefa  ADD COLUMN atividade_id_INT  int(11) ;
ALTER TABLE tarefa  ADD COLUMN protocolo_empresa_venda_INT  bigint(20) ;
ALTER TABLE tarefa  ADD COLUMN protocolo_empresa_atividade_venda_INT  bigint(20) ;
ALTER TABLE `tarefa`
DROP COLUMN `veiculo_usuario_id_INT`;
ALTER TABLE `tarefa`
DROP COLUMN `origem_logradouro_normalizado`;
ALTER TABLE `tarefa`
DROP COLUMN `origem_complemento`;
ALTER TABLE `tarefa`
DROP COLUMN `origem_complemento_normalizado`;
ALTER TABLE `tarefa`
DROP COLUMN `origem_bairro_id_INT`;
ALTER TABLE `tarefa`
DROP COLUMN `destino_logradouro_normalizado`;
ALTER TABLE `tarefa`
DROP COLUMN `destino_complemento`;
ALTER TABLE `tarefa`
DROP COLUMN `destino_complemento_normalizado`;
ALTER TABLE `tarefa`
DROP COLUMN `destino_bairro_id_INT`;
ALTER TABLE `tarefa`
DROP COLUMN `is_realizada_a_pe_BOOLEAN`;
ALTER TABLE `tarefa`
DROP COLUMN `descricao_normalizado`;
ALTER TABLE tarefa ADD INDEX `tarefa_FK_603546143` (empresa_equipe_id_INT) USING BTREE;
ALTER TABLE tarefa ADD INDEX `tarefa_FK_751190186` (tipo_tarefa_id_INT) USING BTREE;
ALTER TABLE tarefa ADD INDEX `tarefa_FK_972564698` (registro_estado_id_INT) USING BTREE;
ALTER TABLE tarefa ADD INDEX `tarefa_FK_558166504` (registro_estado_corporacao_id_INT) USING BTREE;
ALTER TABLE tarefa ADD INDEX `tarefa_FK_189971924` (empresa_atividade_id_INT) USING BTREE;
ALTER TABLE tarefa ADD INDEX `tarefa_FK_443054199` (empresa_venda_id_INT) USING BTREE;
ALTER TABLE tarefa ADD INDEX `tarefa_FK_575897217` (atividade_id_INT) USING BTREE;
ALTER TABLE tarefa
	ADD CONSTRAINT tarefa_FK_603546143 FOREIGN KEY (`empresa_equipe_id_INT`)
	REFERENCES `empresa_equipe` (`id`)	ON UPDATE CASCADE	ON DELETE SET NULL;
ALTER TABLE tarefa
	ADD CONSTRAINT tarefa_FK_189971924 FOREIGN KEY (`empresa_atividade_id_INT`)
	REFERENCES `empresa_atividade` (`id`)	ON UPDATE SET NULL	ON DELETE SET NULL;
ALTER TABLE tarefa
	ADD CONSTRAINT tarefa_FK_751190186 FOREIGN KEY (`tipo_tarefa_id_INT`)
	REFERENCES `tipo_tarefa` (`id`)	ON UPDATE CASCADE	ON DELETE SET NULL;
ALTER TABLE tarefa
	ADD CONSTRAINT tarefa_FK_443054199 FOREIGN KEY (`empresa_venda_id_INT`)
	REFERENCES `empresa_venda` (`id`)	ON UPDATE SET NULL	ON DELETE SET NULL;
ALTER TABLE tarefa
	ADD CONSTRAINT tarefa_FK_972564698 FOREIGN KEY (`registro_estado_id_INT`)
	REFERENCES `registro_estado` (`id`)	ON UPDATE CASCADE	ON DELETE SET NULL;
ALTER TABLE tarefa
	ADD CONSTRAINT tarefa_FK_558166504 FOREIGN KEY (`registro_estado_corporacao_id_INT`)
	REFERENCES `registro_estado_corporacao` (`id`)	ON UPDATE CASCADE	ON DELETE SET NULL;
ALTER TABLE tarefa
	ADD CONSTRAINT tarefa_FK_575897217 FOREIGN KEY (`atividade_id_INT`)
	REFERENCES `atividade` (`id`)	ON UPDATE SET NULL	ON DELETE SET NULL;
ALTER TABLE veiculo_registro  ADD COLUMN tipo_veiculo_registro_id_INT  int(11) ;
ALTER TABLE veiculo_registro ADD INDEX `veiculo_registro_FK_612823486` (tipo_veiculo_registro_id_INT) USING BTREE;
ALTER TABLE veiculo_registro
	ADD CONSTRAINT veiculo_registro_FK_612823486 FOREIGN KEY (`tipo_veiculo_registro_id_INT`)
	REFERENCES `tipo_veiculo_registro` (`id`)	ON UPDATE CASCADE	ON DELETE SET NULL;
CREATE TABLE api (
	id int(11)  NOT NULL  AUTO_INCREMENT ,
	nome varchar(100) ,
	PRIMARY KEY(id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
;
CREATE TABLE api_id (
	id int(11)  NOT NULL  AUTO_INCREMENT ,
	pessoa_id_INT int(11) ,
	usuario_id_INT int(11) ,
	empresa_id_INT int(11) ,
	chave varchar(255) ,
	identificador varchar(255) ,
	cadastro_SEC int(10) ,
	cadastro_OFFSEC int(6) ,
	corporacao_id_INT int(11) ,
	PRIMARY KEY(id),
	 KEY `api_id_FK_814331055` (`pessoa_id_INT`), KEY `api_id_FK_701324463` (`usuario_id_INT`), KEY `api_id_FK_867980957` (`empresa_id_INT`), KEY `api_id_FK_830413819` (`corporacao_id_INT`),
	CONSTRAINT `api_id_FK_814331055` FOREIGN KEY (`pessoa_id_INT`) REFERENCES `pessoa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `api_id_FK_701324463` FOREIGN KEY (`usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `api_id_FK_867980957` FOREIGN KEY (`empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `api_id_FK_830413819` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
;
CREATE TABLE area_menu (
	id int(11)  NOT NULL  AUTO_INCREMENT ,
	nome varchar(252) ,
	PRIMARY KEY(id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
;
CREATE TABLE atividade (
	id int(11)  NOT NULL  AUTO_INCREMENT ,
	identificador varchar(30) ,
	nome varchar(255)  NOT NULL ,
	nome_normalizado varchar(255)  NOT NULL ,
	descricao varchar(512) ,
	empresa_id_INT int(11) ,
	atividade_unidade_medida_id_INT int(11) ,
	prazo_entrega_dia_INT int(11) ,
	duracao_horas_INT int(3) ,
	id_omega_INT int(11) ,
	cadastro_SEC int(10) ,
	cadastro_OFFSEC int(6) ,
	corporacao_id_INT int(11) ,
	relatorio_id_INT int(11) ,
	PRIMARY KEY(id),
	 KEY `empresa_servico_FK_389770508` (`empresa_id_INT`), KEY `empresa_servico_FK_414123535` (`corporacao_id_INT`), KEY `atividade_FK_219207763` (`atividade_unidade_medida_id_INT`), KEY `atividade_FK_411010742` (`relatorio_id_INT`),
	CONSTRAINT `atividade_ibfk_2` FOREIGN KEY (`empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `atividade_FK_219207763` FOREIGN KEY (`atividade_unidade_medida_id_INT`) REFERENCES `atividade_unidade_medida` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `atividade_ibfk_3` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `atividade_FK_411010742` FOREIGN KEY (`relatorio_id_INT`) REFERENCES `relatorio` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
;
CREATE TABLE atividade_tipo (
	id int(11)  NOT NULL  AUTO_INCREMENT ,
	nome varchar(100) ,
	nome_normalizado varchar(100) ,
	corporacao_id_INT int(11) ,
	PRIMARY KEY(id),
	 KEY `empresa_servico_tipo_FK_604431152` (`corporacao_id_INT`),
	CONSTRAINT `atividade_tipo_ibfk_1` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
;
ALTER TABLE atividade_tipo ADD CONSTRAINT nome_098787 UNIQUE (nome, corporacao_id_INT)
;
CREATE TABLE atividade_tipos (
	id int(11)  NOT NULL  AUTO_INCREMENT ,
	atividade_tipo_id_INT int(11) ,
	atividade_id_INT int(11) ,
	corporacao_id_INT int(11) ,
	PRIMARY KEY(id),
	 KEY `atividade_tipos_FK_423400879` (`atividade_tipo_id_INT`), KEY `atividade_tipos_FK_996185303` (`atividade_id_INT`), KEY `atividade_tipos_FK_789062500` (`corporacao_id_INT`),
	CONSTRAINT `atividade_tipos_FK_423400879` FOREIGN KEY (`atividade_tipo_id_INT`) REFERENCES `atividade_tipo` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `atividade_tipos_FK_996185303` FOREIGN KEY (`atividade_id_INT`) REFERENCES `atividade` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `atividade_tipos_FK_789062500` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
;
CREATE TABLE atividade_unidade_medida (
	id int(11)  NOT NULL  AUTO_INCREMENT ,
	nome varchar(100)  NOT NULL ,
	corporacao_id_INT int(11) ,
	PRIMARY KEY(id),
	 KEY `atividade_unidade_medida_FK_833953858` (`corporacao_id_INT`),
	CONSTRAINT `atividade_unidade_medida_FK_833953858` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
;
CREATE TABLE despesa (
	id int(11)  NOT NULL  AUTO_INCREMENT ,
	valor_FLOAT double,
	empresa_id_INT int(11) ,
	vencimento_SEC int(10) ,
	vencimento_OFFSEC int(6) ,
	pagamento_SEC int(10) ,
	pagamento_OFFSEC int(6) ,
	despesa_cotidiano_id_INT int(11) ,
	valor_pagamento_FLOAT double,
	cadastro_usuario_id_INT int(11) ,
	pagamento_usuario_id_INT int(11) ,
	protocolo_INT bigint(20) ,
	corporacao_id_INT int(11) ,
	PRIMARY KEY(id),
	 KEY `despesa_FK_685638428` (`empresa_id_INT`), KEY `despesa_FK_939453125` (`despesa_cotidiano_id_INT`), KEY `despesa_FK_777130127` (`cadastro_usuario_id_INT`), KEY `despesa_FK_818298340` (`pagamento_usuario_id_INT`), KEY `despesa_FK_352508545` (`corporacao_id_INT`),
	CONSTRAINT `despesa_FK_685638428` FOREIGN KEY (`empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `despesa_FK_939453125` FOREIGN KEY (`despesa_cotidiano_id_INT`) REFERENCES `despesa_cotidiano` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `despesa_FK_777130127` FOREIGN KEY (`cadastro_usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `despesa_FK_818298340` FOREIGN KEY (`pagamento_usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `despesa_FK_352508545` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
;
CREATE TABLE despesa_cotidiano (
	id int(11)  NOT NULL  AUTO_INCREMENT ,
	despesa_da_empresa_id_INT int(11) ,
	despesa_do_usuario_id_INT int(11) ,
	cadastro_usuario_id_INT int(11) ,
	id_tipo_despesa_cotidiano_INT int(3) ,
	parametro_INT int(11) ,
	parametro_OFFSEC int(10) ,
	parametro_SEC int(6) ,
	parametro_json varchar(512) ,
	valor_FLOAT double,
	data_limite_cotidiano_SEC int(10) ,
	data_limite_cotidiano_OFFSEC int(6) ,
	corporacao_id_INT int(11) ,
	PRIMARY KEY(id),
	 KEY `despesa_cotidiano_FK_33874511` (`despesa_da_empresa_id_INT`), KEY `despesa_cotidiano_FK_475311279` (`despesa_do_usuario_id_INT`), KEY `despesa_cotidiano_FK_201293945` (`cadastro_usuario_id_INT`), KEY `despesa_cotidiano_FK_755279541` (`corporacao_id_INT`),
	CONSTRAINT `despesa_cotidiano_FK_33874511` FOREIGN KEY (`despesa_da_empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `despesa_cotidiano_FK_475311279` FOREIGN KEY (`despesa_do_usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `despesa_cotidiano_FK_201293945` FOREIGN KEY (`cadastro_usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `despesa_cotidiano_FK_755279541` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
;
CREATE TABLE empresa_atividade (
	id int(11)  NOT NULL  AUTO_INCREMENT ,
	empresa_id_INT int(11) ,
	prazo_entrega_dia_INT int(11) ,
	duracao_horas_INT int(3) ,
	cadastro_SEC int(10) ,
	cadastro_OFFSEC int(6) ,
	preco_custo_FLOAT double,
	preco_venda_FLOAT double,
	atividade_id_INT int(11) ,
	corporacao_id_INT int(11) ,
	PRIMARY KEY(id),
	 KEY `empresa_servico_FK_389770508` (`empresa_id_INT`), KEY `empresa_servico_FK_414123535` (`corporacao_id_INT`), KEY `empresa_atividade_FK_124084472` (`atividade_id_INT`),
	CONSTRAINT `empresa_atividade_ibfk_2` FOREIGN KEY (`empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `empresa_atividade_FK_124084472` FOREIGN KEY (`atividade_id_INT`) REFERENCES `atividade` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `empresa_atividade_ibfk_3` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
;
CREATE TABLE empresa_atividade_compra (
	id int(11)  NOT NULL  AUTO_INCREMENT ,
	empresa_compra_id_INT int(11) ,
	quantidade_FLOAT double NOT NULL ,
	valor_total_FLOAT double NOT NULL ,
	desconto_FLOAT double,
	descricao varchar(255) ,
	atividade_id_INT int(11) ,
	empresa_id_INT int(11) ,
	corporacao_id_INT int(11) ,
	PRIMARY KEY(id),
	 KEY `empresa_servico_compra_FK_892700196` (`empresa_compra_id_INT`), KEY `empresa_servico_compra_FK_118560791` (`corporacao_id_INT`), KEY `empresa_atividade_compra_FK_668334961` (`atividade_id_INT`), KEY `empresa_atividade_compra_FK_537750244` (`empresa_id_INT`),
	CONSTRAINT `empresa_atividade_compra_ibfk_3` FOREIGN KEY (`empresa_compra_id_INT`) REFERENCES `empresa_compra` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `empresa_atividade_compra_FK_668334961` FOREIGN KEY (`atividade_id_INT`) REFERENCES `atividade` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `empresa_atividade_compra_FK_537750244` FOREIGN KEY (`empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `empresa_atividade_compra_ibfk_1` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
;
CREATE TABLE empresa_atividade_venda (
	id int(11)  NOT NULL  AUTO_INCREMENT ,
	empresa_venda_id_INT int(11) ,
	quantidade_FLOAT double NOT NULL ,
	valor_total_FLOAT double NOT NULL ,
	desconto_FLOAT double,
	corporacao_id_INT int(11) ,
	cadastro_SEC int(10) ,
	cadastro_OFFSEC int(6) ,
	atividade_id_INT int(11) ,
	empresa_id_INT int(11) ,
	protocolo_INT bigint(20) ,
	PRIMARY KEY(id),
	 KEY `empresa_servico_venda_FK_937438965` (`empresa_venda_id_INT`), KEY `empresa_servico_venda_FK_338836670` (`corporacao_id_INT`), KEY `empresa_atividade_venda_FK_500762939` (`atividade_id_INT`), KEY `empresa_atividade_venda_FK_194274902` (`empresa_id_INT`),
	CONSTRAINT `empresa_atividade_venda_ibfk_3` FOREIGN KEY (`empresa_venda_id_INT`) REFERENCES `empresa_venda` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `empresa_atividade_venda_ibfk_2` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `empresa_atividade_venda_FK_500762939` FOREIGN KEY (`atividade_id_INT`) REFERENCES `atividade` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `empresa_atividade_venda_FK_194274902` FOREIGN KEY (`empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
;
CREATE TABLE empresa_venda_template (
	id int(11)  NOT NULL  AUTO_INCREMENT ,
	titulo varchar(100) ,
	template_json varchar(512) ,
	produto_id_INT int(11) ,
	empresa_id_INT int(11) ,
	corporacao_id_INT int(11) ,
	PRIMARY KEY(id),
	 KEY `empresa_venda_template_FK_418518066` (`produto_id_INT`), KEY `empresa_venda_template_FK_963958741` (`empresa_id_INT`), KEY `empresa_venda_template_FK_776367188` (`corporacao_id_INT`),
	CONSTRAINT `empresa_venda_template_FK_418518066` FOREIGN KEY (`produto_id_INT`) REFERENCES `produto` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `empresa_venda_template_FK_963958741` FOREIGN KEY (`empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `empresa_venda_template_FK_776367188` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
;
CREATE TABLE mensagem (
	id int(11)  NOT NULL  AUTO_INCREMENT ,
	titulo varchar(100) ,
	mensagem varchar(512) ,
	remetente_usuario_id_INT int(11) ,
	tipo_mensagem_id_INT int(11) ,
	destinatario_usuario_id_INT int(11) ,
	destinatario_pessoa_id_INT int(11) ,
	destinatario_empresa_id_INT int(11) ,
	data_min_envio_SEC int(10) ,
	data_min_envio_OFFSEC int(6) ,
	protocolo_INT bigint(20) ,
	registro_estado_id_INT int(11) ,
	empresa_para_cliente_BOOLEAN int(1) ,
	corporacao_id_INT int(11) ,
	PRIMARY KEY(id),
	 KEY `mensagem_FK_646789551` (`remetente_usuario_id_INT`), KEY `mensagem_FK_970550538` (`tipo_mensagem_id_INT`), KEY `mensagem_FK_119873046` (`destinatario_usuario_id_INT`), KEY `mensagem_FK_67901611` (`destinatario_pessoa_id_INT`), KEY `mensagem_FK_637390137` (`destinatario_empresa_id_INT`), KEY `mensagem_FK_258514404` (`registro_estado_id_INT`), KEY `mensagem_FK_570434570` (`corporacao_id_INT`),
	CONSTRAINT `mensagem_FK_646789551` FOREIGN KEY (`remetente_usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `mensagem_FK_970550538` FOREIGN KEY (`tipo_mensagem_id_INT`) REFERENCES `mensagem` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `mensagem_FK_119873046` FOREIGN KEY (`destinatario_usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `mensagem_FK_67901611` FOREIGN KEY (`destinatario_pessoa_id_INT`) REFERENCES `pessoa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `mensagem_FK_637390137` FOREIGN KEY (`destinatario_empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `mensagem_FK_258514404` FOREIGN KEY (`registro_estado_id_INT`) REFERENCES `registro_estado` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `mensagem_FK_570434570` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
;
CREATE TABLE mensagem_envio (
	id int(11)  NOT NULL  AUTO_INCREMENT ,
	tipo_canal_envio_id_INT int(11) ,
	mensagem_id_INT int(11) ,
	cadastro_SEC int(10) ,
	cadastro_OFFSEC int(6) ,
	obs varchar(100) ,
	identificador varchar(255) ,
	tentativa_INT int(2) ,
	registro_estado_id_INT int(11) ,
	corporacao_id_INT int(11) ,
	PRIMARY KEY(id),
	 KEY `mensagem_envio_FK_703460694` (`tipo_canal_envio_id_INT`), KEY `mensagem_envio_FK_757080078` (`mensagem_id_INT`), KEY `mensagem_envio_FK_273468017` (`registro_estado_id_INT`), KEY `mensagem_envio_FK_794128418` (`corporacao_id_INT`),
	CONSTRAINT `mensagem_envio_FK_703460694` FOREIGN KEY (`tipo_canal_envio_id_INT`) REFERENCES `tipo_canal_envio` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `mensagem_envio_FK_757080078` FOREIGN KEY (`mensagem_id_INT`) REFERENCES `mensagem` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `mensagem_envio_FK_273468017` FOREIGN KEY (`registro_estado_id_INT`) REFERENCES `registro_estado` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `mensagem_envio_FK_794128418` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
;
CREATE TABLE mesa (
	id int(11)  NOT NULL  AUTO_INCREMENT ,
	descricao varchar(100) ,
	empresa_id_INT int(11) ,
	template_json varchar(255) ,
	cadastro_SEC int(10) ,
	cadastro_OFFSEC int(6) ,
	corporacao_id_INT int(11) ,
	PRIMARY KEY(id),
	 KEY `mesa_FK_462615967` (`empresa_id_INT`), KEY `mesa_FK_317504883` (`corporacao_id_INT`),
	CONSTRAINT `mesa_FK_462615967` FOREIGN KEY (`empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `mesa_FK_317504883` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
;
CREATE TABLE mesa_reserva (
	id int(11)  NOT NULL  AUTO_INCREMENT ,
	mesa_id_INT int(11) ,
	cadastro_SEC int(10) ,
	cadastro_OFFSEC int(6) ,
	pessoa_id_INT int(11) ,
	reserva_SEC int(10) ,
	reserva_OFFSEC int(6) ,
	confimado_SEC int(10) ,
	confirmado_OFFSEC int(6) ,
	confirmador_usuario_id_INT int(11) ,
	celular varchar(30) ,
	protocolo_INT bigint(20) ,
	corporacao_id_INT int(11) ,
	PRIMARY KEY(id),
	 KEY `mesa_reserva_FK_235290527` (`mesa_id_INT`), KEY `mesa_reserva_FK_304656982` (`pessoa_id_INT`), KEY `mesa_reserva_FK_974975586` (`confirmador_usuario_id_INT`), KEY `mesa_reserva_FK_961578370` (`corporacao_id_INT`),
	CONSTRAINT `mesa_reserva_FK_235290527` FOREIGN KEY (`mesa_id_INT`) REFERENCES `mesa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `mesa_reserva_FK_304656982` FOREIGN KEY (`pessoa_id_INT`) REFERENCES `pessoa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `mesa_reserva_FK_974975586` FOREIGN KEY (`confirmador_usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `mesa_reserva_FK_961578370` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
;
CREATE TABLE negociacao_divida (
	id int(11)  NOT NULL  AUTO_INCREMENT ,
	devedor_empresa_id_INT int(11) ,
	devedor_pessoa_id_INT int(11) ,
	negociador_usuario_id_INT int(11) ,
	cadastro_SEC int(10) ,
	cadastro_OFFSEC int(6) ,
	resultado_empresa_venda_id_INT int(11) ,
	relatorio_id_INT int(11) ,
	registro_estado_id_INT int(11) ,
	protocolo_INT bigint(20) ,
	corporacao_id_INT int(11) ,
	PRIMARY KEY(id),
	 KEY `negociacao_divida_FK_620697022` (`devedor_empresa_id_INT`), KEY `negociacao_divida_FK_110839843` (`devedor_pessoa_id_INT`), KEY `negociacao_divida_FK_708282471` (`negociador_usuario_id_INT`), KEY `negociacao_divida_FK_563903809` (`resultado_empresa_venda_id_INT`), KEY `negociacao_divida_FK_873504639` (`relatorio_id_INT`), KEY `negociacao_divida_FK_729370117` (`registro_estado_id_INT`), KEY `negociacao_divida_FK_940582276` (`corporacao_id_INT`),
	CONSTRAINT `negociacao_divida_FK_620697022` FOREIGN KEY (`devedor_empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `negociacao_divida_FK_110839843` FOREIGN KEY (`devedor_pessoa_id_INT`) REFERENCES `pessoa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `negociacao_divida_FK_708282471` FOREIGN KEY (`negociador_usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `negociacao_divida_FK_563903809` FOREIGN KEY (`resultado_empresa_venda_id_INT`) REFERENCES `empresa_venda` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `negociacao_divida_FK_873504639` FOREIGN KEY (`relatorio_id_INT`) REFERENCES `relatorio` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `negociacao_divida_FK_729370117` FOREIGN KEY (`registro_estado_id_INT`) REFERENCES `registro_estado` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `negociacao_divida_FK_940582276` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
;
CREATE TABLE negociacao_divida_empresa_venda (
	id int(11)  NOT NULL  AUTO_INCREMENT ,
	empresa_venda_protocolo_INT bigint(11) ,
	negociacao_divida_id_INT int(11) ,
	corporacao_id_INT int(11) ,
	PRIMARY KEY(id),
	 KEY `negociacao_divida_empresa_venda_FK_449615478` (`negociacao_divida_id_INT`), KEY `negociacao_divida_empresa_venda_FK_306701660` (`corporacao_id_INT`),
	CONSTRAINT `negociacao_divida_empresa_venda_FK_449615478` FOREIGN KEY (`negociacao_divida_id_INT`) REFERENCES `negociacao_divida` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `negociacao_divida_empresa_venda_FK_306701660` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
;
CREATE TABLE nota (
	id int(11)  NOT NULL  AUTO_INCREMENT ,
	empresa_id_INT int(11) ,
	pessoa_id_INT int(11) ,
	usuario_id_INT int(11) ,
	veiculo_id_INT int(11) ,
	relatorio_id_INT int(11) ,
	protocolo_INT bigint(20) ,
	corporacao_id_INT int(11) ,
	PRIMARY KEY(id),
	 KEY `nota_FK_922271729` (`empresa_id_INT`), KEY `nota_FK_826232910` (`pessoa_id_INT`), KEY `nota_FK_654876709` (`usuario_id_INT`), KEY `nota_FK_336425781` (`veiculo_id_INT`), KEY `nota_FK_797149659` (`relatorio_id_INT`), KEY `nota_FK_750427246` (`corporacao_id_INT`),
	CONSTRAINT `nota_FK_922271729` FOREIGN KEY (`empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `nota_FK_826232910` FOREIGN KEY (`pessoa_id_INT`) REFERENCES `pessoa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `nota_FK_654876709` FOREIGN KEY (`usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `nota_FK_336425781` FOREIGN KEY (`veiculo_id_INT`) REFERENCES `veiculo` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `nota_FK_797149659` FOREIGN KEY (`relatorio_id_INT`) REFERENCES `relatorio` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `nota_FK_750427246` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
;
CREATE TABLE nota_tipo_nota (
	id int(11)  NOT NULL  AUTO_INCREMENT ,
	nota_id_INT int(11) ,
	tipo_nota_id_INT int(11) ,
	corporacao_id_INT int(11) ,
	PRIMARY KEY(id),
	 KEY `nota_tipo_nota_FK_277954101` (`tipo_nota_id_INT`), KEY `nota_tipo_nota_FK_861236573` (`corporacao_id_INT`),
	CONSTRAINT `nota_tipo_nota_FK_92987060` FOREIGN KEY (`nota_id_INT`) REFERENCES `nota` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `nota_tipo_nota_FK_277954101` FOREIGN KEY (`tipo_nota_id_INT`) REFERENCES `tipo_nota` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `nota_tipo_nota_FK_861236573` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
;
ALTER TABLE nota_tipo_nota ADD CONSTRAINT nota_id_INT UNIQUE (nota_id_INT, tipo_nota_id_INT, corporacao_id_INT)
;
CREATE TABLE pessoa_equipe (
	id int(11)  NOT NULL  AUTO_INCREMENT ,
	empresa_equipe_id_INT int(11) ,
	usuario_id_INT int(11) ,
	pessoa_id_INT int(11) ,
	corporacao_id_INT int(11) ,
	PRIMARY KEY(id),
	 KEY `pessoa_equipe_FK_929412842` (`empresa_equipe_id_INT`), KEY `pessoa_equipe_FK_198364258` (`usuario_id_INT`), KEY `pessoa_equipe_FK_935943604` (`pessoa_id_INT`), KEY `pessoa_equipe_FK_316467285` (`corporacao_id_INT`),
	CONSTRAINT `pessoa_equipe_FK_929412842` FOREIGN KEY (`empresa_equipe_id_INT`) REFERENCES `empresa_equipe` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `pessoa_equipe_FK_198364258` FOREIGN KEY (`usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `pessoa_equipe_FK_935943604` FOREIGN KEY (`pessoa_id_INT`) REFERENCES `pessoa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `pessoa_equipe_FK_316467285` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
;
CREATE TABLE produto (
	id int(11)  NOT NULL  AUTO_INCREMENT ,
	identificador varchar(30) ,
	nome varchar(255)  NOT NULL ,
	nome_normalizado varchar(255)  NOT NULL ,
	descricao varchar(512) ,
	produto_unidade_medida_id_INT int(11) ,
	id_omega_INT int(11) ,
	cadastro_SEC int(10) ,
	cadastro_OFFSEC int(6) ,
	preco_custo_FLOAT double,
	prazo_reposicao_estoque_dias_INT int(6) ,
	codigo_barra char(13) ,
	corporacao_id_INT int(11) ,
	PRIMARY KEY(id),
	 KEY `empresa_produto_FK_183288574` (`corporacao_id_INT`), KEY `produto_FK_485992432` (`produto_unidade_medida_id_INT`),
	CONSTRAINT `produto_FK_485992432` FOREIGN KEY (`produto_unidade_medida_id_INT`) REFERENCES `produto_unidade_medida` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `produto_ibfk_1` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
;
CREATE TABLE produto_tipo (
	id int(11)  NOT NULL  AUTO_INCREMENT ,
	nome varchar(100)  NOT NULL ,
	nome_normalizado varchar(100)  NOT NULL ,
	corporacao_id_INT int(11) ,
	PRIMARY KEY(id),
	 KEY `empresa_produto_tipo_FK_926544190` (`corporacao_id_INT`),
	CONSTRAINT `produto_tipo_ibfk_1` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
;
ALTER TABLE produto_tipo ADD CONSTRAINT nome_98765 UNIQUE (nome, corporacao_id_INT)
;
CREATE TABLE produto_tipos (
	id int(11)  NOT NULL  AUTO_INCREMENT ,
	produto_tipo_id_INT int(11) ,
	produto_id_INT int(11) ,
	corporacao_id_INT int(11) ,
	PRIMARY KEY(id),
	 KEY `produto_tipos_FK_90423584` (`produto_tipo_id_INT`), KEY `produto_tipos_FK_811035157` (`produto_id_INT`), KEY `produto_tipos_FK_529571533` (`corporacao_id_INT`),
	CONSTRAINT `produto_tipos_FK_90423584` FOREIGN KEY (`produto_tipo_id_INT`) REFERENCES `produto_tipo` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `produto_tipos_FK_811035157` FOREIGN KEY (`produto_id_INT`) REFERENCES `produto` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `produto_tipos_FK_529571533` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
;
CREATE TABLE produto_unidade_medida (
	id int(11)  NOT NULL  AUTO_INCREMENT ,
	nome varchar(100)  NOT NULL ,
	abreviacao varchar(10)  NOT NULL ,
	is_float_BOOLEAN int(1) ,
	PRIMARY KEY(id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
;
CREATE TABLE recebivel (
	id int(11)  NOT NULL  AUTO_INCREMENT ,
	valor_FLOAT double,
	devedor_empresa_id_INT int(11) ,
	devedor_pessoa_id_INT int(11) ,
	cadastro_usuario_id_INT int(11) ,
	cadastro_SEC int(10) ,
	cadastro_OFFSEC int(6) ,
	valor_pagamento_FLOAT double,
	pagamento_SEC int(10) ,
	pagamento_OFFSEC int(6) ,
	recebedor_usuario_id_INT int(11) ,
	relatorio_id_INT int(11) ,
	protocolo_INT bigint(20) ,
	corporacao_id_INT int(11) ,
	PRIMARY KEY(id),
	 KEY `recebivel_FK_239410400` (`devedor_empresa_id_INT`), KEY `recebivel_FK_19348144` (`devedor_pessoa_id_INT`), KEY `recebivel_FK_829437256` (`cadastro_usuario_id_INT`), KEY `recebivel_FK_988525391` (`recebedor_usuario_id_INT`), KEY `recebivel_FK_1007080` (`relatorio_id_INT`), KEY `recebivel_FK_595886231` (`corporacao_id_INT`),
	CONSTRAINT `recebivel_FK_239410400` FOREIGN KEY (`devedor_empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `recebivel_FK_19348144` FOREIGN KEY (`devedor_pessoa_id_INT`) REFERENCES `pessoa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `recebivel_FK_829437256` FOREIGN KEY (`cadastro_usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `recebivel_FK_988525391` FOREIGN KEY (`recebedor_usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `recebivel_FK_1007080` FOREIGN KEY (`relatorio_id_INT`) REFERENCES `relatorio` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `recebivel_FK_595886231` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
;
CREATE TABLE recebivel_cotidiano (
	id int(11)  NOT NULL  AUTO_INCREMENT ,
	recebedor_empresa_id_INT int(11) ,
	cadastro_usuario_id_INT int(11) ,
	id_tipo_despesa_recebivel_INT int(3) ,
	parametro_INT int(11) ,
	parametro_OFFSEC int(10) ,
	parametro_SEC int(6) ,
	parametro_json varchar(512) ,
	valor_FLOAT double,
	data_limite_cotidiano_SEC int(10) ,
	data_limite_cotidiano_OFFSEC int(6) ,
	corporacao_id_INT int(11) ,
	PRIMARY KEY(id),
	 KEY `recebivel_cotidiano_FK_447082519` (`recebedor_empresa_id_INT`), KEY `recebivel_cotidiano_FK_452484131` (`cadastro_usuario_id_INT`), KEY `recebivel_cotidiano_FK_900634766` (`corporacao_id_INT`),
	CONSTRAINT `recebivel_cotidiano_FK_447082519` FOREIGN KEY (`recebedor_empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `recebivel_cotidiano_FK_452484131` FOREIGN KEY (`cadastro_usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `recebivel_cotidiano_FK_900634766` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
;
CREATE TABLE registro (
	id int(11)  NOT NULL  AUTO_INCREMENT ,
	protocolo_INT int(11) ,
	cadastro_SEC int(10) ,
	cadastro_OFFSEC int(6) ,
	responsavel_usuario_id_INT int(11) ,
	responsavel_categoria_permissao_id_INT int(11) ,
	descricao varchar(100) ,
	tipo_registro_id_INT int(11) ,
	registro_estado_id_INT int(11) ,
	registro_estado_corporacao_id_INT int(11) ,
	id_origem_chamada_INT int(3) ,
	corporacao_id_INT int(11) ,
	PRIMARY KEY(id),
	 KEY `registro_FK_991607667` (`responsavel_usuario_id_INT`), KEY `registro_FK_883850098` (`responsavel_categoria_permissao_id_INT`), KEY `registro_FK_435150146` (`tipo_registro_id_INT`), KEY `registro_FK_886230469` (`registro_estado_id_INT`), KEY `registro_FK_225860595` (`registro_estado_corporacao_id_INT`), KEY `registro_FK_979919434` (`corporacao_id_INT`),
	CONSTRAINT `registro_FK_991607667` FOREIGN KEY (`responsavel_usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `registro_FK_883850098` FOREIGN KEY (`responsavel_categoria_permissao_id_INT`) REFERENCES `categoria_permissao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `registro_FK_435150146` FOREIGN KEY (`tipo_registro_id_INT`) REFERENCES `tipo_registro` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `registro_FK_886230469` FOREIGN KEY (`registro_estado_id_INT`) REFERENCES `registro_estado` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `registro_FK_225860595` FOREIGN KEY (`registro_estado_corporacao_id_INT`) REFERENCES `registro_estado_corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `registro_FK_979919434` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
;
CREATE TABLE registro_estado (
	id int(11)  NOT NULL  AUTO_INCREMENT ,
	nome varchar(50) ,
	responsavel_usuario_id_INT int(11) ,
	responsavel_categoria_permissao_id_INT int(11) ,
	prazo_dias_INT int(3) ,
	tipo_registro_id_INT int(11) ,
	PRIMARY KEY(id),
	 KEY `registro_estado_FK_27282714` (`responsavel_usuario_id_INT`), KEY `registro_estado_FK_131805420` (`responsavel_categoria_permissao_id_INT`), KEY `registro_estado_FK_634155274` (`tipo_registro_id_INT`),
	CONSTRAINT `registro_estado_FK_27282714` FOREIGN KEY (`responsavel_usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `registro_estado_FK_131805420` FOREIGN KEY (`responsavel_categoria_permissao_id_INT`) REFERENCES `categoria_permissao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `registro_estado_FK_634155274` FOREIGN KEY (`tipo_registro_id_INT`) REFERENCES `tipo_registro` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
;
CREATE TABLE registro_estado_corporacao (
	id int(11)  NOT NULL  AUTO_INCREMENT ,
	nome varchar(50) ,
	responsavel_usuario_id_INT int(11) ,
	responsavel_categoria_permissao_id_INT int(11) ,
	prazo_dias_INT int(3) ,
	tipo_registro_id_INT int(11) ,
	corporacao_id_INT int(11) ,
	PRIMARY KEY(id),
	 KEY `registro_estado_corporacao_FK_488037109` (`responsavel_usuario_id_INT`), KEY `registro_estado_corporacao_FK_697784424` (`responsavel_categoria_permissao_id_INT`), KEY `registro_estado_corporacao_FK_794616700` (`tipo_registro_id_INT`), KEY `registro_estado_corporacao_FK_896209717` (`corporacao_id_INT`),
	CONSTRAINT `registro_estado_corporacao_FK_488037109` FOREIGN KEY (`responsavel_usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `registro_estado_corporacao_FK_697784424` FOREIGN KEY (`responsavel_categoria_permissao_id_INT`) REFERENCES `categoria_permissao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `registro_estado_corporacao_FK_794616700` FOREIGN KEY (`tipo_registro_id_INT`) REFERENCES `tipo_registro` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `registro_estado_corporacao_FK_896209717` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
;
CREATE TABLE registro_fluxo_estado_corporacao (
	id int(11)  NOT NULL  AUTO_INCREMENT ,
	origem_registro_estado_corporacao_id_INT int(11) ,
	origem_registro_estado_id_INT int(11) ,
	destino_registro_estado_corporacao_id_INT int(11) ,
	destino_registro_estado_id_INT int(11) ,
	template_JSON varchar(512) ,
	corporacao_id_INT int(11) ,
	PRIMARY KEY(id),
	 KEY `registro_fluxo_estado_corporacao_FK_25970459` (`origem_registro_estado_corporacao_id_INT`), KEY `registro_fluxo_estado_corporacao_FK_785644532` (`origem_registro_estado_id_INT`), KEY `registro_fluxo_estado_corporacao_FK_761993408` (`destino_registro_estado_corporacao_id_INT`), KEY `registro_fluxo_estado_corporacao_FK_918395997` (`destino_registro_estado_id_INT`), KEY `registro_fluxo_estado_corporacao_FK_13153076` (`corporacao_id_INT`),
	CONSTRAINT `registro_fluxo_estado_corporacao_FK_25970459` FOREIGN KEY (`origem_registro_estado_corporacao_id_INT`) REFERENCES `registro_estado_corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `registro_fluxo_estado_corporacao_FK_785644532` FOREIGN KEY (`origem_registro_estado_id_INT`) REFERENCES `registro_estado` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `registro_fluxo_estado_corporacao_FK_761993408` FOREIGN KEY (`destino_registro_estado_corporacao_id_INT`) REFERENCES `registro_estado_corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `registro_fluxo_estado_corporacao_FK_918395997` FOREIGN KEY (`destino_registro_estado_id_INT`) REFERENCES `registro_estado` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `registro_fluxo_estado_corporacao_FK_13153076` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
;
CREATE TABLE tag (
	id int(11)  NOT NULL  AUTO_INCREMENT ,
	nome varchar(30) ,
	corporacao_id_INT int(11) ,
	PRIMARY KEY(id),
	 KEY `tag_FK_198974609` (`corporacao_id_INT`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
;
CREATE TABLE tarefa_cotidiano (
	id int(11)  NOT NULL  AUTO_INCREMENT ,
	criado_pelo_usuario_id_INT int(11) ,
	categoria_permissao_id_INT int(11) ,
	veiculo_id_INT int(11) ,
	usuario_id_INT int(11) ,
	empresa_equipe_id_INT int(11) ,
	origem_pessoa_id_INT int(11) ,
	origem_empresa_id_INT int(11) ,
	origem_logradouro varchar(255) ,
	origem_numero varchar(30) ,
	origem_cidade_id_INT int(11) ,
	origem_latitude_INT int(6) ,
	origem_longitude_INT int(6) ,
	origem_latitude_real_INT int(6) ,
	origem_longitude_real_INT int(6) ,
	destino_pessoa_id_INT int(11) ,
	destino_empresa_id_INT int(11) ,
	destino_logradouro varchar(255) ,
	destino_numero varchar(30) ,
	destino_cidade_id_INT int(11) ,
	destino_latitude_INT int(6) ,
	destino_longitude_INT int(6) ,
	destino_latitude_real_INT int(6) ,
	destino_longitude_real_INT int(6) ,
	tempo_estimado_carro_INT int(11) ,
	tempo_estimado_a_pe_INT int(11) ,
	distancia_estimada_carro_INT int(11) ,
	distancia_estimada_a_pe_INT int(11) ,
	is_realizada_a_pe_BOOLEAN int(1) ,
	inicio_hora_programada_SEC int(10) ,
	inicio_hora_programada_OFFSEC int(6) ,
	inicio_SEC int(10) ,
	inicio_OFFSEC int(6) ,
	fim_SEC int(10) ,
	fim_OFFSEC int(6) ,
	cadastro_SEC int(10) ,
	cadastro_OFFSEC int(6) ,
	titulo varchar(100) ,
	titulo_normalizado varchar(100) ,
	descricao varchar(512) ,
	corporacao_id_INT int(11) ,
	atividade_id_INT int(11) ,
	id_estado_INT int(4) ,
	id_prioridade_INT int(4) ,
	parametro_SEC int(10) ,
	parametro_OFFSEC int(6) ,
	parametro_json varchar(512) ,
	data_limite_cotidiano_SEC int(10) ,
	data_limite_cotidiano_OFFSEC int(6) ,
	id_tipo_cotidiano_INT int(4) ,
	prazo_SEC int(10) ,
	prazo_OFFSEC int(6) ,
	percentual_completo_INT int(2) ,
	ultima_geracao_SEC int(10) ,
	ultima_geracao_OFFSEC int(6) ,
	relatorio_id_INT int(11) ,
	PRIMARY KEY(id),
	 KEY `tarefa_FK_490081787` (`origem_cidade_id_INT`), KEY `tarefa_FK_618682861` (`destino_cidade_id_INT`), KEY `tarefa_FK_90545654` (`corporacao_id_INT`), KEY `tabela_FK_100` (`usuario_id_INT`), KEY `tarefa_FK_529296875` (`criado_pelo_usuario_id_INT`), KEY `tarefa_FK_70098877` (`categoria_permissao_id_INT`), KEY `tarefa_FK_751892090` (`veiculo_id_INT`), KEY `tarefa_FK_718139649` (`origem_pessoa_id_INT`), KEY `tarefa_FK_466461182` (`origem_empresa_id_INT`), KEY `tarefa_FK_517883301` (`destino_pessoa_id_INT`), KEY `tarefa_FK_919769288` (`destino_empresa_id_INT`), KEY `tarefa_cotidiano_FK_718597412` (`empresa_equipe_id_INT`), KEY `tarefa_cotidiano_FK_316833496` (`atividade_id_INT`), KEY `tarefa_cotidiano_FK_442840576` (`relatorio_id_INT`),
	CONSTRAINT `tarefa_cotidiano_ibfk_8` FOREIGN KEY (`criado_pelo_usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `tarefa_cotidiano_ibfk_10` FOREIGN KEY (`categoria_permissao_id_INT`) REFERENCES `categoria_permissao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `tarefa_cotidiano_ibfk_1` FOREIGN KEY (`veiculo_id_INT`) REFERENCES `veiculo` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `tarefa_cotidiano_ibfk_4` FOREIGN KEY (`usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `tarefa_cotidiano_FK_718597412` FOREIGN KEY (`empresa_equipe_id_INT`) REFERENCES `empresa_equipe` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `tarefa_cotidiano_ibfk_11` FOREIGN KEY (`origem_pessoa_id_INT`) REFERENCES `pessoa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `tarefa_cotidiano_ibfk_6` FOREIGN KEY (`origem_empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `tarefa_cotidiano_ibfk_5` FOREIGN KEY (`origem_cidade_id_INT`) REFERENCES `cidade` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `tarefa_cotidiano_ibfk_7` FOREIGN KEY (`destino_pessoa_id_INT`) REFERENCES `pessoa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `tarefa_cotidiano_ibfk_3` FOREIGN KEY (`destino_empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `tarefa_cotidiano_ibfk_9` FOREIGN KEY (`destino_cidade_id_INT`) REFERENCES `cidade` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `tarefa_cotidiano_ibfk_2` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `tarefa_cotidiano_FK_316833496` FOREIGN KEY (`atividade_id_INT`) REFERENCES `atividade` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
	CONSTRAINT `tarefa_cotidiano_FK_442840576` FOREIGN KEY (`relatorio_id_INT`) REFERENCES `relatorio` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
;
CREATE TABLE tarefa_cotidiano_item (
	id int(11)  NOT NULL  AUTO_INCREMENT ,
	descricao varchar(255) ,
	corporacao_id_INT int(11) ,
	seq_INT int(11) ,
	latitude_INT int(6) ,
	longitude_INT int(6) ,
	tarefa_cotidiano_id_INT int(11) ,
	PRIMARY KEY(id),
	 KEY `tarefa_cotidiano_item_FK_381195068` (`corporacao_id_INT`), KEY `tarefa_cotidiano_item_FK_630126953` (`tarefa_cotidiano_id_INT`),
	CONSTRAINT `tarefa_cotidiano_item_FK_381195068` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
	CONSTRAINT `tarefa_cotidiano_item_FK_630126953` FOREIGN KEY (`tarefa_cotidiano_id_INT`) REFERENCES `tarefa_cotidiano` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
;
CREATE TABLE tarefa_item (
	id int(11)  NOT NULL  AUTO_INCREMENT ,
	descricao varchar(255) ,
	data_conclusao_SEC int(10) ,
	data_conclusao_OFFSEC int(6) ,
	tarefa_id_INT int(11) ,
	cadastro_SEC int(10) ,
	cadastro_OFFSEC int(6) ,
	criado_pelo_usuario_id_INT int(11) ,
	executada_pelo_usuario_id_INT int(11) ,
	corporacao_id_INT int(11) ,
	seq_INT int(11) ,
	latitude_INT int(6) ,
	longitude_INT int(6) ,
	latitude_real_INT int(6) ,
	longitude_real_INT int(6) ,
	PRIMARY KEY(id),
	 KEY `tarefa_item_FK_979400635` (`tarefa_id_INT`), KEY `tarefa_item_FK_803283692` (`criado_pelo_usuario_id_INT`), KEY `tarefa_item_FK_903411866` (`executada_pelo_usuario_id_INT`), KEY `tarefa_item_FK_176757812` (`corporacao_id_INT`),
	CONSTRAINT `tarefa_item_FK_979400635` FOREIGN KEY (`tarefa_id_INT`) REFERENCES `tarefa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `tarefa_item_FK_803283692` FOREIGN KEY (`criado_pelo_usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `tarefa_item_FK_903411866` FOREIGN KEY (`executada_pelo_usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `tarefa_item_FK_176757812` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
;
CREATE TABLE tarefa_relatorio (
	id int(11)  NOT NULL  AUTO_INCREMENT ,
	tarefa_id_INT int(11) ,
	relatorio_id_INT int(11) ,
	corporacao_id_INT int(11) ,
	PRIMARY KEY(id),
	 KEY `tarefa_relatorio_FK_286987304` (`tarefa_id_INT`), KEY `tarefa_relatorio_FK_220855713` (`relatorio_id_INT`), KEY `tarefa_relatorio_FK_496887207` (`corporacao_id_INT`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
;
CREATE TABLE tarefa_tag (
	id int(11)  NOT NULL  AUTO_INCREMENT ,
	tarefa_id_INT int(11) ,
	tag_id_INT int(11) ,
	corporacao_id_INT int(11) ,
	PRIMARY KEY(id),
	 KEY `tarefa_tag_FK_665161133` (`tarefa_id_INT`), KEY `tarefa_tag_FK_816802979` (`tag_id_INT`), KEY `tarefa_tag_FK_330139160` (`corporacao_id_INT`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
;
CREATE TABLE tipo_canal_envio (
	id int(11)  NOT NULL  AUTO_INCREMENT ,
	nome varchar(100) ,
	PRIMARY KEY(id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
;
CREATE TABLE tipo_nota (
	id int(11)  NOT NULL  AUTO_INCREMENT ,
	nome varchar(100) ,
	corporacao_id_INT int(11) ,
	PRIMARY KEY(id),
	 KEY `tipo_nota_FK_962524415` (`corporacao_id_INT`),
	CONSTRAINT `tipo_nota_FK_962524415` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
;
CREATE TABLE tipo_registro (
	id int(11)  NOT NULL  AUTO_INCREMENT ,
	nome varchar(100) ,
	PRIMARY KEY(id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
;
CREATE TABLE tipo_relatorio (
	id int(11)  NOT NULL  AUTO_INCREMENT ,
	nome varchar(100) ,
	PRIMARY KEY(id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
;
CREATE TABLE tipo_tarefa (
	id int(11)  NOT NULL  AUTO_INCREMENT ,
	nome varchar(100) ,
	PRIMARY KEY(id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
;
CREATE TABLE tipo_veiculo_registro (
	id int(11)  NOT NULL  AUTO_INCREMENT ,
	nome varchar(100) ,
	PRIMARY KEY(id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
;
CREATE TABLE usuario_horario_trabalho (
	id int(11)  NOT NULL  AUTO_INCREMENT ,
	hora_inicio_TIME time,
	hora_fim_TIME time,
	data_SEC int(10) ,
	data_OFFSEC int(6) ,
	dia_semana_INT int(2) ,
	empresa_id_INT int(11) ,
	usuario_id_INT int(11) ,
	template_JSON varchar(255) ,
	corporacao_id_INT int(11) ,
	PRIMARY KEY(id),
	 KEY `key_hora478455` (`corporacao_id_INT`), KEY `usuario_horario_trabalho_FK_788391114` (`empresa_id_INT`), KEY `usuario_horario_trabalho_FK_905120850` (`usuario_id_INT`),
	CONSTRAINT `usuario_horario_trabalho_FK_788391114` FOREIGN KEY (`empresa_id_INT`) REFERENCES `empresa` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `usuario_horario_trabalho_FK_905120850` FOREIGN KEY (`usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `usuario_horario_trabalho_ibfk_2` FOREIGN KEY (`corporacao_id_INT`) REFERENCES `corporacao` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
;
DROP TABLE empresa_produto_foto;
;
ALTER TABLE empresa_produto_tipo
	DROP KEY nome_98765
;
DROP TABLE empresa_produto_tipo;
;
DROP TABLE empresa_produto_unidade_medida;
;
DROP TABLE empresa_servico;
;
DROP TABLE empresa_servico_compra;
;
DROP TABLE empresa_servico_foto;
;
ALTER TABLE empresa_servico_tipo
	DROP KEY nome_098787
;
DROP TABLE empresa_servico_tipo;
;
DROP TABLE empresa_servico_unidade_medida;
;
DROP TABLE empresa_servico_venda;
;
DROP TABLE empresa_tipo_venda;
;
DROP TABLE empresa_tipo_venda_parcela;
;
DROP TABLE horario_trabalho_pessoa_empresa;
;
DROP TABLE pessoa_empresa_equipe;
;
DROP TABLE sistema_produto_log_erro;
;
DROP TABLE tarefa_acao;
;
