/*

 Arquivo gerado atrav�s de gerador de c�digo em 25/09/2017 as 14:49:05.
 Para que o arquivo n�o seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

 Tabela correspondente: pessoa
 Sobrescrita de dados: ---SOBRESCRITA_NAO_AUTORIZADA---

 */
omegaApp.controller('formPessoaController',
    ['$scope', '$rootScope', '$element', '$window', '$interval', '$timeout', 'dataService', 'Upload',
     function ($scope, $rootScope, $element, $window, $interval, $timeout, dataService, Upload)
     {
         var vm = this;
         var generatedTasks = omegaApp.generatedFormPessoaController($scope, $rootScope, $element, $window, $interval, $timeout, dataService, Upload, vm);
         try
         {
             //remover chamada caso queira sobrescrever carregamento de dados inicial
             ApplicationUtil.runGeneratedTasks(generatedTasks);

             vm.requiredFields = {
                 fieldIdentificador: false,
                 fieldNome: false,
                 fieldTipoDocumentoId: false,
                 fieldNumeroDocumento: false,
                 fieldIsConsumidorBoolean: false,
                 fieldSexoId: false,
                 fieldTelefone: false,
                 fieldCelular: false,
                 fieldOperadoraId: false,
                 fieldCelularSms: false,
                 fieldEmail: true,
                 fieldLogradouro: false,
                 fieldNumero: false,
                 fieldComplemento: false,
                 fieldCidadeId: false,
                 fieldBairroId: false
             };

             vm.openBairroIdModal = function ()
             {
                 var modalIndex = $rootScope.getNextModalIndex();
                 $rootScope.registerModalInstance($rootScope.showModalDialog('forms', 'Bairro', modalIndex));

                 //atualizando valores da lista
                 var unregisterModal = $rootScope.$watch(
                     function ()
                     {
                         return $rootScope.uibModalInstance[modalIndex]
                     },
                     function ()
                     {
                         if ($rootScope.uibModalInstance[modalIndex] == null)
                         {
                             if (!GeneralUtil.isNullOrEmptyArray(vm.formData.cidadeId))
                             {
                                 vm.getListOfBairroId(vm.formData.cidadeId.id);
                             }

                             vm.formData.bairroId = null;
                             unregisterModal();
                         }

                     });

             };

             vm.getListOfBairroId = function (cidadeId)
             {
                 if (!vm.isInitialLoadFinished)
                 {
                     return;
                 }

                 dataService.getListOfBairroForPessoaForm(cidadeId).then(function (returnContent)
                 {
                     if (RemoteDataUtil.validateRemoteResponse(returnContent))
                     {
                         var remoteDataSet = RemoteDataUtil.getResponseObjectDataSetOrEmptyArray(returnContent);
                         vm.fieldsParameters.fieldBairroId.remoteData = remoteDataSet;
                     }
                     else
                     {
                         LogUtil.logRemoteResponseError(returnContent);
                     }
                 });
             };

             $scope.$watch(
                 //express�o do Watch
                 function ()
                 {
                     return vm.formData.cidadeId
                 },
                 //callback em caso de altera��o
                 function ()
                 {
                     if (!vm.isInitialLoadFinished)
                     {
                         return;
                     }

                     if (!GeneralUtil.isNullOrEmpty(vm.formData.cidadeId))
                     {
                         vm.getListOfBairroId(vm.formData.cidadeId.id);
                     }
                     vm.formData.bairroId = null;
                 }
             );

         }
         catch (ex)
         {
             ApplicationUtil.showErrorMessage(ex.message, 'Erro no controller formPessoaController', vm);
         }
     }]
);