<?php

/*
*
* -------------------------------------------------------
* NOME DO FORMUL�RIO: veiculo
* DATA DE GERA��O:    25.06.2012
* ARQUIVO:            veiculo.php
* TABELA MYSQL:       veiculo
* BANCO DE DADOS:     ponto_eletronico
* -------------------------------------------------------
*
*/

if (isset($_GET["contador"]))
{

    $numeroRegistroInterno = Helper::GET("contador");
}

if (isset($identificadorRelacionamento) && is_numeric($identificadorRelacionamento))
{

    $objVeiculo = new EXTDAO_Veiculo();
    $objVeiculo->select($identificadorRelacionamento);
}
else
{

    $objVeiculo = new EXTDAO_Veiculo();
}

$objArgVeiculo = new Generic_Argument();
$objVeiculo->formatarParaExibicao();

?>

<input type="hidden" name="veiculo_id_<?= $numeroRegistroInterno ?>" id="veiculo_id_<?= $numeroRegistroInterno ?>"
       value="<?= $objVeiculo->getId(); ?>">

<table class="tabela_form">

    <tr class="tr_form">


        <?

        $objArgVeiculo = new Generic_Argument();

        $objArgVeiculo->numeroDoRegistro = "";
        $objArgVeiculo->label = $objVeiculo->label_placa;
        $objArgVeiculo->valor = $objVeiculo->getPlaca();
        $objArgVeiculo->classeCss = "input_text";
        $objArgVeiculo->classeCssFocus = "focus_text";
        $objArgVeiculo->obrigatorio = false;
        $objArgVeiculo->largura = 200;
        $objArgVeiculo->nome = "veiculo_placa_{$numeroRegistroInterno}";
        $objArgVeiculo->id = "veiculo_placa_{$numeroRegistroInterno}";

        ?>

        <td class="td_form_label"><?= $objArgVeiculo->getLabel() ?></td>
        <td class="td_form_campo"><?= $objVeiculo->campoTexto($objArgVeiculo); ?></td>


        <?

        $objArgVeiculo = new Generic_Argument();

        $objArgVeiculo->numeroDoRegistro = "";
        $objArgVeiculo->label = $objVeiculo->label_corporacao_id_INT;
        $objArgVeiculo->valor = $objVeiculo->getCorporacao_id_INT();
        $objArgVeiculo->classeCss = "input_text";
        $objArgVeiculo->classeCssFocus = "focus_text";
        $objArgVeiculo->obrigatorio = false;
        $objArgVeiculo->largura = 200;
        $objArgVeiculo->nome = "veiculo_corporacao_id_INT_{$numeroRegistroInterno}";
        $objArgVeiculo->id = "veiculo_corporacao_id_INT_{$numeroRegistroInterno}";

        $objVeiculo->addInfoCampos("corporacao_id_INT", $objArgVeiculo->label, "TEXTO", $objArgVeiculo->obrigatorio);

        ?>

        <td class="td_form_label"><?= $objArgVeiculo->getLabel() ?></td>
        <td class="td_form_campo">
            <?= $objVeiculo->objCorporacao->getComboBox($objArgVeiculo); ?>
        </td>


    </tr>
    <tr>
        <td colspan="4" class="td_botao_remover_da_lista"><input class="botoes_form" type="button"
                                                                 value="Remover Ve�culo"
                                                                 onclick="javascript:removerDivAjaxEmLista(this);"></td>
    </tr>
</table><br/>

