export class ElementoVisualizacao
{
    public tipo :ElementoVisualizacao.Tipo;
    public descricao :string;
    public constructor(tipo :ElementoVisualizacao.Tipo, descricao :string)
    {
        this.tipo = tipo;
        this.descricao = descricao;
    }

    public static getLista() :Array<ElementoVisualizacao>
    {
        return [

            new ElementoVisualizacao('empresa', "Empresas"),
            new ElementoVisualizacao('ponto', "Pontos Eletrônicos"),
            new ElementoVisualizacao('usuario', "Usuários"),
            new ElementoVisualizacao('rua', "Ruas"),
            new ElementoVisualizacao('estrada', "Estradas"),
            new ElementoVisualizacao('estabelecimento', "Estabelecimentos")

        ];
        
    }

}

export module ElementoVisualizacao
{
    export type Tipo = 'empresa' | 'ponto' | 'usuario' | 'pessoa' | 'rua' | 'estrada' | 'estabelecimento'
}

