"use strict";
exports.__esModule = true;
/// <reference path="../__referencias/workoffline.d.ts" />
var ReactDOM = require("react-dom");
var ContextoGenerico = (function () {
    function ContextoGenerico(domRootSelector) {
        //define o root selector
        this.domRootSelector = domRootSelector;
        this.ajaxRequestCounter = 0;
        this.ajaxProcessingRequests = new Array();
        this.pathToApllicationRoot = "../";
    }
    ContextoGenerico.contextFactory = function () {
    };
    ContextoGenerico.prototype.getDomRootSelector = function () {
        return this.domRootSelector;
    };
    ContextoGenerico.prototype.getPathToApplicationRoot = function () {
        return this.pathToApllicationRoot;
    };
    ContextoGenerico.prototype.setPathToApplicationRoot = function (path) {
        this.pathToApllicationRoot = path;
    };
    ContextoGenerico.prototype.addLoadMask = function () {
        HtmlUtil.mostrarLoading();
    };
    ContextoGenerico.prototype.removeLoadMask = function () {
        HtmlUtil.esconderLoading();
    };
    ContextoGenerico.prototype.renderReactComponent = function (element, containerSelector) {
        return ReactDOM.render(element, document.querySelector(this.domRootSelector + " " + containerSelector));
    };
    return ContextoGenerico;
}());
exports.ContextoGenerico = ContextoGenerico;
