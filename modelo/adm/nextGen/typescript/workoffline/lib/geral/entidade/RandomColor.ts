import { Color } from "lib/geral";

export class RandomColor
{
    public mainColor: Color;
    public foregroundColor: Color;

    public constructor(includeAlpha? :boolean)
    {
        this.mainColor;
        this.foregroundColor;

        let randomRed = RandomColor.getRandomChannelValue();
        let randomGreen = RandomColor.getRandomChannelValue();
        let randomBlue = RandomColor.getRandomChannelValue();

        if(includeAlpha)
        {
            let randomAlpha = RandomColor.getRandomChannelValue();         
            this.mainColor = new Color(randomRed, randomGreen, randomBlue, randomAlpha);       
        }
        else
        {
            this.mainColor = new Color(randomRed, randomGreen, randomBlue);
        }
        
        //cor principal escura
        if (0.299 * randomRed +
            0.587 * randomGreen +
            0.114 * randomBlue <= 128) 
        { 
            this.foregroundColor = new Color(255, 255, 255);
        }
        //cor principal clara
        else
        {
            this.foregroundColor = new Color(0, 0, 0);
        }

    }

    public static getRandomChannelValue() :number
    {
        return Math.round(Math.random() * 255);
    }

    private convertToHex(r :number, g :number, b :number, a? :number)
    {
        let pieces = new Array<string>();

        let redHex = r.toString(16);
        pieces.push(redHex.length == 1 ? "0" + redHex : redHex);

        let greenHex = g.toString(16);
        pieces.push(greenHex.length == 1 ? "0" + greenHex : greenHex);

        let blueHex = b.toString(16);
        pieces.push(blueHex.length == 1 ? "0" + blueHex : blueHex);

        if(a != null)
        {
            let alphaHex = a.toString(16);
            pieces.push(alphaHex.length == 1 ? "0" + alphaHex : alphaHex);
        }

        return `#${pieces.join('')}`;

    }

}