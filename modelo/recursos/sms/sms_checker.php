<?php

include_once '../php/constants.php';
include_once '../php/funcoes.php';
include_once 'sms_config.php';

if (isset($_GET["cliente"])) {

    $cliente = $_GET["cliente"];
    $porta = $arrPortas[$cliente];

    if (strlen($porta)) {

        exec("omegasms -teste=true -porta={$porta}", $saida, $codigoSaida);

        if (count($saida) > 0) {

            if (substr_count($saida[0], "OK") > 0) {

                echo "true";

            } else {

                $saida[0] = str_replace("ERRO - ", "", $saida[0]);

                echo "false: erro na comunicação";

            }

        }

    } else {

        echo "false: porta inválida";

    }

} else {

    echo "false: cliente inválido";

}

?>
