import 'react-dates/initialize';
import 'react-dates/lib/css/_datepicker.css';

import * as React from "react";
import { Modal, FormGroup, ControlLabel, FormControl, HelpBlock, Button } from 'react-bootstrap';
import { DateRangePicker, FocusedInputShape } from 'react-dates';
import { Moment } from 'moment';

import { ContextoGenerico } from 'lib/contexto';
import { ReactProps } from 'lib/geral';
import { ReactComponent } from 'lib/uiComponente';

export class ExportarDados extends ReactComponent<ExportarDados.ExportarDadosProps, ExportarDados.ExportarDadosState>
{
    public dataInicialSelecionada?: Moment;
    public dataFinalSelecionada?: Moment;

    protected constructor(props: ExportarDados.ExportarDadosProps, reactContext: any)
    {
        super(props || {} as ExportarDados.ExportarDadosProps, reactContext);

        this.show = this.show.bind(this);
        this.close = this.close.bind(this);            
        this.state = { show : false, };
    }

    public close() :void 
    {
        this.setState({ show: false });
    }
    
    public show() :void
    {
        this.setState({ show: true });
    }

    public atualizarDatasPadrao() :void
    {
        this.setState({ datasAtualizadas: true });
    }

    public onExportarDadosClick() :void
    {
        this.props.onExportarDadosClick(this.dataInicialSelecionada, this.dataFinalSelecionada);
    }
    
    private getValidationState(): "none" | "block"
    {
        return (!this.dataInicialSelecionada || !this.dataFinalSelecionada) ? "block" : "none";
    }
    
    public render()
    {
        return (
            <Modal
                show={this.state.show} 
                onHide={this.close} 
            >
                <Modal.Header closeButton>
                    Selecione o período desejado
                </Modal.Header>
                <Modal.Body>

                    <DateRangePicker
                        startDateId="start-date"
                        endDateId="end-date"
                        startDate={this.props.dataInicial} 
                        endDate={this.props.dataFinal}
                        onDatesChange={({ startDate, endDate }) => {this. dataInicialSelecionada=startDate; this.dataFinalSelecionada=endDate; } }
                        focusedInput={this.state.campoComFoco}
                        onFocusChange={(input) => this.setState({campoComFoco: input})}                     
                    />
                    
                    <HelpBlock style={ { display: this.getValidationState() } } >Preencha o período para exportação dos dados</HelpBlock>

                </Modal.Body>
                <Modal.Footer>
                    <Button 
                        bsStyle="success"
                        onClick={this.onExportarDadosClick.bind(this)}>
                        Exportar
                    </Button>
                    <Button 
                        bsStyle="danger"
                        onClick={this.close.bind(this)}>
                        Cancelar
                    </Button>                    
                </Modal.Footer>
            </Modal>
        );
    }

}

export namespace ExportarDados
{
    export interface ExportarDadosProps extends ReactProps
    {
        dataInicial?: Moment;
        dataFinal?: Moment;
        onExportarDadosClick: (dataInicial :Moment, dataFinal :Moment) => any;
    }

    export interface ExportarDadosState
    {
        show: boolean,
        datasAtualizadas?: boolean,
        campoComFoco? :FocusedInputShape     
    }
    
}