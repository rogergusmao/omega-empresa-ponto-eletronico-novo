<?php
/*
 *
 * -------------------------------------------------------
 * NOME DO FORMUL�RIO: pessoa_empresa
 * DATA DE GERA��O:    02.07.2012
 * ARQUIVO:            pessoa_empresa.php
 * TABELA MYSQL:       pessoa_empresa
 * BANCO DE DADOS:     ponto_eletronico
 * -------------------------------------------------------
 *
 */

if (isset($_GET["contador"]))
{

    $numeroRegistroInterno = Helper::GET("contador");
}

if (isset($identificadorRelacionamento) && is_numeric($identificadorRelacionamento))
{

    $objPessoa_empresa = new EXTDAO_Pessoa_empresa();
    $objPessoa_empresa->select($identificadorRelacionamento);
}
else
{

    $objPessoa_empresa = new EXTDAO_Pessoa_empresa();
}

$objArgPessoa_empresa = new Generic_Argument();
$objPessoa_empresa->formatarParaExibicao();
?>

<input type="hidden" name="pessoa_empresa_id_<?= $numeroRegistroInterno ?>"
       id="pessoa_empresa_id_<?= $numeroRegistroInterno ?>" value="<?= $objPessoa_empresa->getId(); ?>">

<table class="tabela_form">

    <tr class="tr_form form-empresa-pessoa">

        <?
        $objArgPessoa_empresa = new Generic_Argument();

        $objArgPessoa_empresa->numeroDoRegistro = "";
        $objArgPessoa_empresa->label = $objPessoa_empresa->label_empresa_id_INT;
        $objArgPessoa_empresa->valor = $objPessoa_empresa->getEmpresa_id_INT();
        $objArgPessoa_empresa->classeCss = "input_text empresa";
        $objArgPessoa_empresa->classeCssFocus = "focus_text";
        $objArgPessoa_empresa->obrigatorio = true;
        $objArgPessoa_empresa->largura = 400;
        $objArgPessoa_empresa->nome = "pessoa_empresa_empresa_id_INT_{$numeroRegistroInterno}";
        $objArgPessoa_empresa->id = "pessoa_empresa_empresa_id_INT_{$numeroRegistroInterno}";
        $objArgPessoa_empresa->funcaoGetOptions = "getOptionsMinhaEmpresaAndEmpresasParceiras";

        $objPessoa_empresa->addInfoCampos("empresa_id_INT", $objArgPessoa_empresa->label, "TEXTO", $objArgPessoa_empresa->obrigatorio);
        ?>

        <td class="td_form_label"><?= $objArgPessoa_empresa->getLabel() ?></td>
        <td class="td_form_campo">
            <?= $objPessoa_empresa->campoTexto($objArgPessoa_empresa); ?>
            <? //$objPessoa_empresa->objEmpresa->getComboBox($objArgPessoa_empresa); ?>
        </td>

        <?

        $objArgPessoa_empresa = new Generic_Argument();

        $objArgPessoa_empresa->numeroDoRegistro = "";
        $objArgPessoa_empresa->label = "Profiss�o/Cargo";
        $objArgPessoa_empresa->valor = $objPessoa_empresa->getProfissao_id_INT();
        $objArgPessoa_empresa->classeCss = "input_text profissao";
        $objArgPessoa_empresa->classeCssFocus = "focus_text";
        $objArgPessoa_empresa->obrigatorio = true;
        $objArgPessoa_empresa->largura = 200;
        $objArgPessoa_empresa->nome = "pessoa_empresa_profissao_id_INT_{$numeroRegistroInterno}";
        $objArgPessoa_empresa->id = "pessoa_empresa_profissao_id_INT_{$numeroRegistroInterno}";

        //$objPessoa_empresa->addInfoCampos("profissao_id_INT", $objArgPessoa_empresa->label, "TEXTO", $objArgPessoa_empresa->obrigatorio);
        ?>

        <td class="td_form_label"><?= $objArgPessoa_empresa->getLabel() ?></td>
        <td class="td_form_campo">
            <?= $objPessoa_empresa->campoTexto($objArgPessoa_empresa); ?>
            <? //$objPessoa_empresa->objProfissao->getComboBox($objArgPessoa_empresa); ?>
        </td>


    </tr>
    <tr>
        <td colspan="4" class="td_botao_remover_da_lista"><input class="botoes_form" type="button"
                                                                 value="Remover Profiss�o/Cargo"
                                                                 onclick="javascript:removerDivAjaxEmLista(this);"></td>
    </tr>
</table><br/>

<script type="text/javascript">

    $(document).ready(function ()
    {

        var seletorEmpresa = '<?="pessoa_empresa_empresa_id_INT_{$numeroRegistroInterno}"?>';
        var seletorProfissao = '<?="pessoa_empresa_profissao_id_INT_{$numeroRegistroInterno}"?>';
        var $empresa = $('#'.seletorEmpresa);
        var $profissao = $('#'.seletorProfissao);

        $empresa.autocomplete({
            source: nomesEmpresas
        });
        $profissao.autocomplete({
            source: nomesProfissoes
        });
    });


</script>