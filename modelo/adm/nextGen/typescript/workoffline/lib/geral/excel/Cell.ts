export class Cell
{
    private currentCol :number;
    private currentRow :number; 
    
    public constructor(initialCol :number, initialRow :number)
    {
        this.currentCol = initialCol;
        this.currentRow = initialRow;
    }
    
    public moveCol(step :number = 1) :Cell
    {
        this.currentCol += step;
        return this;
    }

    public moveRow(step :number = 1) :Cell
    {
        this.currentRow += step;
        return this;
    }

    public getRow()
    {
        return this.currentRow;
    }

    public getCol()
    {
        return this.currentCol;            
    }

    public setRow(rowIndex :number)
    {
        this.currentRow = rowIndex;
    }

    public setCol(colIndex :number)
    {
        this.currentCol = colIndex;
    } 

    public getObject()
    {
        return {r: this.currentRow, c: this.currentCol};
    }

    public getString()
    {
        let initial = "A".charCodeAt(0);
        return String.fromCharCode(initial + this.currentCol) + (this.currentRow + 1).toString();
    }

    public clone()
    {
        return new Cell(this.currentCol, this.currentRow);
    }

}