<?php

class EXTDAO_Sistema_sequencia
{
    private static $instance = null;
    private $objBanco = null;

    private function __construct()
    {

        //inicia objeto criando nova conex�o (link) com o banco de dados
        //ir� persistir durante toodo o tempo da request
        $this->objBanco = new Database(
            false,
            false,
            false,
            false,
            false,
            true);
    }

    public static function getInstance()
    {

        if (is_null(self::$instance))
        {

            self::$instance = new self();
        }

        return self::$instance;
    }

    public static function gerarId($nomeTabela, $totGerar = 1)
    {

        return self::getInstance()->gerarIdInterna($nomeTabela, $totGerar);
    }

    public function gerarIdInterna($nomeTabela, $totGerar = 1)
    {

        try{
            $msg = $this->objBanco->queryMensagem("select nextval('{$nomeTabela}', $totGerar)");

            if($msg != null && $msg->erroBancoForaDoAr()){
                return EXTDAO_Sistema_sequencia::VALOR_INVALIDO;
            }else if ($msg != null && $msg->erro())
            {
                throw new SequenciaException("Falha ao realizar update da sequ�ncia '$nomeTabela'.", SequenciaException::THROW_FALHA_UPDATE_SEQUENCIA);
            }
            else
            {
                $ultimoId = $this->objBanco->getPrimeiraTuplaDoResultSet(0);
                if($ultimoId==null || $ultimoId == 0){
                    $idn = rand(1, 999);

                    $call = mysqli_prepare($this->objBanco->getLink(), "CALL initNextval(?, ?, @id{$idn})");

                    //Type specification chars
                    //Character	Description
                    //i	corresponding variable has type integer
                    //d	corresponding variable has type double
                    //s	corresponding variable has type string
                    //b	corresponding variable is a blob and will be sent in packets
                    mysqli_stmt_bind_param($call, 'si', $nomeTabela, $totGerar);
                    mysqli_stmt_execute($call);

                    $select = mysqli_query($this->objBanco->getLink(), "SELECT @id{$idn}");

                    //$msg = $this->objBanco->checkLinkState("[v3e5rfg65frg]");
//                    if($msg != null && $msg->erroBancoForaDoAr()){
//                        return EXTDAO_Sistema_sequencia::VALOR_INVALIDO;
//                    }
//                    else if ($msg != null && $msg->erro())
//                    {
//                        throw new SequenciaException("Falha ao realizar update da sequ�ncia '$nomeTabela'.", SequenciaException::THROW_FALHA_UPDATE_SEQUENCIA);
//                    }

                    $result = mysqli_fetch_assoc($select);
                    $ultimoId = $result["@id{$idn}"];

                    $select->close();
                }

                return $ultimoId;
            }
        }catch(Exception $ex){
            HelperLog::logErro($ex);
            throw new SequenciaException("Falha ao realizar update da sequ�ncia.", SequenciaException::THROW_FALHA_UPDATE_SEQUENCIA);
        }

    }

    function closeDatabase()
    {
        if ($this->objBanco != null)
        {
            $this->objBanco->close();
        }
    }

    public static function close()
    {
        if (self::$instance != null)
        {
            self::$instance->closeDatabase();
        }
    }
}

