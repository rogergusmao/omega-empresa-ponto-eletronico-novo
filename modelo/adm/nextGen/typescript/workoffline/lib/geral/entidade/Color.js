"use strict";
exports.__esModule = true;
var geral_1 = require("lib/geral");
var Color = (function () {
    function Color(redChannelOrHex, greenChannel, blueChannel, alphaChannel) {
        if (redChannelOrHex instanceof String) {
            var channels = Color.convertFromHex(redChannelOrHex);
            if (channels != null) {
                this.redChannel = channels.r;
                this.greenChannel = channels.g;
                this.blueChannel = channels.b;
                this.alphaChannel = channels.a;
            }
        }
        else {
            this.redChannel = redChannelOrHex;
            this.greenChannel = greenChannel;
            this.blueChannel = blueChannel;
            this.alphaChannel = alphaChannel || null;
        }
    }
    Color.prototype.getHexValue = function () {
        if (this.alphaChannel != null) {
            return Color.convertToHex(this.redChannel, this.greenChannel, this.blueChannel, this.alphaChannel);
        }
        else {
            return Color.convertToHex(this.redChannel, this.greenChannel, this.blueChannel);
        }
    };
    Color.convertToHex = function (r, g, b, a) {
        var pieces = new Array();
        var redHex = r.toString(16);
        pieces.push(redHex.length == 1 ? "0" + redHex : redHex);
        var greenHex = g.toString(16);
        pieces.push(greenHex.length == 1 ? "0" + greenHex : greenHex);
        var blueHex = b.toString(16);
        pieces.push(blueHex.length == 1 ? "0" + blueHex : blueHex);
        if (a != null) {
            var alphaHex = a.toString(16);
            pieces.push(alphaHex.length == 1 ? "0" + alphaHex : alphaHex);
        }
        return "#" + pieces.join('');
    };
    Color.convertFromHex = function (hexValue) {
        if (geral_1.Helper.isNullOrEmpty(hexValue)) {
            return null;
        }
        else {
            if (!hexValue.startsWith('#')) {
                hexValue = "#" + hexValue;
            }
            if (hexValue.length == 5) {
                //exemplo: "03FC" para "0033FFCC"
                var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])([a-f\d])$/i;
                hexValue = hexValue.replace(shorthandRegex, function (m, r, g, b, a) {
                    return r + r + g + g + b + b + a + a;
                });
            }
            else if (hexValue.length == 4) {
                //exemplo: "03FC" para "0033FFCC"
                var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
                hexValue = hexValue.replace(shorthandRegex, function (m, r, g, b) {
                    return r + r + g + g + b + b;
                });
            }
            if (hexValue.length == 9) {
                var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hexValue);
                return result ? {
                    r: parseInt(result[1], 16),
                    g: parseInt(result[2], 16),
                    b: parseInt(result[3], 16),
                    a: parseInt(result[4], 16)
                } : null;
            }
            else if (hexValue.length == 7) {
                var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hexValue);
                return result ? {
                    r: parseInt(result[1], 16),
                    g: parseInt(result[2], 16),
                    b: parseInt(result[3], 16),
                    a: null
                } : null;
            }
            else {
                return null;
            }
        }
    };
    return Color;
}());
exports.Color = Color;
