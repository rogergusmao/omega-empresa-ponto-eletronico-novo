import * as React from 'react';
import { Modal } from 'react-bootstrap';

import { PainelFlutuante } from 'lib/uiComponente';
import { ContextoGenerico } from 'lib/contexto';
import { PontoEletronico } from 'aplicacao/modulo/mapa/model';
import { Helper } from 'aplicacao/comum/geral';

export class DetalhesPontoEletronico extends PainelFlutuante<DetalhesPontoEletronico.DetalhesPontoEletronicoProps>
{
    public setDefaultBody() :void
    {
        let modelPontoEletronico = this.props.modelPontoEletronico;
        this.bodyContent = (           
            <div className="corpo-modal-detalhes">   
                <p><b>Pessoa:</b>{modelPontoEletronico.nomePessoa}</p>                  
                <p><b>Tipo:</b> {Helper.getTipoPontoAsString(modelPontoEletronico.tipoPonto)}</p>
                <p><b>Data do Ponto:</b> {DateUtil.getFormattedDateTime(modelPontoEletronico.dataPonto)}</p>    
            </div>                           
        );

    }
    
    public setDefaultHeader(): void
    {
        this.headerContent = (
            <Modal.Title id="contained-modal-title-lg">
                Ponto Eletrônico
            </Modal.Title>
        );
    }

}

export namespace DetalhesPontoEletronico
{
    export interface DetalhesPontoEletronicoProps extends PainelFlutuante.PainelFlutuanteProps
    {
        modelPontoEletronico: PontoEletronico;
    }
}