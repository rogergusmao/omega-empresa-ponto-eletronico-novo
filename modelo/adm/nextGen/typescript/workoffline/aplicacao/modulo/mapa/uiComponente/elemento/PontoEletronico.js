"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var uiComponente_1 = require("lib/uiComponente");
var uiFactory_1 = require("aplicacao/modulo/mapa/uiFactory");
var geral_1 = require("aplicacao/comum/geral");
var PontoEletronico = (function (_super) {
    __extends(PontoEletronico, _super);
    function PontoEletronico(contexto, pontoEletronico) {
        var _this = _super.call(this, contexto, pontoEletronico.latitude, pontoEletronico.longitude, 'ponto') || this;
        _this.modelPontoEletronico = pontoEletronico;
        return _this;
    }
    PontoEletronico.prototype.setDefaultIconeObject = function () {
        switch (this.modelPontoEletronico.tipoPonto) {
            case geral_1.TipoPontoEletronico.PONTO_ENTRADA:
                _super.prototype.setDefaultIconeObject.call(this, this.contexto.getDefaultMarkerFolder() + "map_entrance.png");
                break;
            case geral_1.TipoPontoEletronico.PONTO_SAIDA:
                _super.prototype.setDefaultIconeObject.call(this, this.contexto.getDefaultMarkerFolder() + "map_exit.png");
                break;
        }
    };
    PontoEletronico.prototype.addDefaultClickListener = function () {
        var _this = this;
        _super.prototype.addClickListener.call(this, function () {
            var janelaModal = uiFactory_1.ModalFactory.getModalDetalhesPontoEletronico(_this.contexto, _this.modelPontoEletronico);
            janelaModal.handleShow();
        });
    };
    return PontoEletronico;
}(uiComponente_1.Marcador));
exports.PontoEletronico = PontoEletronico;
