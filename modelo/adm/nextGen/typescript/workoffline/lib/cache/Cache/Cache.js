"use strict";
exports.__esModule = true;
var Factory_1 = require("./Backend/Factory");
var Cache = (function () {
    function Cache(type, config) {
        if (Cache._instance) {
            throw new Error("Error: Instantiation failed: Use Cache.getInstance() instead of new.");
        }
        this.backendCache = Factory_1.Cache_Backend_Factory.factory(type, config);
        Cache._instance = this;
    }
    Cache.getInstance = function (type, config) {
        if (Cache._instance === null) {
            Cache._instance = new Cache(type, config);
        }
        return Cache._instance;
    };
    // Add cache record to the backend cache
    Cache.prototype.add = function (key, data, tagKey) {
        return this.backendCache.add(key, data, tagKey);
    };
    // Get the cache record from the backend cache, if available
    Cache.prototype.get = function (key, tagKey) {
        return this.backendCache.get(key, tagKey);
    };
    // Remove cache record from the backend cache, if available
    Cache.prototype.remove = function (key, tagKey) {
        return this.backendCache.remove(key, tagKey);
    };
    // Add cache record to the backend cache
    Cache.prototype.clear = function (tagKey) {
        return this.backendCache.clear(tagKey);
    };
    return Cache;
}());
Cache._instance = null;
exports.Cache = Cache;
