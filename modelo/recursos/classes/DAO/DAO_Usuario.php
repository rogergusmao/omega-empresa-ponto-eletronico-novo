<?php



class DAO_Usuario extends Generic_DAO
                        {
                            const NOME_ENTIDADE = "usuario";

                            public static $nomeTabela = null;
                            public static $idSistemaTabela = false;
                            public static $databaseFieldsRelatedAttributes = null;
	                        public static $databaseFieldTypes = null;
	                        public static $databaseFieldNames = null;
	                        public static $listAliasTypes = null;
	                        public static $listAliasRelatedAttributes = null;
	                        
	                        public $id;
public $nome;
public $nomeNormalizado;
public $email;
public $senha;
public $statusBoolean;
public $paginaInicial;
public $cadastroSec;
public $cadastroOffsec;

public $labelId;
public $labelNome;
public $labelNomeNormalizado;
public $labelEmail;
public $labelSenha;
public $labelStatusBoolean;
public $labelPaginaInicial;
public $labelCadastroSec;
public $labelCadastroOffsec;

public static $arrPK = array("id");

public function __construct($configDAO = null)
        {
            parent::__construct($configDAO);
            static::__setStaticValues($this->database);           
        }

public static function idSistemaTabela($databaseObject = null){
            if(!(static::$idSistemaTabela))
            {
                static::$idSistemaTabela = EXTDAO_Sistema_tabela::getIdDaSistemaTabela(static::nomeTabela(), $databaseObject);
            }
            return static::$idSistemaTabela;
        }

public function getIdSistemaTabela(){
            return static::idSistemaTabela();
        }

public static function nomeTabela(){
            if(is_null(static::$nomeTabela))
            {
                static::$nomeTabela = "usuario";
            }
            return static::$nomeTabela;
        }

public function getNomeTabela(){
            return static::nomeTabela();
        }

public static function __setStaticValues($databaseObject = null)
                   {    
                        static::nomeTabela();
                        
                        static::idSistemaTabela($databaseObject);
                   
                        static::setDatabaseFieldNames();
                        static::setDatabaseFieldTypes();
                        static::setDatabaseFieldsRelatedAttributeNames();
                        
                        static::setListAliasTypes();
                        static::setListAliasRelatedAttributes();
                   }

public function getExpressaoLabel()
                   {
                        return "{id}";
                   }

public static function setDatabaseFieldNames()
        {
            if(is_null(static::$databaseFieldNames))
            {
                static::$databaseFieldNames = new stdClass();
                static::$databaseFieldNames->id = "id";
static::$databaseFieldNames->nome = "nome";
static::$databaseFieldNames->nomeNormalizado = "nome_normalizado";
static::$databaseFieldNames->email = "email";
static::$databaseFieldNames->senha = "senha";
static::$databaseFieldNames->statusBoolean = "status_BOOLEAN";
static::$databaseFieldNames->paginaInicial = "pagina_inicial";
static::$databaseFieldNames->cadastroSec = "cadastro_SEC";
static::$databaseFieldNames->cadastroOffsec = "cadastro_OFFSEC";

            }
                      
        }

public static function setDatabaseFieldTypes()
        {
            if(is_null(static::$databaseFieldTypes))
            {
                static::$databaseFieldTypes = new stdClass();
                static::$databaseFieldTypes->id = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->nome = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->nomeNormalizado = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->email = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->senha = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->statusBoolean = static::TIPO_VARIAVEL_BOOLEAN;
static::$databaseFieldTypes->paginaInicial = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->cadastroSec = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$databaseFieldTypes->cadastroOffsec = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;

            }
            
        }

public static function setDatabaseFieldsRelatedAttributeNames()
        {
            if(is_null(static::$databaseFieldsRelatedAttributes))
            {
                static::$databaseFieldsRelatedAttributes = new stdClass();
                static::$databaseFieldsRelatedAttributes->id = "id";
static::$databaseFieldsRelatedAttributes->nome = "nome";
static::$databaseFieldsRelatedAttributes->nome_normalizado = "nomeNormalizado";
static::$databaseFieldsRelatedAttributes->email = "email";
static::$databaseFieldsRelatedAttributes->senha = "senha";
static::$databaseFieldsRelatedAttributes->status_BOOLEAN = "statusBoolean";
static::$databaseFieldsRelatedAttributes->pagina_inicial = "paginaInicial";
static::$databaseFieldsRelatedAttributes->cadastro_SEC = "cadastroSec";
static::$databaseFieldsRelatedAttributes->cadastro_OFFSEC = "cadastroOffsec";

            }           
        }

public function setAllFieldsToHumanFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["nome"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["nome_normalizado"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["email"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["senha"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["status_BOOLEAN"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["pagina_inicial"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["cadastro_SEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["cadastro_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
}

public function setAllFieldsToDatabaseFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["nome"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["nome_normalizado"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["email"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["senha"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["status_BOOLEAN"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["pagina_inicial"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["cadastro_SEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["cadastro_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
}



public function setLabels()
        {$this->labelId = "id";
$this->labelNome = "nome";
$this->labelNomeNormalizado = "nomenormalizado";
$this->labelEmail = "email";
$this->labelSenha = "senha";
$this->labelStatusBoolean = "statusBOOLEAN";
$this->labelPaginaInicial = "paginainicial";
$this->labelCadastroSec = "";
$this->labelCadastroOffsec = "";
}

public function __actionAdd($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                $this->setByObject($parameters);
                $this->setDataCadastroAndOffsetInSecondsFromUser($parameters);
                
                $this->formatarParaSQL();
                $msg = $this->insert(true);
                
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                return new Mensagem(null, I18N::getExpression("Usuario adicionado com sucesso."));
                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionEdit($parameters = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try {

                $this->setByObject($parameters);
                $this->setAllFieldsToHumanFormat();
                
                $this->formatarParaSQL();
                $msg = $this->updateByInstanceUsingParameters($this->id, $parameters, true);
                if($msg != null && $msg->erro())
                {
                     return $msg;
                }
        
                return new Mensagem(null, I18N::getExpression("Usuario editado com sucesso."));
            }
            catch(Exception $ex)
            {                
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionRemove($parameters = null)
        {
            $successulRemovalCount = 0;
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                foreach($parameters as $record)
                {
                    if(is_numeric($record->id))
                    {
                        $msg = $this->delete($record->id, true);
                        if($msg == null || $msg->ok())
                        {
                            $successulRemovalCount++;
                        }
                    }
                }
                
                if($successulRemovalCount > 0)
                {
                    $successMessage = count($successulRemovalCount) > 1 ? I18N::getExpression("{0} Usuario foram removidos com sucesso.", $successulRemovalCount) : I18N::getExpression("Usuario removido com sucesso.");
                    return new Mensagem(null, $successMessage);
                }
                else
                {
                    $errorMessage = count($parameters) > 1 ? I18N::getExpression("Falha ao remover Usuario.") : I18N::getExpression("Falha ao remover Usuario.");
                    return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, $errorMessage);                    
                }
            }
            catch (Exception $ex) 
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }

public function __getRecord($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }
            
            try
            {            
                $returnObject = new stdClass();            
                $whereClause = static::getWhereClauseForFilter($parameters->filterParameters);
                $query = "SELECT  id, nome, nome_normalizado, email, senha, status_BOOLEAN, pagina_inicial, cadastro_SEC, cadastro_OFFSEC FROM usuario {$whereClause}";
    
                $msg = $this->database->queryMensagem($query);                
                if ($msg == null)
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Não foi encontrado nenhum registro"));
                    }                    
                    $resultSet = $this->database->result;
                    $resultObject = static::mapResultSetDataToRecord($resultSet);
                    
                    $returnObject->formData = $resultObject;
                    $returnObject->comboBoxesData = null;
                    if($parameters->loadComboBoxesData)
                    {
                        $this->setByObject($resultObject);
                        $returnObject->comboBoxesData =  $this->getFormComboBoxesData();
                    }                    
    
                    
    
                    return new Mensagem_generica($returnObject);
                } 
                else 
                {
                    return $msg;
                }
                
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }


        
        public function __getFilterComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
                
        public function __getFormComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
        
        protected function getFormComboBoxesData()
        {
            $comboBoxesData = new stdClass();
            $listParameters = new stdClass();
            $listParameters->listMappingType = static::LIST_MAPPING_TYPE_COMBOBOX;
            
            

            return $comboBoxesData;
        }
        
        


        
        public static function setListAliasTypes()
        {
            if(is_null(static::$listAliasTypes))
            {
                static::$listAliasTypes = new stdClass();
                static::$listAliasTypes->usuario__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->usuario__nome = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->usuario__email = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->usuario__senha = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->usuario__status_BOOLEAN = static::TIPO_VARIAVEL_BOOLEAN;
static::$listAliasTypes->usuario__pagina_inicial = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->usuario__cadastro_SEC = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$listAliasTypes->usuario__cadastro_OFFSEC = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
            }        
        }
        
        public static function setListAliasRelatedAttributes()
        {
            if(is_null(static::$listAliasRelatedAttributes))
            {
                static::$listAliasRelatedAttributes = new stdClass();
                static::$listAliasRelatedAttributes->usuario__id = "id";
static::$listAliasRelatedAttributes->usuario__nome = "nome";
static::$listAliasRelatedAttributes->usuario__email = "email";
static::$listAliasRelatedAttributes->usuario__senha = "senha";
static::$listAliasRelatedAttributes->usuario__status_BOOLEAN = "statusBoolean";
static::$listAliasRelatedAttributes->usuario__pagina_inicial = "paginaInicial";
static::$listAliasRelatedAttributes->usuario__cadastro_SEC = "cadastroSec";
static::$listAliasRelatedAttributes->usuario__cadastro_OFFSEC = "cadastroOffsec";
            }         
        }

        public function __getList($parameters = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try
            {
                $filterParameters = null;
                if(isset($parameters->filterParameters))
                {
                    $filterParameters = $parameters->filterParameters;
                }
                                    
                    
                
                $sortingParameters = null;
                if(isset($parameters->sortingParameters))
                {
                    $sortingParameters = $parameters->sortingParameters;
                }
                
                $paginationParameters = null;
                if(isset($parameters->paginationParameters))
                {
                    $paginationParameters = $parameters->paginationParameters;
                }
                
                $listMappingType = static::getListMappingType($parameters->listMappingType);
                $mainTableAlias = "u";

                $whereClause = static::getWhereClauseForFilter($filterParameters, $mainTableAlias);
                $orderByClause = static::getOrderByClauseForFilter($sortingParameters, $mainTableAlias);
                $limitClause = static::getLimitClauseForFilter($paginationParameters);

                $queryWithoutLimit = "SELECT u.id FROM usuario u {$whereClause}";
                $query = "SELECT u.id AS usuario__id, u.nome AS usuario__nome, u.email AS usuario__email, u.senha AS usuario__senha, u.status_BOOLEAN AS usuario__status_BOOLEAN, u.pagina_inicial AS usuario__pagina_inicial, u.cadastro_SEC AS usuario__cadastro_SEC, u.cadastro_OFFSEC AS usuario__cadastro_OFFSEC FROM usuario u  {$whereClause} {$orderByClause} {$limitClause}";

                $msg = $this->database->queryMensagem($queryWithoutLimit);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                
                $totalNumberOfRecords = $this->database->rows();
                if($totalNumberOfRecords == 0)
                {
                    return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                }                        
                        
                $msg = $this->database->queryMensagem($query);
                
                //operacao realizaca com sucesso
                if ($msg == null) 
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                    }                        
                
                    $resultSet = $this->database->result;

                    $objReturn = new stdClass();
                    $objReturn->dataSet = static::mapResultSetDataToList($resultSet, $listMappingType, static::$listAliasTypes);

                    if(!Helper::isNullOrEmpty($paginationParameters->recordsPerPage) && !Helper::isNullOrEmpty($paginationParameters->currentPage))
                    {
                        $lastPage = ceil($totalNumberOfRecords / $paginationParameters->recordsPerPage);
                        $objReturn->paginationParameters = new PaginationParams($paginationParameters->recordsPerPage, $paginationParameters->currentPage, $lastPage);
                    }
                    
                    return new Mensagem_generica($objReturn);
                } 
                else
                {
                    return $msg;
                }                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }



public function getId()
            {
                return $this->id;
            }

public function getNome()
            {
                return $this->nome;
            }

public function getNomeNormalizado()
            {
                return $this->nomeNormalizado;
            }

public function getNome_normalizado()
                {
                    return $this->nomeNormalizado;
                }

public function getEmail()
            {
                return $this->email;
            }

public function getSenha()
            {
                return $this->senha;
            }

public function getStatusBoolean()
            {
                return $this->statusBoolean;
            }

public function getStatus_BOOLEAN()
                {
                    return $this->statusBoolean;
                }

public function getPaginaInicial()
            {
                return $this->paginaInicial;
            }

public function getPagina_inicial()
                {
                    return $this->paginaInicial;
                }

public function getCadastroSec()
            {
                return $this->cadastroSec;
            }

public function getCadastro_SEC()
                {
                    return $this->cadastroSec;
                }

public function getCadastroOffsec()
            {
                return $this->cadastroOffsec;
            }

public function getCadastro_OFFSEC()
                {
                    return $this->cadastroOffsec;
                }



function setId($value)
            {
                $this->id = $value;
            }

function setNome($value)
            {
                $this->nome = $value;
            }

function setNomeNormalizado($value)
            {
                $this->nomeNormalizado = $value;
            }

public function setNome_normalizado($value)
                { 
                    $this->nomeNormalizado = $value; 
                }

function setEmail($value)
            {
                $this->email = $value;
            }

function setSenha($value)
            {
                $this->senha = $value;
            }

function setStatusBoolean($value)
            {
                $this->statusBoolean = $value;
            }

public function setStatus_BOOLEAN($value)
                { 
                    $this->statusBoolean = $value; 
                }

function setPaginaInicial($value)
            {
                $this->paginaInicial = $value;
            }

public function setPagina_inicial($value)
                { 
                    $this->paginaInicial = $value; 
                }

function setCadastroSec($value)
            {
                $this->cadastroSec = $value;
            }

public function setCadastro_SEC($value)
                { 
                    $this->cadastroSec = $value; 
                }

function setCadastroOffsec($value)
            {
                $this->cadastroOffsec = $value;
            }

public function setCadastro_OFFSEC($value)
                { 
                    $this->cadastroOffsec = $value; 
                }



public function clear()
        {$this->id = null;
$this->nome = null;
$this->nomeNormalizado = null;
$this->email = null;
$this->senha = null;
$this->statusBoolean = null;
$this->paginaInicial = null;
$this->cadastroSec = null;
$this->cadastroOffsec = null;
if(is_array($this->arrEstadosCampos)){
                        unset($this->arrEstadosCampos);
                     $this->arrEstadosCampos = array();
                    }
                }

public function formatarParaSQL()
        {
$this->nomeNormalizado = $this->formatarStringNormalizadaParaComandoSQL($this->nome);
if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->id)){
$this->id = $this->formatarIntegerParaComandoSQL($this->id);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->id);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->nome)){
$this->nome = $this->formatarStringParaComandoSQL($this->nome);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->nome);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->email)){
$this->email = $this->formatarStringParaComandoSQL($this->email);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->email);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->senha)){
$this->senha = $this->formatarStringParaComandoSQL($this->senha);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->senha);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->statusBoolean)){
$this->statusBoolean = $this->formatarBooleanParaComandoSQL($this->statusBoolean);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->statusBoolean);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->paginaInicial)){
$this->paginaInicial = $this->formatarStringParaComandoSQL($this->paginaInicial);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->paginaInicial);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->cadastroSec)){
$this->cadastroSec = $this->formatarIntegerParaComandoSQL($this->cadastroSec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->cadastroSec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->cadastroOffsec)){
$this->cadastroOffsec = $this->formatarIntegerParaComandoSQL($this->cadastroOffsec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->cadastroOffsec);
}

}

public function formatarParaExibicao()
        {if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->nome)){
$this->nome = $this->formatarStringParaExibicao($this->nome);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->nome);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->nomeNormalizado)){
$this->nomeNormalizado = $this->formatarStringParaExibicao($this->nomeNormalizado);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->nomeNormalizado);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->email)){
$this->email = $this->formatarStringParaExibicao($this->email);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->email);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->senha)){
$this->senha = $this->formatarStringParaExibicao($this->senha);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->senha);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->statusBoolean)){
$this->statusBoolean = $this->formatarBooleanParaExibicao($this->statusBoolean);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->statusBoolean);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->paginaInicial)){
$this->paginaInicial = $this->formatarStringParaExibicao($this->paginaInicial);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->paginaInicial);
}

}

function select($id, $idCorporacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }   

            $sql = "SELECT id, nome, nome_normalizado, email, senha, status_BOOLEAN, pagina_inicial, cadastro_SEC, cadastro_OFFSEC FROM usuario WHERE id = $id";

            $msg = $this->database->queryMensagem($sql);
            $result = $this->database->result;

            if($msg != null && $msg->erro() )
            { 
            		$this->clear();
            		return $msg;
        	} 
        	else if($this->database->rows() == 0)
        	{
                return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nenhum registro foi encontrado."));
            }
        

            $row = $this->database->fetchArray(MYSQLI_NUM);
            
            		$this->id = $row[0];
		$this->nome = $row[1];
		$this->nomeNormalizado = $row[2];
		$this->email = $row[3];
		$this->senha = $row[4];
		$this->statusBoolean = $row[5];
		$this->paginaInicial = $row[6];
		$this->cadastroSec = $row[7];
		$this->cadastroOffsec = $row[8];

            
        }

public function delete($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (is_null($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }

            $sql = "DELETE FROM usuario WHERE id= $id";
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                    && $sincronizar
                        && static::idSistemaTabela() != null
                            && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_REMOVE,
                                null,
                                null,
                                $id,
                                $this->database);
            }

            return $msg;
        }

public function insert($sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null, $opcoes = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $this->defineDataCadastroInSecondsIfNotDefined();
$this->defineDataCadastroOffsetInSecondsIfNotDefined();

            
            if (is_null($opcoes))
            {
                $opcoes = $this->opcoesDAO;
            }

            if (!is_numeric($this->id))
            {
                $this->id = "null";
                if ($opcoes == null || $opcoes[Database::OPCAO_GERAR_ID] != false) 
                {
                    $this->id = EXTDAO_Sistema_sequencia::gerarId(static::nomeTabela());                    
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }    
                    else if($msg == null)
                    {
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
                else 
                {
                    $this->id = null;
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao));
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }
                    else if($msg == null)
                    {
                        $this->id = $this->database->getLastInsertId();
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
            }
            else 
            {
                $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }                        
                else if($msg == null)
                {
                   $msg = new Mensagem_token();
                   $msg->mValor = $this->id;
                }
            }
            if($msg == null || $msg->ok())
            {                
                if($sincronizar
                    && static::idSistemaTabela() != null
                    && isset($this->id)){
                    
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_INSERT,
                                null,
                                null,
                                $this->id,
                                $this->database); 
                	}
            } 
            else
            {
                $this->id = null;
            }
            
            return $msg;
            
        }

public function getSQLInsert($idCorporacao){ 

return "INSERT INTO usuario (id, nome, nome_normalizado, email, senha, status_BOOLEAN, pagina_inicial, cadastro_SEC, cadastro_OFFSEC) VALUES ( $this->id ,  $this->nome ,  $this->nomeNormalizado ,  $this->email ,  $this->senha ,  $this->statusBoolean ,  $this->paginaInicial ,  $this->cadastroSec ,  $this->cadastroOffsec ) "; 

}




        
        public function updateByParameters($id, $objParametros, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao))
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();

            
                
                if (isset($objParametros->nome)) 
                {
                    $arrUpdateFields[] = " nome = {$objParametros->nome} ";
                }


                
                if (isset($objParametros->nomeNormalizado)) 
                {
                    $arrUpdateFields[] = " nome_normalizado = {$objParametros->nomeNormalizado} ";
                }


                
                if (isset($objParametros->email)) 
                {
                    $arrUpdateFields[] = " email = {$objParametros->email} ";
                }


                
                if (isset($objParametros->statusBoolean)) 
                {
                    $arrUpdateFields[] = " status_BOOLEAN = {$objParametros->statusBoolean} ";
                }


                
                if (isset($objParametros->paginaInicial)) 
                {
                    $arrUpdateFields[] = " pagina_inicial = {$objParametros->paginaInicial} ";
                }


                
                if (isset($objParametros->cadastroSec)) 
                {
                    $arrUpdateFields[] = " cadastro_SEC = {$objParametros->cadastroSec} ";
                }


                
                if (isset($objParametros->cadastroOffsec)) 
                {
                    $arrUpdateFields[] = " cadastro_OFFSEC = {$objParametros->cadastroOffsec} ";
                }



            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE usuario SET {$strUpdateFields} WHERE id = {$id}";

            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null)
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function update($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($this->nome)) 
                {                                      
                    $arrUpdateFields[] = " nome = {$this->nome} ";
                }


                
                if (isset($this->nomeNormalizado)) 
                {                                      
                    $arrUpdateFields[] = " nome_normalizado = {$this->nomeNormalizado} ";
                }


                
                if (isset($this->email)) 
                {                                      
                    $arrUpdateFields[] = " email = {$this->email} ";
                }


                
                if (isset($this->statusBoolean)) 
                {                                      
                    $arrUpdateFields[] = " status_BOOLEAN = {$this->statusBoolean} ";
                }


                
                if (isset($this->paginaInicial)) 
                {                                      
                    $arrUpdateFields[] = " pagina_inicial = {$this->paginaInicial} ";
                }


                
                if (isset($this->cadastroSec)) 
                {                                      
                    $arrUpdateFields[] = " cadastro_SEC = {$this->cadastroSec} ";
                }


                
                if (isset($this->cadastroOffsec)) 
                {                                      
                    $arrUpdateFields[] = " cadastro_OFFSEC = {$this->cadastroOffsec} ";
                }


            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE usuario SET {$strUpdateFields} WHERE id = {$id}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function updateByInstanceUsingParameters($id, $parameters, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($parameters->nome)) 
                {                                      
                    $arrUpdateFields[] = " nome = {$this->nome} ";
                }
                
                if (isset($parameters->nomeNormalizado)) 
                {                                      
                    $arrUpdateFields[] = " nome_normalizado = {$this->nomeNormalizado} ";
                }
                
                if (isset($parameters->email)) 
                {                                      
                    $arrUpdateFields[] = " email = {$this->email} ";
                }
                
                if (isset($parameters->statusBoolean)) 
                {                                      
                    $arrUpdateFields[] = " status_BOOLEAN = {$this->statusBoolean} ";
                }
                
                if (isset($parameters->paginaInicial)) 
                {                                      
                    $arrUpdateFields[] = " pagina_inicial = {$this->paginaInicial} ";
                }
                
                if (isset($parameters->cadastroSec)) 
                {                                      
                    $arrUpdateFields[] = " cadastro_SEC = {$this->cadastroSec} ";
                }
                
                if (isset($parameters->cadastroOffsec)) 
                {                                      
                    $arrUpdateFields[] = " cadastro_OFFSEC = {$this->cadastroOffsec} ";
                }
            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE usuario SET {$strUpdateFields} WHERE id = {$id}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }

}

?>
