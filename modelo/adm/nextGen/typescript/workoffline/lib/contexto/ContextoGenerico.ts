/// <reference path="../__referencias/workoffline.d.ts" />
import * as ReactDOM from 'react-dom';

export class ContextoGenerico
{
    protected domRootSelector: string;

    public ajaxRequestCounter: number;
    public ajaxProcessingRequests: Array<number>;
    protected pathToApllicationRoot :string;

    public static contextFactory() : void
    {

    }

    public constructor(domRootSelector: string)
    {
        //define o root selector
        this.domRootSelector = domRootSelector;
        this.ajaxRequestCounter = 0;
        this.ajaxProcessingRequests = new Array<number>();
        this.pathToApllicationRoot = "../";
    }

    public getDomRootSelector()
    {
        return this.domRootSelector;
    }

    public getPathToApplicationRoot()
    {
        return this.pathToApllicationRoot;
    }

    public setPathToApplicationRoot(path :string)
    {
        this.pathToApllicationRoot = path;
    }

    public addLoadMask()
    {
        HtmlUtil.mostrarLoading();
    }

    public removeLoadMask()
    {
        HtmlUtil.esconderLoading();
    }

    public renderReactComponent<T extends React.Component>(element: JSX.Element, containerSelector :string) : T
    {
        return ReactDOM.render(element, document.querySelector(`${this.domRootSelector} ${containerSelector}`)) as T;
    }

}