/*

 Arquivo gerado atrav�s de gerador de c�digo em 07/01/2018 as 15:54:13.
 Para que o arquivo n�o seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

 Tabela correspondente: tipo_documento
 Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

 */

returnObject.getTipoDocumento = function (filterParameters)
{

    var parameters = {
        filterParameters: filterParameters,
        loadComboBoxesData: true
    };

    return $http({
        url: baseURLAdm + "/webservice.php?class=EXTDAO_Tipo_documento&action=__getRecord",
        method: "POST",
        data: parameters,
        headers: {
            'Content-Type': 'application/json'
        }
    });

};

returnObject.addTipoDocumento = function (formData)
{

    return $http({
        url: baseURLAdm + "/webservice.php?class=EXTDAO_Tipo_documento&jsonOutput=true&action=add",
        method: "POST",
        data: formData,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    });

};

returnObject.editTipoDocumento = function (formData)
{

    return $http({
        url: baseURLAdm + "/webservice.php?class=EXTDAO_Tipo_documento&jsonOutput=true&action=edit",
        method: "POST",
        data: formData,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    });

};

returnObject.getAllComboBoxesDataForTipoDocumentoForm = function ()
{
    return $http({
        url: baseURLAdm + "/webservice.php?class=EXTDAO_Tipo_documento&jsonOutput=true&action=__getFormComboBoxesData",
        method: "POST",
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    });

};



