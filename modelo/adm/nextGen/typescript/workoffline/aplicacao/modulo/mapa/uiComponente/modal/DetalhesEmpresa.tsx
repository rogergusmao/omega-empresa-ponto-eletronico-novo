import * as React from 'react';
import * as ReactBootstrap from 'react-bootstrap';

import { ContextoGenerico } from 'lib/contexto';
import { PainelFlutuante } from 'lib/uiComponente';
import { Empresa } from 'aplicacao/modulo/mapa/model';

export class DetalhesEmpresa extends PainelFlutuante<DetalhesEmpresa.DetalhesEmpresaProps>
{
    public setDefaultBody() :void
    {
        let modelEmpresa = this.props.modelEmpresa;
        let listItems = modelEmpresa.wifiNetworks.map(function(wifiNetwork) {
            return (
                <p><b>{wifiNetwork.ssid}</b> 
                (De {DateUtil.getFormattedDateTime(wifiNetwork.dataInicio)} a {DateUtil.getFormattedDateTime(wifiNetwork.dataFim)}</p>
            );
            });

        if(listItems.length > 0)
        {
            listItems = [<div className="lista-redes-wifi">Redes Wifi detectadas:</div>, ...listItems];
        }

        this.bodyContent = (
            <div className="corpo-modal-detalhes">              
                <p><b>Nome da Empresa:</b>{modelEmpresa.nomeEmpresa}</p>                 
                <p><b>Identificador da Empresa:</b> {modelEmpresa.idEmpresa}</p>
                <p><b>Nome da Empresa:</b> {modelEmpresa.nomeEmpresa}</p>
                {listItems}  
            </div>                           
        );

    }

}

export namespace DetalhesEmpresa
{
    export interface DetalhesEmpresaProps extends PainelFlutuante.PainelFlutuanteProps
    {
        modelEmpresa: Empresa;
    }
}