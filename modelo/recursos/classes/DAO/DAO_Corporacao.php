<?php


        /*

        Arquivo gerado através de gerador de código em 12/05/2018 as 14:44:55.
        Para que o arquivo não seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

        Tabela correspondente: corporacao
        Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

        */
        


class DAO_Corporacao extends Generic_DAO
                        {
                            const NOME_ENTIDADE = "corporacao";

                            public static $nomeTabela = null;
                            public static $idSistemaTabela = false;
                            public static $databaseFieldsRelatedAttributes = null;
	                        public static $databaseFieldTypes = null;
	                        public static $databaseFieldNames = null;
	                        public static $listAliasTypes = null;
	                        public static $listAliasRelatedAttributes = null;
	                        
	                        public $id;
public $nome;
public $nomeNormalizado;
public $usuarioDropbox;
public $senhaDropbox;

public $labelId;
public $labelNome;
public $labelNomeNormalizado;
public $labelUsuarioDropbox;
public $labelSenhaDropbox;

public static $arrPK = array("id");

public function __construct($configDAO = null)
        {
            parent::__construct($configDAO);
            static::__setStaticValues($this->database);           
        }

public static function idSistemaTabela($databaseObject = null){
            if(!(static::$idSistemaTabela))
            {
                static::$idSistemaTabela = EXTDAO_Sistema_tabela::getIdDaSistemaTabela(static::nomeTabela(), $databaseObject);
            }
            return static::$idSistemaTabela;
        }

public function getIdSistemaTabela(){
            return static::idSistemaTabela();
        }

public static function nomeTabela(){
            if(is_null(static::$nomeTabela))
            {
                static::$nomeTabela = "corporacao";
            }
            return static::$nomeTabela;
        }

public function getNomeTabela(){
            return static::nomeTabela();
        }

public static function __setStaticValues($databaseObject = null)
                   {    
                        static::nomeTabela();
                        
                        static::idSistemaTabela($databaseObject);
                   
                        static::setDatabaseFieldNames();
                        static::setDatabaseFieldTypes();
                        static::setDatabaseFieldsRelatedAttributeNames();
                        
                        static::setListAliasTypes();
                        static::setListAliasRelatedAttributes();
                   }

public function getExpressaoLabel()
                   {
                        return "{id}";
                   }

public static function setDatabaseFieldNames()
        {
            if(is_null(static::$databaseFieldNames))
            {
                static::$databaseFieldNames = new stdClass();
                static::$databaseFieldNames->id = "id";
static::$databaseFieldNames->nome = "nome";
static::$databaseFieldNames->nomeNormalizado = "nome_normalizado";
static::$databaseFieldNames->usuarioDropbox = "usuario_dropbox";
static::$databaseFieldNames->senhaDropbox = "senha_dropbox";

            }
                      
        }

public static function setDatabaseFieldTypes()
        {
            if(is_null(static::$databaseFieldTypes))
            {
                static::$databaseFieldTypes = new stdClass();
                static::$databaseFieldTypes->id = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->nome = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->nomeNormalizado = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->usuarioDropbox = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->senhaDropbox = static::TIPO_VARIAVEL_TEXT;

            }
            
        }

public static function setDatabaseFieldsRelatedAttributeNames()
        {
            if(is_null(static::$databaseFieldsRelatedAttributes))
            {
                static::$databaseFieldsRelatedAttributes = new stdClass();
                static::$databaseFieldsRelatedAttributes->id = "id";
static::$databaseFieldsRelatedAttributes->nome = "nome";
static::$databaseFieldsRelatedAttributes->nome_normalizado = "nomeNormalizado";
static::$databaseFieldsRelatedAttributes->usuario_dropbox = "usuarioDropbox";
static::$databaseFieldsRelatedAttributes->senha_dropbox = "senhaDropbox";

            }           
        }

public function setAllFieldsToHumanFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["nome"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["nome_normalizado"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["usuario_dropbox"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["senha_dropbox"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
}

public function setAllFieldsToDatabaseFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["nome"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["nome_normalizado"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["usuario_dropbox"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["senha_dropbox"] = static::ESTADO_FORMATACAO_CAMPO_DB;
}



public function setLabels()
        {$this->labelId = "id";
$this->labelNome = "nome";
$this->labelNomeNormalizado = "nomenormalizado";
$this->labelUsuarioDropbox = "usuariodropbox";
$this->labelSenhaDropbox = "senhadropbox";
}

public function __actionAdd($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                $this->setByObject($parameters);
                $this->setDataCadastroAndOffsetInSecondsFromUser($parameters);
                
                $this->formatarParaSQL();
                $msg = $this->insert(true);
                
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                return new Mensagem(null, I18N::getExpression("Corporacao adicionado com sucesso."));
                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionEdit($parameters = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try {

                $this->setByObject($parameters);
                $this->setAllFieldsToHumanFormat();
                
                $this->formatarParaSQL();
                $msg = $this->updateByInstanceUsingParameters($this->id, $parameters, true);
                if($msg != null && $msg->erro())
                {
                     return $msg;
                }
        
                return new Mensagem(null, I18N::getExpression("Corporacao editado com sucesso."));
            }
            catch(Exception $ex)
            {                
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionRemove($parameters = null)
        {
            $successulRemovalCount = 0;
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                foreach($parameters as $record)
                {
                    if(is_numeric($record->id))
                    {
                        $msg = $this->delete($record->id, true);
                        if($msg == null || $msg->ok())
                        {
                            $successulRemovalCount++;
                        }
                    }
                }
                
                if($successulRemovalCount > 0)
                {
                    $successMessage = count($successulRemovalCount) > 1 ? I18N::getExpression("{0} Corporacao foram removidos com sucesso.", $successulRemovalCount) : I18N::getExpression("Corporacao removido com sucesso.");
                    return new Mensagem(null, $successMessage);
                }
                else
                {
                    $errorMessage = count($parameters) > 1 ? I18N::getExpression("Falha ao remover Corporacao.") : I18N::getExpression("Falha ao remover Corporacao.");
                    return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, $errorMessage);                    
                }
            }
            catch (Exception $ex) 
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }

public function __getRecord($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }
            
            try
            {            
                $returnObject = new stdClass();            
                $whereClause = static::getWhereClauseForFilter($parameters->filterParameters);
                $query = "SELECT  id, nome, nome_normalizado, usuario_dropbox, senha_dropbox FROM corporacao {$whereClause}";
    
                $msg = $this->database->queryMensagem($query);                
                if ($msg == null)
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Não foi encontrado nenhum registro"));
                    }                    
                    $resultSet = $this->database->result;
                    $resultObject = static::mapResultSetDataToRecord($resultSet);
                    
                    $returnObject->formData = $resultObject;
                    $returnObject->comboBoxesData = null;
                    if($parameters->loadComboBoxesData)
                    {
                        $this->setByObject($resultObject);
                        $returnObject->comboBoxesData =  $this->getFormComboBoxesData();
                    }                    
    
                    
    
                    return new Mensagem_generica($returnObject);
                } 
                else 
                {
                    return $msg;
                }
                
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }


        
        public function __getFilterComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
                
        public function __getFormComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
        
        protected function getFormComboBoxesData()
        {
            $comboBoxesData = new stdClass();
            $listParameters = new stdClass();
            $listParameters->listMappingType = static::LIST_MAPPING_TYPE_COMBOBOX;
            
            

            return $comboBoxesData;
        }
        
        


        
        public static function setListAliasTypes()
        {
            if(is_null(static::$listAliasTypes))
            {
                static::$listAliasTypes = new stdClass();
                static::$listAliasTypes->corporacao__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->corporacao__nome = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->corporacao__usuario_dropbox = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->corporacao__senha_dropbox = static::TIPO_VARIAVEL_TEXT;
            }        
        }
        
        public static function setListAliasRelatedAttributes()
        {
            if(is_null(static::$listAliasRelatedAttributes))
            {
                static::$listAliasRelatedAttributes = new stdClass();
                static::$listAliasRelatedAttributes->corporacao__id = "id";
static::$listAliasRelatedAttributes->corporacao__nome = "nome";
static::$listAliasRelatedAttributes->corporacao__usuario_dropbox = "usuarioDropbox";
static::$listAliasRelatedAttributes->corporacao__senha_dropbox = "senhaDropbox";
            }         
        }

        public function __getList($parameters = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try
            {
                $filterParameters = null;
                if(isset($parameters->filterParameters))
                {
                    $filterParameters = $parameters->filterParameters;
                }
                                    
                    
                
                $sortingParameters = null;
                if(isset($parameters->sortingParameters))
                {
                    $sortingParameters = $parameters->sortingParameters;
                }
                
                $paginationParameters = null;
                if(isset($parameters->paginationParameters))
                {
                    $paginationParameters = $parameters->paginationParameters;
                }
                
                $listMappingType = static::getListMappingType($parameters->listMappingType);
                $mainTableAlias = "c";

                $whereClause = static::getWhereClauseForFilter($filterParameters, $mainTableAlias);
                $orderByClause = static::getOrderByClauseForFilter($sortingParameters, $mainTableAlias);
                $limitClause = static::getLimitClauseForFilter($paginationParameters);

                $queryWithoutLimit = "SELECT c.id FROM corporacao c {$whereClause}";
                $query = "SELECT c.id AS corporacao__id, c.nome AS corporacao__nome, c.usuario_dropbox AS corporacao__usuario_dropbox, c.senha_dropbox AS corporacao__senha_dropbox FROM corporacao c  {$whereClause} {$orderByClause} {$limitClause}";

                $msg = $this->database->queryMensagem($queryWithoutLimit);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                
                $totalNumberOfRecords = $this->database->rows();
                if($totalNumberOfRecords == 0)
                {
                    return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                }                        
                        
                $msg = $this->database->queryMensagem($query);
                
                //operacao realizaca com sucesso
                if ($msg == null) 
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                    }                        
                
                    $resultSet = $this->database->result;

                    $objReturn = new stdClass();
                    $objReturn->dataSet = static::mapResultSetDataToList($resultSet, $listMappingType, static::$listAliasTypes);

                    if(!Helper::isNullOrEmpty($paginationParameters->recordsPerPage) && !Helper::isNullOrEmpty($paginationParameters->currentPage))
                    {
                        $lastPage = ceil($totalNumberOfRecords / $paginationParameters->recordsPerPage);
                        $objReturn->paginationParameters = new PaginationParams($paginationParameters->recordsPerPage, $paginationParameters->currentPage, $lastPage);
                    }
                    
                    return new Mensagem_generica($objReturn);
                } 
                else
                {
                    return $msg;
                }                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }



public function getId()
            {
                return $this->id;
            }

public function getNome()
            {
                return $this->nome;
            }

public function getNomeNormalizado()
            {
                return $this->nomeNormalizado;
            }

public function getNome_normalizado()
                {
                    return $this->nomeNormalizado;
                }

public function getUsuarioDropbox()
            {
                return $this->usuarioDropbox;
            }

public function getUsuario_dropbox()
                {
                    return $this->usuarioDropbox;
                }

public function getSenhaDropbox()
            {
                return $this->senhaDropbox;
            }

public function getSenha_dropbox()
                {
                    return $this->senhaDropbox;
                }



function setId($value)
            {
                $this->id = $value;
            }

function setNome($value)
            {
                $this->nome = $value;
            }

function setNomeNormalizado($value)
            {
                $this->nomeNormalizado = $value;
            }

public function setNome_normalizado($value)
                { 
                    $this->nomeNormalizado = $value; 
                }

function setUsuarioDropbox($value)
            {
                $this->usuarioDropbox = $value;
            }

public function setUsuario_dropbox($value)
                { 
                    $this->usuarioDropbox = $value; 
                }

function setSenhaDropbox($value)
            {
                $this->senhaDropbox = $value;
            }

public function setSenha_dropbox($value)
                { 
                    $this->senhaDropbox = $value; 
                }



public function clear()
        {$this->id = null;
$this->nome = null;
$this->nomeNormalizado = null;
$this->usuarioDropbox = null;
$this->senhaDropbox = null;
if(is_array($this->arrEstadosCampos)){
                        unset($this->arrEstadosCampos);
                     $this->arrEstadosCampos = array();
                    }
                }

public function formatarParaSQL()
        {
$this->nomeNormalizado = $this->formatarStringNormalizadaParaComandoSQL($this->nome);
if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->id)){
$this->id = $this->formatarIntegerParaComandoSQL($this->id);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->id);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->nome)){
$this->nome = $this->formatarStringParaComandoSQL($this->nome);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->nome);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->usuarioDropbox)){
$this->usuarioDropbox = $this->formatarStringParaComandoSQL($this->usuarioDropbox);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->usuarioDropbox);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->senhaDropbox)){
$this->senhaDropbox = $this->formatarStringParaComandoSQL($this->senhaDropbox);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->senhaDropbox);
}

}

public function formatarParaExibicao()
        {if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->nome)){
$this->nome = $this->formatarStringParaExibicao($this->nome);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->nome);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->nomeNormalizado)){
$this->nomeNormalizado = $this->formatarStringParaExibicao($this->nomeNormalizado);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->nomeNormalizado);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->usuarioDropbox)){
$this->usuarioDropbox = $this->formatarStringParaExibicao($this->usuarioDropbox);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->usuarioDropbox);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->senhaDropbox)){
$this->senhaDropbox = $this->formatarStringParaExibicao($this->senhaDropbox);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->senhaDropbox);
}

}

function select($id, $idCorporacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }   

            $sql = "SELECT id, nome, nome_normalizado, usuario_dropbox, senha_dropbox FROM corporacao WHERE id = $id";

            $msg = $this->database->queryMensagem($sql);
            $result = $this->database->result;

            if($msg != null && $msg->erro() )
            { 
            		$this->clear();
            		return $msg;
        	} 
        	else if($this->database->rows() == 0)
        	{
                return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nenhum registro foi encontrado."));
            }
        

            $row = $this->database->fetchArray(MYSQLI_NUM);
            
            		$this->id = $row[0];
		$this->nome = $row[1];
		$this->nomeNormalizado = $row[2];
		$this->usuarioDropbox = $row[3];
		$this->senhaDropbox = $row[4];

            
        }

public function delete($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (is_null($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }

            $sql = "DELETE FROM corporacao WHERE id= $id";
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                    && $sincronizar
                        && static::idSistemaTabela() != null
                            && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_REMOVE,
                                null,
                                null,
                                $id,
                                $this->database);
            }

            return $msg;
        }

public function insert($sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null, $opcoes = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            
            
            if (is_null($opcoes))
            {
                $opcoes = $this->opcoesDAO;
            }

            if (!is_numeric($this->id))
            {
                $this->id = "null";
                if ($opcoes == null || $opcoes[Database::OPCAO_GERAR_ID] != false) 
                {
                    $this->id = EXTDAO_Sistema_sequencia::gerarId(static::nomeTabela());                    
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }    
                    else if($msg == null)
                    {
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
                else 
                {
                    $this->id = null;
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao));
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }
                    else if($msg == null)
                    {
                        $this->id = $this->database->getLastInsertId();
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
            }
            else 
            {
                $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }                        
                else if($msg == null)
                {
                   $msg = new Mensagem_token();
                   $msg->mValor = $this->id;
                }
            }
            if($msg == null || $msg->ok())
            {                
                if($sincronizar
                    && static::idSistemaTabela() != null
                    && isset($this->id)){
                    
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_INSERT,
                                null,
                                null,
                                $this->id,
                                $this->database); 
                	}
            } 
            else
            {
                $this->id = null;
            }
            
            return $msg;
            
        }

public function getSQLInsert($idCorporacao){ 

return "INSERT INTO corporacao (id, nome, nome_normalizado, usuario_dropbox, senha_dropbox) VALUES ( $this->id ,  $this->nome ,  $this->nomeNormalizado ,  $this->usuarioDropbox ,  $this->senhaDropbox ) "; 

}




        
        public function updateByParameters($id, $objParametros, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao))
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();

            
                
                if (isset($objParametros->nome)) 
                {
                    $arrUpdateFields[] = " nome = {$objParametros->nome} ";
                }


                
                if (isset($objParametros->nomeNormalizado)) 
                {
                    $arrUpdateFields[] = " nome_normalizado = {$objParametros->nomeNormalizado} ";
                }


                
                if (isset($objParametros->usuarioDropbox)) 
                {
                    $arrUpdateFields[] = " usuario_dropbox = {$objParametros->usuarioDropbox} ";
                }


                
                if (isset($objParametros->senhaDropbox)) 
                {
                    $arrUpdateFields[] = " senha_dropbox = {$objParametros->senhaDropbox} ";
                }



            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE corporacao SET {$strUpdateFields} WHERE id = {$id}";

            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null)
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function update($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($this->nome)) 
                {                                      
                    $arrUpdateFields[] = " nome = {$this->nome} ";
                }


                
                if (isset($this->nomeNormalizado)) 
                {                                      
                    $arrUpdateFields[] = " nome_normalizado = {$this->nomeNormalizado} ";
                }


                
                if (isset($this->usuarioDropbox)) 
                {                                      
                    $arrUpdateFields[] = " usuario_dropbox = {$this->usuarioDropbox} ";
                }


                
                if (isset($this->senhaDropbox)) 
                {                                      
                    $arrUpdateFields[] = " senha_dropbox = {$this->senhaDropbox} ";
                }


            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE corporacao SET {$strUpdateFields} WHERE id = {$id}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function updateByInstanceUsingParameters($id, $parameters, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($parameters->nome)) 
                {                                      
                    $arrUpdateFields[] = " nome = {$this->nome} ";
                }
                
                if (isset($parameters->nomeNormalizado)) 
                {                                      
                    $arrUpdateFields[] = " nome_normalizado = {$this->nomeNormalizado} ";
                }
                
                if (isset($parameters->usuarioDropbox)) 
                {                                      
                    $arrUpdateFields[] = " usuario_dropbox = {$this->usuarioDropbox} ";
                }
                
                if (isset($parameters->senhaDropbox)) 
                {                                      
                    $arrUpdateFields[] = " senha_dropbox = {$this->senhaDropbox} ";
                }
            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE corporacao SET {$strUpdateFields} WHERE id = {$id}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }

}

?>
