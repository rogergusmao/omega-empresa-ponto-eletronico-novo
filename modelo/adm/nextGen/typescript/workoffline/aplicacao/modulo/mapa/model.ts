import { Empresa } from './model/Empresa';
import { Usuario } from './model/Usuario';
import { WifiNetwork } from './model/WifiNetwork';
import { PontoEletronico } from './model/PontoEletronico';

export
{
    Empresa,
    Usuario,
    WifiNetwork,
    PontoEletronico    
}



