/*

Arquivo gerado atrav�s de gerador de c�digo em 19/01/2018 as 19:58:40.
Para que o arquivo n�o seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

Tabela correspondente: bairro
Sobrescrita de dados: ---SOBRESCRITA_NAO_AUTORIZADA---

*/

omegaApp.controller('formBairroController',

    ['$scope', '$rootScope', '$element', '$window', '$interval', '$timeout', 'dataService', 'Upload',

     function ($scope, $rootScope, $element, $window, $interval, $timeout, dataService, Upload)
     {

         var vm = this;
         var generatedTasks = omegaApp.generatedFormBairroController($scope, $rootScope, $element, $window, $interval, $timeout, dataService, Upload, vm);

         try
         {
             //remover chamada caso queira sobrescrever carregamento de dados inicial  
             ApplicationUtil.runGeneratedTasks(generatedTasks);

         }
         catch (ex)
         {
             ApplicationUtil.showErrorMessage(ex.message, 'Erro no controller formBairroController', vm);
         }

     }]
);