<?php
    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  Database_ponto_eletronico
    * DATA DE GERAÇÃO: 19.02.2018
    * ARQUIVO:         Database_ponto_eletronico.php
    * BANCO DE DADOS:  
    * -------------------------------------------------------
    *
    */
    
    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************
    class Database_ponto_eletronico {
	

    // *************************
    // DECLARAÇÃO DE TODAS AS TABELAS DO BANCO DE DADOS
    // *************************
    public static $tabelas = array ( 
	 "api", 
	 "area_menu", 
	 "corporacao", 
	 "estado_civil", 
	 "operadora", 
	 "perfil", 
	 "permissao", 
	 "produto_unidade_medida", 
	 "servico", 
	 "sexo", 
	 "sistema_estado_lista", 
	 "sistema_parametro_global", 
	 "sistema_projetos_versao", 
	 "sistema_sequencia", 
	 "sistema_tabela", 
	 "sistema_tipo_campo", 
	 "sistema_tipo_download_arquivo", 
	 "sistema_tipo_operacao_banco", 
	 "tag", 
	 "tarefa_relatorio", 
	 "tarefa_tag", 
	 "tela_estado", 
	 "tela_tipo", 
	 "tela_tipo_componente", 
	 "tipo_anexo", 
	 "tipo_canal_envio", 
	 "tipo_cardinalidade", 
	 "tipo_ponto", 
	 "tipo_registro", 
	 "registro_estado", 
	 "tipo_relatorio", 
	 "tipo_tarefa", 
	 "tipo_veiculo_registro", 
	 "usuario", 
	 "atividade_tipo", 
	 "atividade_unidade_medida", 
	 "categoria_permissao", 
	 "forma_pagamento", 
	 "modelo", 
	 "veiculo", 
	 "pais", 
	 "uf", 
	 "cidade", 
	 "bairro", 
	 "produto_tipo", 
	 "profissao", 
	 "rotina", 
	 "sistema_parametro_global_corporacao", 
	 "tipo_documento", 
	 "tipo_empresa", 
	 "tipo_nota", 
	 "usuario_tipo", 
	 "usuario_tipo_menu", 
	 "usuario_tipo_privilegio", 
	 "permissao_categoria_permissao", 
	 "produto", 
	 "produto_tipos", 
	 "sistema_atributo", 
	 "acesso", 
	 
	 "registro_estado_corporacao", 
	 "registro", 
	 "registro_fluxo_estado_corporacao", 
	 "relatorio", 
	 "empresa", 
	 "atividade", 
	 "atividade_tipos", 
	 "empresa_atividade", 
	 "despesa_cotidiano", 
	 "despesa", 
	 "empresa_compra", 
	 "empresa_atividade_compra", 
	 "empresa_compra_parcela", 
	 "empresa_produto_compra", 
	 "empresa_equipe", 
	 "empresa_perfil", 
	 "empresa_produto", 
	 "empresa_venda_template", 
	 "mesa", 
	 "recebivel_cotidiano", 
	 "usuario_empresa", 
	 "usuario_horario_trabalho", 
	 "pessoa", 
	 "api_id", 
	 "empresa_venda", 
	 "empresa_atividade_venda", 
	 "empresa_produto_venda", 
	 "empresa_venda_parcela", 
	 "negociacao_divida", 
	 "negociacao_divida_empresa_venda", 
	 "tarefa", 
	 "tarefa_item", 
	 "mensagem", 
	 "mensagem_envio", 
	 "mesa_reserva", 
	 "nota", 
	 "nota_tipo_nota", 
	 "pessoa_empresa", 
	 "pessoa_empresa_rotina", 
	 "pessoa_equipe", 
	 "pessoa_usuario", 
	 "ponto", 
	 "recebivel", 
	 "tarefa_cotidiano", 
	 "tarefa_cotidiano_item", 
	 "wifi", 
	 "wifi_registro", 
	 "rede", 
	 "rede_empresa", 
	 "relatorio_anexo", 
	 "sistema_lista", 
	 "sistema_campo", 
	 "sistema_campo_atributo", 
	 "sistema_registro_sincronizador", 
	 "tela", 
	 "tela_sistema_lista", 
	 "tela_componente", 
	 "usuario_categoria_permissao", 
	 "usuario_corporacao", 
	 "usuario_foto", 
	 "usuario_foto_configuracao", 
	 "usuario_menu", 
	 "usuario_privilegio", 
	 "usuario_servico", 
	 "usuario_tipo_corporacao", 
	 "veiculo_usuario", 
	 "usuario_posicao", 
	 "veiculo_registro");
        public static $chavesUnicas = null;

        public static function getChaveUnica($tabela){
            if(Database_ponto_eletronico::$chavesUnicas == null){
                Database_ponto_eletronico::$chavesUnicas = array();
                			Database_ponto_eletronico::$chavesUnicas['api'] = null;
			Database_ponto_eletronico::$chavesUnicas['area_menu'] = null;
			Database_ponto_eletronico::$chavesUnicas['corporacao'] = array('nome_normalizado');
			Database_ponto_eletronico::$chavesUnicas['estado_civil'] = array('nome');
			Database_ponto_eletronico::$chavesUnicas['operadora'] = array('nome_normalizado');
			Database_ponto_eletronico::$chavesUnicas['perfil'] = array('nome');
			Database_ponto_eletronico::$chavesUnicas['permissao'] = array('tag');
			Database_ponto_eletronico::$chavesUnicas['produto_unidade_medida'] = null;
			Database_ponto_eletronico::$chavesUnicas['servico'] = array('nome');
			Database_ponto_eletronico::$chavesUnicas['sexo'] = null;
			Database_ponto_eletronico::$chavesUnicas['sistema_estado_lista'] = null;
			Database_ponto_eletronico::$chavesUnicas['sistema_parametro_global'] = null;
			Database_ponto_eletronico::$chavesUnicas['sistema_projetos_versao'] = null;
			Database_ponto_eletronico::$chavesUnicas['sistema_sequencia'] = null;
			Database_ponto_eletronico::$chavesUnicas['sistema_tabela'] = array('nome');
			Database_ponto_eletronico::$chavesUnicas['sistema_tipo_campo'] = null;
			Database_ponto_eletronico::$chavesUnicas['sistema_tipo_download_arquivo'] = array('nome');
			Database_ponto_eletronico::$chavesUnicas['sistema_tipo_operacao_banco'] = array('nome');
			Database_ponto_eletronico::$chavesUnicas['tag'] = null;
			Database_ponto_eletronico::$chavesUnicas['tarefa_relatorio'] = null;
			Database_ponto_eletronico::$chavesUnicas['tarefa_tag'] = null;
			Database_ponto_eletronico::$chavesUnicas['tela_estado'] = null;
			Database_ponto_eletronico::$chavesUnicas['tela_tipo'] = null;
			Database_ponto_eletronico::$chavesUnicas['tela_tipo_componente'] = null;
			Database_ponto_eletronico::$chavesUnicas['tipo_anexo'] = null;
			Database_ponto_eletronico::$chavesUnicas['tipo_canal_envio'] = null;
			Database_ponto_eletronico::$chavesUnicas['tipo_cardinalidade'] = null;
			Database_ponto_eletronico::$chavesUnicas['tipo_ponto'] = null;
			Database_ponto_eletronico::$chavesUnicas['tipo_registro'] = null;
			Database_ponto_eletronico::$chavesUnicas['tipo_relatorio'] = null;
			Database_ponto_eletronico::$chavesUnicas['tipo_tarefa'] = null;
			Database_ponto_eletronico::$chavesUnicas['tipo_veiculo_registro'] = null;
			Database_ponto_eletronico::$chavesUnicas['usuario'] = array('email');
			Database_ponto_eletronico::$chavesUnicas['atividade_tipo'] = array('nome', 'corporacao_id_INT');
			Database_ponto_eletronico::$chavesUnicas['atividade_unidade_medida'] = null;
			Database_ponto_eletronico::$chavesUnicas['categoria_permissao'] = array('nome_normalizado', 'corporacao_id_INT');
			Database_ponto_eletronico::$chavesUnicas['forma_pagamento'] = null;
			Database_ponto_eletronico::$chavesUnicas['modelo'] = array('nome_normalizado', 'ano_INT', 'corporacao_id_INT');
			Database_ponto_eletronico::$chavesUnicas['veiculo'] = array('placa', 'corporacao_id_INT');
			Database_ponto_eletronico::$chavesUnicas['pais'] = array('nome_normalizado', 'corporacao_id_INT');
			Database_ponto_eletronico::$chavesUnicas['uf'] = array('nome_normalizado', 'corporacao_id_INT', 'pais_id_INT');
			Database_ponto_eletronico::$chavesUnicas['cidade'] = array('nome_normalizado', 'uf_id_INT', 'corporacao_id_INT');
			Database_ponto_eletronico::$chavesUnicas['bairro'] = array('nome_normalizado', 'cidade_id_INT', 'corporacao_id_INT');
			Database_ponto_eletronico::$chavesUnicas['produto_tipo'] = array('nome', 'corporacao_id_INT');
			Database_ponto_eletronico::$chavesUnicas['profissao'] = array('nome_normalizado', 'corporacao_id_INT');
			Database_ponto_eletronico::$chavesUnicas['rotina'] = null;
			Database_ponto_eletronico::$chavesUnicas['sistema_parametro_global_corporacao'] = null;
			Database_ponto_eletronico::$chavesUnicas['tipo_documento'] = null;
			Database_ponto_eletronico::$chavesUnicas['tipo_empresa'] = array('nome', 'corporacao_id_INT');
			Database_ponto_eletronico::$chavesUnicas['tipo_nota'] = null;
			Database_ponto_eletronico::$chavesUnicas['usuario_tipo'] = array('nome', 'corporacao_id_INT');
			Database_ponto_eletronico::$chavesUnicas['usuario_tipo_menu'] = null;
			Database_ponto_eletronico::$chavesUnicas['usuario_tipo_privilegio'] = null;
			Database_ponto_eletronico::$chavesUnicas['permissao_categoria_permissao'] = null;
			Database_ponto_eletronico::$chavesUnicas['produto'] = null;
			Database_ponto_eletronico::$chavesUnicas['produto_tipos'] = null;
			Database_ponto_eletronico::$chavesUnicas['sistema_atributo'] = array('nome', 'sistema_tabela_id_INT');
			Database_ponto_eletronico::$chavesUnicas['acesso'] = null;
			Database_ponto_eletronico::$chavesUnicas['registro_estado'] = null;
			Database_ponto_eletronico::$chavesUnicas['registro_estado_corporacao'] = null;
			Database_ponto_eletronico::$chavesUnicas['registro'] = null;
			Database_ponto_eletronico::$chavesUnicas['registro_fluxo_estado_corporacao'] = null;
			Database_ponto_eletronico::$chavesUnicas['relatorio'] = null;
			Database_ponto_eletronico::$chavesUnicas['empresa'] = array('email', 'corporacao_id_INT');
			Database_ponto_eletronico::$chavesUnicas['atividade'] = null;
			Database_ponto_eletronico::$chavesUnicas['atividade_tipos'] = null;
			Database_ponto_eletronico::$chavesUnicas['empresa_atividade'] = null;
			Database_ponto_eletronico::$chavesUnicas['despesa_cotidiano'] = null;
			Database_ponto_eletronico::$chavesUnicas['despesa'] = null;
			Database_ponto_eletronico::$chavesUnicas['empresa_compra'] = null;
			Database_ponto_eletronico::$chavesUnicas['empresa_atividade_compra'] = null;
			Database_ponto_eletronico::$chavesUnicas['empresa_compra_parcela'] = null;
			Database_ponto_eletronico::$chavesUnicas['empresa_produto_compra'] = null;
			Database_ponto_eletronico::$chavesUnicas['empresa_equipe'] = null;
			Database_ponto_eletronico::$chavesUnicas['empresa_perfil'] = array('empresa_id_INT', 'perfil_id_INT', 'corporacao_id_INT');
			Database_ponto_eletronico::$chavesUnicas['empresa_produto'] = null;
			Database_ponto_eletronico::$chavesUnicas['empresa_venda_template'] = null;
			Database_ponto_eletronico::$chavesUnicas['mesa'] = null;
			Database_ponto_eletronico::$chavesUnicas['recebivel_cotidiano'] = null;
			Database_ponto_eletronico::$chavesUnicas['usuario_empresa'] = array('usuario_id_INT', 'empresa_id_INT', 'corporacao_id_INT');
			Database_ponto_eletronico::$chavesUnicas['usuario_horario_trabalho'] = null;
			Database_ponto_eletronico::$chavesUnicas['pessoa'] = array('email', 'corporacao_id_INT');
			Database_ponto_eletronico::$chavesUnicas['api_id'] = null;
			Database_ponto_eletronico::$chavesUnicas['empresa_venda'] = null;
			Database_ponto_eletronico::$chavesUnicas['empresa_atividade_venda'] = null;
			Database_ponto_eletronico::$chavesUnicas['empresa_produto_venda'] = null;
			Database_ponto_eletronico::$chavesUnicas['empresa_venda_parcela'] = null;
			Database_ponto_eletronico::$chavesUnicas['negociacao_divida'] = null;
			Database_ponto_eletronico::$chavesUnicas['negociacao_divida_empresa_venda'] = null;
			Database_ponto_eletronico::$chavesUnicas['tarefa'] = null;
			Database_ponto_eletronico::$chavesUnicas['tarefa_item'] = null;
			Database_ponto_eletronico::$chavesUnicas['mensagem'] = null;
			Database_ponto_eletronico::$chavesUnicas['mensagem_envio'] = null;
			Database_ponto_eletronico::$chavesUnicas['mesa_reserva'] = null;
			Database_ponto_eletronico::$chavesUnicas['nota'] = null;
			Database_ponto_eletronico::$chavesUnicas['nota_tipo_nota'] = array('nota_id_INT', 'tipo_nota_id_INT', 'corporacao_id_INT');
			Database_ponto_eletronico::$chavesUnicas['pessoa_empresa'] = array('pessoa_id_INT', 'empresa_id_INT', 'profissao_id_INT', 'corporacao_id_INT');
			Database_ponto_eletronico::$chavesUnicas['pessoa_empresa_rotina'] = null;
			Database_ponto_eletronico::$chavesUnicas['pessoa_equipe'] = null;
			Database_ponto_eletronico::$chavesUnicas['pessoa_usuario'] = array('pessoa_id_INT', 'usuario_id_INT', 'corporacao_id_INT');
			Database_ponto_eletronico::$chavesUnicas['ponto'] = null;
			Database_ponto_eletronico::$chavesUnicas['recebivel'] = null;
			Database_ponto_eletronico::$chavesUnicas['tarefa_cotidiano'] = null;
			Database_ponto_eletronico::$chavesUnicas['tarefa_cotidiano_item'] = null;
			Database_ponto_eletronico::$chavesUnicas['wifi'] = null;
			Database_ponto_eletronico::$chavesUnicas['wifi_registro'] = null;
			Database_ponto_eletronico::$chavesUnicas['rede'] = array('nome', 'corporacao_id_INT');
			Database_ponto_eletronico::$chavesUnicas['rede_empresa'] = array('rede_id_INT', 'empresa_id_INT');
			Database_ponto_eletronico::$chavesUnicas['relatorio_anexo'] = null;
			Database_ponto_eletronico::$chavesUnicas['sistema_lista'] = null;
			Database_ponto_eletronico::$chavesUnicas['sistema_campo'] = null;
			Database_ponto_eletronico::$chavesUnicas['sistema_campo_atributo'] = null;
			Database_ponto_eletronico::$chavesUnicas['sistema_registro_sincronizador'] = null;
			Database_ponto_eletronico::$chavesUnicas['tela'] = null;
			Database_ponto_eletronico::$chavesUnicas['tela_sistema_lista'] = null;
			Database_ponto_eletronico::$chavesUnicas['tela_componente'] = null;
			Database_ponto_eletronico::$chavesUnicas['usuario_categoria_permissao'] = array('usuario_id_INT', 'corporacao_id_INT');
			Database_ponto_eletronico::$chavesUnicas['usuario_corporacao'] = array('usuario_id_INT', 'corporacao_id_INT');
			Database_ponto_eletronico::$chavesUnicas['usuario_foto'] = null;
			Database_ponto_eletronico::$chavesUnicas['usuario_foto_configuracao'] = null;
			Database_ponto_eletronico::$chavesUnicas['usuario_menu'] = null;
			Database_ponto_eletronico::$chavesUnicas['usuario_privilegio'] = null;
			Database_ponto_eletronico::$chavesUnicas['usuario_servico'] = array('servico_id_INT', 'usuario_id_INT', 'corporacao_id_INT');
			Database_ponto_eletronico::$chavesUnicas['usuario_tipo_corporacao'] = array('usuario_id_INT', 'corporacao_id_INT');
			Database_ponto_eletronico::$chavesUnicas['veiculo_usuario'] = array('usuario_id_INT', 'veiculo_id_INT', 'corporacao_id_INT');
			Database_ponto_eletronico::$chavesUnicas['usuario_posicao'] = null;
			Database_ponto_eletronico::$chavesUnicas['veiculo_registro'] = null;
            }
            
            return Database_ponto_eletronico::$chavesUnicas[$tabela];
        }
        
        
    // *************************
    // CONSTRUTOR
    // *************************
    
    public function __construct(){
            

    }

    
    // *************************
    // FACTORY
    // *************************
    public function factory(){
        return new Database_ponto_eletronico();

    }
    
        
	public function getVetorTable() {
		// TODO Auto-generated method stub
		return Database_ponto_eletronico::$tabelas;
	}

    } // classe: fim
    

    ?>