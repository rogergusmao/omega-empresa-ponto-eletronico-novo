"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var geral_1 = require("lib/geral");
var geral_2 = require("aplicacao/comum/geral");
var Helper = (function (_super) {
    __extends(Helper, _super);
    function Helper() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Helper.getSexoAsString = function (sexo) {
        switch (sexo) {
            case geral_2.Sexo.MASCULINO:
                return "Masculino";
            case geral_2.Sexo.FEMININO:
                return "Feminino";
            case geral_2.Sexo.NAO_DEFINIDO:
                return "Não definido";
        }
    };
    Helper.getTipoPontoAsString = function (tipoPonto) {
        switch (tipoPonto) {
            case geral_2.TipoPontoEletronico.PONTO_ENTRADA:
                return "Entrada";
            case geral_2.TipoPontoEletronico.PONTO_SAIDA:
                return "Saída";
        }
    };
    return Helper;
}(geral_1.Helper));
exports.Helper = Helper;
