<?php

/*

Arquivo gerado através de gerador de código em 07/01/2018 as 16:46:19.
Para que o arquivo não seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

Tabela correspondente: empresa
Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

*/
?>

<?

global $objFilter;

?>

<?= $objFilter->getFormDebugPanel(); ?>
<div class='row'>
    <div class='col-sm-12'>
        <div class='box omega-filter-box'>
            <div class='box-header blue-background'>
                <div class='title'>
                    <div class='icon-edit'></div>
                    <?= I18N::getExpression("Filtro"); ?>
                </div>
                <div class='actions'>
                    <a class="btn box-collapse btn-xs btn-link" href="#"><i></i></a>
                </div>
            </div>
            <div class='box-content'>

                <form name="<?= $objFilter->getFormName() ?>" class="form form-horizontal" style="margin-bottom: 0;">

                    <fieldset>

                        <?

                        $objCampo = $objFilter->getNumericInputInstance("id");
                        $objCampo->setLabel(I18N::getExpression("Id"));

                        $objCampo->setDefaultValues();
                        $objCampo->setPlaceHolder(I18N::getExpression("Informe o Id."));
                        $objCampo->setAllowNegative(false);
                        $objCampo->setDecimalPlaces(0);
                        $objCampo->setHideThousandsSeparator(true);
                        $objCampo->addValidation(new RequiredValidation(I18N::getExpression("O Id é de preenchimento obrigatório.")));

                        ?>

                        <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                             ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                            <?= $objCampo->renderLabel(); ?>
                            <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                <?= $objCampo->render(); ?>
                                <?= $objCampo->renderValidationBlock(); ?>

                            </div>
                        </div>

                        <?= $objFilter->renderHorizontalLine(); ?>



                        <?

                        $objCampo = $objFilter->getTextInputInstance("nome");
                        $objCampo->setLabel(I18N::getExpression("Nome"));

                        $objCampo->setDefaultValues();
                        $objCampo->setPlaceHolder("Informe o Nome.");
                        $objCampo->addValidation(new MaxLengthValidation(255, I18N::getExpression("O tamanho máximo do Nome deve ser de {0} caracteres.")));
                        $objCampo->addValidation(new RequiredValidation(I18N::getExpression("O Nome é de preenchimento obrigatório.")));

                        ?>

                        <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                             ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                            <?= $objCampo->renderLabel(); ?>
                            <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                <?= $objCampo->render(); ?>
                                <?= $objCampo->renderValidationBlock(); ?>

                            </div>
                        </div>

                        <?= $objFilter->renderHorizontalLine(); ?>



                        <?

                        $objCampo = $objFilter->getTextInputInstance("email");
                        $objCampo->setLabel(I18N::getExpression("Email"));

                        $objCampo->setDefaultValues();
                        $objCampo->setPlaceHolder(I18N::getExpression("Informe o Email."));
                        $objCampo->addValidation(new RequiredValidation(I18N::getExpression("O Email é de preenchimento obrigatório.")));

                        ?>

                        <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                             ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                            <?= $objCampo->renderLabel(); ?>
                            <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                <?= $objCampo->render(); ?>
                                <?= $objCampo->renderValidationBlock(); ?>

                            </div>
                        </div>

                        <?= $objFilter->renderHorizontalLine(); ?>



                        <?

                        $objCampo = $objFilter->getSelectInstance("tipoDocumentoId");
                        $objCampo->setLabel(I18N::getExpression("Tipo de documento"));

                        $objCampo->setPlaceHolder(I18N::getExpression("Selecione um tipo de documento da lista..."), false);
                        $objCampo->setMatchExpression("{{nome}}");
                        $objCampo->setCloseOnSelect(true);

                        $propsFilter = new PropsFilter();
                        $propsFilter->addFieldToFilter("nome");

                        $selectChoiceContent = new SelectChoiceContent("tipoDocumentoId");
                        $selectChoiceContent->setMainString(new AngularString("nome", "div", Select::getSelectHighLightFilter()));

                        $objCampo->setFilter($propsFilter);
                        $objCampo->setSelectChoiceContent($selectChoiceContent);
                        $objCampo->addValidation(new RequiredValidation(I18N::getExpression("O Tipo de documento é de preenchimento obrigatório.")));

                        ?>

                        <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                             ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                            <?= $objCampo->renderLabel(); ?>
                            <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                <?= $objCampo->render(); ?>
                                <?= $objCampo->renderValidationBlock(); ?>

                            </div>
                        </div>

                        <?= $objFilter->renderHorizontalLine(); ?>



                        <?

                        $objCampo = $objFilter->getTextInputInstance("numeroDocumento");
                        $objCampo->setLabel(I18N::getExpression("Número do documento"));

                        $objCampo->setDefaultValues();
                        $objCampo->setPlaceHolder(I18N::getExpression("Informe o Número do documento."));
                        $objCampo->addValidation(new RequiredValidation(I18N::getExpression("O Número do documento é de preenchimento obrigatório.")));

                        ?>

                        <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                             ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                            <?= $objCampo->renderLabel(); ?>
                            <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                <?= $objCampo->render(); ?>
                                <?= $objCampo->renderValidationBlock(); ?>

                            </div>
                        </div>

                        <?= $objFilter->renderHorizontalLine(); ?>



                        <?

                        $objCampo = $objFilter->getSelectInstance("tipoEmpresaId");
                        $objCampo->setLabel(I18N::getExpression("Tipo de empresa"));

                        $objCampo->setPlaceHolder(I18N::getExpression("Selecione um tipo de empresa da lista..."), false);
                        $objCampo->setMatchExpression("{{nome}}");
                        $objCampo->setCloseOnSelect(true);

                        $propsFilter = new PropsFilter();
                        $propsFilter->addFieldToFilter("nome");

                        $selectChoiceContent = new SelectChoiceContent("tipoEmpresaId");
                        $selectChoiceContent->setMainString(new AngularString("nome", "div", Select::getSelectHighLightFilter()));

                        $objCampo->setFilter($propsFilter);
                        $objCampo->setSelectChoiceContent($selectChoiceContent);
                        $objCampo->addValidation(new RequiredValidation(I18N::getExpression("O Tipo de empresa é de preenchimento obrigatório.")));

                        ?>

                        <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                             ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                            <?= $objCampo->renderLabel(); ?>
                            <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                <?= $objCampo->render(); ?>
                                <?= $objCampo->renderValidationBlock(); ?>

                            </div>
                        </div>

                        <?= $objFilter->renderHorizontalLine(); ?>



                        <?

                        $objCampo = $objFilter->getTextInputInstance("logradouro");
                        $objCampo->setLabel(I18N::getExpression("Logradouro"));

                        $objCampo->setDefaultValues();
                        $objCampo->setPlaceHolder("Informe o Logradouro.");
                        $objCampo->addValidation(new MaxLengthValidation(255, I18N::getExpression("O tamanho máximo do Logradouro deve ser de {0} caracteres.")));
                        $objCampo->addValidation(new RequiredValidation(I18N::getExpression("O Logradouro é de preenchimento obrigatório.")));

                        ?>

                        <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                             ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                            <?= $objCampo->renderLabel(); ?>
                            <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                <?= $objCampo->render(); ?>
                                <?= $objCampo->renderValidationBlock(); ?>

                            </div>
                        </div>

                        <?= $objFilter->renderHorizontalLine(); ?>



                        <?

                        $objCampo = $objFilter->getSelectInstance("cidadeId");
                        $objCampo->setLabel(I18N::getExpression("Cidade"));

                        $objCampo->setPlaceHolder(I18N::getExpression("Selecione uma cidade da lista..."), false);
                        $objCampo->setMatchExpression("{{nome}}");
                        $objCampo->setCloseOnSelect(true);

                        $propsFilter = new PropsFilter();
                        $propsFilter->addFieldToFilter("nome");

                        $selectChoiceContent = new SelectChoiceContent("cidadeId");
                        $selectChoiceContent->setMainString(new AngularString("nome", "div", Select::getSelectHighLightFilter()));

                        $objCampo->setFilter($propsFilter);
                        $objCampo->setSelectChoiceContent($selectChoiceContent);
                        $objCampo->addValidation(new RequiredValidation(I18N::getExpression("A Cidade é de preenchimento obrigatório.")));

                        ?>

                        <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                             ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                            <?= $objCampo->renderLabel(); ?>
                            <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                <?= $objCampo->render(); ?>
                                <?= $objCampo->renderValidationBlock(); ?>

                            </div>
                        </div>

                        <?= $objFilter->renderHorizontalLine(); ?>

                    </fieldset>

                    <div class='form-actions form-actions-padding-sm'>
                        <div class='row'>
                            <div class='col-md-11 action-buttons-bar'>

                                <?= $objFilter->renderSubmitButton(); ?>
                                <?= $objFilter->renderClearButton(); ?>

                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
            
