<?php


        /*

        Arquivo gerado através de gerador de código em 12/05/2018 as 14:51:12.
        Para que o arquivo não seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

        Tabela correspondente: tarefa_cotidiano
        Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

        */
        


class DAO_Tarefa_cotidiano extends Generic_DAO
                        {
                            const NOME_ENTIDADE = "tarefa_cotidiano";

                            public static $nomeTabela = null;
                            public static $idSistemaTabela = false;
                            public static $databaseFieldsRelatedAttributes = null;
	                        public static $databaseFieldTypes = null;
	                        public static $databaseFieldNames = null;
	                        public static $listAliasTypes = null;
	                        public static $listAliasRelatedAttributes = null;
	                        
	                        public $id;
public $criadoPeloUsuarioId;
public $categoriaPermissaoId;
public $veiculoId;
public $usuarioId;
public $empresaEquipeId;
public $origemPessoaId;
public $origemEmpresaId;
public $origemLogradouro;
public $origemNumero;
public $origemCidadeId;
public $origemLatitudeInt;
public $origemLongitudeInt;
public $origemLatitudeRealInt;
public $origemLongitudeRealInt;
public $destinoPessoaId;
public $destinoEmpresaId;
public $destinoLogradouro;
public $destinoNumero;
public $destinoCidadeId;
public $destinoLatitudeInt;
public $destinoLongitudeInt;
public $destinoLatitudeRealInt;
public $destinoLongitudeRealInt;
public $tempoEstimadoCarroInt;
public $tempoEstimadoAPeInt;
public $distanciaEstimadaCarroInt;
public $distanciaEstimadaAPeInt;
public $isRealizadaAPeBoolean;
public $inicioHoraProgramadaSec;
public $inicioHoraProgramadaOffsec;
public $inicioSec;
public $inicioOffsec;
public $fimSec;
public $fimOffsec;
public $cadastroSec;
public $cadastroOffsec;
public $titulo;
public $tituloNormalizado;
public $descricao;
public $corporacaoId;
public $atividadeId;
public $idEstadoInt;
public $idPrioridadeInt;
public $parametroSec;
public $parametroOffsec;
public $parametroJson;
public $dataLimiteCotidianoSec;
public $dataLimiteCotidianoOffsec;
public $idTipoCotidianoInt;
public $prazoSec;
public $prazoOffsec;
public $percentualCompletoInt;
public $ultimaGeracaoSec;
public $ultimaGeracaoOffsec;
public $relatorioId;

public $labelId;
public $labelCriadoPeloUsuarioId;
public $labelCategoriaPermissaoId;
public $labelVeiculoId;
public $labelUsuarioId;
public $labelEmpresaEquipeId;
public $labelOrigemPessoaId;
public $labelOrigemEmpresaId;
public $labelOrigemLogradouro;
public $labelOrigemNumero;
public $labelOrigemCidadeId;
public $labelOrigemLatitudeInt;
public $labelOrigemLongitudeInt;
public $labelOrigemLatitudeRealInt;
public $labelOrigemLongitudeRealInt;
public $labelDestinoPessoaId;
public $labelDestinoEmpresaId;
public $labelDestinoLogradouro;
public $labelDestinoNumero;
public $labelDestinoCidadeId;
public $labelDestinoLatitudeInt;
public $labelDestinoLongitudeInt;
public $labelDestinoLatitudeRealInt;
public $labelDestinoLongitudeRealInt;
public $labelTempoEstimadoCarroInt;
public $labelTempoEstimadoAPeInt;
public $labelDistanciaEstimadaCarroInt;
public $labelDistanciaEstimadaAPeInt;
public $labelIsRealizadaAPeBoolean;
public $labelInicioHoraProgramadaSec;
public $labelInicioHoraProgramadaOffsec;
public $labelInicioSec;
public $labelInicioOffsec;
public $labelFimSec;
public $labelFimOffsec;
public $labelCadastroSec;
public $labelCadastroOffsec;
public $labelTitulo;
public $labelTituloNormalizado;
public $labelDescricao;
public $labelCorporacaoId;
public $labelAtividadeId;
public $labelIdEstadoInt;
public $labelIdPrioridadeInt;
public $labelParametroSec;
public $labelParametroOffsec;
public $labelParametroJson;
public $labelDataLimiteCotidianoSec;
public $labelDataLimiteCotidianoOffsec;
public $labelIdTipoCotidianoInt;
public $labelPrazoSec;
public $labelPrazoOffsec;
public $labelPercentualCompletoInt;
public $labelUltimaGeracaoSec;
public $labelUltimaGeracaoOffsec;
public $labelRelatorioId;

public static $arrPK = array("id");

public function __construct($configDAO = null)
        {
            parent::__construct($configDAO);
            static::__setStaticValues($this->database);           
        }

public static function idSistemaTabela($databaseObject = null){
            if(!(static::$idSistemaTabela))
            {
                static::$idSistemaTabela = EXTDAO_Sistema_tabela::getIdDaSistemaTabela(static::nomeTabela(), $databaseObject);
            }
            return static::$idSistemaTabela;
        }

public function getIdSistemaTabela(){
            return static::idSistemaTabela();
        }

public static function nomeTabela(){
            if(is_null(static::$nomeTabela))
            {
                static::$nomeTabela = "tarefa_cotidiano";
            }
            return static::$nomeTabela;
        }

public function getNomeTabela(){
            return static::nomeTabela();
        }

public static function __setStaticValues($databaseObject = null)
                   {    
                        static::nomeTabela();
                        
                        static::idSistemaTabela($databaseObject);
                   
                        static::setDatabaseFieldNames();
                        static::setDatabaseFieldTypes();
                        static::setDatabaseFieldsRelatedAttributeNames();
                        
                        static::setListAliasTypes();
                        static::setListAliasRelatedAttributes();
                   }

public function getExpressaoLabel()
                   {
                        return "{id}";
                   }

public static function setDatabaseFieldNames()
        {
            if(is_null(static::$databaseFieldNames))
            {
                static::$databaseFieldNames = new stdClass();
                static::$databaseFieldNames->id = "id";
static::$databaseFieldNames->criadoPeloUsuarioId = "criado_pelo_usuario_id_INT";
static::$databaseFieldNames->categoriaPermissaoId = "categoria_permissao_id_INT";
static::$databaseFieldNames->veiculoId = "veiculo_id_INT";
static::$databaseFieldNames->usuarioId = "usuario_id_INT";
static::$databaseFieldNames->empresaEquipeId = "empresa_equipe_id_INT";
static::$databaseFieldNames->origemPessoaId = "origem_pessoa_id_INT";
static::$databaseFieldNames->origemEmpresaId = "origem_empresa_id_INT";
static::$databaseFieldNames->origemLogradouro = "origem_logradouro";
static::$databaseFieldNames->origemNumero = "origem_numero";
static::$databaseFieldNames->origemCidadeId = "origem_cidade_id_INT";
static::$databaseFieldNames->origemLatitudeInt = "origem_latitude_INT";
static::$databaseFieldNames->origemLongitudeInt = "origem_longitude_INT";
static::$databaseFieldNames->origemLatitudeRealInt = "origem_latitude_real_INT";
static::$databaseFieldNames->origemLongitudeRealInt = "origem_longitude_real_INT";
static::$databaseFieldNames->destinoPessoaId = "destino_pessoa_id_INT";
static::$databaseFieldNames->destinoEmpresaId = "destino_empresa_id_INT";
static::$databaseFieldNames->destinoLogradouro = "destino_logradouro";
static::$databaseFieldNames->destinoNumero = "destino_numero";
static::$databaseFieldNames->destinoCidadeId = "destino_cidade_id_INT";
static::$databaseFieldNames->destinoLatitudeInt = "destino_latitude_INT";
static::$databaseFieldNames->destinoLongitudeInt = "destino_longitude_INT";
static::$databaseFieldNames->destinoLatitudeRealInt = "destino_latitude_real_INT";
static::$databaseFieldNames->destinoLongitudeRealInt = "destino_longitude_real_INT";
static::$databaseFieldNames->tempoEstimadoCarroInt = "tempo_estimado_carro_INT";
static::$databaseFieldNames->tempoEstimadoAPeInt = "tempo_estimado_a_pe_INT";
static::$databaseFieldNames->distanciaEstimadaCarroInt = "distancia_estimada_carro_INT";
static::$databaseFieldNames->distanciaEstimadaAPeInt = "distancia_estimada_a_pe_INT";
static::$databaseFieldNames->isRealizadaAPeBoolean = "is_realizada_a_pe_BOOLEAN";
static::$databaseFieldNames->inicioHoraProgramadaSec = "inicio_hora_programada_SEC";
static::$databaseFieldNames->inicioHoraProgramadaOffsec = "inicio_hora_programada_OFFSEC";
static::$databaseFieldNames->inicioSec = "inicio_SEC";
static::$databaseFieldNames->inicioOffsec = "inicio_OFFSEC";
static::$databaseFieldNames->fimSec = "fim_SEC";
static::$databaseFieldNames->fimOffsec = "fim_OFFSEC";
static::$databaseFieldNames->cadastroSec = "cadastro_SEC";
static::$databaseFieldNames->cadastroOffsec = "cadastro_OFFSEC";
static::$databaseFieldNames->titulo = "titulo";
static::$databaseFieldNames->tituloNormalizado = "titulo_normalizado";
static::$databaseFieldNames->descricao = "descricao";
static::$databaseFieldNames->corporacaoId = "corporacao_id_INT";
static::$databaseFieldNames->atividadeId = "atividade_id_INT";
static::$databaseFieldNames->idEstadoInt = "id_estado_INT";
static::$databaseFieldNames->idPrioridadeInt = "id_prioridade_INT";
static::$databaseFieldNames->parametroSec = "parametro_SEC";
static::$databaseFieldNames->parametroOffsec = "parametro_OFFSEC";
static::$databaseFieldNames->parametroJson = "parametro_json";
static::$databaseFieldNames->dataLimiteCotidianoSec = "data_limite_cotidiano_SEC";
static::$databaseFieldNames->dataLimiteCotidianoOffsec = "data_limite_cotidiano_OFFSEC";
static::$databaseFieldNames->idTipoCotidianoInt = "id_tipo_cotidiano_INT";
static::$databaseFieldNames->prazoSec = "prazo_SEC";
static::$databaseFieldNames->prazoOffsec = "prazo_OFFSEC";
static::$databaseFieldNames->percentualCompletoInt = "percentual_completo_INT";
static::$databaseFieldNames->ultimaGeracaoSec = "ultima_geracao_SEC";
static::$databaseFieldNames->ultimaGeracaoOffsec = "ultima_geracao_OFFSEC";
static::$databaseFieldNames->relatorioId = "relatorio_id_INT";

            }
                      
        }

public static function setDatabaseFieldTypes()
        {
            if(is_null(static::$databaseFieldTypes))
            {
                static::$databaseFieldTypes = new stdClass();
                static::$databaseFieldTypes->id = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->criadoPeloUsuarioId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->categoriaPermissaoId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->veiculoId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->usuarioId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->empresaEquipeId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->origemPessoaId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->origemEmpresaId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->origemLogradouro = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->origemNumero = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->origemCidadeId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->origemLatitudeInt = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->origemLongitudeInt = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->origemLatitudeRealInt = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->origemLongitudeRealInt = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->destinoPessoaId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->destinoEmpresaId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->destinoLogradouro = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->destinoNumero = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->destinoCidadeId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->destinoLatitudeInt = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->destinoLongitudeInt = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->destinoLatitudeRealInt = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->destinoLongitudeRealInt = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->tempoEstimadoCarroInt = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->tempoEstimadoAPeInt = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->distanciaEstimadaCarroInt = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->distanciaEstimadaAPeInt = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->isRealizadaAPeBoolean = static::TIPO_VARIAVEL_BOOLEAN;
static::$databaseFieldTypes->inicioHoraProgramadaSec = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$databaseFieldTypes->inicioHoraProgramadaOffsec = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$databaseFieldTypes->inicioSec = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$databaseFieldTypes->inicioOffsec = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$databaseFieldTypes->fimSec = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$databaseFieldTypes->fimOffsec = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$databaseFieldTypes->cadastroSec = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$databaseFieldTypes->cadastroOffsec = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$databaseFieldTypes->titulo = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->tituloNormalizado = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->descricao = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->corporacaoId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->atividadeId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->idEstadoInt = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->idPrioridadeInt = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->parametroSec = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$databaseFieldTypes->parametroOffsec = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$databaseFieldTypes->parametroJson = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->dataLimiteCotidianoSec = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$databaseFieldTypes->dataLimiteCotidianoOffsec = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$databaseFieldTypes->idTipoCotidianoInt = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->prazoSec = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$databaseFieldTypes->prazoOffsec = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$databaseFieldTypes->percentualCompletoInt = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->ultimaGeracaoSec = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$databaseFieldTypes->ultimaGeracaoOffsec = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$databaseFieldTypes->relatorioId = static::TIPO_VARIAVEL_INTEGER;

            }
            
        }

public static function setDatabaseFieldsRelatedAttributeNames()
        {
            if(is_null(static::$databaseFieldsRelatedAttributes))
            {
                static::$databaseFieldsRelatedAttributes = new stdClass();
                static::$databaseFieldsRelatedAttributes->id = "id";
static::$databaseFieldsRelatedAttributes->criado_pelo_usuario_id_INT = "criadoPeloUsuarioId";
static::$databaseFieldsRelatedAttributes->categoria_permissao_id_INT = "categoriaPermissaoId";
static::$databaseFieldsRelatedAttributes->veiculo_id_INT = "veiculoId";
static::$databaseFieldsRelatedAttributes->usuario_id_INT = "usuarioId";
static::$databaseFieldsRelatedAttributes->empresa_equipe_id_INT = "empresaEquipeId";
static::$databaseFieldsRelatedAttributes->origem_pessoa_id_INT = "origemPessoaId";
static::$databaseFieldsRelatedAttributes->origem_empresa_id_INT = "origemEmpresaId";
static::$databaseFieldsRelatedAttributes->origem_logradouro = "origemLogradouro";
static::$databaseFieldsRelatedAttributes->origem_numero = "origemNumero";
static::$databaseFieldsRelatedAttributes->origem_cidade_id_INT = "origemCidadeId";
static::$databaseFieldsRelatedAttributes->origem_latitude_INT = "origemLatitudeInt";
static::$databaseFieldsRelatedAttributes->origem_longitude_INT = "origemLongitudeInt";
static::$databaseFieldsRelatedAttributes->origem_latitude_real_INT = "origemLatitudeRealInt";
static::$databaseFieldsRelatedAttributes->origem_longitude_real_INT = "origemLongitudeRealInt";
static::$databaseFieldsRelatedAttributes->destino_pessoa_id_INT = "destinoPessoaId";
static::$databaseFieldsRelatedAttributes->destino_empresa_id_INT = "destinoEmpresaId";
static::$databaseFieldsRelatedAttributes->destino_logradouro = "destinoLogradouro";
static::$databaseFieldsRelatedAttributes->destino_numero = "destinoNumero";
static::$databaseFieldsRelatedAttributes->destino_cidade_id_INT = "destinoCidadeId";
static::$databaseFieldsRelatedAttributes->destino_latitude_INT = "destinoLatitudeInt";
static::$databaseFieldsRelatedAttributes->destino_longitude_INT = "destinoLongitudeInt";
static::$databaseFieldsRelatedAttributes->destino_latitude_real_INT = "destinoLatitudeRealInt";
static::$databaseFieldsRelatedAttributes->destino_longitude_real_INT = "destinoLongitudeRealInt";
static::$databaseFieldsRelatedAttributes->tempo_estimado_carro_INT = "tempoEstimadoCarroInt";
static::$databaseFieldsRelatedAttributes->tempo_estimado_a_pe_INT = "tempoEstimadoAPeInt";
static::$databaseFieldsRelatedAttributes->distancia_estimada_carro_INT = "distanciaEstimadaCarroInt";
static::$databaseFieldsRelatedAttributes->distancia_estimada_a_pe_INT = "distanciaEstimadaAPeInt";
static::$databaseFieldsRelatedAttributes->is_realizada_a_pe_BOOLEAN = "isRealizadaAPeBoolean";
static::$databaseFieldsRelatedAttributes->inicio_hora_programada_SEC = "inicioHoraProgramadaSec";
static::$databaseFieldsRelatedAttributes->inicio_hora_programada_OFFSEC = "inicioHoraProgramadaOffsec";
static::$databaseFieldsRelatedAttributes->inicio_SEC = "inicioSec";
static::$databaseFieldsRelatedAttributes->inicio_OFFSEC = "inicioOffsec";
static::$databaseFieldsRelatedAttributes->fim_SEC = "fimSec";
static::$databaseFieldsRelatedAttributes->fim_OFFSEC = "fimOffsec";
static::$databaseFieldsRelatedAttributes->cadastro_SEC = "cadastroSec";
static::$databaseFieldsRelatedAttributes->cadastro_OFFSEC = "cadastroOffsec";
static::$databaseFieldsRelatedAttributes->titulo = "titulo";
static::$databaseFieldsRelatedAttributes->titulo_normalizado = "tituloNormalizado";
static::$databaseFieldsRelatedAttributes->descricao = "descricao";
static::$databaseFieldsRelatedAttributes->corporacao_id_INT = "corporacaoId";
static::$databaseFieldsRelatedAttributes->atividade_id_INT = "atividadeId";
static::$databaseFieldsRelatedAttributes->id_estado_INT = "idEstadoInt";
static::$databaseFieldsRelatedAttributes->id_prioridade_INT = "idPrioridadeInt";
static::$databaseFieldsRelatedAttributes->parametro_SEC = "parametroSec";
static::$databaseFieldsRelatedAttributes->parametro_OFFSEC = "parametroOffsec";
static::$databaseFieldsRelatedAttributes->parametro_json = "parametroJson";
static::$databaseFieldsRelatedAttributes->data_limite_cotidiano_SEC = "dataLimiteCotidianoSec";
static::$databaseFieldsRelatedAttributes->data_limite_cotidiano_OFFSEC = "dataLimiteCotidianoOffsec";
static::$databaseFieldsRelatedAttributes->id_tipo_cotidiano_INT = "idTipoCotidianoInt";
static::$databaseFieldsRelatedAttributes->prazo_SEC = "prazoSec";
static::$databaseFieldsRelatedAttributes->prazo_OFFSEC = "prazoOffsec";
static::$databaseFieldsRelatedAttributes->percentual_completo_INT = "percentualCompletoInt";
static::$databaseFieldsRelatedAttributes->ultima_geracao_SEC = "ultimaGeracaoSec";
static::$databaseFieldsRelatedAttributes->ultima_geracao_OFFSEC = "ultimaGeracaoOffsec";
static::$databaseFieldsRelatedAttributes->relatorio_id_INT = "relatorioId";

            }           
        }

public function setAllFieldsToHumanFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["criado_pelo_usuario_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["categoria_permissao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["veiculo_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["usuario_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["empresa_equipe_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["origem_pessoa_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["origem_empresa_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["origem_logradouro"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["origem_numero"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["origem_cidade_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["origem_latitude_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["origem_longitude_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["origem_latitude_real_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["origem_longitude_real_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["destino_pessoa_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["destino_empresa_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["destino_logradouro"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["destino_numero"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["destino_cidade_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["destino_latitude_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["destino_longitude_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["destino_latitude_real_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["destino_longitude_real_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["tempo_estimado_carro_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["tempo_estimado_a_pe_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["distancia_estimada_carro_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["distancia_estimada_a_pe_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["is_realizada_a_pe_BOOLEAN"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["inicio_hora_programada_SEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["inicio_hora_programada_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["inicio_SEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["inicio_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["fim_SEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["fim_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["cadastro_SEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["cadastro_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["titulo"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["titulo_normalizado"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["descricao"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["atividade_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["id_estado_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["id_prioridade_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["parametro_SEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["parametro_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["parametro_json"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["data_limite_cotidiano_SEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["data_limite_cotidiano_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["id_tipo_cotidiano_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["prazo_SEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["prazo_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["percentual_completo_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["ultima_geracao_SEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["ultima_geracao_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["relatorio_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
}

public function setAllFieldsToDatabaseFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["criado_pelo_usuario_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["categoria_permissao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["veiculo_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["usuario_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["empresa_equipe_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["origem_pessoa_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["origem_empresa_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["origem_logradouro"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["origem_numero"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["origem_cidade_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["origem_latitude_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["origem_longitude_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["origem_latitude_real_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["origem_longitude_real_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["destino_pessoa_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["destino_empresa_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["destino_logradouro"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["destino_numero"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["destino_cidade_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["destino_latitude_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["destino_longitude_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["destino_latitude_real_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["destino_longitude_real_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["tempo_estimado_carro_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["tempo_estimado_a_pe_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["distancia_estimada_carro_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["distancia_estimada_a_pe_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["is_realizada_a_pe_BOOLEAN"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["inicio_hora_programada_SEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["inicio_hora_programada_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["inicio_SEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["inicio_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["fim_SEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["fim_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["cadastro_SEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["cadastro_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["titulo"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["titulo_normalizado"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["descricao"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["atividade_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["id_estado_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["id_prioridade_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["parametro_SEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["parametro_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["parametro_json"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["data_limite_cotidiano_SEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["data_limite_cotidiano_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["id_tipo_cotidiano_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["prazo_SEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["prazo_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["percentual_completo_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["ultima_geracao_SEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["ultima_geracao_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["relatorio_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
}

  public function getFkObjCriado_pelo_usuario() 
                {
                    if($this->objCriado_pelo_usuario == null)
                    {                        
                        $this->objCriado_pelo_usuario = new EXTDAO_Criado_pelo_usuario_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getCriado_pelo_usuario_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objCriado_pelo_usuario->clear();
                    }
                    elseif($this->objCriado_pelo_usuario->getId() != $idFK)
                    {
                        $this->objCriado_pelo_usuario->select($idFK);
                    }
                    return $this->objCriado_pelo_usuario;
                }
  public function getFkObjCategoria_permissao() 
                {
                    if($this->objCategoria_permissao == null)
                    {                        
                        $this->objCategoria_permissao = new EXTDAO_Categoria_permissao_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getCategoria_permissao_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objCategoria_permissao->clear();
                    }
                    elseif($this->objCategoria_permissao->getId() != $idFK)
                    {
                        $this->objCategoria_permissao->select($idFK);
                    }
                    return $this->objCategoria_permissao;
                }
  public function getFkObjVeiculo() 
                {
                    if($this->objVeiculo == null)
                    {                        
                        $this->objVeiculo = new EXTDAO_Veiculo_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getVeiculo_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objVeiculo->clear();
                    }
                    elseif($this->objVeiculo->getId() != $idFK)
                    {
                        $this->objVeiculo->select($idFK);
                    }
                    return $this->objVeiculo;
                }
  public function getFkObjUsuario() 
                {
                    if($this->objUsuario == null)
                    {                        
                        $this->objUsuario = new EXTDAO_Usuario_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getUsuario_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objUsuario->clear();
                    }
                    elseif($this->objUsuario->getId() != $idFK)
                    {
                        $this->objUsuario->select($idFK);
                    }
                    return $this->objUsuario;
                }
  public function getFkObjEmpresa_equipe() 
                {
                    if($this->objEmpresa_equipe == null)
                    {                        
                        $this->objEmpresa_equipe = new EXTDAO_Empresa_equipe_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getEmpresa_equipe_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objEmpresa_equipe->clear();
                    }
                    elseif($this->objEmpresa_equipe->getId() != $idFK)
                    {
                        $this->objEmpresa_equipe->select($idFK);
                    }
                    return $this->objEmpresa_equipe;
                }
  public function getFkObjOrigem_pessoa() 
                {
                    if($this->objOrigem_pessoa == null)
                    {                        
                        $this->objOrigem_pessoa = new EXTDAO_Origem_pessoa_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getOrigem_pessoa_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objOrigem_pessoa->clear();
                    }
                    elseif($this->objOrigem_pessoa->getId() != $idFK)
                    {
                        $this->objOrigem_pessoa->select($idFK);
                    }
                    return $this->objOrigem_pessoa;
                }
  public function getFkObjOrigem_empresa() 
                {
                    if($this->objOrigem_empresa == null)
                    {                        
                        $this->objOrigem_empresa = new EXTDAO_Origem_empresa_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getOrigem_empresa_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objOrigem_empresa->clear();
                    }
                    elseif($this->objOrigem_empresa->getId() != $idFK)
                    {
                        $this->objOrigem_empresa->select($idFK);
                    }
                    return $this->objOrigem_empresa;
                }
  public function getFkObjOrigem_cidade() 
                {
                    if($this->objOrigem_cidade == null)
                    {                        
                        $this->objOrigem_cidade = new EXTDAO_Origem_cidade_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getOrigem_cidade_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objOrigem_cidade->clear();
                    }
                    elseif($this->objOrigem_cidade->getId() != $idFK)
                    {
                        $this->objOrigem_cidade->select($idFK);
                    }
                    return $this->objOrigem_cidade;
                }
  public function getFkObjDestino_pessoa() 
                {
                    if($this->objDestino_pessoa == null)
                    {                        
                        $this->objDestino_pessoa = new EXTDAO_Destino_pessoa_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getDestino_pessoa_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objDestino_pessoa->clear();
                    }
                    elseif($this->objDestino_pessoa->getId() != $idFK)
                    {
                        $this->objDestino_pessoa->select($idFK);
                    }
                    return $this->objDestino_pessoa;
                }
  public function getFkObjDestino_empresa() 
                {
                    if($this->objDestino_empresa == null)
                    {                        
                        $this->objDestino_empresa = new EXTDAO_Destino_empresa_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getDestino_empresa_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objDestino_empresa->clear();
                    }
                    elseif($this->objDestino_empresa->getId() != $idFK)
                    {
                        $this->objDestino_empresa->select($idFK);
                    }
                    return $this->objDestino_empresa;
                }
  public function getFkObjDestino_cidade() 
                {
                    if($this->objDestino_cidade == null)
                    {                        
                        $this->objDestino_cidade = new EXTDAO_Destino_cidade_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getDestino_cidade_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objDestino_cidade->clear();
                    }
                    elseif($this->objDestino_cidade->getId() != $idFK)
                    {
                        $this->objDestino_cidade->select($idFK);
                    }
                    return $this->objDestino_cidade;
                }
  public function getFkObjCorporacao() 
                {
                    if($this->objCorporacao == null)
                    {                        
                        $this->objCorporacao = new EXTDAO_Corporacao_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getCorporacao_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objCorporacao->clear();
                    }
                    elseif($this->objCorporacao->getId() != $idFK)
                    {
                        $this->objCorporacao->select($idFK);
                    }
                    return $this->objCorporacao;
                }
  public function getFkObjAtividade() 
                {
                    if($this->objAtividade == null)
                    {                        
                        $this->objAtividade = new EXTDAO_Atividade_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getAtividade_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objAtividade->clear();
                    }
                    elseif($this->objAtividade->getId() != $idFK)
                    {
                        $this->objAtividade->select($idFK);
                    }
                    return $this->objAtividade;
                }
  public function getFkObjRelatorio() 
                {
                    if($this->objRelatorio == null)
                    {                        
                        $this->objRelatorio = new EXTDAO_Relatorio_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getRelatorio_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objRelatorio->clear();
                    }
                    elseif($this->objRelatorio->getId() != $idFK)
                    {
                        $this->objRelatorio->select($idFK);
                    }
                    return $this->objRelatorio;
                }


public function setLabels()
        {$this->labelId = "";
$this->labelCriadoPeloUsuarioId = "";
$this->labelCategoriaPermissaoId = "";
$this->labelVeiculoId = "";
$this->labelUsuarioId = "";
$this->labelEmpresaEquipeId = "";
$this->labelOrigemPessoaId = "";
$this->labelOrigemEmpresaId = "";
$this->labelOrigemLogradouro = "";
$this->labelOrigemNumero = "";
$this->labelOrigemCidadeId = "";
$this->labelOrigemLatitudeInt = "";
$this->labelOrigemLongitudeInt = "";
$this->labelOrigemLatitudeRealInt = "";
$this->labelOrigemLongitudeRealInt = "";
$this->labelDestinoPessoaId = "";
$this->labelDestinoEmpresaId = "";
$this->labelDestinoLogradouro = "";
$this->labelDestinoNumero = "";
$this->labelDestinoCidadeId = "";
$this->labelDestinoLatitudeInt = "";
$this->labelDestinoLongitudeInt = "";
$this->labelDestinoLatitudeRealInt = "";
$this->labelDestinoLongitudeRealInt = "";
$this->labelTempoEstimadoCarroInt = "";
$this->labelTempoEstimadoAPeInt = "";
$this->labelDistanciaEstimadaCarroInt = "";
$this->labelDistanciaEstimadaAPeInt = "";
$this->labelIsRealizadaAPeBoolean = "";
$this->labelInicioHoraProgramadaSec = "";
$this->labelInicioHoraProgramadaOffsec = "";
$this->labelInicioSec = "";
$this->labelInicioOffsec = "";
$this->labelFimSec = "";
$this->labelFimOffsec = "";
$this->labelCadastroSec = "";
$this->labelCadastroOffsec = "";
$this->labelTitulo = "";
$this->labelTituloNormalizado = "";
$this->labelDescricao = "";
$this->labelCorporacaoId = "";
$this->labelAtividadeId = "";
$this->labelIdEstadoInt = "";
$this->labelIdPrioridadeInt = "";
$this->labelParametroSec = "";
$this->labelParametroOffsec = "";
$this->labelParametroJson = "";
$this->labelDataLimiteCotidianoSec = "";
$this->labelDataLimiteCotidianoOffsec = "";
$this->labelIdTipoCotidianoInt = "";
$this->labelPrazoSec = "";
$this->labelPrazoOffsec = "";
$this->labelPercentualCompletoInt = "";
$this->labelUltimaGeracaoSec = "";
$this->labelUltimaGeracaoOffsec = "";
$this->labelRelatorioId = "";
}

public function __actionAdd($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                $this->setByObject($parameters);
                $this->setDataCadastroAndOffsetInSecondsFromUser($parameters);
                
                $this->formatarParaSQL();
                $msg = $this->insert(true);
                
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                return new Mensagem(null, I18N::getExpression("Tarefa cotidiano adicionado com sucesso."));
                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionEdit($parameters = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try {

                $this->setByObject($parameters);
                $this->setAllFieldsToHumanFormat();
                
                $this->formatarParaSQL();
                $msg = $this->updateByInstanceUsingParameters($this->id, $parameters, true);
                if($msg != null && $msg->erro())
                {
                     return $msg;
                }
        
                return new Mensagem(null, I18N::getExpression("Tarefa cotidiano editado com sucesso."));
            }
            catch(Exception $ex)
            {                
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionRemove($parameters = null)
        {
            $successulRemovalCount = 0;
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                foreach($parameters as $record)
                {
                    if(is_numeric($record->id))
                    {
                        $msg = $this->delete($record->id, true);
                        if($msg == null || $msg->ok())
                        {
                            $successulRemovalCount++;
                        }
                    }
                }
                
                if($successulRemovalCount > 0)
                {
                    $successMessage = count($successulRemovalCount) > 1 ? I18N::getExpression("{0} Tarefa cotidiano foram removidos com sucesso.", $successulRemovalCount) : I18N::getExpression("Tarefa cotidiano removido com sucesso.");
                    return new Mensagem(null, $successMessage);
                }
                else
                {
                    $errorMessage = count($parameters) > 1 ? I18N::getExpression("Falha ao remover Tarefa cotidiano.") : I18N::getExpression("Falha ao remover Tarefa cotidiano.");
                    return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, $errorMessage);                    
                }
            }
            catch (Exception $ex) 
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }

public function __getRecord($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }
            
            try
            {            
                $returnObject = new stdClass();            
                $whereClause = static::getWhereClauseForFilter($parameters->filterParameters);
                $query = "SELECT  id, criado_pelo_usuario_id_INT, categoria_permissao_id_INT, veiculo_id_INT, usuario_id_INT, empresa_equipe_id_INT, origem_pessoa_id_INT, origem_empresa_id_INT, origem_logradouro, origem_numero, origem_cidade_id_INT, origem_latitude_INT, origem_longitude_INT, origem_latitude_real_INT, origem_longitude_real_INT, destino_pessoa_id_INT, destino_empresa_id_INT, destino_logradouro, destino_numero, destino_cidade_id_INT, destino_latitude_INT, destino_longitude_INT, destino_latitude_real_INT, destino_longitude_real_INT, tempo_estimado_carro_INT, tempo_estimado_a_pe_INT, distancia_estimada_carro_INT, distancia_estimada_a_pe_INT, is_realizada_a_pe_BOOLEAN, inicio_hora_programada_SEC, inicio_hora_programada_OFFSEC, inicio_SEC, inicio_OFFSEC, fim_SEC, fim_OFFSEC, cadastro_SEC, cadastro_OFFSEC, titulo, titulo_normalizado, descricao, corporacao_id_INT, atividade_id_INT, id_estado_INT, id_prioridade_INT, parametro_SEC, parametro_OFFSEC, parametro_json, data_limite_cotidiano_SEC, data_limite_cotidiano_OFFSEC, id_tipo_cotidiano_INT, prazo_SEC, prazo_OFFSEC, percentual_completo_INT, ultima_geracao_SEC, ultima_geracao_OFFSEC, relatorio_id_INT FROM tarefa_cotidiano {$whereClause}";
    
                $msg = $this->database->queryMensagem($query);                
                if ($msg == null)
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Não foi encontrado nenhum registro"));
                    }                    
                    $resultSet = $this->database->result;
                    $resultObject = static::mapResultSetDataToRecord($resultSet);
                    
                    $returnObject->formData = $resultObject;
                    $returnObject->comboBoxesData = null;
                    if($parameters->loadComboBoxesData)
                    {
                        $this->setByObject($resultObject);
                        $returnObject->comboBoxesData =  $this->getFormComboBoxesData();
                    }                    
    
                    
    
                    return new Mensagem_generica($returnObject);
                } 
                else 
                {
                    return $msg;
                }
                
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }


        
        public function __getFilterComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
                
        public function __getFormComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
        
        protected function getFormComboBoxesData()
        {
            $comboBoxesData = new stdClass();
            $listParameters = new stdClass();
            $listParameters->listMappingType = static::LIST_MAPPING_TYPE_COMBOBOX;
            
            
                    
                    $objCategoriaPermissao = new EXTDAO_Categoria_permissao();
                    $comboBoxesData->fieldCategoriaPermissaoId = $objCategoriaPermissao->__getList($listParameters);
                    
                    $objVeiculo = new EXTDAO_Veiculo();
                    $comboBoxesData->fieldVeiculoId = $objVeiculo->__getList($listParameters);
                    
                    $objUsuario = new EXTDAO_Usuario();
                    $comboBoxesData->fieldUsuarioId = $objUsuario->__getList($listParameters);
                    
                    $objEmpresaEquipe = new EXTDAO_Empresa_equipe();
                    $comboBoxesData->fieldEmpresaEquipeId = $objEmpresaEquipe->__getList($listParameters);

            return $comboBoxesData;
        }
        
        


        
        public static function setListAliasTypes()
        {
            if(is_null(static::$listAliasTypes))
            {
                static::$listAliasTypes = new stdClass();
                static::$listAliasTypes->categoria_permissao__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->veiculo__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->usuario__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->empresa_equipe__nome = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->tarefa_cotidiano__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->tarefa_cotidiano__criado_pelo_usuario_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->tarefa_cotidiano__corporacao_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->tarefa_cotidiano__corporacao_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->tarefa_cotidiano__cadastro_OFFSEC = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$listAliasTypes->tarefa_cotidiano__corporacao_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->tarefa_cotidiano__origem_pessoa_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->tarefa_cotidiano__origem_empresa_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->tarefa_cotidiano__origem_logradouro = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->tarefa_cotidiano__origem_numero = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->tarefa_cotidiano__origem_cidade_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->tarefa_cotidiano__origem_latitude_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->tarefa_cotidiano__origem_longitude_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->tarefa_cotidiano__origem_latitude_real_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->tarefa_cotidiano__origem_longitude_real_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->tarefa_cotidiano__destino_pessoa_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->tarefa_cotidiano__destino_empresa_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->tarefa_cotidiano__destino_logradouro = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->tarefa_cotidiano__destino_numero = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->tarefa_cotidiano__destino_cidade_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->tarefa_cotidiano__destino_latitude_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->tarefa_cotidiano__destino_longitude_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->tarefa_cotidiano__destino_latitude_real_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->tarefa_cotidiano__destino_longitude_real_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->tarefa_cotidiano__tempo_estimado_carro_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->tarefa_cotidiano__tempo_estimado_a_pe_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->tarefa_cotidiano__distancia_estimada_carro_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->tarefa_cotidiano__distancia_estimada_a_pe_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->tarefa_cotidiano__is_realizada_a_pe_BOOLEAN = static::TIPO_VARIAVEL_BOOLEAN;
static::$listAliasTypes->tarefa_cotidiano__inicio_hora_programada_SEC = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$listAliasTypes->tarefa_cotidiano__inicio_SEC = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$listAliasTypes->tarefa_cotidiano__fim_SEC = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$listAliasTypes->tarefa_cotidiano__cadastro_SEC = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$listAliasTypes->tarefa_cotidiano__titulo = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->tarefa_cotidiano__descricao = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->tarefa_cotidiano__id_estado_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->tarefa_cotidiano__id_prioridade_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->tarefa_cotidiano__parametro_SEC = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$listAliasTypes->tarefa_cotidiano__parametro_json = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->tarefa_cotidiano__data_limite_cotidiano_SEC = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
            }        
        }
        
        public static function setListAliasRelatedAttributes()
        {
            if(is_null(static::$listAliasRelatedAttributes))
            {
                static::$listAliasRelatedAttributes = new stdClass();
                static::$listAliasRelatedAttributes->categoria_permissao__id = "categoriaPermissaoId";
static::$listAliasRelatedAttributes->veiculo__id = "veiculoId";
static::$listAliasRelatedAttributes->usuario__id = "usuarioId";
static::$listAliasRelatedAttributes->empresa_equipe__nome = "empresaEquipeNome";
static::$listAliasRelatedAttributes->tarefa_cotidiano__id = "id";
static::$listAliasRelatedAttributes->tarefa_cotidiano__criado_pelo_usuario_id_INT = "criadoPeloUsuarioId";
static::$listAliasRelatedAttributes->tarefa_cotidiano__corporacao_id_INT = "corporacaoId";
static::$listAliasRelatedAttributes->tarefa_cotidiano__corporacao_id_INT = "corporacaoId";
static::$listAliasRelatedAttributes->tarefa_cotidiano__cadastro_OFFSEC = "cadastroOffsec";
static::$listAliasRelatedAttributes->tarefa_cotidiano__corporacao_id_INT = "corporacaoId";
static::$listAliasRelatedAttributes->tarefa_cotidiano__origem_pessoa_id_INT = "origemPessoaId";
static::$listAliasRelatedAttributes->tarefa_cotidiano__origem_empresa_id_INT = "origemEmpresaId";
static::$listAliasRelatedAttributes->tarefa_cotidiano__origem_logradouro = "origemLogradouro";
static::$listAliasRelatedAttributes->tarefa_cotidiano__origem_numero = "origemNumero";
static::$listAliasRelatedAttributes->tarefa_cotidiano__origem_cidade_id_INT = "origemCidadeId";
static::$listAliasRelatedAttributes->tarefa_cotidiano__origem_latitude_INT = "origemLatitudeInt";
static::$listAliasRelatedAttributes->tarefa_cotidiano__origem_longitude_INT = "origemLongitudeInt";
static::$listAliasRelatedAttributes->tarefa_cotidiano__origem_latitude_real_INT = "origemLatitudeRealInt";
static::$listAliasRelatedAttributes->tarefa_cotidiano__origem_longitude_real_INT = "origemLongitudeRealInt";
static::$listAliasRelatedAttributes->tarefa_cotidiano__destino_pessoa_id_INT = "destinoPessoaId";
static::$listAliasRelatedAttributes->tarefa_cotidiano__destino_empresa_id_INT = "destinoEmpresaId";
static::$listAliasRelatedAttributes->tarefa_cotidiano__destino_logradouro = "destinoLogradouro";
static::$listAliasRelatedAttributes->tarefa_cotidiano__destino_numero = "destinoNumero";
static::$listAliasRelatedAttributes->tarefa_cotidiano__destino_cidade_id_INT = "destinoCidadeId";
static::$listAliasRelatedAttributes->tarefa_cotidiano__destino_latitude_INT = "destinoLatitudeInt";
static::$listAliasRelatedAttributes->tarefa_cotidiano__destino_longitude_INT = "destinoLongitudeInt";
static::$listAliasRelatedAttributes->tarefa_cotidiano__destino_latitude_real_INT = "destinoLatitudeRealInt";
static::$listAliasRelatedAttributes->tarefa_cotidiano__destino_longitude_real_INT = "destinoLongitudeRealInt";
static::$listAliasRelatedAttributes->tarefa_cotidiano__tempo_estimado_carro_INT = "tempoEstimadoCarroInt";
static::$listAliasRelatedAttributes->tarefa_cotidiano__tempo_estimado_a_pe_INT = "tempoEstimadoAPeInt";
static::$listAliasRelatedAttributes->tarefa_cotidiano__distancia_estimada_carro_INT = "distanciaEstimadaCarroInt";
static::$listAliasRelatedAttributes->tarefa_cotidiano__distancia_estimada_a_pe_INT = "distanciaEstimadaAPeInt";
static::$listAliasRelatedAttributes->tarefa_cotidiano__is_realizada_a_pe_BOOLEAN = "isRealizadaAPeBoolean";
static::$listAliasRelatedAttributes->tarefa_cotidiano__inicio_hora_programada_SEC = "inicioHoraProgramadaSec";
static::$listAliasRelatedAttributes->tarefa_cotidiano__inicio_SEC = "inicioSec";
static::$listAliasRelatedAttributes->tarefa_cotidiano__fim_SEC = "fimSec";
static::$listAliasRelatedAttributes->tarefa_cotidiano__cadastro_SEC = "cadastroSec";
static::$listAliasRelatedAttributes->tarefa_cotidiano__titulo = "titulo";
static::$listAliasRelatedAttributes->tarefa_cotidiano__descricao = "descricao";
static::$listAliasRelatedAttributes->tarefa_cotidiano__id_estado_INT = "idEstadoInt";
static::$listAliasRelatedAttributes->tarefa_cotidiano__id_prioridade_INT = "idPrioridadeInt";
static::$listAliasRelatedAttributes->tarefa_cotidiano__parametro_SEC = "parametroSec";
static::$listAliasRelatedAttributes->tarefa_cotidiano__parametro_json = "parametroJson";
static::$listAliasRelatedAttributes->tarefa_cotidiano__data_limite_cotidiano_SEC = "dataLimiteCotidianoSec";
            }         
        }

        public function __getList($parameters = null, $idCorporacao = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try
            {
                $filterParameters = new stdClass();
                if(isset($parameters->filterParameters))
                {
                    $filterParameters = $parameters->filterParameters;
                }
                                    
                            
            if(is_null($idCorporacao))
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            ListParameters::addFilterParameterToObject($filterParameters, "corporacaoId", $idCorporacao);           
                
                
                $sortingParameters = null;
                if(isset($parameters->sortingParameters))
                {
                    $sortingParameters = $parameters->sortingParameters;
                }
                
                $paginationParameters = null;
                if(isset($parameters->paginationParameters))
                {
                    $paginationParameters = $parameters->paginationParameters;
                }
                
                $listMappingType = static::getListMappingType($parameters->listMappingType);
                $mainTableAlias = "tc";

                $whereClause = static::getWhereClauseForFilter($filterParameters, $mainTableAlias);
                $orderByClause = static::getOrderByClauseForFilter($sortingParameters, $mainTableAlias);
                $limitClause = static::getLimitClauseForFilter($paginationParameters);

                $queryWithoutLimit = "SELECT tc.id FROM tarefa_cotidiano tc {$whereClause}";
                $query = "SELECT cp.id AS categoria_permissao__id, v.id AS veiculo__id, u.id AS usuario__id, ee.nome AS empresa_equipe__nome, tc.id AS tarefa_cotidiano__id, tc.criado_pelo_usuario_id_INT AS tarefa_cotidiano__criado_pelo_usuario_id_INT, tc.corporacao_id_INT AS tarefa_cotidiano__corporacao_id_INT, tc.corporacao_id_INT AS tarefa_cotidiano__corporacao_id_INT, tc.cadastro_OFFSEC AS tarefa_cotidiano__cadastro_OFFSEC, tc.corporacao_id_INT AS tarefa_cotidiano__corporacao_id_INT, tc.origem_pessoa_id_INT AS tarefa_cotidiano__origem_pessoa_id_INT, tc.origem_empresa_id_INT AS tarefa_cotidiano__origem_empresa_id_INT, tc.origem_logradouro AS tarefa_cotidiano__origem_logradouro, tc.origem_numero AS tarefa_cotidiano__origem_numero, tc.origem_cidade_id_INT AS tarefa_cotidiano__origem_cidade_id_INT, tc.origem_latitude_INT AS tarefa_cotidiano__origem_latitude_INT, tc.origem_longitude_INT AS tarefa_cotidiano__origem_longitude_INT, tc.origem_latitude_real_INT AS tarefa_cotidiano__origem_latitude_real_INT, tc.origem_longitude_real_INT AS tarefa_cotidiano__origem_longitude_real_INT, tc.destino_pessoa_id_INT AS tarefa_cotidiano__destino_pessoa_id_INT, tc.destino_empresa_id_INT AS tarefa_cotidiano__destino_empresa_id_INT, tc.destino_logradouro AS tarefa_cotidiano__destino_logradouro, tc.destino_numero AS tarefa_cotidiano__destino_numero, tc.destino_cidade_id_INT AS tarefa_cotidiano__destino_cidade_id_INT, tc.destino_latitude_INT AS tarefa_cotidiano__destino_latitude_INT, tc.destino_longitude_INT AS tarefa_cotidiano__destino_longitude_INT, tc.destino_latitude_real_INT AS tarefa_cotidiano__destino_latitude_real_INT, tc.destino_longitude_real_INT AS tarefa_cotidiano__destino_longitude_real_INT, tc.tempo_estimado_carro_INT AS tarefa_cotidiano__tempo_estimado_carro_INT, tc.tempo_estimado_a_pe_INT AS tarefa_cotidiano__tempo_estimado_a_pe_INT, tc.distancia_estimada_carro_INT AS tarefa_cotidiano__distancia_estimada_carro_INT, tc.distancia_estimada_a_pe_INT AS tarefa_cotidiano__distancia_estimada_a_pe_INT, tc.is_realizada_a_pe_BOOLEAN AS tarefa_cotidiano__is_realizada_a_pe_BOOLEAN, tc.inicio_hora_programada_SEC AS tarefa_cotidiano__inicio_hora_programada_SEC, tc.inicio_SEC AS tarefa_cotidiano__inicio_SEC, tc.fim_SEC AS tarefa_cotidiano__fim_SEC, tc.cadastro_SEC AS tarefa_cotidiano__cadastro_SEC, tc.titulo AS tarefa_cotidiano__titulo, tc.descricao AS tarefa_cotidiano__descricao, tc.id_estado_INT AS tarefa_cotidiano__id_estado_INT, tc.id_prioridade_INT AS tarefa_cotidiano__id_prioridade_INT, tc.parametro_SEC AS tarefa_cotidiano__parametro_SEC, tc.parametro_json AS tarefa_cotidiano__parametro_json, tc.data_limite_cotidiano_SEC AS tarefa_cotidiano__data_limite_cotidiano_SEC FROM tarefa_cotidiano tc LEFT JOIN categoria_permissao cp ON cp.id = tc.categoria_permissao_id_INT LEFT JOIN veiculo v ON v.id = tc.veiculo_id_INT LEFT JOIN usuario u ON u.id = tc.usuario_id_INT LEFT JOIN empresa_equipe ee ON ee.id = tc.empresa_equipe_id_INT {$whereClause} {$orderByClause} {$limitClause}";

                $msg = $this->database->queryMensagem($queryWithoutLimit);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                
                $totalNumberOfRecords = $this->database->rows();
                if($totalNumberOfRecords == 0)
                {
                    return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                }                        
                        
                $msg = $this->database->queryMensagem($query);
                
                //operacao realizaca com sucesso
                if ($msg == null) 
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                    }                        
                
                    $resultSet = $this->database->result;

                    $objReturn = new stdClass();
                    $objReturn->dataSet = static::mapResultSetDataToList($resultSet, $listMappingType, static::$listAliasTypes);

                    if(!Helper::isNullOrEmpty($paginationParameters->recordsPerPage) && !Helper::isNullOrEmpty($paginationParameters->currentPage))
                    {
                        $lastPage = ceil($totalNumberOfRecords / $paginationParameters->recordsPerPage);
                        $objReturn->paginationParameters = new PaginationParams($paginationParameters->recordsPerPage, $paginationParameters->currentPage, $lastPage);
                    }
                    
                    return new Mensagem_generica($objReturn);
                } 
                else
                {
                    return $msg;
                }                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }



public function getId()
            {
                return $this->id;
            }

public function getCriadoPeloUsuarioId()
            {
                return $this->criadoPeloUsuarioId;
            }

public function getCriado_pelo_usuario_id_INT()
                {
                    return $this->criadoPeloUsuarioId;
                }

public function getCategoriaPermissaoId()
            {
                return $this->categoriaPermissaoId;
            }

public function getCategoria_permissao_id_INT()
                {
                    return $this->categoriaPermissaoId;
                }

public function getVeiculoId()
            {
                return $this->veiculoId;
            }

public function getVeiculo_id_INT()
                {
                    return $this->veiculoId;
                }

public function getUsuarioId()
            {
                return $this->usuarioId;
            }

public function getUsuario_id_INT()
                {
                    return $this->usuarioId;
                }

public function getEmpresaEquipeId()
            {
                return $this->empresaEquipeId;
            }

public function getEmpresa_equipe_id_INT()
                {
                    return $this->empresaEquipeId;
                }

public function getOrigemPessoaId()
            {
                return $this->origemPessoaId;
            }

public function getOrigem_pessoa_id_INT()
                {
                    return $this->origemPessoaId;
                }

public function getOrigemEmpresaId()
            {
                return $this->origemEmpresaId;
            }

public function getOrigem_empresa_id_INT()
                {
                    return $this->origemEmpresaId;
                }

public function getOrigemLogradouro()
            {
                return $this->origemLogradouro;
            }

public function getOrigem_logradouro()
                {
                    return $this->origemLogradouro;
                }

public function getOrigemNumero()
            {
                return $this->origemNumero;
            }

public function getOrigem_numero()
                {
                    return $this->origemNumero;
                }

public function getOrigemCidadeId()
            {
                return $this->origemCidadeId;
            }

public function getOrigem_cidade_id_INT()
                {
                    return $this->origemCidadeId;
                }

public function getOrigemLatitudeInt()
            {
                return $this->origemLatitudeInt;
            }

public function getOrigem_latitude_INT()
                {
                    return $this->origemLatitudeInt;
                }

public function getOrigemLongitudeInt()
            {
                return $this->origemLongitudeInt;
            }

public function getOrigem_longitude_INT()
                {
                    return $this->origemLongitudeInt;
                }

public function getOrigemLatitudeRealInt()
            {
                return $this->origemLatitudeRealInt;
            }

public function getOrigem_latitude_real_INT()
                {
                    return $this->origemLatitudeRealInt;
                }

public function getOrigemLongitudeRealInt()
            {
                return $this->origemLongitudeRealInt;
            }

public function getOrigem_longitude_real_INT()
                {
                    return $this->origemLongitudeRealInt;
                }

public function getDestinoPessoaId()
            {
                return $this->destinoPessoaId;
            }

public function getDestino_pessoa_id_INT()
                {
                    return $this->destinoPessoaId;
                }

public function getDestinoEmpresaId()
            {
                return $this->destinoEmpresaId;
            }

public function getDestino_empresa_id_INT()
                {
                    return $this->destinoEmpresaId;
                }

public function getDestinoLogradouro()
            {
                return $this->destinoLogradouro;
            }

public function getDestino_logradouro()
                {
                    return $this->destinoLogradouro;
                }

public function getDestinoNumero()
            {
                return $this->destinoNumero;
            }

public function getDestino_numero()
                {
                    return $this->destinoNumero;
                }

public function getDestinoCidadeId()
            {
                return $this->destinoCidadeId;
            }

public function getDestino_cidade_id_INT()
                {
                    return $this->destinoCidadeId;
                }

public function getDestinoLatitudeInt()
            {
                return $this->destinoLatitudeInt;
            }

public function getDestino_latitude_INT()
                {
                    return $this->destinoLatitudeInt;
                }

public function getDestinoLongitudeInt()
            {
                return $this->destinoLongitudeInt;
            }

public function getDestino_longitude_INT()
                {
                    return $this->destinoLongitudeInt;
                }

public function getDestinoLatitudeRealInt()
            {
                return $this->destinoLatitudeRealInt;
            }

public function getDestino_latitude_real_INT()
                {
                    return $this->destinoLatitudeRealInt;
                }

public function getDestinoLongitudeRealInt()
            {
                return $this->destinoLongitudeRealInt;
            }

public function getDestino_longitude_real_INT()
                {
                    return $this->destinoLongitudeRealInt;
                }

public function getTempoEstimadoCarroInt()
            {
                return $this->tempoEstimadoCarroInt;
            }

public function getTempo_estimado_carro_INT()
                {
                    return $this->tempoEstimadoCarroInt;
                }

public function getTempoEstimadoAPeInt()
            {
                return $this->tempoEstimadoAPeInt;
            }

public function getTempo_estimado_a_pe_INT()
                {
                    return $this->tempoEstimadoAPeInt;
                }

public function getDistanciaEstimadaCarroInt()
            {
                return $this->distanciaEstimadaCarroInt;
            }

public function getDistancia_estimada_carro_INT()
                {
                    return $this->distanciaEstimadaCarroInt;
                }

public function getDistanciaEstimadaAPeInt()
            {
                return $this->distanciaEstimadaAPeInt;
            }

public function getDistancia_estimada_a_pe_INT()
                {
                    return $this->distanciaEstimadaAPeInt;
                }

public function getIsRealizadaAPeBoolean()
            {
                return $this->isRealizadaAPeBoolean;
            }

public function getIs_realizada_a_pe_BOOLEAN()
                {
                    return $this->isRealizadaAPeBoolean;
                }

public function getInicioHoraProgramadaSec()
            {
                return $this->inicioHoraProgramadaSec;
            }

public function getInicio_hora_programada_SEC()
                {
                    return $this->inicioHoraProgramadaSec;
                }

public function getInicioHoraProgramadaOffsec()
            {
                return $this->inicioHoraProgramadaOffsec;
            }

public function getInicio_hora_programada_OFFSEC()
                {
                    return $this->inicioHoraProgramadaOffsec;
                }

public function getInicioSec()
            {
                return $this->inicioSec;
            }

public function getInicio_SEC()
                {
                    return $this->inicioSec;
                }

public function getInicioOffsec()
            {
                return $this->inicioOffsec;
            }

public function getInicio_OFFSEC()
                {
                    return $this->inicioOffsec;
                }

public function getFimSec()
            {
                return $this->fimSec;
            }

public function getFim_SEC()
                {
                    return $this->fimSec;
                }

public function getFimOffsec()
            {
                return $this->fimOffsec;
            }

public function getFim_OFFSEC()
                {
                    return $this->fimOffsec;
                }

public function getCadastroSec()
            {
                return $this->cadastroSec;
            }

public function getCadastro_SEC()
                {
                    return $this->cadastroSec;
                }

public function getCadastroOffsec()
            {
                return $this->cadastroOffsec;
            }

public function getCadastro_OFFSEC()
                {
                    return $this->cadastroOffsec;
                }

public function getTitulo()
            {
                return $this->titulo;
            }

public function getTituloNormalizado()
            {
                return $this->tituloNormalizado;
            }

public function getTitulo_normalizado()
                {
                    return $this->tituloNormalizado;
                }

public function getDescricao()
            {
                return $this->descricao;
            }

public function getCorporacaoId()
            {
                return $this->corporacaoId;
            }

public function getCorporacao_id_INT()
                {
                    return $this->corporacaoId;
                }

public function getAtividadeId()
            {
                return $this->atividadeId;
            }

public function getAtividade_id_INT()
                {
                    return $this->atividadeId;
                }

public function getIdEstadoInt()
            {
                return $this->idEstadoInt;
            }

public function getId_estado_INT()
                {
                    return $this->idEstadoInt;
                }

public function getIdPrioridadeInt()
            {
                return $this->idPrioridadeInt;
            }

public function getId_prioridade_INT()
                {
                    return $this->idPrioridadeInt;
                }

public function getParametroSec()
            {
                return $this->parametroSec;
            }

public function getParametro_SEC()
                {
                    return $this->parametroSec;
                }

public function getParametroOffsec()
            {
                return $this->parametroOffsec;
            }

public function getParametro_OFFSEC()
                {
                    return $this->parametroOffsec;
                }

public function getParametroJson()
            {
                return $this->parametroJson;
            }

public function getParametro_json()
                {
                    return $this->parametroJson;
                }

public function getDataLimiteCotidianoSec()
            {
                return $this->dataLimiteCotidianoSec;
            }

public function getData_limite_cotidiano_SEC()
                {
                    return $this->dataLimiteCotidianoSec;
                }

public function getDataLimiteCotidianoOffsec()
            {
                return $this->dataLimiteCotidianoOffsec;
            }

public function getData_limite_cotidiano_OFFSEC()
                {
                    return $this->dataLimiteCotidianoOffsec;
                }

public function getIdTipoCotidianoInt()
            {
                return $this->idTipoCotidianoInt;
            }

public function getId_tipo_cotidiano_INT()
                {
                    return $this->idTipoCotidianoInt;
                }

public function getPrazoSec()
            {
                return $this->prazoSec;
            }

public function getPrazo_SEC()
                {
                    return $this->prazoSec;
                }

public function getPrazoOffsec()
            {
                return $this->prazoOffsec;
            }

public function getPrazo_OFFSEC()
                {
                    return $this->prazoOffsec;
                }

public function getPercentualCompletoInt()
            {
                return $this->percentualCompletoInt;
            }

public function getPercentual_completo_INT()
                {
                    return $this->percentualCompletoInt;
                }

public function getUltimaGeracaoSec()
            {
                return $this->ultimaGeracaoSec;
            }

public function getUltima_geracao_SEC()
                {
                    return $this->ultimaGeracaoSec;
                }

public function getUltimaGeracaoOffsec()
            {
                return $this->ultimaGeracaoOffsec;
            }

public function getUltima_geracao_OFFSEC()
                {
                    return $this->ultimaGeracaoOffsec;
                }

public function getRelatorioId()
            {
                return $this->relatorioId;
            }

public function getRelatorio_id_INT()
                {
                    return $this->relatorioId;
                }



function setId($value)
            {
                $this->id = $value;
            }

function setCriadoPeloUsuarioId($value)
            {
                $this->criadoPeloUsuarioId = $value;
            }

public function setCriado_pelo_usuario_id_INT($value)
                { 
                    $this->criadoPeloUsuarioId = $value; 
                }

function setCategoriaPermissaoId($value)
            {
                $this->categoriaPermissaoId = $value;
            }

public function setCategoria_permissao_id_INT($value)
                { 
                    $this->categoriaPermissaoId = $value; 
                }

function setVeiculoId($value)
            {
                $this->veiculoId = $value;
            }

public function setVeiculo_id_INT($value)
                { 
                    $this->veiculoId = $value; 
                }

function setUsuarioId($value)
            {
                $this->usuarioId = $value;
            }

public function setUsuario_id_INT($value)
                { 
                    $this->usuarioId = $value; 
                }

function setEmpresaEquipeId($value)
            {
                $this->empresaEquipeId = $value;
            }

public function setEmpresa_equipe_id_INT($value)
                { 
                    $this->empresaEquipeId = $value; 
                }

function setOrigemPessoaId($value)
            {
                $this->origemPessoaId = $value;
            }

public function setOrigem_pessoa_id_INT($value)
                { 
                    $this->origemPessoaId = $value; 
                }

function setOrigemEmpresaId($value)
            {
                $this->origemEmpresaId = $value;
            }

public function setOrigem_empresa_id_INT($value)
                { 
                    $this->origemEmpresaId = $value; 
                }

function setOrigemLogradouro($value)
            {
                $this->origemLogradouro = $value;
            }

public function setOrigem_logradouro($value)
                { 
                    $this->origemLogradouro = $value; 
                }

function setOrigemNumero($value)
            {
                $this->origemNumero = $value;
            }

public function setOrigem_numero($value)
                { 
                    $this->origemNumero = $value; 
                }

function setOrigemCidadeId($value)
            {
                $this->origemCidadeId = $value;
            }

public function setOrigem_cidade_id_INT($value)
                { 
                    $this->origemCidadeId = $value; 
                }

function setOrigemLatitudeInt($value)
            {
                $this->origemLatitudeInt = $value;
            }

public function setOrigem_latitude_INT($value)
                { 
                    $this->origemLatitudeInt = $value; 
                }

function setOrigemLongitudeInt($value)
            {
                $this->origemLongitudeInt = $value;
            }

public function setOrigem_longitude_INT($value)
                { 
                    $this->origemLongitudeInt = $value; 
                }

function setOrigemLatitudeRealInt($value)
            {
                $this->origemLatitudeRealInt = $value;
            }

public function setOrigem_latitude_real_INT($value)
                { 
                    $this->origemLatitudeRealInt = $value; 
                }

function setOrigemLongitudeRealInt($value)
            {
                $this->origemLongitudeRealInt = $value;
            }

public function setOrigem_longitude_real_INT($value)
                { 
                    $this->origemLongitudeRealInt = $value; 
                }

function setDestinoPessoaId($value)
            {
                $this->destinoPessoaId = $value;
            }

public function setDestino_pessoa_id_INT($value)
                { 
                    $this->destinoPessoaId = $value; 
                }

function setDestinoEmpresaId($value)
            {
                $this->destinoEmpresaId = $value;
            }

public function setDestino_empresa_id_INT($value)
                { 
                    $this->destinoEmpresaId = $value; 
                }

function setDestinoLogradouro($value)
            {
                $this->destinoLogradouro = $value;
            }

public function setDestino_logradouro($value)
                { 
                    $this->destinoLogradouro = $value; 
                }

function setDestinoNumero($value)
            {
                $this->destinoNumero = $value;
            }

public function setDestino_numero($value)
                { 
                    $this->destinoNumero = $value; 
                }

function setDestinoCidadeId($value)
            {
                $this->destinoCidadeId = $value;
            }

public function setDestino_cidade_id_INT($value)
                { 
                    $this->destinoCidadeId = $value; 
                }

function setDestinoLatitudeInt($value)
            {
                $this->destinoLatitudeInt = $value;
            }

public function setDestino_latitude_INT($value)
                { 
                    $this->destinoLatitudeInt = $value; 
                }

function setDestinoLongitudeInt($value)
            {
                $this->destinoLongitudeInt = $value;
            }

public function setDestino_longitude_INT($value)
                { 
                    $this->destinoLongitudeInt = $value; 
                }

function setDestinoLatitudeRealInt($value)
            {
                $this->destinoLatitudeRealInt = $value;
            }

public function setDestino_latitude_real_INT($value)
                { 
                    $this->destinoLatitudeRealInt = $value; 
                }

function setDestinoLongitudeRealInt($value)
            {
                $this->destinoLongitudeRealInt = $value;
            }

public function setDestino_longitude_real_INT($value)
                { 
                    $this->destinoLongitudeRealInt = $value; 
                }

function setTempoEstimadoCarroInt($value)
            {
                $this->tempoEstimadoCarroInt = $value;
            }

public function setTempo_estimado_carro_INT($value)
                { 
                    $this->tempoEstimadoCarroInt = $value; 
                }

function setTempoEstimadoAPeInt($value)
            {
                $this->tempoEstimadoAPeInt = $value;
            }

public function setTempo_estimado_a_pe_INT($value)
                { 
                    $this->tempoEstimadoAPeInt = $value; 
                }

function setDistanciaEstimadaCarroInt($value)
            {
                $this->distanciaEstimadaCarroInt = $value;
            }

public function setDistancia_estimada_carro_INT($value)
                { 
                    $this->distanciaEstimadaCarroInt = $value; 
                }

function setDistanciaEstimadaAPeInt($value)
            {
                $this->distanciaEstimadaAPeInt = $value;
            }

public function setDistancia_estimada_a_pe_INT($value)
                { 
                    $this->distanciaEstimadaAPeInt = $value; 
                }

function setIsRealizadaAPeBoolean($value)
            {
                $this->isRealizadaAPeBoolean = $value;
            }

public function setIs_realizada_a_pe_BOOLEAN($value)
                { 
                    $this->isRealizadaAPeBoolean = $value; 
                }

function setInicioHoraProgramadaSec($value)
            {
                $this->inicioHoraProgramadaSec = $value;
            }

public function setInicio_hora_programada_SEC($value)
                { 
                    $this->inicioHoraProgramadaSec = $value; 
                }

function setInicioHoraProgramadaOffsec($value)
            {
                $this->inicioHoraProgramadaOffsec = $value;
            }

public function setInicio_hora_programada_OFFSEC($value)
                { 
                    $this->inicioHoraProgramadaOffsec = $value; 
                }

function setInicioSec($value)
            {
                $this->inicioSec = $value;
            }

public function setInicio_SEC($value)
                { 
                    $this->inicioSec = $value; 
                }

function setInicioOffsec($value)
            {
                $this->inicioOffsec = $value;
            }

public function setInicio_OFFSEC($value)
                { 
                    $this->inicioOffsec = $value; 
                }

function setFimSec($value)
            {
                $this->fimSec = $value;
            }

public function setFim_SEC($value)
                { 
                    $this->fimSec = $value; 
                }

function setFimOffsec($value)
            {
                $this->fimOffsec = $value;
            }

public function setFim_OFFSEC($value)
                { 
                    $this->fimOffsec = $value; 
                }

function setCadastroSec($value)
            {
                $this->cadastroSec = $value;
            }

public function setCadastro_SEC($value)
                { 
                    $this->cadastroSec = $value; 
                }

function setCadastroOffsec($value)
            {
                $this->cadastroOffsec = $value;
            }

public function setCadastro_OFFSEC($value)
                { 
                    $this->cadastroOffsec = $value; 
                }

function setTitulo($value)
            {
                $this->titulo = $value;
            }

function setTituloNormalizado($value)
            {
                $this->tituloNormalizado = $value;
            }

public function setTitulo_normalizado($value)
                { 
                    $this->tituloNormalizado = $value; 
                }

function setDescricao($value)
            {
                $this->descricao = $value;
            }

function setCorporacaoId($value)
            {
                $this->corporacaoId = $value;
            }

public function setCorporacao_id_INT($value)
                { 
                    $this->corporacaoId = $value; 
                }

function setAtividadeId($value)
            {
                $this->atividadeId = $value;
            }

public function setAtividade_id_INT($value)
                { 
                    $this->atividadeId = $value; 
                }

function setIdEstadoInt($value)
            {
                $this->idEstadoInt = $value;
            }

public function setId_estado_INT($value)
                { 
                    $this->idEstadoInt = $value; 
                }

function setIdPrioridadeInt($value)
            {
                $this->idPrioridadeInt = $value;
            }

public function setId_prioridade_INT($value)
                { 
                    $this->idPrioridadeInt = $value; 
                }

function setParametroSec($value)
            {
                $this->parametroSec = $value;
            }

public function setParametro_SEC($value)
                { 
                    $this->parametroSec = $value; 
                }

function setParametroOffsec($value)
            {
                $this->parametroOffsec = $value;
            }

public function setParametro_OFFSEC($value)
                { 
                    $this->parametroOffsec = $value; 
                }

function setParametroJson($value)
            {
                $this->parametroJson = $value;
            }

public function setParametro_json($value)
                { 
                    $this->parametroJson = $value; 
                }

function setDataLimiteCotidianoSec($value)
            {
                $this->dataLimiteCotidianoSec = $value;
            }

public function setData_limite_cotidiano_SEC($value)
                { 
                    $this->dataLimiteCotidianoSec = $value; 
                }

function setDataLimiteCotidianoOffsec($value)
            {
                $this->dataLimiteCotidianoOffsec = $value;
            }

public function setData_limite_cotidiano_OFFSEC($value)
                { 
                    $this->dataLimiteCotidianoOffsec = $value; 
                }

function setIdTipoCotidianoInt($value)
            {
                $this->idTipoCotidianoInt = $value;
            }

public function setId_tipo_cotidiano_INT($value)
                { 
                    $this->idTipoCotidianoInt = $value; 
                }

function setPrazoSec($value)
            {
                $this->prazoSec = $value;
            }

public function setPrazo_SEC($value)
                { 
                    $this->prazoSec = $value; 
                }

function setPrazoOffsec($value)
            {
                $this->prazoOffsec = $value;
            }

public function setPrazo_OFFSEC($value)
                { 
                    $this->prazoOffsec = $value; 
                }

function setPercentualCompletoInt($value)
            {
                $this->percentualCompletoInt = $value;
            }

public function setPercentual_completo_INT($value)
                { 
                    $this->percentualCompletoInt = $value; 
                }

function setUltimaGeracaoSec($value)
            {
                $this->ultimaGeracaoSec = $value;
            }

public function setUltima_geracao_SEC($value)
                { 
                    $this->ultimaGeracaoSec = $value; 
                }

function setUltimaGeracaoOffsec($value)
            {
                $this->ultimaGeracaoOffsec = $value;
            }

public function setUltima_geracao_OFFSEC($value)
                { 
                    $this->ultimaGeracaoOffsec = $value; 
                }

function setRelatorioId($value)
            {
                $this->relatorioId = $value;
            }

public function setRelatorio_id_INT($value)
                { 
                    $this->relatorioId = $value; 
                }



public function clear()
        {$this->id = null;
$this->criadoPeloUsuarioId = null;
if($this->objCriado_pelo_usuario != null) unset($this->objCriado_pelo_usuario);
$this->categoriaPermissaoId = null;
if($this->objCategoria_permissao != null) unset($this->objCategoria_permissao);
$this->veiculoId = null;
if($this->objVeiculo != null) unset($this->objVeiculo);
$this->usuarioId = null;
if($this->objUsuario != null) unset($this->objUsuario);
$this->empresaEquipeId = null;
if($this->objEmpresa_equipe != null) unset($this->objEmpresa_equipe);
$this->origemPessoaId = null;
if($this->objOrigem_pessoa != null) unset($this->objOrigem_pessoa);
$this->origemEmpresaId = null;
if($this->objOrigem_empresa != null) unset($this->objOrigem_empresa);
$this->origemLogradouro = null;
$this->origemNumero = null;
$this->origemCidadeId = null;
if($this->objOrigem_cidade != null) unset($this->objOrigem_cidade);
$this->origemLatitudeInt = null;
$this->origemLongitudeInt = null;
$this->origemLatitudeRealInt = null;
$this->origemLongitudeRealInt = null;
$this->destinoPessoaId = null;
if($this->objDestino_pessoa != null) unset($this->objDestino_pessoa);
$this->destinoEmpresaId = null;
if($this->objDestino_empresa != null) unset($this->objDestino_empresa);
$this->destinoLogradouro = null;
$this->destinoNumero = null;
$this->destinoCidadeId = null;
if($this->objDestino_cidade != null) unset($this->objDestino_cidade);
$this->destinoLatitudeInt = null;
$this->destinoLongitudeInt = null;
$this->destinoLatitudeRealInt = null;
$this->destinoLongitudeRealInt = null;
$this->tempoEstimadoCarroInt = null;
$this->tempoEstimadoAPeInt = null;
$this->distanciaEstimadaCarroInt = null;
$this->distanciaEstimadaAPeInt = null;
$this->isRealizadaAPeBoolean = null;
$this->inicioHoraProgramadaSec = null;
$this->inicioHoraProgramadaOffsec = null;
$this->inicioSec = null;
$this->inicioOffsec = null;
$this->fimSec = null;
$this->fimOffsec = null;
$this->cadastroSec = null;
$this->cadastroOffsec = null;
$this->titulo = null;
$this->tituloNormalizado = null;
$this->descricao = null;
$this->corporacaoId = null;
if($this->objCorporacao != null) unset($this->objCorporacao);
$this->atividadeId = null;
if($this->objAtividade != null) unset($this->objAtividade);
$this->idEstadoInt = null;
$this->idPrioridadeInt = null;
$this->parametroSec = null;
$this->parametroOffsec = null;
$this->parametroJson = null;
$this->dataLimiteCotidianoSec = null;
$this->dataLimiteCotidianoOffsec = null;
$this->idTipoCotidianoInt = null;
$this->prazoSec = null;
$this->prazoOffsec = null;
$this->percentualCompletoInt = null;
$this->ultimaGeracaoSec = null;
$this->ultimaGeracaoOffsec = null;
$this->relatorioId = null;
if($this->objRelatorio != null) unset($this->objRelatorio);
if(is_array($this->arrEstadosCampos)){
                        unset($this->arrEstadosCampos);
                     $this->arrEstadosCampos = array();
                    }
                }

public function formatarParaSQL()
        {
$this->tituloNormalizado = $this->formatarStringNormalizadaParaComandoSQL($this->titulo);
if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->id)){
$this->id = $this->formatarIntegerParaComandoSQL($this->id);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->id);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->criadoPeloUsuarioId)){
$this->criadoPeloUsuarioId = $this->formatarIntegerParaComandoSQL($this->criadoPeloUsuarioId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->criadoPeloUsuarioId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->categoriaPermissaoId)){
$this->categoriaPermissaoId = $this->formatarIntegerParaComandoSQL($this->categoriaPermissaoId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->categoriaPermissaoId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->veiculoId)){
$this->veiculoId = $this->formatarIntegerParaComandoSQL($this->veiculoId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->veiculoId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->usuarioId)){
$this->usuarioId = $this->formatarIntegerParaComandoSQL($this->usuarioId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->usuarioId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->empresaEquipeId)){
$this->empresaEquipeId = $this->formatarIntegerParaComandoSQL($this->empresaEquipeId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->empresaEquipeId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->origemPessoaId)){
$this->origemPessoaId = $this->formatarIntegerParaComandoSQL($this->origemPessoaId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->origemPessoaId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->origemEmpresaId)){
$this->origemEmpresaId = $this->formatarIntegerParaComandoSQL($this->origemEmpresaId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->origemEmpresaId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->origemLogradouro)){
$this->origemLogradouro = $this->formatarStringParaComandoSQL($this->origemLogradouro);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->origemLogradouro);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->origemNumero)){
$this->origemNumero = $this->formatarStringParaComandoSQL($this->origemNumero);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->origemNumero);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->origemCidadeId)){
$this->origemCidadeId = $this->formatarIntegerParaComandoSQL($this->origemCidadeId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->origemCidadeId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->origemLatitudeInt)){
$this->origemLatitudeInt = $this->formatarIntegerParaComandoSQL($this->origemLatitudeInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->origemLatitudeInt);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->origemLongitudeInt)){
$this->origemLongitudeInt = $this->formatarIntegerParaComandoSQL($this->origemLongitudeInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->origemLongitudeInt);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->origemLatitudeRealInt)){
$this->origemLatitudeRealInt = $this->formatarIntegerParaComandoSQL($this->origemLatitudeRealInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->origemLatitudeRealInt);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->origemLongitudeRealInt)){
$this->origemLongitudeRealInt = $this->formatarIntegerParaComandoSQL($this->origemLongitudeRealInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->origemLongitudeRealInt);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->destinoPessoaId)){
$this->destinoPessoaId = $this->formatarIntegerParaComandoSQL($this->destinoPessoaId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->destinoPessoaId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->destinoEmpresaId)){
$this->destinoEmpresaId = $this->formatarIntegerParaComandoSQL($this->destinoEmpresaId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->destinoEmpresaId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->destinoLogradouro)){
$this->destinoLogradouro = $this->formatarStringParaComandoSQL($this->destinoLogradouro);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->destinoLogradouro);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->destinoNumero)){
$this->destinoNumero = $this->formatarStringParaComandoSQL($this->destinoNumero);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->destinoNumero);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->destinoCidadeId)){
$this->destinoCidadeId = $this->formatarIntegerParaComandoSQL($this->destinoCidadeId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->destinoCidadeId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->destinoLatitudeInt)){
$this->destinoLatitudeInt = $this->formatarIntegerParaComandoSQL($this->destinoLatitudeInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->destinoLatitudeInt);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->destinoLongitudeInt)){
$this->destinoLongitudeInt = $this->formatarIntegerParaComandoSQL($this->destinoLongitudeInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->destinoLongitudeInt);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->destinoLatitudeRealInt)){
$this->destinoLatitudeRealInt = $this->formatarIntegerParaComandoSQL($this->destinoLatitudeRealInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->destinoLatitudeRealInt);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->destinoLongitudeRealInt)){
$this->destinoLongitudeRealInt = $this->formatarIntegerParaComandoSQL($this->destinoLongitudeRealInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->destinoLongitudeRealInt);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->tempoEstimadoCarroInt)){
$this->tempoEstimadoCarroInt = $this->formatarIntegerParaComandoSQL($this->tempoEstimadoCarroInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->tempoEstimadoCarroInt);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->tempoEstimadoAPeInt)){
$this->tempoEstimadoAPeInt = $this->formatarIntegerParaComandoSQL($this->tempoEstimadoAPeInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->tempoEstimadoAPeInt);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->distanciaEstimadaCarroInt)){
$this->distanciaEstimadaCarroInt = $this->formatarIntegerParaComandoSQL($this->distanciaEstimadaCarroInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->distanciaEstimadaCarroInt);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->distanciaEstimadaAPeInt)){
$this->distanciaEstimadaAPeInt = $this->formatarIntegerParaComandoSQL($this->distanciaEstimadaAPeInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->distanciaEstimadaAPeInt);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->isRealizadaAPeBoolean)){
$this->isRealizadaAPeBoolean = $this->formatarBooleanParaComandoSQL($this->isRealizadaAPeBoolean);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->isRealizadaAPeBoolean);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->inicioHoraProgramadaSec)){
$this->inicioHoraProgramadaSec = $this->formatarIntegerParaComandoSQL($this->inicioHoraProgramadaSec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->inicioHoraProgramadaSec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->inicioHoraProgramadaOffsec)){
$this->inicioHoraProgramadaOffsec = $this->formatarIntegerParaComandoSQL($this->inicioHoraProgramadaOffsec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->inicioHoraProgramadaOffsec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->inicioSec)){
$this->inicioSec = $this->formatarIntegerParaComandoSQL($this->inicioSec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->inicioSec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->inicioOffsec)){
$this->inicioOffsec = $this->formatarIntegerParaComandoSQL($this->inicioOffsec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->inicioOffsec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->fimSec)){
$this->fimSec = $this->formatarIntegerParaComandoSQL($this->fimSec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->fimSec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->fimOffsec)){
$this->fimOffsec = $this->formatarIntegerParaComandoSQL($this->fimOffsec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->fimOffsec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->cadastroSec)){
$this->cadastroSec = $this->formatarIntegerParaComandoSQL($this->cadastroSec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->cadastroSec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->cadastroOffsec)){
$this->cadastroOffsec = $this->formatarIntegerParaComandoSQL($this->cadastroOffsec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->cadastroOffsec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->titulo)){
$this->titulo = $this->formatarStringParaComandoSQL($this->titulo);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->titulo);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->descricao)){
$this->descricao = $this->formatarStringParaComandoSQL($this->descricao);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->descricao);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->corporacaoId)){
$this->corporacaoId = $this->formatarIntegerParaComandoSQL($this->corporacaoId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->corporacaoId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->atividadeId)){
$this->atividadeId = $this->formatarIntegerParaComandoSQL($this->atividadeId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->atividadeId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->idEstadoInt)){
$this->idEstadoInt = $this->formatarIntegerParaComandoSQL($this->idEstadoInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->idEstadoInt);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->idPrioridadeInt)){
$this->idPrioridadeInt = $this->formatarIntegerParaComandoSQL($this->idPrioridadeInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->idPrioridadeInt);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->parametroSec)){
$this->parametroSec = $this->formatarIntegerParaComandoSQL($this->parametroSec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->parametroSec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->parametroOffsec)){
$this->parametroOffsec = $this->formatarIntegerParaComandoSQL($this->parametroOffsec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->parametroOffsec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->parametroJson)){
$this->parametroJson = $this->formatarStringParaComandoSQL($this->parametroJson);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->parametroJson);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->dataLimiteCotidianoSec)){
$this->dataLimiteCotidianoSec = $this->formatarIntegerParaComandoSQL($this->dataLimiteCotidianoSec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->dataLimiteCotidianoSec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->dataLimiteCotidianoOffsec)){
$this->dataLimiteCotidianoOffsec = $this->formatarIntegerParaComandoSQL($this->dataLimiteCotidianoOffsec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->dataLimiteCotidianoOffsec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->idTipoCotidianoInt)){
$this->idTipoCotidianoInt = $this->formatarIntegerParaComandoSQL($this->idTipoCotidianoInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->idTipoCotidianoInt);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->prazoSec)){
$this->prazoSec = $this->formatarIntegerParaComandoSQL($this->prazoSec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->prazoSec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->prazoOffsec)){
$this->prazoOffsec = $this->formatarIntegerParaComandoSQL($this->prazoOffsec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->prazoOffsec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->percentualCompletoInt)){
$this->percentualCompletoInt = $this->formatarIntegerParaComandoSQL($this->percentualCompletoInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->percentualCompletoInt);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->ultimaGeracaoSec)){
$this->ultimaGeracaoSec = $this->formatarIntegerParaComandoSQL($this->ultimaGeracaoSec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->ultimaGeracaoSec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->ultimaGeracaoOffsec)){
$this->ultimaGeracaoOffsec = $this->formatarIntegerParaComandoSQL($this->ultimaGeracaoOffsec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->ultimaGeracaoOffsec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->relatorioId)){
$this->relatorioId = $this->formatarIntegerParaComandoSQL($this->relatorioId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->relatorioId);
}

}

public function formatarParaExibicao()
        {if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->origemLogradouro)){
$this->origemLogradouro = $this->formatarStringParaExibicao($this->origemLogradouro);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->origemLogradouro);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->origemNumero)){
$this->origemNumero = $this->formatarStringParaExibicao($this->origemNumero);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->origemNumero);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->destinoLogradouro)){
$this->destinoLogradouro = $this->formatarStringParaExibicao($this->destinoLogradouro);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->destinoLogradouro);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->destinoNumero)){
$this->destinoNumero = $this->formatarStringParaExibicao($this->destinoNumero);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->destinoNumero);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->isRealizadaAPeBoolean)){
$this->isRealizadaAPeBoolean = $this->formatarBooleanParaExibicao($this->isRealizadaAPeBoolean);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->isRealizadaAPeBoolean);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->titulo)){
$this->titulo = $this->formatarStringParaExibicao($this->titulo);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->titulo);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->tituloNormalizado)){
$this->tituloNormalizado = $this->formatarStringParaExibicao($this->tituloNormalizado);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->tituloNormalizado);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->descricao)){
$this->descricao = $this->formatarStringParaExibicao($this->descricao);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->descricao);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->parametroJson)){
$this->parametroJson = $this->formatarStringParaExibicao($this->parametroJson);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->parametroJson);
}

}

function select($id, $idCorporacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }   

            $sql = "SELECT id, criado_pelo_usuario_id_INT, categoria_permissao_id_INT, veiculo_id_INT, usuario_id_INT, empresa_equipe_id_INT, origem_pessoa_id_INT, origem_empresa_id_INT, origem_logradouro, origem_numero, origem_cidade_id_INT, origem_latitude_INT, origem_longitude_INT, origem_latitude_real_INT, origem_longitude_real_INT, destino_pessoa_id_INT, destino_empresa_id_INT, destino_logradouro, destino_numero, destino_cidade_id_INT, destino_latitude_INT, destino_longitude_INT, destino_latitude_real_INT, destino_longitude_real_INT, tempo_estimado_carro_INT, tempo_estimado_a_pe_INT, distancia_estimada_carro_INT, distancia_estimada_a_pe_INT, is_realizada_a_pe_BOOLEAN, inicio_hora_programada_SEC, inicio_hora_programada_OFFSEC, inicio_SEC, inicio_OFFSEC, fim_SEC, fim_OFFSEC, cadastro_SEC, cadastro_OFFSEC, titulo, titulo_normalizado, descricao, corporacao_id_INT, atividade_id_INT, id_estado_INT, id_prioridade_INT, parametro_SEC, parametro_OFFSEC, parametro_json, data_limite_cotidiano_SEC, data_limite_cotidiano_OFFSEC, id_tipo_cotidiano_INT, prazo_SEC, prazo_OFFSEC, percentual_completo_INT, ultima_geracao_SEC, ultima_geracao_OFFSEC, relatorio_id_INT FROM tarefa_cotidiano WHERE id = $id AND corporacao_id_INT = $idCorporacao";

            $msg = $this->database->queryMensagem($sql);
            $result = $this->database->result;

            if($msg != null && $msg->erro() )
            { 
            		$this->clear();
            		return $msg;
        	} 
        	else if($this->database->rows() == 0)
        	{
                return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nenhum registro foi encontrado."));
            }
        

            $row = $this->database->fetchArray(MYSQLI_NUM);
            
            		$this->id = $row[0];
		$this->criadoPeloUsuarioId = $row[1];
		$this->categoriaPermissaoId = $row[2];
		$this->veiculoId = $row[3];
		$this->usuarioId = $row[4];
		$this->empresaEquipeId = $row[5];
		$this->origemPessoaId = $row[6];
		$this->origemEmpresaId = $row[7];
		$this->origemLogradouro = $row[8];
		$this->origemNumero = $row[9];
		$this->origemCidadeId = $row[10];
		$this->origemLatitudeInt = $row[11];
		$this->origemLongitudeInt = $row[12];
		$this->origemLatitudeRealInt = $row[13];
		$this->origemLongitudeRealInt = $row[14];
		$this->destinoPessoaId = $row[15];
		$this->destinoEmpresaId = $row[16];
		$this->destinoLogradouro = $row[17];
		$this->destinoNumero = $row[18];
		$this->destinoCidadeId = $row[19];
		$this->destinoLatitudeInt = $row[20];
		$this->destinoLongitudeInt = $row[21];
		$this->destinoLatitudeRealInt = $row[22];
		$this->destinoLongitudeRealInt = $row[23];
		$this->tempoEstimadoCarroInt = $row[24];
		$this->tempoEstimadoAPeInt = $row[25];
		$this->distanciaEstimadaCarroInt = $row[26];
		$this->distanciaEstimadaAPeInt = $row[27];
		$this->isRealizadaAPeBoolean = $row[28];
		$this->inicioHoraProgramadaSec = $row[29];
		$this->inicioHoraProgramadaOffsec = $row[30];
		$this->inicioSec = $row[31];
		$this->inicioOffsec = $row[32];
		$this->fimSec = $row[33];
		$this->fimOffsec = $row[34];
		$this->cadastroSec = $row[35];
		$this->cadastroOffsec = $row[36];
		$this->titulo = $row[37];
		$this->tituloNormalizado = $row[38];
		$this->descricao = $row[39];
		$this->atividadeId = $row[41];
		$this->idEstadoInt = $row[42];
		$this->idPrioridadeInt = $row[43];
		$this->parametroSec = $row[44];
		$this->parametroOffsec = $row[45];
		$this->parametroJson = $row[46];
		$this->dataLimiteCotidianoSec = $row[47];
		$this->dataLimiteCotidianoOffsec = $row[48];
		$this->idTipoCotidianoInt = $row[49];
		$this->prazoSec = $row[50];
		$this->prazoOffsec = $row[51];
		$this->percentualCompletoInt = $row[52];
		$this->ultimaGeracaoSec = $row[53];
		$this->ultimaGeracaoOffsec = $row[54];
		$this->relatorioId = $row[55];

            
        }

public function delete($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (is_null($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }

            $sql = "DELETE FROM tarefa_cotidiano WHERE id= $id AND corporacao_id_INT = $idCorporacao";
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                    && $sincronizar
                        && static::idSistemaTabela() != null
                            && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_REMOVE,
                                null,
                                null,
                                $id,
                                $this->database);
            }

            return $msg;
        }

public function insert($sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null, $opcoes = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $this->defineDataCadastroInSecondsIfNotDefined();
$this->defineDataCadastroOffsetInSecondsIfNotDefined();

            
            if (is_null($opcoes))
            {
                $opcoes = $this->opcoesDAO;
            }

            if (!is_numeric($this->id))
            {
                $this->id = "null";
                if ($opcoes == null || $opcoes[Database::OPCAO_GERAR_ID] != false) 
                {
                    $this->id = EXTDAO_Sistema_sequencia::gerarId(static::nomeTabela());                    
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }    
                    else if($msg == null)
                    {
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
                else 
                {
                    $this->id = null;
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao));
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }
                    else if($msg == null)
                    {
                        $this->id = $this->database->getLastInsertId();
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
            }
            else 
            {
                $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }                        
                else if($msg == null)
                {
                   $msg = new Mensagem_token();
                   $msg->mValor = $this->id;
                }
            }
            if($msg == null || $msg->ok())
            {                
                if($sincronizar
                    && static::idSistemaTabela() != null
                    && isset($this->id)){
                    
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_INSERT,
                                null,
                                null,
                                $this->id,
                                $this->database); 
                	}
            } 
            else
            {
                $this->id = null;
            }
            
            return $msg;
            
        }

public function getSQLInsert($idCorporacao){ 

return "INSERT INTO tarefa_cotidiano (id, criado_pelo_usuario_id_INT, categoria_permissao_id_INT, veiculo_id_INT, usuario_id_INT, empresa_equipe_id_INT, origem_pessoa_id_INT, origem_empresa_id_INT, origem_logradouro, origem_numero, origem_cidade_id_INT, origem_latitude_INT, origem_longitude_INT, origem_latitude_real_INT, origem_longitude_real_INT, destino_pessoa_id_INT, destino_empresa_id_INT, destino_logradouro, destino_numero, destino_cidade_id_INT, destino_latitude_INT, destino_longitude_INT, destino_latitude_real_INT, destino_longitude_real_INT, tempo_estimado_carro_INT, tempo_estimado_a_pe_INT, distancia_estimada_carro_INT, distancia_estimada_a_pe_INT, is_realizada_a_pe_BOOLEAN, inicio_hora_programada_SEC, inicio_hora_programada_OFFSEC, inicio_SEC, inicio_OFFSEC, fim_SEC, fim_OFFSEC, cadastro_SEC, cadastro_OFFSEC, titulo, titulo_normalizado, descricao, corporacao_id_INT, atividade_id_INT, id_estado_INT, id_prioridade_INT, parametro_SEC, parametro_OFFSEC, parametro_json, data_limite_cotidiano_SEC, data_limite_cotidiano_OFFSEC, id_tipo_cotidiano_INT, prazo_SEC, prazo_OFFSEC, percentual_completo_INT, ultima_geracao_SEC, ultima_geracao_OFFSEC, relatorio_id_INT) VALUES ( $this->id ,  $this->criadoPeloUsuarioId ,  $this->categoriaPermissaoId ,  $this->veiculoId ,  $this->usuarioId ,  $this->empresaEquipeId ,  $this->origemPessoaId ,  $this->origemEmpresaId ,  $this->origemLogradouro ,  $this->origemNumero ,  $this->origemCidadeId ,  $this->origemLatitudeInt ,  $this->origemLongitudeInt ,  $this->origemLatitudeRealInt ,  $this->origemLongitudeRealInt ,  $this->destinoPessoaId ,  $this->destinoEmpresaId ,  $this->destinoLogradouro ,  $this->destinoNumero ,  $this->destinoCidadeId ,  $this->destinoLatitudeInt ,  $this->destinoLongitudeInt ,  $this->destinoLatitudeRealInt ,  $this->destinoLongitudeRealInt ,  $this->tempoEstimadoCarroInt ,  $this->tempoEstimadoAPeInt ,  $this->distanciaEstimadaCarroInt ,  $this->distanciaEstimadaAPeInt ,  $this->isRealizadaAPeBoolean ,  $this->inicioHoraProgramadaSec ,  $this->inicioHoraProgramadaOffsec ,  $this->inicioSec ,  $this->inicioOffsec ,  $this->fimSec ,  $this->fimOffsec ,  $this->cadastroSec ,  $this->cadastroOffsec ,  $this->titulo ,  $this->tituloNormalizado ,  $this->descricao ,  $idCorporacao ,  $this->atividadeId ,  $this->idEstadoInt ,  $this->idPrioridadeInt ,  $this->parametroSec ,  $this->parametroOffsec ,  $this->parametroJson ,  $this->dataLimiteCotidianoSec ,  $this->dataLimiteCotidianoOffsec ,  $this->idTipoCotidianoInt ,  $this->prazoSec ,  $this->prazoOffsec ,  $this->percentualCompletoInt ,  $this->ultimaGeracaoSec ,  $this->ultimaGeracaoOffsec ,  $this->relatorioId ) "; 

}




        
        public function updateByParameters($id, $objParametros, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao))
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();

            
                
                if (isset($objParametros->criadoPeloUsuarioId)) 
                {
                    $arrUpdateFields[] = " criado_pelo_usuario_id_INT = {$objParametros->criadoPeloUsuarioId} ";
                }


                
                if (isset($objParametros->categoriaPermissaoId)) 
                {
                    $arrUpdateFields[] = " categoria_permissao_id_INT = {$objParametros->categoriaPermissaoId} ";
                }


                
                if (isset($objParametros->veiculoId)) 
                {
                    $arrUpdateFields[] = " veiculo_id_INT = {$objParametros->veiculoId} ";
                }


                
                if (isset($objParametros->usuarioId)) 
                {
                    $arrUpdateFields[] = " usuario_id_INT = {$objParametros->usuarioId} ";
                }


                
                if (isset($objParametros->empresaEquipeId)) 
                {
                    $arrUpdateFields[] = " empresa_equipe_id_INT = {$objParametros->empresaEquipeId} ";
                }


                
                if (isset($objParametros->origemPessoaId)) 
                {
                    $arrUpdateFields[] = " origem_pessoa_id_INT = {$objParametros->origemPessoaId} ";
                }


                
                if (isset($objParametros->origemEmpresaId)) 
                {
                    $arrUpdateFields[] = " origem_empresa_id_INT = {$objParametros->origemEmpresaId} ";
                }


                
                if (isset($objParametros->origemLogradouro)) 
                {
                    $arrUpdateFields[] = " origem_logradouro = {$objParametros->origemLogradouro} ";
                }


                
                if (isset($objParametros->origemNumero)) 
                {
                    $arrUpdateFields[] = " origem_numero = {$objParametros->origemNumero} ";
                }


                
                if (isset($objParametros->origemCidadeId)) 
                {
                    $arrUpdateFields[] = " origem_cidade_id_INT = {$objParametros->origemCidadeId} ";
                }


                
                if (isset($objParametros->origemLatitudeInt)) 
                {
                    $arrUpdateFields[] = " origem_latitude_INT = {$objParametros->origemLatitudeInt} ";
                }


                
                if (isset($objParametros->origemLongitudeInt)) 
                {
                    $arrUpdateFields[] = " origem_longitude_INT = {$objParametros->origemLongitudeInt} ";
                }


                
                if (isset($objParametros->origemLatitudeRealInt)) 
                {
                    $arrUpdateFields[] = " origem_latitude_real_INT = {$objParametros->origemLatitudeRealInt} ";
                }


                
                if (isset($objParametros->origemLongitudeRealInt)) 
                {
                    $arrUpdateFields[] = " origem_longitude_real_INT = {$objParametros->origemLongitudeRealInt} ";
                }


                
                if (isset($objParametros->destinoPessoaId)) 
                {
                    $arrUpdateFields[] = " destino_pessoa_id_INT = {$objParametros->destinoPessoaId} ";
                }


                
                if (isset($objParametros->destinoEmpresaId)) 
                {
                    $arrUpdateFields[] = " destino_empresa_id_INT = {$objParametros->destinoEmpresaId} ";
                }


                
                if (isset($objParametros->destinoLogradouro)) 
                {
                    $arrUpdateFields[] = " destino_logradouro = {$objParametros->destinoLogradouro} ";
                }


                
                if (isset($objParametros->destinoNumero)) 
                {
                    $arrUpdateFields[] = " destino_numero = {$objParametros->destinoNumero} ";
                }


                
                if (isset($objParametros->destinoCidadeId)) 
                {
                    $arrUpdateFields[] = " destino_cidade_id_INT = {$objParametros->destinoCidadeId} ";
                }


                
                if (isset($objParametros->destinoLatitudeInt)) 
                {
                    $arrUpdateFields[] = " destino_latitude_INT = {$objParametros->destinoLatitudeInt} ";
                }


                
                if (isset($objParametros->destinoLongitudeInt)) 
                {
                    $arrUpdateFields[] = " destino_longitude_INT = {$objParametros->destinoLongitudeInt} ";
                }


                
                if (isset($objParametros->destinoLatitudeRealInt)) 
                {
                    $arrUpdateFields[] = " destino_latitude_real_INT = {$objParametros->destinoLatitudeRealInt} ";
                }


                
                if (isset($objParametros->destinoLongitudeRealInt)) 
                {
                    $arrUpdateFields[] = " destino_longitude_real_INT = {$objParametros->destinoLongitudeRealInt} ";
                }


                
                if (isset($objParametros->tempoEstimadoCarroInt)) 
                {
                    $arrUpdateFields[] = " tempo_estimado_carro_INT = {$objParametros->tempoEstimadoCarroInt} ";
                }


                
                if (isset($objParametros->tempoEstimadoAPeInt)) 
                {
                    $arrUpdateFields[] = " tempo_estimado_a_pe_INT = {$objParametros->tempoEstimadoAPeInt} ";
                }


                
                if (isset($objParametros->distanciaEstimadaCarroInt)) 
                {
                    $arrUpdateFields[] = " distancia_estimada_carro_INT = {$objParametros->distanciaEstimadaCarroInt} ";
                }


                
                if (isset($objParametros->distanciaEstimadaAPeInt)) 
                {
                    $arrUpdateFields[] = " distancia_estimada_a_pe_INT = {$objParametros->distanciaEstimadaAPeInt} ";
                }


                
                if (isset($objParametros->isRealizadaAPeBoolean)) 
                {
                    $arrUpdateFields[] = " is_realizada_a_pe_BOOLEAN = {$objParametros->isRealizadaAPeBoolean} ";
                }


                
                if (isset($objParametros->inicioHoraProgramadaSec)) 
                {
                    $arrUpdateFields[] = " inicio_hora_programada_SEC = {$objParametros->inicioHoraProgramadaSec} ";
                }


                
                if (isset($objParametros->inicioHoraProgramadaOffsec)) 
                {
                    $arrUpdateFields[] = " inicio_hora_programada_OFFSEC = {$objParametros->inicioHoraProgramadaOffsec} ";
                }


                
                if (isset($objParametros->inicioSec)) 
                {
                    $arrUpdateFields[] = " inicio_SEC = {$objParametros->inicioSec} ";
                }


                
                if (isset($objParametros->inicioOffsec)) 
                {
                    $arrUpdateFields[] = " inicio_OFFSEC = {$objParametros->inicioOffsec} ";
                }


                
                if (isset($objParametros->fimSec)) 
                {
                    $arrUpdateFields[] = " fim_SEC = {$objParametros->fimSec} ";
                }


                
                if (isset($objParametros->fimOffsec)) 
                {
                    $arrUpdateFields[] = " fim_OFFSEC = {$objParametros->fimOffsec} ";
                }


                
                if (isset($objParametros->cadastroSec)) 
                {
                    $arrUpdateFields[] = " cadastro_SEC = {$objParametros->cadastroSec} ";
                }


                
                if (isset($objParametros->cadastroOffsec)) 
                {
                    $arrUpdateFields[] = " cadastro_OFFSEC = {$objParametros->cadastroOffsec} ";
                }


                
                if (isset($objParametros->titulo)) 
                {
                    $arrUpdateFields[] = " titulo = {$objParametros->titulo} ";
                }


                
                if (isset($objParametros->tituloNormalizado)) 
                {
                    $arrUpdateFields[] = " titulo_normalizado = {$objParametros->tituloNormalizado} ";
                }


                
                if (isset($objParametros->descricao)) 
                {
                    $arrUpdateFields[] = " descricao = {$objParametros->descricao} ";
                }


                
                if (isset($objParametros->atividadeId)) 
                {
                    $arrUpdateFields[] = " atividade_id_INT = {$objParametros->atividadeId} ";
                }


                
                if (isset($objParametros->idEstadoInt)) 
                {
                    $arrUpdateFields[] = " id_estado_INT = {$objParametros->idEstadoInt} ";
                }


                
                if (isset($objParametros->idPrioridadeInt)) 
                {
                    $arrUpdateFields[] = " id_prioridade_INT = {$objParametros->idPrioridadeInt} ";
                }


                
                if (isset($objParametros->parametroSec)) 
                {
                    $arrUpdateFields[] = " parametro_SEC = {$objParametros->parametroSec} ";
                }


                
                if (isset($objParametros->parametroOffsec)) 
                {
                    $arrUpdateFields[] = " parametro_OFFSEC = {$objParametros->parametroOffsec} ";
                }


                
                if (isset($objParametros->parametroJson)) 
                {
                    $arrUpdateFields[] = " parametro_json = {$objParametros->parametroJson} ";
                }


                
                if (isset($objParametros->dataLimiteCotidianoSec)) 
                {
                    $arrUpdateFields[] = " data_limite_cotidiano_SEC = {$objParametros->dataLimiteCotidianoSec} ";
                }


                
                if (isset($objParametros->dataLimiteCotidianoOffsec)) 
                {
                    $arrUpdateFields[] = " data_limite_cotidiano_OFFSEC = {$objParametros->dataLimiteCotidianoOffsec} ";
                }


                
                if (isset($objParametros->idTipoCotidianoInt)) 
                {
                    $arrUpdateFields[] = " id_tipo_cotidiano_INT = {$objParametros->idTipoCotidianoInt} ";
                }


                
                if (isset($objParametros->prazoSec)) 
                {
                    $arrUpdateFields[] = " prazo_SEC = {$objParametros->prazoSec} ";
                }


                
                if (isset($objParametros->prazoOffsec)) 
                {
                    $arrUpdateFields[] = " prazo_OFFSEC = {$objParametros->prazoOffsec} ";
                }


                
                if (isset($objParametros->percentualCompletoInt)) 
                {
                    $arrUpdateFields[] = " percentual_completo_INT = {$objParametros->percentualCompletoInt} ";
                }


                
                if (isset($objParametros->ultimaGeracaoSec)) 
                {
                    $arrUpdateFields[] = " ultima_geracao_SEC = {$objParametros->ultimaGeracaoSec} ";
                }


                
                if (isset($objParametros->ultimaGeracaoOffsec)) 
                {
                    $arrUpdateFields[] = " ultima_geracao_OFFSEC = {$objParametros->ultimaGeracaoOffsec} ";
                }


                
                if (isset($objParametros->relatorioId)) 
                {
                    $arrUpdateFields[] = " relatorio_id_INT = {$objParametros->relatorioId} ";
                }



            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE tarefa_cotidiano SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";

            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null)
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function update($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($this->criadoPeloUsuarioId)) 
                {                                      
                    $arrUpdateFields[] = " criado_pelo_usuario_id_INT = {$this->criadoPeloUsuarioId} ";
                }


                
                if (isset($this->categoriaPermissaoId)) 
                {                                      
                    $arrUpdateFields[] = " categoria_permissao_id_INT = {$this->categoriaPermissaoId} ";
                }


                
                if (isset($this->veiculoId)) 
                {                                      
                    $arrUpdateFields[] = " veiculo_id_INT = {$this->veiculoId} ";
                }


                
                if (isset($this->usuarioId)) 
                {                                      
                    $arrUpdateFields[] = " usuario_id_INT = {$this->usuarioId} ";
                }


                
                if (isset($this->empresaEquipeId)) 
                {                                      
                    $arrUpdateFields[] = " empresa_equipe_id_INT = {$this->empresaEquipeId} ";
                }


                
                if (isset($this->origemPessoaId)) 
                {                                      
                    $arrUpdateFields[] = " origem_pessoa_id_INT = {$this->origemPessoaId} ";
                }


                
                if (isset($this->origemEmpresaId)) 
                {                                      
                    $arrUpdateFields[] = " origem_empresa_id_INT = {$this->origemEmpresaId} ";
                }


                
                if (isset($this->origemLogradouro)) 
                {                                      
                    $arrUpdateFields[] = " origem_logradouro = {$this->origemLogradouro} ";
                }


                
                if (isset($this->origemNumero)) 
                {                                      
                    $arrUpdateFields[] = " origem_numero = {$this->origemNumero} ";
                }


                
                if (isset($this->origemCidadeId)) 
                {                                      
                    $arrUpdateFields[] = " origem_cidade_id_INT = {$this->origemCidadeId} ";
                }


                
                if (isset($this->origemLatitudeInt)) 
                {                                      
                    $arrUpdateFields[] = " origem_latitude_INT = {$this->origemLatitudeInt} ";
                }


                
                if (isset($this->origemLongitudeInt)) 
                {                                      
                    $arrUpdateFields[] = " origem_longitude_INT = {$this->origemLongitudeInt} ";
                }


                
                if (isset($this->origemLatitudeRealInt)) 
                {                                      
                    $arrUpdateFields[] = " origem_latitude_real_INT = {$this->origemLatitudeRealInt} ";
                }


                
                if (isset($this->origemLongitudeRealInt)) 
                {                                      
                    $arrUpdateFields[] = " origem_longitude_real_INT = {$this->origemLongitudeRealInt} ";
                }


                
                if (isset($this->destinoPessoaId)) 
                {                                      
                    $arrUpdateFields[] = " destino_pessoa_id_INT = {$this->destinoPessoaId} ";
                }


                
                if (isset($this->destinoEmpresaId)) 
                {                                      
                    $arrUpdateFields[] = " destino_empresa_id_INT = {$this->destinoEmpresaId} ";
                }


                
                if (isset($this->destinoLogradouro)) 
                {                                      
                    $arrUpdateFields[] = " destino_logradouro = {$this->destinoLogradouro} ";
                }


                
                if (isset($this->destinoNumero)) 
                {                                      
                    $arrUpdateFields[] = " destino_numero = {$this->destinoNumero} ";
                }


                
                if (isset($this->destinoCidadeId)) 
                {                                      
                    $arrUpdateFields[] = " destino_cidade_id_INT = {$this->destinoCidadeId} ";
                }


                
                if (isset($this->destinoLatitudeInt)) 
                {                                      
                    $arrUpdateFields[] = " destino_latitude_INT = {$this->destinoLatitudeInt} ";
                }


                
                if (isset($this->destinoLongitudeInt)) 
                {                                      
                    $arrUpdateFields[] = " destino_longitude_INT = {$this->destinoLongitudeInt} ";
                }


                
                if (isset($this->destinoLatitudeRealInt)) 
                {                                      
                    $arrUpdateFields[] = " destino_latitude_real_INT = {$this->destinoLatitudeRealInt} ";
                }


                
                if (isset($this->destinoLongitudeRealInt)) 
                {                                      
                    $arrUpdateFields[] = " destino_longitude_real_INT = {$this->destinoLongitudeRealInt} ";
                }


                
                if (isset($this->tempoEstimadoCarroInt)) 
                {                                      
                    $arrUpdateFields[] = " tempo_estimado_carro_INT = {$this->tempoEstimadoCarroInt} ";
                }


                
                if (isset($this->tempoEstimadoAPeInt)) 
                {                                      
                    $arrUpdateFields[] = " tempo_estimado_a_pe_INT = {$this->tempoEstimadoAPeInt} ";
                }


                
                if (isset($this->distanciaEstimadaCarroInt)) 
                {                                      
                    $arrUpdateFields[] = " distancia_estimada_carro_INT = {$this->distanciaEstimadaCarroInt} ";
                }


                
                if (isset($this->distanciaEstimadaAPeInt)) 
                {                                      
                    $arrUpdateFields[] = " distancia_estimada_a_pe_INT = {$this->distanciaEstimadaAPeInt} ";
                }


                
                if (isset($this->isRealizadaAPeBoolean)) 
                {                                      
                    $arrUpdateFields[] = " is_realizada_a_pe_BOOLEAN = {$this->isRealizadaAPeBoolean} ";
                }


                
                if (isset($this->inicioHoraProgramadaSec)) 
                {                                      
                    $arrUpdateFields[] = " inicio_hora_programada_SEC = {$this->inicioHoraProgramadaSec} ";
                }


                
                if (isset($this->inicioHoraProgramadaOffsec)) 
                {                                      
                    $arrUpdateFields[] = " inicio_hora_programada_OFFSEC = {$this->inicioHoraProgramadaOffsec} ";
                }


                
                if (isset($this->inicioSec)) 
                {                                      
                    $arrUpdateFields[] = " inicio_SEC = {$this->inicioSec} ";
                }


                
                if (isset($this->inicioOffsec)) 
                {                                      
                    $arrUpdateFields[] = " inicio_OFFSEC = {$this->inicioOffsec} ";
                }


                
                if (isset($this->fimSec)) 
                {                                      
                    $arrUpdateFields[] = " fim_SEC = {$this->fimSec} ";
                }


                
                if (isset($this->fimOffsec)) 
                {                                      
                    $arrUpdateFields[] = " fim_OFFSEC = {$this->fimOffsec} ";
                }


                
                if (isset($this->cadastroSec)) 
                {                                      
                    $arrUpdateFields[] = " cadastro_SEC = {$this->cadastroSec} ";
                }


                
                if (isset($this->cadastroOffsec)) 
                {                                      
                    $arrUpdateFields[] = " cadastro_OFFSEC = {$this->cadastroOffsec} ";
                }


                
                if (isset($this->titulo)) 
                {                                      
                    $arrUpdateFields[] = " titulo = {$this->titulo} ";
                }


                
                if (isset($this->tituloNormalizado)) 
                {                                      
                    $arrUpdateFields[] = " titulo_normalizado = {$this->tituloNormalizado} ";
                }


                
                if (isset($this->descricao)) 
                {                                      
                    $arrUpdateFields[] = " descricao = {$this->descricao} ";
                }


                
                if (isset($this->atividadeId)) 
                {                                      
                    $arrUpdateFields[] = " atividade_id_INT = {$this->atividadeId} ";
                }


                
                if (isset($this->idEstadoInt)) 
                {                                      
                    $arrUpdateFields[] = " id_estado_INT = {$this->idEstadoInt} ";
                }


                
                if (isset($this->idPrioridadeInt)) 
                {                                      
                    $arrUpdateFields[] = " id_prioridade_INT = {$this->idPrioridadeInt} ";
                }


                
                if (isset($this->parametroSec)) 
                {                                      
                    $arrUpdateFields[] = " parametro_SEC = {$this->parametroSec} ";
                }


                
                if (isset($this->parametroOffsec)) 
                {                                      
                    $arrUpdateFields[] = " parametro_OFFSEC = {$this->parametroOffsec} ";
                }


                
                if (isset($this->parametroJson)) 
                {                                      
                    $arrUpdateFields[] = " parametro_json = {$this->parametroJson} ";
                }


                
                if (isset($this->dataLimiteCotidianoSec)) 
                {                                      
                    $arrUpdateFields[] = " data_limite_cotidiano_SEC = {$this->dataLimiteCotidianoSec} ";
                }


                
                if (isset($this->dataLimiteCotidianoOffsec)) 
                {                                      
                    $arrUpdateFields[] = " data_limite_cotidiano_OFFSEC = {$this->dataLimiteCotidianoOffsec} ";
                }


                
                if (isset($this->idTipoCotidianoInt)) 
                {                                      
                    $arrUpdateFields[] = " id_tipo_cotidiano_INT = {$this->idTipoCotidianoInt} ";
                }


                
                if (isset($this->prazoSec)) 
                {                                      
                    $arrUpdateFields[] = " prazo_SEC = {$this->prazoSec} ";
                }


                
                if (isset($this->prazoOffsec)) 
                {                                      
                    $arrUpdateFields[] = " prazo_OFFSEC = {$this->prazoOffsec} ";
                }


                
                if (isset($this->percentualCompletoInt)) 
                {                                      
                    $arrUpdateFields[] = " percentual_completo_INT = {$this->percentualCompletoInt} ";
                }


                
                if (isset($this->ultimaGeracaoSec)) 
                {                                      
                    $arrUpdateFields[] = " ultima_geracao_SEC = {$this->ultimaGeracaoSec} ";
                }


                
                if (isset($this->ultimaGeracaoOffsec)) 
                {                                      
                    $arrUpdateFields[] = " ultima_geracao_OFFSEC = {$this->ultimaGeracaoOffsec} ";
                }


                
                if (isset($this->relatorioId)) 
                {                                      
                    $arrUpdateFields[] = " relatorio_id_INT = {$this->relatorioId} ";
                }


            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE tarefa_cotidiano SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function updateByInstanceUsingParameters($id, $parameters, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($parameters->criadoPeloUsuarioId)) 
                {                                      
                    $arrUpdateFields[] = " criado_pelo_usuario_id_INT = {$this->criadoPeloUsuarioId} ";
                }
                
                if (isset($parameters->categoriaPermissaoId)) 
                {                                      
                    $arrUpdateFields[] = " categoria_permissao_id_INT = {$this->categoriaPermissaoId} ";
                }
                
                if (isset($parameters->veiculoId)) 
                {                                      
                    $arrUpdateFields[] = " veiculo_id_INT = {$this->veiculoId} ";
                }
                
                if (isset($parameters->usuarioId)) 
                {                                      
                    $arrUpdateFields[] = " usuario_id_INT = {$this->usuarioId} ";
                }
                
                if (isset($parameters->empresaEquipeId)) 
                {                                      
                    $arrUpdateFields[] = " empresa_equipe_id_INT = {$this->empresaEquipeId} ";
                }
                
                if (isset($parameters->origemPessoaId)) 
                {                                      
                    $arrUpdateFields[] = " origem_pessoa_id_INT = {$this->origemPessoaId} ";
                }
                
                if (isset($parameters->origemEmpresaId)) 
                {                                      
                    $arrUpdateFields[] = " origem_empresa_id_INT = {$this->origemEmpresaId} ";
                }
                
                if (isset($parameters->origemLogradouro)) 
                {                                      
                    $arrUpdateFields[] = " origem_logradouro = {$this->origemLogradouro} ";
                }
                
                if (isset($parameters->origemNumero)) 
                {                                      
                    $arrUpdateFields[] = " origem_numero = {$this->origemNumero} ";
                }
                
                if (isset($parameters->origemCidadeId)) 
                {                                      
                    $arrUpdateFields[] = " origem_cidade_id_INT = {$this->origemCidadeId} ";
                }
                
                if (isset($parameters->origemLatitudeInt)) 
                {                                      
                    $arrUpdateFields[] = " origem_latitude_INT = {$this->origemLatitudeInt} ";
                }
                
                if (isset($parameters->origemLongitudeInt)) 
                {                                      
                    $arrUpdateFields[] = " origem_longitude_INT = {$this->origemLongitudeInt} ";
                }
                
                if (isset($parameters->origemLatitudeRealInt)) 
                {                                      
                    $arrUpdateFields[] = " origem_latitude_real_INT = {$this->origemLatitudeRealInt} ";
                }
                
                if (isset($parameters->origemLongitudeRealInt)) 
                {                                      
                    $arrUpdateFields[] = " origem_longitude_real_INT = {$this->origemLongitudeRealInt} ";
                }
                
                if (isset($parameters->destinoPessoaId)) 
                {                                      
                    $arrUpdateFields[] = " destino_pessoa_id_INT = {$this->destinoPessoaId} ";
                }
                
                if (isset($parameters->destinoEmpresaId)) 
                {                                      
                    $arrUpdateFields[] = " destino_empresa_id_INT = {$this->destinoEmpresaId} ";
                }
                
                if (isset($parameters->destinoLogradouro)) 
                {                                      
                    $arrUpdateFields[] = " destino_logradouro = {$this->destinoLogradouro} ";
                }
                
                if (isset($parameters->destinoNumero)) 
                {                                      
                    $arrUpdateFields[] = " destino_numero = {$this->destinoNumero} ";
                }
                
                if (isset($parameters->destinoCidadeId)) 
                {                                      
                    $arrUpdateFields[] = " destino_cidade_id_INT = {$this->destinoCidadeId} ";
                }
                
                if (isset($parameters->destinoLatitudeInt)) 
                {                                      
                    $arrUpdateFields[] = " destino_latitude_INT = {$this->destinoLatitudeInt} ";
                }
                
                if (isset($parameters->destinoLongitudeInt)) 
                {                                      
                    $arrUpdateFields[] = " destino_longitude_INT = {$this->destinoLongitudeInt} ";
                }
                
                if (isset($parameters->destinoLatitudeRealInt)) 
                {                                      
                    $arrUpdateFields[] = " destino_latitude_real_INT = {$this->destinoLatitudeRealInt} ";
                }
                
                if (isset($parameters->destinoLongitudeRealInt)) 
                {                                      
                    $arrUpdateFields[] = " destino_longitude_real_INT = {$this->destinoLongitudeRealInt} ";
                }
                
                if (isset($parameters->tempoEstimadoCarroInt)) 
                {                                      
                    $arrUpdateFields[] = " tempo_estimado_carro_INT = {$this->tempoEstimadoCarroInt} ";
                }
                
                if (isset($parameters->tempoEstimadoAPeInt)) 
                {                                      
                    $arrUpdateFields[] = " tempo_estimado_a_pe_INT = {$this->tempoEstimadoAPeInt} ";
                }
                
                if (isset($parameters->distanciaEstimadaCarroInt)) 
                {                                      
                    $arrUpdateFields[] = " distancia_estimada_carro_INT = {$this->distanciaEstimadaCarroInt} ";
                }
                
                if (isset($parameters->distanciaEstimadaAPeInt)) 
                {                                      
                    $arrUpdateFields[] = " distancia_estimada_a_pe_INT = {$this->distanciaEstimadaAPeInt} ";
                }
                
                if (isset($parameters->isRealizadaAPeBoolean)) 
                {                                      
                    $arrUpdateFields[] = " is_realizada_a_pe_BOOLEAN = {$this->isRealizadaAPeBoolean} ";
                }
                
                if (isset($parameters->inicioHoraProgramadaSec)) 
                {                                      
                    $arrUpdateFields[] = " inicio_hora_programada_SEC = {$this->inicioHoraProgramadaSec} ";
                }
                
                if (isset($parameters->inicioHoraProgramadaOffsec)) 
                {                                      
                    $arrUpdateFields[] = " inicio_hora_programada_OFFSEC = {$this->inicioHoraProgramadaOffsec} ";
                }
                
                if (isset($parameters->inicioSec)) 
                {                                      
                    $arrUpdateFields[] = " inicio_SEC = {$this->inicioSec} ";
                }
                
                if (isset($parameters->inicioOffsec)) 
                {                                      
                    $arrUpdateFields[] = " inicio_OFFSEC = {$this->inicioOffsec} ";
                }
                
                if (isset($parameters->fimSec)) 
                {                                      
                    $arrUpdateFields[] = " fim_SEC = {$this->fimSec} ";
                }
                
                if (isset($parameters->fimOffsec)) 
                {                                      
                    $arrUpdateFields[] = " fim_OFFSEC = {$this->fimOffsec} ";
                }
                
                if (isset($parameters->cadastroSec)) 
                {                                      
                    $arrUpdateFields[] = " cadastro_SEC = {$this->cadastroSec} ";
                }
                
                if (isset($parameters->cadastroOffsec)) 
                {                                      
                    $arrUpdateFields[] = " cadastro_OFFSEC = {$this->cadastroOffsec} ";
                }
                
                if (isset($parameters->titulo)) 
                {                                      
                    $arrUpdateFields[] = " titulo = {$this->titulo} ";
                }
                
                if (isset($parameters->tituloNormalizado)) 
                {                                      
                    $arrUpdateFields[] = " titulo_normalizado = {$this->tituloNormalizado} ";
                }
                
                if (isset($parameters->descricao)) 
                {                                      
                    $arrUpdateFields[] = " descricao = {$this->descricao} ";
                }
                
                if (isset($parameters->atividadeId)) 
                {                                      
                    $arrUpdateFields[] = " atividade_id_INT = {$this->atividadeId} ";
                }
                
                if (isset($parameters->idEstadoInt)) 
                {                                      
                    $arrUpdateFields[] = " id_estado_INT = {$this->idEstadoInt} ";
                }
                
                if (isset($parameters->idPrioridadeInt)) 
                {                                      
                    $arrUpdateFields[] = " id_prioridade_INT = {$this->idPrioridadeInt} ";
                }
                
                if (isset($parameters->parametroSec)) 
                {                                      
                    $arrUpdateFields[] = " parametro_SEC = {$this->parametroSec} ";
                }
                
                if (isset($parameters->parametroOffsec)) 
                {                                      
                    $arrUpdateFields[] = " parametro_OFFSEC = {$this->parametroOffsec} ";
                }
                
                if (isset($parameters->parametroJson)) 
                {                                      
                    $arrUpdateFields[] = " parametro_json = {$this->parametroJson} ";
                }
                
                if (isset($parameters->dataLimiteCotidianoSec)) 
                {                                      
                    $arrUpdateFields[] = " data_limite_cotidiano_SEC = {$this->dataLimiteCotidianoSec} ";
                }
                
                if (isset($parameters->dataLimiteCotidianoOffsec)) 
                {                                      
                    $arrUpdateFields[] = " data_limite_cotidiano_OFFSEC = {$this->dataLimiteCotidianoOffsec} ";
                }
                
                if (isset($parameters->idTipoCotidianoInt)) 
                {                                      
                    $arrUpdateFields[] = " id_tipo_cotidiano_INT = {$this->idTipoCotidianoInt} ";
                }
                
                if (isset($parameters->prazoSec)) 
                {                                      
                    $arrUpdateFields[] = " prazo_SEC = {$this->prazoSec} ";
                }
                
                if (isset($parameters->prazoOffsec)) 
                {                                      
                    $arrUpdateFields[] = " prazo_OFFSEC = {$this->prazoOffsec} ";
                }
                
                if (isset($parameters->percentualCompletoInt)) 
                {                                      
                    $arrUpdateFields[] = " percentual_completo_INT = {$this->percentualCompletoInt} ";
                }
                
                if (isset($parameters->ultimaGeracaoSec)) 
                {                                      
                    $arrUpdateFields[] = " ultima_geracao_SEC = {$this->ultimaGeracaoSec} ";
                }
                
                if (isset($parameters->ultimaGeracaoOffsec)) 
                {                                      
                    $arrUpdateFields[] = " ultima_geracao_OFFSEC = {$this->ultimaGeracaoOffsec} ";
                }
                
                if (isset($parameters->relatorioId)) 
                {                                      
                    $arrUpdateFields[] = " relatorio_id_INT = {$this->relatorioId} ";
                }
            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE tarefa_cotidiano SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }

}

?>
