/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "app" (
  "id" int(11) NOT NULL AUTO_INCREMENT,
  "nome" varchar(50) NOT NULL,
  "nome_normalizado" varchar(50) NOT NULL,
  "data_lancamento_DATE" date DEFAULT NULL,
  "link_download" varchar(512) DEFAULT NULL,
  "total_dia_trial_INT" int(5) DEFAULT NULL,
  PRIMARY KEY ("id")
);
/*!40101 SET character_set_client = @saved_cs_client */;
INSERT INTO "app" VALUES (5,'Omega Ponto Eletr�nico','Omega Ponto Eletronico',NULL,NULL,NULL);
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "bairro" (
  "id" int(11) NOT NULL AUTO_INCREMENT,
  "nome" varchar(100) NOT NULL,
  "nome_normalizado" varchar(100) NOT NULL,
  "cidade_id_INT" int(11) NOT NULL,
  "corporacao_id_INT" int(11) NOT NULL,
  PRIMARY KEY ("id"),
  UNIQUE KEY "nome" ("nome_normalizado","cidade_id_INT","corporacao_id_INT"),
  KEY "b_cidade_FK" ("cidade_id_INT"),
  KEY "bairro_FK_545776367" ("corporacao_id_INT"),
  CONSTRAINT "bairro_FK_545776367" FOREIGN KEY ("corporacao_id_INT") REFERENCES "corporacao" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "bairro_ibfk_1" FOREIGN KEY ("cidade_id_INT") REFERENCES "cidade" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);
/*!40101 SET character_set_client = @saved_cs_client */;
INSERT INTO "bairro" VALUES (1,'CENTRO','CENTRO',1,1);
INSERT INTO "bairro" VALUES (2,'JARDIM PLANALTO','JARDIM PLANALTO',5,1);
INSERT INTO "bairro" VALUES (3,'CENTRO COMERCIAL','CENTRO COMERCIAL',5,1);
INSERT INTO "bairro" VALUES (4,'VILA DOS REM�DIOS','VILA DOS REMEDIOS',1,1);
INSERT INTO "bairro" VALUES (5,'HELENA MARIA','HELENA MARIA',3,1);
INSERT INTO "bairro" VALUES (6,'CENTRO','CENTRO',3,1);
INSERT INTO "bairro" VALUES (7,'SANTA CEC�LIA','SANTA CECILIA',1,1);
INSERT INTO "bairro" VALUES (8,'CONSOLA��O','CONSOLACAO',1,1);
INSERT INTO "bairro" VALUES (9,'PARQUE CONTINENTAL','PARQUE CONTINENTAL',3,1);
INSERT INTO "bairro" VALUES (10,'ALTO DE PINHEIROS','ALTO DE PINHEIROS',1,1);
INSERT INTO "bairro" VALUES (11,'ALPHAVILLE II','ALPHAVILLE II',4,1);
INSERT INTO "bairro" VALUES (12,'BARRA FUNDA','BARRA FUNDA',1,1);
INSERT INTO "bairro" VALUES (13,'ITAIM BIBI','ITAIM BIBI',1,1);
INSERT INTO "bairro" VALUES (14,'REP�BLICA','REPUBLICA',1,1);
INSERT INTO "bairro" VALUES (15,'S�','SE',1,1);
INSERT INTO "bairro" VALUES (16,'VILA LEOPOLDINA','VILA LEOPOLDINA',1,1);
INSERT INTO "bairro" VALUES (17,'ARICANDUVA','ARICANDUVA',1,1);
INSERT INTO "bairro" VALUES (18,'VILA CARR�O','VILA CARRAO',1,1);
INSERT INTO "bairro" VALUES (19,'VILA GUILHERME','VILA GUILHERME',1,1);
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "categoria_permissao" (
  "id" int(11) NOT NULL AUTO_INCREMENT,
  "nome" varchar(100) NOT NULL,
  "nome_normalizado" varchar(100) NOT NULL,
  "corporacao_id_INT" int(11) NOT NULL,
  PRIMARY KEY ("id"),
  UNIQUE KEY "nome" ("nome_normalizado","corporacao_id_INT"),
  KEY "categoria_permissao_FK_780273438" ("corporacao_id_INT"),
  CONSTRAINT "categoria_permissao_FK_780273438" FOREIGN KEY ("corporacao_id_INT") REFERENCES "corporacao" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);
/*!40101 SET character_set_client = @saved_cs_client */;
INSERT INTO "categoria_permissao" VALUES (1,'USU�RIO PADR�O','USUARIO PADRAO',1);
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "cidade" (
  "id" int(3) NOT NULL AUTO_INCREMENT,
  "nome" varchar(100) NOT NULL,
  "nome_normalizado" varchar(100) NOT NULL,
  "uf_id_INT" int(11) NOT NULL,
  "corporacao_id_INT" int(11) NOT NULL,
  PRIMARY KEY ("id"),
  UNIQUE KEY "nome" ("nome_normalizado","uf_id_INT","corporacao_id_INT"),
  KEY "uf_id_INT" ("uf_id_INT"),
  KEY "cidade_FK_928863526" ("corporacao_id_INT"),
  CONSTRAINT "cidade_FK_928863526" FOREIGN KEY ("corporacao_id_INT") REFERENCES "corporacao" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "cidade_ibfk_1" FOREIGN KEY ("uf_id_INT") REFERENCES "uf" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);
/*!40101 SET character_set_client = @saved_cs_client */;
INSERT INTO "cidade" VALUES (1,'S�O PAULO','SAO PAULO',1,1);
INSERT INTO "cidade" VALUES (2,'BELO HORIZONTE','BELO HORIZONTE',2,1);
INSERT INTO "cidade" VALUES (3,'OSASCO','OSASCO',1,1);
INSERT INTO "cidade" VALUES (4,'BARUERI','BARUERI',1,1);
INSERT INTO "cidade" VALUES (5,'CARAPICUIBA','CARAPICUIBA',1,1);
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "corporacao" (
  "id" int(11) NOT NULL AUTO_INCREMENT,
  "nome" varchar(100) NOT NULL,
  "nome_normalizado" varchar(100) NOT NULL,
  "usuario_dropbox" varchar(255) DEFAULT NULL,
  "senha_dropbox" varchar(255) DEFAULT NULL,
  PRIMARY KEY ("id"),
  UNIQUE KEY "corporacao_nome_UNIQUE" ("nome_normalizado")
);
/*!40101 SET character_set_client = @saved_cs_client */;
INSERT INTO "corporacao" VALUES (1,'FACTU','FACTU',NULL,NULL);
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "empresa" (
  "id" int(11) NOT NULL AUTO_INCREMENT,
  "nome" varchar(255) NOT NULL,
  "nome_normalizado" varchar(255) NOT NULL,
  "telefone1" varchar(30) DEFAULT NULL,
  "telefone2" varchar(30) DEFAULT NULL,
  "fax" varchar(30) DEFAULT NULL,
  "celular" varchar(30) DEFAULT NULL,
  "operadora_id_INT" int(11) DEFAULT NULL,
  "celular_sms" varchar(30) DEFAULT NULL,
  "email" varchar(255) DEFAULT NULL,
  "tipo_documento_id_INT" int(11) DEFAULT NULL,
  "numero_documento" varchar(30) DEFAULT NULL,
  "tipo_empresa_id_INT" int(11) DEFAULT NULL,
  "logradouro" varchar(255) DEFAULT NULL,
  "logradouro_normalizado" varchar(255) DEFAULT NULL,
  "numero" varchar(30) DEFAULT NULL,
  "complemento" varchar(255) DEFAULT NULL,
  "complemento_normalizado" varchar(255) DEFAULT NULL,
  "bairro_id_INT" int(11) DEFAULT NULL,
  "cidade_id_INT" int(11) DEFAULT NULL,
  "latitude_INT" int(6) DEFAULT NULL,
  "longitude_INT" int(6) DEFAULT NULL,
  "foto" varchar(50) DEFAULT NULL,
  "corporacao_id_INT" int(11) NOT NULL,
  PRIMARY KEY ("id","corporacao_id_INT"),
  KEY "u_bairro_FK" ("bairro_id_INT"),
  KEY "u_cidade_FK" ("cidade_id_INT"),
  KEY "empresa_FK_222015381" ("corporacao_id_INT"),
  KEY "empresa_FK_261840820" ("tipo_documento_id_INT"),
  KEY "empresa_FK_354888916" ("tipo_empresa_id_INT"),
  KEY "empresa_FK_967346192" ("operadora_id_INT"),
  CONSTRAINT "empresa_FK_222015381" FOREIGN KEY ("corporacao_id_INT") REFERENCES "corporacao" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "empresa_FK_261840820" FOREIGN KEY ("tipo_documento_id_INT") REFERENCES "tipo_documento" ("id") ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT "empresa_FK_354888916" FOREIGN KEY ("tipo_empresa_id_INT") REFERENCES "tipo_empresa" ("id") ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT "empresa_FK_967346192" FOREIGN KEY ("operadora_id_INT") REFERENCES "operadora" ("id") ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT "empresa_ibfk_1" FOREIGN KEY ("bairro_id_INT") REFERENCES "bairro" ("id") ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT "empresa_ibfk_2" FOREIGN KEY ("cidade_id_INT") REFERENCES "cidade" ("id") ON DELETE SET NULL ON UPDATE SET NULL
);
/*!40101 SET character_set_client = @saved_cs_client */;
INSERT INTO "empresa" VALUES (1,'FACTU TRADE MARKETING','FACTU TRADE MARKETING',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'RUA EDUARDO CARLOS PEREIRA','RUA EDUARDO CARLOS PEREIRA','298',NULL,NULL,NULL,1,-23597593,-46614689,NULL,1);
INSERT INTO "empresa" VALUES (6,'RB ALIMENTOS','RB ALIMENTOS','','','','',NULL,'','',NULL,'',NULL,'','','','','',NULL,NULL,NULL,NULL,'',1);
INSERT INTO "empresa" VALUES (8,'OMEGA SOFTWARE','OMEGA SOFTWARE','','','','',NULL,'','',NULL,'',NULL,'AV. PROFESSOR M�RIO WERNECK','AV. PROFESSOR MARIO WERNECK','2451','1102','1102',NULL,2,NULL,NULL,'',1);
INSERT INTO "empresa" VALUES (9,'CARREFOUR BURITIS','CARREFOUR BURITIS','','','','',NULL,'','',NULL,'',NULL,'','','','','',NULL,NULL,NULL,NULL,'',1);
INSERT INTO "empresa" VALUES (10,'CARREFOUR GUTIERREZ','CARREFOUR GUTIERREZ','','','','',NULL,'','',NULL,'',NULL,'AV. ANDR� CAVALCANTI','AV. ANDRE CAVALCANTI','200','','',NULL,2,NULL,NULL,'',1);
INSERT INTO "empresa" VALUES (11,'EXTRA VILA LEOPOLDINA','EXTRA VILA LEOPOLDINA','','','','',NULL,'','',NULL,'',NULL,'AV. IMPERATRIZ LEOPOLDINA','AV. IMPERATRIZ LEOPOLDINA','845','','',16,1,NULL,NULL,'',1);
INSERT INTO "empresa" VALUES (12,'EXTRA S�','EXTRA SE','','','','',NULL,'','',NULL,'',NULL,'RUA GLIC�RIO','RUA GLICERIO','60','','',15,1,NULL,NULL,'',1);
INSERT INTO "empresa" VALUES (13,'EXTRA SANTA CEC�LIA (PALMEIRAS)','EXTRA SANTA CECILIA','','','','',NULL,'','',NULL,'',NULL,'RUA DAS PALMEIRAS','RUA DAS PALMEIRAS','187','','',7,1,NULL,NULL,'',1);
INSERT INTO "empresa" VALUES (14,'EXTRA REP�BLICA','EXTRA REPUBLICA','','','','',NULL,'','',NULL,'',NULL,'RUA REGO FREITAS','RUA REGO FREITAS','172','','',14,1,NULL,NULL,'',1);
INSERT INTO "empresa" VALUES (15,'EXTRA ITAIM BIBI','EXTRA ITAIM BIBI','','','','',NULL,'','',NULL,'',NULL,'RUA CLODOMIRO AMAZONAS','RUA CLODOMIRO AMAZONAS','1308','','',13,1,NULL,NULL,'',1);
INSERT INTO "empresa" VALUES (16,'EXTRA BARRA FUNDA','EXTRA BARRA FUNDA','','','','',NULL,'','',NULL,'',NULL,'RUA TURIASSU','RUA TURIASSU','1205','','',12,1,-23531311,-46674520,'',1);
INSERT INTO "empresa" VALUES (17,'EXTRA ALPHAVILLE II','EXTRA ALPHAVILLE II','','','','',NULL,'','',NULL,'',NULL,'ALAMEDA MADEIRA','ALAMEDA MADEIRA','363','','',11,4,NULL,NULL,'',1);
INSERT INTO "empresa" VALUES (18,'EXTRA ALTO DE PINHEIROS','EXTRA ALTO DE PINHEIROS','','','','',NULL,'','',NULL,'',NULL,'PRA�A PANAMERICANA','PRACA PANAMERICANA','190','','',10,1,NULL,NULL,'',1);
INSERT INTO "empresa" VALUES (19,'EXTRA PARQUE CONTINENTAL','EXTRA PARQUE CONTINENTAL','','','','',NULL,'','',NULL,'',NULL,'AV. ANTONIO SOUZA NOSCHESE','AV. ANTONIO SOUZA NOSCHESE','900','','',9,3,NULL,NULL,'',1);
INSERT INTO "empresa" VALUES (20,'EXTRA CONSOLA��O','EXTRA CONSOLACAO','','','','',NULL,'','',NULL,'',NULL,'RUA FREI CANECA','RUA FREI CANECA','763','','',8,1,NULL,NULL,'',1);
INSERT INTO "empresa" VALUES (21,'EXTRA SANTA CEC�LIA (GENERAL OLIMPIO)','EXTRA SANTA CECILIA','','','','',NULL,'','',NULL,'',NULL,'AV. GENERAL OLIMPIO DA SILVEIRA','AV. GENERAL OLIMPIO DA SILVEIRA','414','','',7,1,NULL,NULL,'',1);
INSERT INTO "empresa" VALUES (22,'EXTRA CENTRO OSASCO','EXTRA CENTRO OSASCO','','','','',NULL,'','',NULL,'',NULL,'RUA DONA PRIMITIVA VIANCO','RUA DONA PRIMITIVA VIANCO','400','','',6,3,NULL,NULL,'',1);
INSERT INTO "empresa" VALUES (23,'EXTRA HELENA MARIA','EXTRA HELENA MARIA','','','','',NULL,'','',NULL,'',NULL,'RUA CATARINA FAZIO ANTONIAZZI','RUA CATARINA FAZIO ANTONIAZZI','23','','',5,3,NULL,NULL,'',1);
INSERT INTO "empresa" VALUES (24,'EXTRA VILA DOS REMEDIOS','EXTRA VILA DOS REMEDIOS','','','','',NULL,'','',NULL,'',NULL,'AV. DOS REM�DIOS','AV. DOS REMEDIOS','756','','',4,1,NULL,NULL,'',1);
INSERT INTO "empresa" VALUES (25,'EXTRA JARDIM PLANALTO','EXTRA JARDIM PLANALTO','','','','',NULL,'','',NULL,'',NULL,'AV. INOC�NCIO SER�FICO','AV. INOCENCIO SERAFICO','3520','','',2,5,NULL,NULL,'',1);
INSERT INTO "empresa" VALUES (26,'EXTRA CENTRO COMERCIAL CARAPICUIBA','EXTRA CENTRO COMERCIAL CARAPICUIBA','','','','',NULL,'','',NULL,'',NULL,'RUA S�NIA MARIA','RUA SONIA MARIA','30','','',3,5,NULL,NULL,'',1);
INSERT INTO "empresa" VALUES (27,'BAUTECH','BAUTECH','','','','',NULL,'','',NULL,'',NULL,'','','','','',NULL,NULL,NULL,NULL,'',1);
INSERT INTO "empresa" VALUES (28,'TELHA NORTE ARICANDUVA II','TELHA NORTE ARACANDUVA II','','','','',NULL,'','',NULL,'',NULL,'AVENIDA ARICANDUVA','','6470','','',17,1,NULL,NULL,'',1);
INSERT INTO "empresa" VALUES (29,'TELHA NORTE ARICANDUVA','TELHA NORTE ARICANDUVA','','','','',NULL,'','',NULL,'',NULL,'RUA COMENDADOR GIL PINHEIRO','','463','','',18,1,NULL,NULL,'',1);
INSERT INTO "empresa" VALUES (30,'TELHA NORTE PRO ARICANDUVA','TELHA NORTE PRO ARICANDUVA','','','','',NULL,'','',NULL,'',NULL,'RUA COMENDADOR GIL PINHEIRO','RUA COMENDADOR GIL PINHEIRO','463','BLOCO 2','BLOCO 2',18,1,NULL,NULL,'',1);
INSERT INTO "empresa" VALUES (31,'LEROY MERLIN LAR CENTER','LEROY MERLIN LAR CENTER','','','','',NULL,'','',NULL,'',NULL,'AVENIDA OTTO BAUMGART','AVENIDA OTTO BAUMGART','500','LAR CENTER','LAR CENTER',19,1,NULL,NULL,'',1);
INSERT INTO "empresa" VALUES (32,'IMIGRANTES','IMIGRANTES','','','','',NULL,'','',NULL,'',NULL,'R. TESTE','R. TESTE','001','','',NULL,1,NULL,NULL,'',1);
INSERT INTO "empresa" VALUES (33,'MORUMBI','MORUMBI','','','','',NULL,'','',NULL,'',NULL,'MORUMBI','MORUMBI','002','','',NULL,1,NULL,NULL,'',1);
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "empresa_compra" (
  "id" int(11) NOT NULL AUTO_INCREMENT,
  "forma_pagamento_id_INT" int(11) DEFAULT NULL,
  "valor_total_FLOAT" float(11,0) DEFAULT NULL,
  "data_DATE" date DEFAULT NULL,
  "hora_TIME" time DEFAULT NULL,
  "usuario_id_INT" int(11) DEFAULT NULL,
  "minha_empresa_id_INT" int(11) DEFAULT NULL,
  "outra_empresa_id_INT" int(11) DEFAULT NULL,
  "foto" varchar(50) DEFAULT NULL,
  "corporacao_id_INT" int(11) DEFAULT NULL,
  PRIMARY KEY ("id"),
  KEY "empresa_compra_FK_903930664" ("forma_pagamento_id_INT"),
  KEY "empresa_compra_FK_592681885" ("usuario_id_INT"),
  KEY "empresa_compra_FK_650939942" ("minha_empresa_id_INT"),
  KEY "empresa_compra_FK_297943115" ("outra_empresa_id_INT"),
  KEY "empresa_compra_FK_180664062" ("corporacao_id_INT"),
  CONSTRAINT "empresa_compra_FK_180664062" FOREIGN KEY ("corporacao_id_INT") REFERENCES "corporacao" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "empresa_compra_FK_297943115" FOREIGN KEY ("outra_empresa_id_INT") REFERENCES "outra_empresa" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "empresa_compra_FK_592681885" FOREIGN KEY ("usuario_id_INT") REFERENCES "usuario" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "empresa_compra_FK_650939942" FOREIGN KEY ("minha_empresa_id_INT") REFERENCES "minha_empresa" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "empresa_compra_FK_903930664" FOREIGN KEY ("forma_pagamento_id_INT") REFERENCES "forma_pagamento" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "empresa_compra_parcela" (
  "id" int(11) NOT NULL AUTO_INCREMENT,
  "empresa_compra_id_INT" int(11) NOT NULL,
  "data_DATE" date NOT NULL,
  "valor_FLOAT" float NOT NULL,
  "corporacao_id_INT" int(11) DEFAULT NULL,
  PRIMARY KEY ("id"),
  KEY "empresa_compra_parcela_FK_172180175" ("empresa_compra_id_INT"),
  KEY "empresa_compra_parcela_FK_238128662" ("corporacao_id_INT"),
  CONSTRAINT "empresa_compra_parcela_FK_172180175" FOREIGN KEY ("empresa_compra_id_INT") REFERENCES "empresa_compra" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "empresa_compra_parcela_FK_238128662" FOREIGN KEY ("corporacao_id_INT") REFERENCES "corporacao" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "empresa_perfil" (
  "id" int(11) NOT NULL AUTO_INCREMENT,
  "empresa_id_INT" int(11) DEFAULT NULL,
  "perfil_id_INT" int(11) DEFAULT NULL,
  "corporacao_id_INT" int(11) DEFAULT NULL,
  PRIMARY KEY ("id"),
  KEY "empresa_perfil_FK_569122315" ("empresa_id_INT"),
  KEY "empresa_perfil_FK_645446777" ("perfil_id_INT"),
  KEY "empresa_perfil_FK_11688232" ("corporacao_id_INT"),
  CONSTRAINT "empresa_perfil_FK_11688232" FOREIGN KEY ("corporacao_id_INT") REFERENCES "corporacao" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "empresa_perfil_FK_569122315" FOREIGN KEY ("empresa_id_INT") REFERENCES "empresa" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "empresa_perfil_FK_645446777" FOREIGN KEY ("perfil_id_INT") REFERENCES "perfil" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);
/*!40101 SET character_set_client = @saved_cs_client */;
INSERT INTO "empresa_perfil" VALUES (4,1,4,1);
INSERT INTO "empresa_perfil" VALUES (18,8,2,1);
INSERT INTO "empresa_perfil" VALUES (19,9,2,1);
INSERT INTO "empresa_perfil" VALUES (20,10,2,1);
INSERT INTO "empresa_perfil" VALUES (21,6,1,1);
INSERT INTO "empresa_perfil" VALUES (22,11,2,1);
INSERT INTO "empresa_perfil" VALUES (23,12,2,1);
INSERT INTO "empresa_perfil" VALUES (24,13,2,1);
INSERT INTO "empresa_perfil" VALUES (25,14,2,1);
INSERT INTO "empresa_perfil" VALUES (26,15,2,1);
INSERT INTO "empresa_perfil" VALUES (27,16,2,1);
INSERT INTO "empresa_perfil" VALUES (28,17,2,1);
INSERT INTO "empresa_perfil" VALUES (29,18,2,1);
INSERT INTO "empresa_perfil" VALUES (30,19,2,1);
INSERT INTO "empresa_perfil" VALUES (31,20,2,1);
INSERT INTO "empresa_perfil" VALUES (32,21,2,1);
INSERT INTO "empresa_perfil" VALUES (33,22,2,1);
INSERT INTO "empresa_perfil" VALUES (34,23,2,1);
INSERT INTO "empresa_perfil" VALUES (35,24,2,1);
INSERT INTO "empresa_perfil" VALUES (36,25,2,1);
INSERT INTO "empresa_perfil" VALUES (37,26,2,1);
INSERT INTO "empresa_perfil" VALUES (38,27,1,1);
INSERT INTO "empresa_perfil" VALUES (39,28,2,1);
INSERT INTO "empresa_perfil" VALUES (40,29,2,1);
INSERT INTO "empresa_perfil" VALUES (41,30,2,1);
INSERT INTO "empresa_perfil" VALUES (42,31,2,1);
INSERT INTO "empresa_perfil" VALUES (43,32,2,1);
INSERT INTO "empresa_perfil" VALUES (44,33,2,1);
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "empresa_produto" (
  "id" int(11) NOT NULL AUTO_INCREMENT,
  "identificador" varchar(30) DEFAULT NULL,
  "nome" varchar(255) NOT NULL,
  "nome_normalizado" varchar(255) NOT NULL,
  "descricao" varchar(512) DEFAULT NULL,
  "empresa_produto_tipo_id_INT" int(11) NOT NULL,
  "empresa_produto_unidade_medida_id_INT" int(11) DEFAULT NULL,
  "empresa_id_INT" int(11) NOT NULL,
  "video" varchar(50) DEFAULT NULL,
  "corporacao_id_INT" int(11) NOT NULL,
  PRIMARY KEY ("id"),
  KEY "empresa_produto_FK_426605225" ("empresa_produto_tipo_id_INT"),
  KEY "empresa_produto_FK_329833984" ("empresa_produto_unidade_medida_id_INT"),
  KEY "empresa_produto_FK_453643799" ("empresa_id_INT"),
  KEY "empresa_produto_FK_183288574" ("corporacao_id_INT"),
  CONSTRAINT "empresa_produto_FK_183288574" FOREIGN KEY ("corporacao_id_INT") REFERENCES "corporacao" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "empresa_produto_FK_329833984" FOREIGN KEY ("empresa_produto_unidade_medida_id_INT") REFERENCES "empresa_produto_unidade_medida" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "empresa_produto_FK_426605225" FOREIGN KEY ("empresa_produto_tipo_id_INT") REFERENCES "empresa_produto_tipo" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "empresa_produto_FK_453643799" FOREIGN KEY ("empresa_id_INT") REFERENCES "empresa" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "empresa_produto_compra" (
  "id" int(11) NOT NULL AUTO_INCREMENT,
  "empresa_produto_id_INT" int(11) NOT NULL,
  "empresa_compra_id_INT" int(11) NOT NULL,
  "quantidade_FLOAT" float(11,0) NOT NULL,
  "valor_total_FLOAT" float(11,0) NOT NULL,
  "corporacao_id_INT" int(11) NOT NULL,
  PRIMARY KEY ("id"),
  KEY "empresa_produto_compra_FK_285888672" ("empresa_produto_id_INT"),
  KEY "empresa_produto_compra_FK_163604736" ("empresa_compra_id_INT"),
  KEY "empresa_produto_compra_FK_69030761" ("corporacao_id_INT"),
  CONSTRAINT "empresa_produto_compra_FK_163604736" FOREIGN KEY ("empresa_compra_id_INT") REFERENCES "empresa_compra" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "empresa_produto_compra_FK_285888672" FOREIGN KEY ("empresa_produto_id_INT") REFERENCES "empresa_produto" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "empresa_produto_compra_FK_69030761" FOREIGN KEY ("corporacao_id_INT") REFERENCES "corporacao" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "empresa_produto_foto" (
  "id" int(11) NOT NULL AUTO_INCREMENT,
  "empresa_produto_id_INT" int(11) NOT NULL,
  "foto" varchar(50) NOT NULL,
  "corporacao_id_INT" int(11) NOT NULL,
  PRIMARY KEY ("id"),
  KEY "empresa_produto_foto_FK_579284668" ("empresa_produto_id_INT"),
  KEY "empresa_produto_foto_FK_675018311" ("corporacao_id_INT"),
  CONSTRAINT "empresa_produto_foto_FK_579284668" FOREIGN KEY ("empresa_produto_id_INT") REFERENCES "empresa_produto" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "empresa_produto_foto_FK_675018311" FOREIGN KEY ("corporacao_id_INT") REFERENCES "corporacao" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "empresa_produto_tipo" (
  "id" int(11) NOT NULL AUTO_INCREMENT,
  "nome" varchar(100) NOT NULL,
  "nome_normalizado" varchar(100) NOT NULL,
  "corporacao_id_INT" int(11) NOT NULL,
  PRIMARY KEY ("id"),
  UNIQUE KEY "nome" ("nome","corporacao_id_INT"),
  KEY "empresa_produto_tipo_FK_926544190" ("corporacao_id_INT"),
  CONSTRAINT "empresa_produto_tipo_FK_926544190" FOREIGN KEY ("corporacao_id_INT") REFERENCES "corporacao" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "empresa_produto_venda" (
  "id" int(11) NOT NULL AUTO_INCREMENT,
  "empresa_produto_id_INT" int(11) NOT NULL,
  "empresa_venda_id_INT" int(11) NOT NULL,
  "quantidade_FLOAT" float(11,0) NOT NULL,
  "valor_total_FLOAT" float(11,0) NOT NULL,
  "corporacao_id_INT" int(11) NOT NULL,
  PRIMARY KEY ("id"),
  KEY "empresa_produto_venda_FK_771057129" ("empresa_produto_id_INT"),
  KEY "empresa_produto_venda_FK_890716553" ("empresa_venda_id_INT"),
  KEY "empresa_produto_venda_FK_292968750" ("corporacao_id_INT"),
  CONSTRAINT "empresa_produto_venda_FK_292968750" FOREIGN KEY ("corporacao_id_INT") REFERENCES "corporacao" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "empresa_produto_venda_FK_771057129" FOREIGN KEY ("empresa_produto_id_INT") REFERENCES "empresa_produto" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "empresa_produto_venda_FK_890716553" FOREIGN KEY ("empresa_venda_id_INT") REFERENCES "empresa_venda" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "empresa_servico" (
  "id" int(11) NOT NULL AUTO_INCREMENT,
  "identificador" varchar(30) DEFAULT NULL,
  "nome" varchar(255) NOT NULL,
  "nome_normalizado" varchar(255) NOT NULL,
  "descricao" varchar(512) DEFAULT NULL,
  "empresa_servico_tipo_id_INT" int(11) NOT NULL,
  "empresa_id_INT" int(11) NOT NULL,
  "empresa_servico_unidade_medida_id_INT" int(11) NOT NULL,
  "prazo_entrega_dia_INT" int(11) DEFAULT NULL,
  "duracao_meses_INT" int(3) DEFAULT NULL,
  "video" varchar(50) DEFAULT NULL,
  "corporacao_id_INT" int(11) NOT NULL,
  PRIMARY KEY ("id"),
  KEY "empresa_servico_FK_261444092" ("empresa_servico_tipo_id_INT"),
  KEY "empresa_servico_FK_389770508" ("empresa_id_INT"),
  KEY "empresa_servico_FK_799224854" ("empresa_servico_unidade_medida_id_INT"),
  KEY "empresa_servico_FK_414123535" ("corporacao_id_INT"),
  CONSTRAINT "empresa_servico_FK_261444092" FOREIGN KEY ("empresa_servico_tipo_id_INT") REFERENCES "empresa_servico_tipo" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "empresa_servico_FK_389770508" FOREIGN KEY ("empresa_id_INT") REFERENCES "empresa" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "empresa_servico_FK_414123535" FOREIGN KEY ("corporacao_id_INT") REFERENCES "corporacao" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "empresa_servico_FK_799224854" FOREIGN KEY ("empresa_servico_unidade_medida_id_INT") REFERENCES "empresa_servico_unidade_medida" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "empresa_servico_compra" (
  "id" int(11) NOT NULL AUTO_INCREMENT,
  "empresa_servico_id_INT" int(11) NOT NULL,
  "empresa_compra_id_INT" int(11) NOT NULL,
  "quantidade_FLOAT" float(11,0) NOT NULL,
  "valor_total_FLOAT" float(11,0) NOT NULL,
  "corporacao_id_INT" int(11) NOT NULL,
  PRIMARY KEY ("id"),
  KEY "empresa_servico_compra_FK_307342529" ("empresa_servico_id_INT"),
  KEY "empresa_servico_compra_FK_892700196" ("empresa_compra_id_INT"),
  KEY "empresa_servico_compra_FK_118560791" ("corporacao_id_INT"),
  CONSTRAINT "empresa_servico_compra_FK_118560791" FOREIGN KEY ("corporacao_id_INT") REFERENCES "corporacao" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "empresa_servico_compra_FK_307342529" FOREIGN KEY ("empresa_servico_id_INT") REFERENCES "empresa_servico" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "empresa_servico_compra_FK_892700196" FOREIGN KEY ("empresa_compra_id_INT") REFERENCES "empresa_compra" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "empresa_servico_foto" (
  "id" int(11) NOT NULL AUTO_INCREMENT,
  "empresa_servico_id_INT" int(11) NOT NULL,
  "foto" varchar(50) NOT NULL,
  "corporacao_id_INT" int(11) NOT NULL,
  PRIMARY KEY ("id"),
  KEY "empresa_servico_foto_FK_719360352" ("empresa_servico_id_INT"),
  KEY "empresa_servico_foto_FK_974517823" ("corporacao_id_INT"),
  CONSTRAINT "empresa_servico_foto_FK_719360352" FOREIGN KEY ("empresa_servico_id_INT") REFERENCES "empresa_servico" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "empresa_servico_foto_FK_974517823" FOREIGN KEY ("corporacao_id_INT") REFERENCES "corporacao" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "empresa_servico_tipo" (
  "id" int(11) NOT NULL AUTO_INCREMENT,
  "nome" varchar(100) NOT NULL,
  "nome_normalizado" varchar(100) NOT NULL,
  "corporacao_id_INT" int(11) NOT NULL,
  PRIMARY KEY ("id"),
  UNIQUE KEY "nome" ("nome","corporacao_id_INT"),
  KEY "empresa_servico_tipo_FK_604431152" ("corporacao_id_INT"),
  CONSTRAINT "empresa_servico_tipo_FK_604431152" FOREIGN KEY ("corporacao_id_INT") REFERENCES "corporacao" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "empresa_servico_unidade_medida" (
  "id" int(11) NOT NULL AUTO_INCREMENT,
  "nome" varchar(100) NOT NULL,
  PRIMARY KEY ("id")
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "empresa_servico_venda" (
  "id" int(11) NOT NULL AUTO_INCREMENT,
  "empresa_servico_id_INT" int(11) NOT NULL,
  "empresa_venda_id_INT" int(11) NOT NULL,
  "quantidade_FLOAT" float(11,0) NOT NULL,
  "valor_total_FLOAT" float(11,0) NOT NULL,
  "corporacao_id_INT" int(11) NOT NULL,
  PRIMARY KEY ("id"),
  KEY "empresa_servico_venda_FK_310333252" ("empresa_servico_id_INT"),
  KEY "empresa_servico_venda_FK_937438965" ("empresa_venda_id_INT"),
  KEY "empresa_servico_venda_FK_338836670" ("corporacao_id_INT"),
  CONSTRAINT "empresa_servico_venda_FK_310333252" FOREIGN KEY ("empresa_servico_id_INT") REFERENCES "empresa_servico" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "empresa_servico_venda_FK_338836670" FOREIGN KEY ("corporacao_id_INT") REFERENCES "corporacao" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "empresa_servico_venda_FK_937438965" FOREIGN KEY ("empresa_venda_id_INT") REFERENCES "empresa_venda" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "empresa_tipo_venda" (
  "id" int(11) NOT NULL AUTO_INCREMENT,
  "empresa_produto_id_INT" int(11) DEFAULT NULL,
  "empresa_servico_id_INT" int(11) DEFAULT NULL,
  "valor_total_FLOAT" float(11,0) NOT NULL,
  "corporacao_id_INT" int(11) NOT NULL,
  PRIMARY KEY ("id"),
  KEY "empresa_tipo_venda_FK_699096680" ("empresa_produto_id_INT"),
  KEY "empresa_tipo_venda_FK_140777588" ("empresa_servico_id_INT"),
  KEY "empresa_tipo_venda_FK_768371582" ("corporacao_id_INT"),
  CONSTRAINT "empresa_tipo_venda_FK_140777588" FOREIGN KEY ("empresa_servico_id_INT") REFERENCES "empresa_servico" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "empresa_tipo_venda_FK_699096680" FOREIGN KEY ("empresa_produto_id_INT") REFERENCES "empresa_produto" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "empresa_tipo_venda_FK_768371582" FOREIGN KEY ("corporacao_id_INT") REFERENCES "corporacao" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "empresa_tipo_venda_parcela" (
  "id" int(11) NOT NULL AUTO_INCREMENT,
  "empresa_tipo_venda_id_INT" int(11) NOT NULL,
  "porcentagem_FLOAT" float(4,0) NOT NULL,
  "corporacao_id_INT" int(11) NOT NULL,
  PRIMARY KEY ("id"),
  KEY "empresa_tipo_venda_parcela_FK_864746094" ("empresa_tipo_venda_id_INT"),
  KEY "empresa_tipo_venda_parcela_FK_884063721" ("corporacao_id_INT"),
  CONSTRAINT "empresa_tipo_venda_parcela_FK_864746094" FOREIGN KEY ("empresa_tipo_venda_id_INT") REFERENCES "empresa_tipo_venda" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "empresa_tipo_venda_parcela_FK_884063721" FOREIGN KEY ("corporacao_id_INT") REFERENCES "corporacao" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "empresa_venda" (
  "id" int(11) NOT NULL AUTO_INCREMENT,
  "forma_pagamento_id_INT" int(11) DEFAULT NULL,
  "valor_total_FLOAT" float(11,0) NOT NULL,
  "data_DATE" date NOT NULL,
  "hora_TIME" time NOT NULL,
  "usuario_id_INT" int(11) NOT NULL,
  "cliente_empresa_id_INT" int(11) DEFAULT NULL,
  "fornecedor_empresa_id_INT" int(11) DEFAULT NULL,
  "pessoa_id_INT" int(11) DEFAULT NULL,
  "foto" varchar(50) DEFAULT NULL,
  "corporacao_id_INT" int(11) NOT NULL,
  PRIMARY KEY ("id")
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "empresa_venda_parcela" (
  "id" int(11) NOT NULL AUTO_INCREMENT,
  "empresa_venda_id_INT" int(11) NOT NULL,
  "data_DATE" date NOT NULL,
  "valor_FLOAT" float NOT NULL,
  "corporacao_id_INT" int(11) NOT NULL,
  PRIMARY KEY ("id"),
  KEY "empresa_venda_parcela_FK_200561523" ("empresa_venda_id_INT"),
  KEY "empresa_venda_parcela_FK_50445556" ("corporacao_id_INT"),
  CONSTRAINT "empresa_venda_parcela_FK_200561523" FOREIGN KEY ("empresa_venda_id_INT") REFERENCES "empresa_venda" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "empresa_venda_parcela_FK_50445556" FOREIGN KEY ("corporacao_id_INT") REFERENCES "corporacao" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "estado_civil" (
  "id" int(11) NOT NULL AUTO_INCREMENT,
  "nome" varchar(255) DEFAULT NULL,
  PRIMARY KEY ("id"),
  UNIQUE KEY "nome" ("nome")
);
/*!40101 SET character_set_client = @saved_cs_client */;
INSERT INTO "estado_civil" VALUES (2,'casado');
INSERT INTO "estado_civil" VALUES (3,'divorciado');
INSERT INTO "estado_civil" VALUES (1,'solteiro');
INSERT INTO "estado_civil" VALUES (4,'vi�vo');
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "forma_pagamento" (
  "id" int(11) NOT NULL AUTO_INCREMENT,
  "nome" varchar(255) DEFAULT NULL,
  "nome_normalizado" varchar(255) DEFAULT NULL,
  "corporacao_id_INT" int(11) DEFAULT NULL,
  PRIMARY KEY ("id"),
  KEY "forma_pagamento_FK_734954834" ("corporacao_id_INT"),
  CONSTRAINT "forma_pagamento_FK_734954834" FOREIGN KEY ("corporacao_id_INT") REFERENCES "corporacao" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "modelo" (
  "id" int(11) NOT NULL AUTO_INCREMENT,
  "nome" varchar(100) NOT NULL,
  "nome_normalizado" varchar(100) NOT NULL,
  "ano_INT" int(4) DEFAULT NULL,
  "corporacao_id_INT" int(11) NOT NULL,
  PRIMARY KEY ("id"),
  KEY "nome" ("nome_normalizado","ano_INT","corporacao_id_INT")
);
/*!40101 SET character_set_client = @saved_cs_client */;
INSERT INTO "modelo" VALUES (1,'FUSCA','FUSCA',2202,88);
INSERT INTO "modelo" VALUES (2,'CORSA','CORSA',1999,114);
INSERT INTO "modelo" VALUES (3,'TOCO','TOCO',1992,114);
INSERT INTO "modelo" VALUES (4,'CHEVROLET CELTA','CHEVROLET CELTA',NULL,1);
INSERT INTO "modelo" VALUES (5,'VOLKSWAGEN GOL','VOLKSWAGEN GOL',NULL,1);
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "operadora" (
  "id" int(11) NOT NULL AUTO_INCREMENT,
  "nome" varchar(50) NOT NULL,
  "nome_normalizado" varchar(50) NOT NULL,
  PRIMARY KEY ("id"),
  UNIQUE KEY "nome_operadora_UNIQUE" ("nome_normalizado")
);
/*!40101 SET character_set_client = @saved_cs_client */;
INSERT INTO "operadora" VALUES (1,'NEXTEL','NEXTEL');
INSERT INTO "operadora" VALUES (2,'VIVO','VIVO');
INSERT INTO "operadora" VALUES (3,'OI','OI');
INSERT INTO "operadora" VALUES (4,'CLARO','CLARO');
INSERT INTO "operadora" VALUES (5,'TIM','TIM');
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "pais" (
  "id" int(11) NOT NULL AUTO_INCREMENT,
  "nome" varchar(100) NOT NULL,
  "nome_normalizado" varchar(100) NOT NULL,
  "corporacao_id_INT" int(11) NOT NULL,
  PRIMARY KEY ("id"),
  UNIQUE KEY "nome" ("nome_normalizado","corporacao_id_INT"),
  KEY "pais_FK_330535889" ("corporacao_id_INT"),
  CONSTRAINT "pais_FK_330535889" FOREIGN KEY ("corporacao_id_INT") REFERENCES "corporacao" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);
/*!40101 SET character_set_client = @saved_cs_client */;
INSERT INTO "pais" VALUES (1,'BRASIL','BRASIL',1);
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "perfil" (
  "id" int(11) NOT NULL AUTO_INCREMENT,
  "nome" varchar(30) DEFAULT NULL,
  PRIMARY KEY ("id"),
  UNIQUE KEY "nome_perfil_UNIQUE" ("nome")
);
/*!40101 SET character_set_client = @saved_cs_client */;
INSERT INTO "perfil" VALUES (2,'Cliente');
INSERT INTO "perfil" VALUES (3,'Fornecedor');
INSERT INTO "perfil" VALUES (4,'Minha EmpresaModel');
INSERT INTO "perfil" VALUES (1,'Parceiro');
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "permissao" (
  "id" int(11) NOT NULL AUTO_INCREMENT,
  "nome" varchar(100) DEFAULT NULL,
  "tag" varchar(255) DEFAULT NULL,
  "pai_permissao_id_INT" int(11) DEFAULT NULL,
  PRIMARY KEY ("id"),
  UNIQUE KEY "tag" ("tag"),
  KEY "permissao_fk" ("pai_permissao_id_INT"),
  CONSTRAINT "permissao_fk" FOREIGN KEY ("pai_permissao_id_INT") REFERENCES "permissao" ("id") ON DELETE SET NULL ON UPDATE SET NULL
);
/*!40101 SET character_set_client = @saved_cs_client */;
INSERT INTO "permissao" VALUES (1,'Ponto Eletr�nico','ServicePontoEletronicoActivityMobile',NULL);
INSERT INTO "permissao" VALUES (2,'Rastreamento','ServiceRastrearActivityMobile',NULL);
INSERT INTO "permissao" VALUES (3,'Cadastros','ServiceCadastroActivityMobile',NULL);
INSERT INTO "permissao" VALUES (4,'Cat�logos','ServiceCatalogoActivityMobile',NULL);
INSERT INTO "permissao" VALUES (5,'Monitoramento Remoto','ServiceMonitoramentoRemotoActivityMobile',NULL);
INSERT INTO "permissao" VALUES (7,'Servi�os Online','ServiceOnlineActivityMobile',NULL);
INSERT INTO "permissao" VALUES (8,'Permiss�es','ServicePermissaoActivityMobile',NULL);
INSERT INTO "permissao" VALUES (9,'Configura��o','ConfigurationActivityMobile',NULL);
INSERT INTO "permissao" VALUES (10,'Sobre','AboutActivityMobile',NULL);
INSERT INTO "permissao" VALUES (11,'Teclado','SelectEmpresaPontoEletronicoActivityMobile',1);
INSERT INTO "permissao" VALUES (12,'Leitor QR Code','PontoEletronicoActivityMobile',1);
INSERT INTO "permissao" VALUES (13,'Hist�rico de Rota','SelectUsuarioPosicaoActivityMobile',2);
INSERT INTO "permissao" VALUES (14,'Monitorar','SelectVeiculoUsuarioActivityMobile',2);
INSERT INTO "permissao" VALUES (15,'Cadastro EmpresaModel','FormEmpresaActivityMobile',3);
INSERT INTO "permissao" VALUES (16,'Cadastro Pessoa','FormPessoaActivityMobile',3);
INSERT INTO "permissao" VALUES (17,'Cadastro Tarefa','FormTarefaActivityMobile',3);
INSERT INTO "permissao" VALUES (18,'Cadastro Ve�culo','FormVeiculoActivityMobile',3);
INSERT INTO "permissao" VALUES (19,'Cadastro Usu�rio do Ve�culo','FormVeiculoUsuarioActivityMobile',3);
INSERT INTO "permissao" VALUES (20,'Cadastro Usu�rio','FormUsuarioActivityMobile',3);
INSERT INTO "permissao" VALUES (21,'Cat�logo EmpresaModel','FilterEmpresaActivityMobile',4);
INSERT INTO "permissao" VALUES (22,'Cat�logo Pessoa','FilterPessoaActivityMobile',4);
INSERT INTO "permissao" VALUES (23,'Cat�logo Tarefa','FilterTarefaActivityMobile',4);
INSERT INTO "permissao" VALUES (24,'Cat�logo Ve�culo','FilterVeiculoActivityMobile',4);
INSERT INTO "permissao" VALUES (25,'Cat�logo Usu�rio do Ve�culo','FilterVeiculoUsuarioActivityMobile',4);
INSERT INTO "permissao" VALUES (26,'Cat�logo Usu�rio','FilterUsuarioActivityMobile',4);
INSERT INTO "permissao" VALUES (27,'Monitorar Usu�rio','SelectServicoMonitoramentoRemotoUsuarioActivityMobile',5);
INSERT INTO "permissao" VALUES (28,'Configura��o Monitoramento',NULL,5);
INSERT INTO "permissao" VALUES (29,'Cadastrar Categoria Permiss�o','FormCategoriaPermissaoActivityMobile',8);
INSERT INTO "permissao" VALUES (30,'Gerenciar Categoria Permiss�o','SelectCategoriaPermissaoActivityMobile',8);
INSERT INTO "permissao" VALUES (31,'Cadastro Estado','FormEstadoActivityMobile',74);
INSERT INTO "permissao" VALUES (32,'Cat�logo Estado','FilterEstadoActivityMobile',75);
INSERT INTO "permissao" VALUES (33,'Cadastro Cidade','FormCidadeActivityMobile',74);
INSERT INTO "permissao" VALUES (34,'Cat�logo Cidade','FilterCidadeActivityMobile',75);
INSERT INTO "permissao" VALUES (35,'Cadastro Bairro','FormBairroActivityMobile',74);
INSERT INTO "permissao" VALUES (36,'Cat�logo Bairro','FilterBairroActivityMobile',75);
INSERT INTO "permissao" VALUES (37,'Alterar Senha','AlterarSenhaUsuarioActivityMobile',9);
INSERT INTO "permissao" VALUES (45,'Editar EmpresaModel','EditEmpresa',21);
INSERT INTO "permissao" VALUES (47,'Editar Tarefa','EditTarefa',23);
INSERT INTO "permissao" VALUES (48,'Editar Ve�culo','EditVeiculo',24);
INSERT INTO "permissao" VALUES (49,'Editar Usu�rio do Ve�culo','EditVeiculoUsuario',25);
INSERT INTO "permissao" VALUES (50,'Editar Usu�rio','EditUsuario',26);
INSERT INTO "permissao" VALUES (51,'Editar Estado','EditUf',32);
INSERT INTO "permissao" VALUES (52,'Editar Cidade','EditCidade',34);
INSERT INTO "permissao" VALUES (53,'Editar Bairro','EditBairro',36);
INSERT INTO "permissao" VALUES (54,'Remover EmpresaModel','RemoveEmpresa',21);
INSERT INTO "permissao" VALUES (56,'Remover Tarefa','RemoveTarefa',23);
INSERT INTO "permissao" VALUES (57,'Remover Ve�culo','RemoveVeiculo',24);
INSERT INTO "permissao" VALUES (58,'Remover Usu�rio do Ve�culo','RemoveVeiculoUsuario',25);
INSERT INTO "permissao" VALUES (59,'Remover Usu�rio','RemoveUsuario',26);
INSERT INTO "permissao" VALUES (60,'Remover Estado','RemoveUf',32);
INSERT INTO "permissao" VALUES (61,'Remover Cidade','RemoveCidade',34);
INSERT INTO "permissao" VALUES (62,'Remover Bairro','RemoveBairro',36);
INSERT INTO "permissao" VALUES (68,'Sincronizar Dados','SynchronizeReceiveActivityMobile',9);
INSERT INTO "permissao" VALUES (69,'Hist�rico Monitoramento Usu�rio','SelectServicoHistoricoMonitoramentoRemotoUsuarioActivityMobile',5);
INSERT INTO "permissao" VALUES (70,'Cat�logo Pa�s','FilterPaisActivityMobile',75);
INSERT INTO "permissao" VALUES (71,'Cadastro Pa�s','FormPaisActivityMobile',74);
INSERT INTO "permissao" VALUES (72,'Editar Pa�s','EditPais',70);
INSERT INTO "permissao" VALUES (73,'Remover Pa�s','RemovePais',70);
INSERT INTO "permissao" VALUES (74,'Cadastro Geral','ServiceCadastroGeralActivityMobile',3);
INSERT INTO "permissao" VALUES (75,'Cat�logo Geral','ServiceCatalogoGeralActivityMobile',4);
INSERT INTO "permissao" VALUES (76,'Cadastro Corpora��o','FormCorporacaoActivityMobile',9);
INSERT INTO "permissao" VALUES (77,'Cat�logo Corpora��o','FilterCorporacaoActivityMobile',9);
INSERT INTO "permissao" VALUES (78,'Meus Servi�os','MenuServicoOnlineUsuarioActivityMobile',7);
INSERT INTO "permissao" VALUES (79,'Servi�o Online Usu�rio','SelectServicoOnlineGrupoUsuarioActivityMobile',7);
INSERT INTO "permissao" VALUES (80,'Dirigir Ve�culo','DirigirVeiculoActivityMobile',2);
INSERT INTO "permissao" VALUES (85,'Cadastro Profiss�o','FormProfissaoActivityMobile',74);
INSERT INTO "permissao" VALUES (86,'Cadastro Tipo Documento','FormTipoDocumentoActivityMobile',74);
INSERT INTO "permissao" VALUES (87,'Cat�logo Profiss�o','FilterProfissaoActivityMobile',75);
INSERT INTO "permissao" VALUES (88,'Cat�logo Tipo Documento','FilterTipoDocumentoActivityMobile',75);
INSERT INTO "permissao" VALUES (89,'Remover Profiss�o','RemoveProfissao',87);
INSERT INTO "permissao" VALUES (90,'Remover Tipo Documento','RemoveTipoDocumento',88);
INSERT INTO "permissao" VALUES (91,'Editar Profiss�o','EditProfissao',87);
INSERT INTO "permissao" VALUES (92,'Editar Tipo Documento','EditTipoDocumento',88);
INSERT INTO "permissao" VALUES (93,'Cat�logo Tipo EmpresaModel','FilterTipoEmpresaActivityMobile',75);
INSERT INTO "permissao" VALUES (94,'Cadastro Tipo EmpresaModel','FormTipoEmpresaActivityMobile',74);
INSERT INTO "permissao" VALUES (95,'Remover Tipo EmpresaModel','RemoveTipoEmpresa',93);
INSERT INTO "permissao" VALUES (96,'Editar Tipo EmpresaModel','EditTipoEmpresa',93);
INSERT INTO "permissao" VALUES (99,'Cadastro Modelo','FormModeloActivityMobile',74);
INSERT INTO "permissao" VALUES (100,'Cat�logo Modelo','FilterModeloActivityMobile',75);
INSERT INTO "permissao" VALUES (101,'Cat�logo Id Pessoa EmpresaModel','FilterPessoaEmpresaActivityMobile',1);
INSERT INTO "permissao" VALUES (102,'Sincroniza��o Ponto','SincronizacaoPontoActivityMobile',1);
INSERT INTO "permissao" VALUES (103,'Tutorial Ponto Eletr�nico','TutorialPontoEletronicoActivityMobile',1);
INSERT INTO "permissao" VALUES (104,'Minhas Tarefas','FilterMinhasTarefasActivityMobile',NULL);
INSERT INTO "permissao" VALUES (105,'Cat�logo Rede','FilterRedeActivityMobile',4);
INSERT INTO "permissao" VALUES (106,'Cadastro Rede','FormRedeActivityMobile',3);
INSERT INTO "permissao" VALUES (107,'Minha Rotina','ListPessoaEmpresaRotinaActivityMobile',1);
INSERT INTO "permissao" VALUES (108,'Meus Pontos Batidos','ListMeuPontoActivityMobile',1);
INSERT INTO "permissao" VALUES (109,'Cat�logo de Pontos Batidos','FilterPontoActivityMobile',1);
INSERT INTO "permissao" VALUES (110,'Relat�rio','ServiceRelatorioActivityMobile',NULL);
INSERT INTO "permissao" VALUES (111,'Cat�logo Relat�rio','FilterRelatorioActivityMobile',110);
INSERT INTO "permissao" VALUES (112,'Cadastro Relat�rio','FormRelatorioActivityMobile',110);
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "permissao_categoria_permissao" (
  "id" int(11) NOT NULL AUTO_INCREMENT,
  "permissao_id_INT" int(11) NOT NULL,
  "categoria_permissao_id_INT" int(11) NOT NULL,
  "corporacao_id_INT" int(11) NOT NULL,
  PRIMARY KEY ("id"),
  UNIQUE KEY "permissao_id_INT" ("permissao_id_INT","categoria_permissao_id_INT","corporacao_id_INT"),
  KEY "permissao_categoria_permissao_FK_924652100" ("categoria_permissao_id_INT"),
  KEY "permissao_categoria_permissao_FK_616943359" ("corporacao_id_INT"),
  CONSTRAINT "permissao_categoria_permissao_FK_417755127" FOREIGN KEY ("permissao_id_INT") REFERENCES "permissao" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "permissao_categoria_permissao_FK_475219727" FOREIGN KEY ("permissao_id_INT") REFERENCES "permissao" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "permissao_categoria_permissao_FK_616943359" FOREIGN KEY ("corporacao_id_INT") REFERENCES "corporacao" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "permissao_categoria_permissao_FK_622711182" FOREIGN KEY ("permissao_id_INT") REFERENCES "permissao" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "permissao_categoria_permissao_FK_670501709" FOREIGN KEY ("permissao_id_INT") REFERENCES "permissao" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "permissao_categoria_permissao_FK_726348877" FOREIGN KEY ("permissao_id_INT") REFERENCES "permissao" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "permissao_categoria_permissao_FK_761047364" FOREIGN KEY ("permissao_id_INT") REFERENCES "permissao" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "permissao_categoria_permissao_FK_798095703" FOREIGN KEY ("permissao_id_INT") REFERENCES "permissao" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "permissao_categoria_permissao_FK_838378907" FOREIGN KEY ("permissao_id_INT") REFERENCES "permissao" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "permissao_categoria_permissao_FK_924652100" FOREIGN KEY ("categoria_permissao_id_INT") REFERENCES "categoria_permissao" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "permissao_categoria_permissao_FK_970184327" FOREIGN KEY ("permissao_id_INT") REFERENCES "permissao" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);
/*!40101 SET character_set_client = @saved_cs_client */;
INSERT INTO "permissao_categoria_permissao" VALUES (324,1,1,1);
INSERT INTO "permissao_categoria_permissao" VALUES (1,1,13,36);
INSERT INTO "permissao_categoria_permissao" VALUES (49,1,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (200,1,21,143);
INSERT INTO "permissao_categoria_permissao" VALUES (327,2,1,1);
INSERT INTO "permissao_categoria_permissao" VALUES (55,2,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (206,2,21,143);
INSERT INTO "permissao_categoria_permissao" VALUES (36,3,15,38);
INSERT INTO "permissao_categoria_permissao" VALUES (26,3,17,38);
INSERT INTO "permissao_categoria_permissao" VALUES (31,3,18,38);
INSERT INTO "permissao_categoria_permissao" VALUES (44,3,19,38);
INSERT INTO "permissao_categoria_permissao" VALUES (59,3,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (210,3,21,143);
INSERT INTO "permissao_categoria_permissao" VALUES (269,3,22,146);
INSERT INTO "permissao_categoria_permissao" VALUES (38,4,15,38);
INSERT INTO "permissao_categoria_permissao" VALUES (75,4,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (226,4,21,143);
INSERT INTO "permissao_categoria_permissao" VALUES (274,4,22,146);
INSERT INTO "permissao_categoria_permissao" VALUES (115,5,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (119,7,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (263,7,21,143);
INSERT INTO "permissao_categoria_permissao" VALUES (40,8,15,38);
INSERT INTO "permissao_categoria_permissao" VALUES (122,8,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (279,8,22,146);
INSERT INTO "permissao_categoria_permissao" VALUES (42,9,15,38);
INSERT INTO "permissao_categoria_permissao" VALUES (125,9,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (266,9,21,143);
INSERT INTO "permissao_categoria_permissao" VALUES (282,9,22,146);
INSERT INTO "permissao_categoria_permissao" VALUES (130,10,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (268,10,21,143);
INSERT INTO "permissao_categoria_permissao" VALUES (286,10,22,146);
INSERT INTO "permissao_categoria_permissao" VALUES (2,11,13,36);
INSERT INTO "permissao_categoria_permissao" VALUES (50,11,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (201,11,21,143);
INSERT INTO "permissao_categoria_permissao" VALUES (3,12,13,36);
INSERT INTO "permissao_categoria_permissao" VALUES (51,12,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (202,12,21,143);
INSERT INTO "permissao_categoria_permissao" VALUES (328,13,1,1);
INSERT INTO "permissao_categoria_permissao" VALUES (56,13,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (207,13,21,143);
INSERT INTO "permissao_categoria_permissao" VALUES (329,14,1,1);
INSERT INTO "permissao_categoria_permissao" VALUES (57,14,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (208,14,21,143);
INSERT INTO "permissao_categoria_permissao" VALUES (27,15,17,38);
INSERT INTO "permissao_categoria_permissao" VALUES (32,15,18,38);
INSERT INTO "permissao_categoria_permissao" VALUES (45,15,19,38);
INSERT INTO "permissao_categoria_permissao" VALUES (60,15,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (211,15,21,143);
INSERT INTO "permissao_categoria_permissao" VALUES (270,15,22,146);
INSERT INTO "permissao_categoria_permissao" VALUES (37,16,15,38);
INSERT INTO "permissao_categoria_permissao" VALUES (28,16,17,38);
INSERT INTO "permissao_categoria_permissao" VALUES (33,16,18,38);
INSERT INTO "permissao_categoria_permissao" VALUES (46,16,19,38);
INSERT INTO "permissao_categoria_permissao" VALUES (61,16,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (212,16,21,143);
INSERT INTO "permissao_categoria_permissao" VALUES (271,16,22,146);
INSERT INTO "permissao_categoria_permissao" VALUES (62,17,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (213,17,21,143);
INSERT INTO "permissao_categoria_permissao" VALUES (63,18,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (214,18,21,143);
INSERT INTO "permissao_categoria_permissao" VALUES (64,19,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (215,19,21,143);
INSERT INTO "permissao_categoria_permissao" VALUES (29,20,17,38);
INSERT INTO "permissao_categoria_permissao" VALUES (34,20,18,38);
INSERT INTO "permissao_categoria_permissao" VALUES (47,20,19,38);
INSERT INTO "permissao_categoria_permissao" VALUES (65,20,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (216,20,21,143);
INSERT INTO "permissao_categoria_permissao" VALUES (272,20,22,146);
INSERT INTO "permissao_categoria_permissao" VALUES (76,21,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (227,21,21,143);
INSERT INTO "permissao_categoria_permissao" VALUES (275,21,22,146);
INSERT INTO "permissao_categoria_permissao" VALUES (39,22,15,38);
INSERT INTO "permissao_categoria_permissao" VALUES (79,22,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (230,22,21,143);
INSERT INTO "permissao_categoria_permissao" VALUES (276,22,22,146);
INSERT INTO "permissao_categoria_permissao" VALUES (80,23,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (231,23,21,143);
INSERT INTO "permissao_categoria_permissao" VALUES (83,24,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (234,24,21,143);
INSERT INTO "permissao_categoria_permissao" VALUES (86,25,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (237,25,21,143);
INSERT INTO "permissao_categoria_permissao" VALUES (89,26,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (277,26,22,146);
INSERT INTO "permissao_categoria_permissao" VALUES (116,27,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (117,28,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (41,29,15,38);
INSERT INTO "permissao_categoria_permissao" VALUES (123,29,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (280,29,22,146);
INSERT INTO "permissao_categoria_permissao" VALUES (124,30,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (281,30,22,146);
INSERT INTO "permissao_categoria_permissao" VALUES (67,31,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (218,31,21,143);
INSERT INTO "permissao_categoria_permissao" VALUES (93,32,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (241,32,21,143);
INSERT INTO "permissao_categoria_permissao" VALUES (68,33,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (219,33,21,143);
INSERT INTO "permissao_categoria_permissao" VALUES (96,34,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (244,34,21,143);
INSERT INTO "permissao_categoria_permissao" VALUES (69,35,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (220,35,21,143);
INSERT INTO "permissao_categoria_permissao" VALUES (99,36,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (247,36,21,143);
INSERT INTO "permissao_categoria_permissao" VALUES (43,37,15,38);
INSERT INTO "permissao_categoria_permissao" VALUES (126,37,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (283,37,22,146);
INSERT INTO "permissao_categoria_permissao" VALUES (77,45,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (228,45,21,143);
INSERT INTO "permissao_categoria_permissao" VALUES (81,47,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (232,47,21,143);
INSERT INTO "permissao_categoria_permissao" VALUES (84,48,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (235,48,21,143);
INSERT INTO "permissao_categoria_permissao" VALUES (87,49,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (238,49,21,143);
INSERT INTO "permissao_categoria_permissao" VALUES (90,50,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (94,51,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (242,51,21,143);
INSERT INTO "permissao_categoria_permissao" VALUES (97,52,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (245,52,21,143);
INSERT INTO "permissao_categoria_permissao" VALUES (100,53,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (248,53,21,143);
INSERT INTO "permissao_categoria_permissao" VALUES (78,54,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (229,54,21,143);
INSERT INTO "permissao_categoria_permissao" VALUES (82,56,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (233,56,21,143);
INSERT INTO "permissao_categoria_permissao" VALUES (85,57,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (236,57,21,143);
INSERT INTO "permissao_categoria_permissao" VALUES (88,58,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (239,58,21,143);
INSERT INTO "permissao_categoria_permissao" VALUES (91,59,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (95,60,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (243,60,21,143);
INSERT INTO "permissao_categoria_permissao" VALUES (98,61,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (246,61,21,143);
INSERT INTO "permissao_categoria_permissao" VALUES (101,62,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (249,62,21,143);
INSERT INTO "permissao_categoria_permissao" VALUES (127,68,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (267,68,21,143);
INSERT INTO "permissao_categoria_permissao" VALUES (118,69,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (102,70,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (250,70,21,143);
INSERT INTO "permissao_categoria_permissao" VALUES (70,71,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (221,71,21,143);
INSERT INTO "permissao_categoria_permissao" VALUES (103,72,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (251,72,21,143);
INSERT INTO "permissao_categoria_permissao" VALUES (104,73,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (252,73,21,143);
INSERT INTO "permissao_categoria_permissao" VALUES (30,74,17,38);
INSERT INTO "permissao_categoria_permissao" VALUES (35,74,18,38);
INSERT INTO "permissao_categoria_permissao" VALUES (48,74,19,38);
INSERT INTO "permissao_categoria_permissao" VALUES (66,74,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (217,74,21,143);
INSERT INTO "permissao_categoria_permissao" VALUES (273,74,22,146);
INSERT INTO "permissao_categoria_permissao" VALUES (92,75,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (240,75,21,143);
INSERT INTO "permissao_categoria_permissao" VALUES (278,75,22,146);
INSERT INTO "permissao_categoria_permissao" VALUES (128,76,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (284,76,22,146);
INSERT INTO "permissao_categoria_permissao" VALUES (129,77,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (285,77,22,146);
INSERT INTO "permissao_categoria_permissao" VALUES (120,78,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (264,78,21,143);
INSERT INTO "permissao_categoria_permissao" VALUES (121,79,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (265,79,21,143);
INSERT INTO "permissao_categoria_permissao" VALUES (330,80,1,1);
INSERT INTO "permissao_categoria_permissao" VALUES (58,80,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (209,80,21,143);
INSERT INTO "permissao_categoria_permissao" VALUES (71,85,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (222,85,21,143);
INSERT INTO "permissao_categoria_permissao" VALUES (72,86,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (223,86,21,143);
INSERT INTO "permissao_categoria_permissao" VALUES (105,87,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (253,87,21,143);
INSERT INTO "permissao_categoria_permissao" VALUES (108,88,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (256,88,21,143);
INSERT INTO "permissao_categoria_permissao" VALUES (106,89,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (254,89,21,143);
INSERT INTO "permissao_categoria_permissao" VALUES (109,90,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (257,90,21,143);
INSERT INTO "permissao_categoria_permissao" VALUES (107,91,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (255,91,21,143);
INSERT INTO "permissao_categoria_permissao" VALUES (110,92,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (258,92,21,143);
INSERT INTO "permissao_categoria_permissao" VALUES (111,93,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (259,93,21,143);
INSERT INTO "permissao_categoria_permissao" VALUES (73,94,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (224,94,21,143);
INSERT INTO "permissao_categoria_permissao" VALUES (112,95,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (260,95,21,143);
INSERT INTO "permissao_categoria_permissao" VALUES (113,96,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (261,96,21,143);
INSERT INTO "permissao_categoria_permissao" VALUES (74,99,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (225,99,21,143);
INSERT INTO "permissao_categoria_permissao" VALUES (114,100,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (262,100,21,143);
INSERT INTO "permissao_categoria_permissao" VALUES (52,101,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (203,101,21,143);
INSERT INTO "permissao_categoria_permissao" VALUES (53,102,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (204,102,21,143);
INSERT INTO "permissao_categoria_permissao" VALUES (54,103,20,143);
INSERT INTO "permissao_categoria_permissao" VALUES (205,103,21,143);
INSERT INTO "permissao_categoria_permissao" VALUES (325,107,1,1);
INSERT INTO "permissao_categoria_permissao" VALUES (326,108,1,1);
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "pessoa" (
  "id" int(11) NOT NULL AUTO_INCREMENT,
  "identificador" varchar(30) DEFAULT NULL,
  "nome" varchar(255) NOT NULL,
  "nome_normalizado" varchar(255) NOT NULL,
  "tipo_documento_id_INT" int(11) DEFAULT NULL,
  "numero_documento" varchar(30) DEFAULT NULL,
  "is_consumidor_BOOLEAN" int(1) DEFAULT NULL,
  "sexo_id_INT" int(11) DEFAULT NULL,
  "telefone" varchar(30) DEFAULT NULL,
  "celular" varchar(30) DEFAULT NULL,
  "operadora_id_INT" int(11) DEFAULT NULL,
  "celular_sms" varchar(30) DEFAULT NULL,
  "email" varchar(255) DEFAULT NULL,
  "logradouro" varchar(255) DEFAULT NULL,
  "logradouro_normalizado" varchar(255) DEFAULT NULL,
  "numero" varchar(30) DEFAULT '',
  "complemento" varchar(255) DEFAULT NULL,
  "complemento_normalizado" varchar(255) DEFAULT NULL,
  "cidade_id_INT" int(11) DEFAULT NULL,
  "bairro_id_INT" int(11) DEFAULT NULL,
  "latitude_INT" int(6) DEFAULT NULL,
  "longitude_INT" int(6) DEFAULT NULL,
  "foto" varchar(50) DEFAULT NULL,
  "corporacao_id_INT" int(11) NOT NULL,
  PRIMARY KEY ("id"),
  UNIQUE KEY "nome" ("email","corporacao_id_INT"),
  KEY "e_bairro_FK" ("bairro_id_INT"),
  KEY "e_cidade_FK" ("cidade_id_INT"),
  KEY "funcionario_sexo_FK" ("sexo_id_INT"),
  KEY "pessoa_FK_169738769" ("corporacao_id_INT"),
  KEY "pessoa_FK_224273681" ("tipo_documento_id_INT"),
  KEY "pessoa_FK_5187988" ("operadora_id_INT"),
  CONSTRAINT "funcionario_sexo_FK" FOREIGN KEY ("sexo_id_INT") REFERENCES "sexo" ("id") ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT "pessoa_FK_169738769" FOREIGN KEY ("corporacao_id_INT") REFERENCES "corporacao" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "pessoa_FK_224273681" FOREIGN KEY ("tipo_documento_id_INT") REFERENCES "tipo_documento" ("id") ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT "pessoa_FK_549713135" FOREIGN KEY ("operadora_id_INT") REFERENCES "operadora" ("id") ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT "pessoa_ibfk_1" FOREIGN KEY ("bairro_id_INT") REFERENCES "bairro" ("id") ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT "pessoa_ibfk_2" FOREIGN KEY ("cidade_id_INT") REFERENCES "cidade" ("id") ON DELETE SET NULL ON UPDATE SET NULL
);
/*!40101 SET character_set_client = @saved_cs_client */;
INSERT INTO "pessoa" VALUES (3,NULL,'CLIENTE DE TESTE','CLIENTE DE TESTE',NULL,NULL,1,1,'(31) 2515-5046',NULL,NULL,NULL,'eduardo.pesquisa.ufmg@gmail.com','AV. DAS NA��ES UNIDAS','AV. DAS NACOES UNIDAS','1110','154','154',1,1,NULL,NULL,NULL,1);
INSERT INTO "pessoa" VALUES (15,'','TATIANA YAMADA','TATIANA YAMADA',NULL,'',NULL,2,'','',NULL,'','bautech01@factu.com.br','','','','','',NULL,NULL,NULL,NULL,'',1);
INSERT INTO "pessoa" VALUES (17,'','ADRIANO','ADRIANO',NULL,'',NULL,1,'','',NULL,'','anderson@factu.com.br','','','','','',NULL,NULL,NULL,NULL,'',1);
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "pessoa_empresa" (
  "id" int(11) NOT NULL AUTO_INCREMENT,
  "pessoa_id_INT" int(11) NOT NULL,
  "empresa_id_INT" int(11) NOT NULL,
  "profissao_id_INT" int(11) DEFAULT NULL,
  "corporacao_id_INT" int(11) NOT NULL,
  PRIMARY KEY ("id"),
  UNIQUE KEY "pessoa_empresa_UNIQUE" ("pessoa_id_INT","empresa_id_INT","profissao_id_INT","corporacao_id_INT"),
  KEY "empresa_id_INT" ("empresa_id_INT"),
  KEY "fe_profissao_FK" ("profissao_id_INT"),
  KEY "pessoa_empresa_FK_41381835" ("pessoa_id_INT"),
  KEY "pessoa_empresa_FK_199859619" ("corporacao_id_INT"),
  CONSTRAINT "fe_profissao_FK" FOREIGN KEY ("profissao_id_INT") REFERENCES "profissao" ("id") ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT "pessoa_empresa_FK_199859619" FOREIGN KEY ("corporacao_id_INT") REFERENCES "corporacao" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "pessoa_empresa_FK_41381835" FOREIGN KEY ("pessoa_id_INT") REFERENCES "pessoa" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "pessoa_empresa_ibfk_2" FOREIGN KEY ("empresa_id_INT") REFERENCES "empresa" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);
/*!40101 SET character_set_client = @saved_cs_client */;
INSERT INTO "pessoa_empresa" VALUES (77,15,27,2,1);
INSERT INTO "pessoa_empresa" VALUES (79,15,28,2,1);
INSERT INTO "pessoa_empresa" VALUES (78,15,29,2,1);
INSERT INTO "pessoa_empresa" VALUES (80,15,30,2,1);
INSERT INTO "pessoa_empresa" VALUES (81,15,31,2,1);
INSERT INTO "pessoa_empresa" VALUES (84,15,32,2,1);
INSERT INTO "pessoa_empresa" VALUES (85,15,33,2,1);
INSERT INTO "pessoa_empresa" VALUES (104,17,6,2,1);
INSERT INTO "pessoa_empresa" VALUES (103,17,11,2,1);
INSERT INTO "pessoa_empresa" VALUES (101,17,12,2,1);
INSERT INTO "pessoa_empresa" VALUES (100,17,13,2,1);
INSERT INTO "pessoa_empresa" VALUES (98,17,14,2,1);
INSERT INTO "pessoa_empresa" VALUES (95,17,15,2,1);
INSERT INTO "pessoa_empresa" VALUES (90,17,16,2,1);
INSERT INTO "pessoa_empresa" VALUES (88,17,17,2,1);
INSERT INTO "pessoa_empresa" VALUES (89,17,18,2,1);
INSERT INTO "pessoa_empresa" VALUES (97,17,19,2,1);
INSERT INTO "pessoa_empresa" VALUES (93,17,20,2,1);
INSERT INTO "pessoa_empresa" VALUES (99,17,21,2,1);
INSERT INTO "pessoa_empresa" VALUES (92,17,22,2,1);
INSERT INTO "pessoa_empresa" VALUES (94,17,23,2,1);
INSERT INTO "pessoa_empresa" VALUES (102,17,24,2,1);
INSERT INTO "pessoa_empresa" VALUES (96,17,25,2,1);
INSERT INTO "pessoa_empresa" VALUES (91,17,26,2,1);
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "pessoa_empresa_rotina" (
  "id" int(11) NOT NULL AUTO_INCREMENT,
  "pessoa_empresa_id_INT" int(11) NOT NULL,
  "semana_INT" int(4) NOT NULL,
  "dia_semana_INT" int(4) NOT NULL,
  "corporacao_id_INT" int(11) NOT NULL,
  PRIMARY KEY ("id"),
  KEY "pessoa_empresa_rotina_FK_188354492" ("pessoa_empresa_id_INT"),
  KEY "pessoa_empresa_rotina_FK_969879151" ("corporacao_id_INT"),
  CONSTRAINT "pessoa_empresa_rotina_FK_188354492" FOREIGN KEY ("pessoa_empresa_id_INT") REFERENCES "pessoa_empresa" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "pessoa_empresa_rotina_FK_969879151" FOREIGN KEY ("corporacao_id_INT") REFERENCES "corporacao" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);
/*!40101 SET character_set_client = @saved_cs_client */;
INSERT INTO "pessoa_empresa_rotina" VALUES (1445,78,1,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1446,78,1,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1447,78,1,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1448,78,1,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1449,78,1,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1450,78,1,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1451,78,2,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1452,78,2,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1453,78,2,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1454,78,2,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1455,78,2,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1456,78,2,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1457,78,3,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1458,78,3,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1459,78,3,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1460,78,3,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1461,78,3,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1462,78,3,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1463,78,4,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1464,78,4,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1465,78,4,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1466,78,4,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1467,78,4,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1468,78,4,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1469,79,1,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1470,79,1,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1471,79,1,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1472,79,1,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1473,79,1,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1474,79,1,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1475,79,2,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1476,79,2,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1477,79,2,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1478,79,2,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1479,79,2,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1480,79,2,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1481,79,3,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1482,79,3,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1483,79,3,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1484,79,3,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1485,79,3,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1486,79,3,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1487,79,4,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1488,79,4,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1489,79,4,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1490,79,4,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1491,79,4,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1492,79,4,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1493,80,1,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1494,80,1,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1495,80,1,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1496,80,1,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1497,80,1,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1498,80,1,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1499,80,2,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1500,80,2,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1501,80,2,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1502,80,2,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1503,80,2,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1504,80,2,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1505,80,3,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1506,80,3,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1507,80,3,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1508,80,3,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1509,80,3,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1510,80,3,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1511,80,4,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1512,80,4,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1513,80,4,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1514,80,4,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1515,80,4,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1516,80,4,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1517,81,1,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1518,81,1,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1519,81,1,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1520,81,1,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1521,81,1,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1522,81,1,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1523,81,2,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1524,81,2,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1525,81,2,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1526,81,2,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1527,81,2,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1528,81,2,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1529,81,3,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1530,81,3,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1531,81,3,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1532,81,3,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1533,81,3,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1534,81,3,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1535,81,4,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1536,81,4,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1537,81,4,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1538,81,4,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1539,81,4,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1540,81,4,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1541,88,1,1,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1542,88,1,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1543,88,1,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1544,88,1,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1545,88,1,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1546,88,1,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1547,88,1,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1548,88,2,1,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1549,88,2,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1550,88,2,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1551,88,2,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1552,88,2,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1553,88,2,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1554,88,2,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1555,88,3,1,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1556,88,3,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1557,88,3,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1558,88,3,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1559,88,3,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1560,88,3,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1561,88,3,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1562,88,4,1,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1563,88,4,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1564,88,4,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1565,88,4,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1566,88,4,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1567,88,4,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1568,88,4,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1569,89,1,1,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1570,89,1,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1571,89,1,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1572,89,1,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1573,89,1,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1574,89,1,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1575,89,1,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1576,89,2,1,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1577,89,2,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1578,89,2,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1579,89,2,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1580,89,2,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1581,89,2,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1582,89,2,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1583,89,3,1,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1584,89,3,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1585,89,3,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1586,89,3,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1587,89,3,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1588,89,3,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1589,89,3,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1590,89,4,1,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1591,89,4,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1592,89,4,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1593,89,4,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1594,89,4,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1595,89,4,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1596,89,4,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1597,90,1,1,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1598,90,1,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1599,90,1,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1600,90,1,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1601,90,1,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1602,90,1,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1603,90,1,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1604,90,2,1,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1605,90,2,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1606,90,2,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1607,90,2,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1608,90,2,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1609,90,2,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1610,90,2,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1611,90,3,1,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1612,90,3,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1613,90,3,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1614,90,3,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1615,90,3,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1616,90,3,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1617,90,3,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1618,90,4,1,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1619,90,4,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1620,90,4,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1621,90,4,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1622,90,4,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1623,90,4,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1624,90,4,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1625,91,1,1,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1626,91,1,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1627,91,1,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1628,91,1,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1629,91,1,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1630,91,1,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1631,91,1,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1632,91,2,1,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1633,91,2,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1634,91,2,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1635,91,2,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1636,91,2,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1637,91,2,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1638,91,2,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1639,91,3,1,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1640,91,3,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1641,91,3,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1642,91,3,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1643,91,3,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1644,91,3,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1645,91,3,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1646,91,4,1,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1647,91,4,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1648,91,4,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1649,91,4,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1650,91,4,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1651,91,4,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1652,91,4,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1653,92,1,1,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1654,92,1,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1655,92,1,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1656,92,1,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1657,92,1,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1658,92,1,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1659,92,1,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1660,92,2,1,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1661,92,2,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1662,92,2,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1663,92,2,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1664,92,2,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1665,92,2,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1666,92,2,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1667,92,3,1,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1668,92,3,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1669,92,3,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1670,92,3,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1671,92,3,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1672,92,3,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1673,92,3,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1674,92,4,1,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1675,92,4,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1676,92,4,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1677,92,4,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1678,92,4,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1679,92,4,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1680,92,4,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1681,93,1,1,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1682,93,1,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1683,93,1,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1684,93,1,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1685,93,1,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1686,93,1,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1687,93,1,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1688,93,2,1,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1689,93,2,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1690,93,2,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1691,93,2,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1692,93,2,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1693,93,2,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1694,93,2,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1695,93,3,1,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1696,93,3,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1697,93,3,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1698,93,3,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1699,93,3,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1700,93,3,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1701,93,3,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1702,93,4,1,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1703,93,4,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1704,93,4,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1705,93,4,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1706,93,4,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1707,93,4,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1708,93,4,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1709,94,1,1,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1710,94,1,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1711,94,1,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1712,94,1,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1713,94,1,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1714,94,1,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1715,94,1,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1716,94,2,1,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1717,94,2,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1718,94,2,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1719,94,2,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1720,94,2,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1721,94,2,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1722,94,2,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1723,94,3,1,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1724,94,3,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1725,94,3,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1726,94,3,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1727,94,3,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1728,94,3,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1729,94,3,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1730,94,4,1,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1731,94,4,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1732,94,4,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1733,94,4,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1734,94,4,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1735,94,4,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1736,94,4,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1737,95,1,1,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1738,95,1,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1739,95,1,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1740,95,1,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1741,95,1,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1742,95,1,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1743,95,1,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1744,95,2,1,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1745,95,2,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1746,95,2,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1747,95,2,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1748,95,2,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1749,95,2,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1750,95,2,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1751,95,3,1,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1752,95,3,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1753,95,3,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1754,95,3,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1755,95,3,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1756,95,3,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1757,95,3,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1758,95,4,1,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1759,95,4,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1760,95,4,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1761,95,4,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1762,95,4,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1763,95,4,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1764,95,4,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1765,96,1,1,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1766,96,1,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1767,96,1,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1768,96,1,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1769,96,1,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1770,96,1,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1771,96,1,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1772,96,2,1,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1773,96,2,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1774,96,2,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1775,96,2,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1776,96,2,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1777,96,2,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1778,96,2,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1779,96,3,1,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1780,96,3,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1781,96,3,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1782,96,3,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1783,96,3,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1784,96,3,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1785,96,3,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1786,96,4,1,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1787,96,4,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1788,96,4,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1789,96,4,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1790,96,4,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1791,96,4,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1792,96,4,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1793,97,1,1,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1794,97,1,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1795,97,1,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1796,97,1,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1797,97,1,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1798,97,1,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1799,97,1,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1800,97,2,1,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1801,97,2,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1802,97,2,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1803,97,2,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1804,97,2,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1805,97,2,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1806,97,2,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1807,97,3,1,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1808,97,3,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1809,97,3,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1810,97,3,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1811,97,3,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1812,97,3,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1813,97,3,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1814,97,4,1,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1815,97,4,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1816,97,4,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1817,97,4,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1818,97,4,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1819,97,4,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1820,97,4,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1821,98,1,1,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1822,98,1,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1823,98,1,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1824,98,1,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1825,98,1,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1826,98,1,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1827,98,1,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1828,98,2,1,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1829,98,2,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1830,98,2,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1831,98,2,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1832,98,2,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1833,98,2,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1834,98,2,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1835,98,3,1,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1836,98,3,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1837,98,3,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1838,98,3,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1839,98,3,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1840,98,3,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1841,98,3,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1842,98,4,1,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1843,98,4,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1844,98,4,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1845,98,4,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1846,98,4,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1847,98,4,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1848,98,4,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1849,99,1,1,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1850,99,1,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1851,99,1,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1852,99,1,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1853,99,1,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1854,99,1,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1855,99,1,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1856,99,2,1,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1857,99,2,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1858,99,2,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1859,99,2,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1860,99,2,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1861,99,2,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1862,99,2,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1863,99,3,1,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1864,99,3,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1865,99,3,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1866,99,3,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1867,99,3,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1868,99,3,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1869,99,3,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1870,99,4,1,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1871,99,4,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1872,99,4,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1873,99,4,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1874,99,4,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1875,99,4,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1876,99,4,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1877,100,1,1,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1878,100,1,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1879,100,1,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1880,100,1,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1881,100,1,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1882,100,1,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1883,100,1,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1884,100,2,1,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1885,100,2,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1886,100,2,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1887,100,2,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1888,100,2,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1889,100,2,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1890,100,2,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1891,100,3,1,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1892,100,3,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1893,100,3,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1894,100,3,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1895,100,3,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1896,100,3,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1897,100,3,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1898,100,4,1,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1899,100,4,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1900,100,4,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1901,100,4,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1902,100,4,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1903,100,4,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1904,100,4,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1905,101,1,1,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1906,101,1,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1907,101,1,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1908,101,1,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1909,101,1,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1910,101,1,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1911,101,1,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1912,101,2,1,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1913,101,2,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1914,101,2,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1915,101,2,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1916,101,2,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1917,101,2,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1918,101,2,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1919,101,3,1,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1920,101,3,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1921,101,3,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1922,101,3,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1923,101,3,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1924,101,3,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1925,101,3,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1926,101,4,1,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1927,101,4,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1928,101,4,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1929,101,4,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1930,101,4,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1931,101,4,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1932,101,4,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1933,102,1,1,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1934,102,1,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1935,102,1,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1936,102,1,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1937,102,1,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1938,102,1,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1939,102,1,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1940,102,2,1,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1941,102,2,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1942,102,2,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1943,102,2,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1944,102,2,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1945,102,2,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1946,102,2,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1947,102,3,1,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1948,102,3,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1949,102,3,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1950,102,3,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1951,102,3,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1952,102,3,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1953,102,3,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1954,102,4,1,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1955,102,4,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1956,102,4,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1957,102,4,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1958,102,4,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1959,102,4,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1960,102,4,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1961,103,1,1,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1962,103,1,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1963,103,1,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1964,103,1,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1965,103,1,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1966,103,1,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1967,103,1,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1968,103,2,1,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1969,103,2,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1970,103,2,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1971,103,2,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1972,103,2,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1973,103,2,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1974,103,2,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1975,103,3,1,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1976,103,3,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1977,103,3,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1978,103,3,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1979,103,3,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1980,103,3,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1981,103,3,7,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1982,103,4,1,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1983,103,4,2,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1984,103,4,3,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1985,103,4,4,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1986,103,4,5,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1987,103,4,6,1);
INSERT INTO "pessoa_empresa_rotina" VALUES (1988,103,4,7,1);
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "pessoa_usuario" (
  "id" int(11) NOT NULL AUTO_INCREMENT,
  "pessoa_id_INT" int(11) NOT NULL,
  "usuario_id_INT" int(11) NOT NULL,
  "corporacao_id_INT" int(11) NOT NULL,
  PRIMARY KEY ("id"),
  UNIQUE KEY "pessoa_usuario_UNIQUE" ("pessoa_id_INT","usuario_id_INT","corporacao_id_INT"),
  KEY "pessoa_usuario_FK_347076416" ("pessoa_id_INT"),
  KEY "pessoa_usuario_FK_629943848" ("usuario_id_INT"),
  KEY "pessoa_usuario_FK_759368897" ("corporacao_id_INT"),
  CONSTRAINT "pessoa_usuario_FK_347076416" FOREIGN KEY ("pessoa_id_INT") REFERENCES "pessoa" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "pessoa_usuario_FK_629943848" FOREIGN KEY ("usuario_id_INT") REFERENCES "usuario" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "pessoa_usuario_FK_759368897" FOREIGN KEY ("corporacao_id_INT") REFERENCES "corporacao" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);
/*!40101 SET character_set_client = @saved_cs_client */;
INSERT INTO "pessoa_usuario" VALUES (8,15,7,1);
INSERT INTO "pessoa_usuario" VALUES (10,15,13,1);
INSERT INTO "pessoa_usuario" VALUES (11,17,12,1);
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "ponto" (
  "id" int(11) NOT NULL AUTO_INCREMENT,
  "pessoa_id_INT" int(11) NOT NULL,
  "hora_TIME" time NOT NULL,
  "dia_DATE" date NOT NULL,
  "foto" varchar(50) DEFAULT NULL,
  "empresa_id_INT" int(11) DEFAULT NULL,
  "profissao_id_INT" int(11) DEFAULT NULL,
  "usuario_id_INT" int(11) DEFAULT NULL,
  "is_entrada_BOOLEAN" int(1) NOT NULL,
  "latitude_FLOAT" int(6) DEFAULT NULL,
  "longitude_FLOAT" int(6) DEFAULT NULL,
  "tipo_ponto_id_INT" int(11) DEFAULT NULL,
  "corporacao_id_INT" int(11) NOT NULL,
  PRIMARY KEY ("id"),
  KEY "ponto_FK_748565674" ("pessoa_id_INT"),
  KEY "ponto_FK_228240967" ("corporacao_id_INT"),
  KEY "ponto_FK_329772949" ("empresa_id_INT"),
  KEY "ponto_FK_160339355" ("profissao_id_INT"),
  KEY "ponto_FK_509979248" ("usuario_id_INT"),
  KEY "ponto_FK_1234567" ("tipo_ponto_id_INT"),
  CONSTRAINT "ponto_FK_1234567" FOREIGN KEY ("tipo_ponto_id_INT") REFERENCES "tipo_ponto" ("id") ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT "ponto_FK_160339355" FOREIGN KEY ("profissao_id_INT") REFERENCES "profissao" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "ponto_FK_228240967" FOREIGN KEY ("corporacao_id_INT") REFERENCES "corporacao" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "ponto_FK_329772949" FOREIGN KEY ("empresa_id_INT") REFERENCES "empresa" ("id") ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT "ponto_FK_509979248" FOREIGN KEY ("usuario_id_INT") REFERENCES "usuario" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);
/*!40101 SET character_set_client = @saved_cs_client */;
INSERT INTO "ponto" VALUES (1,2,'00:21:00','2011-01-10',NULL,1,NULL,3,1,NULL,NULL,NULL,1);
INSERT INTO "ponto" VALUES (2,1,'01:07:00','2011-01-10',NULL,1,1,3,1,NULL,NULL,1,1);
INSERT INTO "ponto" VALUES (3,1,'01:07:00','2011-01-10',NULL,1,1,3,1,NULL,NULL,1,1);
INSERT INTO "ponto" VALUES (4,2,'01:15:00','2011-01-10',NULL,1,NULL,3,1,NULL,NULL,1,1);
INSERT INTO "ponto" VALUES (5,2,'01:18:00','2011-01-10',NULL,1,NULL,3,1,NULL,NULL,1,1);
INSERT INTO "ponto" VALUES (6,1,'11:41:00','2012-10-03',NULL,1,1,1,1,-23647119,-46575983,1,1);
INSERT INTO "ponto" VALUES (7,1,'11:46:00','2012-10-03',NULL,1,1,1,0,-23647119,-46575983,1,1);
INSERT INTO "ponto" VALUES (8,1,'11:46:00','2012-10-03',NULL,1,1,1,0,-23647119,-46575983,1,1);
INSERT INTO "ponto" VALUES (9,1,'11:47:00','2012-10-03',NULL,1,1,1,1,-23647119,-46575983,3,1);
INSERT INTO "ponto" VALUES (10,1,'11:48:00','2012-10-03',NULL,1,1,1,0,-23647119,-46575983,3,1);
INSERT INTO "ponto" VALUES (11,1,'11:52:00','2012-10-03',NULL,1,1,1,1,-23597541,-46615020,1,1);
INSERT INTO "ponto" VALUES (12,1,'15:09:00','2012-10-03',NULL,1,1,1,1,-23597556,-46614776,1,1);
INSERT INTO "ponto" VALUES (13,1,'15:09:00','2012-10-03',NULL,1,1,1,0,-23597556,-46614776,1,1);
INSERT INTO "ponto" VALUES (14,2,'15:09:00','2011-01-11',NULL,1,NULL,3,1,-21389184,-42694351,1,1);
INSERT INTO "ponto" VALUES (15,1,'15:10:00','2012-10-03',NULL,1,1,1,1,-23597556,-46614776,3,1);
INSERT INTO "ponto" VALUES (16,1,'15:10:00','2012-10-03',NULL,1,1,1,0,-23597556,-46614776,3,1);
INSERT INTO "ponto" VALUES (17,1,'15:45:00','2012-10-03',NULL,1,1,1,1,-23597556,-46614776,1,1);
INSERT INTO "ponto" VALUES (18,1,'17:16:00','2012-10-03',NULL,1,1,1,1,-23597607,-46614872,1,1);
INSERT INTO "ponto" VALUES (19,1,'17:18:00','2012-10-03',NULL,1,1,1,0,-23597607,-46614872,1,1);
INSERT INTO "ponto" VALUES (20,1,'15:11:00','2012-10-04',NULL,1,1,1,1,-23597579,-46614903,1,1);
INSERT INTO "ponto" VALUES (21,1,'15:14:00','2012-10-04',NULL,1,1,1,1,-23597579,-46614903,3,1);
INSERT INTO "ponto" VALUES (22,1,'15:55:00','2012-10-04',NULL,1,1,1,1,-23597579,-46614903,3,1);
INSERT INTO "ponto" VALUES (23,1,'11:23:00','2012-10-05',NULL,1,1,1,1,-23597590,-46614881,1,1);
INSERT INTO "ponto" VALUES (24,1,'11:26:00','2012-10-05',NULL,1,1,1,1,-23597590,-46614881,1,1);
INSERT INTO "ponto" VALUES (25,1,'11:26:00','2012-10-05',NULL,1,1,1,1,-23597590,-46614881,1,1);
INSERT INTO "ponto" VALUES (26,1,'11:26:00','2012-10-05',NULL,1,1,1,0,-23597590,-46614881,1,1);
INSERT INTO "ponto" VALUES (27,1,'10:36:00','2012-10-15',NULL,1,1,1,1,-23597608,-46614907,1,1);
INSERT INTO "ponto" VALUES (28,1,'10:36:00','2012-10-15',NULL,1,1,1,1,-23597608,-46614907,3,1);
INSERT INTO "ponto" VALUES (29,11,'20:03:00','2012-11-06',NULL,NULL,1,3,1,-21382627,-42693030,2,1);
INSERT INTO "ponto" VALUES (30,1,'09:40:00','2012-11-13',NULL,1,NULL,1,1,-23597458,-46614948,1,1);
INSERT INTO "ponto" VALUES (31,1,'09:29:00','2012-11-13',NULL,1,NULL,2,1,-19976027,-43968670,1,1);
INSERT INTO "ponto" VALUES (32,1,'14:36:00','2012-11-14',NULL,NULL,1,1,1,-23597585,-46614866,1,1);
INSERT INTO "ponto" VALUES (33,1,'14:36:00','2012-11-14',NULL,NULL,1,1,0,-23597585,-46614866,1,1);
INSERT INTO "ponto" VALUES (34,1,'20:34:00','2012-11-18',NULL,6,2,2,1,-19975949,-43968421,1,1);
INSERT INTO "ponto" VALUES (35,1,'20:40:00','2012-11-18',NULL,10,2,2,1,-19975949,-43968421,1,1);
INSERT INTO "ponto" VALUES (36,1,'21:25:00','2012-11-18',NULL,6,2,2,1,-19976050,-43968667,3,1);
INSERT INTO "ponto" VALUES (37,1,'10:41:00','2012-11-19',NULL,10,2,2,1,-19976089,-43968424,2,1);
INSERT INTO "ponto" VALUES (38,11,'04:08:00','2012-11-20',NULL,10,2,3,1,-21382508,-42693581,2,1);
INSERT INTO "ponto" VALUES (39,11,'04:08:00','2012-11-20',NULL,10,2,3,1,-21382508,-42693581,3,1);
INSERT INTO "ponto" VALUES (40,12,'09:58:00','2012-11-20',NULL,18,1,6,1,-23597575,-46614871,1,1);
INSERT INTO "ponto" VALUES (41,12,'10:02:00','2012-11-20',NULL,18,1,6,0,-23597575,-46614871,1,1);
INSERT INTO "ponto" VALUES (42,12,'10:04:00','2012-11-20',NULL,18,1,6,1,-23597575,-46614871,1,1);
INSERT INTO "ponto" VALUES (43,12,'10:05:00','2012-11-20',NULL,26,1,6,1,-23597575,-46614871,1,1);
INSERT INTO "ponto" VALUES (44,12,'10:05:00','2012-11-20',NULL,26,1,6,0,-23597575,-46614871,1,1);
INSERT INTO "ponto" VALUES (45,12,'10:25:00','2012-11-20',NULL,18,1,6,1,-23597575,-46614871,1,1);
INSERT INTO "ponto" VALUES (46,12,'10:40:00','2012-11-20',NULL,18,1,6,1,-23597575,-46614871,1,1);
INSERT INTO "ponto" VALUES (47,12,'11:25:00','2012-11-20',NULL,12,1,6,0,-23597575,-46614871,1,1);
INSERT INTO "ponto" VALUES (48,1,'21:16:00','2012-11-20',NULL,16,2,2,1,-19976053,-43968627,1,1);
INSERT INTO "ponto" VALUES (49,1,'21:18:00','2012-11-20',NULL,23,2,2,0,-19976053,-43968627,1,1);
INSERT INTO "ponto" VALUES (50,12,'17:30:00','2012-11-21',NULL,22,1,6,1,-23597622,-46614939,1,1);
INSERT INTO "ponto" VALUES (51,12,'17:31:00','2012-11-21',NULL,22,1,6,0,-23597622,-46614939,1,1);
INSERT INTO "ponto" VALUES (52,12,'17:32:00','2012-11-21',NULL,26,1,6,1,-23597622,-46614939,1,1);
INSERT INTO "ponto" VALUES (53,12,'17:40:00','2012-11-21',NULL,13,1,6,1,-23597622,-46614939,1,1);
INSERT INTO "ponto" VALUES (54,12,'17:41:00','2012-11-21',NULL,24,1,6,0,-23597622,-46614939,1,1);
INSERT INTO "ponto" VALUES (55,12,'18:00:00','2012-11-21',NULL,23,1,6,1,-23597621,-46614838,1,1);
INSERT INTO "ponto" VALUES (56,12,'18:00:00','2012-11-21',NULL,23,1,6,0,-23597621,-46614838,1,1);
INSERT INTO "ponto" VALUES (57,12,'18:12:00','2012-11-21',NULL,23,1,6,0,-23577932,-46610856,1,1);
INSERT INTO "ponto" VALUES (58,12,'18:13:00','2012-11-21',NULL,23,1,6,0,-23577768,-46610690,1,1);
INSERT INTO "ponto" VALUES (59,12,'18:13:00','2012-11-21',NULL,23,1,6,1,-23577768,-46610690,1,1);
INSERT INTO "ponto" VALUES (60,12,'18:13:00','2012-11-21',NULL,23,1,6,1,-23577768,-46610690,1,1);
INSERT INTO "ponto" VALUES (61,12,'13:11:00','2012-11-22',NULL,24,1,6,1,-23517911,-46752637,1,1);
INSERT INTO "ponto" VALUES (62,12,'13:11:00','2012-11-22',NULL,24,1,6,1,-23517911,-46752637,1,1);
INSERT INTO "ponto" VALUES (63,12,'14:35:00','2012-11-22',NULL,24,1,6,0,-23517911,-46752637,1,1);
INSERT INTO "ponto" VALUES (64,12,'14:35:00','2012-11-22',NULL,24,1,6,0,-23517911,-46752637,1,1);
INSERT INTO "ponto" VALUES (65,12,'14:35:00','2012-11-22',NULL,24,1,6,0,-23517911,-46752637,1,1);
INSERT INTO "ponto" VALUES (66,12,'14:36:00','2012-11-22',NULL,24,1,6,0,-23517911,-46752637,1,1);
INSERT INTO "ponto" VALUES (67,12,'15:30:00','2012-11-22',NULL,13,1,6,1,-23537469,-46651200,1,1);
INSERT INTO "ponto" VALUES (68,12,'15:47:00','2012-11-22',NULL,13,1,6,1,-23537799,-46651094,1,1);
INSERT INTO "ponto" VALUES (69,12,'16:36:00','2012-11-22',NULL,13,1,6,0,-23537799,-46651094,1,1);
INSERT INTO "ponto" VALUES (70,12,'17:24:00','2012-11-22',NULL,14,1,6,1,-23542062,-46646450,1,1);
INSERT INTO "ponto" VALUES (71,12,'19:40:00','2012-11-22',NULL,14,1,6,0,-23542015,-46646436,1,1);
INSERT INTO "ponto" VALUES (72,12,'19:40:00','2012-11-22',NULL,14,1,6,0,-23542015,-46646436,1,1);
INSERT INTO "ponto" VALUES (73,12,'11:07:00','2012-11-23',NULL,19,1,6,1,-23544463,-46755759,1,1);
INSERT INTO "ponto" VALUES (74,12,'12:36:00','2012-11-23',NULL,19,1,6,0,-23544532,-46755825,1,1);
INSERT INTO "ponto" VALUES (75,12,'12:53:00','2012-11-23',NULL,22,1,6,1,-23531492,-46776024,1,1);
INSERT INTO "ponto" VALUES (76,12,'14:20:00','2012-11-23',NULL,22,1,6,0,-23531440,-46776106,1,1);
INSERT INTO "ponto" VALUES (77,12,'14:42:00','2012-11-23',NULL,23,1,6,1,-23502432,-46789942,1,1);
INSERT INTO "ponto" VALUES (78,12,'15:12:00','2012-11-23',NULL,23,1,6,0,-23539609,-46772557,1,1);
INSERT INTO "ponto" VALUES (79,12,'12:52:00','2012-11-26',NULL,18,1,6,1,-23552787,-46708533,1,1);
INSERT INTO "ponto" VALUES (80,12,'14:05:00','2012-11-26',NULL,18,1,6,0,-23552576,-46708346,1,1);
INSERT INTO "ponto" VALUES (81,12,'14:32:00','2012-11-26',NULL,16,1,6,1,-23531350,-46674606,1,1);
INSERT INTO "ponto" VALUES (82,12,'15:56:00','2012-11-26',NULL,16,1,6,0,-23530813,-46672915,1,1);
INSERT INTO "ponto" VALUES (83,12,'16:07:00','2012-11-26',NULL,14,1,6,1,-23544163,-46646012,1,1);
INSERT INTO "ponto" VALUES (84,12,'16:07:00','2012-11-26',NULL,14,1,6,1,-23544163,-46646012,1,1);
INSERT INTO "ponto" VALUES (85,12,'19:06:00','2012-11-26',NULL,14,1,6,0,-23543114,-46646503,1,1);
INSERT INTO "ponto" VALUES (86,12,'13:05:00','2012-11-27',NULL,20,1,6,1,-23554717,-46654786,1,1);
INSERT INTO "ponto" VALUES (87,12,'13:05:00','2012-11-27',NULL,20,1,6,1,-23554717,-46654786,1,1);
INSERT INTO "ponto" VALUES (88,12,'13:24:00','2012-11-27',NULL,20,1,6,0,-23554498,-46654498,1,1);
INSERT INTO "ponto" VALUES (89,12,'13:48:00','2012-11-27',NULL,21,1,6,1,-23531490,-46660957,1,1);
INSERT INTO "ponto" VALUES (90,12,'14:40:00','2012-11-27',NULL,21,1,6,0,-23531490,-46660957,1,1);
INSERT INTO "ponto" VALUES (91,12,'16:44:00','2012-11-27',NULL,13,1,6,1,-23537634,-46651013,1,1);
INSERT INTO "ponto" VALUES (92,12,'16:44:00','2012-11-27',NULL,13,1,6,1,-23537634,-46651013,1,1);
INSERT INTO "ponto" VALUES (93,12,'19:00:00','2012-11-27',NULL,13,1,6,0,-23541365,-46646577,1,1);
INSERT INTO "ponto" VALUES (94,12,'06:52:00','2012-11-30',NULL,20,1,6,1,-23554300,-46654468,1,1);
INSERT INTO "ponto" VALUES (95,12,'06:52:00','2012-11-30',NULL,20,1,6,1,-23554300,-46654468,1,1);
INSERT INTO "ponto" VALUES (96,12,'13:21:00','2012-11-30',NULL,20,1,6,0,-23554981,-46654602,1,1);
INSERT INTO "ponto" VALUES (97,12,'15:01:00','2012-11-30',NULL,23,1,6,1,-23501991,-46790095,1,1);
INSERT INTO "ponto" VALUES (98,12,'16:12:00','2012-11-30',NULL,23,1,6,0,-23502551,-46789865,1,1);
INSERT INTO "ponto" VALUES (99,12,'12:13:00','2012-12-03',NULL,16,1,6,1,-23531312,-46674649,1,1);
INSERT INTO "ponto" VALUES (100,12,'13:56:00','2012-12-03',NULL,16,1,6,0,-23523023,-46670301,1,1);
INSERT INTO "ponto" VALUES (101,12,'16:06:00','2012-12-03',NULL,14,1,6,1,-23543422,-46646850,1,1);
INSERT INTO "ponto" VALUES (102,12,'18:00:00','2012-12-03',NULL,14,1,6,0,-23544341,-46646618,1,1);
INSERT INTO "ponto" VALUES (103,12,'18:28:00','2012-12-03',NULL,21,1,6,1,-23531503,-46660529,1,1);
INSERT INTO "ponto" VALUES (104,12,'19:01:00','2012-12-03',NULL,21,1,6,0,-23531400,-46661129,1,1);
INSERT INTO "ponto" VALUES (105,12,'12:32:00','2012-12-04',NULL,13,1,6,1,-23537188,-46651301,1,1);
INSERT INTO "ponto" VALUES (106,12,'13:22:00','2012-12-04',NULL,13,1,6,0,-23537498,-46651025,1,1);
INSERT INTO "ponto" VALUES (107,12,'13:22:00','2012-12-04',NULL,13,1,6,0,-23537498,-46651025,1,1);
INSERT INTO "ponto" VALUES (108,12,'13:40:00','2012-12-04',NULL,12,1,6,1,-23554218,-46628347,1,1);
INSERT INTO "ponto" VALUES (109,12,'13:56:00','2012-12-04',NULL,12,1,6,0,-23554218,-46628347,1,1);
INSERT INTO "ponto" VALUES (110,12,'14:29:00','2012-12-04',NULL,18,1,6,1,-23554185,-46707810,1,1);
INSERT INTO "ponto" VALUES (111,12,'16:08:00','2012-12-04',NULL,18,1,6,0,-23552635,-46708996,1,1);
INSERT INTO "ponto" VALUES (112,1,'14:35:00','2012-12-05',NULL,1,2,1,1,-23597577,-46614903,1,1);
INSERT INTO "ponto" VALUES (113,12,'09:06:00','2012-12-18',NULL,16,1,6,1,-23597631,-46614850,1,1);
INSERT INTO "ponto" VALUES (114,12,'09:07:00','2012-12-18',NULL,16,1,6,0,-23597631,-46614850,1,1);
INSERT INTO "ponto" VALUES (115,11,'16:14:00','2013-01-20',NULL,17,2,3,1,-21382717,-42694079,1,1);
INSERT INTO "ponto" VALUES (116,11,'17:52:00','2013-01-20',NULL,10,2,3,1,-21382717,-42694079,1,1);
INSERT INTO "ponto" VALUES (117,11,'17:52:00','2013-01-20',NULL,10,2,3,1,-21382717,-42694079,1,1);
INSERT INTO "ponto" VALUES (118,11,'18:03:00','2013-01-20',NULL,10,2,3,1,-21382717,-42694079,1,1);
INSERT INTO "ponto" VALUES (119,11,'18:17:00','2013-01-20',NULL,10,2,3,1,-21382717,-42694079,1,1);
INSERT INTO "ponto" VALUES (120,11,'18:17:00','2013-01-20',NULL,10,2,3,1,-21382717,-42694079,1,1);
INSERT INTO "ponto" VALUES (121,11,'18:30:00','2013-01-20',NULL,10,2,3,1,-21382717,-42694079,1,1);
INSERT INTO "ponto" VALUES (122,11,'18:31:00','2013-01-20',NULL,10,2,3,1,-21382717,-42694079,1,1);
INSERT INTO "ponto" VALUES (123,12,'09:22:00','2013-01-21',NULL,16,1,6,1,-23597536,-46614912,1,1);
INSERT INTO "ponto" VALUES (124,12,'09:28:00','2013-01-21',NULL,16,1,6,0,-23597536,-46614912,1,1);
INSERT INTO "ponto" VALUES (125,12,'09:28:00','2013-01-21',NULL,26,1,6,1,-23597536,-46614912,1,1);
INSERT INTO "ponto" VALUES (126,12,'09:29:00','2013-01-21',NULL,26,1,6,1,-23597536,-46614912,3,1);
INSERT INTO "ponto" VALUES (127,12,'09:29:00','2013-01-21',NULL,26,1,6,1,-23597536,-46614912,3,1);
INSERT INTO "ponto" VALUES (128,12,'09:29:00','2013-01-21',NULL,26,1,6,1,-23597536,-46614912,1,1);
INSERT INTO "ponto" VALUES (129,12,'11:25:00','2013-01-21',NULL,18,1,6,1,-23597551,-46614799,1,1);
INSERT INTO "ponto" VALUES (130,12,'11:28:00','2013-01-21',NULL,18,1,6,0,-23597539,-46614769,1,1);
INSERT INTO "ponto" VALUES (131,12,'14:13:00','2013-01-21',NULL,18,1,6,1,-23597561,-46614913,1,1);
INSERT INTO "ponto" VALUES (132,12,'14:13:00','2013-01-21',NULL,18,1,6,0,-23597561,-46614913,1,1);
INSERT INTO "ponto" VALUES (133,12,'11:59:00','2013-01-22',NULL,17,1,6,1,-23596663,-46621050,1,1);
INSERT INTO "ponto" VALUES (134,12,'14:59:00','2013-01-22',NULL,17,1,6,0,-23597614,-46614808,1,1);
INSERT INTO "ponto" VALUES (135,12,'15:08:00','2013-01-22',NULL,18,1,6,1,-23597583,-46614843,1,1);
INSERT INTO "ponto" VALUES (136,12,'15:09:00','2013-01-22',NULL,18,1,6,0,-23597583,-46614843,1,1);
INSERT INTO "ponto" VALUES (137,12,'16:51:00','2013-01-31',NULL,16,1,6,1,-23597694,-46614897,1,1);
INSERT INTO "ponto" VALUES (138,12,'16:52:00','2013-01-31',NULL,16,1,6,0,-23597694,-46614897,1,1);
INSERT INTO "ponto" VALUES (139,12,'16:51:00','2013-01-31',NULL,16,1,6,1,-23597694,-46614897,1,1);
INSERT INTO "ponto" VALUES (140,12,'16:52:00','2013-01-31',NULL,16,1,6,0,-23597694,-46614897,1,1);
INSERT INTO "ponto" VALUES (141,12,'09:50:00','2013-02-04',NULL,16,1,6,1,-23521222,-46673572,1,1);
INSERT INTO "ponto" VALUES (142,12,'13:39:00','2013-02-04',NULL,16,1,6,0,-23531366,-46674502,1,1);
INSERT INTO "ponto" VALUES (143,12,'12:26:00','2013-02-05',NULL,24,1,6,1,-23517798,-46752966,1,1);
INSERT INTO "ponto" VALUES (144,12,'12:36:00','2013-02-05',NULL,24,1,6,0,-23517798,-46752966,1,1);
INSERT INTO "ponto" VALUES (145,12,'12:47:00','2013-02-05',NULL,11,1,6,1,-23528891,-46731255,1,1);
INSERT INTO "ponto" VALUES (146,12,'13:24:00','2013-02-05',NULL,11,1,6,0,-23528910,-46731601,1,1);
INSERT INTO "ponto" VALUES (147,12,'13:35:00','2013-02-05',NULL,18,1,6,1,-23528910,-46731601,1,1);
INSERT INTO "ponto" VALUES (148,12,'15:09:00','2013-02-05',NULL,18,1,6,0,-23554619,-46708824,1,1);
INSERT INTO "ponto" VALUES (149,12,'16:03:00','2013-02-05',NULL,14,1,6,1,-23532314,-46658198,1,1);
INSERT INTO "ponto" VALUES (150,12,'16:57:00','2013-02-05',NULL,14,1,6,0,-23542103,-46646435,1,1);
INSERT INTO "ponto" VALUES (151,12,'16:59:00','2013-02-05',NULL,14,1,6,0,-23542103,-46646435,1,1);
INSERT INTO "ponto" VALUES (152,12,'14:39:00','2013-02-06',NULL,21,1,6,1,-23531433,-46659202,1,1);
INSERT INTO "ponto" VALUES (153,12,'15:01:00','2013-02-06',NULL,21,1,6,0,-23531841,-46659132,1,1);
INSERT INTO "ponto" VALUES (154,12,'15:07:00','2013-02-06',NULL,13,1,6,1,-23535120,-46653870,1,1);
INSERT INTO "ponto" VALUES (155,12,'15:32:00','2013-02-06',NULL,13,1,6,0,-23537582,-46651161,1,1);
INSERT INTO "ponto" VALUES (156,12,'15:38:00','2013-02-06',NULL,20,1,6,1,-23537582,-46651161,1,1);
INSERT INTO "ponto" VALUES (157,12,'18:04:00','2013-02-06',NULL,20,1,6,0,-23550668,-46653985,1,1);
INSERT INTO "ponto" VALUES (158,12,'18:04:00','2013-02-06',NULL,20,1,6,0,-23550668,-46653985,1,1);
INSERT INTO "ponto" VALUES (159,12,'18:04:00','2013-02-06',NULL,20,1,6,0,-23550668,-46653985,1,1);
INSERT INTO "ponto" VALUES (160,12,'13:06:00','2013-02-07',NULL,14,1,6,1,-23542206,-46646328,1,1);
INSERT INTO "ponto" VALUES (161,12,'19:57:00','2013-02-07',NULL,14,1,6,0,-23542222,-46646598,1,1);
INSERT INTO "ponto" VALUES (162,12,'19:58:00','2013-02-07',NULL,14,1,6,0,-23542222,-46646598,1,1);
INSERT INTO "ponto" VALUES (163,12,'10:41:00','2013-02-08',NULL,19,1,6,1,-23544978,-46756347,1,1);
INSERT INTO "ponto" VALUES (164,12,'16:32:00','2013-02-08',NULL,19,1,6,0,-23527382,-46761036,1,1);
INSERT INTO "ponto" VALUES (165,12,'08:38:00','2013-02-14',NULL,21,1,6,1,-23531772,-46659245,1,1);
INSERT INTO "ponto" VALUES (166,12,'15:25:00','2013-02-14',NULL,21,1,6,0,-23503102,-46653793,1,1);
INSERT INTO "ponto" VALUES (167,12,'15:30:00','2013-02-14',NULL,13,1,6,1,-23531787,-46658896,1,1);
INSERT INTO "ponto" VALUES (168,12,'15:40:00','2013-02-14',NULL,13,1,6,0,-23531787,-46658896,1,1);
INSERT INTO "ponto" VALUES (169,12,'16:12:00','2013-02-14',NULL,15,1,6,1,-23595051,-46676486,1,1);
INSERT INTO "ponto" VALUES (170,12,'16:14:00','2013-02-14',NULL,15,1,6,0,-23595051,-46676486,1,1);
INSERT INTO "ponto" VALUES (171,12,'09:20:00','2013-02-15',NULL,16,1,6,1,-23531483,-46674822,1,1);
INSERT INTO "ponto" VALUES (172,12,'11:22:00','2013-02-15',NULL,16,1,6,0,-23531542,-46674917,1,1);
INSERT INTO "ponto" VALUES (173,12,'13:39:00','2013-02-15',NULL,11,1,6,1,-23529234,-46732367,1,1);
INSERT INTO "ponto" VALUES (174,12,'16:41:00','2013-02-15',NULL,11,1,6,0,-23529117,-46731249,1,1);
INSERT INTO "ponto" VALUES (175,12,'14:48:00','2013-02-19',NULL,20,1,6,1,-23592014,-46625581,1,1);
INSERT INTO "ponto" VALUES (176,12,'14:42:00','2013-02-20',NULL,21,1,6,1,-23592011,-46625582,1,1);
INSERT INTO "ponto" VALUES (177,12,'14:43:00','2013-02-20',NULL,16,1,6,0,-23592011,-46625582,1,1);
INSERT INTO "ponto" VALUES (178,12,'14:47:00','2013-02-20',NULL,18,1,6,1,-23597606,-46614927,1,1);
INSERT INTO "ponto" VALUES (179,12,'14:47:00','2013-02-20',NULL,18,1,6,0,-23597606,-46614927,1,1);
INSERT INTO "ponto" VALUES (180,12,'08:12:00','2013-02-22',NULL,20,1,6,1,-23586165,-46759860,1,1);
INSERT INTO "ponto" VALUES (181,12,'08:12:00','2013-02-22',NULL,20,1,6,1,-23586165,-46759860,1,1);
INSERT INTO "ponto" VALUES (182,12,'09:40:00','2013-02-22',NULL,20,1,6,0,-23554288,-46654268,1,1);
INSERT INTO "ponto" VALUES (183,12,'09:40:00','2013-02-22',NULL,20,1,6,0,-23554288,-46654268,1,1);
INSERT INTO "ponto" VALUES (184,12,'10:23:00','2013-02-22',NULL,13,1,6,1,-23537364,-46654528,1,1);
INSERT INTO "ponto" VALUES (185,12,'11:46:00','2013-02-22',NULL,13,1,6,0,-23531409,-46660686,1,1);
INSERT INTO "ponto" VALUES (186,12,'11:46:00','2013-02-22',NULL,21,1,6,1,-23531409,-46660686,1,1);
INSERT INTO "ponto" VALUES (187,12,'13:07:00','2013-02-22',NULL,21,1,6,0,-23531471,-46660842,1,1);
INSERT INTO "ponto" VALUES (188,12,'13:07:00','2013-02-22',NULL,21,1,6,0,-23531471,-46660842,1,1);
INSERT INTO "ponto" VALUES (189,12,'13:23:00','2013-02-22',NULL,16,1,6,1,-23531167,-46660463,1,1);
INSERT INTO "ponto" VALUES (190,12,'17:35:00','2013-02-22',NULL,16,1,6,0,-23586772,-46826809,1,1);
INSERT INTO "ponto" VALUES (191,12,'17:36:00','2013-02-22',NULL,16,1,6,0,-23586772,-46826809,1,1);
INSERT INTO "ponto" VALUES (192,12,'17:36:00','2013-02-22',NULL,16,1,6,0,-23586772,-46826809,1,1);
INSERT INTO "ponto" VALUES (193,12,'17:36:00','2013-02-22',NULL,16,1,6,0,-23586772,-46826809,1,1);
INSERT INTO "ponto" VALUES (194,12,'08:42:00','2013-02-25',NULL,15,1,6,1,-23571493,-46707744,1,1);
INSERT INTO "ponto" VALUES (195,12,'10:28:00','2013-02-25',NULL,15,1,6,0,-23596055,-46679972,1,1);
INSERT INTO "ponto" VALUES (196,12,'10:28:00','2013-02-25',NULL,15,1,6,0,-23596055,-46679972,1,1);
INSERT INTO "ponto" VALUES (197,12,'13:37:00','2013-02-25',NULL,18,1,6,1,-23548582,-46746228,1,1);
INSERT INTO "ponto" VALUES (198,12,'14:15:00','2013-02-25',NULL,18,1,6,0,-23533148,-46748337,1,1);
INSERT INTO "ponto" VALUES (199,12,'14:15:00','2013-02-25',NULL,11,1,6,1,-23533148,-46748337,1,1);
INSERT INTO "ponto" VALUES (200,12,'15:20:00','2013-02-25',NULL,11,1,6,0,-23528380,-46731870,1,1);
INSERT INTO "ponto" VALUES (201,12,'08:29:00','2013-02-26',NULL,12,1,6,1,-23570837,-46691263,1,1);
INSERT INTO "ponto" VALUES (202,12,'10:03:00','2013-02-26',NULL,12,1,6,0,-23553687,-46624142,1,1);
INSERT INTO "ponto" VALUES (203,12,'10:03:00','2013-02-26',NULL,12,1,6,0,-23553687,-46624142,1,1);
INSERT INTO "ponto" VALUES (204,12,'10:15:00','2013-02-26',NULL,14,1,6,1,-23554164,-46630482,1,1);
INSERT INTO "ponto" VALUES (205,12,'11:37:00','2013-02-26',NULL,14,1,6,0,-23543110,-46629898,1,1);
INSERT INTO "ponto" VALUES (206,12,'12:34:00','2013-02-26',NULL,20,1,6,1,-23543342,-46630988,1,1);
INSERT INTO "ponto" VALUES (207,12,'07:39:00','2013-02-27',NULL,22,1,6,1,-23583873,-46779271,1,1);
INSERT INTO "ponto" VALUES (208,12,'09:47:00','2013-02-27',NULL,22,1,6,0,-23525730,-46668303,1,1);
INSERT INTO "ponto" VALUES (209,12,'10:08:00','2013-02-27',NULL,19,1,6,1,-23531676,-46762361,1,1);
INSERT INTO "ponto" VALUES (210,12,'19:26:00','2013-02-27',NULL,19,1,6,0,-23583044,-46825000,1,1);
INSERT INTO "ponto" VALUES (211,17,'01:18:00','2013-03-01',NULL,6,2,3,1,-21382428,-42693655,1,1);
INSERT INTO "ponto" VALUES (212,17,'11:46:00','2013-03-04',NULL,6,2,3,1,-21382245,-42693733,2,1);
INSERT INTO "ponto" VALUES (213,17,'11:55:00','2013-03-04',NULL,6,2,3,1,-21382245,-42693733,1,1);
INSERT INTO "ponto" VALUES (214,17,'12:53:00','2013-03-04',NULL,6,2,12,0,-23597584,-46614952,1,1);
INSERT INTO "ponto" VALUES (215,17,'08:52:00','2013-03-05',NULL,6,2,12,1,-23597755,-46614878,1,1);
INSERT INTO "ponto" VALUES (216,17,'09:04:00','2013-03-05',NULL,17,2,12,1,-23597614,-46614882,1,1);
INSERT INTO "ponto" VALUES (217,17,'09:04:00','2013-03-05',NULL,17,2,12,0,-23597614,-46614882,1,1);
INSERT INTO "ponto" VALUES (218,17,'09:05:00','2013-03-05',NULL,17,2,12,0,-23597614,-46614882,1,1);
INSERT INTO "ponto" VALUES (219,17,'09:07:00','2013-03-05',NULL,18,2,12,1,-23597665,-46614874,1,1);
INSERT INTO "ponto" VALUES (220,17,'09:08:00','2013-03-05',NULL,18,2,12,0,-23597665,-46614874,1,1);
INSERT INTO "ponto" VALUES (221,17,'09:21:00','2013-03-05',NULL,16,2,12,1,-23597655,-46614816,1,1);
INSERT INTO "ponto" VALUES (222,17,'09:21:00','2013-03-05',NULL,16,2,12,0,-23597655,-46614816,1,1);
INSERT INTO "ponto" VALUES (223,17,'10:37:00','2013-03-05',NULL,20,2,12,1,-23561954,-46676713,1,1);
INSERT INTO "ponto" VALUES (224,3,'15:59:00','2013-03-05',NULL,8,NULL,3,1,-21389232,-42694100,1,1);
INSERT INTO "ponto" VALUES (225,3,'15:59:00','2013-03-05',NULL,8,NULL,3,1,-21389232,-42694100,2,1);
INSERT INTO "ponto" VALUES (226,3,'15:59:00','2013-03-05',NULL,8,NULL,3,1,-21389232,-42694100,3,1);
INSERT INTO "ponto" VALUES (227,3,'16:00:00','2013-03-05',NULL,8,NULL,3,1,-21389232,-42694100,1,1);
INSERT INTO "ponto" VALUES (228,3,'16:00:00','2013-03-05',NULL,8,NULL,3,1,-21389232,-42694100,2,1);
INSERT INTO "ponto" VALUES (229,3,'16:00:00','2013-03-05',NULL,8,NULL,3,1,-21389232,-42694100,3,1);
INSERT INTO "ponto" VALUES (230,17,'15:24:00','2013-03-05',NULL,18,2,12,1,-23553474,-46708462,1,1);
INSERT INTO "ponto" VALUES (231,17,'16:38:00','2013-03-05',NULL,18,2,12,0,-23553474,-46708462,1,1);
INSERT INTO "ponto" VALUES (232,17,'16:38:00','2013-03-05',NULL,18,2,12,0,-23553474,-46708462,1,1);
INSERT INTO "ponto" VALUES (233,17,'16:38:00','2013-03-05',NULL,18,2,12,0,-23553474,-46708462,1,1);
INSERT INTO "ponto" VALUES (234,17,'08:50:00','2013-03-06',NULL,14,2,12,1,-23551799,-46657623,1,1);
INSERT INTO "ponto" VALUES (235,17,'09:53:00','2013-03-06',NULL,14,2,12,0,-23542812,-46646523,1,1);
INSERT INTO "ponto" VALUES (236,17,'10:03:00','2013-03-06',NULL,13,2,12,1,-23539638,-46650236,1,1);
INSERT INTO "ponto" VALUES (237,17,'12:47:00','2013-03-06',NULL,13,2,12,0,-23538201,-46652466,1,1);
INSERT INTO "ponto" VALUES (238,17,'13:35:00','2013-03-06',NULL,21,2,12,1,-23532111,-46660434,1,1);
INSERT INTO "ponto" VALUES (239,17,'13:56:00','2013-03-06',NULL,21,2,12,0,-23530193,-46666781,1,1);
INSERT INTO "ponto" VALUES (240,17,'13:56:00','2013-03-06',NULL,21,2,12,0,-23530193,-46666781,1,1);
INSERT INTO "ponto" VALUES (241,17,'15:12:00','2013-03-06',NULL,11,2,12,1,-23528758,-46731851,1,1);
INSERT INTO "ponto" VALUES (242,17,'15:43:00','2013-03-06',NULL,11,2,12,0,-23528735,-46731758,1,1);
INSERT INTO "ponto" VALUES (243,17,'09:03:00','2013-03-07',NULL,18,2,12,1,-23553793,-46708399,1,1);
INSERT INTO "ponto" VALUES (244,17,'10:42:00','2013-03-07',NULL,18,2,12,0,-23552494,-46709312,1,1);
INSERT INTO "ponto" VALUES (245,17,'11:39:00','2013-03-07',NULL,15,2,12,1,-23592497,-46677519,1,1);
INSERT INTO "ponto" VALUES (246,17,'11:39:00','2013-03-07',NULL,15,2,12,1,-23592497,-46677519,1,1);
INSERT INTO "ponto" VALUES (247,17,'13:20:00','2013-03-07',NULL,15,2,12,0,-23595518,-46676392,1,1);
INSERT INTO "ponto" VALUES (248,17,'14:48:00','2013-03-07',NULL,20,2,12,1,-23553811,-46653716,1,1);
INSERT INTO "ponto" VALUES (249,17,'15:36:00','2013-03-07',NULL,20,2,12,0,-23554961,-46655056,1,1);
INSERT INTO "ponto" VALUES (250,17,'08:00:00','2013-03-08',NULL,25,2,12,1,-23548219,-46832618,1,1);
INSERT INTO "ponto" VALUES (251,17,'13:46:00','2013-03-09',NULL,22,2,12,1,-23583022,-46825043,1,1);
INSERT INTO "ponto" VALUES (252,17,'09:14:00','2013-03-11',NULL,20,2,12,1,-23555674,-46656274,1,1);
INSERT INTO "ponto" VALUES (253,17,'10:21:00','2013-03-11',NULL,20,2,12,0,-23553559,-46652800,1,1);
INSERT INTO "ponto" VALUES (254,17,'10:21:00','2013-03-11',NULL,20,2,12,0,-23553559,-46652800,1,1);
INSERT INTO "ponto" VALUES (255,17,'10:40:00','2013-03-11',NULL,14,2,12,1,-23547419,-46647988,1,1);
INSERT INTO "ponto" VALUES (256,17,'13:29:00','2013-03-11',NULL,14,2,12,0,-23543566,-46646747,1,1);
INSERT INTO "ponto" VALUES (257,17,'13:56:00','2013-03-11',NULL,13,2,12,1,-23540511,-46649146,1,1);
INSERT INTO "ponto" VALUES (258,17,'14:26:00','2013-03-11',NULL,13,2,12,0,-23537484,-46650485,1,1);
INSERT INTO "ponto" VALUES (259,17,'14:55:00','2013-03-11',NULL,21,2,12,1,-23531563,-46660843,1,1);
INSERT INTO "ponto" VALUES (260,17,'14:55:00','2013-03-11',NULL,21,2,12,1,-23531563,-46660843,1,1);
INSERT INTO "ponto" VALUES (261,15,'11:43:00','2013-03-11',NULL,29,2,7,1,-23597573,-46614889,1,1);
INSERT INTO "ponto" VALUES (262,15,'11:44:00','2013-03-11',NULL,29,2,7,0,-23597667,-46614909,1,1);
INSERT INTO "ponto" VALUES (263,15,'16:36:00','2013-03-11',NULL,31,2,7,1,-23597677,-46614824,1,1);
INSERT INTO "ponto" VALUES (264,15,'16:37:00','2013-03-11',NULL,31,2,7,0,-23597677,-46614824,1,1);
INSERT INTO "ponto" VALUES (265,15,'16:39:00','2013-03-11',NULL,28,2,7,1,-23597677,-46614824,1,1);
INSERT INTO "ponto" VALUES (266,15,'16:40:00','2013-03-11',NULL,28,2,7,0,-23597677,-46614824,1,1);
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "profissao" (
  "id" int(11) NOT NULL AUTO_INCREMENT,
  "nome" varchar(255) NOT NULL,
  "nome_normalizado" varchar(255) NOT NULL,
  "corporacao_id_INT" int(11) NOT NULL,
  PRIMARY KEY ("id"),
  UNIQUE KEY "nome" ("nome_normalizado","corporacao_id_INT"),
  KEY "profissao_FK_779541016" ("corporacao_id_INT"),
  CONSTRAINT "profissao_FK_779541016" FOREIGN KEY ("corporacao_id_INT") REFERENCES "corporacao" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);
/*!40101 SET character_set_client = @saved_cs_client */;
INSERT INTO "profissao" VALUES (1,'VENDEDOR','VENDEDOR',1);
INSERT INTO "profissao" VALUES (2,'PROMOTOR(A) DE VENDAS','PROMOTOR(A) DE VENDAS',1);
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "rede" (
  "id" int(11) NOT NULL AUTO_INCREMENT,
  "nome" varchar(100) NOT NULL,
  "nome_normalizado" varchar(100) NOT NULL,
  "corporacao_id_INT" int(11) NOT NULL,
  PRIMARY KEY ("id"),
  UNIQUE KEY "rede_unique" ("nome"),
  KEY "rede_FK_554260254" ("corporacao_id_INT"),
  CONSTRAINT "rede_FK_554260254" FOREIGN KEY ("corporacao_id_INT") REFERENCES "corporacao" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);
/*!40101 SET character_set_client = @saved_cs_client */;
INSERT INTO "rede" VALUES (1,'CARREFOUR','CARREFOUR',1);
INSERT INTO "rede" VALUES (2,'LEROY MERLIN','LEROY MERLIN',1);
INSERT INTO "rede" VALUES (4,'EXTRA','EXTRA',1);
INSERT INTO "rede" VALUES (5,'OMEGA','OMEGA',1);
INSERT INTO "rede" VALUES (6,'TELHA NORTE','TELHA NORTE',1);
INSERT INTO "rede" VALUES (7,'TELHANORTE','TELHANORTE',1);
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "rede_empresa" (
  "id" int(11) NOT NULL AUTO_INCREMENT,
  "rede_id_INT" int(11) NOT NULL,
  "empresa_id_INT" int(11) NOT NULL,
  "corporacao_id_INT" int(11) NOT NULL,
  PRIMARY KEY ("id"),
  UNIQUE KEY "rede_empresa_UNIQUE" ("rede_id_INT","empresa_id_INT"),
  KEY "rede_empresa_FK_472778320" ("corporacao_id_INT"),
  CONSTRAINT "rede_empresa_FK_472778320" FOREIGN KEY ("corporacao_id_INT") REFERENCES "corporacao" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);
/*!40101 SET character_set_client = @saved_cs_client */;
INSERT INTO "rede_empresa" VALUES (1,1,3,1);
INSERT INTO "rede_empresa" VALUES (2,2,31,1);
INSERT INTO "rede_empresa" VALUES (4,4,11,1);
INSERT INTO "rede_empresa" VALUES (5,4,12,1);
INSERT INTO "rede_empresa" VALUES (6,4,13,1);
INSERT INTO "rede_empresa" VALUES (7,4,14,1);
INSERT INTO "rede_empresa" VALUES (8,4,15,1);
INSERT INTO "rede_empresa" VALUES (9,4,16,1);
INSERT INTO "rede_empresa" VALUES (10,4,17,1);
INSERT INTO "rede_empresa" VALUES (11,4,18,1);
INSERT INTO "rede_empresa" VALUES (12,4,19,1);
INSERT INTO "rede_empresa" VALUES (13,4,20,1);
INSERT INTO "rede_empresa" VALUES (14,4,21,1);
INSERT INTO "rede_empresa" VALUES (15,4,22,1);
INSERT INTO "rede_empresa" VALUES (16,4,23,1);
INSERT INTO "rede_empresa" VALUES (17,4,24,1);
INSERT INTO "rede_empresa" VALUES (18,4,25,1);
INSERT INTO "rede_empresa" VALUES (19,4,26,1);
INSERT INTO "rede_empresa" VALUES (20,6,29,1);
INSERT INTO "rede_empresa" VALUES (21,6,28,1);
INSERT INTO "rede_empresa" VALUES (22,6,30,1);
INSERT INTO "rede_empresa" VALUES (23,7,32,1);
INSERT INTO "rede_empresa" VALUES (24,7,33,1);
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "relatorio" (
  "id" int(11) NOT NULL AUTO_INCREMENT,
  "titulo" varchar(100) NOT NULL,
  "titulo_normalizado" varchar(100) DEFAULT NULL,
  "descricao" varchar(512) DEFAULT NULL,
  "descricao_normalizado" varchar(512) DEFAULT NULL,
  "pessoa_id_INT" int(11) DEFAULT NULL,
  "empresa_id_INT" int(11) DEFAULT NULL,
  "hora_TIME" time NOT NULL,
  "dia_DATE" date NOT NULL,
  "usuario_id_INT" int(11) DEFAULT NULL,
  "corporacao_id_INT" int(11) NOT NULL,
  PRIMARY KEY ("id"),
  KEY "relatorio_FK_584716797" ("pessoa_id_INT"),
  KEY "relatorio_FK_735870362" ("empresa_id_INT"),
  KEY "relatorio_FK_445983887" ("usuario_id_INT"),
  KEY "relatorio_FK_395233154" ("corporacao_id_INT"),
  CONSTRAINT "relatorio_FK_395233154" FOREIGN KEY ("corporacao_id_INT") REFERENCES "corporacao" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "relatorio_FK_445983887" FOREIGN KEY ("usuario_id_INT") REFERENCES "usuario" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "relatorio_FK_584716797" FOREIGN KEY ("pessoa_id_INT") REFERENCES "pessoa" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "relatorio_FK_735870362" FOREIGN KEY ("empresa_id_INT") REFERENCES "empresa" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);
/*!40101 SET character_set_client = @saved_cs_client */;
INSERT INTO "relatorio" VALUES (1,'MEXE','MEXE','ELA','ELA',NULL,NULL,'01:02:27','2012-11-21',3,1);
INSERT INTO "relatorio" VALUES (2,'SGJFEB','SGJFEB','HDSHH','HDSHH',NULL,NULL,'19:30:48','2012-11-21',3,1);
INSERT INTO "relatorio" VALUES (3,'GGYG','GGYG','HHHFG','HHHFG',NULL,NULL,'19:37:46','2012-11-21',3,1);
INSERT INTO "relatorio" VALUES (4,'FJD','FJD','GJFDR','GJFDR',NULL,NULL,'20:18:12','2012-11-21',3,1);
INSERT INTO "relatorio" VALUES (5,'GHFU','GHFU','FGYRR','FGYRR',NULL,NULL,'20:26:23','2012-11-21',3,1);
INSERT INTO "relatorio" VALUES (6,'FDV','FDV','HJ','HJ',NULL,NULL,'20:31:07','2012-11-21',3,1);
INSERT INTO "relatorio" VALUES (7,'GHDD','GHDD','GHDDE','GHDDE',NULL,NULL,'20:35:52','2012-11-21',3,1);
INSERT INTO "relatorio" VALUES (8,'LFLF','LFLF','HYS','HYS',NULL,NULL,'20:37:21','2012-11-21',3,1);
INSERT INTO "relatorio" VALUES (9,'VHGF','VHGF','FHHD','FHHD',NULL,NULL,'20:40:53','2012-11-21',3,1);
INSERT INTO "relatorio" VALUES (10,'THCD','THCD','FHRE','FHRE',NULL,NULL,'20:45:16','2012-11-21',3,1);
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "relatorio_anexo" (
  "id" int(11) NOT NULL AUTO_INCREMENT,
  "relatorio_id_INT" int(11) NOT NULL,
  "arquivo" varchar(50) NOT NULL,
  "tipo_anexo_id_INT" int(11) NOT NULL,
  "corporacao_id_INT" int(11) NOT NULL,
  PRIMARY KEY ("id"),
  KEY "relatorio_anexo_FK_268829345" ("relatorio_id_INT"),
  KEY "relatorio_anexo_FK_663513184" ("tipo_anexo_id_INT"),
  KEY "relatorio_anexo_FK_730926514" ("corporacao_id_INT"),
  CONSTRAINT "relatorio_anexo_FK_268829345" FOREIGN KEY ("relatorio_id_INT") REFERENCES "relatorio" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "relatorio_anexo_FK_663513184" FOREIGN KEY ("tipo_anexo_id_INT") REFERENCES "tipo_anexo" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "relatorio_anexo_FK_730926514" FOREIGN KEY ("corporacao_id_INT") REFERENCES "corporacao" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);
/*!40101 SET character_set_client = @saved_cs_client */;
INSERT INTO "relatorio_anexo" VALUES (1,1,'2012_11_21_01_02_3_486494249.jpg',1,1);
INSERT INTO "relatorio_anexo" VALUES (2,1,'/mnt/sdcard/PontoEletronicoModel/Temp/2012_11_21_01_01_',2,1);
INSERT INTO "relatorio_anexo" VALUES (3,1,'/mnt/sdcard/PontoEletronicoModel/Temp/2012_11_21_01_01_',3,1);
INSERT INTO "relatorio_anexo" VALUES (4,2,'2012_11_21_19_30_3_1863834472.jpg',1,1);
INSERT INTO "relatorio_anexo" VALUES (5,2,'/mnt/sdcard/PontoEletronicoModel/Temp/2012_11_21_19_29_',2,1);
INSERT INTO "relatorio_anexo" VALUES (6,2,'/mnt/sdcard/PontoEletronicoModel/Temp/2012_11_21_19_30_',3,1);
INSERT INTO "relatorio_anexo" VALUES (7,3,'/mnt/sdcard/PontoEletronicoModel/Temp/2012_11_21_19_37_',2,1);
INSERT INTO "relatorio_anexo" VALUES (8,4,'/mnt/sdcard/PontoEletronicoModel/Temp/2012_11_21_20_17_',2,1);
INSERT INTO "relatorio_anexo" VALUES (9,5,'/mnt/sdcard/PontoEletronicoModel/Temp/2012_11_21_20_25_',2,1);
INSERT INTO "relatorio_anexo" VALUES (10,6,'/mnt/sdcard/PontoEletronicoModel/Temp/2012_11_21_20_30_',2,1);
INSERT INTO "relatorio_anexo" VALUES (11,7,'/mnt/sdcard/PontoEletronicoModel/Temp/2012_11_21_20_35_',2,1);
INSERT INTO "relatorio_anexo" VALUES (12,8,'/mnt/sdcard/PontoEletronicoModel/Temp/2012_11_21_20_36_',2,1);
INSERT INTO "relatorio_anexo" VALUES (13,9,'/mnt/sdcard/PontoEletronicoModel/Temp/2012_11_21_20_40_',2,1);
INSERT INTO "relatorio_anexo" VALUES (14,10,'/mnt/sdcard/PontoEletronicoModel/Temp/2012_11_21_20_44_',2,1);
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "rotina" (
  "id" int(11) NOT NULL AUTO_INCREMENT,
  "ultima_dia_contagem_ciclo_DATE" date NOT NULL,
  "corporacao_id_INT" int(11) NOT NULL,
  PRIMARY KEY ("id"),
  KEY "rotina_FK_259857177" ("corporacao_id_INT"),
  CONSTRAINT "rotina_FK_259857177" FOREIGN KEY ("corporacao_id_INT") REFERENCES "corporacao" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);
/*!40101 SET character_set_client = @saved_cs_client */;
INSERT INTO "rotina" VALUES (1,'2012-10-15',1);
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "servico" (
  "id" int(11) NOT NULL AUTO_INCREMENT,
  "nome" varchar(100) DEFAULT NULL,
  "tag" varchar(100) DEFAULT NULL,
  PRIMARY KEY ("id"),
  UNIQUE KEY "tag" ("nome")
);
/*!40101 SET character_set_client = @saved_cs_client */;
INSERT INTO "servico" VALUES (1,'Tirar Foto Interna','ServiceTiraFoto');
INSERT INTO "servico" VALUES (2,'Rastrear Posi��o','ServiceMyPosition');
INSERT INTO "servico" VALUES (3,'Tirar Foto Externa','ServiceTiraFoto');
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "sexo" (
  "id" int(11) NOT NULL,
  "nome" varchar(30) NOT NULL,
  PRIMARY KEY ("id")
);
/*!40101 SET character_set_client = @saved_cs_client */;
INSERT INTO "sexo" VALUES (1,'Masculino');
INSERT INTO "sexo" VALUES (2,'Feminino');
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "sistema_tabela" (
  "id" int(11) NOT NULL AUTO_INCREMENT,
  "nome" varchar(50) NOT NULL,
  "data_modificacao_estrutura_DATETIME" date DEFAULT NULL,
  "frequencia_sincronizador_INT" int(2) DEFAULT NULL,
  "banco_mobile_BOOLEAN" int(1) DEFAULT NULL,
  "banco_web_BOOLEAN" int(1) DEFAULT NULL,
  "transmissao_web_para_mobile_BOOLEAN" int(1) DEFAULT NULL,
  "transmissao_mobile_para_web_BOOLEAN" int(1) DEFAULT NULL,
  PRIMARY KEY ("id"),
  UNIQUE KEY "nome" ("nome")
);
/*!40101 SET character_set_client = @saved_cs_client */;
INSERT INTO "sistema_tabela" VALUES (1,'bairro',NULL,-1,1,1,1,1);
INSERT INTO "sistema_tabela" VALUES (2,'categoria_permissao',NULL,-1,1,1,1,1);
INSERT INTO "sistema_tabela" VALUES (3,'permissao_categoria_permissao',NULL,-1,1,1,1,1);
INSERT INTO "sistema_tabela" VALUES (4,'cidade',NULL,-1,1,1,1,1);
INSERT INTO "sistema_tabela" VALUES (5,'empresa',NULL,-1,1,1,1,1);
INSERT INTO "sistema_tabela" VALUES (6,'pessoa',NULL,-1,1,1,1,1);
INSERT INTO "sistema_tabela" VALUES (7,'pessoa_empresa',NULL,-1,1,1,1,1);
INSERT INTO "sistema_tabela" VALUES (8,'modelo',NULL,-1,1,1,1,1);
INSERT INTO "sistema_tabela" VALUES (9,'ponto',NULL,-1,1,1,1,1);
INSERT INTO "sistema_tabela" VALUES (10,'profissao',NULL,-1,1,1,1,1);
INSERT INTO "sistema_tabela" VALUES (11,'tarefa',NULL,-1,1,1,1,1);
INSERT INTO "sistema_tabela" VALUES (12,'tipo_empresa',NULL,-1,1,1,1,1);
INSERT INTO "sistema_tabela" VALUES (13,'uf',NULL,-1,1,1,1,1);
INSERT INTO "sistema_tabela" VALUES (14,'usuario',NULL,-1,1,1,1,1);
INSERT INTO "sistema_tabela" VALUES (15,'usuario_categoria_permissao',NULL,-1,1,1,1,1);
INSERT INTO "sistema_tabela" VALUES (16,'usuario_corporacao',NULL,-1,1,1,1,1);
INSERT INTO "sistema_tabela" VALUES (17,'usuario_foto',NULL,-1,1,1,1,1);
INSERT INTO "sistema_tabela" VALUES (18,'usuario_posicao',NULL,-1,0,1,0,0);
INSERT INTO "sistema_tabela" VALUES (19,'usuario_servico',NULL,-1,1,1,1,1);
INSERT INTO "sistema_tabela" VALUES (20,'veiculo',NULL,-1,1,1,1,1);
INSERT INTO "sistema_tabela" VALUES (21,'veiculo_usuario',NULL,-1,1,1,1,1);
INSERT INTO "sistema_tabela" VALUES (22,'pais',NULL,-1,1,1,1,1);
INSERT INTO "sistema_tabela" VALUES (23,'tipo_documento',NULL,-1,1,1,1,1);
INSERT INTO "sistema_tabela" VALUES (24,'empresa_perfil',NULL,-1,1,1,1,1);
INSERT INTO "sistema_tabela" VALUES (30,'perfil',NULL,-1,1,1,1,1);
INSERT INTO "sistema_tabela" VALUES (31,'servico',NULL,-1,1,1,1,1);
INSERT INTO "sistema_tabela" VALUES (39,'sexo',NULL,-1,1,1,1,1);
INSERT INTO "sistema_tabela" VALUES (46,'permissao',NULL,-1,1,1,1,1);
INSERT INTO "sistema_tabela" VALUES (49,'corporacao',NULL,-1,1,1,1,1);
INSERT INTO "sistema_tabela" VALUES (54,'empresa_compra',NULL,-1,1,1,1,1);
INSERT INTO "sistema_tabela" VALUES (55,'empresa_compra_parcela',NULL,-1,1,1,1,1);
INSERT INTO "sistema_tabela" VALUES (56,'empresa_produto',NULL,-1,1,1,1,1);
INSERT INTO "sistema_tabela" VALUES (57,'empresa_produto_compra',NULL,-1,1,1,1,1);
INSERT INTO "sistema_tabela" VALUES (58,'empresa_produto_foto',NULL,-1,1,1,1,1);
INSERT INTO "sistema_tabela" VALUES (59,'empresa_produto_unidade_medida',NULL,0,1,1,1,1);
INSERT INTO "sistema_tabela" VALUES (60,'empresa_produto_venda',NULL,-1,1,1,1,1);
INSERT INTO "sistema_tabela" VALUES (61,'empresa_servico',NULL,-1,1,1,1,1);
INSERT INTO "sistema_tabela" VALUES (62,'empresa_servico_compra',NULL,-1,1,1,1,1);
INSERT INTO "sistema_tabela" VALUES (63,'empresa_servico_foto',NULL,-1,1,1,1,1);
INSERT INTO "sistema_tabela" VALUES (64,'empresa_servico_unidade_medida',NULL,-1,1,1,1,1);
INSERT INTO "sistema_tabela" VALUES (65,'empresa_servico_venda',NULL,-1,1,1,1,1);
INSERT INTO "sistema_tabela" VALUES (66,'empresa_tipo_venda',NULL,-1,1,1,1,1);
INSERT INTO "sistema_tabela" VALUES (67,'empresa_tipo_venda_parcela',NULL,-1,1,1,1,1);
INSERT INTO "sistema_tabela" VALUES (68,'empresa_venda',NULL,-1,1,1,1,1);
INSERT INTO "sistema_tabela" VALUES (69,'empresa_venda_parcela',NULL,-1,1,1,1,1);
INSERT INTO "sistema_tabela" VALUES (70,'versao',NULL,-1,1,1,1,0);
INSERT INTO "sistema_tabela" VALUES (71,'empresa_produto_tipo',NULL,-1,1,1,1,1);
INSERT INTO "sistema_tabela" VALUES (72,'empresa_servico_tipo',NULL,-1,1,1,1,1);
INSERT INTO "sistema_tabela" VALUES (73,'usuario_tipo',NULL,-1,0,1,1,0);
INSERT INTO "sistema_tabela" VALUES (75,'usuario_tipo_corporacao',NULL,-1,1,1,1,1);
INSERT INTO "sistema_tabela" VALUES (76,'usuario_tipo_menu',NULL,0,0,1,1,0);
INSERT INTO "sistema_tabela" VALUES (77,'operadora',NULL,-1,1,1,1,1);
INSERT INTO "sistema_tabela" VALUES (81,'app',NULL,-1,1,1,1,1);
INSERT INTO "sistema_tabela" VALUES (82,'tipo_ponto',NULL,-1,1,1,1,1);
INSERT INTO "sistema_tabela" VALUES (83,'rede',NULL,-1,1,1,1,1);
INSERT INTO "sistema_tabela" VALUES (84,'rede_empresa',NULL,-1,1,1,1,1);
INSERT INTO "sistema_tabela" VALUES (85,'pessoa_empresa_rotina',NULL,-1,1,1,1,1);
INSERT INTO "sistema_tabela" VALUES (86,'rotina',NULL,-1,1,1,1,1);
INSERT INTO "sistema_tabela" VALUES (87,'pessoa_usuario',NULL,-1,1,1,1,1);
INSERT INTO "sistema_tabela" VALUES (89,'relatorio',NULL,-1,1,1,1,1);
INSERT INTO "sistema_tabela" VALUES (90,'tipo_anexo',NULL,-1,1,1,1,1);
INSERT INTO "sistema_tabela" VALUES (91,'relatorio_anexo',NULL,-1,1,1,1,1);
INSERT INTO "sistema_tabela" VALUES (104,'acesso',NULL,0,0,1,1,0);
INSERT INTO "sistema_tabela" VALUES (125,'configuracao',NULL,0,0,1,0,1);
INSERT INTO "sistema_tabela" VALUES (126,'estado_civil',NULL,1,1,1,1,0);
INSERT INTO "sistema_tabela" VALUES (130,'usuario_menu',NULL,0,0,1,0,0);
INSERT INTO "sistema_tabela" VALUES (138,'ponto_endereco',NULL,0,0,1,0,0);
INSERT INTO "sistema_tabela" VALUES (139,'sistema_tabela',NULL,-1,1,1,1,0);
INSERT INTO "sistema_tabela" VALUES (143,'forma_pagamento',NULL,-1,1,1,1,1);
INSERT INTO "sistema_tabela" VALUES (148,'operacao_sistema',NULL,0,0,1,0,0);
INSERT INTO "sistema_tabela" VALUES (152,'usuario_privilegio',NULL,0,0,1,0,0);
INSERT INTO "sistema_tabela" VALUES (167,'usuario_tipo_privilegio',NULL,0,0,1,0,0);
INSERT INTO "sistema_tabela" VALUES (168,'usuario_foto_configuracao',NULL,-1,1,1,1,1);
INSERT INTO "sistema_tabela" VALUES (171,'sistema_tipo_operacao_banco',NULL,1,1,1,1,0);
INSERT INTO "sistema_tabela" VALUES (176,'sistema_registro_sincronizador',NULL,0,1,1,0,0);
INSERT INTO "sistema_tabela" VALUES (238,'sistema_banco_versao',NULL,-1,1,1,0,0);
INSERT INTO "sistema_tabela" VALUES (253,'sistema_usuario_sincronizador',NULL,0,0,1,0,0);
INSERT INTO "sistema_tabela" VALUES (257,'sistema_log_erro_mobile',NULL,-1,1,1,0,1);
INSERT INTO "sistema_tabela" VALUES (259,'sistema_download_banco_usuario_sincronizador',NULL,0,0,1,0,0);
INSERT INTO "sistema_tabela" VALUES (277,'sistema',NULL,0,0,1,0,0);
INSERT INTO "sistema_tabela" VALUES (307,'usuario_empresa',NULL,0,0,1,0,0);
INSERT INTO "sistema_tabela" VALUES (317,'sistema_tipo_mobile',NULL,1,1,1,1,0);
INSERT INTO "sistema_tabela" VALUES (327,'sistema_tipo_log_erro',NULL,-1,1,1,0,1);
INSERT INTO "sistema_tabela" VALUES (332,'sistema_operacao_mobile',NULL,0,0,1,0,0);
INSERT INTO "sistema_tabela" VALUES (335,'sistema_mobile_conectado',NULL,0,0,1,0,0);
INSERT INTO "sistema_tabela" VALUES (336,'sistema_tipo_acao_mobile',NULL,1,1,1,1,0);
INSERT INTO "sistema_tabela" VALUES (337,'sistema_usuario_mensagem',NULL,-1,1,1,0,1);
INSERT INTO "sistema_tabela" VALUES (343,'sistema_tipo_download_arquivo',NULL,0,0,1,0,0);
INSERT INTO "sistema_tabela" VALUES (344,'sistema_tipo_usuario_mensagem',NULL,1,1,1,1,0);
INSERT INTO "sistema_tabela" VALUES (441,'horario_trabalho_pessoa_empresa',NULL,-1,1,1,0,1);
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "sistema_tipo_acao_mobile" (
  "id" int(11) NOT NULL AUTO_INCREMENT,
  "nome" varchar(150) NOT NULL,
  PRIMARY KEY ("id"),
  UNIQUE KEY "sistema_tipo_acao_mobile_UNIQUE" ("nome")
);
/*!40101 SET character_set_client = @saved_cs_client */;
INSERT INTO "sistema_tipo_acao_mobile" VALUES (4,'criar estrutura banco sqlite do mobile no servidor');
INSERT INTO "sistema_tipo_acao_mobile" VALUES (2,'transferir arquivo banco sqlite do mobile para o servidor');
INSERT INTO "sistema_tipo_acao_mobile" VALUES (1,'transferir arquivo banco sqlite do servidor para o mobile');
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "sistema_tipo_mobile" (
  "id" int(11) NOT NULL AUTO_INCREMENT,
  "nome" varchar(50) NOT NULL,
  PRIMARY KEY ("id")
);
/*!40101 SET character_set_client = @saved_cs_client */;
INSERT INTO "sistema_tipo_mobile" VALUES (1,'android');
INSERT INTO "sistema_tipo_mobile" VALUES (2,'iphone');
INSERT INTO "sistema_tipo_mobile" VALUES (3,'windows 8');
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "sistema_tipo_operacao_banco" (
  "id" int(11) NOT NULL AUTO_INCREMENT,
  "nome" varchar(10) DEFAULT NULL,
  PRIMARY KEY ("id"),
  UNIQUE KEY "nome" ("nome")
);
/*!40101 SET character_set_client = @saved_cs_client */;
INSERT INTO "sistema_tipo_operacao_banco" VALUES (3,'Editar');
INSERT INTO "sistema_tipo_operacao_banco" VALUES (2,'Inserir');
INSERT INTO "sistema_tipo_operacao_banco" VALUES (1,'Remover');
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "sistema_tipo_usuario_mensagem" (
  "id" int(11) NOT NULL AUTO_INCREMENT,
  "nome" varchar(50) NOT NULL,
  PRIMARY KEY ("id")
);
/*!40101 SET character_set_client = @saved_cs_client */;
INSERT INTO "sistema_tipo_usuario_mensagem" VALUES (1,'erro');
INSERT INTO "sistema_tipo_usuario_mensagem" VALUES (2,'sugestao melhoria');
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "tarefa" (
  "id" int(11) NOT NULL AUTO_INCREMENT,
  "criado_pelo_usuario_id_INT" int(11) DEFAULT NULL,
  "categoria_permissao_id_INT" int(11) DEFAULT NULL,
  "veiculo_id_INT" int(11) DEFAULT NULL,
  "usuario_id_INT" int(11) DEFAULT NULL,
  "veiculo_usuario_id_INT" int(11) DEFAULT NULL,
  "origem_pessoa_id_INT" int(11) DEFAULT NULL,
  "origem_empresa_id_INT" int(11) DEFAULT NULL,
  "origem_logradouro" varchar(255) DEFAULT NULL,
  "origem_logradouro_normalizado" varchar(255) DEFAULT NULL,
  "origem_numero" varchar(30) DEFAULT NULL,
  "origem_complemento" varchar(255) DEFAULT NULL,
  "origem_complemento_normalizado" varchar(255) DEFAULT NULL,
  "origem_cidade_id_INT" int(11) DEFAULT NULL,
  "origem_bairro_id_INT" int(11) DEFAULT NULL,
  "origem_latitude_INT" int(6) DEFAULT NULL,
  "origem_longitude_INT" int(6) DEFAULT NULL,
  "origem_latitude_real_INT" int(6) DEFAULT NULL,
  "origem_longitude_real_INT" int(6) DEFAULT NULL,
  "destino_pessoa_id_INT" int(11) DEFAULT NULL,
  "destino_empresa_id_INT" int(11) DEFAULT NULL,
  "destino_logradouro" varchar(255) DEFAULT NULL,
  "destino_logradouro_normalizado" varchar(255) DEFAULT NULL,
  "destino_numero" varchar(30) DEFAULT NULL,
  "destino_complemento" varchar(255) DEFAULT NULL,
  "destino_complemento_normalizado" varchar(255) DEFAULT NULL,
  "destino_cidade_id_INT" int(11) DEFAULT NULL,
  "destino_bairro_id_INT" int(11) DEFAULT NULL,
  "destino_latitude_INT" int(6) DEFAULT NULL,
  "destino_longitude_INT" int(6) DEFAULT NULL,
  "destino_latitude_real_INT" int(6) DEFAULT NULL,
  "destino_longitude_real_INT" int(6) DEFAULT NULL,
  "tempo_estimado_carro_INT" int(11) DEFAULT NULL,
  "tempo_estimado_a_pe_INT" int(11) DEFAULT NULL,
  "distancia_estimada_carro_INT" int(11) DEFAULT NULL,
  "distancia_estimada_a_pe_INT" int(11) DEFAULT NULL,
  "is_realizada_a_pe_BOOLEAN" int(1) DEFAULT NULL,
  "inicio_data_programada_DATE" date DEFAULT NULL,
  "inicio_hora_programada_TIME" time DEFAULT NULL,
  "data_exibir_DATE" date DEFAULT NULL,
  "inicio_data_DATE" date DEFAULT NULL,
  "inicio_hora_TIME" time DEFAULT NULL,
  "fim_data_DATE" date DEFAULT NULL,
  "fim_hora_TIME" time DEFAULT NULL,
  "data_cadastro_DATE" date DEFAULT NULL,
  "hora_cadastro_TIME" time DEFAULT NULL,
  "titulo" varchar(100) DEFAULT NULL,
  "titulo_normalizado" varchar(100) DEFAULT NULL,
  "descricao" varchar(512) DEFAULT NULL,
  "descricao_normalizado" varchar(512) DEFAULT NULL,
  "corporacao_id_INT" int(11) NOT NULL,
  PRIMARY KEY ("id"),
  KEY "tarefa_FK_490081787" ("origem_cidade_id_INT"),
  KEY "tarefa_FK_748779297" ("origem_bairro_id_INT"),
  KEY "tarefa_FK_618682861" ("destino_cidade_id_INT"),
  KEY "tarefa_FK_672546387" ("destino_bairro_id_INT"),
  KEY "tarefa_FK_90545654" ("corporacao_id_INT"),
  KEY "tabela_FK_100" ("usuario_id_INT"),
  KEY "tarefa_FK_529296875" ("criado_pelo_usuario_id_INT"),
  KEY "tarefa_FK_70098877" ("categoria_permissao_id_INT"),
  KEY "tarefa_FK_751892090" ("veiculo_id_INT"),
  KEY "tarefa_FK_114227295" ("veiculo_usuario_id_INT"),
  KEY "tarefa_FK_718139649" ("origem_pessoa_id_INT"),
  KEY "tarefa_FK_466461182" ("origem_empresa_id_INT"),
  KEY "tarefa_FK_517883301" ("destino_pessoa_id_INT"),
  KEY "tarefa_FK_919769288" ("destino_empresa_id_INT"),
  CONSTRAINT "tabela_FK_100" FOREIGN KEY ("usuario_id_INT") REFERENCES "usuario" ("id") ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT "tarefa_FK_114227295" FOREIGN KEY ("veiculo_usuario_id_INT") REFERENCES "veiculo_usuario" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "tarefa_FK_466461182" FOREIGN KEY ("origem_empresa_id_INT") REFERENCES "origem_empresa" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "tarefa_FK_517883301" FOREIGN KEY ("destino_pessoa_id_INT") REFERENCES "destino_pessoa" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "tarefa_FK_529296875" FOREIGN KEY ("criado_pelo_usuario_id_INT") REFERENCES "criado_pelo_usuario" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "tarefa_FK_618682861" FOREIGN KEY ("destino_cidade_id_INT") REFERENCES "cidade" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "tarefa_FK_672546387" FOREIGN KEY ("destino_bairro_id_INT") REFERENCES "bairro" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "tarefa_FK_70098877" FOREIGN KEY ("categoria_permissao_id_INT") REFERENCES "categoria_permissao" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "tarefa_FK_718139649" FOREIGN KEY ("origem_pessoa_id_INT") REFERENCES "origem_pessoa" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "tarefa_FK_748779297" FOREIGN KEY ("origem_bairro_id_INT") REFERENCES "bairro" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "tarefa_FK_751892090" FOREIGN KEY ("veiculo_id_INT") REFERENCES "veiculo" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "tarefa_FK_90545654" FOREIGN KEY ("corporacao_id_INT") REFERENCES "corporacao" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "tarefa_FK_919769288" FOREIGN KEY ("destino_empresa_id_INT") REFERENCES "destino_empresa" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);
/*!40101 SET character_set_client = @saved_cs_client */;
INSERT INTO "tarefa" VALUES (17,2,NULL,NULL,2,NULL,NULL,2,'RUA FLOR MAIO','RUA FLOR MAIO','655',NULL,NULL,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2012-10-17','10:00:00','2012-10-17',NULL,NULL,NULL,NULL,'2012-10-16','14:56:47','VISITA A BAUTECH','VISITA A BAUTECH','INSTALAR EQUIPAMENTO DE TESTE\r\nCONFERIR ESTOQUE DO PRODUTO DE TESTE','INSTALAR EQUIPAMENTO DE TESTE\r\nCONFERIR ESTOQUE DO PRODUTO DE TESTE',1);
INSERT INTO "tarefa" VALUES (18,2,NULL,NULL,2,NULL,NULL,2,'RUA FLOR MAIO','RUA FLOR MAIO','655',NULL,NULL,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2012-10-17','10:00:00','2012-10-17',NULL,NULL,NULL,NULL,'2012-10-16','14:56:47','VISITA A BAUTECH','VISITA A BAUTECH','INSTALAR EQUIPAMENTO DE TESTE\r\nCONFERIR ESTOQUE DO PRODUTO DE TESTE','INSTALAR EQUIPAMENTO DE TESTE\r\nCONFERIR ESTOQUE DO PRODUTO DE TESTE',1);
INSERT INTO "tarefa" VALUES (19,2,NULL,NULL,2,NULL,NULL,2,'RUA FLOR MAIO','RUA FLOR MAIO','655',NULL,NULL,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2012-10-19','10:00:00','2012-10-19',NULL,NULL,NULL,NULL,'2012-10-16','14:56:47','VISITA A BAUTECH','VISITA A BAUTECH','INSTALAR EQUIPAMENTO DE TESTE\r\nCONFERIR ESTOQUE DO PRODUTO DE TESTE','INSTALAR EQUIPAMENTO DE TESTE\r\nCONFERIR ESTOQUE DO PRODUTO DE TESTE',1);
INSERT INTO "tarefa" VALUES (20,2,NULL,NULL,2,NULL,NULL,2,'RUA FLOR MAIO','RUA FLOR MAIO','655',NULL,NULL,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2012-10-22','10:00:00','2012-10-22',NULL,NULL,NULL,NULL,'2012-10-16','14:56:47','VISITA A BAUTECH','VISITA A BAUTECH','INSTALAR EQUIPAMENTO DE TESTE\r\nCONFERIR ESTOQUE DO PRODUTO DE TESTE','INSTALAR EQUIPAMENTO DE TESTE\r\nCONFERIR ESTOQUE DO PRODUTO DE TESTE',1);
INSERT INTO "tarefa" VALUES (21,2,NULL,NULL,2,NULL,NULL,2,'RUA FLOR MAIO','RUA FLOR MAIO','655',NULL,NULL,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2012-10-24','10:00:00','2012-10-24',NULL,NULL,NULL,NULL,'2012-10-16','14:56:47','VISITA A BAUTECH','VISITA A BAUTECH','INSTALAR EQUIPAMENTO DE TESTE\r\nCONFERIR ESTOQUE DO PRODUTO DE TESTE','INSTALAR EQUIPAMENTO DE TESTE\r\nCONFERIR ESTOQUE DO PRODUTO DE TESTE',1);
INSERT INTO "tarefa" VALUES (22,2,NULL,NULL,2,NULL,NULL,2,'RUA FLOR MAIO','RUA FLOR MAIO','655',NULL,NULL,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2012-10-26','10:00:00','2012-10-26',NULL,NULL,NULL,NULL,'2012-10-16','14:56:47','VISITA A BAUTECH','VISITA A BAUTECH','INSTALAR EQUIPAMENTO DE TESTE\r\nCONFERIR ESTOQUE DO PRODUTO DE TESTE','INSTALAR EQUIPAMENTO DE TESTE\r\nCONFERIR ESTOQUE DO PRODUTO DE TESTE',1);
INSERT INTO "tarefa" VALUES (23,2,NULL,NULL,2,NULL,NULL,2,'RUA FLOR MAIO','RUA FLOR MAIO','655',NULL,NULL,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2012-10-29','10:00:00','2012-10-29',NULL,NULL,NULL,NULL,'2012-10-16','14:56:47','VISITA A BAUTECH','VISITA A BAUTECH','INSTALAR EQUIPAMENTO DE TESTE\r\nCONFERIR ESTOQUE DO PRODUTO DE TESTE','INSTALAR EQUIPAMENTO DE TESTE\r\nCONFERIR ESTOQUE DO PRODUTO DE TESTE',1);
INSERT INTO "tarefa" VALUES (24,2,NULL,NULL,2,NULL,NULL,2,'RUA FLOR MAIO','RUA FLOR MAIO','655',NULL,NULL,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2012-10-31','10:00:00','2012-10-31',NULL,NULL,NULL,NULL,'2012-10-16','14:56:47','VISITA A BAUTECH','VISITA A BAUTECH','INSTALAR EQUIPAMENTO DE TESTE\r\nCONFERIR ESTOQUE DO PRODUTO DE TESTE','INSTALAR EQUIPAMENTO DE TESTE\r\nCONFERIR ESTOQUE DO PRODUTO DE TESTE',1);
INSERT INTO "tarefa" VALUES (25,2,NULL,NULL,2,NULL,NULL,2,'RUA FLOR MAIO','RUA FLOR MAIO','655',NULL,NULL,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2012-11-02','10:00:00','2012-11-02',NULL,NULL,NULL,NULL,'2012-10-16','14:56:47','VISITA A BAUTECH','VISITA A BAUTECH','INSTALAR EQUIPAMENTO DE TESTE\r\nCONFERIR ESTOQUE DO PRODUTO DE TESTE','INSTALAR EQUIPAMENTO DE TESTE\r\nCONFERIR ESTOQUE DO PRODUTO DE TESTE',1);
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "tipo_anexo" (
  "id" int(11) NOT NULL AUTO_INCREMENT,
  "nome" varchar(30) NOT NULL,
  PRIMARY KEY ("id")
);
/*!40101 SET character_set_client = @saved_cs_client */;
INSERT INTO "tipo_anexo" VALUES (1,'foto');
INSERT INTO "tipo_anexo" VALUES (2,'audio');
INSERT INTO "tipo_anexo" VALUES (3,'filme');
INSERT INTO "tipo_anexo" VALUES (4,'arquivo');
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "tipo_documento" (
  "id" int(11) NOT NULL AUTO_INCREMENT,
  "nome" varchar(100) NOT NULL,
  "nome_normalizado" varchar(100) NOT NULL,
  "corporacao_id_INT" int(11) NOT NULL,
  PRIMARY KEY ("id"),
  KEY "tipo_documento_FK_958312989" ("corporacao_id_INT"),
  CONSTRAINT "tipo_documento_FK_958312989" FOREIGN KEY ("corporacao_id_INT") REFERENCES "corporacao" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "tipo_empresa" (
  "id" int(11) NOT NULL AUTO_INCREMENT,
  "nome" varchar(100) NOT NULL,
  "nome_normalizado" varchar(100) NOT NULL,
  "corporacao_id_INT" int(11) NOT NULL,
  PRIMARY KEY ("id"),
  UNIQUE KEY "nome" ("nome","corporacao_id_INT"),
  KEY "tipo_empresa_FK_221710205" ("corporacao_id_INT"),
  CONSTRAINT "tipo_empresa_FK_221710205" FOREIGN KEY ("corporacao_id_INT") REFERENCES "corporacao" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);
/*!40101 SET character_set_client = @saved_cs_client */;
INSERT INTO "tipo_empresa" VALUES (1,'MARKETING E PUBLICIDADE','MARKETING E PUBLICIDADE',1);
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "tipo_ponto" (
  "id" int(11) NOT NULL AUTO_INCREMENT,
  "nome" varchar(30) DEFAULT NULL,
  PRIMARY KEY ("id")
);
/*!40101 SET character_set_client = @saved_cs_client */;
INSERT INTO "tipo_ponto" VALUES (1,'Jornada de Trabalho');
INSERT INTO "tipo_ponto" VALUES (2,'Caf�');
INSERT INTO "tipo_ponto" VALUES (3,'Almo�o');
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "uf" (
  "id" int(11) NOT NULL AUTO_INCREMENT,
  "nome" varchar(255) NOT NULL,
  "nome_normalizado" varchar(255) NOT NULL,
  "sigla" char(2) DEFAULT NULL,
  "pais_id_INT" int(11) NOT NULL,
  "corporacao_id_INT" int(11) NOT NULL,
  PRIMARY KEY ("id"),
  UNIQUE KEY "nome2" ("nome_normalizado","corporacao_id_INT","pais_id_INT"),
  KEY "uf_FK_728454590" ("pais_id_INT"),
  KEY "uf_FK_724060059" ("corporacao_id_INT"),
  CONSTRAINT "uf_FK_724060059" FOREIGN KEY ("corporacao_id_INT") REFERENCES "corporacao" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "uf_FK_728454590" FOREIGN KEY ("pais_id_INT") REFERENCES "pais" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);
/*!40101 SET character_set_client = @saved_cs_client */;
INSERT INTO "uf" VALUES (1,'S�O PAULO','SAO PAULO','SP',1,1);
INSERT INTO "uf" VALUES (2,'MINAS GERAIS','MINAS GERAIS','MG',1,1);
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "usuario" (
  "id" int(11) NOT NULL AUTO_INCREMENT,
  "nome" varchar(255) NOT NULL,
  "nome_normalizado" varchar(255) NOT NULL,
  "email" varchar(255) NOT NULL,
  "senha" varchar(100) DEFAULT NULL,
  "status_BOOLEAN" int(1) DEFAULT NULL,
  "pagina_inicial" text,
  PRIMARY KEY ("id"),
  UNIQUE KEY "email" ("email")
);
/*!40101 SET character_set_client = @saved_cs_client */;
INSERT INTO "usuario" VALUES (1,'ANDR�','ANDRE','andre@factu.com.br','18563035b358c4f559f4bbc16004bf79',1,'');
INSERT INTO "usuario" VALUES (2,'EDUARDO ALVES','EDUARDO ALVES','eduardo@omegasoftware.com.br','18563035b358c4f559f4bbc16004bf79',1,'');
INSERT INTO "usuario" VALUES (3,'ROGER','ROGER','roger@omegasoftware.com.br','18563035b358c4f559f4bbc16004bf79',1,'');
INSERT INTO "usuario" VALUES (4,'KELLY','KELLY','kelly@factu.com.br','18563035b358c4f559f4bbc16004bf79',1,'');
INSERT INTO "usuario" VALUES (5,'RICARDO BRASIL','RICARDO BRASIL','andre.brasil@factu.com.br','18563035b358c4f559f4bbc16004bf79',1,'');
INSERT INTO "usuario" VALUES (6,'RICARDO','RICARDO','rb01@factu.com.br','517e2715ef55789988df1bcc61156205',1,'');
INSERT INTO "usuario" VALUES (7,'TATIANA YAMADA','TATIANA YAMADA','bautech01@factu.com.br','517e2715ef55789988df1bcc61156205',1,'');
INSERT INTO "usuario" VALUES (8,'RAISSA','RAISSA','raissa@factu.com.br','18563035b358c4f559f4bbc16004bf79',1,'');
INSERT INTO "usuario" VALUES (9,'EDUARDO DE TESTE','','eduardo2@omegasoftware.com.br','8ffbcb8b27121de4a439478002b454cd',1,'');
INSERT INTO "usuario" VALUES (10,'LUIS DESTITO','LUIS DESTITO','luis@factu.com.br','1d15abf444adf71e9387fa56b7694ec5',1,'');
INSERT INTO "usuario" VALUES (12,'ADRIANO','ADRIANO','rb02@factu.com.br','18563035b358c4f559f4bbc16004bf79',1,'');
INSERT INTO "usuario" VALUES (13,'GLAUCO','GLAUCO','glauco@rbalimentos.com.br','8644dff3bbca1deda27d072da5958ca6',1,'');
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "usuario_categoria_permissao" (
  "id" int(11) NOT NULL AUTO_INCREMENT,
  "usuario_id_INT" int(11) NOT NULL,
  "categoria_permissao_id_INT" int(11) NOT NULL,
  "corporacao_id_INT" int(11) NOT NULL,
  PRIMARY KEY ("id"),
  UNIQUE KEY "usuario_id_INT" ("usuario_id_INT","corporacao_id_INT"),
  KEY "usuario_categoria_permissao_FK_734741211" ("categoria_permissao_id_INT"),
  KEY "usuario_categoria_permissao_FK_776031494" ("corporacao_id_INT"),
  CONSTRAINT "usuario_categoria_permissao_FK_346038818" FOREIGN KEY ("usuario_id_INT") REFERENCES "usuario" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "usuario_categoria_permissao_FK_404357910" FOREIGN KEY ("usuario_id_INT") REFERENCES "usuario" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "usuario_categoria_permissao_FK_499389648" FOREIGN KEY ("usuario_id_INT") REFERENCES "usuario" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "usuario_categoria_permissao_FK_516815186" FOREIGN KEY ("usuario_id_INT") REFERENCES "usuario" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "usuario_categoria_permissao_FK_540985107" FOREIGN KEY ("usuario_id_INT") REFERENCES "usuario" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "usuario_categoria_permissao_FK_578552246" FOREIGN KEY ("usuario_id_INT") REFERENCES "usuario" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "usuario_categoria_permissao_FK_642578125" FOREIGN KEY ("usuario_id_INT") REFERENCES "usuario" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "usuario_categoria_permissao_FK_734741211" FOREIGN KEY ("categoria_permissao_id_INT") REFERENCES "categoria_permissao" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "usuario_categoria_permissao_FK_776031494" FOREIGN KEY ("corporacao_id_INT") REFERENCES "corporacao" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "usuario_categoria_permissao_FK_810546875" FOREIGN KEY ("usuario_id_INT") REFERENCES "usuario" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "usuario_categoria_permissao_FK_852142334" FOREIGN KEY ("usuario_id_INT") REFERENCES "usuario" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);
/*!40101 SET character_set_client = @saved_cs_client */;
INSERT INTO "usuario_categoria_permissao" VALUES (1,2,1,1);
INSERT INTO "usuario_categoria_permissao" VALUES (2,6,1,1);
INSERT INTO "usuario_categoria_permissao" VALUES (3,7,1,1);
INSERT INTO "usuario_categoria_permissao" VALUES (4,8,1,1);
INSERT INTO "usuario_categoria_permissao" VALUES (5,9,1,1);
INSERT INTO "usuario_categoria_permissao" VALUES (6,12,1,1);
INSERT INTO "usuario_categoria_permissao" VALUES (7,13,1,1);
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "usuario_corporacao" (
  "id" int(11) NOT NULL AUTO_INCREMENT,
  "usuario_id_INT" int(11) NOT NULL,
  "corporacao_id_INT" int(11) NOT NULL,
  "status_BOOLEAN" int(1) NOT NULL,
  "is_adm_BOOLEAN" int(1) NOT NULL,
  PRIMARY KEY ("id"),
  UNIQUE KEY "usuario_corporacao_UNIQUE" ("usuario_id_INT","corporacao_id_INT"),
  KEY "usuario_corporacao_FK_972991944" ("usuario_id_INT"),
  KEY "usuario_corporacao_FK_135986328" ("corporacao_id_INT"),
  CONSTRAINT "usuario_corporacao_FK_135986328" FOREIGN KEY ("corporacao_id_INT") REFERENCES "corporacao" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "usuario_corporacao_FK_972991944" FOREIGN KEY ("usuario_id_INT") REFERENCES "usuario" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);
/*!40101 SET character_set_client = @saved_cs_client */;
INSERT INTO "usuario_corporacao" VALUES (1,1,1,1,1);
INSERT INTO "usuario_corporacao" VALUES (2,2,1,1,0);
INSERT INTO "usuario_corporacao" VALUES (3,3,1,1,1);
INSERT INTO "usuario_corporacao" VALUES (7,7,1,1,0);
INSERT INTO "usuario_corporacao" VALUES (17,10,1,1,0);
INSERT INTO "usuario_corporacao" VALUES (19,8,1,1,1);
INSERT INTO "usuario_corporacao" VALUES (23,13,1,1,0);
INSERT INTO "usuario_corporacao" VALUES (24,12,1,1,0);
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "usuario_foto" (
  "id" int(11) NOT NULL AUTO_INCREMENT,
  "data_DATE" date DEFAULT NULL,
  "hora_TIME" time DEFAULT NULL,
  "foto_interna" varchar(50) DEFAULT NULL,
  "foto_externa" varchar(50) DEFAULT NULL,
  "usuario_id_INT" int(11) DEFAULT NULL,
  "corporacao_id_INT" int(11) DEFAULT NULL,
  PRIMARY KEY ("id"),
  KEY "usuario_foto_FK_640136719" ("usuario_id_INT"),
  KEY "usuario_foto_FK_401641846" ("corporacao_id_INT"),
  CONSTRAINT "usuario_foto_FK_401641846" FOREIGN KEY ("corporacao_id_INT") REFERENCES "corporacao" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "usuario_foto_FK_640136719" FOREIGN KEY ("usuario_id_INT") REFERENCES "usuario" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "usuario_foto_configuracao" (
  "id" int(11) NOT NULL AUTO_INCREMENT,
  "is_camera_interna_BOOLEAN" int(1) NOT NULL,
  "periodo_segundo_INT" int(7) NOT NULL,
  "usuario_id_INT" int(11) NOT NULL,
  "corporacao_id_INT" int(11) NOT NULL,
  PRIMARY KEY ("id"),
  KEY "usuario_foto_configuracao_FK_765228272" ("usuario_id_INT"),
  KEY "usuario_foto_configuracao_FK_640441895" ("corporacao_id_INT"),
  CONSTRAINT "usuario_foto_configuracao_FK_640441895" FOREIGN KEY ("corporacao_id_INT") REFERENCES "corporacao" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "usuario_foto_configuracao_FK_765228272" FOREIGN KEY ("usuario_id_INT") REFERENCES "usuario" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "usuario_servico" (
  "id" int(11) NOT NULL AUTO_INCREMENT,
  "status_BOOLEAN" int(1) NOT NULL,
  "servico_id_INT" int(11) NOT NULL,
  "usuario_id_INT" int(11) NOT NULL,
  "corporacao_id_INT" int(11) DEFAULT NULL,
  PRIMARY KEY ("id"),
  UNIQUE KEY "servico_id_INT" ("servico_id_INT","usuario_id_INT","corporacao_id_INT"),
  KEY "usuario_servico_FK_158416748" ("usuario_id_INT"),
  KEY "usuario_servico_FK_909790039" ("corporacao_id_INT"),
  CONSTRAINT "usuario_servico_FK_136291504" FOREIGN KEY ("servico_id_INT") REFERENCES "servico" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "usuario_servico_FK_158416748" FOREIGN KEY ("usuario_id_INT") REFERENCES "usuario" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "usuario_servico_FK_39550781" FOREIGN KEY ("servico_id_INT") REFERENCES "servico" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "usuario_servico_FK_454101562" FOREIGN KEY ("servico_id_INT") REFERENCES "servico" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "usuario_servico_FK_591949463" FOREIGN KEY ("servico_id_INT") REFERENCES "servico" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "usuario_servico_FK_652526856" FOREIGN KEY ("servico_id_INT") REFERENCES "servico" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "usuario_servico_FK_744171143" FOREIGN KEY ("servico_id_INT") REFERENCES "servico" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "usuario_servico_FK_770629883" FOREIGN KEY ("servico_id_INT") REFERENCES "servico" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "usuario_servico_FK_909790039" FOREIGN KEY ("corporacao_id_INT") REFERENCES "corporacao" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);
/*!40101 SET character_set_client = @saved_cs_client */;
INSERT INTO "usuario_servico" VALUES (1,0,2,3,1);
INSERT INTO "usuario_servico" VALUES (2,0,2,2,1);
INSERT INTO "usuario_servico" VALUES (3,0,3,3,1);
INSERT INTO "usuario_servico" VALUES (4,0,1,2,1);
INSERT INTO "usuario_servico" VALUES (5,1,1,6,1);
INSERT INTO "usuario_servico" VALUES (6,1,2,6,1);
INSERT INTO "usuario_servico" VALUES (7,1,3,6,1);
INSERT INTO "usuario_servico" VALUES (8,0,1,4,1);
INSERT INTO "usuario_servico" VALUES (9,0,2,4,1);
INSERT INTO "usuario_servico" VALUES (10,0,3,4,1);
INSERT INTO "usuario_servico" VALUES (11,1,2,7,1);
INSERT INTO "usuario_servico" VALUES (12,0,3,7,1);
INSERT INTO "usuario_servico" VALUES (13,0,1,7,1);
INSERT INTO "usuario_servico" VALUES (14,1,2,8,1);
INSERT INTO "usuario_servico" VALUES (15,0,3,8,1);
INSERT INTO "usuario_servico" VALUES (16,0,1,8,1);
INSERT INTO "usuario_servico" VALUES (17,1,2,9,1);
INSERT INTO "usuario_servico" VALUES (18,0,3,9,1);
INSERT INTO "usuario_servico" VALUES (19,0,1,9,1);
INSERT INTO "usuario_servico" VALUES (20,0,2,10,1);
INSERT INTO "usuario_servico" VALUES (21,0,3,10,1);
INSERT INTO "usuario_servico" VALUES (22,0,1,10,1);
INSERT INTO "usuario_servico" VALUES (1706,0,3,2,1);
INSERT INTO "usuario_servico" VALUES (1710,1,2,12,1);
INSERT INTO "usuario_servico" VALUES (1711,1,3,12,1);
INSERT INTO "usuario_servico" VALUES (1712,1,1,12,1);
INSERT INTO "usuario_servico" VALUES (1713,0,2,13,1);
INSERT INTO "usuario_servico" VALUES (1714,0,3,13,1);
INSERT INTO "usuario_servico" VALUES (1715,0,1,13,1);
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "usuario_tipo_corporacao" (
  "id" int(11) NOT NULL AUTO_INCREMENT,
  "usuario_id_INT" int(11) NOT NULL,
  "usuario_tipo_id_INT" int(11) NOT NULL,
  "corporacao_id_INT" int(11) NOT NULL,
  PRIMARY KEY ("id"),
  UNIQUE KEY "usuario_tipo_corporacao_UNIQUE" ("usuario_id_INT","corporacao_id_INT"),
  KEY "usuario_tipo_corporacao_FK_644226074" ("usuario_id_INT"),
  KEY "usuario_tipo_corporacao_FK_503631592" ("usuario_tipo_id_INT"),
  KEY "usuario_tipo_corporacao_FK_788208008" ("corporacao_id_INT"),
  CONSTRAINT "usuario_tipo_corporacao_FK_503631592" FOREIGN KEY ("usuario_tipo_id_INT") REFERENCES "usuario_tipo" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "usuario_tipo_corporacao_FK_644226074" FOREIGN KEY ("usuario_id_INT") REFERENCES "usuario" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "usuario_tipo_corporacao_FK_788208008" FOREIGN KEY ("corporacao_id_INT") REFERENCES "corporacao" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);
/*!40101 SET character_set_client = @saved_cs_client */;
INSERT INTO "usuario_tipo_corporacao" VALUES (1,1,1,1);
INSERT INTO "usuario_tipo_corporacao" VALUES (2,2,1,1);
INSERT INTO "usuario_tipo_corporacao" VALUES (3,3,1,1);
INSERT INTO "usuario_tipo_corporacao" VALUES (4,4,1,1);
INSERT INTO "usuario_tipo_corporacao" VALUES (5,5,1,1);
INSERT INTO "usuario_tipo_corporacao" VALUES (6,6,2,1);
INSERT INTO "usuario_tipo_corporacao" VALUES (7,7,2,1);
INSERT INTO "usuario_tipo_corporacao" VALUES (8,8,2,1);
INSERT INTO "usuario_tipo_corporacao" VALUES (9,9,2,1);
INSERT INTO "usuario_tipo_corporacao" VALUES (10,10,1,1);
INSERT INTO "usuario_tipo_corporacao" VALUES (15,12,2,1);
INSERT INTO "usuario_tipo_corporacao" VALUES (16,13,2,1);
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "veiculo" (
  "id" int(11) NOT NULL AUTO_INCREMENT,
  "placa" varchar(20) NOT NULL,
  "modelo_id_INT" int(11) DEFAULT NULL,
  "corporacao_id_INT" int(11) NOT NULL,
  PRIMARY KEY ("id"),
  UNIQUE KEY "placa" ("placa","corporacao_id_INT"),
  KEY "veiculo_FK_363189697" ("corporacao_id_INT"),
  KEY "veiculo_FK_481506348" ("modelo_id_INT"),
  CONSTRAINT "veiculo_FK_363189697" FOREIGN KEY ("corporacao_id_INT") REFERENCES "corporacao" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "veiculo_FK_481506348" FOREIGN KEY ("modelo_id_INT") REFERENCES "modelo" ("id") ON DELETE SET NULL ON UPDATE SET NULL
);
/*!40101 SET character_set_client = @saved_cs_client */;
INSERT INTO "veiculo" VALUES (1,'FED-8575',4,1);
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "veiculo_usuario" (
  "id" int(11) NOT NULL AUTO_INCREMENT,
  "usuario_id_INT" int(11) NOT NULL,
  "data_DATE" date DEFAULT NULL,
  "hora_TIME" time DEFAULT NULL,
  "veiculo_id_INT" int(11) NOT NULL,
  "is_ativo_BOOLEAN" int(1) DEFAULT NULL,
  "corporacao_id_INT" int(11) NOT NULL,
  PRIMARY KEY ("id"),
  UNIQUE KEY "usuario_id_INT" ("usuario_id_INT","veiculo_id_INT","corporacao_id_INT"),
  KEY "veiculo_usuario_FK_266174316" ("veiculo_id_INT"),
  KEY "veiculo_usuario_FK_358489990" ("corporacao_id_INT"),
  CONSTRAINT "veiculo_usuario_FK_100769043" FOREIGN KEY ("usuario_id_INT") REFERENCES "usuario" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "veiculo_usuario_FK_20477294" FOREIGN KEY ("usuario_id_INT") REFERENCES "usuario" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "veiculo_usuario_FK_231353759" FOREIGN KEY ("usuario_id_INT") REFERENCES "usuario" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "veiculo_usuario_FK_266174316" FOREIGN KEY ("veiculo_id_INT") REFERENCES "veiculo" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "veiculo_usuario_FK_312896728" FOREIGN KEY ("usuario_id_INT") REFERENCES "usuario" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "veiculo_usuario_FK_358489990" FOREIGN KEY ("corporacao_id_INT") REFERENCES "corporacao" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "veiculo_usuario_FK_373809814" FOREIGN KEY ("usuario_id_INT") REFERENCES "usuario" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "veiculo_usuario_FK_437774658" FOREIGN KEY ("usuario_id_INT") REFERENCES "usuario" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "veiculo_usuario_FK_666107178" FOREIGN KEY ("usuario_id_INT") REFERENCES "usuario" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "veiculo_usuario_FK_701263428" FOREIGN KEY ("usuario_id_INT") REFERENCES "usuario" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "veiculo_usuario_FK_946105957" FOREIGN KEY ("usuario_id_INT") REFERENCES "usuario" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "versao" (
  "id" int(11) NOT NULL,
  "versao_INT" int(5) DEFAULT NULL,
  PRIMARY KEY ("id")
);
/*!40101 SET character_set_client = @saved_cs_client */;
INSERT INTO "versao" VALUES (1,1);
