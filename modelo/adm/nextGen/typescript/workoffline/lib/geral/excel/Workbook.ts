import * as XLSX from 'xlsx';

import { Sheet, Cell } from 'lib/geral';

export class Workbook
{
    public sheets: Array<Sheet>;

    public nomeArquivoWorkbook: string;
    public conteudoWorkbook :any;

    private celulaInicial :Cell;        
    public constructor(nomeArquivoExportacao :string)
    {
        this.nomeArquivoWorkbook = nomeArquivoExportacao;
        this.celulaInicial = new Cell(1, 1);
        this.sheets = new Array<Sheet>();
    }

    public addSheet(sheet :Sheet) :void
    {
        this.sheets.push(sheet);
    }

    public createNewSheet(nomeSheet? :string) :Sheet
    {
        if(!nomeSheet)
        {
            nomeSheet = `Planilha ${this.sheets.length+1}`;
        }            

        let sheet = new Sheet(nomeSheet);
        this.sheets.push(sheet);

        return sheet;
    }

    private getSheetNamesArray()
    {
        let sheetNames = new Array<string>();
        for(let sheet of this.sheets)
        {
            sheetNames.push(sheet.getSheetName())
        }

        return sheetNames;
    }

    private getSheetsObject()
    {
        let sheetsObject :any = {};
        for(let sheet of this.sheets)
        {
            sheetsObject[sheet.getSheetName()] = sheet.getConteudo();
        }

        return sheetsObject;
    }

    public exportar()
    {
        XLSX.writeFile({
            SheetNames: this.getSheetNamesArray(),
            Sheets: this.getSheetsObject()
        }, this.nomeArquivoWorkbook, { cellStyles: true, cellDates: true, bookType: 'xlsx' });

    }

}