<?php


        /*

        Arquivo gerado através de gerador de código em 12/05/2018 as 14:49:03.
        Para que o arquivo não seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

        Tabela correspondente: ponto
        Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

        */
        


class DAO_Ponto extends Generic_DAO
                        {
                            const NOME_ENTIDADE = "ponto";

                            public static $nomeTabela = null;
                            public static $idSistemaTabela = false;
                            public static $databaseFieldsRelatedAttributes = null;
	                        public static $databaseFieldTypes = null;
	                        public static $databaseFieldNames = null;
	                        public static $listAliasTypes = null;
	                        public static $listAliasRelatedAttributes = null;
	                        
	                        public $id;
public $pessoaId;
public $dataSec;
public $dataOffsec;
public $foto;
public $empresaId;
public $profissaoId;
public $usuarioId;
public $isEntradaBoolean;
public $latitudeInt;
public $longitudeInt;
public $tipoPontoId;
public $corporacaoId;
public $descricao;
public $endereco;

public $labelId;
public $labelPessoaId;
public $labelDataSec;
public $labelDataOffsec;
public $labelFoto;
public $labelEmpresaId;
public $labelProfissaoId;
public $labelUsuarioId;
public $labelIsEntradaBoolean;
public $labelLatitudeInt;
public $labelLongitudeInt;
public $labelTipoPontoId;
public $labelCorporacaoId;
public $labelDescricao;
public $labelEndereco;

public static $arrPK = array("id");

public function __construct($configDAO = null)
        {
            parent::__construct($configDAO);
            static::__setStaticValues($this->database);           
        }

public static function idSistemaTabela($databaseObject = null){
            if(!(static::$idSistemaTabela))
            {
                static::$idSistemaTabela = EXTDAO_Sistema_tabela::getIdDaSistemaTabela(static::nomeTabela(), $databaseObject);
            }
            return static::$idSistemaTabela;
        }

public function getIdSistemaTabela(){
            return static::idSistemaTabela();
        }

public static function nomeTabela(){
            if(is_null(static::$nomeTabela))
            {
                static::$nomeTabela = "ponto";
            }
            return static::$nomeTabela;
        }

public function getNomeTabela(){
            return static::nomeTabela();
        }

public static function __setStaticValues($databaseObject = null)
                   {    
                        static::nomeTabela();
                        
                        static::idSistemaTabela($databaseObject);
                   
                        static::setDatabaseFieldNames();
                        static::setDatabaseFieldTypes();
                        static::setDatabaseFieldsRelatedAttributeNames();
                        
                        static::setListAliasTypes();
                        static::setListAliasRelatedAttributes();
                   }

public function getExpressaoLabel()
                   {
                        return "{id}";
                   }

public static function setDatabaseFieldNames()
        {
            if(is_null(static::$databaseFieldNames))
            {
                static::$databaseFieldNames = new stdClass();
                static::$databaseFieldNames->id = "id";
static::$databaseFieldNames->pessoaId = "pessoa_id_INT";
static::$databaseFieldNames->dataSec = "data_SEC";
static::$databaseFieldNames->dataOffsec = "data_OFFSEC";
static::$databaseFieldNames->foto = "foto";
static::$databaseFieldNames->empresaId = "empresa_id_INT";
static::$databaseFieldNames->profissaoId = "profissao_id_INT";
static::$databaseFieldNames->usuarioId = "usuario_id_INT";
static::$databaseFieldNames->isEntradaBoolean = "is_entrada_BOOLEAN";
static::$databaseFieldNames->latitudeInt = "latitude_INT";
static::$databaseFieldNames->longitudeInt = "longitude_INT";
static::$databaseFieldNames->tipoPontoId = "tipo_ponto_id_INT";
static::$databaseFieldNames->corporacaoId = "corporacao_id_INT";
static::$databaseFieldNames->descricao = "descricao";
static::$databaseFieldNames->endereco = "endereco";

            }
                      
        }

public static function setDatabaseFieldTypes()
        {
            if(is_null(static::$databaseFieldTypes))
            {
                static::$databaseFieldTypes = new stdClass();
                static::$databaseFieldTypes->id = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->pessoaId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->dataSec = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$databaseFieldTypes->dataOffsec = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$databaseFieldTypes->foto = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->empresaId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->profissaoId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->usuarioId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->isEntradaBoolean = static::TIPO_VARIAVEL_BOOLEAN;
static::$databaseFieldTypes->latitudeInt = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->longitudeInt = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->tipoPontoId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->corporacaoId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->descricao = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->endereco = static::TIPO_VARIAVEL_TEXT;

            }
            
        }

public static function setDatabaseFieldsRelatedAttributeNames()
        {
            if(is_null(static::$databaseFieldsRelatedAttributes))
            {
                static::$databaseFieldsRelatedAttributes = new stdClass();
                static::$databaseFieldsRelatedAttributes->id = "id";
static::$databaseFieldsRelatedAttributes->pessoa_id_INT = "pessoaId";
static::$databaseFieldsRelatedAttributes->data_SEC = "dataSec";
static::$databaseFieldsRelatedAttributes->data_OFFSEC = "dataOffsec";
static::$databaseFieldsRelatedAttributes->foto = "foto";
static::$databaseFieldsRelatedAttributes->empresa_id_INT = "empresaId";
static::$databaseFieldsRelatedAttributes->profissao_id_INT = "profissaoId";
static::$databaseFieldsRelatedAttributes->usuario_id_INT = "usuarioId";
static::$databaseFieldsRelatedAttributes->is_entrada_BOOLEAN = "isEntradaBoolean";
static::$databaseFieldsRelatedAttributes->latitude_INT = "latitudeInt";
static::$databaseFieldsRelatedAttributes->longitude_INT = "longitudeInt";
static::$databaseFieldsRelatedAttributes->tipo_ponto_id_INT = "tipoPontoId";
static::$databaseFieldsRelatedAttributes->corporacao_id_INT = "corporacaoId";
static::$databaseFieldsRelatedAttributes->descricao = "descricao";
static::$databaseFieldsRelatedAttributes->endereco = "endereco";

            }           
        }

public function setAllFieldsToHumanFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["pessoa_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["data_SEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["data_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["foto"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["empresa_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["profissao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["usuario_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["is_entrada_BOOLEAN"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["latitude_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["longitude_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["tipo_ponto_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["descricao"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["endereco"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
}

public function setAllFieldsToDatabaseFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["pessoa_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["data_SEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["data_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["foto"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["empresa_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["profissao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["usuario_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["is_entrada_BOOLEAN"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["latitude_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["longitude_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["tipo_ponto_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["descricao"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["endereco"] = static::ESTADO_FORMATACAO_CAMPO_DB;
}

  public function getFkObjPessoa() 
                {
                    if($this->objPessoa == null)
                    {                        
                        $this->objPessoa = new EXTDAO_Pessoa_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getPessoa_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objPessoa->clear();
                    }
                    elseif($this->objPessoa->getId() != $idFK)
                    {
                        $this->objPessoa->select($idFK);
                    }
                    return $this->objPessoa;
                }
  public function getFkObjEmpresa() 
                {
                    if($this->objEmpresa == null)
                    {                        
                        $this->objEmpresa = new EXTDAO_Empresa_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getEmpresa_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objEmpresa->clear();
                    }
                    elseif($this->objEmpresa->getId() != $idFK)
                    {
                        $this->objEmpresa->select($idFK);
                    }
                    return $this->objEmpresa;
                }
  public function getFkObjProfissao() 
                {
                    if($this->objProfissao == null)
                    {                        
                        $this->objProfissao = new EXTDAO_Profissao_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getProfissao_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objProfissao->clear();
                    }
                    elseif($this->objProfissao->getId() != $idFK)
                    {
                        $this->objProfissao->select($idFK);
                    }
                    return $this->objProfissao;
                }
  public function getFkObjUsuario() 
                {
                    if($this->objUsuario == null)
                    {                        
                        $this->objUsuario = new EXTDAO_Usuario_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getUsuario_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objUsuario->clear();
                    }
                    elseif($this->objUsuario->getId() != $idFK)
                    {
                        $this->objUsuario->select($idFK);
                    }
                    return $this->objUsuario;
                }
  public function getFkObjTipo_ponto() 
                {
                    if($this->objTipo_ponto == null)
                    {                        
                        $this->objTipo_ponto = new EXTDAO_Tipo_ponto_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getTipo_ponto_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objTipo_ponto->clear();
                    }
                    elseif($this->objTipo_ponto->getId() != $idFK)
                    {
                        $this->objTipo_ponto->select($idFK);
                    }
                    return $this->objTipo_ponto;
                }
  public function getFkObjCorporacao() 
                {
                    if($this->objCorporacao == null)
                    {                        
                        $this->objCorporacao = new EXTDAO_Corporacao_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getCorporacao_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objCorporacao->clear();
                    }
                    elseif($this->objCorporacao->getId() != $idFK)
                    {
                        $this->objCorporacao->select($idFK);
                    }
                    return $this->objCorporacao;
                }


public function setLabels()
        {$this->labelId = "id";
$this->labelPessoaId = "pessoaidINT";
$this->labelDataSec = "dataSec";
$this->labelDataOffsec = "dataOffsec";
$this->labelFoto = "foto";
$this->labelEmpresaId = "empresaidINT";
$this->labelProfissaoId = "profissaoidINT";
$this->labelUsuarioId = "usuarioidINT";
$this->labelIsEntradaBoolean = "isentradaBOOLEAN";
$this->labelLatitudeInt = "latitudeINT";
$this->labelLongitudeInt = "longitudeINT";
$this->labelTipoPontoId = "tipopontoidINT";
$this->labelCorporacaoId = "corporacaoidINT";
$this->labelDescricao = "";
$this->labelEndereco = "";
}

public function __actionAdd($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                $this->setByObject($parameters);
                $this->setDataCadastroAndOffsetInSecondsFromUser($parameters);
                
                $this->formatarParaSQL();
                $msg = $this->insert(true);
                
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                return new Mensagem(null, I18N::getExpression("Ponto adicionado com sucesso."));
                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionEdit($parameters = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try {

                $this->setByObject($parameters);
                $this->setAllFieldsToHumanFormat();
                
                $this->formatarParaSQL();
                $msg = $this->updateByInstanceUsingParameters($this->id, $parameters, true);
                if($msg != null && $msg->erro())
                {
                     return $msg;
                }
        
                return new Mensagem(null, I18N::getExpression("Ponto editado com sucesso."));
            }
            catch(Exception $ex)
            {                
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionRemove($parameters = null)
        {
            $successulRemovalCount = 0;
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                foreach($parameters as $record)
                {
                    if(is_numeric($record->id))
                    {
                        $msg = $this->delete($record->id, true);
                        if($msg == null || $msg->ok())
                        {
                            $successulRemovalCount++;
                        }
                    }
                }
                
                if($successulRemovalCount > 0)
                {
                    $successMessage = count($successulRemovalCount) > 1 ? I18N::getExpression("{0} Ponto foram removidos com sucesso.", $successulRemovalCount) : I18N::getExpression("Ponto removido com sucesso.");
                    return new Mensagem(null, $successMessage);
                }
                else
                {
                    $errorMessage = count($parameters) > 1 ? I18N::getExpression("Falha ao remover Ponto.") : I18N::getExpression("Falha ao remover Ponto.");
                    return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, $errorMessage);                    
                }
            }
            catch (Exception $ex) 
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }

public function __getRecord($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }
            
            try
            {            
                $returnObject = new stdClass();            
                $whereClause = static::getWhereClauseForFilter($parameters->filterParameters);
                $query = "SELECT  id, pessoa_id_INT, data_SEC, data_OFFSEC, foto, empresa_id_INT, profissao_id_INT, usuario_id_INT, is_entrada_BOOLEAN, latitude_INT, longitude_INT, tipo_ponto_id_INT, corporacao_id_INT, descricao, endereco FROM ponto {$whereClause}";
    
                $msg = $this->database->queryMensagem($query);                
                if ($msg == null)
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Não foi encontrado nenhum registro"));
                    }                    
                    $resultSet = $this->database->result;
                    $resultObject = static::mapResultSetDataToRecord($resultSet);
                    
                    $returnObject->formData = $resultObject;
                    $returnObject->comboBoxesData = null;
                    if($parameters->loadComboBoxesData)
                    {
                        $this->setByObject($resultObject);
                        $returnObject->comboBoxesData =  $this->getFormComboBoxesData();
                    }                    
    
                    
    
                    return new Mensagem_generica($returnObject);
                } 
                else 
                {
                    return $msg;
                }
                
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }


        
        public function __getFilterComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
                
        public function __getFormComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
        
        protected function getFormComboBoxesData()
        {
            $comboBoxesData = new stdClass();
            $listParameters = new stdClass();
            $listParameters->listMappingType = static::LIST_MAPPING_TYPE_COMBOBOX;
            
            
                    
                    $objPessoa = new EXTDAO_Pessoa();
                    $comboBoxesData->fieldPessoaId = $objPessoa->__getList($listParameters);
                    
                    $objEmpresa = new EXTDAO_Empresa();
                    $comboBoxesData->fieldEmpresaId = $objEmpresa->__getList($listParameters);
                    
                    $objProfissao = new EXTDAO_Profissao();
                    $comboBoxesData->fieldProfissaoId = $objProfissao->__getList($listParameters);
                    
                    $objUsuario = new EXTDAO_Usuario();
                    $comboBoxesData->fieldUsuarioId = $objUsuario->__getList($listParameters);
                    
                    $objTipoPonto = new EXTDAO_Tipo_ponto();
                    $comboBoxesData->fieldTipoPontoId = $objTipoPonto->__getList($listParameters);

            return $comboBoxesData;
        }
        
        


        
        public static function setListAliasTypes()
        {
            if(is_null(static::$listAliasTypes))
            {
                static::$listAliasTypes = new stdClass();
                static::$listAliasTypes->pessoa__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->empresa__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->profissao__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->usuario__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->tipo_ponto__nome = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->ponto__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->ponto__ind_celular_valido_BOOLEAN = static::TIPO_VARIAVEL_BOOLEAN;
static::$listAliasTypes->ponto__data_SEC = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$listAliasTypes->ponto__data_OFFSEC = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$listAliasTypes->ponto__foto = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->ponto__ind_celular_valido_BOOLEAN = static::TIPO_VARIAVEL_BOOLEAN;
static::$listAliasTypes->ponto__corporacao_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->ponto__cadastro_OFFSEC = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$listAliasTypes->ponto__is_entrada_BOOLEAN = static::TIPO_VARIAVEL_BOOLEAN;
static::$listAliasTypes->ponto__latitude_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->ponto__longitude_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->ponto__nome = static::TIPO_VARIAVEL_TEXT;
            }        
        }
        
        public static function setListAliasRelatedAttributes()
        {
            if(is_null(static::$listAliasRelatedAttributes))
            {
                static::$listAliasRelatedAttributes = new stdClass();
                static::$listAliasRelatedAttributes->pessoa__id = "pessoaId";
static::$listAliasRelatedAttributes->empresa__id = "empresaId";
static::$listAliasRelatedAttributes->profissao__id = "profissaoId";
static::$listAliasRelatedAttributes->usuario__id = "usuarioId";
static::$listAliasRelatedAttributes->tipo_ponto__nome = "tipoPontoNome";
static::$listAliasRelatedAttributes->ponto__id = "id";
static::$listAliasRelatedAttributes->ponto__ind_celular_valido_BOOLEAN = "indCelularValidoBoolean";
static::$listAliasRelatedAttributes->ponto__data_SEC = "dataSec";
static::$listAliasRelatedAttributes->ponto__data_OFFSEC = "dataOffsec";
static::$listAliasRelatedAttributes->ponto__foto = "foto";
static::$listAliasRelatedAttributes->ponto__ind_celular_valido_BOOLEAN = "indCelularValidoBoolean";
static::$listAliasRelatedAttributes->ponto__corporacao_id_INT = "corporacaoId";
static::$listAliasRelatedAttributes->ponto__cadastro_OFFSEC = "cadastroOffsec";
static::$listAliasRelatedAttributes->ponto__is_entrada_BOOLEAN = "isEntradaBoolean";
static::$listAliasRelatedAttributes->ponto__latitude_INT = "latitudeInt";
static::$listAliasRelatedAttributes->ponto__longitude_INT = "longitudeInt";
static::$listAliasRelatedAttributes->ponto__nome = "nome";
            }         
        }

        public function __getList($parameters = null, $idCorporacao = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try
            {
                $filterParameters = new stdClass();
                if(isset($parameters->filterParameters))
                {
                    $filterParameters = $parameters->filterParameters;
                }
                                    
                            
            if(is_null($idCorporacao))
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            ListParameters::addFilterParameterToObject($filterParameters, "corporacaoId", $idCorporacao);           
                
                
                $sortingParameters = null;
                if(isset($parameters->sortingParameters))
                {
                    $sortingParameters = $parameters->sortingParameters;
                }
                
                $paginationParameters = null;
                if(isset($parameters->paginationParameters))
                {
                    $paginationParameters = $parameters->paginationParameters;
                }
                
                $listMappingType = static::getListMappingType($parameters->listMappingType);
                $mainTableAlias = "p";

                $whereClause = static::getWhereClauseForFilter($filterParameters, $mainTableAlias);
                $orderByClause = static::getOrderByClauseForFilter($sortingParameters, $mainTableAlias);
                $limitClause = static::getLimitClauseForFilter($paginationParameters);

                $queryWithoutLimit = "SELECT p.id FROM ponto p {$whereClause}";
                $query = "SELECT p1.id AS pessoa__id, e.id AS empresa__id, p2.id AS profissao__id, u.id AS usuario__id, tp.nome AS tipo_ponto__nome, p.id AS ponto__id, p.ind_celular_valido_BOOLEAN AS ponto__ind_celular_valido_BOOLEAN, p.data_SEC AS ponto__data_SEC, p.data_OFFSEC AS ponto__data_OFFSEC, p.foto AS ponto__foto, p.ind_celular_valido_BOOLEAN AS ponto__ind_celular_valido_BOOLEAN, p.corporacao_id_INT AS ponto__corporacao_id_INT, p.cadastro_OFFSEC AS ponto__cadastro_OFFSEC, p.is_entrada_BOOLEAN AS ponto__is_entrada_BOOLEAN, p.latitude_INT AS ponto__latitude_INT, p.longitude_INT AS ponto__longitude_INT, p.nome AS ponto__nome FROM ponto p LEFT JOIN pessoa p1 ON p1.id = p.pessoa_id_INT LEFT JOIN empresa e ON e.id = p.empresa_id_INT LEFT JOIN profissao p2 ON p2.id = p.profissao_id_INT LEFT JOIN usuario u ON u.id = p.usuario_id_INT LEFT JOIN tipo_ponto tp ON tp.id = p.tipo_ponto_id_INT {$whereClause} {$orderByClause} {$limitClause}";

                $msg = $this->database->queryMensagem($queryWithoutLimit);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                
                $totalNumberOfRecords = $this->database->rows();
                if($totalNumberOfRecords == 0)
                {
                    return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                }                        
                        
                $msg = $this->database->queryMensagem($query);
                
                //operacao realizaca com sucesso
                if ($msg == null) 
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                    }                        
                
                    $resultSet = $this->database->result;

                    $objReturn = new stdClass();
                    $objReturn->dataSet = static::mapResultSetDataToList($resultSet, $listMappingType, static::$listAliasTypes);

                    if(!Helper::isNullOrEmpty($paginationParameters->recordsPerPage) && !Helper::isNullOrEmpty($paginationParameters->currentPage))
                    {
                        $lastPage = ceil($totalNumberOfRecords / $paginationParameters->recordsPerPage);
                        $objReturn->paginationParameters = new PaginationParams($paginationParameters->recordsPerPage, $paginationParameters->currentPage, $lastPage);
                    }
                    
                    return new Mensagem_generica($objReturn);
                } 
                else
                {
                    return $msg;
                }                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }



public function getId()
            {
                return $this->id;
            }

public function getPessoaId()
            {
                return $this->pessoaId;
            }

public function getPessoa_id_INT()
                {
                    return $this->pessoaId;
                }

public function getDataSec()
            {
                return $this->dataSec;
            }

public function getData_SEC()
                {
                    return $this->dataSec;
                }

public function getDataOffsec()
            {
                return $this->dataOffsec;
            }

public function getData_OFFSEC()
                {
                    return $this->dataOffsec;
                }

public function getFoto()
            {
                return $this->foto;
            }

public function getEmpresaId()
            {
                return $this->empresaId;
            }

public function getEmpresa_id_INT()
                {
                    return $this->empresaId;
                }

public function getProfissaoId()
            {
                return $this->profissaoId;
            }

public function getProfissao_id_INT()
                {
                    return $this->profissaoId;
                }

public function getUsuarioId()
            {
                return $this->usuarioId;
            }

public function getUsuario_id_INT()
                {
                    return $this->usuarioId;
                }

public function getIsEntradaBoolean()
            {
                return $this->isEntradaBoolean;
            }

public function getIs_entrada_BOOLEAN()
                {
                    return $this->isEntradaBoolean;
                }

public function getLatitudeInt()
            {
                return $this->latitudeInt;
            }

public function getLatitude_INT()
                {
                    return $this->latitudeInt;
                }

public function getLongitudeInt()
            {
                return $this->longitudeInt;
            }

public function getLongitude_INT()
                {
                    return $this->longitudeInt;
                }

public function getTipoPontoId()
            {
                return $this->tipoPontoId;
            }

public function getTipo_ponto_id_INT()
                {
                    return $this->tipoPontoId;
                }

public function getCorporacaoId()
            {
                return $this->corporacaoId;
            }

public function getCorporacao_id_INT()
                {
                    return $this->corporacaoId;
                }

public function getDescricao()
            {
                return $this->descricao;
            }

public function getEndereco()
            {
                return $this->endereco;
            }



function setId($value)
            {
                $this->id = $value;
            }

function setPessoaId($value)
            {
                $this->pessoaId = $value;
            }

public function setPessoa_id_INT($value)
                { 
                    $this->pessoaId = $value; 
                }

function setDataSec($value)
            {
                $this->dataSec = $value;
            }

public function setData_SEC($value)
                { 
                    $this->dataSec = $value; 
                }

function setDataOffsec($value)
            {
                $this->dataOffsec = $value;
            }

public function setData_OFFSEC($value)
                { 
                    $this->dataOffsec = $value; 
                }

function setFoto($value)
            {
                $this->foto = $value;
            }

function setEmpresaId($value)
            {
                $this->empresaId = $value;
            }

public function setEmpresa_id_INT($value)
                { 
                    $this->empresaId = $value; 
                }

function setProfissaoId($value)
            {
                $this->profissaoId = $value;
            }

public function setProfissao_id_INT($value)
                { 
                    $this->profissaoId = $value; 
                }

function setUsuarioId($value)
            {
                $this->usuarioId = $value;
            }

public function setUsuario_id_INT($value)
                { 
                    $this->usuarioId = $value; 
                }

function setIsEntradaBoolean($value)
            {
                $this->isEntradaBoolean = $value;
            }

public function setIs_entrada_BOOLEAN($value)
                { 
                    $this->isEntradaBoolean = $value; 
                }

function setLatitudeInt($value)
            {
                $this->latitudeInt = $value;
            }

public function setLatitude_INT($value)
                { 
                    $this->latitudeInt = $value; 
                }

function setLongitudeInt($value)
            {
                $this->longitudeInt = $value;
            }

public function setLongitude_INT($value)
                { 
                    $this->longitudeInt = $value; 
                }

function setTipoPontoId($value)
            {
                $this->tipoPontoId = $value;
            }

public function setTipo_ponto_id_INT($value)
                { 
                    $this->tipoPontoId = $value; 
                }

function setCorporacaoId($value)
            {
                $this->corporacaoId = $value;
            }

public function setCorporacao_id_INT($value)
                { 
                    $this->corporacaoId = $value; 
                }

function setDescricao($value)
            {
                $this->descricao = $value;
            }

function setEndereco($value)
            {
                $this->endereco = $value;
            }



public function clear()
        {$this->id = null;
$this->pessoaId = null;
if($this->objPessoa != null) unset($this->objPessoa);
$this->dataSec = null;
$this->dataOffsec = null;
$this->foto = null;
$this->empresaId = null;
if($this->objEmpresa != null) unset($this->objEmpresa);
$this->profissaoId = null;
if($this->objProfissao != null) unset($this->objProfissao);
$this->usuarioId = null;
if($this->objUsuario != null) unset($this->objUsuario);
$this->isEntradaBoolean = null;
$this->latitudeInt = null;
$this->longitudeInt = null;
$this->tipoPontoId = null;
if($this->objTipo_ponto != null) unset($this->objTipo_ponto);
$this->corporacaoId = null;
if($this->objCorporacao != null) unset($this->objCorporacao);
$this->descricao = null;
$this->endereco = null;
if(is_array($this->arrEstadosCampos)){
                        unset($this->arrEstadosCampos);
                     $this->arrEstadosCampos = array();
                    }
                }

public function formatarParaSQL()
        {
if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->id)){
$this->id = $this->formatarIntegerParaComandoSQL($this->id);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->id);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->pessoaId)){
$this->pessoaId = $this->formatarIntegerParaComandoSQL($this->pessoaId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->pessoaId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->dataSec)){
$this->dataSec = $this->formatarIntegerParaComandoSQL($this->dataSec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->dataSec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->dataOffsec)){
$this->dataOffsec = $this->formatarIntegerParaComandoSQL($this->dataOffsec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->dataOffsec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->foto)){
$this->foto = $this->formatarStringParaComandoSQL($this->foto);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->foto);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->empresaId)){
$this->empresaId = $this->formatarIntegerParaComandoSQL($this->empresaId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->empresaId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->profissaoId)){
$this->profissaoId = $this->formatarIntegerParaComandoSQL($this->profissaoId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->profissaoId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->usuarioId)){
$this->usuarioId = $this->formatarIntegerParaComandoSQL($this->usuarioId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->usuarioId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->isEntradaBoolean)){
$this->isEntradaBoolean = $this->formatarBooleanParaComandoSQL($this->isEntradaBoolean);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->isEntradaBoolean);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->latitudeInt)){
$this->latitudeInt = $this->formatarIntegerParaComandoSQL($this->latitudeInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->latitudeInt);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->longitudeInt)){
$this->longitudeInt = $this->formatarIntegerParaComandoSQL($this->longitudeInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->longitudeInt);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->tipoPontoId)){
$this->tipoPontoId = $this->formatarIntegerParaComandoSQL($this->tipoPontoId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->tipoPontoId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->corporacaoId)){
$this->corporacaoId = $this->formatarIntegerParaComandoSQL($this->corporacaoId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->corporacaoId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->descricao)){
$this->descricao = $this->formatarStringParaComandoSQL($this->descricao);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->descricao);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->endereco)){
$this->endereco = $this->formatarStringParaComandoSQL($this->endereco);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->endereco);
}

}

public function formatarParaExibicao()
        {if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->foto)){
$this->foto = $this->formatarStringParaExibicao($this->foto);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->foto);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->isEntradaBoolean)){
$this->isEntradaBoolean = $this->formatarBooleanParaExibicao($this->isEntradaBoolean);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->isEntradaBoolean);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->descricao)){
$this->descricao = $this->formatarStringParaExibicao($this->descricao);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->descricao);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->endereco)){
$this->endereco = $this->formatarStringParaExibicao($this->endereco);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->endereco);
}

}

function select($id, $idCorporacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }   

            $sql = "SELECT id, pessoa_id_INT, data_SEC, data_OFFSEC, foto, empresa_id_INT, profissao_id_INT, usuario_id_INT, is_entrada_BOOLEAN, latitude_INT, longitude_INT, tipo_ponto_id_INT, corporacao_id_INT, descricao, endereco FROM ponto WHERE id = $id AND corporacao_id_INT = $idCorporacao";

            $msg = $this->database->queryMensagem($sql);
            $result = $this->database->result;

            if($msg != null && $msg->erro() )
            { 
            		$this->clear();
            		return $msg;
        	} 
        	else if($this->database->rows() == 0)
        	{
                return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nenhum registro foi encontrado."));
            }
        

            $row = $this->database->fetchArray(MYSQLI_NUM);
            
            		$this->id = $row[0];
		$this->pessoaId = $row[1];
		$this->dataSec = $row[2];
		$this->dataOffsec = $row[3];
		$this->foto = $row[4];
		$this->empresaId = $row[5];
		$this->profissaoId = $row[6];
		$this->usuarioId = $row[7];
		$this->isEntradaBoolean = $row[8];
		$this->latitudeInt = $row[9];
		$this->longitudeInt = $row[10];
		$this->tipoPontoId = $row[11];
		$this->descricao = $row[13];
		$this->endereco = $row[14];

            
        }

public function delete($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (is_null($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }

            $sql = "DELETE FROM ponto WHERE id= $id AND corporacao_id_INT = $idCorporacao";
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                    && $sincronizar
                        && static::idSistemaTabela() != null
                            && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_REMOVE,
                                null,
                                null,
                                $id,
                                $this->database);
            }

            return $msg;
        }

public function insert($sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null, $opcoes = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            
            
            if (is_null($opcoes))
            {
                $opcoes = $this->opcoesDAO;
            }

            if (!is_numeric($this->id))
            {
                $this->id = "null";
                if ($opcoes == null || $opcoes[Database::OPCAO_GERAR_ID] != false) 
                {
                    $this->id = EXTDAO_Sistema_sequencia::gerarId(static::nomeTabela());                    
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }    
                    else if($msg == null)
                    {
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
                else 
                {
                    $this->id = null;
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao));
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }
                    else if($msg == null)
                    {
                        $this->id = $this->database->getLastInsertId();
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
            }
            else 
            {
                $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }                        
                else if($msg == null)
                {
                   $msg = new Mensagem_token();
                   $msg->mValor = $this->id;
                }
            }
            if($msg == null || $msg->ok())
            {                
                if($sincronizar
                    && static::idSistemaTabela() != null
                    && isset($this->id)){
                    
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_INSERT,
                                null,
                                null,
                                $this->id,
                                $this->database); 
                	}
            } 
            else
            {
                $this->id = null;
            }
            
            return $msg;
            
        }

public function getSQLInsert($idCorporacao){ 

return "INSERT INTO ponto (id, pessoa_id_INT, data_SEC, data_OFFSEC, foto, empresa_id_INT, profissao_id_INT, usuario_id_INT, is_entrada_BOOLEAN, latitude_INT, longitude_INT, tipo_ponto_id_INT, corporacao_id_INT, descricao, endereco) VALUES ( $this->id ,  $this->pessoaId ,  $this->dataSec ,  $this->dataOffsec ,  $this->foto ,  $this->empresaId ,  $this->profissaoId ,  $this->usuarioId ,  $this->isEntradaBoolean ,  $this->latitudeInt ,  $this->longitudeInt ,  $this->tipoPontoId ,  $idCorporacao ,  $this->descricao ,  $this->endereco ) "; 

}




        
        public function updateByParameters($id, $objParametros, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao))
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();

            
                
                if (isset($objParametros->pessoaId)) 
                {
                    $arrUpdateFields[] = " pessoa_id_INT = {$objParametros->pessoaId} ";
                }


                
                if (isset($objParametros->dataSec)) 
                {
                    $arrUpdateFields[] = " data_SEC = {$objParametros->dataSec} ";
                }


                
                if (isset($objParametros->dataOffsec)) 
                {
                    $arrUpdateFields[] = " data_OFFSEC = {$objParametros->dataOffsec} ";
                }


                
                if (isset($objParametros->foto)) 
                {
                    $arrUpdateFields[] = " foto = {$objParametros->foto} ";
                }


                
                if (isset($objParametros->empresaId)) 
                {
                    $arrUpdateFields[] = " empresa_id_INT = {$objParametros->empresaId} ";
                }


                
                if (isset($objParametros->profissaoId)) 
                {
                    $arrUpdateFields[] = " profissao_id_INT = {$objParametros->profissaoId} ";
                }


                
                if (isset($objParametros->usuarioId)) 
                {
                    $arrUpdateFields[] = " usuario_id_INT = {$objParametros->usuarioId} ";
                }


                
                if (isset($objParametros->isEntradaBoolean)) 
                {
                    $arrUpdateFields[] = " is_entrada_BOOLEAN = {$objParametros->isEntradaBoolean} ";
                }


                
                if (isset($objParametros->latitudeInt)) 
                {
                    $arrUpdateFields[] = " latitude_INT = {$objParametros->latitudeInt} ";
                }


                
                if (isset($objParametros->longitudeInt)) 
                {
                    $arrUpdateFields[] = " longitude_INT = {$objParametros->longitudeInt} ";
                }


                
                if (isset($objParametros->tipoPontoId)) 
                {
                    $arrUpdateFields[] = " tipo_ponto_id_INT = {$objParametros->tipoPontoId} ";
                }


                
                if (isset($objParametros->descricao)) 
                {
                    $arrUpdateFields[] = " descricao = {$objParametros->descricao} ";
                }


                
                if (isset($objParametros->endereco)) 
                {
                    $arrUpdateFields[] = " endereco = {$objParametros->endereco} ";
                }



            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE ponto SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";

            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null)
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function update($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($this->pessoaId)) 
                {                                      
                    $arrUpdateFields[] = " pessoa_id_INT = {$this->pessoaId} ";
                }


                
                if (isset($this->dataSec)) 
                {                                      
                    $arrUpdateFields[] = " data_SEC = {$this->dataSec} ";
                }


                
                if (isset($this->dataOffsec)) 
                {                                      
                    $arrUpdateFields[] = " data_OFFSEC = {$this->dataOffsec} ";
                }


                
                if (isset($this->foto)) 
                {                                      
                    $arrUpdateFields[] = " foto = {$this->foto} ";
                }


                
                if (isset($this->empresaId)) 
                {                                      
                    $arrUpdateFields[] = " empresa_id_INT = {$this->empresaId} ";
                }


                
                if (isset($this->profissaoId)) 
                {                                      
                    $arrUpdateFields[] = " profissao_id_INT = {$this->profissaoId} ";
                }


                
                if (isset($this->usuarioId)) 
                {                                      
                    $arrUpdateFields[] = " usuario_id_INT = {$this->usuarioId} ";
                }


                
                if (isset($this->isEntradaBoolean)) 
                {                                      
                    $arrUpdateFields[] = " is_entrada_BOOLEAN = {$this->isEntradaBoolean} ";
                }


                
                if (isset($this->latitudeInt)) 
                {                                      
                    $arrUpdateFields[] = " latitude_INT = {$this->latitudeInt} ";
                }


                
                if (isset($this->longitudeInt)) 
                {                                      
                    $arrUpdateFields[] = " longitude_INT = {$this->longitudeInt} ";
                }


                
                if (isset($this->tipoPontoId)) 
                {                                      
                    $arrUpdateFields[] = " tipo_ponto_id_INT = {$this->tipoPontoId} ";
                }


                
                if (isset($this->descricao)) 
                {                                      
                    $arrUpdateFields[] = " descricao = {$this->descricao} ";
                }


                
                if (isset($this->endereco)) 
                {                                      
                    $arrUpdateFields[] = " endereco = {$this->endereco} ";
                }


            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE ponto SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function updateByInstanceUsingParameters($id, $parameters, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($parameters->pessoaId)) 
                {                                      
                    $arrUpdateFields[] = " pessoa_id_INT = {$this->pessoaId} ";
                }
                
                if (isset($parameters->dataSec)) 
                {                                      
                    $arrUpdateFields[] = " data_SEC = {$this->dataSec} ";
                }
                
                if (isset($parameters->dataOffsec)) 
                {                                      
                    $arrUpdateFields[] = " data_OFFSEC = {$this->dataOffsec} ";
                }
                
                if (isset($parameters->foto)) 
                {                                      
                    $arrUpdateFields[] = " foto = {$this->foto} ";
                }
                
                if (isset($parameters->empresaId)) 
                {                                      
                    $arrUpdateFields[] = " empresa_id_INT = {$this->empresaId} ";
                }
                
                if (isset($parameters->profissaoId)) 
                {                                      
                    $arrUpdateFields[] = " profissao_id_INT = {$this->profissaoId} ";
                }
                
                if (isset($parameters->usuarioId)) 
                {                                      
                    $arrUpdateFields[] = " usuario_id_INT = {$this->usuarioId} ";
                }
                
                if (isset($parameters->isEntradaBoolean)) 
                {                                      
                    $arrUpdateFields[] = " is_entrada_BOOLEAN = {$this->isEntradaBoolean} ";
                }
                
                if (isset($parameters->latitudeInt)) 
                {                                      
                    $arrUpdateFields[] = " latitude_INT = {$this->latitudeInt} ";
                }
                
                if (isset($parameters->longitudeInt)) 
                {                                      
                    $arrUpdateFields[] = " longitude_INT = {$this->longitudeInt} ";
                }
                
                if (isset($parameters->tipoPontoId)) 
                {                                      
                    $arrUpdateFields[] = " tipo_ponto_id_INT = {$this->tipoPontoId} ";
                }
                
                if (isset($parameters->descricao)) 
                {                                      
                    $arrUpdateFields[] = " descricao = {$this->descricao} ";
                }
                
                if (isset($parameters->endereco)) 
                {                                      
                    $arrUpdateFields[] = " endereco = {$this->endereco} ";
                }
            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE ponto SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }

}

?>
