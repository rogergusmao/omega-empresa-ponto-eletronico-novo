<?php
/*
 *
 * -------------------------------------------------------
 * NOME DO FORMUL�RIO: rede_empresa
 * DATA DE GERA��O:    21.10.2012
 * ARQUIVO:            rede_empresa.php
 * TABELA MYSQL:       rede_empresa
 * BANCO DE DADOS:     ponto_eletronico
 * -------------------------------------------------------
 *
 */

if (isset($_GET["contador"]))
{

    $numeroRegistroInterno = Helper::GET("contador");
}
if (isset($identificadorRelacionamento) && is_numeric($identificadorRelacionamento))
{

    $objRede_empresa = new EXTDAO_Rede_empresa();
    $objRede_empresa->select($identificadorRelacionamento);

    $objEmpresa = new EXTDAO_Empresa();
    $idEmpresa = $objRede_empresa->getEmpresa_id_INT();

    $objEmpresa->select($idEmpresa);
}
else
{

    $objRede_empresa = new EXTDAO_Rede_empresa();
    $objEmpresa = new EXTDAO_Empresa();
}

$objArgRede_empresa = new Generic_Argument();

$objRede_empresa->formatarParaExibicao();
$objEmpresa->formatarParaExibicao();

?>

<input type="hidden" name="rede_empresa_id_<?= $numeroRegistroInterno ?>"
       id="rede_empresa_id_<?= $numeroRegistroInterno ?>" value="<?= $objRede_empresa->getId(); ?>">

<table class="tabela_form">

    <tr class="tr_form">

        <?
        $objArgRede_empresa = new Generic_Argument();
        $objArgRede_empresa->numeroDoRegistro = "";
        $objArgRede_empresa->label = $objEmpresa->label_nome;
        $objArgRede_empresa->valor = $objEmpresa->getNome();
        $objArgRede_empresa->classeCss = "input_text";
        $objArgRede_empresa->classeCssFocus = "focus_text";
        $objArgRede_empresa->obrigatorio = true;
        $objArgRede_empresa->largura = 400;
        $objArgRede_empresa->nome = "rede_empresa_nome_{$numeroRegistroInterno}";
        $objArgRede_empresa->id = $objArgRede_empresa->nome;
        ?>

        <td class="td_form_label" style="width: 200px;"><?= $objArgRede_empresa->getLabel() ?></td>
        <td class="td_form_campo" colspan="3">

            <?= $objEmpresa->campoTexto($objArgRede_empresa); ?>

        </td>
    </tr>
    <tr class="tr_form">


        <?
        $objArgRede_empresa = new Generic_Argument();
        $objArgRede_empresa->numeroDoRegistro = "";
        $objArgRede_empresa->label = $objEmpresa->label_telefone1;
        $objArgRede_empresa->valor = $objEmpresa->getTelefone1();
        $objArgRede_empresa->classeCss = "input_text";
        $objArgRede_empresa->classeCssFocus = "focus_text";
        $objArgRede_empresa->obrigatorio = false;
        $objArgRede_empresa->largura = 200;
        $objArgRede_empresa->nome = "rede_empresa_telefone1_{$numeroRegistroInterno}";
        $objArgRede_empresa->id = $objArgRede_empresa->nome;
        ?>

        <td class="td_form_label"><?= $objArgRede_empresa->getLabel() ?></td>
        <td class="td_form_campo">

            <?= $objEmpresa->campoTelefone($objArgRede_empresa); ?>

        </td>


        <?
        $objArgRede_empresa = new Generic_Argument();
        $objArgRede_empresa->numeroDoRegistro = "";
        $objArgRede_empresa->label = $objEmpresa->label_telefone2;
        $objArgRede_empresa->valor = $objEmpresa->getTelefone2();
        $objArgRede_empresa->classeCss = "input_text";
        $objArgRede_empresa->classeCssFocus = "focus_text";
        $objArgRede_empresa->obrigatorio = false;
        $objArgRede_empresa->largura = 200;
        $objArgRede_empresa->nome = "rede_empresa_telefone2_{$numeroRegistroInterno}";
        $objArgRede_empresa->id = $objArgRede_empresa->nome;
        ?>

        <td class="td_form_label" style="width: 300px;"><?= $objArgRede_empresa->getLabel() ?></td>
        <td class="td_form_campo">

            <?= $objEmpresa->campoTelefone($objArgRede_empresa); ?>

        </td>
    </tr>
    <tr class="tr_form">


        <?
        $objArgRede_empresa = new Generic_Argument();
        $objArgRede_empresa->numeroDoRegistro = "";
        $objArgRede_empresa->label = $objEmpresa->label_fax;
        $objArgRede_empresa->valor = $objEmpresa->getFax();
        $objArgRede_empresa->classeCss = "input_text";
        $objArgRede_empresa->classeCssFocus = "focus_text";
        $objArgRede_empresa->obrigatorio = false;
        $objArgRede_empresa->largura = 200;
        $objArgRede_empresa->nome = "rede_empresa_fax_{$numeroRegistroInterno}";
        $objArgRede_empresa->id = $objArgRede_empresa->nome;
        ?>

        <td class="td_form_label"><?= $objArgRede_empresa->getLabel() ?></td>
        <td class="td_form_campo">

            <?= $objEmpresa->campoTelefone($objArgRede_empresa); ?>

        </td>


        <?
        $objArgRede_empresa = new Generic_Argument();
        $objArgRede_empresa->numeroDoRegistro = "";
        $objArgRede_empresa->label = $objEmpresa->label_tipo_empresa_id_INT;
        $objArgRede_empresa->valor = $objEmpresa->getTipo_empresa_id_INT();
        $objArgRede_empresa->classeCss = "input_text";
        $objArgRede_empresa->classeCssFocus = "focus_text";
        $objArgRede_empresa->obrigatorio = false;
        $objArgRede_empresa->largura = 300;
        $objArgRede_empresa->nome = "rede_empresa_tipo_empresa_id_INT_{$numeroRegistroInterno}";
        $objArgRede_empresa->id = $objArgRede_empresa->nome;

        $objEmpresa->addInfoCampos("tipo_empresa_id_INT", $objArgRede_empresa->label, "TEXTO", $objArgRede_empresa->obrigatorio);
        ?>

        <td class="td_form_label"><?= $objArgRede_empresa->getLabel() ?></td>
        <td class="td_form_campo">
            <?= $objEmpresa->objTipo_empresa->getComboBox($objArgRede_empresa); ?>
        </td>

    </tr>
    <tr class="tr_form">


        <?
        $objArgRede_empresa = new Generic_Argument();
        $objArgRede_empresa->numeroDoRegistro = "";
        $objArgRede_empresa->label = $objEmpresa->label_email;
        $objArgRede_empresa->valor = $objEmpresa->getEmail();
        $objArgRede_empresa->classeCss = "input_text";
        $objArgRede_empresa->classeCssFocus = "focus_text";
        $objArgRede_empresa->obrigatorio = false;
        $objArgRede_empresa->largura = 300;
        $objArgRede_empresa->nome = "rede_empresa_email_{$numeroRegistroInterno}";
        $objArgRede_empresa->id = $objArgRede_empresa->nome;
        ?>

        <td class="td_form_label"><?= $objArgRede_empresa->getLabel() ?></td>
        <td class="td_form_campo" colspan="3">

            <?= $objEmpresa->campoEmail($objArgRede_empresa); ?>

        </td>

    </tr>
    <tr class="tr_form">


        <?
        $objArgRede_empresa = new Generic_Argument();
        $objArgRede_empresa->numeroDoRegistro = "";
        $objArgRede_empresa->label = $objEmpresa->label_logradouro;
        $objArgRede_empresa->valor = $objEmpresa->getLogradouro();
        $objArgRede_empresa->classeCss = "input_text";
        $objArgRede_empresa->classeCssFocus = "focus_text";
        $objArgRede_empresa->obrigatorio = true;
        $objArgRede_empresa->largura = 400;
        $objArgRede_empresa->nome = "rede_empresa_logradouro_{$numeroRegistroInterno}";
        $objArgRede_empresa->id = $objArgRede_empresa->nome;
        ?>

        <td class="td_form_label"><?= $objArgRede_empresa->getLabel() ?></td>
        <td class="td_form_campo" colspan="3">

            <?= $objEmpresa->campoTexto($objArgRede_empresa); ?>

        </td>
    <tr class="tr_form">


        <?
        $objArgRede_empresa = new Generic_Argument();
        $objArgRede_empresa->numeroDoRegistro = "";
        $objArgRede_empresa->label = $objEmpresa->label_numero;
        $objArgRede_empresa->valor = $objEmpresa->getNumero();
        $objArgRede_empresa->classeCss = "input_text";
        $objArgRede_empresa->classeCssFocus = "focus_text";
        $objArgRede_empresa->obrigatorio = true;
        $objArgRede_empresa->largura = 200;
        $objArgRede_empresa->nome = "rede_empresa_numero_{$numeroRegistroInterno}";
        $objArgRede_empresa->id = $objArgRede_empresa->nome;
        ?>

        <td class="td_form_label"><?= $objArgRede_empresa->getLabel() ?></td>
        <td class="td_form_campo">

            <?= $objEmpresa->campoTexto($objArgRede_empresa); ?>

        </td>

        <?
        $objArgRede_empresa = new Generic_Argument();
        $objArgRede_empresa->numeroDoRegistro = "";
        $objArgRede_empresa->label = $objEmpresa->label_complemento;
        $objArgRede_empresa->valor = $objEmpresa->getComplemento();
        $objArgRede_empresa->classeCss = "input_text";
        $objArgRede_empresa->classeCssFocus = "focus_text";
        $objArgRede_empresa->obrigatorio = false;
        $objArgRede_empresa->largura = 200;
        $objArgRede_empresa->nome = "rede_empresa_complemento_{$numeroRegistroInterno}";
        $objArgRede_empresa->id = $objArgRede_empresa->nome;
        ?>

        <td class="td_form_label"><?= $objArgRede_empresa->getLabel() ?></td>
        <td class="td_form_campo">

            <?= $objEmpresa->campoTexto($objArgRede_empresa); ?>

        </td>
    </tr>
    <tr class="tr_form">

        <?

        $objArgRede_empresa = new Generic_Argument();
        $objArgRede_empresa->numeroDoRegistro = "";
        $objArgRede_empresa->label = $objEmpresa->label_cidade_id_INT;
        $objArgRede_empresa->valor = $objEmpresa->getCidade_id_INT();
        $objArgRede_empresa->classeCss = "input_text";
        $objArgRede_empresa->classeCssFocus = "focus_text";
        $objArgRede_empresa->obrigatorio = true;
        $objArgRede_empresa->largura = 200;
        $objArgRede_empresa->carregarFilhosAutomaticamente = true;
        $objArgRede_empresa->nome = "rede_empresa_cidade_id_INT_{$numeroRegistroInterno}";
        $objArgRede_empresa->id = $objArgRede_empresa->nome;

        $objAjax = new Ajax();

        $objArgRede_empresa->arrQueriesFilhos[] = "SELECT id, nome FROM bairro WHERE cidade_id_INT=? ORDER BY nome ASC";
        $objArgRede_empresa->query1 = "SELECT c.id, CONCAT(c.nome, ' - ', uf.sigla) FROM cidade AS c, uf AS uf WHERE c.uf_id_INT=uf.id";
        $arrFilhos = array("rede_empresa_bairro_id_INT_{$numeroRegistroInterno}");

        if ($objEmpresa->getBairro_id_INT())
        {

            $valoresFilhos = array($objEmpresa->getBairro_id_INT());
        }
        else
        {

            $valoresFilhos = false;
        }

        ?>

        <td class="td_form_label"><?= $objArgRede_empresa->getLabel(); ?></td>
        <td class="td_form_campo">

            <?= $objAjax->getComboBoxAjax($objArgRede_empresa, $arrFilhos, $valoresFilhos); ?>

            <a class="botoes_form" style="text-decoration: none;" target="_blank"
               href="index.php?tipo=forms&page=cidade&next_action=recarregarListasDeCidades&contador=<?= $numeroRegistroInterno ?>">Adicionar
                Cidade</a>

        </td>

        <?

        $objArgRede_empresa = new Generic_Argument();
        $objArgRede_empresa->numeroDoRegistro = "";
        $objArgRede_empresa->label = $objEmpresa->label_bairro_id_INT;
        $objArgRede_empresa->valor = $objEmpresa->getBairro_id_INT();
        $objArgRede_empresa->classeCss = "input_text";
        $objArgRede_empresa->classeCssFocus = "focus_text";
        $objArgRede_empresa->obrigatorio = false;
        $objArgRede_empresa->largura = 200;
        $objArgRede_empresa->funcaoGetOptions = "getOptionsEmBranco";
        $objArgRede_empresa->nome = "rede_empresa_bairro_id_INT_{$numeroRegistroInterno}";
        $objArgRede_empresa->id = $objArgRede_empresa->nome;

        ?>

        <td class="td_form_label"><?= $objArgRede_empresa->getLabel(); ?></td>
        <td class="td_form_campo">

            <?= $objEmpresa->getComboBox($objArgRede_empresa); ?>

            <a class="botoes_form" style="text-decoration: none;" target="_blank"
               href="index.php?tipo=forms&page=bairro&next_action=recarregarListasDeBairros&contador=<?= $numeroRegistroInterno ?>">Adicionar
                Bairro</a>

        </td>

    </tr>
    <tr>
        <td colspan="4" class="td_botao_remover_da_lista">
            <input class="botoes_form" type="button" value="Remover Empresa"
                   onclick="javascript:removerDivAjaxEmLista(this);">
        </td>
    </tr>

</table>
<br/>

