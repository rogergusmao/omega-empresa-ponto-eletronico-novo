"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var React = require("react");
var react_bootstrap_1 = require("react-bootstrap");
var uiComponente_1 = require("lib/uiComponente");
var TotalizadorDeHoras = (function (_super) {
    __extends(TotalizadorDeHoras, _super);
    function TotalizadorDeHoras(props, reactContext) {
        var _this = _super.call(this, props, reactContext) || this;
        _this.dadosTotalizador = new Array();
        return _this;
    }
    TotalizadorDeHoras.prototype.fixLayout = function () {
        var maxHeight = uiComponente_1.DomContainer.getMaxAllowedHeight();
    };
    TotalizadorDeHoras.prototype.setData = function (pessoas) {
        this.dadosTotalizador = pessoas;
        this.setState({ dadosAtualizados: true });
    };
    TotalizadorDeHoras.prototype.render = function () {
        if (this.dadosTotalizador.length > 0) {
            return (<div className="row">
                    <div className="col-sm-12">  
                        {this.dadosTotalizador.map(function (value) { return (<div key={value.nomePessoa} className="box-content box-statistic">
                                <h3 className="title" style={{ color: value.corBackground.getHexValue() }}>{value.getTempoTotalFormatado()}</h3>
                                <small>{value.nomePessoa}</small>
                                <div className="icon-user align-right" style={{ color: value.corBackground.getHexValue() }}></div>
                            </div>); })}  
                    </div>
                </div>);
        }
        else {
            return (<react_bootstrap_1.Alert bsStyle="warning" className="mb-0">
                        <strong></strong> Nenhum ponto encontrado no período
                    </react_bootstrap_1.Alert>);
        }
    };
    return TotalizadorDeHoras;
}(uiComponente_1.ReactComponent));
exports.TotalizadorDeHoras = TotalizadorDeHoras;
