<?php

$idUsuario = Helper::POSTGET("id_usuario");
$crypt = new Crypt();
$idUsuarioCrypt = $crypt->cryptGet(array("id_usuario"), array($idUsuario));
$objUsuario = new EXTDAO_Usuario();
$objUsuario->select($idUsuario);
$emailAntigo = $objUsuario->getEmail();
$nomeAntigo = $objUsuario->getNome();

?>

<input type="hidden" name="junk" id="junk" value="junk">

<input type="hidden" name="class" id="class" value="EXTDAO_Usuario">
<input type="hidden" name="action" id="action" value="trocarEmailUsuario">

<table width="350" align="center" class="tabela_popup">
    <input type="hidden" id="id_usuario" name="id_usuario" value="<?= $idUsuario; ?>"/>

    <tr>
        <td height="11" align="left" valign="middle">Alterar email</td>
    </tr>
    <tr>
        <td height="11" align="left" valign="middle" class="textos5"
            style="padding-left: 5px; padding-top: 10px; padding-bottom: 10px; color: #333333;">

            Preencha o campo abaixo com o novo e-mail desejado.

        </td>
    </tr>
    <tr class="passo1">
        <td height="22" align="left" valign="middle" style="padding-top: 5px; color: #333333;">


            Email: <input type="text" maxlength="200" id="email" name="email" style="width: 300px;"
                          class="input_text" onfocus="this.className='focus_text';"
                          onblur="this.className='input_text';"/>

            <input type="hidden" name="email_antigo" id="email_antigo" value="<?= $emailAntigo ?>"/>

            <div style="padding: 5px 5px 5px 5px; text-align: right;">
                <a id="verificaExistenciaDoCliente" class="botoes_form" style="cursor: pointer;"
                   onclick="verificaExistenciaDoCliente(this);"/>Verificar email</a>
            </div>
            <div style="padding: 5px 5px 5px 5px; text-align: right; ">
                <a class="botoes_form" id="escolherOutroEmail" style="cursor: pointer; display: none"
                   onclick="escolherOutroEmail(this);"/>Escolher outro email</a>
            </div>
        </td>
    </tr>

    <tr class="passoUsuarioExistente passoUsuarioInexistente" style="display: none;">
        <td height="22" align="left" valign="middle" style="padding-top: 5px; color: #333333;">
            Confirma email: <input type="text" maxlength="200" id="confirmaEmail" name="confirmaEmail"
                                   style="width: 300px;"
                                   class="input_text" onfocus="this.className='focus_text';"
                                   onblur="this.className='input_text';"/>
        </td>
    </tr>

    <tr class="passoUsuarioExistente passoUsuarioInexistente" style="display: none;">
        <td>Nome:</td>
        <td height="22" align="left" valign="middle" style="padding-top: 5px; color: #333333;">
            <input type="text" maxlength="200" id="nome" name="nome" style="width: 300px;"
                   class="input_text" onfocus="this.className='focus_text';"
                   onblur="this.className='input_text';"/>


        </td>
    </tr>

    <tr class="passoUsuarioInexistente" style="display: none;">
        <td height="22" align="left" valign="middle" style="padding-top: 5px; color: #333333;">
            Senha: <input type="text" maxlength="200" id="senha" name="senha" style="width: 300px;"
                          class="input_text" onfocus="this.className='focus_text';"
                          onblur="this.className='input_text';"/>
        </td>
    </tr>
    <tr class="passoUsuarioExistente passoUsuarioInexistente" style="display: none;">
        <td height="22" align="left" valign="middle" style="padding-top: 5px; color: #333333;">
            <div style="padding: 5px 5px 5px 5px; text-align: right;">
                <a class="botoes_form" style="cursor: pointer;"
                   onclick="trocarEmailDoClientePeloSistema(this);"/>Salvar</a>
            </div>
        </td>
    </tr>
</table>

<script type="text/javascript">

    function trocarEmailDoClientePeloSistema(el)
    {
        try
        {
            if (!validarCampos())
            {
                return;
            }
            var $el = $(el);
            var email = $('#email').val();
            var confirmaEmail = $('#confirmaEmail').val();

            if (email !== confirmaEmail)
            {
                bootbox.alert({
                    message: "O email não confere com o anterior."
                });
                return;
            }

            var post = {
                email: $('#email').val(),
                nome: $('#nome').val(),
                id_usuario: $('#id_usuario').val(),
                senha: $('#senha').val()
            };

            adicionarLoading($el);
            $el.css('display', 'none');

            var funcaoASerChamada = function ()
            {
                try
                {
                    var retorno = arguments[0];
                    retorno = JSON.parse(retorno);
                    var codRetorno = retorno.mCodRetorno;

                    if (codRetorno == PROTOCOLO_SISTEMA.SIM)
                    {
                        bootbox.alert({
                            message: "Edição realizada com sucesso"
                        }, function (result)
                        {
                            $('#email1').val(post.email);
                            $('#nome1').val(post.nome);
                        });

                    }
                    else
                    {
                        $el.css('display', 'inline-block');
                        removerLoading($el);
                        bootbox.alert({
                            message: retorno.mMensagem
                        });
                    }
                    return true;

                }
                catch (err)
                {
                    bootbox.alert({
                        message: err.stack
                    });
                    $el.css('display', 'inline-block');
                    return false;
                }
                finally
                {
                    removerLoading($el);
                }
            }
            carregarValorRemotoAsyncPost(
                'webservice.php?class=BO_SICOB&action=trocarEmailDoClientePeloSistema',
                null,
                funcaoASerChamada,
                post);

        }
        catch (err)
        {
            removerLoading($el);
            bootbox.alert({
                message: err.stack
            });
            return false;
        }
        finally
        {

        }
        return false;
    }

    function escolherOutroEmail()
    {

        $('.passoUsuarioExistente').css('display', 'none');
        $('.passoUsuarioInexistente').css('display', 'none');
        $('#email').removeAttr('disabled');
        $('#escolherOutroEmail').css('display', 'none');
        $('#verificaExistenciaDoCliente').css('display', 'inline-block');
    }

    function verificaExistenciaDoCliente(el)
    {
        try
        {
            var $el = $(el);
            var post = {
                email: $('#email').val()
            };
            adicionarLoading($el);
            $el.css('display', 'none');

            var funcaoASerChamada = function ()
            {
                try
                {
                    var retorno = arguments[0];
                    retorno = JSON.parse(retorno);
                    var codRetorno = retorno.mCodRetorno;
                    var msg = retorno.mMensagem;
                    //cliente ja cadastrado na central
                    if (codRetorno === PROTOCOLO_SISTEMA.SIM)
                    {
                        $('#email').attr('disabled', 'disabled');
                        $('.passoUsuarioExistente').css('display', 'block');
                        $('#escolherOutroEmail').css('display', 'inline-block');
                        removerLoading($el);
                    }
                    //cliente ainda nao tem cadastro
                    else
                    {
                        if (codRetorno === PROTOCOLO_SISTEMA.NAO)
                        {
                            $('#email').attr('disabled', 'disabled');
                            $('.passoUsuarioInexistente').css('display', 'block');
                            $('#escolherOutroEmail').css('display', 'inline-block');
                            removerLoading($el);
                        }
                        else
                        {
                            $el.css('display', 'inline-block');
                            removerLoading($el);
                            bootbox.alert({
                                message: msg
                            });
                        }
                    }
                    return true;

                }
                catch (err)
                {
                    removerLoading($el);
                    $el.css('display', 'inline-block');
                    bootbox.alert({
                        message: err.stack
                    });
                    return false;
                }
            }
            carregarValorRemotoAsyncPost(
                'webservice.php?class=BO_SICOB&action=verificaExistenciaDoCliente',
                null,
                funcaoASerChamada,
                post);

        }
        catch (err)
        {
            removerLoading($el);
            $el.css('display', 'inline-block');
            bootbox.alert({
                message: err.stack
            });
            return false;
        }
        finally
        {

        }
        return false;

    }


</script>
