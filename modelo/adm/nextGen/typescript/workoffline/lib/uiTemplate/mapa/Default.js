"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var MapaTemplate_1 = require("./MapaTemplate");
var Default = (function (_super) {
    __extends(Default, _super);
    function Default() {
        return _super.call(this, "Padrão") || this;
    }
    Default.prototype.getStyles = function () {
        return [{
                "featureType": "landscape",
                "stylers": [{ "hue": "#FFBB00" }, { "saturation": 43.400000000000006 }, { "lightness": 37.599999999999994 }, { "gamma": 1 }]
            },
            {
                "featureType": "road.highway",
                "stylers": [{ "hue": "#FFC200" }, { "saturation": -61.8 }, { "lightness": 45.599999999999994 }, { "gamma": 1 }]
            },
            {
                "featureType": "road.arterial",
                "stylers": [{ "hue": "#FF0300" }, { "saturation": -100 }, { "lightness": 51.19999999999999 }, { "gamma": 1 }]
            },
            {
                "featureType": "road.local",
                "stylers": [{ "hue": "#FF0300" }, { "saturation": -100 }, { "lightness": 52 }, { "gamma": 1 }]
            },
            {
                "featureType": "water",
                "stylers": [{ "hue": "#0078FF" }, { "saturation": -13.200000000000003 }, { "lightness": 2.4000000000000057 }, { "gamma": 1 }]
            },
            {
                "featureType": "poi",
                "stylers": [{ "hue": "#FF0300" }, { "saturation": -100 }, { "lightness": 25 }, { "gamma": 1 }]
            }];
    };
    return Default;
}(MapaTemplate_1.MapaTemplate));
exports.Default = Default;
