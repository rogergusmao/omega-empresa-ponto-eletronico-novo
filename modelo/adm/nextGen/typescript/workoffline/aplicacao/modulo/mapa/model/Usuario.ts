import { Model } from "lib/model";
import { Sexo, FonteInformacao, Helper } from 'aplicacao/comum/geral';
import { Usuario as UsuarioRemoteModel } from "aplicacao/modulo/mapa/remoteModel";
import { GeoLocationHelper } from 'lib/geral';

export class Usuario extends Model
{
    public latitude :number;
    public longitude :number;
    public velocidade :number;
    public idUsuario :number;
    public nomeUsuario :string;
    public sexoUsuario :Sexo;
    public dataPosicao :Date;
    public fonteInformacao :FonteInformacao;

    public static parseFromRemoteObject(remoteObject: UsuarioRemoteModel)
    {
        let instance = new Usuario();
        instance.latitude = GeoLocationHelper.getFloatValue(remoteObject.latitude);
        instance.longitude = GeoLocationHelper.getFloatValue(remoteObject.longitude);
        instance.velocidade = Helper.parseInt(remoteObject.velocidade);
        instance.sexoUsuario = Helper.parseInt(remoteObject.idSexoUsuario);
        instance.nomeUsuario = remoteObject.nomeUsuario;
        instance.idUsuario = Helper.parseInt(remoteObject.idUsuario);
        instance.fonteInformacao = Helper.parseInt(remoteObject.fonteInformacao);
        instance.dataPosicao = DateUtil.getDateObject(Helper.parseInt(remoteObject.cadastroSec) + Helper.parseInt(remoteObject.cadastroOffsec));

        return instance;
    }

}