"use strict";
exports.__esModule = true;
var Dimensoes = (function () {
    function Dimensoes(largura, altura) {
        this.largura = (typeof largura == 'string') ? largura : largura + 'px';
        this.altura = (typeof altura == 'string') ? altura : altura + 'px';
    }
    return Dimensoes;
}());
exports.Dimensoes = Dimensoes;
