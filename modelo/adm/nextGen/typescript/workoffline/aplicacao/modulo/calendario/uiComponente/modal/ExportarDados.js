"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
require("react-dates/initialize");
require("react-dates/lib/css/_datepicker.css");
var React = require("react");
var react_bootstrap_1 = require("react-bootstrap");
var react_dates_1 = require("react-dates");
var uiComponente_1 = require("lib/uiComponente");
var ExportarDados = (function (_super) {
    __extends(ExportarDados, _super);
    function ExportarDados(props, reactContext) {
        var _this = _super.call(this, props || {}, reactContext) || this;
        _this.show = _this.show.bind(_this);
        _this.close = _this.close.bind(_this);
        _this.state = { show: false };
        return _this;
    }
    ExportarDados.prototype.close = function () {
        this.setState({ show: false });
    };
    ExportarDados.prototype.show = function () {
        this.setState({ show: true });
    };
    ExportarDados.prototype.atualizarDatasPadrao = function () {
        this.setState({ datasAtualizadas: true });
    };
    ExportarDados.prototype.onExportarDadosClick = function () {
        this.props.onExportarDadosClick(this.dataInicialSelecionada, this.dataFinalSelecionada);
    };
    ExportarDados.prototype.getValidationState = function () {
        return (!this.dataInicialSelecionada || !this.dataFinalSelecionada) ? "block" : "none";
    };
    ExportarDados.prototype.render = function () {
        var _this = this;
        return (<react_bootstrap_1.Modal show={this.state.show} onHide={this.close}>
                <react_bootstrap_1.Modal.Header closeButton>
                    Selecione o período desejado
                </react_bootstrap_1.Modal.Header>
                <react_bootstrap_1.Modal.Body>

                    <react_dates_1.DateRangePicker startDateId="start-date" endDateId="end-date" startDate={this.props.dataInicial} endDate={this.props.dataFinal} onDatesChange={function (_a) {
            var startDate = _a.startDate, endDate = _a.endDate;
            _this.dataInicialSelecionada = startDate;
            _this.dataFinalSelecionada = endDate;
        }} focusedInput={this.state.campoComFoco} onFocusChange={function (input) { return _this.setState({ campoComFoco: input }); }}/>
                    
                    <react_bootstrap_1.HelpBlock style={{ display: this.getValidationState() }}>Preencha o período para exportação dos dados</react_bootstrap_1.HelpBlock>

                </react_bootstrap_1.Modal.Body>
                <react_bootstrap_1.Modal.Footer>
                    <react_bootstrap_1.Button bsStyle="success" onClick={this.onExportarDadosClick.bind(this)}>
                        Exportar
                    </react_bootstrap_1.Button>
                    <react_bootstrap_1.Button bsStyle="danger" onClick={this.close.bind(this)}>
                        Cancelar
                    </react_bootstrap_1.Button>                    
                </react_bootstrap_1.Modal.Footer>
            </react_bootstrap_1.Modal>);
    };
    return ExportarDados;
}(uiComponente_1.ReactComponent));
exports.ExportarDados = ExportarDados;
