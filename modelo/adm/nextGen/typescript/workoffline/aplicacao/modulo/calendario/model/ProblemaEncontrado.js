"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var model_1 = require("lib/model");
var geral_1 = require("aplicacao/comum/geral");
var ProblemaEncontrado = (function (_super) {
    __extends(ProblemaEncontrado, _super);
    function ProblemaEncontrado() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    ProblemaEncontrado.prototype.getTituloProblema = function () {
        switch (this.eventoAssociado.pontoInicial.tipoPonto) {
            case geral_1.TipoPontoEletronico.PONTO_ENTRADA:
                return "Ponto de Entrada sem Saida";
            case geral_1.TipoPontoEletronico.PONTO_SAIDA:
                return "Ponto de Saida sem Entrada";
        }
    };
    ProblemaEncontrado.prototype.getDescricaoProblema = function () {
        return this.eventoAssociado.pontoInicial.dataPonto.toDate().toLocaleString();
    };
    return ProblemaEncontrado;
}(model_1.Model));
exports.ProblemaEncontrado = ProblemaEncontrado;
