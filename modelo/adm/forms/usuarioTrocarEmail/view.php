<?php

/*

Arquivo gerado através de gerador de código em 11/01/2018 as 18:52:36.
Para que o arquivo não seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

Tabela correspondente: pais
Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

*/
?>

<!--Definição da View Angular JS -->
<?

$objForm = new FormEnvironment("mainForm", "formUsuarioTrocarEmailController", "vm");
$objForm->setLayoutOneColumn();

?>
<div class='row' id='content-wrapper' ng-controller="<?= $objForm->getAngularControllerAttributeValue() ?>">

    <?= $objForm->setModalEnvironmentIfApplicable(); ?>
    <?= $objForm->setEditionVariablesIfApplicable(); ?>

    <div class='col-xs-12'>

        <div class='row'>
            <div class='col-sm-12'>
                <div class='page-header'>
                    <h1 class='pull-left'>
                        <i class='icon-edit'></i>
                        <span><?= I18N::getExpression("Alterar e-mail do usuário"); ?></span>
                    </h1>
                    <div class='pull-right close-button-container'>
                        <?= $objForm->renderCloseModalButtonIfExists(); ?>
                    </div>
                </div>
            </div>
        </div>

        <?= $objForm->getFormDebugPanel(); ?>
        <?= $objForm->getModalMessagesPanel(); ?>

        <div class='row'>
            <div class='col-sm-12'>
                <div class='box'>
                    <div class='box-content'>

                        <form name="<?= $objForm->getFormName() ?>" class="form form-horizontal"
                              style="margin-bottom: 0;" novalidate>

                            <?php

                            $objCampo = $objForm->getTextInputInstance("emailAtual", TextInput::TRANSFORMATION_DIRECTIVE_LOWER);
                            $objCampo->setLabel(I18N::getExpression("E-mail atual"));

                            $objCampo->setDefaultValues();
                            $objCampo->setLabelCssClass("col-sm-3 col-md-3 control-label");
                            $objCampo->setContainerCssClass("col-sm-9 col-md-9");

                            ?>

                            <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                 ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                                <?= $objCampo->renderLabel(); ?>
                                <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                    <?= $objCampo->render(); ?>
                                    <?= $objCampo->renderValidationBlock(); ?>

                                </div>


                            </div>

                            <?= $objForm->renderHorizontalLine(); ?>

                            <?php

                            $objCampo = $objForm->getTextInputInstance("emailNovo", TextInput::TRANSFORMATION_DIRECTIVE_LOWER);
                            $objCampo->setLabel(I18N::getExpression("Novo e-mail"));

                            $objCampo->setDefaultValues();
                            $objCampo->setLabelCssClass("col-sm-3 col-md-3 control-label");
                            $objCampo->setContainerCssClass("col-sm-9 col-md-9");
                            $objCampo->setPlaceHolder("Informe o e-mail.");
                            $objCampo->addValidation(new MaxLengthValidation(100, I18N::getExpression("O tamanho máximo do E-mail deve ser de {0} caracteres.")));
                            $objCampo->addValidation(new RequiredValidation(I18N::getExpression("O novo e-mail é de preenchimento obrigatório.")));

                            ?>

                            <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                 ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                                <?= $objCampo->renderLabel(); ?>
                                <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                    <?= $objCampo->render(); ?>
                                    <?= $objCampo->renderValidationBlock(); ?>

                                </div>

                            </div>

                            <?= $objForm->renderHorizontalLine(); ?>

                            <?php
                            $objCampo = $objForm->getPasswordInputInstance("senha");
                            $objCampo->setLabel("Senha");
                            $objCampo->setDefaultValues();
                            $objCampo->setLabelCssClass("col-sm-3 col-md-3 control-label");
                            $objCampo->setContainerCssClass("col-sm-9 col-md-9");
                            $objCampo->setPlaceHolder("Informe a Senha.");
                            $objCampo->addValidation(new MinLengthValidation(6, "O tamanho mínimo da senha deve ser de {0} caracteres."));
                            $objCampo->addValidation(new MaxLengthValidation(40, "O tamanho máximo da senha deve ser de {0} caracteres."));
                            $objCampo->addValidation(new RequiredValidation("A Senha é de preenchimento obrigatório."));
                            ?>

                            <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                 ng-if="vm.exibirCamposDeSenha"
                                 ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                                <?= $objCampo->renderLabel(); ?>
                                <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                    <?= $objCampo->render(); ?>
                                    <?= $objCampo->renderValidationBlock(); ?>

                                </div>

                            </div>

                            <?= $objForm->renderHorizontalLine(); ?>

                            <?php
                            $objCampo = $objForm->getPasswordInputInstance("confirmacaoSenha");
                            $objCampo->setLabel("Confirmação de Senha");
                            $objCampo->setDefaultValues();
                            $objCampo->setLabelCssClass("col-sm-3 col-md-3 control-label");
                            $objCampo->setContainerCssClass("col-sm-9 col-md-9");
                            $objCampo->setPlaceHolder("Informe a Confirmação de Senha.");
                            $objCampo->addValidation(new MinLengthValidation(6, "O tamanho mínimo da confirmação de senha deve ser de {0} caracteres."));
                            $objCampo->addValidation(new MaxLengthValidation(40, "O tamanho máximo da confirmação de senha deve ser de {0} caracteres."));
                            $objCampo->addValidation(new RequiredValidation("A confirmação da senha é de preenchimento obrigatório."));
                            ?>

                            <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                 ng-if="vm.exibirCamposDeSenha"
                                 ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                                <?= $objCampo->renderLabel(); ?>
                                <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                    <?= $objCampo->render(); ?>
                                    <?= $objCampo->renderValidationBlock(); ?>

                                </div>

                            </div>

                            <div class='form-actions form-actions-padding-sm'>
                                <div class='row'>
                                    <div class='col-md-10 action-buttons-bar'>

                                        <?= $objForm->renderSubmitButton(); ?>

                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php

if (!Helper::isAngularModal())
{
    $arrDataServicesRelacionados = null;
    $arrControllersRelacionados = null;

    ?>

    <!--Definição do Controller Angular JS -->
    <script type="text/javascript">

        <?=Helper::incluirConteudoDataServices(true, $arrDataServicesRelacionados); ?>

    </script>

    <?= Helper::incluirControllerAngularJS(true, $arrControllersRelacionados); ?>

<?php } ?>

        
