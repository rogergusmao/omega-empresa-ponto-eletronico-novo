export class SearchableAttribute<T>
{
    private valor: T;
    private isSearchable: boolean;
    private isPrimaryKey: boolean;

    constructor(valor: T = null, isSearchable: boolean = true, isPrimaryKey: boolean = false)
    {
        this.valor = valor;
        this.isSearchable = isSearchable;
        this.isPrimaryKey = isPrimaryKey;
    }

    public getValue(): T
    {
        return this.valor;
    }

    public setValue(valor: T): void
    {
        this.valor = valor;
    }

    public getSearchable(): boolean
    {
        return this.isSearchable;
    }

    public setSearchable(isSearchable: boolean)
    {
        this.isSearchable = isSearchable;
    }

    public getPrimaryKey(): boolean
    {
        return this.isPrimaryKey;
    }

    public setPrimaryKey(isPrimaryKey: boolean)
    {
        this.isPrimaryKey = isPrimaryKey;
    }

}