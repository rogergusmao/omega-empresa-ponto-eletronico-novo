"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var uiComponente_1 = require("lib/uiComponente");
var uiFactory_1 = require("aplicacao/modulo/mapa/uiFactory");
var geral_1 = require("aplicacao/comum/geral");
var Usuario = (function (_super) {
    __extends(Usuario, _super);
    function Usuario(contexto, modelUsuario) {
        var _this = _super.call(this, contexto, modelUsuario.latitude, modelUsuario.longitude, 'usuario') || this;
        _this.modelUsuario = modelUsuario;
        _this.setDefaultLabelObject(modelUsuario.nomeUsuario);
        return _this;
    }
    Usuario.prototype.setDefaultIconeObject = function () {
        switch (this.modelUsuario.sexoUsuario) {
            case geral_1.Sexo.MASCULINO:
            case geral_1.Sexo.NAO_DEFINIDO:
                _super.prototype.setDefaultIconeObject.call(this, this.contexto.getDefaultMarkerFolder() + "map_male.png");
                break;
            case geral_1.Sexo.FEMININO:
                _super.prototype.setDefaultIconeObject.call(this, this.contexto.getDefaultMarkerFolder() + "map_female.png");
                break;
        }
    };
    Usuario.prototype.addDefaultClickListener = function () {
        var _this = this;
        _super.prototype.addClickListener.call(this, function () {
            var janelaModal = uiFactory_1.ModalFactory.getModalDetalhesUsuario(_this.contexto, _this.modelUsuario);
            janelaModal.handleShow();
        });
    };
    return Usuario;
}(uiComponente_1.Marcador));
exports.Usuario = Usuario;
