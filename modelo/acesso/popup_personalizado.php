<?php

include_once '../recursos/php/constants.php';
include_once '../recursos/php/database_config.php';
include_once '../adm/imports/header.php';
include_once '../recursos/php/funcoes.php';
include_once '../adm/imports/instancias.php';
include_once '../adm/imports/lingua.php';

session_name(IDENTIFICADOR_SESSAO_CLIENTE);
session_start();

$objSeguranca = new Seguranca();
$objSeguranca->matarSessao();

?>

<html>
<head>
    <title><?= TITULO_PAGINAS_CLIENTE ?></title>

    <?= Helper::getTagDoFavicon(); ?>


    <?= Javascript::importarTodasAsBibliotecas(); ?>

    <? include('imports/herder.php'); ?>
    <?= Helper::carregarCssFlatty(); ?>

</head>
<body class='contrast-blue login contrast-background'>

<? Javascript::imprimirCorpoDoTooltip(); ?>
<div class='middle-container'>
    <div class='middle-row'>
        <div class='middle-wrapper'>
            <div class='login-container-header'>
                <div class='container'>
                    <div class='row'>
                        <div class='col-sm-12'>
                            <div class='text-center'>
                                <img width="121" height="31" src="css/flatty/assets/images/logo_lg.svg"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class='login-container'>
                <div class='container'>
                    <div class='row'>
                        <div class='col-sm-12'>


                            <?

                            Helper::includePHP(1, "client_area/imports/mensagens_popup.php");
                            $tipo = Helper::GET("tipo");
                            $pagina = Helper::GET("page");
                            if ($tipo && $pagina) {

                                if (Helper::GET("dialog")) {

                                    Helper::imprimirComandoJavascript("$(document).ready(function(){
                    setTimeout(\"autoRedimensionarDialog()\", 1000);
                })");
                                }


                                Helper::includePHP(false, "client_area/" . Helper::GET("tipo") . "/" . Helper::GET("page") . ".php");
                            }

                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class='login-container-footer'>
                <div class='container'>
                    <div class='row'>
                        <div class='col-sm-12'>
                            <div class='text-center'>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?
Helper::includePHP(false, "client_area/imports/mensagens.php");
echo Javascript::importarBibliotecasFlatty();
?>

</body>

</html>
