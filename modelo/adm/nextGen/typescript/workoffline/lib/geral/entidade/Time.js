"use strict";
exports.__esModule = true;
var Time = (function () {
    function Time(horaOrSegundos, minuto, segundo) {
        if (horaOrSegundos && typeof minuto == 'undefined' && typeof segundo == 'undefined') {
            var date = new Date(horaOrSegundos);
            this.hora = date.getHours();
            this.minuto = date.getMinutes();
            this.segundo = date.getSeconds();
        }
        else {
            this.hora = horaOrSegundos;
            this.minuto = minuto;
            this.segundo = segundo;
        }
    }
    return Time;
}());
exports.Time = Time;
