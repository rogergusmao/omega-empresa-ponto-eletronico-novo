<?php

/*

Arquivo gerado através de gerador de código em 05/01/2018 as 19:25:29.
Para que o arquivo não seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

Tabela correspondente: pessoa
Sobrescrita de dados: ---SOBRESCRITA_NAO_AUTORIZADA---

*/

?>

<!--Definição da View Angular JS -->
<?

$objGrid = new GridEnvironment("filterForm", "listPessoaController", "vm");

?>
<?= $objGrid->setModalEnvironmentIfApplicable(); ?>
<div class='row' id='content-wrapper' ng-controller="<?= $objGrid->getAngularControllerAttributeValue() ?>">

    <!--Definição do template do dialog exibido para confirmar remoção -->
    <script type="text/ng-template" id="before-remove-template.html">
        <div class="modal-header">
            <h4 class="modal-title">
                <span class="glyphicon"></span>
                <?= I18N::getExpression("Atenção"); ?>
            </h4>
        </div>
        <div class="modal-body">
            <span class="help-block"
                  ng-show="vm.selectedRecordsForRemoval.length > 1"><?= I18N::getExpression("Deseja remover os {0} registros selecionados?", array("{{vm.selectedRecordsForRemoval.length}}")); ?></span>
            <span class="help-block"
                  ng-show="vm.selectedRecordsForRemoval.length == 1"><?= I18N::getExpression("Deseja remover o registro selecionado?"); ?></span>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-danger"
                    ng-click="vm.closeRemoveMessageDialog()"><?= I18N::getExpression("Não") ?></button>
            <button type="button" class="btn btn-success"
                    ng-click="vm.removeSelectedRecords()"><?= I18N::getExpression("Sim") ?></button>
        </div>
    </script>

    <div class='col-xs-12 omega-list-view'>
        <div class='row'>
            <div class='col-sm-12'>
                <div class='page-header'>
                    <h1 class='pull-left'>
                        <i class='icon-table'></i>
                        <span><?= I18N::getExpression("Gerenciar Pessoas"); ?></span>
                    </h1>
                </div>
            </div>
        </div>

        <?= $objGrid->getModalMessagesPanel(); ?>

        <?

        global $objFilter;
        $objFilter = $objGrid->getFilterEnvironment();
        $objFilter->setLayoutTwoColumns();

        ?>

        <?= Helper::includeSubView("filter_view"); ?>


        <div class="row">

            <div class="col-sm-12"
                 ng-if="<?= $objGrid->getAngularControllerAs() ?>.isFiltered && isNullOrEmptyArray(<?= $objGrid->getAngularControllerAs() ?>.dataSet)">

                <div class="alert alert-info">
                    <h4>
                        <i class="icon-info-sign"></i>
                        <?= I18N::getExpression("Aviso"); ?>
                    </h4>
                    <?= I18N::getExpression("Nenhum registro satisfaz os critérios de busca."); ?>
                </div>

            </div>

            <div class="col-sm-12"
                 ng-if="!isNullOrEmptyArray(<?= $objGrid->getAngularControllerAs() ?>.dataSet) || !isNullOrEmpty(<?= $objGrid->getAngularControllerAs() ?>.listFilter)">

                <div class="box bordered-box blue-border">
                    <div class="box-header blue-background">
                        <div class="title">
                            <div class='icon-list-alt'></div>
                            <?= I18N::getExpression("Lista de Pessoas") ?>
                        </div>
                        <?= $objGrid->renderListActionButtons(); ?>
                    </div>

                    <div class='collapse navbar-collapse navbar-ex1-collapse blue-background omega-in-list-filter-container'>

                        <?= $objGrid->renderHighlightAndFilterPanel(); ?>

                        <?= $objGrid->renderMultipleRecordsActionsPanel(); ?>

                    </div>

                    <div class="box-content box-no-padding omega-list">
                        <div class="responsive-table">
                            <div class="scrollable-area dataTables_wrapper">
                                <table class="table table-bordered table-striped omega-list-table">
                                    <thead>
                                    <tr>
                                        <th class="omega-list-checkbox-th">
                                            <?= $objGrid->renderSelectAllRecordsCheckbox(); ?>
                                        </th>

                                        <?= $objGrid->renderColumnHeader('id', 'Id', 'pessoa', 'id'); ?>
                                        <?= $objGrid->renderColumnHeader('nome', 'Nome', 'pessoa', 'nome'); ?>
                                        <?= $objGrid->renderColumnHeader('tipoDocumentoId', 'Tipo de documento', 'tipo_documento', 'nome'); ?>
                                        <?= $objGrid->renderColumnHeader('numeroDocumento', 'Número do documento', 'pessoa', 'numero_documento'); ?>
                                        <?= $objGrid->renderColumnHeader('sexoId', 'Sexo', 'sexo', 'nome'); ?>
                                        <?= $objGrid->renderColumnHeader('email', 'E-mail', 'pessoa', 'email'); ?>
                                        <?= $objGrid->renderColumnHeader('cidadeId', 'Cidade', 'cidade', 'nome'); ?>


                                        <th class="omega-list-record-options-th">
                                            <?= I18N::getExpression("Opções") ?>
                                        </th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr ng-repeat="record in <?= $objGrid->getAngularControllerAs() ?>.dataSet track by $index">

                                        <?

                                        $arrAtributosCheckbox = array("ng-if" => "!record.temUsuarioAssociado");

                                        ?>

                                        <td class="omega-list-checkbox-td">
                                            <?= $objGrid->renderRecordCheckbox('record', $arrAtributosCheckbox); ?>

                                            <span class="badge badge-important" ng-if="record.temUsuarioAssociado">
                                                <?= I18N::getExpression("Usuário"); ?>
                                            </span>

                                        </td>

                                        <td class="omega-list-record-content-td">
                                            <?= $objGrid->renderColumnContent('record', 'pessoa', 'id'); ?>
                                        </td>
                                        <td class="omega-list-record-content-td">
                                            <?= $objGrid->renderColumnContent('record', 'pessoa', 'nome'); ?>
                                        </td>
                                        <td class="omega-list-record-content-td">
                                            <?= $objGrid->renderColumnContent('record', 'tipo_documento', 'nome'); ?>
                                        </td>
                                        <td class="omega-list-record-content-td">
                                            <?= $objGrid->renderColumnContent('record', 'pessoa', 'numero_documento'); ?>
                                        </td>
                                        <td class="omega-list-record-content-td">
                                            <?= $objGrid->renderColumnContent('record', 'sexo', 'nome'); ?>
                                        </td>
                                        <td class="omega-list-record-content-td">
                                            <?= $objGrid->renderColumnContent('record', 'pessoa', 'email'); ?>
                                        </td>
                                        <td class="omega-list-record-content-td">
                                            <?= $objGrid->renderColumnContent('record', 'cidade', 'nome'); ?>
                                        </td>

                                        <td class="omega-list-record-options-td">
                                            <?= $objGrid->renderEditButton('record'); ?>
                                            <?= $objGrid->renderDeleteButton('record'); ?>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>

                            <div class="row datatables-bottom">

                                <?= $objGrid->renderPositionText(); ?>
                                <?= $objGrid->renderPaginationBar(); ?>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<!--Definição do Controller Angular JS -->
<script type="text/javascript">

    <?=Helper::incluirConteudoDataServices(); ?>

</script>

<?= Helper::incluirControllerAngularJS(); ?>

        
