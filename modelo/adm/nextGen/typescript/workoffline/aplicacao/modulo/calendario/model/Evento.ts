import * as Moment from 'moment';
import * as FullCalendarInputTypes from 'fullcalendar/src/types/input-types';
import { EventObjectInput } from 'fullcalendar';

import { Model } from 'lib/model';
import { Color, MomentHelper } from 'lib/geral';
import { PontoEletronico } from 'aplicacao/modulo/calendario/model';

export namespace Evento
{
    export interface FullCalendarEvent extends EventObjectInput
    {
        idPessoa: number,
        idPontoInicial: number,
        title :string;
        start :string;
        end :string;
        backgroundColor :string;
        textColor :string;
        borderColor: string;
    }

}

export class Evento extends Model
{
    public titulo :string;
    public descricao :string;

    public idPessoa :number;
    public corBackgroundEvento :Color;
    public corForegroudEvento :Color;
    public pontoInicial :PontoEletronico;
    public pontoFinal :PontoEletronico;

    public getEventoFullCalendar() :Evento.FullCalendarEvent
    {
        if(this.pontoFinal != null)
        {
            return {
                id: `${this.pontoInicial.idPonto},${this.pontoFinal.idPonto}`,
                idPessoa: this.idPessoa,
                idPontoInicial: this.pontoInicial.idPonto,
                title: this.titulo,
                start: MomentHelper.formatMoment(this.pontoInicial.dataPonto, MomentHelper.FORMATO_DATAHORA_FULLCALENDAR),
                end: MomentHelper.formatMoment(this.pontoFinal.dataPonto, MomentHelper.FORMATO_DATAHORA_FULLCALENDAR),
                backgroundColor: this.corBackgroundEvento.getHexValue(),
                textColor: this.corForegroudEvento.getHexValue(),
                borderColor: this.corBackgroundEvento.getHexValue(),
                className: 'event-ok',
                eventOverlap: false                                        
            };
        }
        else
        {
            let dataPonto = Moment(this.pontoInicial.dataPonto);

            return {
                id: `${this.pontoInicial.idPonto}`,
                idPessoa: this.idPessoa,
                idPontoInicial: this.pontoInicial.idPonto,
                title: this.titulo,
                start: MomentHelper.formatMoment(this.pontoInicial.dataPonto, MomentHelper.FORMATO_DATAHORA_FULLCALENDAR),
                end: dataPonto.add(1, "hour").format(MomentHelper.FORMATO_DATAHORA_FULLCALENDAR),
                backgroundColor: this.corBackgroundEvento.getHexValue(),
                textColor: this.corForegroudEvento.getHexValue(),
                borderColor: this.corBackgroundEvento.getHexValue(),
                className: 'event-error',
                eventOverlap: false                
            };
        }
    }

}