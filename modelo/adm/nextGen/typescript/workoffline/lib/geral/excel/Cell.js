"use strict";
exports.__esModule = true;
var Cell = (function () {
    function Cell(initialCol, initialRow) {
        this.currentCol = initialCol;
        this.currentRow = initialRow;
    }
    Cell.prototype.moveCol = function (step) {
        if (step === void 0) { step = 1; }
        this.currentCol += step;
        return this;
    };
    Cell.prototype.moveRow = function (step) {
        if (step === void 0) { step = 1; }
        this.currentRow += step;
        return this;
    };
    Cell.prototype.getRow = function () {
        return this.currentRow;
    };
    Cell.prototype.getCol = function () {
        return this.currentCol;
    };
    Cell.prototype.setRow = function (rowIndex) {
        this.currentRow = rowIndex;
    };
    Cell.prototype.setCol = function (colIndex) {
        this.currentCol = colIndex;
    };
    Cell.prototype.getObject = function () {
        return { r: this.currentRow, c: this.currentCol };
    };
    Cell.prototype.getString = function () {
        var initial = "A".charCodeAt(0);
        return String.fromCharCode(initial + this.currentCol) + (this.currentRow + 1).toString();
    };
    Cell.prototype.clone = function () {
        return new Cell(this.currentCol, this.currentRow);
    };
    return Cell;
}());
exports.Cell = Cell;
