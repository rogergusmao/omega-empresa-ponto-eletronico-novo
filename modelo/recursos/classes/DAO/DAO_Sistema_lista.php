<?php


        /*

        Arquivo gerado através de gerador de código em 12/05/2018 as 14:38:17.
        Para que o arquivo não seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

        Tabela correspondente: sistema_lista
        Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

        */
        


class DAO_Sistema_lista extends Generic_DAO
                        {
                            const NOME_ENTIDADE = "sistema_lista";

                            public static $nomeTabela = null;
                            public static $idSistemaTabela = false;
                            public static $databaseFieldsRelatedAttributes = null;
	                        public static $databaseFieldTypes = null;
	                        public static $databaseFieldNames = null;
	                        public static $listAliasTypes = null;
	                        public static $listAliasRelatedAttributes = null;
	                        
	                        public $id;
public $nome;
public $descricao;
public $sistemaTabelaId;
public $sistemaEstadoListaId;
public $ultimaAlteracaoUsuarioId;
public $ultimaAlteracaoSec;
public $ultimaAlteracaoOffsec;
public $criacaoSec;
public $criacaoOffsec;
public $criadorUsuarioId;

public $labelId;
public $labelNome;
public $labelDescricao;
public $labelSistemaTabelaId;
public $labelSistemaEstadoListaId;
public $labelUltimaAlteracaoUsuarioId;
public $labelUltimaAlteracaoSec;
public $labelUltimaAlteracaoOffsec;
public $labelCriacaoSec;
public $labelCriacaoOffsec;
public $labelCriadorUsuarioId;

public static $arrPK = array("id");

public function __construct($configDAO = null)
        {
            parent::__construct($configDAO);
            static::__setStaticValues($this->database);           
        }

public static function idSistemaTabela($databaseObject = null){
            if(!(static::$idSistemaTabela))
            {
                static::$idSistemaTabela = EXTDAO_Sistema_tabela::getIdDaSistemaTabela(static::nomeTabela(), $databaseObject);
            }
            return static::$idSistemaTabela;
        }

public function getIdSistemaTabela(){
            return static::idSistemaTabela();
        }

public static function nomeTabela(){
            if(is_null(static::$nomeTabela))
            {
                static::$nomeTabela = "sistema_lista";
            }
            return static::$nomeTabela;
        }

public function getNomeTabela(){
            return static::nomeTabela();
        }

public static function __setStaticValues($databaseObject = null)
                   {    
                        static::nomeTabela();
                        
                        static::idSistemaTabela($databaseObject);
                   
                        static::setDatabaseFieldNames();
                        static::setDatabaseFieldTypes();
                        static::setDatabaseFieldsRelatedAttributeNames();
                        
                        static::setListAliasTypes();
                        static::setListAliasRelatedAttributes();
                   }

public function getExpressaoLabel()
                   {
                        return "{nome}";
                   }

public static function setDatabaseFieldNames()
        {
            if(is_null(static::$databaseFieldNames))
            {
                static::$databaseFieldNames = new stdClass();
                static::$databaseFieldNames->id = "id";
static::$databaseFieldNames->nome = "nome";
static::$databaseFieldNames->descricao = "descricao";
static::$databaseFieldNames->sistemaTabelaId = "sistema_tabela_id_INT";
static::$databaseFieldNames->sistemaEstadoListaId = "sistema_estado_lista_id_INT";
static::$databaseFieldNames->ultimaAlteracaoUsuarioId = "ultima_alteracao_usuario_id_INT";
static::$databaseFieldNames->ultimaAlteracaoSec = "ultima_alteracao_SEC";
static::$databaseFieldNames->ultimaAlteracaoOffsec = "ultima_alteracao_OFFSEC";
static::$databaseFieldNames->criacaoSec = "criacao_SEC";
static::$databaseFieldNames->criacaoOffsec = "criacao_OFFSEC";
static::$databaseFieldNames->criadorUsuarioId = "criador_usuario_id_INT";

            }
                      
        }

public static function setDatabaseFieldTypes()
        {
            if(is_null(static::$databaseFieldTypes))
            {
                static::$databaseFieldTypes = new stdClass();
                static::$databaseFieldTypes->id = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->nome = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->descricao = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->sistemaTabelaId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->sistemaEstadoListaId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->ultimaAlteracaoUsuarioId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->ultimaAlteracaoSec = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$databaseFieldTypes->ultimaAlteracaoOffsec = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$databaseFieldTypes->criacaoSec = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$databaseFieldTypes->criacaoOffsec = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$databaseFieldTypes->criadorUsuarioId = static::TIPO_VARIAVEL_INTEGER;

            }
            
        }

public static function setDatabaseFieldsRelatedAttributeNames()
        {
            if(is_null(static::$databaseFieldsRelatedAttributes))
            {
                static::$databaseFieldsRelatedAttributes = new stdClass();
                static::$databaseFieldsRelatedAttributes->id = "id";
static::$databaseFieldsRelatedAttributes->nome = "nome";
static::$databaseFieldsRelatedAttributes->descricao = "descricao";
static::$databaseFieldsRelatedAttributes->sistema_tabela_id_INT = "sistemaTabelaId";
static::$databaseFieldsRelatedAttributes->sistema_estado_lista_id_INT = "sistemaEstadoListaId";
static::$databaseFieldsRelatedAttributes->ultima_alteracao_usuario_id_INT = "ultimaAlteracaoUsuarioId";
static::$databaseFieldsRelatedAttributes->ultima_alteracao_SEC = "ultimaAlteracaoSec";
static::$databaseFieldsRelatedAttributes->ultima_alteracao_OFFSEC = "ultimaAlteracaoOffsec";
static::$databaseFieldsRelatedAttributes->criacao_SEC = "criacaoSec";
static::$databaseFieldsRelatedAttributes->criacao_OFFSEC = "criacaoOffsec";
static::$databaseFieldsRelatedAttributes->criador_usuario_id_INT = "criadorUsuarioId";

            }           
        }

public function setAllFieldsToHumanFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["nome"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["descricao"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["sistema_tabela_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["sistema_estado_lista_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["ultima_alteracao_usuario_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["ultima_alteracao_SEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["ultima_alteracao_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["criacao_SEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["criacao_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["criador_usuario_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
}

public function setAllFieldsToDatabaseFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["nome"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["descricao"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["sistema_tabela_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["sistema_estado_lista_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["ultima_alteracao_usuario_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["ultima_alteracao_SEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["ultima_alteracao_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["criacao_SEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["criacao_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["criador_usuario_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
}

  public function getFkObjSistema_tabela() 
                {
                    if($this->objSistema_tabela == null)
                    {                        
                        $this->objSistema_tabela = new EXTDAO_Sistema_tabela_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getSistema_tabela_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objSistema_tabela->clear();
                    }
                    elseif($this->objSistema_tabela->getId() != $idFK)
                    {
                        $this->objSistema_tabela->select($idFK);
                    }
                    return $this->objSistema_tabela;
                }
  public function getFkObjSistema_estado_lista() 
                {
                    if($this->objSistema_estado_lista == null)
                    {                        
                        $this->objSistema_estado_lista = new EXTDAO_Sistema_estado_lista_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getSistema_estado_lista_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objSistema_estado_lista->clear();
                    }
                    elseif($this->objSistema_estado_lista->getId() != $idFK)
                    {
                        $this->objSistema_estado_lista->select($idFK);
                    }
                    return $this->objSistema_estado_lista;
                }
  public function getFkObjUltima_alteracao_usuario() 
                {
                    if($this->objUltima_alteracao_usuario == null)
                    {                        
                        $this->objUltima_alteracao_usuario = new EXTDAO_Ultima_alteracao_usuario_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getUltima_alteracao_usuario_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objUltima_alteracao_usuario->clear();
                    }
                    elseif($this->objUltima_alteracao_usuario->getId() != $idFK)
                    {
                        $this->objUltima_alteracao_usuario->select($idFK);
                    }
                    return $this->objUltima_alteracao_usuario;
                }
  public function getFkObjCriador_usuario() 
                {
                    if($this->objCriador_usuario == null)
                    {                        
                        $this->objCriador_usuario = new EXTDAO_Criador_usuario_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getCriador_usuario_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objCriador_usuario->clear();
                    }
                    elseif($this->objCriador_usuario->getId() != $idFK)
                    {
                        $this->objCriador_usuario->select($idFK);
                    }
                    return $this->objCriador_usuario;
                }


public function setLabels()
        {$this->labelId = "";
$this->labelNome = "";
$this->labelDescricao = "";
$this->labelSistemaTabelaId = "";
$this->labelSistemaEstadoListaId = "";
$this->labelUltimaAlteracaoUsuarioId = "";
$this->labelUltimaAlteracaoSec = "";
$this->labelUltimaAlteracaoOffsec = "";
$this->labelCriacaoSec = "";
$this->labelCriacaoOffsec = "";
$this->labelCriadorUsuarioId = "";
}

public function __actionAdd($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                $this->setByObject($parameters);
                $this->setDataCadastroAndOffsetInSecondsFromUser($parameters);
                
                $this->formatarParaSQL();
                $msg = $this->insert(true);
                
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                return new Mensagem(null, I18N::getExpression("Sistema lista adicionado com sucesso."));
                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionEdit($parameters = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try {

                $this->setByObject($parameters);
                $this->setAllFieldsToHumanFormat();
                
                $this->formatarParaSQL();
                $msg = $this->updateByInstanceUsingParameters($this->id, $parameters, true);
                if($msg != null && $msg->erro())
                {
                     return $msg;
                }
        
                return new Mensagem(null, I18N::getExpression("Sistema lista editado com sucesso."));
            }
            catch(Exception $ex)
            {                
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionRemove($parameters = null)
        {
            $successulRemovalCount = 0;
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                foreach($parameters as $record)
                {
                    if(is_numeric($record->id))
                    {
                        $msg = $this->delete($record->id, true);
                        if($msg == null || $msg->ok())
                        {
                            $successulRemovalCount++;
                        }
                    }
                }
                
                if($successulRemovalCount > 0)
                {
                    $successMessage = count($successulRemovalCount) > 1 ? I18N::getExpression("{0} Sistema lista foram removidos com sucesso.", $successulRemovalCount) : I18N::getExpression("Sistema lista removido com sucesso.");
                    return new Mensagem(null, $successMessage);
                }
                else
                {
                    $errorMessage = count($parameters) > 1 ? I18N::getExpression("Falha ao remover Sistema lista.") : I18N::getExpression("Falha ao remover Sistema lista.");
                    return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, $errorMessage);                    
                }
            }
            catch (Exception $ex) 
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }

public function __getRecord($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }
            
            try
            {            
                $returnObject = new stdClass();            
                $whereClause = static::getWhereClauseForFilter($parameters->filterParameters);
                $query = "SELECT  id, nome, descricao, sistema_tabela_id_INT, sistema_estado_lista_id_INT, ultima_alteracao_usuario_id_INT, ultima_alteracao_SEC, ultima_alteracao_OFFSEC, criacao_SEC, criacao_OFFSEC, criador_usuario_id_INT FROM sistema_lista {$whereClause}";
    
                $msg = $this->database->queryMensagem($query);                
                if ($msg == null)
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Não foi encontrado nenhum registro"));
                    }                    
                    $resultSet = $this->database->result;
                    $resultObject = static::mapResultSetDataToRecord($resultSet);
                    
                    $returnObject->formData = $resultObject;
                    $returnObject->comboBoxesData = null;
                    if($parameters->loadComboBoxesData)
                    {
                        $this->setByObject($resultObject);
                        $returnObject->comboBoxesData =  $this->getFormComboBoxesData();
                    }                    
    
                    
    
                    return new Mensagem_generica($returnObject);
                } 
                else 
                {
                    return $msg;
                }
                
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }


        
        public function __getFilterComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
                
        public function __getFormComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
        
        protected function getFormComboBoxesData()
        {
            $comboBoxesData = new stdClass();
            $listParameters = new stdClass();
            $listParameters->listMappingType = static::LIST_MAPPING_TYPE_COMBOBOX;
            
            
                    
                    $objSistemaTabela = new EXTDAO_Sistema_tabela();
                    $comboBoxesData->fieldSistemaTabelaId = $objSistemaTabela->__getList($listParameters);
                    
                    $objSistemaEstadoLista = new EXTDAO_Sistema_estado_lista();
                    $comboBoxesData->fieldSistemaEstadoListaId = $objSistemaEstadoLista->__getList($listParameters);

            return $comboBoxesData;
        }
        
        


        
        public static function setListAliasTypes()
        {
            if(is_null(static::$listAliasTypes))
            {
                static::$listAliasTypes = new stdClass();
                static::$listAliasTypes->sistema_tabela__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->sistema_estado_lista__nome = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->sistema_lista__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->sistema_lista__nome = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->sistema_lista__descricao = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->sistema_lista__atributos_chave_unica = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->sistema_lista__nome = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->sistema_lista__ultima_alteracao_usuario_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->sistema_lista__ultima_alteracao_SEC = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$listAliasTypes->sistema_lista__ultima_alteracao_OFFSEC = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$listAliasTypes->sistema_lista__criacao_SEC = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$listAliasTypes->sistema_lista__criacao_OFFSEC = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$listAliasTypes->sistema_lista__criador_usuario_id_INT = static::TIPO_VARIAVEL_INTEGER;
            }        
        }
        
        public static function setListAliasRelatedAttributes()
        {
            if(is_null(static::$listAliasRelatedAttributes))
            {
                static::$listAliasRelatedAttributes = new stdClass();
                static::$listAliasRelatedAttributes->sistema_tabela__id = "sistemaTabelaId";
static::$listAliasRelatedAttributes->sistema_estado_lista__nome = "sistemaEstadoListaNome";
static::$listAliasRelatedAttributes->sistema_lista__id = "id";
static::$listAliasRelatedAttributes->sistema_lista__nome = "nome";
static::$listAliasRelatedAttributes->sistema_lista__descricao = "descricao";
static::$listAliasRelatedAttributes->sistema_lista__atributos_chave_unica = "atributosChaveUnica";
static::$listAliasRelatedAttributes->sistema_lista__nome = "nome";
static::$listAliasRelatedAttributes->sistema_lista__ultima_alteracao_usuario_id_INT = "ultimaAlteracaoUsuarioId";
static::$listAliasRelatedAttributes->sistema_lista__ultima_alteracao_SEC = "ultimaAlteracaoSec";
static::$listAliasRelatedAttributes->sistema_lista__ultima_alteracao_OFFSEC = "ultimaAlteracaoOffsec";
static::$listAliasRelatedAttributes->sistema_lista__criacao_SEC = "criacaoSec";
static::$listAliasRelatedAttributes->sistema_lista__criacao_OFFSEC = "criacaoOffsec";
static::$listAliasRelatedAttributes->sistema_lista__criador_usuario_id_INT = "criadorUsuarioId";
            }         
        }

        public function __getList($parameters = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try
            {
                $filterParameters = null;
                if(isset($parameters->filterParameters))
                {
                    $filterParameters = $parameters->filterParameters;
                }
                                    
                    
                
                $sortingParameters = null;
                if(isset($parameters->sortingParameters))
                {
                    $sortingParameters = $parameters->sortingParameters;
                }
                
                $paginationParameters = null;
                if(isset($parameters->paginationParameters))
                {
                    $paginationParameters = $parameters->paginationParameters;
                }
                
                $listMappingType = static::getListMappingType($parameters->listMappingType);
                $mainTableAlias = "sl";

                $whereClause = static::getWhereClauseForFilter($filterParameters, $mainTableAlias);
                $orderByClause = static::getOrderByClauseForFilter($sortingParameters, $mainTableAlias);
                $limitClause = static::getLimitClauseForFilter($paginationParameters);

                $queryWithoutLimit = "SELECT sl.id FROM sistema_lista sl {$whereClause}";
                $query = "SELECT st.id AS sistema_tabela__id, sel.nome AS sistema_estado_lista__nome, sl.id AS sistema_lista__id, sl.nome AS sistema_lista__nome, sl.descricao AS sistema_lista__descricao, sl.atributos_chave_unica AS sistema_lista__atributos_chave_unica, sl.nome AS sistema_lista__nome, sl.ultima_alteracao_usuario_id_INT AS sistema_lista__ultima_alteracao_usuario_id_INT, sl.ultima_alteracao_SEC AS sistema_lista__ultima_alteracao_SEC, sl.ultima_alteracao_OFFSEC AS sistema_lista__ultima_alteracao_OFFSEC, sl.criacao_SEC AS sistema_lista__criacao_SEC, sl.criacao_OFFSEC AS sistema_lista__criacao_OFFSEC, sl.criador_usuario_id_INT AS sistema_lista__criador_usuario_id_INT FROM sistema_lista sl LEFT JOIN sistema_tabela st ON st.id = sl.sistema_tabela_id_INT LEFT JOIN sistema_estado_lista sel ON sel.id = sl.sistema_estado_lista_id_INT {$whereClause} {$orderByClause} {$limitClause}";

                $msg = $this->database->queryMensagem($queryWithoutLimit);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                
                $totalNumberOfRecords = $this->database->rows();
                if($totalNumberOfRecords == 0)
                {
                    return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                }                        
                        
                $msg = $this->database->queryMensagem($query);
                
                //operacao realizaca com sucesso
                if ($msg == null) 
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                    }                        
                
                    $resultSet = $this->database->result;

                    $objReturn = new stdClass();
                    $objReturn->dataSet = static::mapResultSetDataToList($resultSet, $listMappingType, static::$listAliasTypes);

                    if(!Helper::isNullOrEmpty($paginationParameters->recordsPerPage) && !Helper::isNullOrEmpty($paginationParameters->currentPage))
                    {
                        $lastPage = ceil($totalNumberOfRecords / $paginationParameters->recordsPerPage);
                        $objReturn->paginationParameters = new PaginationParams($paginationParameters->recordsPerPage, $paginationParameters->currentPage, $lastPage);
                    }
                    
                    return new Mensagem_generica($objReturn);
                } 
                else
                {
                    return $msg;
                }                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }



public function getId()
            {
                return $this->id;
            }

public function getNome()
            {
                return $this->nome;
            }

public function getDescricao()
            {
                return $this->descricao;
            }

public function getSistemaTabelaId()
            {
                return $this->sistemaTabelaId;
            }

public function getSistema_tabela_id_INT()
                {
                    return $this->sistemaTabelaId;
                }

public function getSistemaEstadoListaId()
            {
                return $this->sistemaEstadoListaId;
            }

public function getSistema_estado_lista_id_INT()
                {
                    return $this->sistemaEstadoListaId;
                }

public function getUltimaAlteracaoUsuarioId()
            {
                return $this->ultimaAlteracaoUsuarioId;
            }

public function getUltima_alteracao_usuario_id_INT()
                {
                    return $this->ultimaAlteracaoUsuarioId;
                }

public function getUltimaAlteracaoSec()
            {
                return $this->ultimaAlteracaoSec;
            }

public function getUltima_alteracao_SEC()
                {
                    return $this->ultimaAlteracaoSec;
                }

public function getUltimaAlteracaoOffsec()
            {
                return $this->ultimaAlteracaoOffsec;
            }

public function getUltima_alteracao_OFFSEC()
                {
                    return $this->ultimaAlteracaoOffsec;
                }

public function getCriacaoSec()
            {
                return $this->criacaoSec;
            }

public function getCriacao_SEC()
                {
                    return $this->criacaoSec;
                }

public function getCriacaoOffsec()
            {
                return $this->criacaoOffsec;
            }

public function getCriacao_OFFSEC()
                {
                    return $this->criacaoOffsec;
                }

public function getCriadorUsuarioId()
            {
                return $this->criadorUsuarioId;
            }

public function getCriador_usuario_id_INT()
                {
                    return $this->criadorUsuarioId;
                }



function setId($value)
            {
                $this->id = $value;
            }

function setNome($value)
            {
                $this->nome = $value;
            }

function setDescricao($value)
            {
                $this->descricao = $value;
            }

function setSistemaTabelaId($value)
            {
                $this->sistemaTabelaId = $value;
            }

public function setSistema_tabela_id_INT($value)
                { 
                    $this->sistemaTabelaId = $value; 
                }

function setSistemaEstadoListaId($value)
            {
                $this->sistemaEstadoListaId = $value;
            }

public function setSistema_estado_lista_id_INT($value)
                { 
                    $this->sistemaEstadoListaId = $value; 
                }

function setUltimaAlteracaoUsuarioId($value)
            {
                $this->ultimaAlteracaoUsuarioId = $value;
            }

public function setUltima_alteracao_usuario_id_INT($value)
                { 
                    $this->ultimaAlteracaoUsuarioId = $value; 
                }

function setUltimaAlteracaoSec($value)
            {
                $this->ultimaAlteracaoSec = $value;
            }

public function setUltima_alteracao_SEC($value)
                { 
                    $this->ultimaAlteracaoSec = $value; 
                }

function setUltimaAlteracaoOffsec($value)
            {
                $this->ultimaAlteracaoOffsec = $value;
            }

public function setUltima_alteracao_OFFSEC($value)
                { 
                    $this->ultimaAlteracaoOffsec = $value; 
                }

function setCriacaoSec($value)
            {
                $this->criacaoSec = $value;
            }

public function setCriacao_SEC($value)
                { 
                    $this->criacaoSec = $value; 
                }

function setCriacaoOffsec($value)
            {
                $this->criacaoOffsec = $value;
            }

public function setCriacao_OFFSEC($value)
                { 
                    $this->criacaoOffsec = $value; 
                }

function setCriadorUsuarioId($value)
            {
                $this->criadorUsuarioId = $value;
            }

public function setCriador_usuario_id_INT($value)
                { 
                    $this->criadorUsuarioId = $value; 
                }



public function clear()
        {$this->id = null;
$this->nome = null;
$this->descricao = null;
$this->sistemaTabelaId = null;
if($this->objSistema_tabela != null) unset($this->objSistema_tabela);
$this->sistemaEstadoListaId = null;
if($this->objSistema_estado_lista != null) unset($this->objSistema_estado_lista);
$this->ultimaAlteracaoUsuarioId = null;
if($this->objUltima_alteracao_usuario != null) unset($this->objUltima_alteracao_usuario);
$this->ultimaAlteracaoSec = null;
$this->ultimaAlteracaoOffsec = null;
$this->criacaoSec = null;
$this->criacaoOffsec = null;
$this->criadorUsuarioId = null;
if($this->objCriador_usuario != null) unset($this->objCriador_usuario);
if(is_array($this->arrEstadosCampos)){
                        unset($this->arrEstadosCampos);
                     $this->arrEstadosCampos = array();
                    }
                }

public function formatarParaSQL()
        {
if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->id)){
$this->id = $this->formatarIntegerParaComandoSQL($this->id);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->id);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->nome)){
$this->nome = $this->formatarStringParaComandoSQL($this->nome);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->nome);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->descricao)){
$this->descricao = $this->formatarStringParaComandoSQL($this->descricao);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->descricao);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->sistemaTabelaId)){
$this->sistemaTabelaId = $this->formatarIntegerParaComandoSQL($this->sistemaTabelaId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->sistemaTabelaId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->sistemaEstadoListaId)){
$this->sistemaEstadoListaId = $this->formatarIntegerParaComandoSQL($this->sistemaEstadoListaId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->sistemaEstadoListaId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->ultimaAlteracaoUsuarioId)){
$this->ultimaAlteracaoUsuarioId = $this->formatarIntegerParaComandoSQL($this->ultimaAlteracaoUsuarioId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->ultimaAlteracaoUsuarioId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->ultimaAlteracaoSec)){
$this->ultimaAlteracaoSec = $this->formatarIntegerParaComandoSQL($this->ultimaAlteracaoSec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->ultimaAlteracaoSec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->ultimaAlteracaoOffsec)){
$this->ultimaAlteracaoOffsec = $this->formatarIntegerParaComandoSQL($this->ultimaAlteracaoOffsec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->ultimaAlteracaoOffsec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->criacaoSec)){
$this->criacaoSec = $this->formatarIntegerParaComandoSQL($this->criacaoSec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->criacaoSec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->criacaoOffsec)){
$this->criacaoOffsec = $this->formatarIntegerParaComandoSQL($this->criacaoOffsec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->criacaoOffsec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->criadorUsuarioId)){
$this->criadorUsuarioId = $this->formatarIntegerParaComandoSQL($this->criadorUsuarioId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->criadorUsuarioId);
}

}

public function formatarParaExibicao()
        {if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->nome)){
$this->nome = $this->formatarStringParaExibicao($this->nome);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->nome);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->descricao)){
$this->descricao = $this->formatarStringParaExibicao($this->descricao);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->descricao);
}

}

function select($id, $idCorporacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }   

            $sql = "SELECT id, nome, descricao, sistema_tabela_id_INT, sistema_estado_lista_id_INT, ultima_alteracao_usuario_id_INT, ultima_alteracao_SEC, ultima_alteracao_OFFSEC, criacao_SEC, criacao_OFFSEC, criador_usuario_id_INT FROM sistema_lista WHERE id = $id";

            $msg = $this->database->queryMensagem($sql);
            $result = $this->database->result;

            if($msg != null && $msg->erro() )
            { 
            		$this->clear();
            		return $msg;
        	} 
        	else if($this->database->rows() == 0)
        	{
                return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nenhum registro foi encontrado."));
            }
        

            $row = $this->database->fetchArray(MYSQLI_NUM);
            
            		$this->id = $row[0];
		$this->nome = $row[1];
		$this->descricao = $row[2];
		$this->sistemaTabelaId = $row[3];
		$this->sistemaEstadoListaId = $row[4];
		$this->ultimaAlteracaoUsuarioId = $row[5];
		$this->ultimaAlteracaoSec = $row[6];
		$this->ultimaAlteracaoOffsec = $row[7];
		$this->criacaoSec = $row[8];
		$this->criacaoOffsec = $row[9];
		$this->criadorUsuarioId = $row[10];

            
        }

public function delete($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (is_null($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }

            $sql = "DELETE FROM sistema_lista WHERE id= $id";
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                    && $sincronizar
                        && static::idSistemaTabela() != null
                            && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_REMOVE,
                                null,
                                null,
                                $id,
                                $this->database);
            }

            return $msg;
        }

public function insert($sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null, $opcoes = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            
            
            if (is_null($opcoes))
            {
                $opcoes = $this->opcoesDAO;
            }

            if (!is_numeric($this->id))
            {
                $this->id = "null";
                if ($opcoes == null || $opcoes[Database::OPCAO_GERAR_ID] != false) 
                {
                    $this->id = EXTDAO_Sistema_sequencia::gerarId(static::nomeTabela());                    
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }    
                    else if($msg == null)
                    {
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
                else 
                {
                    $this->id = null;
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao));
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }
                    else if($msg == null)
                    {
                        $this->id = $this->database->getLastInsertId();
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
            }
            else 
            {
                $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }                        
                else if($msg == null)
                {
                   $msg = new Mensagem_token();
                   $msg->mValor = $this->id;
                }
            }
            if($msg == null || $msg->ok())
            {                
                if($sincronizar
                    && static::idSistemaTabela() != null
                    && isset($this->id)){
                    
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_INSERT,
                                null,
                                null,
                                $this->id,
                                $this->database); 
                	}
            } 
            else
            {
                $this->id = null;
            }
            
            return $msg;
            
        }

public function getSQLInsert($idCorporacao){ 

return "INSERT INTO sistema_lista (id, nome, descricao, sistema_tabela_id_INT, sistema_estado_lista_id_INT, ultima_alteracao_usuario_id_INT, ultima_alteracao_SEC, ultima_alteracao_OFFSEC, criacao_SEC, criacao_OFFSEC, criador_usuario_id_INT) VALUES ( $this->id ,  $this->nome ,  $this->descricao ,  $this->sistemaTabelaId ,  $this->sistemaEstadoListaId ,  $this->ultimaAlteracaoUsuarioId ,  $this->ultimaAlteracaoSec ,  $this->ultimaAlteracaoOffsec ,  $this->criacaoSec ,  $this->criacaoOffsec ,  $this->criadorUsuarioId ) "; 

}




        
        public function updateByParameters($id, $objParametros, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao))
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();

            
                
                if (isset($objParametros->nome)) 
                {
                    $arrUpdateFields[] = " nome = {$objParametros->nome} ";
                }


                
                if (isset($objParametros->descricao)) 
                {
                    $arrUpdateFields[] = " descricao = {$objParametros->descricao} ";
                }


                
                if (isset($objParametros->sistemaTabelaId)) 
                {
                    $arrUpdateFields[] = " sistema_tabela_id_INT = {$objParametros->sistemaTabelaId} ";
                }


                
                if (isset($objParametros->sistemaEstadoListaId)) 
                {
                    $arrUpdateFields[] = " sistema_estado_lista_id_INT = {$objParametros->sistemaEstadoListaId} ";
                }


                
                if (isset($objParametros->ultimaAlteracaoUsuarioId)) 
                {
                    $arrUpdateFields[] = " ultima_alteracao_usuario_id_INT = {$objParametros->ultimaAlteracaoUsuarioId} ";
                }


                
                if (isset($objParametros->ultimaAlteracaoSec)) 
                {
                    $arrUpdateFields[] = " ultima_alteracao_SEC = {$objParametros->ultimaAlteracaoSec} ";
                }


                
                if (isset($objParametros->ultimaAlteracaoOffsec)) 
                {
                    $arrUpdateFields[] = " ultima_alteracao_OFFSEC = {$objParametros->ultimaAlteracaoOffsec} ";
                }


                
                if (isset($objParametros->criacaoSec)) 
                {
                    $arrUpdateFields[] = " criacao_SEC = {$objParametros->criacaoSec} ";
                }


                
                if (isset($objParametros->criacaoOffsec)) 
                {
                    $arrUpdateFields[] = " criacao_OFFSEC = {$objParametros->criacaoOffsec} ";
                }


                
                if (isset($objParametros->criadorUsuarioId)) 
                {
                    $arrUpdateFields[] = " criador_usuario_id_INT = {$objParametros->criadorUsuarioId} ";
                }



            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE sistema_lista SET {$strUpdateFields} WHERE id = {$id}";

            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null)
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function update($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($this->nome)) 
                {                                      
                    $arrUpdateFields[] = " nome = {$this->nome} ";
                }


                
                if (isset($this->descricao)) 
                {                                      
                    $arrUpdateFields[] = " descricao = {$this->descricao} ";
                }


                
                if (isset($this->sistemaTabelaId)) 
                {                                      
                    $arrUpdateFields[] = " sistema_tabela_id_INT = {$this->sistemaTabelaId} ";
                }


                
                if (isset($this->sistemaEstadoListaId)) 
                {                                      
                    $arrUpdateFields[] = " sistema_estado_lista_id_INT = {$this->sistemaEstadoListaId} ";
                }


                
                if (isset($this->ultimaAlteracaoUsuarioId)) 
                {                                      
                    $arrUpdateFields[] = " ultima_alteracao_usuario_id_INT = {$this->ultimaAlteracaoUsuarioId} ";
                }


                
                if (isset($this->ultimaAlteracaoSec)) 
                {                                      
                    $arrUpdateFields[] = " ultima_alteracao_SEC = {$this->ultimaAlteracaoSec} ";
                }


                
                if (isset($this->ultimaAlteracaoOffsec)) 
                {                                      
                    $arrUpdateFields[] = " ultima_alteracao_OFFSEC = {$this->ultimaAlteracaoOffsec} ";
                }


                
                if (isset($this->criacaoSec)) 
                {                                      
                    $arrUpdateFields[] = " criacao_SEC = {$this->criacaoSec} ";
                }


                
                if (isset($this->criacaoOffsec)) 
                {                                      
                    $arrUpdateFields[] = " criacao_OFFSEC = {$this->criacaoOffsec} ";
                }


                
                if (isset($this->criadorUsuarioId)) 
                {                                      
                    $arrUpdateFields[] = " criador_usuario_id_INT = {$this->criadorUsuarioId} ";
                }


            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE sistema_lista SET {$strUpdateFields} WHERE id = {$id}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function updateByInstanceUsingParameters($id, $parameters, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($parameters->nome)) 
                {                                      
                    $arrUpdateFields[] = " nome = {$this->nome} ";
                }
                
                if (isset($parameters->descricao)) 
                {                                      
                    $arrUpdateFields[] = " descricao = {$this->descricao} ";
                }
                
                if (isset($parameters->sistemaTabelaId)) 
                {                                      
                    $arrUpdateFields[] = " sistema_tabela_id_INT = {$this->sistemaTabelaId} ";
                }
                
                if (isset($parameters->sistemaEstadoListaId)) 
                {                                      
                    $arrUpdateFields[] = " sistema_estado_lista_id_INT = {$this->sistemaEstadoListaId} ";
                }
                
                if (isset($parameters->ultimaAlteracaoUsuarioId)) 
                {                                      
                    $arrUpdateFields[] = " ultima_alteracao_usuario_id_INT = {$this->ultimaAlteracaoUsuarioId} ";
                }
                
                if (isset($parameters->ultimaAlteracaoSec)) 
                {                                      
                    $arrUpdateFields[] = " ultima_alteracao_SEC = {$this->ultimaAlteracaoSec} ";
                }
                
                if (isset($parameters->ultimaAlteracaoOffsec)) 
                {                                      
                    $arrUpdateFields[] = " ultima_alteracao_OFFSEC = {$this->ultimaAlteracaoOffsec} ";
                }
                
                if (isset($parameters->criacaoSec)) 
                {                                      
                    $arrUpdateFields[] = " criacao_SEC = {$this->criacaoSec} ";
                }
                
                if (isset($parameters->criacaoOffsec)) 
                {                                      
                    $arrUpdateFields[] = " criacao_OFFSEC = {$this->criacaoOffsec} ";
                }
                
                if (isset($parameters->criadorUsuarioId)) 
                {                                      
                    $arrUpdateFields[] = " criador_usuario_id_INT = {$this->criadorUsuarioId} ";
                }
            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE sistema_lista SET {$strUpdateFields} WHERE id = {$id}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }

}

?>
