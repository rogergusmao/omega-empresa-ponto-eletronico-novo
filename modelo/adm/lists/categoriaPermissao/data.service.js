/*

 Arquivo gerado atrav�s de gerador de c�digo em 07/01/2018 as 15:54:09.
 Para que o arquivo n�o seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

 Tabela correspondente: categoria_permissao
 Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

 */

returnObject.getAllComboBoxesDataForCategoriaPermissaoFilter = function ()
{
    return $http({
        url: baseURLAdm + "/webservice.php?class=EXTDAO_Categoria_permissao&jsonOutput=true&action=__getFilterComboBoxesData",
        method: "POST",
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    });

};

returnObject.getCategoriaPermissaoList = function (listParams)
{
    listParams.listMappingType = Constants.ListMappingType.GRID;
    return $http.post(baseURLAdm + "/webservice.php?class=EXTDAO_Categoria_permissao&action=__getList", listParams);
};

returnObject.removeRecords = function (recordsPKs)
{
    return $http.post(baseURLAdm + "/webservice.php?class=EXTDAO_Categoria_permissao&action=remove", recordsPKs);
};



