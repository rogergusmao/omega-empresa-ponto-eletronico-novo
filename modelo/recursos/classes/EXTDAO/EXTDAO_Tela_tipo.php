<?php

/*

Arquivo gerado através de gerador de código em 06/09/2017 as 21:06:36.
Para que o arquivo não seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

Tabela correspondente: tela_tipo
Sobrescrita de dados: ---SOBRESCRITA_NAO_AUTORIZADA---

*/
?>

<?php

class EXTDAO_Tela_tipo extends DAO_Tela_tipo
{
    public function __construct($configDAO)
    {
        parent::__construct($configDAO);
        $this->nomeClasse = "EXTDAO_Tela_tipo";
    }

    public function factory()
    {
        return new EXTDAO_Tela_tipo();
    }
}

?>
