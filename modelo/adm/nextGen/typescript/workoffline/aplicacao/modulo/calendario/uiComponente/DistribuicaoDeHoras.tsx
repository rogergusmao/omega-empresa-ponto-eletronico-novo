import * as FullCalendar from 'fullcalendar';
import * as FullCalendarInputTypes from 'fullcalendar/src/types/input-types';
import * as React from 'react';

import { PieChart, Pie, Cell, Label, Legend } from 'recharts';
import { Alert } from 'react-bootstrap';

import { ReactProps } from 'lib/geral';
import { DomContainer, ReactComponent } from 'lib/uiComponente';
import { Pessoa } from 'aplicacao/modulo/calendario/model';

export class DistribuicaoDeHoras extends ReactComponent<DistribuicaoDeHoras.DistribuicaoDeHorasProps, DistribuicaoDeHoras.DistribuicaoDeHorasState>
{
    private dadosGrafico :Array<DistribuicaoDeHoras.DadosGrafico>;

    public constructor(props: DistribuicaoDeHoras.DistribuicaoDeHorasProps, reactContext: any)
    { 
        super(props, reactContext);  
        this.dadosGrafico = new Array<DistribuicaoDeHoras.DadosGrafico>();
    }

    public fixLayout()
    {
        var maxHeight: number = DomContainer.getMaxAllowedHeight();            
    }

    public setData(pessoas :Array<Pessoa>)
    {            
        this.dadosGrafico = pessoas.map(pessoa => {

            let dados :DistribuicaoDeHoras.DadosGrafico = { 
                legenda: `${pessoa.nomePessoa} (${pessoa.getTempoTotalFormatado()})`,
                valor: pessoa.getTempoTotal().asHours(),
                cor: pessoa.corBackground.getHexValue()
            };

            return dados;
        });

        this.setState({ dataRefresh: true });
    }

    public render()
    {                                              
        let altura = this.props.altura ? this.props.altura : this.props.largura;

        if(this.dadosGrafico.length > 0)
        {
            return (                     
                <PieChart width={this.props.largura} height={altura}>
                    <Legend verticalAlign="top" align="left" height={36}/>
                    <Pie data={this.dadosGrafico} dataKey="valor" nameKey="legenda" cx="50%" cy="50%" innerRadius={0} isAnimationActive={false}>
                        {
                            this.dadosGrafico.map((entry, index) => <Cell key={`cell-${index}`} fill={entry.cor}/>)
                        }
                    </Pie>
                </PieChart>    
            );
        }
        else
        {
            return (<Alert bsStyle="warning" className="mb-0">
                        <strong></strong> Nenhum ponto encontrado no período
                    </Alert>);
        }

    }

}

export namespace DistribuicaoDeHoras
{
    export interface DistribuicaoDeHorasProps extends ReactProps
    {
        largura: number,
        altura?: number
    }

    export interface DistribuicaoDeHorasState
    {
        dataRefresh :boolean;
    }

    export interface DadosGrafico
    {
        legenda :string;
        valor: number;
        cor :string;
    }

}