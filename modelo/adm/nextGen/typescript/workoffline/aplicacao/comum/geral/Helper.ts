import { Helper as LibHelper } from 'lib/geral'
import { Sexo, TipoPontoEletronico } from 'aplicacao/comum/geral';

export class Helper extends LibHelper
{
    static getSexoAsString(sexo: Sexo)
    {
        switch (sexo)
        {
            case Sexo.MASCULINO:
                return "Masculino";

            case Sexo.FEMININO:
                return "Feminino";

            case Sexo.NAO_DEFINIDO:
                return "Não definido";
        }

    }

    static getTipoPontoAsString(tipoPonto: TipoPontoEletronico)
    {
        switch (tipoPonto)
        {
            case TipoPontoEletronico.PONTO_ENTRADA:
                return "Entrada";

            case TipoPontoEletronico.PONTO_SAIDA:
                return "Saída";
        }

    }


}