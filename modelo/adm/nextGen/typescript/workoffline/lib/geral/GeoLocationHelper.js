"use strict";
exports.__esModule = true;
var geral_1 = require("lib/geral");
var GeoLocationHelper = (function () {
    function GeoLocationHelper() {
    }
    GeoLocationHelper.getFloatValue = function (integerCoordinate) {
        if (integerCoordinate instanceof String) {
            integerCoordinate = geral_1.Helper.parseInt(integerCoordinate);
        }
        return integerCoordinate / 1e6;
    };
    GeoLocationHelper.getIntegerValue = function (floatCoordinate) {
        return floatCoordinate * 1e6;
    };
    GeoLocationHelper.getGoogleLatLng = function (integerLat, integerLng) {
        var floatLat = GeoLocationHelper.getFloatValue(integerLat);
        var floatLng = GeoLocationHelper.getFloatValue(integerLng);
        return new google.maps.LatLng(floatLat, floatLng);
    };
    GeoLocationHelper.isValidCoordinate = function (floatLat, floatLng) {
        return (floatLat != null && floatLat >= -90 && floatLat <= 90
            && floatLng != null && floatLng >= -180 && floatLng <= 180);
    };
    return GeoLocationHelper;
}());
exports.GeoLocationHelper = GeoLocationHelper;
