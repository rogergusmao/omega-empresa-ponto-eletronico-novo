import { RemoteModel } from 'lib/model';

export class Empresa extends RemoteModel
{
    public latitude :string;
    public longitude :string;
    public nomeEmpresa :string;
    public idEmpresa :string;
}