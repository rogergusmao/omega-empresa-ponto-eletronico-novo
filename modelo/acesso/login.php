<?php

    include_once '../recursos/php/constants.php';
    include_once '../recursos/php/database_config.php';
    include_once '../adm/imports/header.php';
    include_once '../recursos/php/funcoes.php';
    include_once '../adm/imports/instancias.php';
    include_once '../adm/imports/lingua.php';

    session_name(IDENTIFICADOR_SESSAO_CLIENTE);
    session_start();

    $objSeguranca = new Seguranca();
    $objSeguranca->matarSessao();

?>

<html>
<head>
    <title><?= TITULO_PAGINAS ?></title>

    <?= Helper::getTagDoFavicon(); ?>


    <?= Javascript::importarTodasAsBibliotecas(); ?>
    <?= Javascript::importarBibliotecaDaTela("login"); ?>
    <? include('imports/herder.php'); ?>
    <?= Helper::carregarCssFlatty(); ?>

</head>
<body class='contrast-blue login contrast-background'>

<?

    Javascript::imprimirCorpoDoTooltip();

?>

<div class='middle-container'>
    <div class='middle-row'>
        <div class='middle-wrapper'>
            <div class='login-container-header'>
                <div class='container'>
                    <div class='row'>
                        <div class='col-sm-12'>
                            <div class='text-center'>
                                <img width="121" height="31" src="css/flatty/assets/images/logo_lg.svg"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class='login-container'>
                <div class='container'>
                    <div class='row'>
                        <div class='col-sm-4 col-sm-offset-4'>
                            <h1 class='text-center title'>
                                <?=I18N::getExpression("Login") ?>
                            </h1>

                            <form action="actions.php?class=Seguranca_Pagamento&amp;action=login" class="validate-form"
                                  method="post" name="formulario_login" id="formulario_login"
                                  onsubmit="return validaFormularioLogin(this);" novalidate="novalidate">

                                <input type="hidden" id="next_action" name="next_action" value="pagina_inicial">

                                <div class="form-group">
                                    <div class="controls with-icon-over-input">
                                        <span id="tf_txtLogin">
                                            <input type="text" name="txtLogin" value="" id="txtLogin"
                                                      class="form-control" style="  text-transform:lowercase;  "
                                                      placeholder="E-mail" autocomplete="off">
                                            <span class="textfieldInvalidFormatMsg">
                                                <?=I18N::getExpression("Valor inválido") ?>
                                            </span>
                                            <span class="textfieldRequiredMsg">
                                                <?=I18N::getExpression("Preenchimento obrigatório") ?>
                                            </span>
                                        </span>

                                        <script type="text/javascript">

                                            var textfield_txtLogin = new Spry.Widget.ValidationTextField("tf_txtLogin", "email", {
                                                validateOn: ["blur"],
                                                isRequired: true
                                            });

                                        </script>
                                        <br>

                                        <i class="icon-user text-muted"></i>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="controls with-icon-over-input">
                                                <span id="tf_txtSenha">
                                                    <input type="password" name="txtSenha" value=""
                                                                              id="txtSenha" class="form-control"
                                                                              style="   " placeholder="senha"
                                                                              autocomplete="off">
                                                    <span class="textfieldInvalidFormatMsg">
                                                        <?=I18N::getExpression("Valor inválido") ?>
                                                    </span>
                                                    <span class="textfieldRequiredMsg">
                                                        <?=I18N::getExpression("Preenchimento obrigatório") ?>
                                                    </span>
                                                </span>

                                        <script type="text/javascript">

                                            var textfield_txtSenha = new Spry.Widget.ValidationTextField("tf_txtSenha", "none", {
                                                validateOn: ["blur"],
                                                isRequired: true
                                            });

                                        </script>
                                        <i class="icon-lock text-muted"></i>
                                    </div>
                                </div>
                                <div class="checkbox">


                                    <label for="remember_me">
                                        <input id="remember_me" name="remember_me" type="checkbox" value="1">
                                        <?=I18N::getExpression("Relembrar") ?>
                                    </label>
                                </div>
                                <button class="btn btn-block">
                                    <?=I18N::getExpression("Acessar") ?>
                                </button>
                            </form>
                            <div class='text-center'>
                                <a data-toggle='modal' href='#modal-example2'>
                                    <?=I18N::getExpression("Esqueceu sua senha") ?>
                                </a>
                            </div>

                            <? include('paginas/lembrar_senha.php'); ?>

                        </div>
                    </div>
                </div>
            </div>
            <div class='login-container-footer'>
                <div class='container'>
                    <div class='row'>
                        <div class='col-sm-12'>
                            <div class='text-center'>
                                <a href='popup_personalizado.php?tipo=formularios&page=cliente_simplificado'>
                                    <i class='icon-user'></i>
                                    <?=I18N::getExpression("Você é novo na {0}?", Helper::getNomeDaEmpresa()) ?>
                                    <strong>
                                        <?=I18N::getExpression("Cadastre-se") ?>
                                    </strong>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?
    echo Javascript::importarBibliotecasFlatty();
?>

</body>

</html>
