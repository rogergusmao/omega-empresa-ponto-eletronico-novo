import { ContextoGenerico } from 'lib/contexto';

export class DomContainer<T extends ContextoGenerico>
{
    public contexto: T;
    public domSelector: string;

    public constructor(contexto: T, domSelector: string)
    {
        this.contexto = contexto;
        this.domSelector = domSelector;
    }

    public $(): JQuery;
    public $(selector: string): JQuery;
    public $(selector?: string): JQuery
    {
        return $(this.contexto.getDomRootSelector() + ' ' + this.domSelector + (typeof selector == 'undefined' ? "" : " " + selector));
    }

    public $root(): JQuery;
    public $root(selector: string): JQuery;
    public $root(selector?: string): JQuery
    {
        return $(this.contexto.getDomRootSelector() + (typeof selector == 'undefined' ? "" : " " + selector));
    }

    public static $documentBody(): JQuery;
    public static $documentBody(selector: string): JQuery;
    public static $documentBody(selector?: string): JQuery
    {
        return $('body' + (typeof selector == 'undefined' ? "" : " " + selector));
    }

    public static getMaxAllowedHeight()
    {
        var maxHeight: number = $(window).height();

        DomContainer.$documentBody('.navbar').each(function ()
        {
            maxHeight -= $(this).outerHeight();
        });

        return maxHeight;
    }

    public static getMaxAllowedWidth()
    {
        var maxWidth: number = $(window).width();

        this.$documentBody('#main-nav-bg').each(function ()
        {
            maxWidth -= $(this).outerWidth();
        });

        return maxWidth;
    }

}