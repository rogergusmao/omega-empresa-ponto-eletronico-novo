<?php

class Servicos_web_ponto_eletronico_anonimo
{
    //Como chamar algum webservice da classe:
    //http://127.0.0.1/hospedagem/10001/adm/actions.php?class=Servicos_web&action=
    public function __construct()
    {
    }

    public static function factory()
    {
        return new Servicos_web_ponto_eletronico_anonimo();
    }

    public static function uploadLogErro()
    {
        $chave = Helper::POSTGET("chave", true);
        $campos = MCrypt::decryptGet($chave);

        if (!$campos)
        {
            return Mensagem::factoryAcessoNegado();
        }

        $s = $campos["hash"]["palavra-secreta"];

        if ($s != "sakdmfwr42309rwfmvfc2034kr03wf0jkfvmo3m40tfjk3qefksdpfgmsodmgb409j5tg09w4ej5gksdlfkg34ijrt093")
        {
            HelperLog::logPerigo("PERIGO[2F3WFR]: " . print_r($s, true));
            return Mensagem::factoryAcessoNegado();
        }

        $corporacao = isset($campos["hash"]["corporacao"]) ? $campos["hash"]["corporacao"] : null;
        $idMI = isset($campos["hash"]["id_mobile_identificador"]) ? $campos["hash"]["id_mobile_identificador"] : null;
        $msg = BO_Log_app::uploadLogErro($corporacao, $idMI);
        return $msg;

    }

}
