<?php

include '../../recursos/languages/pt-br.php';
include '../../recursos/php/funcoes.php';
include '../../recursos/php/constants.php';
include '../../recursos/php/database_config.php';

$nomeScript = Helper::getNomeDoScriptAtual();
$vIsPrimeiraVez = Helper::GET("is_primeira_vez_BOOLEAN");

$registrosPorPagina = REGISTROS_POR_PAGINA;
$registrosPesquisa = 1;

$obj = new EXTDAO_Mensagem_sms_pessoa();
$obj->setByGet($registrosPesquisa);
$obj->formatarParaSQL();

$strCondicao = array();
$strGET = array();

if (!Helper::isNull($obj->getUsuario_id_INT()))
{

    $strCondicao[] = "msp.usuario_id_INT={$obj->getUsuario_id_INT()}";
    $strGET[] = "usuario_id_INT1={$obj->getUsuario_id_INT()}";
}

if (!Helper::isNull($obj->getPessoa_id_INT()))
{

    $strCondicao[] = "msp.pessoa_id_INT1={$obj->getPessoa_id_INT()}";
    $strGET[] = "pessoa_id_INT1={$obj->getPessoa_id_INT()}";
}

if (!Helper::isNull($obj->getMensagem_sms_id_INT()))
{

    $strCondicao[] = "msp.mensagem_sms_id_INT1={$obj->getMensagem_sms_id_INT()}";
    $strGET[] = "mensagem_sms_id_INT1={$obj->getMensagem_sms_id_INT()}";
}

if (!Helper::isNull($obj->getFoi_enviado_BOOLEAN()))
{

    $strCondicao[] = "msp.foi_enviado_BOOLEAN1={$obj->getFoi_enviado_BOOLEAN()}";
    $strGET[] = "foi_enviado_BOOLEAN1={$obj->getFoi_enviado_BOOLEAN()}";
}
if (!Helper::isNull(Helper::GET("datetime_inicio_cadastro1")))
{

    $datetimeInicio = Helper::GET("datetime_inicio_cadastro1");

    $strCondicao[] = "ms.dataCadastro >=" . Helper::formatarDataTimeParaComandoSQL($datetimeInicio);
    $strGET[] = "datetime_inicio_cadastro1={$datetimeInicio}";
    $strDescricao[] = "&bull; <b>Data inicial:</b> {$datetimeInicio}";
}

if (!Helper::isNull(Helper::GET("datetime_fim_cadastro1")))
{

    $datetimeFim = Helper::GET("datetime_fim_cadastro1");

    $strCondicao[] = "ms.dataCadastro <=" . Helper::formatarDataTimeParaComandoSQL($datetimeFim);
    $strGET[] = "datetime_fim_cadastro1={$datetimeFim}";
    $strDescricao[] = "&bull; <b>Data final:</b> {$datetimeFim} anos";
}

if (!Helper::isNull(Helper::GET("datetime_inicio_envio1")))
{

    $datetimeInicio = Helper::GET("datetime_inicio_envio1");

    $strCondicao[] = "msp.data_envio_DATETIME >=" . Helper::formatarDataTimeParaComandoSQL($datetimeInicio);
    $strGET[] = "datetime_inicio_envio1={$datetimeInicio}";
    $strDescricao[] = "&bull; <b>Data inicial:</b> {$datetimeInicio}";
}

if (!Helper::isNull(Helper::GET("datetime_fim_envio1")))
{

    $datetimeFim = Helper::GET("datetime_fim_envio1");

    $strCondicao[] = "msp.data_envio_DATETIME <=" . Helper::formatarDataTimeParaComandoSQL($datetimeFim);
    $strGET[] = "datetime_fim_envio1={$datetimeFim}";
    $strDescricao[] = "&bull; <b>Data final:</b> {$datetimeFim} anos";
}

if (!Helper::isNull($obj->getTelefone_id_INT()))
{

    $strCondicao[] = "msp.telefone_id_INT1={$obj->getTelefone_id_INT()}";
    $strGET[] = "telefone_id_INT1={$obj->getTelefone_id_INT()}";
}

if (!Helper::isNull($obj->getOperadora_id_INT()))
{

    $strCondicao[] = "msp.operadora_id_INT1={$obj->getOperadora_id_INT()}";
    $strGET[] = "operadora_id_INT1={$obj->getOperadora_id_INT()}";
}

$consulta = "";
for ($i = 0; $i < count($strCondicao); $i++)
{

    $consulta .= " AND " . $strCondicao[$i];
}

$varGET = "";
for ($i = 0; $i < count($strGET); $i++)
{

    $varGET .= "&" . $strGET[$i];
}

if ($nomeScript == "index.php" && !isset($vIsPrimeiraVez))
{

    echo "<center>";
    Helper::imprimirMensagem("A janela de download abrirá em alguns segundos.\nCaso não abra automaticamente, <a class='link_padrao' href='pages/exportar_relatorio_mensagem_sms_pessoa.php?is_primeira_vez_BOOLEAN=0&$varGET'>clique aqui</a>.");
    echo "</center>";

    Helper::mudarLocation("pages/exportar_relatorio_mensagem_sms_pessoa.php?is_primeira_vez_BOOLEAN=0&$varGET");
    exit();
}

$consultaNumero = "SELECT COUNT(msp.id)
                       FROM mensagem_sms_pessoa msp JOIN mensagem_sms ms ON msp.mensagem_sms_id_INT= ms.id
                       WHERE msp.excluido_BOOLEAN=0 {$consulta}";

$objBanco = new Database();

$objBanco->query($consultaNumero);
$numeroRegistros = $objBanco->getPrimeiraTuplaDoResultSet(0);

$limites = Helper::getLimitesRegsPaginacao($registrosPorPagina, $numeroRegistros);

$consultaRegistros = "SELECT msp.id, msp.foi_enviado_BOOLEAN, msp.pessoa_id_INT, msp.numero_tentativa_INT
                          FROM mensagem_sms_pessoa msp JOIN mensagem_sms ms ON msp.mensagem_sms_id_INT= ms.id
                          WHERE msp.excluido_BOOLEAN=0 {$consulta} 
                          ORDER BY msp.id LIMIT {$limites[0]},{$limites[1]}";

$objBanco->query($consultaRegistros);

$stringRetorno = "";
$vetorCabecalho = array("Nome", "Celular", "Enviado");
$stringRetorno .= Helper::getStrLinhaCSVDoVetorDeDados($vetorCabecalho);

for ($i = 1; $regs = $objBanco->fetchArray(); $i++)
{

    $vId = $regs[0];
    $obj->select($vId);
    $obj->formatarParaExibicao();

    $foiEnviado = $regs[1];
    $vIdPessoa = $regs[2];
    $vNumeroTentativa = $regs[3];
    if (Helper::isNull($vNumeroTentativa))
    {
        $vNumeroTentativa = 0;
    }
    $vNome = "";

    if (strlen($vIdPessoa))
    {

        $objPessoa = new EXTDAO_Pessoa();
        $objPessoa->select($vIdPessoa);
        $vPrimeiroNome = $objPessoa->getPrimeiro_nome();
        $vSobrenome = $objPessoa->getSobrenomes;

        if (strlen($vPrimeiroNome) || strlen($vSobrenome))
        {

            $vNome = $vPrimeiroNome;
            if (strlen($vSobrenome))
            {

                if (strlen($vNome))
                {
                    $vNome .= " " . $vSobrenome;
                }

                else
                {
                    $vNome .= $vSobrenome;
                }
            }
        }
    }

    $vCelular = "";
    if (strlen($vIdPessoa))
    {
        $vCelular = $objPessoa->getCelular();
    }

    $vStrEnvio = "";
    if ($foiEnviado)
    {

        $dataDoEnvio = Helper::formatarDataTimeParaExibicao($dataDoEnvio);
        $vStrEnvio = "Sim ({$dataDoEnvio})";
    }
    else
    {

        if ($vNumeroTentativa >= LIMITE_TENTATICA_ENVIO_SMS)
        {
            $vStrEnvio = "Erro";
        }
        else
        {
            $vStrEnvio = "Não";
        }
    }

    $vetorTupla = array($vId, $vCelular, $vStrEnvio);
    $stringRetorno .= Helper::getStrLinhaCSVDoVetorDeDados($vetorTupla);
}

$objDownload = new Download("relatorio_mensagem_sms_pessoa.csv");
print $objDownload->ds_download($stringRetorno);

?>
