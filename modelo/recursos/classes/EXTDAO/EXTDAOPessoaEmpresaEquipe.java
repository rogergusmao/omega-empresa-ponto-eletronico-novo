

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  EXTDAOPessoaEmpresaEquipe
        * DATA DE GERA��O: 21.08.2017
        * ARQUIVO:         EXTDAOPessoaEmpresaEquipe.java
        * TABELA MYSQL:    pessoa_empresa_equipe
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronicofactu.database.EXTDAO;

        import app.omegasoftware.pontoeletronicofactu.database.DAO.DAOPessoaEmpresaEquipe;
        import app.omegasoftware.pontoeletronicofactu.database.Attribute;
        import app.omegasoftware.pontoeletronicofactu.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronicofactu.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronicofactu.database.Database;
        import app.omegasoftware.pontoeletronicofactu.database.Table;


        // **********************
        // DECLARA��O DA CLASSE
        // **********************


    
        

        public class EXTDAOPessoaEmpresaEquipe extends DAOPessoaEmpresaEquipe
        {


        // *************************
        // DECLARA��O DE ATRIBUTOS
        // *************************
        
        // *************************
        // CONSTRUTOR
        // *************************
        public EXTDAOPessoaEmpresaEquipe(Database database){
            super(database);

        }
        
        // *************************
        // FACTORY
        // *************************
        public Table factory(){
            return new EXTDAOPessoaEmpresaEquipe(this.getDatabase());

        }
        

        } // classe: fim


        