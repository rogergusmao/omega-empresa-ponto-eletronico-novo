import * as Moment from 'moment';

import { MomentHelper } from 'lib/geral';
import { RemoteDataService } from 'lib/dataService';
import { DadosIniciais as DadosIniciaisCalendario } from 'aplicacao/modulo/calendario/remoteModel';
import { PontoEletronico as PontoEletronicoCalendarioModel } from 'aplicacao/modulo/calendario/model';

import {
    DadosIniciais as DadosIniciaisMapa, 
    Usuario as UsuarioMapa, 
    PontoEletronico as PontoEletronicoMapa, 
    Empresa as EmpresaMapa, 
    WifiNetwork as WifiNetworkMapa } from 'aplicacao/modulo/mapa/remoteModel';


export class PontoEletronicoServices extends RemoteDataService
{
    public constructor()
    {
        super();
        this.remoteDefaultClassName = "Servicos_web_ponto_eletronico";
    }

    public async getDadosIniciaisDoCalendario(startDatetime: Moment.Moment, endDatetime :Moment.Moment): Promise<DadosIniciaisCalendario>
    {
        let url = this.getServiceUrl("getDadosCalendario");
        let utcTimestampStart = MomentHelper.getUtcTimestampInSeconds(startDatetime);
        let utcTimestampEnd = MomentHelper.getUtcTimestampInSeconds(endDatetime);
        
        let params :any = { dataInicialSec: utcTimestampStart, dataFinalSec: utcTimestampEnd };            
        return await RemoteDataService.getAsyncItem<DadosIniciaisCalendario>(url, params);
    }

    public async getDadosIniciaisDoMapa(): Promise<DadosIniciaisMapa>
    {
        let url = this.getServiceUrl("getDadosMapa");
        return await RemoteDataService.getAsyncItem<DadosIniciaisMapa>(url);
    }

    public async getPontosEletronicos(): Promise<Array<PontoEletronicoMapa>>
    {
        let url = this.getServiceUrl("getPontosEletronicos");
        return await RemoteDataService.getAsyncList<PontoEletronicoMapa>(url);
    }

    public async getUsuarios(): Promise<Array<UsuarioMapa>>
    {
        let url = this.getServiceUrl("getUsuarios");
        return await RemoteDataService.getAsyncList<UsuarioMapa>(url);
    }

    public async getEmpresas(): Promise<Array<EmpresaMapa>>
    {
        let url = this.getServiceUrl("getEmpresas");
        return await RemoteDataService.getAsyncList<EmpresaMapa>(url);
    }

    public async getWifiNetworks(): Promise<Array<WifiNetworkMapa>>
    {
        let url = this.getServiceUrl("getWifiNetworks");
        return await RemoteDataService.getAsyncList<WifiNetworkMapa>(url);
    }

    public async gravarPontoEletronicoParaCorrecao(pontoEletronico: PontoEletronicoCalendarioModel): Promise<any>
    {
        let url = this.getServiceUrl("gravarPontoEletronicoParaCorrecao");
        let utcTimestampPonto = MomentHelper.getUtcTimestampInSeconds(pontoEletronico.dataPonto);
        let utcTimezoneOffset = MomentHelper.getTimezoneOffsetInSeconds(pontoEletronico.dataPonto);

        let tipoPonto = pontoEletronico.tipoPonto;
        let idPessoa = pontoEletronico.idPessoa;
        let idPontoReferencia = pontoEletronico.idPonto;
            
        let params :any = { 
            utcTimestampPonto: utcTimestampPonto, 
            utcTimezoneOffset: utcTimezoneOffset, 
            tipoPonto: tipoPonto, 
            idPessoa: idPessoa,
            idPontoReferencia: idPontoReferencia 
        };          
        
        return await RemoteDataService.getAsyncItem<any>(url, params);   
    }

}
    