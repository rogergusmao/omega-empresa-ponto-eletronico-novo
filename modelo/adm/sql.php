<?php

require_once '../recursos/php/configuracao_biblioteca_compartilhada.php';

require_once $workspaceRaiz . DIRETORIO_BIBLIOTECAS_COMPARTILHADAS . '/imports/header.php';

ConfiguracaoCorporacaoDinamica::init(getWebservicesSemCache());

require_once '../recursos/php/funcoes.php';

$objBanco = new Database();

$user = $objBanco->getUser();
$senha = $objBanco->getSenha();
$nome = $objBanco->getDBName();
$host = $objBanco->getHost();

$resp = "mysqldump --host={$host} --user={$user} --password={$senha} --databases {$nome} --skip-set-charset --skip-comments --compact > teste.sql";

$output = Helper::shellExec($resp);
print $output;

