"use strict";
exports.__esModule = true;
var MomentJS = require("moment");
var MomentHelper = (function () {
    function MomentHelper() {
    }
    MomentHelper.getMomentInstance = function (dateObject) {
        return MomentJS(dateObject);
    };
    MomentHelper.formatMomentToDatetimeExibicao = function (momentObject) {
        return momentObject.format(MomentHelper.FORMATO_DATAHORA_EXIBICAO);
    };
    MomentHelper.formatMomentToDateExibicao = function (momentObject) {
        return momentObject.format(MomentHelper.FORMATO_DATA_EXIBICAO);
    };
    MomentHelper.formatMoment = function (momentObject, formato) {
        return momentObject.format(formato);
    };
    MomentHelper.formatDate = function (dateObject, formato) {
        return MomentHelper.getMomentInstance(dateObject).format(formato);
    };
    MomentHelper.getUtcTimestampInSeconds = function (momentObject) {
        return momentObject.utc().valueOf() / 1000;
    };
    MomentHelper.getTimezoneOffsetInSeconds = function (momentObject) {
        return momentObject.utcOffset() * 60;
    };
    return MomentHelper;
}());
MomentHelper.FORMATO_DATA_FULLCALENDAR = 'YYYY-MM-DD';
MomentHelper.FORMATO_DATAHORA_FULLCALENDAR = 'YYYY-MM-DDTHH:mm:ss';
MomentHelper.FORMATO_DATA_MYSQL = 'YYYY-MM-DD';
MomentHelper.FORMATO_DATAHORA_MYSQL = 'YYYY-MM-DD HH:mm:ss';
MomentHelper.FORMATO_DATA_EXIBICAO = 'DD/MM/YYYY';
MomentHelper.FORMATO_DATAHORA_EXIBICAO = 'DD/MM/YYYY HH:mm:ss';
exports.MomentHelper = MomentHelper;
