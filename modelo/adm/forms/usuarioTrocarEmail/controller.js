/*

Arquivo gerado atrav�s de gerador de c�digo em 19/01/2018 as 22:03:45.
Para que o arquivo n�o seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

Tabela correspondente: usuario
Sobrescrita de dados: ---SOBRESCRITA_NAO_AUTORIZADA---

*/
omegaApp.controller('formUsuarioTrocarEmailController',
    ['$scope', '$rootScope', '$element', '$window', '$interval', '$timeout', 'dataService', 'Upload',
     function ($scope, $rootScope, $element, $window, $interval, $timeout, dataService, Upload)
     {
         var vm = this;

         try
         {
             vm.rootControllerElement = $element;
             vm.fieldsTypes = {};

             vm.messages = {
                 error: [],
                 warning: [],
                 info: [],
                 success: []
             };

             vm.exibirCamposDeSenha = false;

             vm.isModalView = false;
             vm.idUsuario = null;

             vm.formData = {
                 emailAtual: dataService.emailAtual,
                 emailNovo: null,
                 senha: null,
                 confirmacaoSenha: null
             };

             vm.disabledFields = {
                 fieldEmailAtual: true,
                 fieldEmailNovo: false,
                 fieldSenha: false,
                 fieldConfirmacaoSenha: false
             };

             vm.requiredFields = {
                 fieldEmailAtual: true,
                 fieldEmailNovo: true,
                 fieldSenha: true,
                 fieldConfirmacaoSenha: true
             };

             vm.fieldsParameters = {
                 fieldEmailAtual: null,
                 fieldEmailNovo: null,
                 fieldSenha: null,
                 fieldConfirmacaoSenha: null
             };

             if (GeneralUtil.isNullOrEmpty(dataService.idUsuario) || !GeneralUtil.isNumberOrNumericString(dataService.idUsuario))
             {
                 $rootScope.closeLastModalInstance();
                 ApplicationUtil.showErrorMessage("Usu�rio inv�lido");
             }

             vm.submitForm = function (formInstance)
             {
                 if ($rootScope.validateForm(formInstance, vm))
                 {
                     //caso seja a primeira chamada
                     if (!vm.exibirCamposDeSenha)
                     {
                         var emailNovo = vm.formData.emailNovo;
                         var idUsuario = dataService.idUsuario;

                         dataService.alterarEmailUsuario(idUsuario, emailNovo).then(function (returnContent)
                         {
                             if (RemoteDataUtil.validateRemoteResponse(returnContent, [PROTOCOLO_SISTEMA.OPERACAO_REALIZADA_COM_SUCESSO, PROTOCOLO_SISTEMA.USUARIO_JA_EXISTE_NO_SICOB_E_NO_SISTEMA]))
                             {
                                 var returnObject = RemoteDataUtil.getResponseObject(returnContent);
                                 var responseCode = RemoteDataUtil.getResponseCode(returnContent);
                                 var responseMessage = RemoteDataUtil.getResponseMessageStringSafe(returnContent);

                                 if (responseCode == PROTOCOLO_SISTEMA.USUARIO_JA_EXISTE_NO_SICOB_E_NO_SISTEMA)
                                 {
                                     ApplicationUtil.showInfoMessage(responseMessage, null, vm);

                                     vm.exibirCamposDeSenha = true;
                                     vm.disabledFields.fieldEmailNovo = true;
                                 }
                                 else
                                 {
                                     $rootScope.closeLastModalInstance();
                                     ApplicationUtil.showSuccessMessage(responseMessage);
                                 }

                             }
                             else
                             {
                                 RemoteDataUtil.defaultRemoteErrorHandling(returnContent, vm);
                             }
                         });
                     }
                     else
                     {

                         var emailNovo = vm.formData.emailNovo;
                         var idUsuario = dataService.idUsuario;
                         var senha = vm.formData.senha;
                         var confirmacaoSenha = vm.formData.confirmacaoSenha;

                         //caso seja cadastro de usu�rio, valida a senha e a confirma��o
                         if (senha != confirmacaoSenha)
                         {
                             ApplicationUtil.showErrorMessage(I18NUtil.getExpression("A senha e a confirma��o de senha n�o correspondem.", "Password does not match with confirmation."), null, vm);
                         }
                         else
                         {
                             dataService.alterarEmailAndSenhaUsuario(idUsuario, emailNovo, senha).then(function (returnContent)
                             {
                                 if (RemoteDataUtil.validateRemoteResponse(returnContent, [PROTOCOLO_SISTEMA.OPERACAO_REALIZADA_COM_SUCESSO]))
                                 {
                                     var responseMessage = RemoteDataUtil.getResponseMessageStringSafe(returnContent);

                                     $rootScope.closeLastModalInstance();

                                     ApplicationUtil.showSuccessMessage(responseMessage);
                                 }
                                 else
                                 {
                                     RemoteDataUtil.defaultRemoteErrorHandling(returnContent, vm);
                                 }
                             });

                         }

                     }

                 }

             };

         }
         catch (ex)
         {
             ApplicationUtil.showErrorMessage(ex.message, 'Erro no controller usuarioTrocarEmailController', vm);
         }

     }]
);