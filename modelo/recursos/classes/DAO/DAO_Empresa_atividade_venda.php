<?php


        /*

        Arquivo gerado através de gerador de código em 12/05/2018 as 14:45:58.
        Para que o arquivo não seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

        Tabela correspondente: empresa_atividade_venda
        Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

        */
        


class DAO_Empresa_atividade_venda extends Generic_DAO
                        {
                            const NOME_ENTIDADE = "empresa_atividade_venda";

                            public static $nomeTabela = null;
                            public static $idSistemaTabela = false;
                            public static $databaseFieldsRelatedAttributes = null;
	                        public static $databaseFieldTypes = null;
	                        public static $databaseFieldNames = null;
	                        public static $listAliasTypes = null;
	                        public static $listAliasRelatedAttributes = null;
	                        
	                        public $id;
public $empresaVendaId;
public $quantidadeFloat;
public $valorTotalFloat;
public $descontoFloat;
public $corporacaoId;
public $cadastroSec;
public $cadastroOffsec;
public $atividadeId;
public $empresaId;
public $protocoloInt;

public $labelId;
public $labelEmpresaVendaId;
public $labelQuantidadeFloat;
public $labelValorTotalFloat;
public $labelDescontoFloat;
public $labelCorporacaoId;
public $labelCadastroSec;
public $labelCadastroOffsec;
public $labelAtividadeId;
public $labelEmpresaId;
public $labelProtocoloInt;

public static $arrPK = array("id");

public function __construct($configDAO = null)
        {
            parent::__construct($configDAO);
            static::__setStaticValues($this->database);           
        }

public static function idSistemaTabela($databaseObject = null){
            if(!(static::$idSistemaTabela))
            {
                static::$idSistemaTabela = EXTDAO_Sistema_tabela::getIdDaSistemaTabela(static::nomeTabela(), $databaseObject);
            }
            return static::$idSistemaTabela;
        }

public function getIdSistemaTabela(){
            return static::idSistemaTabela();
        }

public static function nomeTabela(){
            if(is_null(static::$nomeTabela))
            {
                static::$nomeTabela = "empresa_atividade_venda";
            }
            return static::$nomeTabela;
        }

public function getNomeTabela(){
            return static::nomeTabela();
        }

public static function __setStaticValues($databaseObject = null)
                   {    
                        static::nomeTabela();
                        
                        static::idSistemaTabela($databaseObject);
                   
                        static::setDatabaseFieldNames();
                        static::setDatabaseFieldTypes();
                        static::setDatabaseFieldsRelatedAttributeNames();
                        
                        static::setListAliasTypes();
                        static::setListAliasRelatedAttributes();
                   }

public function getExpressaoLabel()
                   {
                        return "{id}";
                   }

public static function setDatabaseFieldNames()
        {
            if(is_null(static::$databaseFieldNames))
            {
                static::$databaseFieldNames = new stdClass();
                static::$databaseFieldNames->id = "id";
static::$databaseFieldNames->empresaVendaId = "empresa_venda_id_INT";
static::$databaseFieldNames->quantidadeFloat = "quantidade_FLOAT";
static::$databaseFieldNames->valorTotalFloat = "valor_total_FLOAT";
static::$databaseFieldNames->descontoFloat = "desconto_FLOAT";
static::$databaseFieldNames->corporacaoId = "corporacao_id_INT";
static::$databaseFieldNames->cadastroSec = "cadastro_SEC";
static::$databaseFieldNames->cadastroOffsec = "cadastro_OFFSEC";
static::$databaseFieldNames->atividadeId = "atividade_id_INT";
static::$databaseFieldNames->empresaId = "empresa_id_INT";
static::$databaseFieldNames->protocoloInt = "protocolo_INT";

            }
                      
        }

public static function setDatabaseFieldTypes()
        {
            if(is_null(static::$databaseFieldTypes))
            {
                static::$databaseFieldTypes = new stdClass();
                static::$databaseFieldTypes->id = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->empresaVendaId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->quantidadeFloat = static::TIPO_VARIAVEL_FLOAT;
static::$databaseFieldTypes->valorTotalFloat = static::TIPO_VARIAVEL_FLOAT;
static::$databaseFieldTypes->descontoFloat = static::TIPO_VARIAVEL_FLOAT;
static::$databaseFieldTypes->corporacaoId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->cadastroSec = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$databaseFieldTypes->cadastroOffsec = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$databaseFieldTypes->atividadeId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->empresaId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->protocoloInt = static::TIPO_VARIAVEL_INTEGER;

            }
            
        }

public static function setDatabaseFieldsRelatedAttributeNames()
        {
            if(is_null(static::$databaseFieldsRelatedAttributes))
            {
                static::$databaseFieldsRelatedAttributes = new stdClass();
                static::$databaseFieldsRelatedAttributes->id = "id";
static::$databaseFieldsRelatedAttributes->empresa_venda_id_INT = "empresaVendaId";
static::$databaseFieldsRelatedAttributes->quantidade_FLOAT = "quantidadeFloat";
static::$databaseFieldsRelatedAttributes->valor_total_FLOAT = "valorTotalFloat";
static::$databaseFieldsRelatedAttributes->desconto_FLOAT = "descontoFloat";
static::$databaseFieldsRelatedAttributes->corporacao_id_INT = "corporacaoId";
static::$databaseFieldsRelatedAttributes->cadastro_SEC = "cadastroSec";
static::$databaseFieldsRelatedAttributes->cadastro_OFFSEC = "cadastroOffsec";
static::$databaseFieldsRelatedAttributes->atividade_id_INT = "atividadeId";
static::$databaseFieldsRelatedAttributes->empresa_id_INT = "empresaId";
static::$databaseFieldsRelatedAttributes->protocolo_INT = "protocoloInt";

            }           
        }

public function setAllFieldsToHumanFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["empresa_venda_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["quantidade_FLOAT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["valor_total_FLOAT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["desconto_FLOAT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["cadastro_SEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["cadastro_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["atividade_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["empresa_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["protocolo_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
}

public function setAllFieldsToDatabaseFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["empresa_venda_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["quantidade_FLOAT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["valor_total_FLOAT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["desconto_FLOAT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["cadastro_SEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["cadastro_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["atividade_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["empresa_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["protocolo_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
}

  public function getFkObjEmpresa_venda() 
                {
                    if($this->objEmpresa_venda == null)
                    {                        
                        $this->objEmpresa_venda = new EXTDAO_Empresa_venda_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getEmpresa_venda_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objEmpresa_venda->clear();
                    }
                    elseif($this->objEmpresa_venda->getId() != $idFK)
                    {
                        $this->objEmpresa_venda->select($idFK);
                    }
                    return $this->objEmpresa_venda;
                }
  public function getFkObjCorporacao() 
                {
                    if($this->objCorporacao == null)
                    {                        
                        $this->objCorporacao = new EXTDAO_Corporacao_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getCorporacao_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objCorporacao->clear();
                    }
                    elseif($this->objCorporacao->getId() != $idFK)
                    {
                        $this->objCorporacao->select($idFK);
                    }
                    return $this->objCorporacao;
                }
  public function getFkObjAtividade() 
                {
                    if($this->objAtividade == null)
                    {                        
                        $this->objAtividade = new EXTDAO_Atividade_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getAtividade_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objAtividade->clear();
                    }
                    elseif($this->objAtividade->getId() != $idFK)
                    {
                        $this->objAtividade->select($idFK);
                    }
                    return $this->objAtividade;
                }
  public function getFkObjEmpresa() 
                {
                    if($this->objEmpresa == null)
                    {                        
                        $this->objEmpresa = new EXTDAO_Empresa_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getEmpresa_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objEmpresa->clear();
                    }
                    elseif($this->objEmpresa->getId() != $idFK)
                    {
                        $this->objEmpresa->select($idFK);
                    }
                    return $this->objEmpresa;
                }


public function setLabels()
        {$this->labelId = "";
$this->labelEmpresaVendaId = "";
$this->labelQuantidadeFloat = "";
$this->labelValorTotalFloat = "";
$this->labelDescontoFloat = "";
$this->labelCorporacaoId = "";
$this->labelCadastroSec = "";
$this->labelCadastroOffsec = "";
$this->labelAtividadeId = "";
$this->labelEmpresaId = "";
$this->labelProtocoloInt = "";
}

public function __actionAdd($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                $this->setByObject($parameters);
                $this->setDataCadastroAndOffsetInSecondsFromUser($parameters);
                
                $this->formatarParaSQL();
                $msg = $this->insert(true);
                
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                return new Mensagem(null, I18N::getExpression("Empresa atividade venda adicionado com sucesso."));
                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionEdit($parameters = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try {

                $this->setByObject($parameters);
                $this->setAllFieldsToHumanFormat();
                
                $this->formatarParaSQL();
                $msg = $this->updateByInstanceUsingParameters($this->id, $parameters, true);
                if($msg != null && $msg->erro())
                {
                     return $msg;
                }
        
                return new Mensagem(null, I18N::getExpression("Empresa atividade venda editado com sucesso."));
            }
            catch(Exception $ex)
            {                
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionRemove($parameters = null)
        {
            $successulRemovalCount = 0;
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                foreach($parameters as $record)
                {
                    if(is_numeric($record->id))
                    {
                        $msg = $this->delete($record->id, true);
                        if($msg == null || $msg->ok())
                        {
                            $successulRemovalCount++;
                        }
                    }
                }
                
                if($successulRemovalCount > 0)
                {
                    $successMessage = count($successulRemovalCount) > 1 ? I18N::getExpression("{0} Empresa atividade venda foram removidos com sucesso.", $successulRemovalCount) : I18N::getExpression("Empresa atividade venda removido com sucesso.");
                    return new Mensagem(null, $successMessage);
                }
                else
                {
                    $errorMessage = count($parameters) > 1 ? I18N::getExpression("Falha ao remover Empresa atividade venda.") : I18N::getExpression("Falha ao remover Empresa atividade venda.");
                    return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, $errorMessage);                    
                }
            }
            catch (Exception $ex) 
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }

public function __getRecord($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }
            
            try
            {            
                $returnObject = new stdClass();            
                $whereClause = static::getWhereClauseForFilter($parameters->filterParameters);
                $query = "SELECT  id, empresa_venda_id_INT, quantidade_FLOAT, valor_total_FLOAT, desconto_FLOAT, corporacao_id_INT, cadastro_SEC, cadastro_OFFSEC, atividade_id_INT, empresa_id_INT, protocolo_INT FROM empresa_atividade_venda {$whereClause}";
    
                $msg = $this->database->queryMensagem($query);                
                if ($msg == null)
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Não foi encontrado nenhum registro"));
                    }                    
                    $resultSet = $this->database->result;
                    $resultObject = static::mapResultSetDataToRecord($resultSet);
                    
                    $returnObject->formData = $resultObject;
                    $returnObject->comboBoxesData = null;
                    if($parameters->loadComboBoxesData)
                    {
                        $this->setByObject($resultObject);
                        $returnObject->comboBoxesData =  $this->getFormComboBoxesData();
                    }                    
    
                    
    
                    return new Mensagem_generica($returnObject);
                } 
                else 
                {
                    return $msg;
                }
                
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }


        
        public function __getFilterComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
                
        public function __getFormComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
        
        protected function getFormComboBoxesData()
        {
            $comboBoxesData = new stdClass();
            $listParameters = new stdClass();
            $listParameters->listMappingType = static::LIST_MAPPING_TYPE_COMBOBOX;
            
            
                    
                    $objEmpresaVenda = new EXTDAO_Empresa_venda();
                    $comboBoxesData->fieldEmpresaVendaId = $objEmpresaVenda->__getList($listParameters);
                    
                    $objAtividade = new EXTDAO_Atividade();
                    $comboBoxesData->fieldAtividadeId = $objAtividade->__getList($listParameters);
                    
                    $objEmpresa = new EXTDAO_Empresa();
                    $comboBoxesData->fieldEmpresaId = $objEmpresa->__getList($listParameters);

            return $comboBoxesData;
        }
        
        


        
        public static function setListAliasTypes()
        {
            if(is_null(static::$listAliasTypes))
            {
                static::$listAliasTypes = new stdClass();
                static::$listAliasTypes->empresa_venda__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->atividade__nome = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->empresa__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->empresa_atividade_venda__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->empresa_atividade_venda__corporacao_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->empresa_atividade_venda__quantidade_FLOAT = static::TIPO_VARIAVEL_FLOAT;
static::$listAliasTypes->empresa_atividade_venda__valor_total_FLOAT = static::TIPO_VARIAVEL_FLOAT;
static::$listAliasTypes->empresa_atividade_venda__desconto_FLOAT = static::TIPO_VARIAVEL_FLOAT;
static::$listAliasTypes->empresa_atividade_venda__cadastro_SEC = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$listAliasTypes->empresa_atividade_venda__relatorio_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->empresa_atividade_venda__ind_celular_valido_BOOLEAN = static::TIPO_VARIAVEL_BOOLEAN;
            }        
        }
        
        public static function setListAliasRelatedAttributes()
        {
            if(is_null(static::$listAliasRelatedAttributes))
            {
                static::$listAliasRelatedAttributes = new stdClass();
                static::$listAliasRelatedAttributes->empresa_venda__id = "empresaVendaId";
static::$listAliasRelatedAttributes->atividade__nome = "atividadeNome";
static::$listAliasRelatedAttributes->empresa__id = "empresaId";
static::$listAliasRelatedAttributes->empresa_atividade_venda__id = "id";
static::$listAliasRelatedAttributes->empresa_atividade_venda__corporacao_id_INT = "corporacaoId";
static::$listAliasRelatedAttributes->empresa_atividade_venda__quantidade_FLOAT = "quantidadeFloat";
static::$listAliasRelatedAttributes->empresa_atividade_venda__valor_total_FLOAT = "valorTotalFloat";
static::$listAliasRelatedAttributes->empresa_atividade_venda__desconto_FLOAT = "descontoFloat";
static::$listAliasRelatedAttributes->empresa_atividade_venda__cadastro_SEC = "cadastroSec";
static::$listAliasRelatedAttributes->empresa_atividade_venda__relatorio_id_INT = "relatorioId";
static::$listAliasRelatedAttributes->empresa_atividade_venda__ind_celular_valido_BOOLEAN = "indCelularValidoBoolean";
            }         
        }

        public function __getList($parameters = null, $idCorporacao = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try
            {
                $filterParameters = new stdClass();
                if(isset($parameters->filterParameters))
                {
                    $filterParameters = $parameters->filterParameters;
                }
                                    
                            
            if(is_null($idCorporacao))
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            ListParameters::addFilterParameterToObject($filterParameters, "corporacaoId", $idCorporacao);           
                
                
                $sortingParameters = null;
                if(isset($parameters->sortingParameters))
                {
                    $sortingParameters = $parameters->sortingParameters;
                }
                
                $paginationParameters = null;
                if(isset($parameters->paginationParameters))
                {
                    $paginationParameters = $parameters->paginationParameters;
                }
                
                $listMappingType = static::getListMappingType($parameters->listMappingType);
                $mainTableAlias = "eav";

                $whereClause = static::getWhereClauseForFilter($filterParameters, $mainTableAlias);
                $orderByClause = static::getOrderByClauseForFilter($sortingParameters, $mainTableAlias);
                $limitClause = static::getLimitClauseForFilter($paginationParameters);

                $queryWithoutLimit = "SELECT eav.id FROM empresa_atividade_venda eav {$whereClause}";
                $query = "SELECT ev.id AS empresa_venda__id, a.nome AS atividade__nome, e.id AS empresa__id, eav.id AS empresa_atividade_venda__id, eav.corporacao_id_INT AS empresa_atividade_venda__corporacao_id_INT, eav.quantidade_FLOAT AS empresa_atividade_venda__quantidade_FLOAT, eav.valor_total_FLOAT AS empresa_atividade_venda__valor_total_FLOAT, eav.desconto_FLOAT AS empresa_atividade_venda__desconto_FLOAT, eav.cadastro_SEC AS empresa_atividade_venda__cadastro_SEC, eav.relatorio_id_INT AS empresa_atividade_venda__relatorio_id_INT, eav.ind_celular_valido_BOOLEAN AS empresa_atividade_venda__ind_celular_valido_BOOLEAN FROM empresa_atividade_venda eav LEFT JOIN empresa_venda ev ON ev.id = eav.empresa_venda_id_INT LEFT JOIN atividade a ON a.id = eav.atividade_id_INT LEFT JOIN empresa e ON e.id = eav.empresa_id_INT {$whereClause} {$orderByClause} {$limitClause}";

                $msg = $this->database->queryMensagem($queryWithoutLimit);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                
                $totalNumberOfRecords = $this->database->rows();
                if($totalNumberOfRecords == 0)
                {
                    return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                }                        
                        
                $msg = $this->database->queryMensagem($query);
                
                //operacao realizaca com sucesso
                if ($msg == null) 
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                    }                        
                
                    $resultSet = $this->database->result;

                    $objReturn = new stdClass();
                    $objReturn->dataSet = static::mapResultSetDataToList($resultSet, $listMappingType, static::$listAliasTypes);

                    if(!Helper::isNullOrEmpty($paginationParameters->recordsPerPage) && !Helper::isNullOrEmpty($paginationParameters->currentPage))
                    {
                        $lastPage = ceil($totalNumberOfRecords / $paginationParameters->recordsPerPage);
                        $objReturn->paginationParameters = new PaginationParams($paginationParameters->recordsPerPage, $paginationParameters->currentPage, $lastPage);
                    }
                    
                    return new Mensagem_generica($objReturn);
                } 
                else
                {
                    return $msg;
                }                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }



public function getId()
            {
                return $this->id;
            }

public function getEmpresaVendaId()
            {
                return $this->empresaVendaId;
            }

public function getEmpresa_venda_id_INT()
                {
                    return $this->empresaVendaId;
                }

public function getQuantidadeFloat()
            {
                return $this->quantidadeFloat;
            }

public function getQuantidade_FLOAT()
                {
                    return $this->quantidadeFloat;
                }

public function getValorTotalFloat()
            {
                return $this->valorTotalFloat;
            }

public function getValor_total_FLOAT()
                {
                    return $this->valorTotalFloat;
                }

public function getDescontoFloat()
            {
                return $this->descontoFloat;
            }

public function getDesconto_FLOAT()
                {
                    return $this->descontoFloat;
                }

public function getCorporacaoId()
            {
                return $this->corporacaoId;
            }

public function getCorporacao_id_INT()
                {
                    return $this->corporacaoId;
                }

public function getCadastroSec()
            {
                return $this->cadastroSec;
            }

public function getCadastro_SEC()
                {
                    return $this->cadastroSec;
                }

public function getCadastroOffsec()
            {
                return $this->cadastroOffsec;
            }

public function getCadastro_OFFSEC()
                {
                    return $this->cadastroOffsec;
                }

public function getAtividadeId()
            {
                return $this->atividadeId;
            }

public function getAtividade_id_INT()
                {
                    return $this->atividadeId;
                }

public function getEmpresaId()
            {
                return $this->empresaId;
            }

public function getEmpresa_id_INT()
                {
                    return $this->empresaId;
                }

public function getProtocoloInt()
            {
                return $this->protocoloInt;
            }

public function getProtocolo_INT()
                {
                    return $this->protocoloInt;
                }



function setId($value)
            {
                $this->id = $value;
            }

function setEmpresaVendaId($value)
            {
                $this->empresaVendaId = $value;
            }

public function setEmpresa_venda_id_INT($value)
                { 
                    $this->empresaVendaId = $value; 
                }

function setQuantidadeFloat($value)
            {
                $this->quantidadeFloat = $value;
            }

public function setQuantidade_FLOAT($value)
                { 
                    $this->quantidadeFloat = $value; 
                }

function setValorTotalFloat($value)
            {
                $this->valorTotalFloat = $value;
            }

public function setValor_total_FLOAT($value)
                { 
                    $this->valorTotalFloat = $value; 
                }

function setDescontoFloat($value)
            {
                $this->descontoFloat = $value;
            }

public function setDesconto_FLOAT($value)
                { 
                    $this->descontoFloat = $value; 
                }

function setCorporacaoId($value)
            {
                $this->corporacaoId = $value;
            }

public function setCorporacao_id_INT($value)
                { 
                    $this->corporacaoId = $value; 
                }

function setCadastroSec($value)
            {
                $this->cadastroSec = $value;
            }

public function setCadastro_SEC($value)
                { 
                    $this->cadastroSec = $value; 
                }

function setCadastroOffsec($value)
            {
                $this->cadastroOffsec = $value;
            }

public function setCadastro_OFFSEC($value)
                { 
                    $this->cadastroOffsec = $value; 
                }

function setAtividadeId($value)
            {
                $this->atividadeId = $value;
            }

public function setAtividade_id_INT($value)
                { 
                    $this->atividadeId = $value; 
                }

function setEmpresaId($value)
            {
                $this->empresaId = $value;
            }

public function setEmpresa_id_INT($value)
                { 
                    $this->empresaId = $value; 
                }

function setProtocoloInt($value)
            {
                $this->protocoloInt = $value;
            }

public function setProtocolo_INT($value)
                { 
                    $this->protocoloInt = $value; 
                }



public function clear()
        {$this->id = null;
$this->empresaVendaId = null;
if($this->objEmpresa_venda != null) unset($this->objEmpresa_venda);
$this->quantidadeFloat = null;
$this->valorTotalFloat = null;
$this->descontoFloat = null;
$this->corporacaoId = null;
if($this->objCorporacao != null) unset($this->objCorporacao);
$this->cadastroSec = null;
$this->cadastroOffsec = null;
$this->atividadeId = null;
if($this->objAtividade != null) unset($this->objAtividade);
$this->empresaId = null;
if($this->objEmpresa != null) unset($this->objEmpresa);
$this->protocoloInt = null;
if(is_array($this->arrEstadosCampos)){
                        unset($this->arrEstadosCampos);
                     $this->arrEstadosCampos = array();
                    }
                }

public function formatarParaSQL()
        {
if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->id)){
$this->id = $this->formatarIntegerParaComandoSQL($this->id);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->id);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->empresaVendaId)){
$this->empresaVendaId = $this->formatarIntegerParaComandoSQL($this->empresaVendaId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->empresaVendaId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->quantidadeFloat)){
$this->quantidadeFloat = $this->formatarFloatParaComandoSQL($this->quantidadeFloat);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->quantidadeFloat);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->valorTotalFloat)){
$this->valorTotalFloat = $this->formatarFloatParaComandoSQL($this->valorTotalFloat);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->valorTotalFloat);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->descontoFloat)){
$this->descontoFloat = $this->formatarFloatParaComandoSQL($this->descontoFloat);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->descontoFloat);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->corporacaoId)){
$this->corporacaoId = $this->formatarIntegerParaComandoSQL($this->corporacaoId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->corporacaoId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->cadastroSec)){
$this->cadastroSec = $this->formatarIntegerParaComandoSQL($this->cadastroSec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->cadastroSec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->cadastroOffsec)){
$this->cadastroOffsec = $this->formatarIntegerParaComandoSQL($this->cadastroOffsec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->cadastroOffsec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->atividadeId)){
$this->atividadeId = $this->formatarIntegerParaComandoSQL($this->atividadeId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->atividadeId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->empresaId)){
$this->empresaId = $this->formatarIntegerParaComandoSQL($this->empresaId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->empresaId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->protocoloInt)){
$this->protocoloInt = $this->formatarIntegerParaComandoSQL($this->protocoloInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->protocoloInt);
}

}

public function formatarParaExibicao()
        {if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->quantidadeFloat)){
$this->quantidadeFloat = $this->formatarFloatParaExibicao($this->quantidadeFloat);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->quantidadeFloat);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->valorTotalFloat)){
$this->valorTotalFloat = $this->formatarFloatParaExibicao($this->valorTotalFloat);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->valorTotalFloat);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->descontoFloat)){
$this->descontoFloat = $this->formatarFloatParaExibicao($this->descontoFloat);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->descontoFloat);
}

}

function select($id, $idCorporacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }   

            $sql = "SELECT id, empresa_venda_id_INT, quantidade_FLOAT, valor_total_FLOAT, desconto_FLOAT, corporacao_id_INT, cadastro_SEC, cadastro_OFFSEC, atividade_id_INT, empresa_id_INT, protocolo_INT FROM empresa_atividade_venda WHERE id = $id AND corporacao_id_INT = $idCorporacao";

            $msg = $this->database->queryMensagem($sql);
            $result = $this->database->result;

            if($msg != null && $msg->erro() )
            { 
            		$this->clear();
            		return $msg;
        	} 
        	else if($this->database->rows() == 0)
        	{
                return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nenhum registro foi encontrado."));
            }
        

            $row = $this->database->fetchArray(MYSQLI_NUM);
            
            		$this->id = $row[0];
		$this->empresaVendaId = $row[1];
		$this->quantidadeFloat = $row[2];
		$this->valorTotalFloat = $row[3];
		$this->descontoFloat = $row[4];
		$this->cadastroSec = $row[6];
		$this->cadastroOffsec = $row[7];
		$this->atividadeId = $row[8];
		$this->empresaId = $row[9];
		$this->protocoloInt = $row[10];

            
        }

public function delete($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (is_null($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }

            $sql = "DELETE FROM empresa_atividade_venda WHERE id= $id AND corporacao_id_INT = $idCorporacao";
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                    && $sincronizar
                        && static::idSistemaTabela() != null
                            && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_REMOVE,
                                null,
                                null,
                                $id,
                                $this->database);
            }

            return $msg;
        }

public function insert($sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null, $opcoes = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $this->defineDataCadastroInSecondsIfNotDefined();
$this->defineDataCadastroOffsetInSecondsIfNotDefined();

            
            if (is_null($opcoes))
            {
                $opcoes = $this->opcoesDAO;
            }

            if (!is_numeric($this->id))
            {
                $this->id = "null";
                if ($opcoes == null || $opcoes[Database::OPCAO_GERAR_ID] != false) 
                {
                    $this->id = EXTDAO_Sistema_sequencia::gerarId(static::nomeTabela());                    
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }    
                    else if($msg == null)
                    {
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
                else 
                {
                    $this->id = null;
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao));
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }
                    else if($msg == null)
                    {
                        $this->id = $this->database->getLastInsertId();
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
            }
            else 
            {
                $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }                        
                else if($msg == null)
                {
                   $msg = new Mensagem_token();
                   $msg->mValor = $this->id;
                }
            }
            if($msg == null || $msg->ok())
            {                
                if($sincronizar
                    && static::idSistemaTabela() != null
                    && isset($this->id)){
                    
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_INSERT,
                                null,
                                null,
                                $this->id,
                                $this->database); 
                	}
            } 
            else
            {
                $this->id = null;
            }
            
            return $msg;
            
        }

public function getSQLInsert($idCorporacao){ 

return "INSERT INTO empresa_atividade_venda (id, empresa_venda_id_INT, quantidade_FLOAT, valor_total_FLOAT, desconto_FLOAT, corporacao_id_INT, cadastro_SEC, cadastro_OFFSEC, atividade_id_INT, empresa_id_INT, protocolo_INT) VALUES ( $this->id ,  $this->empresaVendaId ,  $this->quantidadeFloat ,  $this->valorTotalFloat ,  $this->descontoFloat ,  $idCorporacao ,  $this->cadastroSec ,  $this->cadastroOffsec ,  $this->atividadeId ,  $this->empresaId ,  $this->protocoloInt ) "; 

}




        
        public function updateByParameters($id, $objParametros, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao))
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();

            
                
                if (isset($objParametros->empresaVendaId)) 
                {
                    $arrUpdateFields[] = " empresa_venda_id_INT = {$objParametros->empresaVendaId} ";
                }


                
                if (isset($objParametros->quantidadeFloat)) 
                {
                    $arrUpdateFields[] = " quantidade_FLOAT = {$objParametros->quantidadeFloat} ";
                }


                
                if (isset($objParametros->valorTotalFloat)) 
                {
                    $arrUpdateFields[] = " valor_total_FLOAT = {$objParametros->valorTotalFloat} ";
                }


                
                if (isset($objParametros->descontoFloat)) 
                {
                    $arrUpdateFields[] = " desconto_FLOAT = {$objParametros->descontoFloat} ";
                }


                
                if (isset($objParametros->cadastroSec)) 
                {
                    $arrUpdateFields[] = " cadastro_SEC = {$objParametros->cadastroSec} ";
                }


                
                if (isset($objParametros->cadastroOffsec)) 
                {
                    $arrUpdateFields[] = " cadastro_OFFSEC = {$objParametros->cadastroOffsec} ";
                }


                
                if (isset($objParametros->atividadeId)) 
                {
                    $arrUpdateFields[] = " atividade_id_INT = {$objParametros->atividadeId} ";
                }


                
                if (isset($objParametros->empresaId)) 
                {
                    $arrUpdateFields[] = " empresa_id_INT = {$objParametros->empresaId} ";
                }


                
                if (isset($objParametros->protocoloInt)) 
                {
                    $arrUpdateFields[] = " protocolo_INT = {$objParametros->protocoloInt} ";
                }



            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE empresa_atividade_venda SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";

            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null)
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function update($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($this->empresaVendaId)) 
                {                                      
                    $arrUpdateFields[] = " empresa_venda_id_INT = {$this->empresaVendaId} ";
                }


                
                if (isset($this->quantidadeFloat)) 
                {                                      
                    $arrUpdateFields[] = " quantidade_FLOAT = {$this->quantidadeFloat} ";
                }


                
                if (isset($this->valorTotalFloat)) 
                {                                      
                    $arrUpdateFields[] = " valor_total_FLOAT = {$this->valorTotalFloat} ";
                }


                
                if (isset($this->descontoFloat)) 
                {                                      
                    $arrUpdateFields[] = " desconto_FLOAT = {$this->descontoFloat} ";
                }


                
                if (isset($this->cadastroSec)) 
                {                                      
                    $arrUpdateFields[] = " cadastro_SEC = {$this->cadastroSec} ";
                }


                
                if (isset($this->cadastroOffsec)) 
                {                                      
                    $arrUpdateFields[] = " cadastro_OFFSEC = {$this->cadastroOffsec} ";
                }


                
                if (isset($this->atividadeId)) 
                {                                      
                    $arrUpdateFields[] = " atividade_id_INT = {$this->atividadeId} ";
                }


                
                if (isset($this->empresaId)) 
                {                                      
                    $arrUpdateFields[] = " empresa_id_INT = {$this->empresaId} ";
                }


                
                if (isset($this->protocoloInt)) 
                {                                      
                    $arrUpdateFields[] = " protocolo_INT = {$this->protocoloInt} ";
                }


            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE empresa_atividade_venda SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function updateByInstanceUsingParameters($id, $parameters, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($parameters->empresaVendaId)) 
                {                                      
                    $arrUpdateFields[] = " empresa_venda_id_INT = {$this->empresaVendaId} ";
                }
                
                if (isset($parameters->quantidadeFloat)) 
                {                                      
                    $arrUpdateFields[] = " quantidade_FLOAT = {$this->quantidadeFloat} ";
                }
                
                if (isset($parameters->valorTotalFloat)) 
                {                                      
                    $arrUpdateFields[] = " valor_total_FLOAT = {$this->valorTotalFloat} ";
                }
                
                if (isset($parameters->descontoFloat)) 
                {                                      
                    $arrUpdateFields[] = " desconto_FLOAT = {$this->descontoFloat} ";
                }
                
                if (isset($parameters->cadastroSec)) 
                {                                      
                    $arrUpdateFields[] = " cadastro_SEC = {$this->cadastroSec} ";
                }
                
                if (isset($parameters->cadastroOffsec)) 
                {                                      
                    $arrUpdateFields[] = " cadastro_OFFSEC = {$this->cadastroOffsec} ";
                }
                
                if (isset($parameters->atividadeId)) 
                {                                      
                    $arrUpdateFields[] = " atividade_id_INT = {$this->atividadeId} ";
                }
                
                if (isset($parameters->empresaId)) 
                {                                      
                    $arrUpdateFields[] = " empresa_id_INT = {$this->empresaId} ";
                }
                
                if (isset($parameters->protocoloInt)) 
                {                                      
                    $arrUpdateFields[] = " protocolo_INT = {$this->protocoloInt} ";
                }
            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE empresa_atividade_venda SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }

}

?>
