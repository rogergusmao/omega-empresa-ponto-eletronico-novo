<?php

//include_once '../recursos/php/configuracao_corporacao_config_dinamico.php';
require_once '../recursos/php/configuracao_biblioteca_compartilhada.php';

require_once $workspaceRaiz . DIRETORIO_BIBLIOTECAS_COMPARTILHADAS . '/imports/header_json.php';

ConfiguracaoCorporacaoDinamica::init(getWebservicesSemCache());

require_once '../recursos/php/funcoes.php';

class GerarIdAvulso extends InterfaceScript
{
    public function __construct()
    {
        parent::__construct(true);
    }

    public function render()
    {
        try
        {
            $id = EXTDAO_Sistema_sequencia::gerarId(
                Helper::POSTGET("tabela"),
                Helper::POSTGET("tot_gerar"));
            if ($id == null)
            {
                $msg = new Mensagem(PROTOCOLO_SISTEMA::SERVIDOR_OCUPADO, null, $id);
            }
            else
            {
                $msg = new Mensagem_token(null, null, $id);
            }
            $strRet = json_encode($msg);
            echo $strRet;
            HelperLog::verbose($strRet);

            HelperLog::webservice($strRet);

            Database::closeAll();
            HelperRedis::closeAll();
        }
        catch (Exception $exc)
        {

            Database::closeAll();
            HelperRedis::closeAll();

            $msg = new Mensagem(null, null, $exc);
            $log = Registry::get('HelperLog');
            $log->gravarLogEmCasoErro($msg);
            $strRet = json_encode($msg);
            echo $strRet;

            HelperLog::verbose(null, $exc);
        }
    }
}

$script = new GerarIdAvulso();
$script->render();

require_once '../recursos/php/on_finish.php';
