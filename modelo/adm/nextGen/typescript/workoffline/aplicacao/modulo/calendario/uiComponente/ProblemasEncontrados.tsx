import * as React from 'react';
import { Label, Alert } from 'react-bootstrap';

import * as FullCalendar from 'fullcalendar';
import * as FullCalendarInputTypes from 'fullcalendar/src/types/input-types';

import { ReactProps } from 'lib/geral';
import { ReactComponent, DomContainer } from 'lib/uiComponente';
import { Pessoa, Evento, ProblemaEncontrado } from 'aplicacao/modulo/calendario/model';

export class ProblemasEncontrados extends ReactComponent<ProblemasEncontrados.ProblemasEncontradosProps, ProblemasEncontrados.ProblemasEncontradosState>
{
    private dadosCalendario :Array<Pessoa>;

    public constructor(props: ProblemasEncontrados.ProblemasEncontradosProps, reactContext: any)
    { 
        super(props, reactContext);  
        this.dadosCalendario = new Array<Pessoa>();   
    }

    public fixLayout()
    {
        var maxHeight: number = DomContainer.getMaxAllowedHeight();            
    }

    public setData(pessoas :Array<Pessoa>)
    {
        this.dadosCalendario = pessoas;
        this.setState({ dadosAtualizados: true });
    }

    public render()
    {
        let problemasEncontrados = new Array<ProblemaEncontrado>();
        if(this.dadosCalendario.length > 0)
        {            
            for(let pessoa of this.dadosCalendario)
            {
                for(let evento of pessoa.eventos)
                {
                    //caso o evento não tenha ponto final, adiciona como problema
                    if(evento.pontoFinal == null)
                    {
                        let problemaEncontrado = new ProblemaEncontrado();
                        problemaEncontrado.id = evento.pontoInicial.idPonto;
                        problemaEncontrado.nomePessoa = evento.pontoInicial.nomePessoa;
                        problemaEncontrado.cor = evento.corBackgroundEvento;
                        problemaEncontrado.eventoAssociado = evento;
                        problemasEncontrados.push(problemaEncontrado);
                    }
                }
            }
        }
        
        if(problemasEncontrados.length > 0)
        {
            return (                     
                <div className="row">
                    <div className="col-sm-12">  
                        {problemasEncontrados.map((value) => (  
                            <div key={value.id} 
                                 className="box-content box-statistic problema-encontrado-item" 
                                 onClick={() => this.props.onProblemaClick(value.eventoAssociado)}>
                                <h3 className="title" style={ { color: value.cor.getHexValue() } }>{value.getTituloProblema()}</h3>
                                <small>{value.nomePessoa}</small><br />
                                <small>{value.getDescricaoProblema()}</small>
                                <div className="icon-warning-sign align-right" style={ { color: value.cor.getHexValue() } }></div>
                            </div> 
                        )) }  
                    </div>
                </div>                 
            );
        }
        else
        {
            return (<Alert bsStyle="warning" className="mb-0">
                        <strong></strong> Nenhum problema encontrado no período
                    </Alert>);
        }
        
        
    }

}

export namespace ProblemasEncontrados
{
    export interface ProblemasEncontradosProps extends ReactProps
    {
        onProblemaClick: (evento: Evento) => void;
    }

    export interface ProblemasEncontradosState
    {
        dadosAtualizados :boolean;
    }

}