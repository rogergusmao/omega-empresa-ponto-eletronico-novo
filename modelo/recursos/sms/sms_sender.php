<?php

session_name("SMS_SENDER");
session_start();

include_once '../php/constants.php';
include_once '../php/funcoes.php';
include_once 'sms_config.php';

$objCrypt = new Crypt();

if (isset($_GET["msg"]) && isset($_GET["telefone"]) && isset($_GET["cliente"])) {

    $mensagem = str_replace("\n", "", urldecode(Helper::GET("msg")));
    $destinatario = $objCrypt->decrypt(Helper::GET("telefone"));
    $cliente = Helper::GET("cliente");

    if (isset($arrPortas[$cliente])) {

        $porta = $arrPortas[$cliente];

        if (is_numeric($destinatario) && strlen($destinatario) == 10) {

            $saida = array();
            $codigoSaida = 0;

            $comando = "omegasms -telefone={$destinatario} -mensagem=\"{$mensagem}\" -porta={$porta}";

            exec("{$comando}", $saida, $codigoSaida);

            if (count($saida) > 0) {

                if (substr_count($saida[0], "ERRO") > 0) {

                    echo "Erro: O envio da mensagem falhou.\nComando:{$comando}\nCelular: {$destinatario}\nMensagem: {$mensagem}\nPorta: {$porta}\nCliente:{$cliente}\nErro retornado: {$saida[0]}";

                }

            }

        } else {

            echo "Erro: destinatário inválido";

        }

    }

} else {

    echo "Erro: cliente inválido";

}


?>
