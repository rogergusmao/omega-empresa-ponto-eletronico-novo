<?php


        /*

        Arquivo gerado através de gerador de código em 12/05/2018 as 14:28:41.
        Para que o arquivo não seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

        Tabela correspondente: empresa_compra
        Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

        */
        


class DAO_Empresa_compra extends Generic_DAO
                        {
                            const NOME_ENTIDADE = "empresa_compra";

                            public static $nomeTabela = null;
                            public static $idSistemaTabela = false;
                            public static $databaseFieldsRelatedAttributes = null;
	                        public static $databaseFieldTypes = null;
	                        public static $databaseFieldNames = null;
	                        public static $listAliasTypes = null;
	                        public static $listAliasRelatedAttributes = null;
	                        
	                        public $id;
public $formaPagamentoId;
public $valorTotalFloat;
public $dataSec;
public $dataOffsec;
public $usuarioId;
public $minhaEmpresaId;
public $outraEmpresaId;
public $corporacaoId;
public $vencimentoSec;
public $vencimentoOffsec;
public $relatorioId;
public $registroEstadoId;
public $registroEstadoCorporacaoId;
public $fechamentoSec;
public $fechamentoOffsec;
public $valorPagoFloat;
public $descontoFloat;
public $protocoloInt;

public $labelId;
public $labelFormaPagamentoId;
public $labelValorTotalFloat;
public $labelDataSec;
public $labelDataOffsec;
public $labelUsuarioId;
public $labelMinhaEmpresaId;
public $labelOutraEmpresaId;
public $labelCorporacaoId;
public $labelVencimentoSec;
public $labelVencimentoOffsec;
public $labelRelatorioId;
public $labelRegistroEstadoId;
public $labelRegistroEstadoCorporacaoId;
public $labelFechamentoSec;
public $labelFechamentoOffsec;
public $labelValorPagoFloat;
public $labelDescontoFloat;
public $labelProtocoloInt;

public static $arrPK = array("id");

public function __construct($configDAO = null)
        {
            parent::__construct($configDAO);
            static::__setStaticValues($this->database);           
        }

public static function idSistemaTabela($databaseObject = null){
            if(!(static::$idSistemaTabela))
            {
                static::$idSistemaTabela = EXTDAO_Sistema_tabela::getIdDaSistemaTabela(static::nomeTabela(), $databaseObject);
            }
            return static::$idSistemaTabela;
        }

public function getIdSistemaTabela(){
            return static::idSistemaTabela();
        }

public static function nomeTabela(){
            if(is_null(static::$nomeTabela))
            {
                static::$nomeTabela = "empresa_compra";
            }
            return static::$nomeTabela;
        }

public function getNomeTabela(){
            return static::nomeTabela();
        }

public static function __setStaticValues($databaseObject = null)
                   {    
                        static::nomeTabela();
                        
                        static::idSistemaTabela($databaseObject);
                   
                        static::setDatabaseFieldNames();
                        static::setDatabaseFieldTypes();
                        static::setDatabaseFieldsRelatedAttributeNames();
                        
                        static::setListAliasTypes();
                        static::setListAliasRelatedAttributes();
                   }

public function getExpressaoLabel()
                   {
                        return "{id}";
                   }

public static function setDatabaseFieldNames()
        {
            if(is_null(static::$databaseFieldNames))
            {
                static::$databaseFieldNames = new stdClass();
                static::$databaseFieldNames->id = "id";
static::$databaseFieldNames->formaPagamentoId = "forma_pagamento_id_INT";
static::$databaseFieldNames->valorTotalFloat = "valor_total_FLOAT";
static::$databaseFieldNames->dataSec = "data_SEC";
static::$databaseFieldNames->dataOffsec = "data_OFFSEC";
static::$databaseFieldNames->usuarioId = "usuario_id_INT";
static::$databaseFieldNames->minhaEmpresaId = "minha_empresa_id_INT";
static::$databaseFieldNames->outraEmpresaId = "outra_empresa_id_INT";
static::$databaseFieldNames->corporacaoId = "corporacao_id_INT";
static::$databaseFieldNames->vencimentoSec = "vencimento_SEC";
static::$databaseFieldNames->vencimentoOffsec = "vencimento_OFFSEC";
static::$databaseFieldNames->relatorioId = "relatorio_id_INT";
static::$databaseFieldNames->registroEstadoId = "registro_estado_id_INT";
static::$databaseFieldNames->registroEstadoCorporacaoId = "registro_estado_corporacao_id_INT";
static::$databaseFieldNames->fechamentoSec = "fechamento_SEC";
static::$databaseFieldNames->fechamentoOffsec = "fechamento_OFFSEC";
static::$databaseFieldNames->valorPagoFloat = "valor_pago_FLOAT";
static::$databaseFieldNames->descontoFloat = "desconto_FLOAT";
static::$databaseFieldNames->protocoloInt = "protocolo_INT";

            }
                      
        }

public static function setDatabaseFieldTypes()
        {
            if(is_null(static::$databaseFieldTypes))
            {
                static::$databaseFieldTypes = new stdClass();
                static::$databaseFieldTypes->id = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->formaPagamentoId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->valorTotalFloat = static::TIPO_VARIAVEL_FLOAT;
static::$databaseFieldTypes->dataSec = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$databaseFieldTypes->dataOffsec = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$databaseFieldTypes->usuarioId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->minhaEmpresaId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->outraEmpresaId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->corporacaoId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->vencimentoSec = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$databaseFieldTypes->vencimentoOffsec = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$databaseFieldTypes->relatorioId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->registroEstadoId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->registroEstadoCorporacaoId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->fechamentoSec = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$databaseFieldTypes->fechamentoOffsec = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$databaseFieldTypes->valorPagoFloat = static::TIPO_VARIAVEL_FLOAT;
static::$databaseFieldTypes->descontoFloat = static::TIPO_VARIAVEL_FLOAT;
static::$databaseFieldTypes->protocoloInt = static::TIPO_VARIAVEL_INTEGER;

            }
            
        }

public static function setDatabaseFieldsRelatedAttributeNames()
        {
            if(is_null(static::$databaseFieldsRelatedAttributes))
            {
                static::$databaseFieldsRelatedAttributes = new stdClass();
                static::$databaseFieldsRelatedAttributes->id = "id";
static::$databaseFieldsRelatedAttributes->forma_pagamento_id_INT = "formaPagamentoId";
static::$databaseFieldsRelatedAttributes->valor_total_FLOAT = "valorTotalFloat";
static::$databaseFieldsRelatedAttributes->data_SEC = "dataSec";
static::$databaseFieldsRelatedAttributes->data_OFFSEC = "dataOffsec";
static::$databaseFieldsRelatedAttributes->usuario_id_INT = "usuarioId";
static::$databaseFieldsRelatedAttributes->minha_empresa_id_INT = "minhaEmpresaId";
static::$databaseFieldsRelatedAttributes->outra_empresa_id_INT = "outraEmpresaId";
static::$databaseFieldsRelatedAttributes->corporacao_id_INT = "corporacaoId";
static::$databaseFieldsRelatedAttributes->vencimento_SEC = "vencimentoSec";
static::$databaseFieldsRelatedAttributes->vencimento_OFFSEC = "vencimentoOffsec";
static::$databaseFieldsRelatedAttributes->relatorio_id_INT = "relatorioId";
static::$databaseFieldsRelatedAttributes->registro_estado_id_INT = "registroEstadoId";
static::$databaseFieldsRelatedAttributes->registro_estado_corporacao_id_INT = "registroEstadoCorporacaoId";
static::$databaseFieldsRelatedAttributes->fechamento_SEC = "fechamentoSec";
static::$databaseFieldsRelatedAttributes->fechamento_OFFSEC = "fechamentoOffsec";
static::$databaseFieldsRelatedAttributes->valor_pago_FLOAT = "valorPagoFloat";
static::$databaseFieldsRelatedAttributes->desconto_FLOAT = "descontoFloat";
static::$databaseFieldsRelatedAttributes->protocolo_INT = "protocoloInt";

            }           
        }

public function setAllFieldsToHumanFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["forma_pagamento_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["valor_total_FLOAT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["data_SEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["data_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["usuario_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["minha_empresa_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["outra_empresa_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["vencimento_SEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["vencimento_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["relatorio_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["registro_estado_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["registro_estado_corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["fechamento_SEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["fechamento_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["valor_pago_FLOAT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["desconto_FLOAT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["protocolo_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
}

public function setAllFieldsToDatabaseFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["forma_pagamento_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["valor_total_FLOAT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["data_SEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["data_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["usuario_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["minha_empresa_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["outra_empresa_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["vencimento_SEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["vencimento_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["relatorio_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["registro_estado_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["registro_estado_corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["fechamento_SEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["fechamento_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["valor_pago_FLOAT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["desconto_FLOAT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["protocolo_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
}

  public function getFkObjForma_pagamento() 
                {
                    if($this->objForma_pagamento == null)
                    {                        
                        $this->objForma_pagamento = new EXTDAO_Forma_pagamento_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getForma_pagamento_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objForma_pagamento->clear();
                    }
                    elseif($this->objForma_pagamento->getId() != $idFK)
                    {
                        $this->objForma_pagamento->select($idFK);
                    }
                    return $this->objForma_pagamento;
                }
  public function getFkObjUsuario() 
                {
                    if($this->objUsuario == null)
                    {                        
                        $this->objUsuario = new EXTDAO_Usuario_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getUsuario_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objUsuario->clear();
                    }
                    elseif($this->objUsuario->getId() != $idFK)
                    {
                        $this->objUsuario->select($idFK);
                    }
                    return $this->objUsuario;
                }
  public function getFkObjMinha_empresa() 
                {
                    if($this->objMinha_empresa == null)
                    {                        
                        $this->objMinha_empresa = new EXTDAO_Minha_empresa_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getMinha_empresa_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objMinha_empresa->clear();
                    }
                    elseif($this->objMinha_empresa->getId() != $idFK)
                    {
                        $this->objMinha_empresa->select($idFK);
                    }
                    return $this->objMinha_empresa;
                }
  public function getFkObjOutra_empresa() 
                {
                    if($this->objOutra_empresa == null)
                    {                        
                        $this->objOutra_empresa = new EXTDAO_Outra_empresa_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getOutra_empresa_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objOutra_empresa->clear();
                    }
                    elseif($this->objOutra_empresa->getId() != $idFK)
                    {
                        $this->objOutra_empresa->select($idFK);
                    }
                    return $this->objOutra_empresa;
                }
  public function getFkObjCorporacao() 
                {
                    if($this->objCorporacao == null)
                    {                        
                        $this->objCorporacao = new EXTDAO_Corporacao_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getCorporacao_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objCorporacao->clear();
                    }
                    elseif($this->objCorporacao->getId() != $idFK)
                    {
                        $this->objCorporacao->select($idFK);
                    }
                    return $this->objCorporacao;
                }
  public function getFkObjRelatorio() 
                {
                    if($this->objRelatorio == null)
                    {                        
                        $this->objRelatorio = new EXTDAO_Relatorio_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getRelatorio_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objRelatorio->clear();
                    }
                    elseif($this->objRelatorio->getId() != $idFK)
                    {
                        $this->objRelatorio->select($idFK);
                    }
                    return $this->objRelatorio;
                }
  public function getFkObjRegistro_estado() 
                {
                    if($this->objRegistro_estado == null)
                    {                        
                        $this->objRegistro_estado = new EXTDAO_Registro_estado_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getRegistro_estado_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objRegistro_estado->clear();
                    }
                    elseif($this->objRegistro_estado->getId() != $idFK)
                    {
                        $this->objRegistro_estado->select($idFK);
                    }
                    return $this->objRegistro_estado;
                }
  public function getFkObjRegistro_estado_corporacao() 
                {
                    if($this->objRegistro_estado_corporacao == null)
                    {                        
                        $this->objRegistro_estado_corporacao = new EXTDAO_Registro_estado_corporacao_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getRegistro_estado_corporacao_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objRegistro_estado_corporacao->clear();
                    }
                    elseif($this->objRegistro_estado_corporacao->getId() != $idFK)
                    {
                        $this->objRegistro_estado_corporacao->select($idFK);
                    }
                    return $this->objRegistro_estado_corporacao;
                }


public function setLabels()
        {$this->labelId = "id";
$this->labelFormaPagamentoId = "formapagamentoidINT";
$this->labelValorTotalFloat = "valortotalFLOAT";
$this->labelDataSec = "";
$this->labelDataOffsec = "";
$this->labelUsuarioId = "usuarioidINT";
$this->labelMinhaEmpresaId = "minhaempresaidINT";
$this->labelOutraEmpresaId = "outraempresaidINT";
$this->labelCorporacaoId = "corporacaoidINT";
$this->labelVencimentoSec = "";
$this->labelVencimentoOffsec = "";
$this->labelRelatorioId = "";
$this->labelRegistroEstadoId = "";
$this->labelRegistroEstadoCorporacaoId = "";
$this->labelFechamentoSec = "";
$this->labelFechamentoOffsec = "";
$this->labelValorPagoFloat = "";
$this->labelDescontoFloat = "";
$this->labelProtocoloInt = "";
}

public function __actionAdd($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                $this->setByObject($parameters);
                $this->setDataCadastroAndOffsetInSecondsFromUser($parameters);
                
                $this->formatarParaSQL();
                $msg = $this->insert(true);
                
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                return new Mensagem(null, I18N::getExpression("Empresa compra adicionado com sucesso."));
                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionEdit($parameters = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try {

                $this->setByObject($parameters);
                $this->setAllFieldsToHumanFormat();
                
                $this->formatarParaSQL();
                $msg = $this->updateByInstanceUsingParameters($this->id, $parameters, true);
                if($msg != null && $msg->erro())
                {
                     return $msg;
                }
        
                return new Mensagem(null, I18N::getExpression("Empresa compra editado com sucesso."));
            }
            catch(Exception $ex)
            {                
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionRemove($parameters = null)
        {
            $successulRemovalCount = 0;
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                foreach($parameters as $record)
                {
                    if(is_numeric($record->id))
                    {
                        $msg = $this->delete($record->id, true);
                        if($msg == null || $msg->ok())
                        {
                            $successulRemovalCount++;
                        }
                    }
                }
                
                if($successulRemovalCount > 0)
                {
                    $successMessage = count($successulRemovalCount) > 1 ? I18N::getExpression("{0} Empresa compra foram removidos com sucesso.", $successulRemovalCount) : I18N::getExpression("Empresa compra removido com sucesso.");
                    return new Mensagem(null, $successMessage);
                }
                else
                {
                    $errorMessage = count($parameters) > 1 ? I18N::getExpression("Falha ao remover Empresa compra.") : I18N::getExpression("Falha ao remover Empresa compra.");
                    return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, $errorMessage);                    
                }
            }
            catch (Exception $ex) 
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }

public function __getRecord($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }
            
            try
            {            
                $returnObject = new stdClass();            
                $whereClause = static::getWhereClauseForFilter($parameters->filterParameters);
                $query = "SELECT  id, forma_pagamento_id_INT, valor_total_FLOAT, data_SEC, data_OFFSEC, usuario_id_INT, minha_empresa_id_INT, outra_empresa_id_INT, corporacao_id_INT, vencimento_SEC, vencimento_OFFSEC, relatorio_id_INT, registro_estado_id_INT, registro_estado_corporacao_id_INT, fechamento_SEC, fechamento_OFFSEC, valor_pago_FLOAT, desconto_FLOAT, protocolo_INT FROM empresa_compra {$whereClause}";
    
                $msg = $this->database->queryMensagem($query);                
                if ($msg == null)
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Não foi encontrado nenhum registro"));
                    }                    
                    $resultSet = $this->database->result;
                    $resultObject = static::mapResultSetDataToRecord($resultSet);
                    
                    $returnObject->formData = $resultObject;
                    $returnObject->comboBoxesData = null;
                    if($parameters->loadComboBoxesData)
                    {
                        $this->setByObject($resultObject);
                        $returnObject->comboBoxesData =  $this->getFormComboBoxesData();
                    }                    
    
                    
    
                    return new Mensagem_generica($returnObject);
                } 
                else 
                {
                    return $msg;
                }
                
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }


        
        public function __getFilterComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
                
        public function __getFormComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
        
        protected function getFormComboBoxesData()
        {
            $comboBoxesData = new stdClass();
            $listParameters = new stdClass();
            $listParameters->listMappingType = static::LIST_MAPPING_TYPE_COMBOBOX;
            
            
                    
                    $objFormaPagamento = new EXTDAO_Forma_pagamento();
                    $comboBoxesData->fieldFormaPagamentoId = $objFormaPagamento->__getList($listParameters);
                    
                    $objUsuario = new EXTDAO_Usuario();
                    $comboBoxesData->fieldUsuarioId = $objUsuario->__getList($listParameters);

            return $comboBoxesData;
        }
        
        


        
        public static function setListAliasTypes()
        {
            if(is_null(static::$listAliasTypes))
            {
                static::$listAliasTypes = new stdClass();
                static::$listAliasTypes->forma_pagamento__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->usuario__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->empresa_compra__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->empresa_compra__corporacao_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->empresa_compra__valor_total_FLOAT = static::TIPO_VARIAVEL_FLOAT;
static::$listAliasTypes->empresa_compra__data_SEC = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$listAliasTypes->empresa_compra__data_OFFSEC = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$listAliasTypes->empresa_compra__cadastro_OFFSEC = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$listAliasTypes->empresa_compra__minha_empresa_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->empresa_compra__outra_empresa_id_INT = static::TIPO_VARIAVEL_INTEGER;
            }        
        }
        
        public static function setListAliasRelatedAttributes()
        {
            if(is_null(static::$listAliasRelatedAttributes))
            {
                static::$listAliasRelatedAttributes = new stdClass();
                static::$listAliasRelatedAttributes->forma_pagamento__id = "formaPagamentoId";
static::$listAliasRelatedAttributes->usuario__id = "usuarioId";
static::$listAliasRelatedAttributes->empresa_compra__id = "id";
static::$listAliasRelatedAttributes->empresa_compra__corporacao_id_INT = "corporacaoId";
static::$listAliasRelatedAttributes->empresa_compra__valor_total_FLOAT = "valorTotalFloat";
static::$listAliasRelatedAttributes->empresa_compra__data_SEC = "dataSec";
static::$listAliasRelatedAttributes->empresa_compra__data_OFFSEC = "dataOffsec";
static::$listAliasRelatedAttributes->empresa_compra__cadastro_OFFSEC = "cadastroOffsec";
static::$listAliasRelatedAttributes->empresa_compra__minha_empresa_id_INT = "minhaEmpresaId";
static::$listAliasRelatedAttributes->empresa_compra__outra_empresa_id_INT = "outraEmpresaId";
            }         
        }

        public function __getList($parameters = null, $idCorporacao = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try
            {
                $filterParameters = new stdClass();
                if(isset($parameters->filterParameters))
                {
                    $filterParameters = $parameters->filterParameters;
                }
                                    
                            
            if(is_null($idCorporacao))
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            ListParameters::addFilterParameterToObject($filterParameters, "corporacaoId", $idCorporacao);           
                
                
                $sortingParameters = null;
                if(isset($parameters->sortingParameters))
                {
                    $sortingParameters = $parameters->sortingParameters;
                }
                
                $paginationParameters = null;
                if(isset($parameters->paginationParameters))
                {
                    $paginationParameters = $parameters->paginationParameters;
                }
                
                $listMappingType = static::getListMappingType($parameters->listMappingType);
                $mainTableAlias = "ec";

                $whereClause = static::getWhereClauseForFilter($filterParameters, $mainTableAlias);
                $orderByClause = static::getOrderByClauseForFilter($sortingParameters, $mainTableAlias);
                $limitClause = static::getLimitClauseForFilter($paginationParameters);

                $queryWithoutLimit = "SELECT ec.id FROM empresa_compra ec {$whereClause}";
                $query = "SELECT fp.id AS forma_pagamento__id, u.id AS usuario__id, ec.id AS empresa_compra__id, ec.corporacao_id_INT AS empresa_compra__corporacao_id_INT, ec.valor_total_FLOAT AS empresa_compra__valor_total_FLOAT, ec.data_SEC AS empresa_compra__data_SEC, ec.data_OFFSEC AS empresa_compra__data_OFFSEC, ec.cadastro_OFFSEC AS empresa_compra__cadastro_OFFSEC, ec.minha_empresa_id_INT AS empresa_compra__minha_empresa_id_INT, ec.outra_empresa_id_INT AS empresa_compra__outra_empresa_id_INT FROM empresa_compra ec LEFT JOIN forma_pagamento fp ON fp.id = ec.forma_pagamento_id_INT LEFT JOIN usuario u ON u.id = ec.usuario_id_INT {$whereClause} {$orderByClause} {$limitClause}";

                $msg = $this->database->queryMensagem($queryWithoutLimit);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                
                $totalNumberOfRecords = $this->database->rows();
                if($totalNumberOfRecords == 0)
                {
                    return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                }                        
                        
                $msg = $this->database->queryMensagem($query);
                
                //operacao realizaca com sucesso
                if ($msg == null) 
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                    }                        
                
                    $resultSet = $this->database->result;

                    $objReturn = new stdClass();
                    $objReturn->dataSet = static::mapResultSetDataToList($resultSet, $listMappingType, static::$listAliasTypes);

                    if(!Helper::isNullOrEmpty($paginationParameters->recordsPerPage) && !Helper::isNullOrEmpty($paginationParameters->currentPage))
                    {
                        $lastPage = ceil($totalNumberOfRecords / $paginationParameters->recordsPerPage);
                        $objReturn->paginationParameters = new PaginationParams($paginationParameters->recordsPerPage, $paginationParameters->currentPage, $lastPage);
                    }
                    
                    return new Mensagem_generica($objReturn);
                } 
                else
                {
                    return $msg;
                }                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }



public function getId()
            {
                return $this->id;
            }

public function getFormaPagamentoId()
            {
                return $this->formaPagamentoId;
            }

public function getForma_pagamento_id_INT()
                {
                    return $this->formaPagamentoId;
                }

public function getValorTotalFloat()
            {
                return $this->valorTotalFloat;
            }

public function getValor_total_FLOAT()
                {
                    return $this->valorTotalFloat;
                }

public function getDataSec()
            {
                return $this->dataSec;
            }

public function getData_SEC()
                {
                    return $this->dataSec;
                }

public function getDataOffsec()
            {
                return $this->dataOffsec;
            }

public function getData_OFFSEC()
                {
                    return $this->dataOffsec;
                }

public function getUsuarioId()
            {
                return $this->usuarioId;
            }

public function getUsuario_id_INT()
                {
                    return $this->usuarioId;
                }

public function getMinhaEmpresaId()
            {
                return $this->minhaEmpresaId;
            }

public function getMinha_empresa_id_INT()
                {
                    return $this->minhaEmpresaId;
                }

public function getOutraEmpresaId()
            {
                return $this->outraEmpresaId;
            }

public function getOutra_empresa_id_INT()
                {
                    return $this->outraEmpresaId;
                }

public function getCorporacaoId()
            {
                return $this->corporacaoId;
            }

public function getCorporacao_id_INT()
                {
                    return $this->corporacaoId;
                }

public function getVencimentoSec()
            {
                return $this->vencimentoSec;
            }

public function getVencimento_SEC()
                {
                    return $this->vencimentoSec;
                }

public function getVencimentoOffsec()
            {
                return $this->vencimentoOffsec;
            }

public function getVencimento_OFFSEC()
                {
                    return $this->vencimentoOffsec;
                }

public function getRelatorioId()
            {
                return $this->relatorioId;
            }

public function getRelatorio_id_INT()
                {
                    return $this->relatorioId;
                }

public function getRegistroEstadoId()
            {
                return $this->registroEstadoId;
            }

public function getRegistro_estado_id_INT()
                {
                    return $this->registroEstadoId;
                }

public function getRegistroEstadoCorporacaoId()
            {
                return $this->registroEstadoCorporacaoId;
            }

public function getRegistro_estado_corporacao_id_INT()
                {
                    return $this->registroEstadoCorporacaoId;
                }

public function getFechamentoSec()
            {
                return $this->fechamentoSec;
            }

public function getFechamento_SEC()
                {
                    return $this->fechamentoSec;
                }

public function getFechamentoOffsec()
            {
                return $this->fechamentoOffsec;
            }

public function getFechamento_OFFSEC()
                {
                    return $this->fechamentoOffsec;
                }

public function getValorPagoFloat()
            {
                return $this->valorPagoFloat;
            }

public function getValor_pago_FLOAT()
                {
                    return $this->valorPagoFloat;
                }

public function getDescontoFloat()
            {
                return $this->descontoFloat;
            }

public function getDesconto_FLOAT()
                {
                    return $this->descontoFloat;
                }

public function getProtocoloInt()
            {
                return $this->protocoloInt;
            }

public function getProtocolo_INT()
                {
                    return $this->protocoloInt;
                }



function setId($value)
            {
                $this->id = $value;
            }

function setFormaPagamentoId($value)
            {
                $this->formaPagamentoId = $value;
            }

public function setForma_pagamento_id_INT($value)
                { 
                    $this->formaPagamentoId = $value; 
                }

function setValorTotalFloat($value)
            {
                $this->valorTotalFloat = $value;
            }

public function setValor_total_FLOAT($value)
                { 
                    $this->valorTotalFloat = $value; 
                }

function setDataSec($value)
            {
                $this->dataSec = $value;
            }

public function setData_SEC($value)
                { 
                    $this->dataSec = $value; 
                }

function setDataOffsec($value)
            {
                $this->dataOffsec = $value;
            }

public function setData_OFFSEC($value)
                { 
                    $this->dataOffsec = $value; 
                }

function setUsuarioId($value)
            {
                $this->usuarioId = $value;
            }

public function setUsuario_id_INT($value)
                { 
                    $this->usuarioId = $value; 
                }

function setMinhaEmpresaId($value)
            {
                $this->minhaEmpresaId = $value;
            }

public function setMinha_empresa_id_INT($value)
                { 
                    $this->minhaEmpresaId = $value; 
                }

function setOutraEmpresaId($value)
            {
                $this->outraEmpresaId = $value;
            }

public function setOutra_empresa_id_INT($value)
                { 
                    $this->outraEmpresaId = $value; 
                }

function setCorporacaoId($value)
            {
                $this->corporacaoId = $value;
            }

public function setCorporacao_id_INT($value)
                { 
                    $this->corporacaoId = $value; 
                }

function setVencimentoSec($value)
            {
                $this->vencimentoSec = $value;
            }

public function setVencimento_SEC($value)
                { 
                    $this->vencimentoSec = $value; 
                }

function setVencimentoOffsec($value)
            {
                $this->vencimentoOffsec = $value;
            }

public function setVencimento_OFFSEC($value)
                { 
                    $this->vencimentoOffsec = $value; 
                }

function setRelatorioId($value)
            {
                $this->relatorioId = $value;
            }

public function setRelatorio_id_INT($value)
                { 
                    $this->relatorioId = $value; 
                }

function setRegistroEstadoId($value)
            {
                $this->registroEstadoId = $value;
            }

public function setRegistro_estado_id_INT($value)
                { 
                    $this->registroEstadoId = $value; 
                }

function setRegistroEstadoCorporacaoId($value)
            {
                $this->registroEstadoCorporacaoId = $value;
            }

public function setRegistro_estado_corporacao_id_INT($value)
                { 
                    $this->registroEstadoCorporacaoId = $value; 
                }

function setFechamentoSec($value)
            {
                $this->fechamentoSec = $value;
            }

public function setFechamento_SEC($value)
                { 
                    $this->fechamentoSec = $value; 
                }

function setFechamentoOffsec($value)
            {
                $this->fechamentoOffsec = $value;
            }

public function setFechamento_OFFSEC($value)
                { 
                    $this->fechamentoOffsec = $value; 
                }

function setValorPagoFloat($value)
            {
                $this->valorPagoFloat = $value;
            }

public function setValor_pago_FLOAT($value)
                { 
                    $this->valorPagoFloat = $value; 
                }

function setDescontoFloat($value)
            {
                $this->descontoFloat = $value;
            }

public function setDesconto_FLOAT($value)
                { 
                    $this->descontoFloat = $value; 
                }

function setProtocoloInt($value)
            {
                $this->protocoloInt = $value;
            }

public function setProtocolo_INT($value)
                { 
                    $this->protocoloInt = $value; 
                }



public function clear()
        {$this->id = null;
$this->formaPagamentoId = null;
if($this->objForma_pagamento != null) unset($this->objForma_pagamento);
$this->valorTotalFloat = null;
$this->dataSec = null;
$this->dataOffsec = null;
$this->usuarioId = null;
if($this->objUsuario != null) unset($this->objUsuario);
$this->minhaEmpresaId = null;
if($this->objMinha_empresa != null) unset($this->objMinha_empresa);
$this->outraEmpresaId = null;
if($this->objOutra_empresa != null) unset($this->objOutra_empresa);
$this->corporacaoId = null;
if($this->objCorporacao != null) unset($this->objCorporacao);
$this->vencimentoSec = null;
$this->vencimentoOffsec = null;
$this->relatorioId = null;
if($this->objRelatorio != null) unset($this->objRelatorio);
$this->registroEstadoId = null;
if($this->objRegistro_estado != null) unset($this->objRegistro_estado);
$this->registroEstadoCorporacaoId = null;
if($this->objRegistro_estado_corporacao != null) unset($this->objRegistro_estado_corporacao);
$this->fechamentoSec = null;
$this->fechamentoOffsec = null;
$this->valorPagoFloat = null;
$this->descontoFloat = null;
$this->protocoloInt = null;
if(is_array($this->arrEstadosCampos)){
                        unset($this->arrEstadosCampos);
                     $this->arrEstadosCampos = array();
                    }
                }

public function formatarParaSQL()
        {
if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->id)){
$this->id = $this->formatarIntegerParaComandoSQL($this->id);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->id);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->formaPagamentoId)){
$this->formaPagamentoId = $this->formatarIntegerParaComandoSQL($this->formaPagamentoId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->formaPagamentoId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->valorTotalFloat)){
$this->valorTotalFloat = $this->formatarFloatParaComandoSQL($this->valorTotalFloat);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->valorTotalFloat);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->dataSec)){
$this->dataSec = $this->formatarIntegerParaComandoSQL($this->dataSec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->dataSec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->dataOffsec)){
$this->dataOffsec = $this->formatarIntegerParaComandoSQL($this->dataOffsec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->dataOffsec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->usuarioId)){
$this->usuarioId = $this->formatarIntegerParaComandoSQL($this->usuarioId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->usuarioId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->minhaEmpresaId)){
$this->minhaEmpresaId = $this->formatarIntegerParaComandoSQL($this->minhaEmpresaId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->minhaEmpresaId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->outraEmpresaId)){
$this->outraEmpresaId = $this->formatarIntegerParaComandoSQL($this->outraEmpresaId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->outraEmpresaId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->corporacaoId)){
$this->corporacaoId = $this->formatarIntegerParaComandoSQL($this->corporacaoId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->corporacaoId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->vencimentoSec)){
$this->vencimentoSec = $this->formatarIntegerParaComandoSQL($this->vencimentoSec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->vencimentoSec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->vencimentoOffsec)){
$this->vencimentoOffsec = $this->formatarIntegerParaComandoSQL($this->vencimentoOffsec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->vencimentoOffsec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->relatorioId)){
$this->relatorioId = $this->formatarIntegerParaComandoSQL($this->relatorioId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->relatorioId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->registroEstadoId)){
$this->registroEstadoId = $this->formatarIntegerParaComandoSQL($this->registroEstadoId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->registroEstadoId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->registroEstadoCorporacaoId)){
$this->registroEstadoCorporacaoId = $this->formatarIntegerParaComandoSQL($this->registroEstadoCorporacaoId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->registroEstadoCorporacaoId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->fechamentoSec)){
$this->fechamentoSec = $this->formatarIntegerParaComandoSQL($this->fechamentoSec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->fechamentoSec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->fechamentoOffsec)){
$this->fechamentoOffsec = $this->formatarIntegerParaComandoSQL($this->fechamentoOffsec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->fechamentoOffsec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->valorPagoFloat)){
$this->valorPagoFloat = $this->formatarFloatParaComandoSQL($this->valorPagoFloat);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->valorPagoFloat);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->descontoFloat)){
$this->descontoFloat = $this->formatarFloatParaComandoSQL($this->descontoFloat);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->descontoFloat);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->protocoloInt)){
$this->protocoloInt = $this->formatarIntegerParaComandoSQL($this->protocoloInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->protocoloInt);
}

}

public function formatarParaExibicao()
        {if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->valorTotalFloat)){
$this->valorTotalFloat = $this->formatarFloatParaExibicao($this->valorTotalFloat);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->valorTotalFloat);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->valorPagoFloat)){
$this->valorPagoFloat = $this->formatarFloatParaExibicao($this->valorPagoFloat);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->valorPagoFloat);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->descontoFloat)){
$this->descontoFloat = $this->formatarFloatParaExibicao($this->descontoFloat);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->descontoFloat);
}

}

function select($id, $idCorporacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }   

            $sql = "SELECT id, forma_pagamento_id_INT, valor_total_FLOAT, data_SEC, data_OFFSEC, usuario_id_INT, minha_empresa_id_INT, outra_empresa_id_INT, corporacao_id_INT, vencimento_SEC, vencimento_OFFSEC, relatorio_id_INT, registro_estado_id_INT, registro_estado_corporacao_id_INT, fechamento_SEC, fechamento_OFFSEC, valor_pago_FLOAT, desconto_FLOAT, protocolo_INT FROM empresa_compra WHERE id = $id AND corporacao_id_INT = $idCorporacao";

            $msg = $this->database->queryMensagem($sql);
            $result = $this->database->result;

            if($msg != null && $msg->erro() )
            { 
            		$this->clear();
            		return $msg;
        	} 
        	else if($this->database->rows() == 0)
        	{
                return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nenhum registro foi encontrado."));
            }
        

            $row = $this->database->fetchArray(MYSQLI_NUM);
            
            		$this->id = $row[0];
		$this->formaPagamentoId = $row[1];
		$this->valorTotalFloat = $row[2];
		$this->dataSec = $row[3];
		$this->dataOffsec = $row[4];
		$this->usuarioId = $row[5];
		$this->minhaEmpresaId = $row[6];
		$this->outraEmpresaId = $row[7];
		$this->vencimentoSec = $row[9];
		$this->vencimentoOffsec = $row[10];
		$this->relatorioId = $row[11];
		$this->registroEstadoId = $row[12];
		$this->registroEstadoCorporacaoId = $row[13];
		$this->fechamentoSec = $row[14];
		$this->fechamentoOffsec = $row[15];
		$this->valorPagoFloat = $row[16];
		$this->descontoFloat = $row[17];
		$this->protocoloInt = $row[18];

            
        }

public function delete($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (is_null($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }

            $sql = "DELETE FROM empresa_compra WHERE id= $id AND corporacao_id_INT = $idCorporacao";
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                    && $sincronizar
                        && static::idSistemaTabela() != null
                            && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_REMOVE,
                                null,
                                null,
                                $id,
                                $this->database);
            }

            return $msg;
        }

public function insert($sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null, $opcoes = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            
            
            if (is_null($opcoes))
            {
                $opcoes = $this->opcoesDAO;
            }

            if (!is_numeric($this->id))
            {
                $this->id = "null";
                if ($opcoes == null || $opcoes[Database::OPCAO_GERAR_ID] != false) 
                {
                    $this->id = EXTDAO_Sistema_sequencia::gerarId(static::nomeTabela());                    
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }    
                    else if($msg == null)
                    {
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
                else 
                {
                    $this->id = null;
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao));
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }
                    else if($msg == null)
                    {
                        $this->id = $this->database->getLastInsertId();
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
            }
            else 
            {
                $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }                        
                else if($msg == null)
                {
                   $msg = new Mensagem_token();
                   $msg->mValor = $this->id;
                }
            }
            if($msg == null || $msg->ok())
            {                
                if($sincronizar
                    && static::idSistemaTabela() != null
                    && isset($this->id)){
                    
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_INSERT,
                                null,
                                null,
                                $this->id,
                                $this->database); 
                	}
            } 
            else
            {
                $this->id = null;
            }
            
            return $msg;
            
        }

public function getSQLInsert($idCorporacao){ 

return "INSERT INTO empresa_compra (id, forma_pagamento_id_INT, valor_total_FLOAT, data_SEC, data_OFFSEC, usuario_id_INT, minha_empresa_id_INT, outra_empresa_id_INT, corporacao_id_INT, vencimento_SEC, vencimento_OFFSEC, relatorio_id_INT, registro_estado_id_INT, registro_estado_corporacao_id_INT, fechamento_SEC, fechamento_OFFSEC, valor_pago_FLOAT, desconto_FLOAT, protocolo_INT) VALUES ( $this->id ,  $this->formaPagamentoId ,  $this->valorTotalFloat ,  $this->dataSec ,  $this->dataOffsec ,  $this->usuarioId ,  $this->minhaEmpresaId ,  $this->outraEmpresaId ,  $idCorporacao ,  $this->vencimentoSec ,  $this->vencimentoOffsec ,  $this->relatorioId ,  $this->registroEstadoId ,  $this->registroEstadoCorporacaoId ,  $this->fechamentoSec ,  $this->fechamentoOffsec ,  $this->valorPagoFloat ,  $this->descontoFloat ,  $this->protocoloInt ) "; 

}




        
        public function updateByParameters($id, $objParametros, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao))
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();

            
                
                if (isset($objParametros->formaPagamentoId)) 
                {
                    $arrUpdateFields[] = " forma_pagamento_id_INT = {$objParametros->formaPagamentoId} ";
                }


                
                if (isset($objParametros->valorTotalFloat)) 
                {
                    $arrUpdateFields[] = " valor_total_FLOAT = {$objParametros->valorTotalFloat} ";
                }


                
                if (isset($objParametros->dataSec)) 
                {
                    $arrUpdateFields[] = " data_SEC = {$objParametros->dataSec} ";
                }


                
                if (isset($objParametros->dataOffsec)) 
                {
                    $arrUpdateFields[] = " data_OFFSEC = {$objParametros->dataOffsec} ";
                }


                
                if (isset($objParametros->usuarioId)) 
                {
                    $arrUpdateFields[] = " usuario_id_INT = {$objParametros->usuarioId} ";
                }


                
                if (isset($objParametros->minhaEmpresaId)) 
                {
                    $arrUpdateFields[] = " minha_empresa_id_INT = {$objParametros->minhaEmpresaId} ";
                }


                
                if (isset($objParametros->outraEmpresaId)) 
                {
                    $arrUpdateFields[] = " outra_empresa_id_INT = {$objParametros->outraEmpresaId} ";
                }


                
                if (isset($objParametros->vencimentoSec)) 
                {
                    $arrUpdateFields[] = " vencimento_SEC = {$objParametros->vencimentoSec} ";
                }


                
                if (isset($objParametros->vencimentoOffsec)) 
                {
                    $arrUpdateFields[] = " vencimento_OFFSEC = {$objParametros->vencimentoOffsec} ";
                }


                
                if (isset($objParametros->relatorioId)) 
                {
                    $arrUpdateFields[] = " relatorio_id_INT = {$objParametros->relatorioId} ";
                }


                
                if (isset($objParametros->registroEstadoId)) 
                {
                    $arrUpdateFields[] = " registro_estado_id_INT = {$objParametros->registroEstadoId} ";
                }


                
                if (isset($objParametros->registroEstadoCorporacaoId)) 
                {
                    $arrUpdateFields[] = " registro_estado_corporacao_id_INT = {$objParametros->registroEstadoCorporacaoId} ";
                }


                
                if (isset($objParametros->fechamentoSec)) 
                {
                    $arrUpdateFields[] = " fechamento_SEC = {$objParametros->fechamentoSec} ";
                }


                
                if (isset($objParametros->fechamentoOffsec)) 
                {
                    $arrUpdateFields[] = " fechamento_OFFSEC = {$objParametros->fechamentoOffsec} ";
                }


                
                if (isset($objParametros->valorPagoFloat)) 
                {
                    $arrUpdateFields[] = " valor_pago_FLOAT = {$objParametros->valorPagoFloat} ";
                }


                
                if (isset($objParametros->descontoFloat)) 
                {
                    $arrUpdateFields[] = " desconto_FLOAT = {$objParametros->descontoFloat} ";
                }


                
                if (isset($objParametros->protocoloInt)) 
                {
                    $arrUpdateFields[] = " protocolo_INT = {$objParametros->protocoloInt} ";
                }



            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE empresa_compra SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";

            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null)
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function update($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($this->formaPagamentoId)) 
                {                                      
                    $arrUpdateFields[] = " forma_pagamento_id_INT = {$this->formaPagamentoId} ";
                }


                
                if (isset($this->valorTotalFloat)) 
                {                                      
                    $arrUpdateFields[] = " valor_total_FLOAT = {$this->valorTotalFloat} ";
                }


                
                if (isset($this->dataSec)) 
                {                                      
                    $arrUpdateFields[] = " data_SEC = {$this->dataSec} ";
                }


                
                if (isset($this->dataOffsec)) 
                {                                      
                    $arrUpdateFields[] = " data_OFFSEC = {$this->dataOffsec} ";
                }


                
                if (isset($this->usuarioId)) 
                {                                      
                    $arrUpdateFields[] = " usuario_id_INT = {$this->usuarioId} ";
                }


                
                if (isset($this->minhaEmpresaId)) 
                {                                      
                    $arrUpdateFields[] = " minha_empresa_id_INT = {$this->minhaEmpresaId} ";
                }


                
                if (isset($this->outraEmpresaId)) 
                {                                      
                    $arrUpdateFields[] = " outra_empresa_id_INT = {$this->outraEmpresaId} ";
                }


                
                if (isset($this->vencimentoSec)) 
                {                                      
                    $arrUpdateFields[] = " vencimento_SEC = {$this->vencimentoSec} ";
                }


                
                if (isset($this->vencimentoOffsec)) 
                {                                      
                    $arrUpdateFields[] = " vencimento_OFFSEC = {$this->vencimentoOffsec} ";
                }


                
                if (isset($this->relatorioId)) 
                {                                      
                    $arrUpdateFields[] = " relatorio_id_INT = {$this->relatorioId} ";
                }


                
                if (isset($this->registroEstadoId)) 
                {                                      
                    $arrUpdateFields[] = " registro_estado_id_INT = {$this->registroEstadoId} ";
                }


                
                if (isset($this->registroEstadoCorporacaoId)) 
                {                                      
                    $arrUpdateFields[] = " registro_estado_corporacao_id_INT = {$this->registroEstadoCorporacaoId} ";
                }


                
                if (isset($this->fechamentoSec)) 
                {                                      
                    $arrUpdateFields[] = " fechamento_SEC = {$this->fechamentoSec} ";
                }


                
                if (isset($this->fechamentoOffsec)) 
                {                                      
                    $arrUpdateFields[] = " fechamento_OFFSEC = {$this->fechamentoOffsec} ";
                }


                
                if (isset($this->valorPagoFloat)) 
                {                                      
                    $arrUpdateFields[] = " valor_pago_FLOAT = {$this->valorPagoFloat} ";
                }


                
                if (isset($this->descontoFloat)) 
                {                                      
                    $arrUpdateFields[] = " desconto_FLOAT = {$this->descontoFloat} ";
                }


                
                if (isset($this->protocoloInt)) 
                {                                      
                    $arrUpdateFields[] = " protocolo_INT = {$this->protocoloInt} ";
                }


            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE empresa_compra SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function updateByInstanceUsingParameters($id, $parameters, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($parameters->formaPagamentoId)) 
                {                                      
                    $arrUpdateFields[] = " forma_pagamento_id_INT = {$this->formaPagamentoId} ";
                }
                
                if (isset($parameters->valorTotalFloat)) 
                {                                      
                    $arrUpdateFields[] = " valor_total_FLOAT = {$this->valorTotalFloat} ";
                }
                
                if (isset($parameters->dataSec)) 
                {                                      
                    $arrUpdateFields[] = " data_SEC = {$this->dataSec} ";
                }
                
                if (isset($parameters->dataOffsec)) 
                {                                      
                    $arrUpdateFields[] = " data_OFFSEC = {$this->dataOffsec} ";
                }
                
                if (isset($parameters->usuarioId)) 
                {                                      
                    $arrUpdateFields[] = " usuario_id_INT = {$this->usuarioId} ";
                }
                
                if (isset($parameters->minhaEmpresaId)) 
                {                                      
                    $arrUpdateFields[] = " minha_empresa_id_INT = {$this->minhaEmpresaId} ";
                }
                
                if (isset($parameters->outraEmpresaId)) 
                {                                      
                    $arrUpdateFields[] = " outra_empresa_id_INT = {$this->outraEmpresaId} ";
                }
                
                if (isset($parameters->vencimentoSec)) 
                {                                      
                    $arrUpdateFields[] = " vencimento_SEC = {$this->vencimentoSec} ";
                }
                
                if (isset($parameters->vencimentoOffsec)) 
                {                                      
                    $arrUpdateFields[] = " vencimento_OFFSEC = {$this->vencimentoOffsec} ";
                }
                
                if (isset($parameters->relatorioId)) 
                {                                      
                    $arrUpdateFields[] = " relatorio_id_INT = {$this->relatorioId} ";
                }
                
                if (isset($parameters->registroEstadoId)) 
                {                                      
                    $arrUpdateFields[] = " registro_estado_id_INT = {$this->registroEstadoId} ";
                }
                
                if (isset($parameters->registroEstadoCorporacaoId)) 
                {                                      
                    $arrUpdateFields[] = " registro_estado_corporacao_id_INT = {$this->registroEstadoCorporacaoId} ";
                }
                
                if (isset($parameters->fechamentoSec)) 
                {                                      
                    $arrUpdateFields[] = " fechamento_SEC = {$this->fechamentoSec} ";
                }
                
                if (isset($parameters->fechamentoOffsec)) 
                {                                      
                    $arrUpdateFields[] = " fechamento_OFFSEC = {$this->fechamentoOffsec} ";
                }
                
                if (isset($parameters->valorPagoFloat)) 
                {                                      
                    $arrUpdateFields[] = " valor_pago_FLOAT = {$this->valorPagoFloat} ";
                }
                
                if (isset($parameters->descontoFloat)) 
                {                                      
                    $arrUpdateFields[] = " desconto_FLOAT = {$this->descontoFloat} ";
                }
                
                if (isset($parameters->protocoloInt)) 
                {                                      
                    $arrUpdateFields[] = " protocolo_INT = {$this->protocoloInt} ";
                }
            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE empresa_compra SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }

}

?>
