<?php

/*
*
* -------------------------------------------------------
* NOME DO FORMUL�RIO: relatorio_anexo
* DATA DE GERA��O:    05.11.2012
* ARQUIVO:            relatorio_anexo.php
* TABELA MYSQL:       relatorio_anexo
* BANCO DE DADOS:     ponto_eletronico
* -------------------------------------------------------
*
*/

if (isset($_GET["contador"]))
{

    $numeroRegistroInterno = Helper::GET("contador");
}

if (isset($identificadorRelacionamento) && is_numeric($identificadorRelacionamento))
{

    $objRelatorio_anexo = new EXTDAO_Relatorio_anexo();
    $objRelatorio_anexo->select($identificadorRelacionamento);
}
else
{

    $objRelatorio_anexo = new EXTDAO_Relatorio_anexo();
}

$objArgRelatorio_anexo = new Generic_Argument();
$objRelatorio_anexo->formatarParaExibicao();

?>

<input type="hidden" name="relatorio_anexo_id_<?= $numeroRegistroInterno ?>"
       id="relatorio_anexo_id_<?= $numeroRegistroInterno ?>" value="<?= $objRelatorio_anexo->getId(); ?>">

<table class="tabela_form">

    <tr class="tr_form">


        <?

        $objArgRelatorio_anexo = new Generic_Argument();

        $objArgRelatorio_anexo->numeroDoRegistro = "";
        $objArgRelatorio_anexo->label = $objRelatorio_anexo->label_arquivo;
        $objArgRelatorio_anexo->valor = $objRelatorio_anexo->getArquivo();
        $objArgRelatorio_anexo->classeCss = "input_text";
        $objArgRelatorio_anexo->classeCssFocus = "focus_text";
        $objArgRelatorio_anexo->obrigatorio = true;
        $objArgRelatorio_anexo->largura = 200;
        $objArgRelatorio_anexo->nome = "relatorio_anexo_arquivo_{$numeroRegistroInterno}";
        $objArgRelatorio_anexo->id = "relatorio_anexo_arquivo_{$numeroRegistroInterno}";

        ?>

        <td class="td_form_label"><?= $objArgRelatorio_anexo->getLabel() ?></td>
        <td class="td_form_campo"><?= $objRelatorio_anexo->campoTexto($objArgRelatorio_anexo); ?></td>


        <?

        $objArgRelatorio_anexo = new Generic_Argument();

        $objArgRelatorio_anexo->numeroDoRegistro = "";
        $objArgRelatorio_anexo->label = $objRelatorio_anexo->label_tipo_anexo_id_INT;
        $objArgRelatorio_anexo->valor = $objRelatorio_anexo->getTipo_anexo_id_INT();
        $objArgRelatorio_anexo->classeCss = "input_text";
        $objArgRelatorio_anexo->classeCssFocus = "focus_text";
        $objArgRelatorio_anexo->obrigatorio = true;
        $objArgRelatorio_anexo->largura = 200;
        $objArgRelatorio_anexo->nome = "relatorio_anexo_tipo_anexo_id_INT_{$numeroRegistroInterno}";
        $objArgRelatorio_anexo->id = "relatorio_anexo_tipo_anexo_id_INT_{$numeroRegistroInterno}";

        $objRelatorio_anexo->addInfoCampos("tipo_anexo_id_INT", $objArgRelatorio_anexo->label, "TEXTO", $objArgRelatorio_anexo->obrigatorio);

        ?>

        <td class="td_form_label"><?= $objArgRelatorio_anexo->getLabel() ?></td>
        <td class="td_form_campo">
            <?= $objRelatorio_anexo->objTipo_anexo->getComboBox($objArgRelatorio_anexo); ?>
        </td>


    </tr>
    <tr>
        <td colspan="4" class="td_botao_remover_da_lista"><input class="botoes_form" type="button"
                                                                 value="Remover Anexo Do Relat�rio"
                                                                 onclick="javascript:removerDivAjaxEmLista(this);"></td>
    </tr>
</table><br/>

