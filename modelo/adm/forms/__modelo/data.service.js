returnObject.getCliente = function (id)
{
    var params = {id: id};
    return $http.get(baseURLAdm + "/actions.php?class=EXTDAO_Acesso&action=getCliente", {params: params});
};
returnObject.getListOfPessoa = function ()
{
    var params = {};
    return $http.get(baseURLAdm + "/actions.php?class=EXTDAO_Acesso&action=getListaPessoas", {params: params});
};
returnObject.getListOfPaises = function ()
{
    var params = {};
    return $http.get(baseURLAdm + "/actions.php?class=EXTDAO_Acesso&action=getListaPaises", {params: params});
};
returnObject.removeDocumentoUploadFile = function (fileUniqueId)
{
    var params = {fileUniqueId: fileUniqueId};
    return $http.get(baseURLAdm + "/actions.php?class=EXTDAO_Acesso&action=removeDocumentoUploadFile", {params: params});
};
returnObject.addCliente = function (formData)
{
    return $http({
        url: "actions.php?class=EXTDAO_Acesso&jsonOutput=true&action=saveCliente",
        method: "POST",
        data: formData,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    });
};
returnObject.editCliente = function (formData)
{
    return $http({
        url: "actions.php?class=EXTDAO_Acesso&jsonOutput=true&action=saveCliente",
        method: "POST",
        data: formData,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    });
};


