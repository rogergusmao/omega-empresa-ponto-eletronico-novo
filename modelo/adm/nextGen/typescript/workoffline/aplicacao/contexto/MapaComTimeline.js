"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var uiComponente_1 = require("lib/uiComponente");
var contexto_1 = require("lib/contexto");
var geral_1 = require("lib/geral");
var MapaComTimeline = (function (_super) {
    __extends(MapaComTimeline, _super);
    function MapaComTimeline(domRootSelector, mapContainerSelector) {
        var _this = 
        //chama o construtor do pai
        _super.call(this, domRootSelector, mapContainerSelector, new geral_1.Tempo()) || this;
        //define o root selector
        _this.domRootSelector = domRootSelector;
        //inicializa mapa
        _this.initializeMap();
        return _this;
    }
    MapaComTimeline.prototype.initializeMap = function () {
        this.mapObject = new uiComponente_1.Mapa(this);
        this.mapObject.addResizeListener();
    };
    MapaComTimeline.prototype.getDefaultMarkerFolder = function () {
        return undefined;
    };
    return MapaComTimeline;
}(contexto_1.ContextoTempo));
exports.MapaComTimeline = MapaComTimeline;
