import * as React from 'react';
import * as ReactDOM from 'react-dom';
import * as Moment from 'moment';

import { OptionsInput } from 'fullcalendar/src/types/input-types';
import { EventObjectInput, View } from 'fullcalendar';

import { ContextoGenerico } from 'lib/contexto';
import { ReactComponent } from 'lib/uiComponente';
import { Helper, ReactProps } from 'lib/geral';

export class Calendario<P extends Calendario.CalendarioProps, S extends Calendario.CalendarioState> extends ReactComponent<P, S>
{
    private static readonly INTERNAL_CONTAINER_CLASSNAME :string = "calendar-internal-container";  

    public calendarInputOptions :OptionsInput;
    public calendarEvents :Array<EventObjectInput>;

    protected jQuerySelector: string;

    public constructor(props: P, reactContext: any)
    { 
        super(props || {} as P, reactContext);           
        this.jQuerySelector = `.${Calendario.INTERNAL_CONTAINER_CLASSNAME}`;

        if(this.props.addResizeListener)
        {
            this.addResizeListener();
        }

    }

    public setDefaultOptions()
    {
        var defaultOptions :OptionsInput = 
        {
            columnFormat: 'ddd',             
            defaultView: 'agendaWeek',
            timeFormat: 'hh:mm',
            editable: false,
            selectable: true,
            allDaySlot: false,
            themeSystem: 'bootstrap3',
            nowIndicator: true,
            displayEventTime: true,
            eventOverlap: false,    
            slotEventOverlap: false,   
            selectOverlap: false,            
            header: {
                left: 'prev,next today',       
                center: 'title',            
                right: 'month,agendaWeek,agendaDay'
            },                
            contentHeight: 'auto',
            now: Moment(new Date()),                
            views : {
                month: {
                    titleFormat: 'MMMM YYYY',
                    columnFormat: 'ddd'
                },
                day: {
                    titleFormat: 'DD MMMM YYYY'
                },
                week: {
                    titleFormat: "DD MMMM YYYY",
                    titleRangeSeparator: '-'                    
                }
            }                                
        };

        this.calendarInputOptions = defaultOptions;
        this.setDefaultEventListeners();
    }

    public setDefaultEventListeners()
    {
        if(this.calendarInputOptions == null)
        {
            this.calendarInputOptions = {};
        }

        if(this.props.onEventClick)
        {
            this.calendarInputOptions.eventClick = this.props.onEventClick.bind(this);
        }

        if(this.props.onEventMouseOver)
        {
            this.calendarInputOptions.eventMouseover = this.props.onEventMouseOver.bind(this);
        }
        
        if(this.props.onCalendarViewChange)
        {
            this.calendarInputOptions.viewRender = this.props.onCalendarViewChange.bind(this);
        }        
    }

    public fixLayout()
    {

    }

    private addResizeListener() : void
    {
        $(window).resize(() =>
        {
            this.fixLayout();
            this.setState({ resize: true });
        });
    }

    public render()
    {
        if(this.props.setDefaultOptions)
        {
            this.setDefaultOptions();
        }

        if(!Helper.isNullOrEmpty(this.props.titulo))
        {
            return (
                <div className="box">
                    <div className="box-header">
                        <div className="title">
                            <i className="icon-group"/> {this.props.titulo}
                        </div>
                    </div>
                    <div className={`box-content ${Calendario.INTERNAL_CONTAINER_CLASSNAME}`}></div>
                </div>
            );                
        }
        else
        {
            return <div className={`${Calendario.INTERNAL_CONTAINER_CLASSNAME}`}></div>
        }
    }
    
    public setDateRange(initialDate :Date, finalDate :Date)
    {
        this.calendarInputOptions.visibleRange = 
        {
            start: initialDate,
            end: finalDate
        };

        this.setState({ dateRangeChanged: true });
    }

    public limparEventos()
    {
        this.calendarEvents = new Array<EventObjectInput>();
        this.setState({ eventsCleared: true });
    }

    public adicionarEventos(events: Array<EventObjectInput>)
    {
        this.calendarEvents = events;
        this.setState({ eventsAdded: true });
    }
    
    public resizeCalendar()
    {
        this.setState({ resize: true });          
    }

    private updateRenderFull()
    {
        $(this.jQuerySelector).fullCalendar('destroy');
        $(this.jQuerySelector).fullCalendar(this.calendarInputOptions);
    }

    public componentDidMount()
    {
        this.fixLayout();
        $(this.jQuerySelector).fullCalendar(this.calendarInputOptions);
    }

    public componentDidUpdate()
    {
        if(this.state.resize)
        {
            let height = $(this.jQuerySelector).height();
            let width = $(this.jQuerySelector).width();

            $(this.jQuerySelector).fullCalendar('option', 'aspectRatio', width/height);
        }

        if(this.state.eventsCleared)
        {
            $(this.jQuerySelector).fullCalendar('removeEvents', undefined);
        }
        
        if(this.state.eventsAdded)
        {
            $(this.jQuerySelector).fullCalendar('renderEvents', this.calendarEvents);
        }

        if(this.state.dateRangeChanged)
        {
            $(this.jQuerySelector).fullCalendar('option', 'visibleRange', this.calendarInputOptions.visibleRange);
        }

        if(this.state.optionsChanged)
        {
            this.updateRenderFull();
        }

    }

}

export namespace Calendario
{
    export interface CalendarioProps extends ReactProps
    {
        titulo?: string;
        setDefaultOptions: boolean;
        onEventClick?: (event: EventObjectInput, jsEvent: MouseEvent, view: View) => any;        
        onCalendarViewChange?: (view: View, element: JQuery) => any; 
        onEventMouseOver?: (event: EventObjectInput, jsEvent: MouseEvent, view: View) => any;    
        addResizeListener? :boolean; 
    }

    export interface CalendarioState
    {
        eventsCleared :boolean;
        eventsAdded :boolean;    
        optionsChanged :boolean; 
        dateRangeChanged :boolean;
        resize: boolean;
    }

    

}