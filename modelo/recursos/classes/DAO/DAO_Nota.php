<?php


        /*

        Arquivo gerado através de gerador de código em 12/05/2018 as 14:35:07.
        Para que o arquivo não seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

        Tabela correspondente: nota
        Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

        */
        


class DAO_Nota extends Generic_DAO
                        {
                            const NOME_ENTIDADE = "nota";

                            public static $nomeTabela = null;
                            public static $idSistemaTabela = false;
                            public static $databaseFieldsRelatedAttributes = null;
	                        public static $databaseFieldTypes = null;
	                        public static $databaseFieldNames = null;
	                        public static $listAliasTypes = null;
	                        public static $listAliasRelatedAttributes = null;
	                        
	                        public $id;
public $empresaId;
public $pessoaId;
public $usuarioId;
public $veiculoId;
public $relatorioId;
public $protocoloInt;
public $corporacaoId;

public $labelId;
public $labelEmpresaId;
public $labelPessoaId;
public $labelUsuarioId;
public $labelVeiculoId;
public $labelRelatorioId;
public $labelProtocoloInt;
public $labelCorporacaoId;

public static $arrPK = array("id");

public function __construct($configDAO = null)
        {
            parent::__construct($configDAO);
            static::__setStaticValues($this->database);           
        }

public static function idSistemaTabela($databaseObject = null){
            if(!(static::$idSistemaTabela))
            {
                static::$idSistemaTabela = EXTDAO_Sistema_tabela::getIdDaSistemaTabela(static::nomeTabela(), $databaseObject);
            }
            return static::$idSistemaTabela;
        }

public function getIdSistemaTabela(){
            return static::idSistemaTabela();
        }

public static function nomeTabela(){
            if(is_null(static::$nomeTabela))
            {
                static::$nomeTabela = "nota";
            }
            return static::$nomeTabela;
        }

public function getNomeTabela(){
            return static::nomeTabela();
        }

public static function __setStaticValues($databaseObject = null)
                   {    
                        static::nomeTabela();
                        
                        static::idSistemaTabela($databaseObject);
                   
                        static::setDatabaseFieldNames();
                        static::setDatabaseFieldTypes();
                        static::setDatabaseFieldsRelatedAttributeNames();
                        
                        static::setListAliasTypes();
                        static::setListAliasRelatedAttributes();
                   }

public function getExpressaoLabel()
                   {
                        return "{id}";
                   }

public static function setDatabaseFieldNames()
        {
            if(is_null(static::$databaseFieldNames))
            {
                static::$databaseFieldNames = new stdClass();
                static::$databaseFieldNames->id = "id";
static::$databaseFieldNames->empresaId = "empresa_id_INT";
static::$databaseFieldNames->pessoaId = "pessoa_id_INT";
static::$databaseFieldNames->usuarioId = "usuario_id_INT";
static::$databaseFieldNames->veiculoId = "veiculo_id_INT";
static::$databaseFieldNames->relatorioId = "relatorio_id_INT";
static::$databaseFieldNames->protocoloInt = "protocolo_INT";
static::$databaseFieldNames->corporacaoId = "corporacao_id_INT";

            }
                      
        }

public static function setDatabaseFieldTypes()
        {
            if(is_null(static::$databaseFieldTypes))
            {
                static::$databaseFieldTypes = new stdClass();
                static::$databaseFieldTypes->id = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->empresaId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->pessoaId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->usuarioId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->veiculoId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->relatorioId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->protocoloInt = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->corporacaoId = static::TIPO_VARIAVEL_INTEGER;

            }
            
        }

public static function setDatabaseFieldsRelatedAttributeNames()
        {
            if(is_null(static::$databaseFieldsRelatedAttributes))
            {
                static::$databaseFieldsRelatedAttributes = new stdClass();
                static::$databaseFieldsRelatedAttributes->id = "id";
static::$databaseFieldsRelatedAttributes->empresa_id_INT = "empresaId";
static::$databaseFieldsRelatedAttributes->pessoa_id_INT = "pessoaId";
static::$databaseFieldsRelatedAttributes->usuario_id_INT = "usuarioId";
static::$databaseFieldsRelatedAttributes->veiculo_id_INT = "veiculoId";
static::$databaseFieldsRelatedAttributes->relatorio_id_INT = "relatorioId";
static::$databaseFieldsRelatedAttributes->protocolo_INT = "protocoloInt";
static::$databaseFieldsRelatedAttributes->corporacao_id_INT = "corporacaoId";

            }           
        }

public function setAllFieldsToHumanFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["empresa_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["pessoa_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["usuario_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["veiculo_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["relatorio_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["protocolo_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
}

public function setAllFieldsToDatabaseFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["empresa_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["pessoa_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["usuario_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["veiculo_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["relatorio_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["protocolo_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
}

  public function getFkObjEmpresa() 
                {
                    if($this->objEmpresa == null)
                    {                        
                        $this->objEmpresa = new EXTDAO_Empresa_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getEmpresa_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objEmpresa->clear();
                    }
                    elseif($this->objEmpresa->getId() != $idFK)
                    {
                        $this->objEmpresa->select($idFK);
                    }
                    return $this->objEmpresa;
                }
  public function getFkObjPessoa() 
                {
                    if($this->objPessoa == null)
                    {                        
                        $this->objPessoa = new EXTDAO_Pessoa_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getPessoa_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objPessoa->clear();
                    }
                    elseif($this->objPessoa->getId() != $idFK)
                    {
                        $this->objPessoa->select($idFK);
                    }
                    return $this->objPessoa;
                }
  public function getFkObjUsuario() 
                {
                    if($this->objUsuario == null)
                    {                        
                        $this->objUsuario = new EXTDAO_Usuario_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getUsuario_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objUsuario->clear();
                    }
                    elseif($this->objUsuario->getId() != $idFK)
                    {
                        $this->objUsuario->select($idFK);
                    }
                    return $this->objUsuario;
                }
  public function getFkObjVeiculo() 
                {
                    if($this->objVeiculo == null)
                    {                        
                        $this->objVeiculo = new EXTDAO_Veiculo_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getVeiculo_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objVeiculo->clear();
                    }
                    elseif($this->objVeiculo->getId() != $idFK)
                    {
                        $this->objVeiculo->select($idFK);
                    }
                    return $this->objVeiculo;
                }
  public function getFkObjRelatorio() 
                {
                    if($this->objRelatorio == null)
                    {                        
                        $this->objRelatorio = new EXTDAO_Relatorio_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getRelatorio_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objRelatorio->clear();
                    }
                    elseif($this->objRelatorio->getId() != $idFK)
                    {
                        $this->objRelatorio->select($idFK);
                    }
                    return $this->objRelatorio;
                }
  public function getFkObjCorporacao() 
                {
                    if($this->objCorporacao == null)
                    {                        
                        $this->objCorporacao = new EXTDAO_Corporacao_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getCorporacao_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objCorporacao->clear();
                    }
                    elseif($this->objCorporacao->getId() != $idFK)
                    {
                        $this->objCorporacao->select($idFK);
                    }
                    return $this->objCorporacao;
                }


public function setLabels()
        {$this->labelId = "";
$this->labelEmpresaId = "";
$this->labelPessoaId = "";
$this->labelUsuarioId = "";
$this->labelVeiculoId = "";
$this->labelRelatorioId = "";
$this->labelProtocoloInt = "";
$this->labelCorporacaoId = "";
}

public function __actionAdd($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                $this->setByObject($parameters);
                $this->setDataCadastroAndOffsetInSecondsFromUser($parameters);
                
                $this->formatarParaSQL();
                $msg = $this->insert(true);
                
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                return new Mensagem(null, I18N::getExpression("Nota adicionado com sucesso."));
                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionEdit($parameters = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try {

                $this->setByObject($parameters);
                $this->setAllFieldsToHumanFormat();
                
                $this->formatarParaSQL();
                $msg = $this->updateByInstanceUsingParameters($this->id, $parameters, true);
                if($msg != null && $msg->erro())
                {
                     return $msg;
                }
        
                return new Mensagem(null, I18N::getExpression("Nota editado com sucesso."));
            }
            catch(Exception $ex)
            {                
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionRemove($parameters = null)
        {
            $successulRemovalCount = 0;
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                foreach($parameters as $record)
                {
                    if(is_numeric($record->id))
                    {
                        $msg = $this->delete($record->id, true);
                        if($msg == null || $msg->ok())
                        {
                            $successulRemovalCount++;
                        }
                    }
                }
                
                if($successulRemovalCount > 0)
                {
                    $successMessage = count($successulRemovalCount) > 1 ? I18N::getExpression("{0} Nota foram removidos com sucesso.", $successulRemovalCount) : I18N::getExpression("Nota removido com sucesso.");
                    return new Mensagem(null, $successMessage);
                }
                else
                {
                    $errorMessage = count($parameters) > 1 ? I18N::getExpression("Falha ao remover Nota.") : I18N::getExpression("Falha ao remover Nota.");
                    return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, $errorMessage);                    
                }
            }
            catch (Exception $ex) 
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }

public function __getRecord($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }
            
            try
            {            
                $returnObject = new stdClass();            
                $whereClause = static::getWhereClauseForFilter($parameters->filterParameters);
                $query = "SELECT  id, empresa_id_INT, pessoa_id_INT, usuario_id_INT, veiculo_id_INT, relatorio_id_INT, protocolo_INT, corporacao_id_INT FROM nota {$whereClause}";
    
                $msg = $this->database->queryMensagem($query);                
                if ($msg == null)
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Não foi encontrado nenhum registro"));
                    }                    
                    $resultSet = $this->database->result;
                    $resultObject = static::mapResultSetDataToRecord($resultSet);
                    
                    $returnObject->formData = $resultObject;
                    $returnObject->comboBoxesData = null;
                    if($parameters->loadComboBoxesData)
                    {
                        $this->setByObject($resultObject);
                        $returnObject->comboBoxesData =  $this->getFormComboBoxesData();
                    }                    
    
                    
    
                    return new Mensagem_generica($returnObject);
                } 
                else 
                {
                    return $msg;
                }
                
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }


        
        public function __getFilterComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
                
        public function __getFormComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
        
        protected function getFormComboBoxesData()
        {
            $comboBoxesData = new stdClass();
            $listParameters = new stdClass();
            $listParameters->listMappingType = static::LIST_MAPPING_TYPE_COMBOBOX;
            
            
                    
                    $objEmpresa = new EXTDAO_Empresa();
                    $comboBoxesData->fieldEmpresaId = $objEmpresa->__getList($listParameters);
                    
                    $objPessoa = new EXTDAO_Pessoa();
                    $comboBoxesData->fieldPessoaId = $objPessoa->__getList($listParameters);
                    
                    $objUsuario = new EXTDAO_Usuario();
                    $comboBoxesData->fieldUsuarioId = $objUsuario->__getList($listParameters);
                    
                    $objVeiculo = new EXTDAO_Veiculo();
                    $comboBoxesData->fieldVeiculoId = $objVeiculo->__getList($listParameters);
                    
                    $objRelatorio = new EXTDAO_Relatorio();
                    $comboBoxesData->fieldRelatorioId = $objRelatorio->__getList($listParameters);

            return $comboBoxesData;
        }
        
        


        
        public static function setListAliasTypes()
        {
            if(is_null(static::$listAliasTypes))
            {
                static::$listAliasTypes = new stdClass();
                static::$listAliasTypes->empresa__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->pessoa__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->usuario__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->veiculo__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->relatorio__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->nota__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->nota__ind_celular_valido_BOOLEAN = static::TIPO_VARIAVEL_BOOLEAN;
static::$listAliasTypes->nota__ind_celular_valido_BOOLEAN = static::TIPO_VARIAVEL_BOOLEAN;
static::$listAliasTypes->nota__cadastro_OFFSEC = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$listAliasTypes->nota__corporacao_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->nota__tipo_relatorio_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->nota__protocolo_INT = static::TIPO_VARIAVEL_INTEGER;
            }        
        }
        
        public static function setListAliasRelatedAttributes()
        {
            if(is_null(static::$listAliasRelatedAttributes))
            {
                static::$listAliasRelatedAttributes = new stdClass();
                static::$listAliasRelatedAttributes->empresa__id = "empresaId";
static::$listAliasRelatedAttributes->pessoa__id = "pessoaId";
static::$listAliasRelatedAttributes->usuario__id = "usuarioId";
static::$listAliasRelatedAttributes->veiculo__id = "veiculoId";
static::$listAliasRelatedAttributes->relatorio__id = "relatorioId";
static::$listAliasRelatedAttributes->nota__id = "id";
static::$listAliasRelatedAttributes->nota__ind_celular_valido_BOOLEAN = "indCelularValidoBoolean";
static::$listAliasRelatedAttributes->nota__ind_celular_valido_BOOLEAN = "indCelularValidoBoolean";
static::$listAliasRelatedAttributes->nota__cadastro_OFFSEC = "cadastroOffsec";
static::$listAliasRelatedAttributes->nota__corporacao_id_INT = "corporacaoId";
static::$listAliasRelatedAttributes->nota__tipo_relatorio_id_INT = "tipoRelatorioId";
static::$listAliasRelatedAttributes->nota__protocolo_INT = "protocoloInt";
            }         
        }

        public function __getList($parameters = null, $idCorporacao = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try
            {
                $filterParameters = new stdClass();
                if(isset($parameters->filterParameters))
                {
                    $filterParameters = $parameters->filterParameters;
                }
                                    
                            
            if(is_null($idCorporacao))
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            ListParameters::addFilterParameterToObject($filterParameters, "corporacaoId", $idCorporacao);           
                
                
                $sortingParameters = null;
                if(isset($parameters->sortingParameters))
                {
                    $sortingParameters = $parameters->sortingParameters;
                }
                
                $paginationParameters = null;
                if(isset($parameters->paginationParameters))
                {
                    $paginationParameters = $parameters->paginationParameters;
                }
                
                $listMappingType = static::getListMappingType($parameters->listMappingType);
                $mainTableAlias = "n";

                $whereClause = static::getWhereClauseForFilter($filterParameters, $mainTableAlias);
                $orderByClause = static::getOrderByClauseForFilter($sortingParameters, $mainTableAlias);
                $limitClause = static::getLimitClauseForFilter($paginationParameters);

                $queryWithoutLimit = "SELECT n.id FROM nota n {$whereClause}";
                $query = "SELECT e.id AS empresa__id, p.id AS pessoa__id, u.id AS usuario__id, v.id AS veiculo__id, r.id AS relatorio__id, n.id AS nota__id, n.ind_celular_valido_BOOLEAN AS nota__ind_celular_valido_BOOLEAN, n.ind_celular_valido_BOOLEAN AS nota__ind_celular_valido_BOOLEAN, n.cadastro_OFFSEC AS nota__cadastro_OFFSEC, n.corporacao_id_INT AS nota__corporacao_id_INT, n.tipo_relatorio_id_INT AS nota__tipo_relatorio_id_INT, n.protocolo_INT AS nota__protocolo_INT FROM nota n LEFT JOIN empresa e ON e.id = n.empresa_id_INT LEFT JOIN pessoa p ON p.id = n.pessoa_id_INT LEFT JOIN usuario u ON u.id = n.usuario_id_INT LEFT JOIN veiculo v ON v.id = n.veiculo_id_INT LEFT JOIN relatorio r ON r.id = n.relatorio_id_INT {$whereClause} {$orderByClause} {$limitClause}";

                $msg = $this->database->queryMensagem($queryWithoutLimit);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                
                $totalNumberOfRecords = $this->database->rows();
                if($totalNumberOfRecords == 0)
                {
                    return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                }                        
                        
                $msg = $this->database->queryMensagem($query);
                
                //operacao realizaca com sucesso
                if ($msg == null) 
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                    }                        
                
                    $resultSet = $this->database->result;

                    $objReturn = new stdClass();
                    $objReturn->dataSet = static::mapResultSetDataToList($resultSet, $listMappingType, static::$listAliasTypes);

                    if(!Helper::isNullOrEmpty($paginationParameters->recordsPerPage) && !Helper::isNullOrEmpty($paginationParameters->currentPage))
                    {
                        $lastPage = ceil($totalNumberOfRecords / $paginationParameters->recordsPerPage);
                        $objReturn->paginationParameters = new PaginationParams($paginationParameters->recordsPerPage, $paginationParameters->currentPage, $lastPage);
                    }
                    
                    return new Mensagem_generica($objReturn);
                } 
                else
                {
                    return $msg;
                }                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }



public function getId()
            {
                return $this->id;
            }

public function getEmpresaId()
            {
                return $this->empresaId;
            }

public function getEmpresa_id_INT()
                {
                    return $this->empresaId;
                }

public function getPessoaId()
            {
                return $this->pessoaId;
            }

public function getPessoa_id_INT()
                {
                    return $this->pessoaId;
                }

public function getUsuarioId()
            {
                return $this->usuarioId;
            }

public function getUsuario_id_INT()
                {
                    return $this->usuarioId;
                }

public function getVeiculoId()
            {
                return $this->veiculoId;
            }

public function getVeiculo_id_INT()
                {
                    return $this->veiculoId;
                }

public function getRelatorioId()
            {
                return $this->relatorioId;
            }

public function getRelatorio_id_INT()
                {
                    return $this->relatorioId;
                }

public function getProtocoloInt()
            {
                return $this->protocoloInt;
            }

public function getProtocolo_INT()
                {
                    return $this->protocoloInt;
                }

public function getCorporacaoId()
            {
                return $this->corporacaoId;
            }

public function getCorporacao_id_INT()
                {
                    return $this->corporacaoId;
                }



function setId($value)
            {
                $this->id = $value;
            }

function setEmpresaId($value)
            {
                $this->empresaId = $value;
            }

public function setEmpresa_id_INT($value)
                { 
                    $this->empresaId = $value; 
                }

function setPessoaId($value)
            {
                $this->pessoaId = $value;
            }

public function setPessoa_id_INT($value)
                { 
                    $this->pessoaId = $value; 
                }

function setUsuarioId($value)
            {
                $this->usuarioId = $value;
            }

public function setUsuario_id_INT($value)
                { 
                    $this->usuarioId = $value; 
                }

function setVeiculoId($value)
            {
                $this->veiculoId = $value;
            }

public function setVeiculo_id_INT($value)
                { 
                    $this->veiculoId = $value; 
                }

function setRelatorioId($value)
            {
                $this->relatorioId = $value;
            }

public function setRelatorio_id_INT($value)
                { 
                    $this->relatorioId = $value; 
                }

function setProtocoloInt($value)
            {
                $this->protocoloInt = $value;
            }

public function setProtocolo_INT($value)
                { 
                    $this->protocoloInt = $value; 
                }

function setCorporacaoId($value)
            {
                $this->corporacaoId = $value;
            }

public function setCorporacao_id_INT($value)
                { 
                    $this->corporacaoId = $value; 
                }



public function clear()
        {$this->id = null;
$this->empresaId = null;
if($this->objEmpresa != null) unset($this->objEmpresa);
$this->pessoaId = null;
if($this->objPessoa != null) unset($this->objPessoa);
$this->usuarioId = null;
if($this->objUsuario != null) unset($this->objUsuario);
$this->veiculoId = null;
if($this->objVeiculo != null) unset($this->objVeiculo);
$this->relatorioId = null;
if($this->objRelatorio != null) unset($this->objRelatorio);
$this->protocoloInt = null;
$this->corporacaoId = null;
if($this->objCorporacao != null) unset($this->objCorporacao);
if(is_array($this->arrEstadosCampos)){
                        unset($this->arrEstadosCampos);
                     $this->arrEstadosCampos = array();
                    }
                }

public function formatarParaSQL()
        {
if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->id)){
$this->id = $this->formatarIntegerParaComandoSQL($this->id);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->id);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->empresaId)){
$this->empresaId = $this->formatarIntegerParaComandoSQL($this->empresaId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->empresaId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->pessoaId)){
$this->pessoaId = $this->formatarIntegerParaComandoSQL($this->pessoaId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->pessoaId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->usuarioId)){
$this->usuarioId = $this->formatarIntegerParaComandoSQL($this->usuarioId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->usuarioId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->veiculoId)){
$this->veiculoId = $this->formatarIntegerParaComandoSQL($this->veiculoId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->veiculoId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->relatorioId)){
$this->relatorioId = $this->formatarIntegerParaComandoSQL($this->relatorioId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->relatorioId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->protocoloInt)){
$this->protocoloInt = $this->formatarIntegerParaComandoSQL($this->protocoloInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->protocoloInt);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->corporacaoId)){
$this->corporacaoId = $this->formatarIntegerParaComandoSQL($this->corporacaoId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->corporacaoId);
}

}

public function formatarParaExibicao()
        {}

function select($id, $idCorporacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }   

            $sql = "SELECT id, empresa_id_INT, pessoa_id_INT, usuario_id_INT, veiculo_id_INT, relatorio_id_INT, protocolo_INT, corporacao_id_INT FROM nota WHERE id = $id AND corporacao_id_INT = $idCorporacao";

            $msg = $this->database->queryMensagem($sql);
            $result = $this->database->result;

            if($msg != null && $msg->erro() )
            { 
            		$this->clear();
            		return $msg;
        	} 
        	else if($this->database->rows() == 0)
        	{
                return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nenhum registro foi encontrado."));
            }
        

            $row = $this->database->fetchArray(MYSQLI_NUM);
            
            		$this->id = $row[0];
		$this->empresaId = $row[1];
		$this->pessoaId = $row[2];
		$this->usuarioId = $row[3];
		$this->veiculoId = $row[4];
		$this->relatorioId = $row[5];
		$this->protocoloInt = $row[6];

            
        }

public function delete($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (is_null($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }

            $sql = "DELETE FROM nota WHERE id= $id AND corporacao_id_INT = $idCorporacao";
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                    && $sincronizar
                        && static::idSistemaTabela() != null
                            && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_REMOVE,
                                null,
                                null,
                                $id,
                                $this->database);
            }

            return $msg;
        }

public function insert($sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null, $opcoes = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            
            
            if (is_null($opcoes))
            {
                $opcoes = $this->opcoesDAO;
            }

            if (!is_numeric($this->id))
            {
                $this->id = "null";
                if ($opcoes == null || $opcoes[Database::OPCAO_GERAR_ID] != false) 
                {
                    $this->id = EXTDAO_Sistema_sequencia::gerarId(static::nomeTabela());                    
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }    
                    else if($msg == null)
                    {
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
                else 
                {
                    $this->id = null;
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao));
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }
                    else if($msg == null)
                    {
                        $this->id = $this->database->getLastInsertId();
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
            }
            else 
            {
                $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }                        
                else if($msg == null)
                {
                   $msg = new Mensagem_token();
                   $msg->mValor = $this->id;
                }
            }
            if($msg == null || $msg->ok())
            {                
                if($sincronizar
                    && static::idSistemaTabela() != null
                    && isset($this->id)){
                    
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_INSERT,
                                null,
                                null,
                                $this->id,
                                $this->database); 
                	}
            } 
            else
            {
                $this->id = null;
            }
            
            return $msg;
            
        }

public function getSQLInsert($idCorporacao){ 

return "INSERT INTO nota (id, empresa_id_INT, pessoa_id_INT, usuario_id_INT, veiculo_id_INT, relatorio_id_INT, protocolo_INT, corporacao_id_INT) VALUES ( $this->id ,  $this->empresaId ,  $this->pessoaId ,  $this->usuarioId ,  $this->veiculoId ,  $this->relatorioId ,  $this->protocoloInt ,  $idCorporacao ) "; 

}




        
        public function updateByParameters($id, $objParametros, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao))
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();

            
                
                if (isset($objParametros->empresaId)) 
                {
                    $arrUpdateFields[] = " empresa_id_INT = {$objParametros->empresaId} ";
                }


                
                if (isset($objParametros->pessoaId)) 
                {
                    $arrUpdateFields[] = " pessoa_id_INT = {$objParametros->pessoaId} ";
                }


                
                if (isset($objParametros->usuarioId)) 
                {
                    $arrUpdateFields[] = " usuario_id_INT = {$objParametros->usuarioId} ";
                }


                
                if (isset($objParametros->veiculoId)) 
                {
                    $arrUpdateFields[] = " veiculo_id_INT = {$objParametros->veiculoId} ";
                }


                
                if (isset($objParametros->relatorioId)) 
                {
                    $arrUpdateFields[] = " relatorio_id_INT = {$objParametros->relatorioId} ";
                }


                
                if (isset($objParametros->protocoloInt)) 
                {
                    $arrUpdateFields[] = " protocolo_INT = {$objParametros->protocoloInt} ";
                }



            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE nota SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";

            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null)
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function update($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($this->empresaId)) 
                {                                      
                    $arrUpdateFields[] = " empresa_id_INT = {$this->empresaId} ";
                }


                
                if (isset($this->pessoaId)) 
                {                                      
                    $arrUpdateFields[] = " pessoa_id_INT = {$this->pessoaId} ";
                }


                
                if (isset($this->usuarioId)) 
                {                                      
                    $arrUpdateFields[] = " usuario_id_INT = {$this->usuarioId} ";
                }


                
                if (isset($this->veiculoId)) 
                {                                      
                    $arrUpdateFields[] = " veiculo_id_INT = {$this->veiculoId} ";
                }


                
                if (isset($this->relatorioId)) 
                {                                      
                    $arrUpdateFields[] = " relatorio_id_INT = {$this->relatorioId} ";
                }


                
                if (isset($this->protocoloInt)) 
                {                                      
                    $arrUpdateFields[] = " protocolo_INT = {$this->protocoloInt} ";
                }


            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE nota SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function updateByInstanceUsingParameters($id, $parameters, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($parameters->empresaId)) 
                {                                      
                    $arrUpdateFields[] = " empresa_id_INT = {$this->empresaId} ";
                }
                
                if (isset($parameters->pessoaId)) 
                {                                      
                    $arrUpdateFields[] = " pessoa_id_INT = {$this->pessoaId} ";
                }
                
                if (isset($parameters->usuarioId)) 
                {                                      
                    $arrUpdateFields[] = " usuario_id_INT = {$this->usuarioId} ";
                }
                
                if (isset($parameters->veiculoId)) 
                {                                      
                    $arrUpdateFields[] = " veiculo_id_INT = {$this->veiculoId} ";
                }
                
                if (isset($parameters->relatorioId)) 
                {                                      
                    $arrUpdateFields[] = " relatorio_id_INT = {$this->relatorioId} ";
                }
                
                if (isset($parameters->protocoloInt)) 
                {                                      
                    $arrUpdateFields[] = " protocolo_INT = {$this->protocoloInt} ";
                }
            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE nota SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }

}

?>
