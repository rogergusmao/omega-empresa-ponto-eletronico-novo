<?php

include("../php/funcoes.php");
include("../php/constants.php");
include("../php/database_config.php");

$imei = Helper::POST("imei");

if (strlen($imei)) {

    $strImei = ".{$imei}";

}

$nomeDoArquivoDB = "database{$strImei}.db";
$nomeDoArquivoZip = "database{$strImei}.zip";
$diretorioApartirDaRaiz = "recursos/sync/databases";
$diretorio = Helper::acharRaiz() . $diretorioApartirDaRaiz;
$banco = new Database();

if (is_file("{$diretorio}/{$nomeDoArquivoDB}")) {

    unlink("{$diretorio}/{$nomeDoArquivoDB}");

}

if (is_file("{$diretorio}/{$nomeDoArquivoZip}")) {

    unlink("{$diretorio}/{$nomeDoArquivoZip}");

}

$banco->query("SELECT DISTINCT nome 
               FROM sistema_tabela 
               WHERE (frequencia_sincronizador_INT = -1 OR frequencia_sincronizador_INT > 0) AND 
               transmissao_web_para_mobile_BOOLEAN = 1 AND 
               banco_mobile_BOOLEAN = 1");


$arrTabelas = Helper::getResultSetToArrayDeUmCampo($banco->result);
$arrTabelasUtilizar = array();

$banco->query("SHOW TABLES");

while ($dadosTabelasExistentes = $banco->fetchArray()) {

    $tabelaExistente = $dadosTabelasExistentes[0];

    if (in_array($tabelaExistente, $arrTabelas)) {

        $arrTabelasUtilizar[] = $tabelaExistente;

    }

}


if (is_array($arrTabelasUtilizar)) {

    $strTabelas = implode(" ", $arrTabelasUtilizar);

} else {

    $strTabelas = "";

}

$configuracaoDatabasePrincipal = Registry::get('ConfiguracaoDatabasePrincipal');

$host = $configuracaoDatabasePrincipal->host;
$usuario = $configuracaoDatabasePrincipal->usuario;
$senha = $configuracaoDatabasePrincipal->senha;
$banco = $configuracaoDatabasePrincipal->DBName;

Helper::shellExec("sh ./db_converter.original.sh --host={$host} --user={$usuario} --password={$senha} {$banco} {$strTabelas} | sqlite3 {$diretorio}/{$nomeDoArquivoDB}");

//shell_exec("sh ./db_converter.original.sh --host={$host} --user={$usuario} --password={$senha} {$banco} {$strTabelas} > teste.sqlite.sql");
//$comando = "mysqldump --compatible=ansi --default-character-set=latin1 --skip-extended-insert --compact --host={$host} --user={$usuario} --password={$senha} {$banco} {$strTabelas} > teste.sql";
//shell_exec("mysqldump --compatible=ansi --skip-extended-insert --compact --host={$host} --user={$usuario} --password={$senha} {$banco} {$strTabelas} > teste.sql");

Helper::criarArquivoZip(array("{$diretorio}/{$nomeDoArquivoDB}"), "{$diretorio}/{$nomeDoArquivoZip}", true);

$tamanhoEmBytes = filesize("{$diretorio}/{$nomeDoArquivoZip}");
$timestamp = filemtime("{$diretorio}/{$nomeDoArquivoZip}");
$urlAcesso = "http://" . DOMINIO_DE_ACESSO . "/" . $diretorioApartirDaRaiz . "/" . $nomeDoArquivoZip;

print "{$tamanhoEmBytes};{$timestamp};{$urlAcesso}";

?>