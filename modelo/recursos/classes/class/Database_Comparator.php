<?php

class Database_Comparator
{
    public $mainDatabase;
    public $arrComparedDatabases;
    public $arrComparedTables;
    public $arrMainDatabaseTables;
    public $corporacaId;

    public function __construct($mainDatabase, $arrComparedDatabases, $arrComparedTables, $corporacaId)
    {
        if ($mainDatabase instanceof Database)
        {
            $this->mainDatabase = $mainDatabase;
        }
        else
        {
            throw new Exception("The 'Main Database' parameter is invalid.");
        }

        $this->mainDatabase = $mainDatabase;

        $this->corporacaId = corporacaId;

        if (is_array($arrComparedDatabases))
        {
            $this->arrComparedDatabases = $arrComparedDatabases;
        }
        elseif ($arrComparedDatabases instanceof Database_SQLite)
        {
            $this->arrComparedDatabases = array();
            $this->arrComparedDatabases[] = $arrComparedDatabases;
        }
        else
        {
            throw new Exception("The 'Compared Databases' parameter is invalid.");
        }

        $this->arrMainDatabaseTables = $this->mainDatabase->getTablesArray();

        foreach ($arrComparedTables as $comparedTable)
        {
            if (in_array($comparedTable, $this->arrMainDatabaseTables))
            {
                $this->arrComparedTables[] = $comparedTable;
            }
        }
    }

    public function compareDatabases($indexOfComparedDatabases)
    {
        $comparisonResults = new Database_Comparison_Result();

        if (count($this->arrComparedDatabases) - 1 > $indexOfComparedDatabases)
        {
            throw new Exception("The compared database index is invalid.");
        }

        $comparedDatabase = $this->arrComparedDatabases[$indexOfComparedDatabases];
        $comparedDatabaseTables = $comparedDatabase->getTablesArray();

        $comparisonResults->databaseFilename = $comparedDatabase->getFilename();

        foreach ($this->arrComparedTables as $comparedTable)
        {
            if (in_array($comparedTable, $comparedDatabaseTables))
            {
                $arrFields = $this->mainDatabase->getFieldsArray($comparedTable);
                $arrPrimaryKey = $this->mainDatabase->getPrimaryKeyFieldsArray($comparedTable);

                $this->organizeFieldOrder($arrFields, $arrPrimaryKey);
                $strFields = implode(", ", $arrFields);

                $whereCorporacao = "";
                if (array_search("corporacao_id_INT", $arrFields) != false)
                {
                    $whereCorporacao .= " WHERE corporacao_id_INT = {$this->corporacaId} ";
                }

                $q = "SELECT {$strFields} FROM {$comparedTable} $whereCorporacao ORDER BY {$this->getPrimaryKeyAsString($arrPrimaryKey)}";

                $mainResultSet = $this->mainDatabase->query($q, false);
                $secondaryResultSet = $comparedDatabase->query("SELECT {$strFields} FROM {$comparedTable} $whereCorporacao ORDER BY {$this->getPrimaryKeyAsString($arrPrimaryKey)}", false);

                $tableComparisonResults = new Table_Comparison_Result();
                $tableComparisonResults->tableName = $comparedTable;
                $tableComparisonResults->columns = $arrFields;
                $tableComparisonResults->overallResult = ComparisonResults::IDENTICAL;
                if ($this->mainDatabase->errCode !== false)
                {
                    $tableComparisonResults->overallResult = ComparisonResults::MAIN_QUERY_ERROR;
                    $tableComparisonResults->errorMessage = "MySQL error {$this->mainDatabase->errCode}: {$this->mainDatabase->errMessage}";
                }
                elseif (strlen($this->secondaryDatabase->errMessage))
                {
                    $tableComparisonResults->overallResult = ComparisonResults::SECONDARY_QUERY_ERROR;
                    $tableComparisonResults->errorMessage = "SQLite error: {$comparedDatabase->errMessage}";
                }
                else
                {
                    $this->compareRecords($mainResultSet, $secondaryResultSet, $arrPrimaryKey, $comparedDatabase, $tableComparisonResults);

                    if ($tableComparisonResults->overallResult == ComparisonResults::DIFFERENT)
                    {
                        $comparisonResults->overallResult = ComparisonResults::DIFFERENT;
                    }
                }

                $comparisonResults->addTableResult($tableComparisonResults);
            }
            else
            {
                $tableComparisonResults = new Table_Comparison_Result();
                $tableComparisonResults->tableExists = false;

                $comparisonResults->addTableResult($tableComparisonResults);
            }
        }

        return ($comparisonResults);
    }

    public function getPrimaryKeyAsString($arrPrimaryKey)
    {
        return implode(",", $arrPrimaryKey);
    }

    public function organizeFieldOrder(&$arrFields, $arrPrimaryKey)
    {
        for ($i = count($arrPrimaryKey) - 1; $i >= 0; $i--)
        {
            $primaryKey = $arrPrimaryKey[$i];
            $idField = array_search($primaryKey, $arrFields);
            if ($idField !== false)
            {
                unset($arrFields[$idField]);
                array_unshift($arrFields, $primaryKey);
            }
        }

        $arrFields = array_values($arrFields);
    }

    public function compareRecords(&$mainResultSet, &$comparedResultSet, &$arrPrimaryKeyFields, &$comparedDatabase, Table_Comparison_Result &$tableComparisonResults)
    {
        $primaryKeyNumberOfColumns = count($arrPrimaryKeyFields);
        $numberOfColumns = null;
        $identical = true;

        $rowMainDb = null;
        $rowSecondaryDb = null;

        $avancarMain = ($this->mainDatabase->rows() > 0) ? true : false;
        $avancarSecondary = ($comparedDatabase->rows() > 0) ? true : false;

        $loopSize = $comparedDatabase->rows() > $this->mainDatabase->rows() ? $comparedDatabase->rows() : $this->mainDatabase->rows();
        for ($i = 0; $i < $loopSize; $i++)
        {
            $pkMainMenor = false;
            $pkSecondaryMenor = false;

            if ($avancarMain)
            {
                $rowMainDb = mysqli_fetch_array($mainResultSet, MYSQL_NUM);
            }

            if ($avancarSecondary)
            {
                $rowSecondaryDb = $comparedResultSet->fetch(PDO::FETCH_NUM);
            }

            if ($numberOfColumns === null)
            {
                $numberOfColumns = count($rowMainDb);
            }

            if ($rowMainDb === null)
            {
                $pkMainMenor = true;
                $identical = false;
            }
            elseif ($pkSecondaryMenor === null)
            {
                $pkSecondaryMenor = true;
                $identical = false;
            }
            else
            {
                for ($j = 0; $j < $primaryKeyNumberOfColumns; $j++)
                {
                    if ($rowMainDb[$j] < $rowSecondaryDb[$j])
                    {
                        $pkMainMenor = true;
                        $identical = false;
                        break;
                    }
                    elseif ($rowMainDb[$j] > $rowSecondaryDb[$j])
                    {
                        $pkSecondaryMenor = true;
                        $identical = false;
                        break;
                    }
                }
            }

            //chaves primarias sao iguais
            if ($pkMainMenor === false && $pkSecondaryMenor === false)
            {
                //come�a apos a chave primaria, pois a mesma j� foi comparada
                for ($j = $primaryKeyNumberOfColumns; $j < $numberOfColumns; $j++)
                {
                    if ($rowMainDb[$j] != $rowSecondaryDb[$j])
                    {
                        $tableComparisonResults->recordsDiferentes[] = array($rowMainDb, $rowSecondaryDb);
                        $identical = false;
                        break;
                    }
                }

                $avancarMain = true;
                $avancarSecondary = true;
            }
            elseif ($pkMainMenor)
            {
                $tableComparisonResults->recordsSobrando[] = $rowSecondaryDb;
                $avancarMain = false;
                $avancarSecondary = true;
                $identical = false;
            }
            else
            {
                $tableComparisonResults->recordsFaltando[] = $rowMainDb;
                $avancarMain = true;
                $avancarSecondary = false;
                $identical = false;
            }
        }

        $tableComparisonResults->overallResult = ($identical) ? ComparisonResults::IDENTICAL : ComparisonResults::DIFFERENT;
    }

    public static function getStandardizedType($type, $sourceDatabaseType)
    {
        if ($sourceDatabaseType == DatabaseTypes::MYSQL)
        {
            if (strpos($type, "(") > -1)
            {
                $type = substr($type, 0, strpos($type, "("));
            }

            switch (strtolower($type))
            {
                case "int":
                case "tinyint":
                    return ColumnTypes::INTEGER;
                    break;
                case "float":
                case "double":
                case "decimal":
                    return ColumnTypes::FLOAT;
                    break;
                case "varchar":
                case "text":
                    return ColumnTypes::TEXT;
                    break;
                case "date":
                    return ColumnTypes::DATE;
                    break;
                case "time":
                    return ColumnTypes::TIME;
                    break;
                case "datetime":
                    return ColumnTypes::DATETIME;
                    break;
                case "blob":
                    return ColumnTypes::BLOB;
                    break;
            }
        }
        elseif ($sourceDatabaseType == DatabaseTypes::SQLITE)
        {
            switch (strtolower($type))
            {
                case "integer":
                    return ColumnTypes::INTEGER;
                    break;
                case "real":
                    return ColumnTypes::FLOAT;
                    break;
                case "varchar":
                case "text":
                    return ColumnTypes::TEXT;
                    break;
                case "blob":
                    return ColumnTypes::BLOB;
                    break;
            }
        }
    }
}

abstract class DatabaseTypes
{
    const MYSQL = 0;
    const SQLITE = 1;
}

abstract class ComparisonResults
{
    const IDENTICAL = 0;
    const DIFFERENT = 1;
    const MAIN_QUERY_ERROR = 2;
    const SECONDARY_QUERY_ERROR = 3;
}

abstract class ColumnTypes
{
    const INTEGER = "INTEGER";
    const FLOAT = "FLOAT";
    const TEXT = "TEXT";
    const DATE = "DATE";
    const TIME = "TIME";
    const DATETIME = "DATETIME";
    const BLOB = "BLOB";
}

class Database_Comparison_Result
{
    public $databaseFilename;
    public $overallResult = ComparisonResults::IDENTICAL;
    public $arrTableResults = array();

    public function addTableResult($tableResult)
    {
        //colocando apenas os resultados diferentes
        if ($tableResult->overallResult != ComparisonResults::IDENTICAL)
        {
            $this->arrTableResults[] = $tableResult;
        }
    }
}

class Table_Comparison_Result
{
    public $tableName;
    public $overallResult = ComparisonResults::IDENTICAL;
    public $errorMessage = "";
    public $tableExists = true;
    public $columns;
    public $columnsFaltando;
    public $columnsSobrando;
    public $columnsDiferentes;
    public $recordsSobrando;
    public $recordsFaltando;
    public $recordsDiferentes;
}

