"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
require("react-datetime/css/react-datetime.css");
var React = require("react");
var Datetime = require("react-datetime");
var react_bootstrap_1 = require("react-bootstrap");
var uiComponente_1 = require("lib/uiComponente");
var model_1 = require("aplicacao/modulo/calendario/model");
var geral_1 = require("aplicacao/comum/geral");
var geral_2 = require("lib/geral");
var CorrigirPonto = (function (_super) {
    __extends(CorrigirPonto, _super);
    function CorrigirPonto(props, reactContext) {
        var _this = _super.call(this, props || {}, reactContext) || this;
        _this.show = _this.show.bind(_this);
        _this.close = _this.close.bind(_this);
        _this.state = { show: false };
        return _this;
    }
    CorrigirPonto.prototype.close = function () {
        this.setState({ show: false, dataPonto: null });
    };
    CorrigirPonto.prototype.show = function () {
        this.setState({ show: true, dataPonto: null });
    };
    CorrigirPonto.prototype.onSalvarDadosClick = function () {
        var dataHoraPonto = this.state.dataPonto;
        if (geral_1.Helper.isNullOrEmpty(dataHoraPonto)) {
            this.setState({ dataEmBranco: true });
        }
        else if ((this.props.dataMinimaPermitida != null && dataHoraPonto.isBefore(this.props.dataMinimaPermitida))
            || (this.props.dataMaximaPermitida != null && dataHoraPonto.isAfter(this.props.dataMaximaPermitida))) {
            this.setState({ dataInvalida: true });
        }
        else {
            var novoPonto = new model_1.PontoEletronico();
            //grava no modelo o id do ponto de referencia
            novoPonto.idPonto = this.props.evento.pontoInicial.idPonto;
            novoPonto.idPessoa = this.props.evento.idPessoa;
            novoPonto.dataPonto = this.state.dataPonto;
            novoPonto.tipoPonto = this.props.evento.pontoInicial.tipoPonto == geral_1.TipoPontoEletronico.PONTO_ENTRADA ?
                geral_1.TipoPontoEletronico.PONTO_SAIDA : geral_1.TipoPontoEletronico.PONTO_ENTRADA;
            //fecha a modal
            this.close();
            //chama função no contexto da aplicação para persistir os dados
            this.props.onSalvarDadosClick(novoPonto);
        }
    };
    CorrigirPonto.prototype.isDayValid = function (date) {
        if (this.props.dataMinimaPermitida != null && date.isBefore(this.props.dataMinimaPermitida.clone().startOf('day'))) {
            return false;
        }
        if (this.props.dataMaximaPermitida != null && date.isAfter(this.props.dataMaximaPermitida.clone().endOf('day'))) {
            return false;
        }
        return true;
    };
    CorrigirPonto.prototype.corrigirControles = function () {
        setTimeout(function () {
            $('.rdtNext span').html('&gt;');
            $('.rdtPrev span').html('&lt;');
            $('.rdtCounters .rdtBtn:first-child').html('&xwedge;');
            $('.rdtCounters .rdtBtn:last-child').html('&xvee;');
        });
    };
    CorrigirPonto.prototype.render = function () {
        var _this = this;
        //corrige caracteres do controle de data
        this.corrigirControles();
        debugger;
        var eventoCorrente = this.props.evento;
        var conteudoPontoExistente;
        var conteudoPontoASerCriado;
        var labelPontoExistente = null;
        var labelPontoASerCriado = null;
        if (eventoCorrente.pontoInicial.tipoPonto == geral_1.TipoPontoEletronico.PONTO_ENTRADA) {
            labelPontoExistente = "Ponto de Entrada";
            labelPontoASerCriado = "Ponto de Saida";
        }
        else if (eventoCorrente.pontoInicial.tipoPonto == geral_1.TipoPontoEletronico.PONTO_SAIDA) {
            labelPontoExistente = "Ponto de Saida";
            labelPontoASerCriado = "Ponto de Entrada";
        }
        var mensagemDatasMinimasMaximasPermtidas = [];
        if (this.props.dataMinimaPermitida) {
            mensagemDatasMinimasMaximasPermtidas.push(<p className="mb-0">Menor data permitida: {geral_2.MomentHelper.formatMomentToDatetimeExibicao(this.props.dataMinimaPermitida)}</p>);
        }
        if (this.props.dataMaximaPermitida) {
            mensagemDatasMinimasMaximasPermtidas.push(<p className="mb-0">Maior data permitida: {geral_2.MomentHelper.formatMomentToDatetimeExibicao(this.props.dataMaximaPermitida)}</p>);
        }
        conteudoPontoExistente = (<div className="mb-3" key="ponto-existente">
                <div className="lead text-contrast mb-1">{labelPontoExistente}:</div>

                <address>
                    <strong>{eventoCorrente.pontoInicial.nomePessoa}</strong><br />              
                    Identificador do Ponto Eletrônico: {eventoCorrente.pontoInicial.idPonto}<br />              
                    Data: {eventoCorrente.pontoInicial.dataPonto.toDate().toLocaleDateString()}<br />
                    Hora: {eventoCorrente.pontoInicial.dataPonto.toDate().toLocaleTimeString()}<br /> 
                </address>

            </div>);
        conteudoPontoASerCriado = (<div key="ponto-a-ser-criado">
                <div className="lead text-contrast mb-1">{labelPontoASerCriado}:</div>

                <strong>{eventoCorrente.pontoInicial.nomePessoa}</strong><br />  
                Definir data e hora:
                <Datetime className="mb-1 datetime-picker" isValidDate={this.isDayValid.bind(this)} value={this.state.dataPonto} onViewModeChange={function () { _this.corrigirControles(); }} onFocus={function () { _this.corrigirControles(); }} onChange={function (selectedDate) { return _this.setState({ dataPonto: selectedDate }); }}/>
                {mensagemDatasMinimasMaximasPermtidas}
            </div>);
        var conteudoCorpo;
        if (eventoCorrente.pontoInicial.tipoPonto == geral_1.TipoPontoEletronico.PONTO_ENTRADA) {
            conteudoCorpo = [conteudoPontoExistente, conteudoPontoASerCriado];
        }
        else if (eventoCorrente.pontoInicial.tipoPonto == geral_1.TipoPontoEletronico.PONTO_SAIDA) {
            conteudoCorpo = [conteudoPontoASerCriado, conteudoPontoExistente];
        }
        var mensagemValidacao = null;
        var mensagemValidacaoJSX = null;
        if (this.state.dataEmBranco) {
            mensagemValidacao = "A data do ponto deve ser informada.";
        }
        else if (this.state.dataInvalida) {
            if (this.props.dataMinimaPermitida != null && this.props.dataMaximaPermitida != null) {
                mensagemValidacao = "A data do ponto deve estar compreendida entre " + geral_2.MomentHelper.formatMomentToDatetimeExibicao(this.props.dataMinimaPermitida) + " e " + geral_2.MomentHelper.formatMomentToDatetimeExibicao(this.props.dataMaximaPermitida) + ".";
            }
            else if (this.props.dataMinimaPermitida != null) {
                mensagemValidacao = "A data do ponto deve ser maior que " + geral_2.MomentHelper.formatMomentToDatetimeExibicao(this.props.dataMinimaPermitida);
            }
            else if (this.props.dataMaximaPermitida != null) {
                mensagemValidacao = "A data do ponto deve ser menor que " + geral_2.MomentHelper.formatMomentToDatetimeExibicao(this.props.dataMaximaPermitida);
            }
        }
        if (mensagemValidacao != null) {
            mensagemValidacaoJSX = (<react_bootstrap_1.Alert bsStyle="warning" className="mb-2">
                    {mensagemValidacao}
                </react_bootstrap_1.Alert>);
        }
        return (<react_bootstrap_1.Modal show={this.state.show} onHide={this.close}>
                <react_bootstrap_1.Modal.Header closeButton>
                    Corrigir Ponto Eletrônico
                </react_bootstrap_1.Modal.Header>
                <react_bootstrap_1.Modal.Body>
                    {mensagemValidacaoJSX}
                    {conteudoCorpo}                
                </react_bootstrap_1.Modal.Body>
                <react_bootstrap_1.Modal.Footer>
                    <react_bootstrap_1.Button bsStyle="success" onClick={this.onSalvarDadosClick.bind(this)}>
                        Salvar Dados
                    </react_bootstrap_1.Button>
                    <react_bootstrap_1.Button bsStyle="danger" onClick={this.close.bind(this)}>
                        Cancelar
                    </react_bootstrap_1.Button>                    
                </react_bootstrap_1.Modal.Footer>
            </react_bootstrap_1.Modal>);
    };
    return CorrigirPonto;
}(uiComponente_1.ReactComponent));
exports.CorrigirPonto = CorrigirPonto;
