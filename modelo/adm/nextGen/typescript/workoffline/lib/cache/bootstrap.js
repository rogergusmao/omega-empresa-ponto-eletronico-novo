"use strict";
exports.__esModule = true;
var Factory_1 = require("./Cache/Backend/Factory");
var LocalStorage_1 = require("./Cache/Backend/LocalStorage/LocalStorage");
// Register the backends
Factory_1.Cache_Backend_Factory.addBackendDefinition(new LocalStorage_1.Cache_Backend_LocalStorage());
