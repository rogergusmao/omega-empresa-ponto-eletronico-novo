/*

 Arquivo gerado atrav�s de gerador de c�digo em 07/01/2018 as 15:54:10.
 Para que o arquivo n�o seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

 Tabela correspondente: pais
 Sobrescrita de dados: ---SOBRESCRITA_NAO_AUTORIZADA---

 */

returnObject.alterarEmailUsuario = function (idUsuario, emailNovo)
{
    var parameters = {
        idUsuario: idUsuario,
        emailNovo: emailNovo
    };

    debugger;
    return $http({
        url: baseURLAdm + "/webservice.php?class=EXTDAO_Usuario&action=alterarEmailUsuario",
        method: "POST",
        data: parameters,
        headers: {
            'Content-Type': 'application/json'
        }
    });

};

returnObject.alterarEmailAndSenhaUsuario = function (idUsuario, emailNovo, senha)
{
    var parameters = {
        idUsuario: idUsuario,
        emailNovo: emailNovo,
        senha: senha
    };

    return $http({
        url: baseURLAdm + "/webservice.php?class=EXTDAO_Usuario&action=alterarEmailAndSenhaUsuario",
        method: "POST",
        data: parameters,
        headers: {
            'Content-Type': 'application/json'
        }
    });

};
