"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var model_1 = require("lib/model");
var geral_1 = require("aplicacao/comum/geral");
var geral_2 = require("lib/geral");
var Usuario = (function (_super) {
    __extends(Usuario, _super);
    function Usuario() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Usuario.parseFromRemoteObject = function (remoteObject) {
        var instance = new Usuario();
        instance.latitude = geral_2.GeoLocationHelper.getFloatValue(remoteObject.latitude);
        instance.longitude = geral_2.GeoLocationHelper.getFloatValue(remoteObject.longitude);
        instance.velocidade = geral_1.Helper.parseInt(remoteObject.velocidade);
        instance.sexoUsuario = geral_1.Helper.parseInt(remoteObject.idSexoUsuario);
        instance.nomeUsuario = remoteObject.nomeUsuario;
        instance.idUsuario = geral_1.Helper.parseInt(remoteObject.idUsuario);
        instance.fonteInformacao = geral_1.Helper.parseInt(remoteObject.fonteInformacao);
        instance.dataPosicao = DateUtil.getDateObject(geral_1.Helper.parseInt(remoteObject.cadastroSec) + geral_1.Helper.parseInt(remoteObject.cadastroOffsec));
        return instance;
    };
    return Usuario;
}(model_1.Model));
exports.Usuario = Usuario;
