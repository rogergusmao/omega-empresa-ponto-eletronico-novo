<?php


        /*

        Arquivo gerado através de gerador de código em 12/05/2018 as 14:47:19.
        Para que o arquivo não seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

        Tabela correspondente: negociacao_divida
        Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

        */
        


class DAO_Negociacao_divida extends Generic_DAO
                        {
                            const NOME_ENTIDADE = "negociacao_divida";

                            public static $nomeTabela = null;
                            public static $idSistemaTabela = false;
                            public static $databaseFieldsRelatedAttributes = null;
	                        public static $databaseFieldTypes = null;
	                        public static $databaseFieldNames = null;
	                        public static $listAliasTypes = null;
	                        public static $listAliasRelatedAttributes = null;
	                        
	                        public $id;
public $devedorEmpresaId;
public $devedorPessoaId;
public $negociadorUsuarioId;
public $cadastroSec;
public $cadastroOffsec;
public $resultadoEmpresaVendaId;
public $relatorioId;
public $registroEstadoId;
public $protocoloInt;
public $corporacaoId;

public $labelId;
public $labelDevedorEmpresaId;
public $labelDevedorPessoaId;
public $labelNegociadorUsuarioId;
public $labelCadastroSec;
public $labelCadastroOffsec;
public $labelResultadoEmpresaVendaId;
public $labelRelatorioId;
public $labelRegistroEstadoId;
public $labelProtocoloInt;
public $labelCorporacaoId;

public static $arrPK = array("id");

public function __construct($configDAO = null)
        {
            parent::__construct($configDAO);
            static::__setStaticValues($this->database);           
        }

public static function idSistemaTabela($databaseObject = null){
            if(!(static::$idSistemaTabela))
            {
                static::$idSistemaTabela = EXTDAO_Sistema_tabela::getIdDaSistemaTabela(static::nomeTabela(), $databaseObject);
            }
            return static::$idSistemaTabela;
        }

public function getIdSistemaTabela(){
            return static::idSistemaTabela();
        }

public static function nomeTabela(){
            if(is_null(static::$nomeTabela))
            {
                static::$nomeTabela = "negociacao_divida";
            }
            return static::$nomeTabela;
        }

public function getNomeTabela(){
            return static::nomeTabela();
        }

public static function __setStaticValues($databaseObject = null)
                   {    
                        static::nomeTabela();
                        
                        static::idSistemaTabela($databaseObject);
                   
                        static::setDatabaseFieldNames();
                        static::setDatabaseFieldTypes();
                        static::setDatabaseFieldsRelatedAttributeNames();
                        
                        static::setListAliasTypes();
                        static::setListAliasRelatedAttributes();
                   }

public function getExpressaoLabel()
                   {
                        return "{id}";
                   }

public static function setDatabaseFieldNames()
        {
            if(is_null(static::$databaseFieldNames))
            {
                static::$databaseFieldNames = new stdClass();
                static::$databaseFieldNames->id = "id";
static::$databaseFieldNames->devedorEmpresaId = "devedor_empresa_id_INT";
static::$databaseFieldNames->devedorPessoaId = "devedor_pessoa_id_INT";
static::$databaseFieldNames->negociadorUsuarioId = "negociador_usuario_id_INT";
static::$databaseFieldNames->cadastroSec = "cadastro_SEC";
static::$databaseFieldNames->cadastroOffsec = "cadastro_OFFSEC";
static::$databaseFieldNames->resultadoEmpresaVendaId = "resultado_empresa_venda_id_INT";
static::$databaseFieldNames->relatorioId = "relatorio_id_INT";
static::$databaseFieldNames->registroEstadoId = "registro_estado_id_INT";
static::$databaseFieldNames->protocoloInt = "protocolo_INT";
static::$databaseFieldNames->corporacaoId = "corporacao_id_INT";

            }
                      
        }

public static function setDatabaseFieldTypes()
        {
            if(is_null(static::$databaseFieldTypes))
            {
                static::$databaseFieldTypes = new stdClass();
                static::$databaseFieldTypes->id = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->devedorEmpresaId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->devedorPessoaId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->negociadorUsuarioId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->cadastroSec = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$databaseFieldTypes->cadastroOffsec = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$databaseFieldTypes->resultadoEmpresaVendaId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->relatorioId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->registroEstadoId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->protocoloInt = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->corporacaoId = static::TIPO_VARIAVEL_INTEGER;

            }
            
        }

public static function setDatabaseFieldsRelatedAttributeNames()
        {
            if(is_null(static::$databaseFieldsRelatedAttributes))
            {
                static::$databaseFieldsRelatedAttributes = new stdClass();
                static::$databaseFieldsRelatedAttributes->id = "id";
static::$databaseFieldsRelatedAttributes->devedor_empresa_id_INT = "devedorEmpresaId";
static::$databaseFieldsRelatedAttributes->devedor_pessoa_id_INT = "devedorPessoaId";
static::$databaseFieldsRelatedAttributes->negociador_usuario_id_INT = "negociadorUsuarioId";
static::$databaseFieldsRelatedAttributes->cadastro_SEC = "cadastroSec";
static::$databaseFieldsRelatedAttributes->cadastro_OFFSEC = "cadastroOffsec";
static::$databaseFieldsRelatedAttributes->resultado_empresa_venda_id_INT = "resultadoEmpresaVendaId";
static::$databaseFieldsRelatedAttributes->relatorio_id_INT = "relatorioId";
static::$databaseFieldsRelatedAttributes->registro_estado_id_INT = "registroEstadoId";
static::$databaseFieldsRelatedAttributes->protocolo_INT = "protocoloInt";
static::$databaseFieldsRelatedAttributes->corporacao_id_INT = "corporacaoId";

            }           
        }

public function setAllFieldsToHumanFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["devedor_empresa_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["devedor_pessoa_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["negociador_usuario_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["cadastro_SEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["cadastro_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["resultado_empresa_venda_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["relatorio_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["registro_estado_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["protocolo_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
}

public function setAllFieldsToDatabaseFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["devedor_empresa_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["devedor_pessoa_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["negociador_usuario_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["cadastro_SEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["cadastro_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["resultado_empresa_venda_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["relatorio_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["registro_estado_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["protocolo_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
}

  public function getFkObjDevedor_empresa() 
                {
                    if($this->objDevedor_empresa == null)
                    {                        
                        $this->objDevedor_empresa = new EXTDAO_Devedor_empresa_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getDevedor_empresa_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objDevedor_empresa->clear();
                    }
                    elseif($this->objDevedor_empresa->getId() != $idFK)
                    {
                        $this->objDevedor_empresa->select($idFK);
                    }
                    return $this->objDevedor_empresa;
                }
  public function getFkObjDevedor_pessoa() 
                {
                    if($this->objDevedor_pessoa == null)
                    {                        
                        $this->objDevedor_pessoa = new EXTDAO_Devedor_pessoa_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getDevedor_pessoa_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objDevedor_pessoa->clear();
                    }
                    elseif($this->objDevedor_pessoa->getId() != $idFK)
                    {
                        $this->objDevedor_pessoa->select($idFK);
                    }
                    return $this->objDevedor_pessoa;
                }
  public function getFkObjNegociador_usuario() 
                {
                    if($this->objNegociador_usuario == null)
                    {                        
                        $this->objNegociador_usuario = new EXTDAO_Negociador_usuario_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getNegociador_usuario_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objNegociador_usuario->clear();
                    }
                    elseif($this->objNegociador_usuario->getId() != $idFK)
                    {
                        $this->objNegociador_usuario->select($idFK);
                    }
                    return $this->objNegociador_usuario;
                }
  public function getFkObjResultado_empresa_venda() 
                {
                    if($this->objResultado_empresa_venda == null)
                    {                        
                        $this->objResultado_empresa_venda = new EXTDAO_Resultado_empresa_venda_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getResultado_empresa_venda_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objResultado_empresa_venda->clear();
                    }
                    elseif($this->objResultado_empresa_venda->getId() != $idFK)
                    {
                        $this->objResultado_empresa_venda->select($idFK);
                    }
                    return $this->objResultado_empresa_venda;
                }
  public function getFkObjRelatorio() 
                {
                    if($this->objRelatorio == null)
                    {                        
                        $this->objRelatorio = new EXTDAO_Relatorio_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getRelatorio_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objRelatorio->clear();
                    }
                    elseif($this->objRelatorio->getId() != $idFK)
                    {
                        $this->objRelatorio->select($idFK);
                    }
                    return $this->objRelatorio;
                }
  public function getFkObjRegistro_estado() 
                {
                    if($this->objRegistro_estado == null)
                    {                        
                        $this->objRegistro_estado = new EXTDAO_Registro_estado_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getRegistro_estado_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objRegistro_estado->clear();
                    }
                    elseif($this->objRegistro_estado->getId() != $idFK)
                    {
                        $this->objRegistro_estado->select($idFK);
                    }
                    return $this->objRegistro_estado;
                }
  public function getFkObjCorporacao() 
                {
                    if($this->objCorporacao == null)
                    {                        
                        $this->objCorporacao = new EXTDAO_Corporacao_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getCorporacao_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objCorporacao->clear();
                    }
                    elseif($this->objCorporacao->getId() != $idFK)
                    {
                        $this->objCorporacao->select($idFK);
                    }
                    return $this->objCorporacao;
                }


public function setLabels()
        {$this->labelId = "";
$this->labelDevedorEmpresaId = "";
$this->labelDevedorPessoaId = "";
$this->labelNegociadorUsuarioId = "";
$this->labelCadastroSec = "";
$this->labelCadastroOffsec = "";
$this->labelResultadoEmpresaVendaId = "";
$this->labelRelatorioId = "";
$this->labelRegistroEstadoId = "";
$this->labelProtocoloInt = "";
$this->labelCorporacaoId = "";
}

public function __actionAdd($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                $this->setByObject($parameters);
                $this->setDataCadastroAndOffsetInSecondsFromUser($parameters);
                
                $this->formatarParaSQL();
                $msg = $this->insert(true);
                
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                return new Mensagem(null, I18N::getExpression("Negociacao divida adicionado com sucesso."));
                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionEdit($parameters = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try {

                $this->setByObject($parameters);
                $this->setAllFieldsToHumanFormat();
                
                $this->formatarParaSQL();
                $msg = $this->updateByInstanceUsingParameters($this->id, $parameters, true);
                if($msg != null && $msg->erro())
                {
                     return $msg;
                }
        
                return new Mensagem(null, I18N::getExpression("Negociacao divida editado com sucesso."));
            }
            catch(Exception $ex)
            {                
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionRemove($parameters = null)
        {
            $successulRemovalCount = 0;
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                foreach($parameters as $record)
                {
                    if(is_numeric($record->id))
                    {
                        $msg = $this->delete($record->id, true);
                        if($msg == null || $msg->ok())
                        {
                            $successulRemovalCount++;
                        }
                    }
                }
                
                if($successulRemovalCount > 0)
                {
                    $successMessage = count($successulRemovalCount) > 1 ? I18N::getExpression("{0} Negociacao divida foram removidos com sucesso.", $successulRemovalCount) : I18N::getExpression("Negociacao divida removido com sucesso.");
                    return new Mensagem(null, $successMessage);
                }
                else
                {
                    $errorMessage = count($parameters) > 1 ? I18N::getExpression("Falha ao remover Negociacao divida.") : I18N::getExpression("Falha ao remover Negociacao divida.");
                    return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, $errorMessage);                    
                }
            }
            catch (Exception $ex) 
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }

public function __getRecord($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }
            
            try
            {            
                $returnObject = new stdClass();            
                $whereClause = static::getWhereClauseForFilter($parameters->filterParameters);
                $query = "SELECT  id, devedor_empresa_id_INT, devedor_pessoa_id_INT, negociador_usuario_id_INT, cadastro_SEC, cadastro_OFFSEC, resultado_empresa_venda_id_INT, relatorio_id_INT, registro_estado_id_INT, protocolo_INT, corporacao_id_INT FROM negociacao_divida {$whereClause}";
    
                $msg = $this->database->queryMensagem($query);                
                if ($msg == null)
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Não foi encontrado nenhum registro"));
                    }                    
                    $resultSet = $this->database->result;
                    $resultObject = static::mapResultSetDataToRecord($resultSet);
                    
                    $returnObject->formData = $resultObject;
                    $returnObject->comboBoxesData = null;
                    if($parameters->loadComboBoxesData)
                    {
                        $this->setByObject($resultObject);
                        $returnObject->comboBoxesData =  $this->getFormComboBoxesData();
                    }                    
    
                    
    
                    return new Mensagem_generica($returnObject);
                } 
                else 
                {
                    return $msg;
                }
                
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }


        
        public function __getFilterComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
                
        public function __getFormComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
        
        protected function getFormComboBoxesData()
        {
            $comboBoxesData = new stdClass();
            $listParameters = new stdClass();
            $listParameters->listMappingType = static::LIST_MAPPING_TYPE_COMBOBOX;
            
            
                    
                    $objRelatorio = new EXTDAO_Relatorio();
                    $comboBoxesData->fieldRelatorioId = $objRelatorio->__getList($listParameters);
                    
                    $objRegistroEstado = new EXTDAO_Registro_estado();
                    $comboBoxesData->fieldRegistroEstadoId = $objRegistroEstado->__getList($listParameters);

            return $comboBoxesData;
        }
        
        


        
        public static function setListAliasTypes()
        {
            if(is_null(static::$listAliasTypes))
            {
                static::$listAliasTypes = new stdClass();
                static::$listAliasTypes->relatorio__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->registro_estado__nome = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->negociacao_divida__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->negociacao_divida__devedor_empresa_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->negociacao_divida__devedor_pessoa_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->negociacao_divida__negociador_usuario_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->negociacao_divida__cadastro_SEC = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$listAliasTypes->negociacao_divida__resultado_empresa_venda_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->negociacao_divida__tipo_relatorio_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->negociacao_divida__tipo_registro_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->negociacao_divida__protocolo_INT = static::TIPO_VARIAVEL_INTEGER;
            }        
        }
        
        public static function setListAliasRelatedAttributes()
        {
            if(is_null(static::$listAliasRelatedAttributes))
            {
                static::$listAliasRelatedAttributes = new stdClass();
                static::$listAliasRelatedAttributes->relatorio__id = "relatorioId";
static::$listAliasRelatedAttributes->registro_estado__nome = "registroEstadoNome";
static::$listAliasRelatedAttributes->negociacao_divida__id = "id";
static::$listAliasRelatedAttributes->negociacao_divida__devedor_empresa_id_INT = "devedorEmpresaId";
static::$listAliasRelatedAttributes->negociacao_divida__devedor_pessoa_id_INT = "devedorPessoaId";
static::$listAliasRelatedAttributes->negociacao_divida__negociador_usuario_id_INT = "negociadorUsuarioId";
static::$listAliasRelatedAttributes->negociacao_divida__cadastro_SEC = "cadastroSec";
static::$listAliasRelatedAttributes->negociacao_divida__resultado_empresa_venda_id_INT = "resultadoEmpresaVendaId";
static::$listAliasRelatedAttributes->negociacao_divida__tipo_relatorio_id_INT = "tipoRelatorioId";
static::$listAliasRelatedAttributes->negociacao_divida__tipo_registro_id_INT = "tipoRegistroId";
static::$listAliasRelatedAttributes->negociacao_divida__protocolo_INT = "protocoloInt";
            }         
        }

        public function __getList($parameters = null, $idCorporacao = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try
            {
                $filterParameters = new stdClass();
                if(isset($parameters->filterParameters))
                {
                    $filterParameters = $parameters->filterParameters;
                }
                                    
                            
            if(is_null($idCorporacao))
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            ListParameters::addFilterParameterToObject($filterParameters, "corporacaoId", $idCorporacao);           
                
                
                $sortingParameters = null;
                if(isset($parameters->sortingParameters))
                {
                    $sortingParameters = $parameters->sortingParameters;
                }
                
                $paginationParameters = null;
                if(isset($parameters->paginationParameters))
                {
                    $paginationParameters = $parameters->paginationParameters;
                }
                
                $listMappingType = static::getListMappingType($parameters->listMappingType);
                $mainTableAlias = "nd";

                $whereClause = static::getWhereClauseForFilter($filterParameters, $mainTableAlias);
                $orderByClause = static::getOrderByClauseForFilter($sortingParameters, $mainTableAlias);
                $limitClause = static::getLimitClauseForFilter($paginationParameters);

                $queryWithoutLimit = "SELECT nd.id FROM negociacao_divida nd {$whereClause}";
                $query = "SELECT r.id AS relatorio__id, re.nome AS registro_estado__nome, nd.id AS negociacao_divida__id, nd.devedor_empresa_id_INT AS negociacao_divida__devedor_empresa_id_INT, nd.devedor_pessoa_id_INT AS negociacao_divida__devedor_pessoa_id_INT, nd.negociador_usuario_id_INT AS negociacao_divida__negociador_usuario_id_INT, nd.cadastro_SEC AS negociacao_divida__cadastro_SEC, nd.resultado_empresa_venda_id_INT AS negociacao_divida__resultado_empresa_venda_id_INT, nd.tipo_relatorio_id_INT AS negociacao_divida__tipo_relatorio_id_INT, nd.tipo_registro_id_INT AS negociacao_divida__tipo_registro_id_INT, nd.protocolo_INT AS negociacao_divida__protocolo_INT FROM negociacao_divida nd LEFT JOIN relatorio r ON r.id = nd.relatorio_id_INT LEFT JOIN registro_estado re ON re.id = nd.registro_estado_id_INT {$whereClause} {$orderByClause} {$limitClause}";

                $msg = $this->database->queryMensagem($queryWithoutLimit);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                
                $totalNumberOfRecords = $this->database->rows();
                if($totalNumberOfRecords == 0)
                {
                    return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                }                        
                        
                $msg = $this->database->queryMensagem($query);
                
                //operacao realizaca com sucesso
                if ($msg == null) 
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                    }                        
                
                    $resultSet = $this->database->result;

                    $objReturn = new stdClass();
                    $objReturn->dataSet = static::mapResultSetDataToList($resultSet, $listMappingType, static::$listAliasTypes);

                    if(!Helper::isNullOrEmpty($paginationParameters->recordsPerPage) && !Helper::isNullOrEmpty($paginationParameters->currentPage))
                    {
                        $lastPage = ceil($totalNumberOfRecords / $paginationParameters->recordsPerPage);
                        $objReturn->paginationParameters = new PaginationParams($paginationParameters->recordsPerPage, $paginationParameters->currentPage, $lastPage);
                    }
                    
                    return new Mensagem_generica($objReturn);
                } 
                else
                {
                    return $msg;
                }                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }



public function getId()
            {
                return $this->id;
            }

public function getDevedorEmpresaId()
            {
                return $this->devedorEmpresaId;
            }

public function getDevedor_empresa_id_INT()
                {
                    return $this->devedorEmpresaId;
                }

public function getDevedorPessoaId()
            {
                return $this->devedorPessoaId;
            }

public function getDevedor_pessoa_id_INT()
                {
                    return $this->devedorPessoaId;
                }

public function getNegociadorUsuarioId()
            {
                return $this->negociadorUsuarioId;
            }

public function getNegociador_usuario_id_INT()
                {
                    return $this->negociadorUsuarioId;
                }

public function getCadastroSec()
            {
                return $this->cadastroSec;
            }

public function getCadastro_SEC()
                {
                    return $this->cadastroSec;
                }

public function getCadastroOffsec()
            {
                return $this->cadastroOffsec;
            }

public function getCadastro_OFFSEC()
                {
                    return $this->cadastroOffsec;
                }

public function getResultadoEmpresaVendaId()
            {
                return $this->resultadoEmpresaVendaId;
            }

public function getResultado_empresa_venda_id_INT()
                {
                    return $this->resultadoEmpresaVendaId;
                }

public function getRelatorioId()
            {
                return $this->relatorioId;
            }

public function getRelatorio_id_INT()
                {
                    return $this->relatorioId;
                }

public function getRegistroEstadoId()
            {
                return $this->registroEstadoId;
            }

public function getRegistro_estado_id_INT()
                {
                    return $this->registroEstadoId;
                }

public function getProtocoloInt()
            {
                return $this->protocoloInt;
            }

public function getProtocolo_INT()
                {
                    return $this->protocoloInt;
                }

public function getCorporacaoId()
            {
                return $this->corporacaoId;
            }

public function getCorporacao_id_INT()
                {
                    return $this->corporacaoId;
                }



function setId($value)
            {
                $this->id = $value;
            }

function setDevedorEmpresaId($value)
            {
                $this->devedorEmpresaId = $value;
            }

public function setDevedor_empresa_id_INT($value)
                { 
                    $this->devedorEmpresaId = $value; 
                }

function setDevedorPessoaId($value)
            {
                $this->devedorPessoaId = $value;
            }

public function setDevedor_pessoa_id_INT($value)
                { 
                    $this->devedorPessoaId = $value; 
                }

function setNegociadorUsuarioId($value)
            {
                $this->negociadorUsuarioId = $value;
            }

public function setNegociador_usuario_id_INT($value)
                { 
                    $this->negociadorUsuarioId = $value; 
                }

function setCadastroSec($value)
            {
                $this->cadastroSec = $value;
            }

public function setCadastro_SEC($value)
                { 
                    $this->cadastroSec = $value; 
                }

function setCadastroOffsec($value)
            {
                $this->cadastroOffsec = $value;
            }

public function setCadastro_OFFSEC($value)
                { 
                    $this->cadastroOffsec = $value; 
                }

function setResultadoEmpresaVendaId($value)
            {
                $this->resultadoEmpresaVendaId = $value;
            }

public function setResultado_empresa_venda_id_INT($value)
                { 
                    $this->resultadoEmpresaVendaId = $value; 
                }

function setRelatorioId($value)
            {
                $this->relatorioId = $value;
            }

public function setRelatorio_id_INT($value)
                { 
                    $this->relatorioId = $value; 
                }

function setRegistroEstadoId($value)
            {
                $this->registroEstadoId = $value;
            }

public function setRegistro_estado_id_INT($value)
                { 
                    $this->registroEstadoId = $value; 
                }

function setProtocoloInt($value)
            {
                $this->protocoloInt = $value;
            }

public function setProtocolo_INT($value)
                { 
                    $this->protocoloInt = $value; 
                }

function setCorporacaoId($value)
            {
                $this->corporacaoId = $value;
            }

public function setCorporacao_id_INT($value)
                { 
                    $this->corporacaoId = $value; 
                }



public function clear()
        {$this->id = null;
$this->devedorEmpresaId = null;
if($this->objDevedor_empresa != null) unset($this->objDevedor_empresa);
$this->devedorPessoaId = null;
if($this->objDevedor_pessoa != null) unset($this->objDevedor_pessoa);
$this->negociadorUsuarioId = null;
if($this->objNegociador_usuario != null) unset($this->objNegociador_usuario);
$this->cadastroSec = null;
$this->cadastroOffsec = null;
$this->resultadoEmpresaVendaId = null;
if($this->objResultado_empresa_venda != null) unset($this->objResultado_empresa_venda);
$this->relatorioId = null;
if($this->objRelatorio != null) unset($this->objRelatorio);
$this->registroEstadoId = null;
if($this->objRegistro_estado != null) unset($this->objRegistro_estado);
$this->protocoloInt = null;
$this->corporacaoId = null;
if($this->objCorporacao != null) unset($this->objCorporacao);
if(is_array($this->arrEstadosCampos)){
                        unset($this->arrEstadosCampos);
                     $this->arrEstadosCampos = array();
                    }
                }

public function formatarParaSQL()
        {
if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->id)){
$this->id = $this->formatarIntegerParaComandoSQL($this->id);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->id);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->devedorEmpresaId)){
$this->devedorEmpresaId = $this->formatarIntegerParaComandoSQL($this->devedorEmpresaId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->devedorEmpresaId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->devedorPessoaId)){
$this->devedorPessoaId = $this->formatarIntegerParaComandoSQL($this->devedorPessoaId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->devedorPessoaId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->negociadorUsuarioId)){
$this->negociadorUsuarioId = $this->formatarIntegerParaComandoSQL($this->negociadorUsuarioId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->negociadorUsuarioId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->cadastroSec)){
$this->cadastroSec = $this->formatarIntegerParaComandoSQL($this->cadastroSec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->cadastroSec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->cadastroOffsec)){
$this->cadastroOffsec = $this->formatarIntegerParaComandoSQL($this->cadastroOffsec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->cadastroOffsec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->resultadoEmpresaVendaId)){
$this->resultadoEmpresaVendaId = $this->formatarIntegerParaComandoSQL($this->resultadoEmpresaVendaId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->resultadoEmpresaVendaId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->relatorioId)){
$this->relatorioId = $this->formatarIntegerParaComandoSQL($this->relatorioId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->relatorioId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->registroEstadoId)){
$this->registroEstadoId = $this->formatarIntegerParaComandoSQL($this->registroEstadoId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->registroEstadoId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->protocoloInt)){
$this->protocoloInt = $this->formatarIntegerParaComandoSQL($this->protocoloInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->protocoloInt);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->corporacaoId)){
$this->corporacaoId = $this->formatarIntegerParaComandoSQL($this->corporacaoId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->corporacaoId);
}

}

public function formatarParaExibicao()
        {}

function select($id, $idCorporacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }   

            $sql = "SELECT id, devedor_empresa_id_INT, devedor_pessoa_id_INT, negociador_usuario_id_INT, cadastro_SEC, cadastro_OFFSEC, resultado_empresa_venda_id_INT, relatorio_id_INT, registro_estado_id_INT, protocolo_INT, corporacao_id_INT FROM negociacao_divida WHERE id = $id AND corporacao_id_INT = $idCorporacao";

            $msg = $this->database->queryMensagem($sql);
            $result = $this->database->result;

            if($msg != null && $msg->erro() )
            { 
            		$this->clear();
            		return $msg;
        	} 
        	else if($this->database->rows() == 0)
        	{
                return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nenhum registro foi encontrado."));
            }
        

            $row = $this->database->fetchArray(MYSQLI_NUM);
            
            		$this->id = $row[0];
		$this->devedorEmpresaId = $row[1];
		$this->devedorPessoaId = $row[2];
		$this->negociadorUsuarioId = $row[3];
		$this->cadastroSec = $row[4];
		$this->cadastroOffsec = $row[5];
		$this->resultadoEmpresaVendaId = $row[6];
		$this->relatorioId = $row[7];
		$this->registroEstadoId = $row[8];
		$this->protocoloInt = $row[9];

            
        }

public function delete($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (is_null($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }

            $sql = "DELETE FROM negociacao_divida WHERE id= $id AND corporacao_id_INT = $idCorporacao";
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                    && $sincronizar
                        && static::idSistemaTabela() != null
                            && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_REMOVE,
                                null,
                                null,
                                $id,
                                $this->database);
            }

            return $msg;
        }

public function insert($sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null, $opcoes = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $this->defineDataCadastroInSecondsIfNotDefined();
$this->defineDataCadastroOffsetInSecondsIfNotDefined();

            
            if (is_null($opcoes))
            {
                $opcoes = $this->opcoesDAO;
            }

            if (!is_numeric($this->id))
            {
                $this->id = "null";
                if ($opcoes == null || $opcoes[Database::OPCAO_GERAR_ID] != false) 
                {
                    $this->id = EXTDAO_Sistema_sequencia::gerarId(static::nomeTabela());                    
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }    
                    else if($msg == null)
                    {
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
                else 
                {
                    $this->id = null;
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao));
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }
                    else if($msg == null)
                    {
                        $this->id = $this->database->getLastInsertId();
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
            }
            else 
            {
                $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }                        
                else if($msg == null)
                {
                   $msg = new Mensagem_token();
                   $msg->mValor = $this->id;
                }
            }
            if($msg == null || $msg->ok())
            {                
                if($sincronizar
                    && static::idSistemaTabela() != null
                    && isset($this->id)){
                    
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_INSERT,
                                null,
                                null,
                                $this->id,
                                $this->database); 
                	}
            } 
            else
            {
                $this->id = null;
            }
            
            return $msg;
            
        }

public function getSQLInsert($idCorporacao){ 

return "INSERT INTO negociacao_divida (id, devedor_empresa_id_INT, devedor_pessoa_id_INT, negociador_usuario_id_INT, cadastro_SEC, cadastro_OFFSEC, resultado_empresa_venda_id_INT, relatorio_id_INT, registro_estado_id_INT, protocolo_INT, corporacao_id_INT) VALUES ( $this->id ,  $this->devedorEmpresaId ,  $this->devedorPessoaId ,  $this->negociadorUsuarioId ,  $this->cadastroSec ,  $this->cadastroOffsec ,  $this->resultadoEmpresaVendaId ,  $this->relatorioId ,  $this->registroEstadoId ,  $this->protocoloInt ,  $idCorporacao ) "; 

}




        
        public function updateByParameters($id, $objParametros, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao))
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();

            
                
                if (isset($objParametros->devedorEmpresaId)) 
                {
                    $arrUpdateFields[] = " devedor_empresa_id_INT = {$objParametros->devedorEmpresaId} ";
                }


                
                if (isset($objParametros->devedorPessoaId)) 
                {
                    $arrUpdateFields[] = " devedor_pessoa_id_INT = {$objParametros->devedorPessoaId} ";
                }


                
                if (isset($objParametros->negociadorUsuarioId)) 
                {
                    $arrUpdateFields[] = " negociador_usuario_id_INT = {$objParametros->negociadorUsuarioId} ";
                }


                
                if (isset($objParametros->cadastroSec)) 
                {
                    $arrUpdateFields[] = " cadastro_SEC = {$objParametros->cadastroSec} ";
                }


                
                if (isset($objParametros->cadastroOffsec)) 
                {
                    $arrUpdateFields[] = " cadastro_OFFSEC = {$objParametros->cadastroOffsec} ";
                }


                
                if (isset($objParametros->resultadoEmpresaVendaId)) 
                {
                    $arrUpdateFields[] = " resultado_empresa_venda_id_INT = {$objParametros->resultadoEmpresaVendaId} ";
                }


                
                if (isset($objParametros->relatorioId)) 
                {
                    $arrUpdateFields[] = " relatorio_id_INT = {$objParametros->relatorioId} ";
                }


                
                if (isset($objParametros->registroEstadoId)) 
                {
                    $arrUpdateFields[] = " registro_estado_id_INT = {$objParametros->registroEstadoId} ";
                }


                
                if (isset($objParametros->protocoloInt)) 
                {
                    $arrUpdateFields[] = " protocolo_INT = {$objParametros->protocoloInt} ";
                }



            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE negociacao_divida SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";

            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null)
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function update($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($this->devedorEmpresaId)) 
                {                                      
                    $arrUpdateFields[] = " devedor_empresa_id_INT = {$this->devedorEmpresaId} ";
                }


                
                if (isset($this->devedorPessoaId)) 
                {                                      
                    $arrUpdateFields[] = " devedor_pessoa_id_INT = {$this->devedorPessoaId} ";
                }


                
                if (isset($this->negociadorUsuarioId)) 
                {                                      
                    $arrUpdateFields[] = " negociador_usuario_id_INT = {$this->negociadorUsuarioId} ";
                }


                
                if (isset($this->cadastroSec)) 
                {                                      
                    $arrUpdateFields[] = " cadastro_SEC = {$this->cadastroSec} ";
                }


                
                if (isset($this->cadastroOffsec)) 
                {                                      
                    $arrUpdateFields[] = " cadastro_OFFSEC = {$this->cadastroOffsec} ";
                }


                
                if (isset($this->resultadoEmpresaVendaId)) 
                {                                      
                    $arrUpdateFields[] = " resultado_empresa_venda_id_INT = {$this->resultadoEmpresaVendaId} ";
                }


                
                if (isset($this->relatorioId)) 
                {                                      
                    $arrUpdateFields[] = " relatorio_id_INT = {$this->relatorioId} ";
                }


                
                if (isset($this->registroEstadoId)) 
                {                                      
                    $arrUpdateFields[] = " registro_estado_id_INT = {$this->registroEstadoId} ";
                }


                
                if (isset($this->protocoloInt)) 
                {                                      
                    $arrUpdateFields[] = " protocolo_INT = {$this->protocoloInt} ";
                }


            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE negociacao_divida SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function updateByInstanceUsingParameters($id, $parameters, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($parameters->devedorEmpresaId)) 
                {                                      
                    $arrUpdateFields[] = " devedor_empresa_id_INT = {$this->devedorEmpresaId} ";
                }
                
                if (isset($parameters->devedorPessoaId)) 
                {                                      
                    $arrUpdateFields[] = " devedor_pessoa_id_INT = {$this->devedorPessoaId} ";
                }
                
                if (isset($parameters->negociadorUsuarioId)) 
                {                                      
                    $arrUpdateFields[] = " negociador_usuario_id_INT = {$this->negociadorUsuarioId} ";
                }
                
                if (isset($parameters->cadastroSec)) 
                {                                      
                    $arrUpdateFields[] = " cadastro_SEC = {$this->cadastroSec} ";
                }
                
                if (isset($parameters->cadastroOffsec)) 
                {                                      
                    $arrUpdateFields[] = " cadastro_OFFSEC = {$this->cadastroOffsec} ";
                }
                
                if (isset($parameters->resultadoEmpresaVendaId)) 
                {                                      
                    $arrUpdateFields[] = " resultado_empresa_venda_id_INT = {$this->resultadoEmpresaVendaId} ";
                }
                
                if (isset($parameters->relatorioId)) 
                {                                      
                    $arrUpdateFields[] = " relatorio_id_INT = {$this->relatorioId} ";
                }
                
                if (isset($parameters->registroEstadoId)) 
                {                                      
                    $arrUpdateFields[] = " registro_estado_id_INT = {$this->registroEstadoId} ";
                }
                
                if (isset($parameters->protocoloInt)) 
                {                                      
                    $arrUpdateFields[] = " protocolo_INT = {$this->protocoloInt} ";
                }
            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE negociacao_divida SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }

}

?>
