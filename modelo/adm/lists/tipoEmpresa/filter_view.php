<?php
/*

Arquivo gerado através de gerador de código em 02/03/2017 as 19:48:07.
Para que o arquivo não seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

Tabela correspondente: tipo_empresa
Sobrescrita de dados: ---SOBRESCRITA_NAO_AUTORIZADA---

*/
?>

<?
global $objFilter;
?>

<?= $objFilter->getFormDebugPanel(); ?>
<div class='row'>
    <div class='col-sm-12'>
        <div class='box'>
            <div class='box-header blue-background'>
                <div class='title'>
                    <div class='icon-edit'></div>
                    <?= I18N::getExpression("Filtro"); ?>
                </div>
                <div class='actions'>
                    <a class="btn box-collapse btn-xs btn-link" href="#"><i></i></a>
                </div>
            </div>
            <div class='box-content'>

                <form name="<?= $objFilter->getFormName() ?>" class="form form-horizontal" style="margin-bottom: 0;">

                    <fieldset>

                        <?
                        $objCampo = $objFilter->getNumericInputInstance("id");
                        $objCampo->setLabel("Id");
                        $objCampo->setDefaultValues();
                        $objCampo->setPlaceHolder("Informe o Id.");
                        $objCampo->setAllowNegative(false);
                        $objCampo->setDecimalPlaces(0);
                        $objCampo->setHideThousandsSeparator(true);
                        ?>

                        <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                             ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                            <?= $objCampo->renderLabel(); ?>
                            <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                <?= $objCampo->render(); ?>
                                <?= $objCampo->renderValidationBlock(); ?>

                            </div>
                        </div>

                        <?= $objFilter->renderHorizontalLine(); ?>



                        <?
                        $objCampo = $objFilter->getTextInputInstance("nome");
                        $objCampo->setLabel("Nome");
                        $objCampo->setDefaultValues();
                        $objCampo->setPlaceHolder("Informe o Nome.");
                        ?>

                        <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                             ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                            <?= $objCampo->renderLabel(); ?>
                            <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                <?= $objCampo->render(); ?>
                                <?= $objCampo->renderValidationBlock(); ?>

                            </div>
                        </div>

                        <?= $objFilter->renderHorizontalLine(); ?>

                    </fieldset>

                    <div class='form-actions form-actions-padding-sm'>
                        <div class='row'>
                            <div class='col-md-11 col-md-offset-1'>

                                <?= $objFilter->renderSubmitButton(); ?>
                                <?= $objFilter->renderClearButton(); ?>

                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
            
