import { DadosIniciais } from './remoteModel/DadosIniciais';
import { PontoEletronico } from './remoteModel/PontoEletronico';
import { Empresa } from './remoteModel/Empresa';
import { Usuario } from './remoteModel/Usuario';
import { WifiNetwork } from './remoteModel/WifiNetwork';

export
{
    DadosIniciais,
    PontoEletronico,
    Empresa,
    Usuario,
    WifiNetwork
};

