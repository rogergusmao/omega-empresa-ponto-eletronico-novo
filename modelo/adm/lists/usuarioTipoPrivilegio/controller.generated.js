

        /*

        Arquivo gerado atrav�s de gerador de c�digo em 07/09/2017 as 01:32:16.
        Para que o arquivo n�o seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

        Tabela correspondente: usuario_tipo_privilegio
        Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

        */

omegaApp.generatedListUsuarioTipoPrivilegioController = function($scope, $rootScope, $window, $interval, $timeout, dataService, Upload, vm) {try { vm.fieldsTypes = {};

vm.disabledFilterFields = {
        fieldId: false,
fieldUsuarioTipoId: false,
fieldIdentificadorFuncionalidade: false,
};

vm.requiredFilterFields = {
        fieldId: false,
fieldUsuarioTipoId: false,
fieldIdentificadorFuncionalidade: false,
};

vm.filterFieldsParameters = {
        fieldId: null,
fieldUsuarioTipoId: {remoteData: null},
fieldIdentificadorFuncionalidade: null,
};

vm.filterData = {
        id: null,
usuarioTipoId: null,
identificadorFuncionalidade: null,
};

vm.filterOperators = {
        id: Constants.FilterOperator.EQUALS,
usuarioTipoId: Constants.FilterOperator.EQUALS,
identificadorFuncionalidade: Constants.FilterOperator.CONTAINS,
};

vm.sortingClass = {
        id: Constants.ListSortingClass.IDLE,
usuarioTipoId: Constants.ListSortingClass.IDLE,
identificadorFuncionalidade: Constants.ListSortingClass.IDLE,
};

vm.paginationParameters =
        {
            currentPage: 1,
            recordsPerPage: Constants.LIST_DEFAULT_NUMBER_OF_RECORDS_PER_PAGE,
            lastPage: null,
            visibleButtonsArray: []
        };

vm.sortingParameters = [];

$rootScope.filterListWatch(vm);

vm.getRemoteFilterData = function()
        {
            var parameters =
            {
                filterParameters: GeneralUtil.getFilterParameters(vm.preservedFilterData, vm.filterOperators),
                sortingParameters: vm.sortingParameters,
                paginationParameters: vm.paginationParameters
            };

            dataService.getList(parameters).then(function(returnContent)
            {
                if (GeneralUtil.validateRemoteResponse(returnContent))
                {
                    var remoteContent = returnContent.data.mObj;

                    vm.originalGridData = remoteContent.gridData;
                    vm.gridData = angular.copy(vm.originalGridData);
                    vm.paginationParameters = remoteContent.paginationParameters;
                    vm.isFiltered = true;

                    PaginationUtil.updateVisiblePaginationButtonsArray(vm);
                }
                else
                {
                    $rootScope.instantErrorMessages.push(returnContent.data.mMensagem);
                }
            });

        };

vm.getListOfUsuarioTipoId = function()
                {
                    

                        

                        dataService.getListOfUsuarioTipoId().then(function (returnContent)
                        {
                            if (GeneralUtil.validateRemoteResponse(returnContent))
                            {
                                var remoteContent = GeneralUtil.isNullOrEmpty(returnContent.data.mObj) ? [] : returnContent.data.mObj;
                                vm.filterFieldsParameters.fieldUsuarioTipoId.remoteData = remoteContent.gridData;
                            }
                            else
                            {
                                $rootScope.instantErrorMessages.push(returnContent.data.mMensagem);
                            }

                        });

                    

                };vm.getListOfCorporacaoId = function()
                {
                    

                        

                        dataService.getListOfCorporacaoId().then(function (returnContent)
                        {
                            if (GeneralUtil.validateRemoteResponse(returnContent))
                            {
                                var remoteContent = GeneralUtil.isNullOrEmpty(returnContent.data.mObj) ? [] : returnContent.data.mObj;
                                vm.filterFieldsParameters.fieldCorporacaoId.remoteData = remoteContent.gridData;
                            }
                            else
                            {
                                $rootScope.instantErrorMessages.push(returnContent.data.mMensagem);
                            }

                        });

                    

                };

vm.selectedRecords = [];
        vm.selectedRecordsForRemoval = [];

        vm.showRemoveMessageDialog = function(records)
        {
            records = records || vm.selectedRecords;
            vm.selectedRecordsForRemoval = angular.copy(records);

            $rootScope.showDialog(vm, 'before-remove-template.html');
        };

        vm.closeRemoveMessageDialog = function()
        {
            vm.selectedRecordsForRemoval = [];
            $rootScope.closeMessageDialog();
        };

        vm.removeSelectedRecords = function()
        {
            var parameters = vm.selectedRecordsForRemoval;
            dataService.removeRecords(parameters).then(function (returnContent)
            {
                vm.closeRemoveMessageDialog();
                if (GeneralUtil.validateRemoteResponse(returnContent))
                {
                    vm.getRemoteFilterData(function(){ $rootScope.showSuccessMessage(returnContent.data.mMensagem) });
                }
                else
                {
                    $rootScope.instantErrorMessages.push(returnContent.data.mMensagem);
                }
            });
        };

vm.editRecord = function(primaryKey)
        {
            var parameters = $.param(primaryKey);
            document.location = 'index.php?tipo=forms&page=usuarioTipoPrivilegio&' + parameters;
        };

}
                        catch(ex)
                        {
                            $rootScope.instantErrorMessages.push(new Message('Erro no controller generatedListUsuarioTipoPrivilegioController', ex.message));
                        }

                }