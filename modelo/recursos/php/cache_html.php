<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once Helper::acharRaizWorkspace() . DIRETORIO_BIBLIOTECAS_COMPARTILHADAS . "/classes/SingletonCacheHtml.php";

$singletonHtml = SingletonCacheHtml::getSingleton();

$singletonHtml->setUrls(
    "PontoEletronicoModel",
    null,
    null,
    null,
    array("login.php"));

$singletonHtml->exitIfCached();

$singletonHtml->startCachingIfNecessary();