<?php

class Protocolo_dados_hospedagem extends Interface_protocolo
{
    public $idAssinatura;
    public $dominio;
    public $nomeSite;

    public static function constroi($pVetorValor)
    {
        if (is_array($pVetorValor)) {
            $vObj = new Protocolo_arquivo();
            $vObj->idAssinatura = $pVetorValor[0];
            $vObj->dominio = $pVetorValor[1];
            $vObj->nomeSite = $pVetorValor[2];
            return $vObj;
        }
        return null;
    }

    public function factory($objJson)
    {

        $obj = new Protocolo_dados_hospedagem();
        $obj->idAssinatura = $objJson->idAssinatura;
        $obj->dominio = $objJson->dominio;
        $obj->nomeSite = $objJson->nomeSite;

        return $obj;
    }


}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
