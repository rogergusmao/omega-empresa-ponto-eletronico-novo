<?php
//Pagina Wrapper para Popups

include_once '../recursos/php/constants.php';
include_once '../recursos/php/database_config.php';
include_once '../client_area/imports/header.php';
include_once '../recursos/php/funcoes.php';
include_once '../client_area/imports/instancias.php';
include_once '../adm/imports/lingua.php';
include_once '../client_area/imports/sessao.php';
include_once '../recursos/libs/pag_seguro/PagSeguroLibrary.php';


$tipo = Helper::GET("tipo");
$pagina = Helper::GET("page");
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Frameset//EN"
    "http://www.w3.org/TR/html4/frameset.dtd">
<html>
<head>
    <title><?= TITULO_PAGINAS_CLIENTE ?></title>

    <?= Helper::getTagDoFavicon(); ?>
    <?= Helper::carregarCssFlatty(); ?>

    <?= Javascript::importarTodasAsBibliotecas(); ?>

</head>

<body leftmargin="0" topmargin="0" id="body_identificator">

<?
Javascript::imprimirCorpoDoTooltip();

Helper::includePHP(1, "client_area/imports/mensagens_popup.php");

$passouSeguranca = true;

if ($passouSeguranca) {

    if ($tipo && $pagina) {

        if (Helper::GET("dialog")) {

            Helper::imprimirComandoJavascript("$(document).ready(function(){

                                                        setTimeout(\"autoRedimensionarDialog()\", 1000);

                                                    })");
        }


        Helper::includePHP(false, "client_area/" . Helper::GET("tipo") . "/" . Helper::GET("page") . ".php");
    }
}

?>
<?= Javascript::importarBibliotecasFlatty(); ?>
</body>
