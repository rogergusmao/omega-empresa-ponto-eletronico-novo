<?php

/*

Arquivo gerado através de gerador de código em 09/09/2017 as 16:39:59.
Para que o arquivo não seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

Tabela correspondente: empresa_equipe
Sobrescrita de dados: ---SOBRESCRITA_NAO_AUTORIZADA---

*/
?>

<?php

class EXTDAO_Empresa_equipe extends DAO_Empresa_equipe
{
    public function __construct($configDAO)
    {
        parent::__construct($configDAO);
        $this->nomeClasse = "EXTDAO_Empresa_equipe";
    }

    public function factory()
    {
        return new EXTDAO_Empresa_equipe();
    }
}

?>
