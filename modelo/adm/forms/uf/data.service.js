/*

 Arquivo gerado atrav�s de gerador de c�digo em 07/01/2018 as 15:54:14.
 Para que o arquivo n�o seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

 Tabela correspondente: uf
 Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

 */

returnObject.getUf = function (filterParameters)
{

    var parameters = {
        filterParameters: filterParameters,
        loadComboBoxesData: true
    };

    return $http({
        url: baseURLAdm + "/webservice.php?class=EXTDAO_Uf&action=__getRecord",
        method: "POST",
        data: parameters,
        headers: {
            'Content-Type': 'application/json'
        }
    });

};

returnObject.addUf = function (formData)
{

    return $http({
        url: baseURLAdm + "/webservice.php?class=EXTDAO_Uf&jsonOutput=true&action=add",
        method: "POST",
        data: formData,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    });

};

returnObject.editUf = function (formData)
{

    return $http({
        url: baseURLAdm + "/webservice.php?class=EXTDAO_Uf&jsonOutput=true&action=edit",
        method: "POST",
        data: formData,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    });

};

returnObject.getAllComboBoxesDataForUfForm = function ()
{
    return $http({
        url: baseURLAdm + "/webservice.php?class=EXTDAO_Uf&jsonOutput=true&action=__getFormComboBoxesData",
        method: "POST",
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    });

};

returnObject.getListOfPaisForUfForm = function ()
{
    var params = {filterParameters: null, listMappingType: Constants.ListMappingType.COMBOBOX, hideLoading: true};
    return $http.post(baseURLAdm + "/webservice.php?class=EXTDAO_Pais&action=__getList", params);
};
                

