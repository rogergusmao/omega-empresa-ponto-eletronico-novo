<?php

include '../../recursos/languages/pt-br.php';
include '../../recursos/php/funcoes.php';
include '../../recursos/php/constants.php';
include '../../recursos/php/database_config.php';

$nomeScript = Helper::getNomeDoScriptAtual();
$idMensagemSMS = Helper::GET("mensagem_sms_id_INT");

$vIsPrimeiraVez = Helper::GET("is_primeira_vez_BOOLEAN");

$registrosPorPagina = REGISTROS_POR_PAGINA;

$registrosPesquisa = 1;

$obj = new EXTDAO_Mensagem_sms();
$obj->setByGet($registrosPesquisa);
$obj->formatarParaSQL();

$strCondicao = array();
$strGET = array();

if (!Helper::isNull($obj->getUsuario_id_INT()))
{
    $strCondicao[] = "usuario_id_INT={$obj->getUsuario_id_INT()}";
    $strGET[] = "usuario_id_INT1={$obj->getUsuario_id_INT()}";
}

if (!Helper::isNull($obj->getTitulo()))
{
    $strCondicao[] = "titulo LIKE '%{$obj->getTitulo()}%'";
    $strGET[] = "titulo1={$obj->getTitulo()}";
}

if (!Helper::isNull($obj->getMensagem_TEXTO()))
{

    $strCondicao[] = "mensagem_TEXTO LIKE '%{$obj->getMensagem_TEXTO()}%'";
    $strGET[] = "mensagem_TEXTO1={$obj->getMensagem_TEXTO()}";
}

if (!Helper::isNull(Helper::GET("datetime_inicio_cadastro1")))
{

    $datetimeInicio = Helper::GET("datetime_inicio_cadastro1");

    $strCondicao[] = "dataCadastro >=" . Helper::formatarDataTimeParaComandoSQL($datetimeInicio);
    $strGET[] = "datetime_inicio_cadastro1={$datetimeInicio}";
    $strDescricao[] = "&bull; <b>Data inicial:</b> {$datetimeInicio}";
}

if (!Helper::isNull(Helper::GET("datetime_fim_cadastro1")))
{

    $datetimeFim = Helper::GET("datetime_fim_cadastro1");

    $strCondicao[] = "dataCadastro <=" . Helper::formatarDataTimeParaComandoSQL($datetimeFim);
    $strGET[] = "datetime_fim_cadastro1={$datetimeFim}";
    $strDescricao[] = "&bull; <b>Data final:</b> {$datetimeFim} anos";
}
$consulta = "";

for ($i = 0; $i < count($strCondicao); $i++)
{

    $consulta .= " AND " . $strCondicao[$i];
}
$varGET = "";
for ($i = 0; $i < count($strGET); $i++)
{

    $varGET .= "&" . $strGET[$i];
}

if ($nomeScript == "index.php" && !isset($vIsPrimeiraVez))
{
    echo "<center>";
    Helper::imprimirMensagem("A janela de download abrirá em alguns segundos.\nCaso não abra automaticamente, <a class='link_padrao' href='pages/exportar_campanha_mensagem_sms.php?$varGET'>clique aqui</a>.");
    echo "</center>";

    Helper::mudarLocation("pages/exportar_campanha_mensagem_sms.php?$varGET");
    exit();
}

/*
*
* -------------------------------------------------------
* NOME DA LIST:       mensagem_sms
* NOME DA CLASSE DAO: DAO_Mensagem_sms
* DATA DE GERAÇÃO:    17.06.2012
* ARQUIVO:            EXTDAO_Mensagem_sms.php
* TABELA MYSQL:       mensagem_sms
* BANCO DE DADOS:     email_marketing
* -------------------------------------------------------
*
*/

$consultaNumero = "SELECT COUNT(id) FROM mensagem_sms
    WHERE excluido_BOOLEAN=0 AND is_instantanea_BOOLEAN = '0' {$consulta}";

$objBanco = new Database();

$objBanco->query($consultaNumero);
$numeroRegistros = $objBanco->getPrimeiraTuplaDoResultSet(0);

$limites = Helper::getLimitesRegsPaginacao($registrosPorPagina, $numeroRegistros);

$consultaRegistros = "SELECT id
    FROM mensagem_sms 
    WHERE excluido_BOOLEAN=0 AND is_instantanea_BOOLEAN = '0' {$consulta} 
    ORDER BY mensagem_TEXTO LIMIT {$limites[0]},{$limites[1]}";

$objBanco->query($consultaRegistros);
$stringRetorno = "";
$vetorCabecalho = array(
    $obj->label_id,
    $obj->label_usuario_id_INT,
    $obj->label_titulo,
    $obj->label_data_agendamento_DATETIME,
    $obj->label_dataCadastro,
    $obj->label_dataEdicao,
    "A Enviar",
    "Enviado",
    "Erro Envio",
    "Total Destinatário");

if ($objBanco->rows == 0)
{
    ?>
    <table>
        <tr class="tr_list_conteudo_impar">
            <td colspan="6">
                <?= Helper::imprimirMensagem("Nenhuma mensagem SMS cadastrada.") ?>
            </td>
        </tr>
    </table>
    <?
}
else
{
    $stringRetorno .= Helper::getStrLinhaCSVDoVetorDeDados($vetorCabecalho);
    for ($i = 1; $regs = $objBanco->fetchArray(); $i++)
    {
        $vIdMensagemSMS = $regs[0];
        $obj->select($regs[0]);
        $obj->formatarParaExibicao();
        $vNomeUsuario = "";
        if (strlen($obj->getUsuario_id_INT()))
        {
            $obj->objUsuario->select($obj->getUsuario_id_INT());
            $obj->objUsuario->formatarParaExibicao();
            $vNomeUsuario = $obj->objUsuario->valorCampoLabel();
        }
        $vTitulo = "";
        $vTitulo = $obj->getTitulo();

        $totalDestinatario = 0;
        $objBanco2 = new Database();
        $objBanco2->query("SELECT COUNT(id)
                    FROM mensagem_sms_pessoa 
                    WHERE mensagem_sms_id_INT={$vIdMensagemSMS}  
                    AND (foi_enviado_BOOLEAN = '0' AND (numero_tentativa_INT < " . LIMITE_TENTATICA_ENVIO_SMS . " OR numero_tentativa_INT IS NULL) )
                    ORDER BY id");
        $numeroTotalAEnviar = $objBanco2->getPrimeiraTuplaDoResultSet(0);
        if (Helper::isNull($numeroTotalAEnviar))
        {
            $numeroTotalAEnviar = 0;
        }

        $totalDestinatario += $numeroTotalAEnviar;

        $objBanco2->query("SELECT COUNT(id)
                    FROM mensagem_sms_pessoa 
                    WHERE mensagem_sms_id_INT={$vIdMensagemSMS}  
                    AND (foi_enviado_BOOLEAN = '1' AND NOT data_envio_DATETIME IS NULL )
                    ORDER BY id");

        $numeroTotalDeEnvios = $objBanco2->getPrimeiraTuplaDoResultSet(0);
        if (Helper::isNull($numeroTotalDeEnvios))
        {
            $numeroTotalDeEnvios = 0;
        }

        $totalDestinatario += $numeroTotalDeEnvios;

        $objBanco2->query("SELECT COUNT(id)
                    FROM mensagem_sms_pessoa 
                    WHERE mensagem_sms_id_INT={$vIdMensagemSMS}  
                    AND (foi_enviado_BOOLEAN = '0' AND numero_tentativa_INT >= " . LIMITE_TENTATICA_ENVIO_SMS . ")
                    ORDER BY id");

        $numeroDeEnviosComErro = $objBanco2->getPrimeiraTuplaDoResultSet(0);
        if (Helper::isNull($numeroDeEnviosComErro))
        {
            $numeroDeEnviosComErro = 0;
        }

        $totalDestinatario += $numeroDeEnviosComErro;

        $vetorTupla = array(
            $obj->getId(),
            $vNomeUsuario,
            $vTitulo,
            $obj->getData_agendamento_DATETIME(),
            $obj->getDataCadastro(),
            $obj->getDataEdicao(),
            $numeroTotalAEnviar,
            $numeroTotalDeEnvios,
            $numeroDeEnviosComErro,
            $totalDestinatario);

        $stringRetorno .= Helper::getStrLinhaCSVDoVetorDeDados($vetorTupla);
    }
    $objDownload = new Download("campanha_mensagem_sms.csv");
    print $objDownload->ds_download($stringRetorno);
}
?>

                
