
$('#botaoAlterarSenha').click(function(){

    alterarSenha('#modalAlterarSenha');

});

function onAlterarSenhaModalShow()
{
    $('#modalAlterarSenha').find('input').val('');
    $('#modalAlterarSenha').find('.alert').hide();
}

function alterarSenha(modalSelector)
{
    try
    {
        var $el = $(modalSelector);

        //esconde mensagens da modal
        $el.find('.alert').hide();

        var senhaAtual = $el.find('#senhaAtual').val();
        var senhaNova = $el.find('#senhaNova').val();
        var senhaNovaConfirmacao = $el.find('#senhaNovaConfirmacao').val();

        if(GeneralUtil.isNullOrEmpty(senhaAtual))
        {
            $el.find('#mensagem-1').show();
            return;
        }

        if(GeneralUtil.isNullOrEmpty(senhaNova))
        {
            $el.find('#mensagem-2').show();
            return;
        }

        if (senhaNova !== senhaNovaConfirmacao)
        {
            $el.find('#mensagem-3').show();
            return;
        }

        if(senhaAtual == senhaNova)
        {
            $el.find('#mensagem-4').show();
            return;
        }

        var funcaoASerChamada = function (retorno)
        {
            try
            {
                debugger;
                var codRetorno = retorno.mCodRetorno;
                var mensagemRetorno = retorno.mMensagem;

                if (codRetorno == PROTOCOLO_SISTEMA.SIM)
                {
                    $el.find('#mensagem-5 .conteudo-mensagem').html(mensagemRetorno);
                    $el.find('#mensagem-5').show();

                    $el.find('#senhaAtual').val('');
                    $el.find('#senhaNova').val('');
                    $el.find('#senhaNovaConfirmacao').val('');

                    setTimeout(function(){

                        //fecha modal
                        $(modalSelector).find('button.close').click();

                    }, 4000);

                }
                else
                {
                    HtmlUtil.esconderLoading();
                    $el.find('#mensagem-5 .conteudo-mensagem').html(mensagemRetorno);
                    $el.find('#mensagem-5').show();
                }
                return true;

            }
            catch (err)
            {
                HtmlUtil.esconderLoading();

                $el.find('#mensagem-5 .conteudo-mensagem').html(err.stack);
                $el.find('#mensagem-5').show();
                return false;
            }

        };

        HtmlUtil.mostrarLoading();

        var dadosPost = {
            senhaAtual: senhaAtual,
            senhaNova: senhaNova
        };

        $.post( 'webservice.php?class=EXTDAO_Usuario&action=alterarSenha', dadosPost).done(funcaoASerChamada);

    }
    catch (err)
    {
        HtmlUtil.esconderLoading();
        $el.find('#mensagem-5 .conteudo-mensagem').html(err.stack);
        $el.find('#mensagem-5').show();
        return false;
    }
    finally
    {

    }
    return false;
}

