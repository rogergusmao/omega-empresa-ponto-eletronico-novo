<?php

/*

Arquivo gerado através de gerador de código em 22/09/2017 as 20:58:11.
Para que o arquivo não seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

Tabela correspondente: pessoa
Sobrescrita de dados: ---SOBRESCRITA_NAO_AUTORIZADA---

*/

?>

<!--Definição da View Angular JS -->
<?

$objForm = new FormEnvironment("mainForm", "formPessoaController", "vm");
$objForm->setLayoutOneColumn();

?>
<div class='row' id='content-wrapper' ng-controller="<?= $objForm->getAngularControllerAttributeValue() ?>">

    <?= $objForm->setModalEnvironmentIfApplicable(); ?>
    <?= $objForm->setEditionVariablesIfApplicable(); ?>

    <!--Definição do template do dialog exibido após inserção -->
    <script type="text/ng-template" id="after-pessoa-insert-template.html">
        <div class="modal-header">
            <h4 class="modal-title">
                <span class="glyphicon"></span>
                <?= I18N::getExpression("Pessoa adicionada com sucesso!"); ?>
            </h4>
        </div>
        <div class="modal-body">
            <span class="help-block"><?= I18N::getExpression("O que deseja fazer agora?"); ?></span>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" ng-click="vm.loadListOfPessoa()">
                <?= I18N::getExpression("Listar pessoas"); ?>
            </button>
            <button type="button" class="btn" ng-click="closeMessageDialog()">
                <?= I18N::getExpression("Permanecer na tela"); ?>
            </button>
            <button type="button" class="btn btn-success" ng-click="vm.addNewPessoa()">
                <?= I18N::getExpression("Adicionar pessoa"); ?>
            </button>
        </div>
    </script>

    <!--Definição do template do exibido dialog após update -->
    <script type="text/ng-template" id="after-pessoa-update-template.html">
        <div class="modal-header">
            <h4 class="modal-title">
                <span class="glyphicon"></span>
                <?= I18N::getExpression("Pessoa alterada com sucesso!"); ?>

            </h4>
        </div>
        <div class="modal-body">
            <span class="help-block"><?= I18N::getExpression("O que deseja fazer agora?"); ?></span>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" ng-click="vm.loadListOfPessoa()">
                <?= I18N::getExpression("Listar pessoas"); ?>
            </button>
            <button type="button" class="btn" ng-click="closeMessageDialog()">
                <?= I18N::getExpression("Permanecer na tela"); ?>
            </button>
            <button type="button" class="btn btn-success" ng-click="vm.addNewPessoa()">
                <?= I18N::getExpression("Adicionar pessoa"); ?>
            </button>
        </div>
    </script>

    <div class='col-xs-12'>

        <div class='row'>
            <div class='col-sm-12'>
                <div class='page-header'>
                    <h1 class='pull-left'>
                        <i class='icon-edit'></i>
                        <span><?= I18N::getExpression("Cadastro de Pessoa"); ?></span>
                    </h1>
                    <div class='pull-right close-button-container'>
                        <?= $objForm->renderCloseModalButtonIfExists(); ?>
                    </div>
                </div>
            </div>
        </div>

        <?= $objForm->getFormDebugPanel(); ?>

        <div class='row'>
            <div class='col-sm-12'>
                <div class='box'>
                    <div class='box-header blue-background'>
                        <div class='title'>
                            <div class='icon-user'></div>
                            <?= I18N::getExpression("Dados da pessoa"); ?>
                        </div>
                        <div class='actions'>
                            <a class="btn btn-xs btn-link " href="javascript: void(0);"
                               ng-click="expandOrShrinkPanels(<?= $objForm->getFormName() ?>)">
                                <i ng-class="getExpandOrShinkPanelsButtonClass(<?= $objForm->getFormName() ?>)"></i>
                            </a>
                        </div>
                    </div>
                    <div class='box-content'>

                        <form name="<?= $objForm->getFormName() ?>" class="form form-horizontal"
                              style="margin-bottom: 0;" novalidate>

                            <?php

                            $objCampo = $objForm->getTextInputInstance("nome", TextInput::TRANSFORMATION_DIRECTIVE_UPPER);
                            $objCampo->setLabel("Nome");

                            $objCampo->setDefaultValues();
                            $objCampo->setPlaceHolder("Informe o Nome.");
                            $objCampo->addValidation(new MaxLengthValidation(255, I18N::getExpression("O tamanho máximo do Nome deve ser de {0} caracteres.")));
                            $objCampo->addValidation(new RequiredValidation(I18N::getExpression("O Nome é de preenchimento obrigatório.")));

                            ?>

                            <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                 ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                                <?= $objCampo->renderLabel(); ?>
                                <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                    <?= $objCampo->render(); ?>
                                    <?= $objCampo->renderValidationBlock(); ?>

                                </div>


                            </div>

                            <?= $objForm->renderHorizontalLine(); ?>

                            <?php

                            $objCampo = $objForm->getTextInputInstance("email");
                            $objCampo->setLabel("E-mail");

                            $objCampo->setDefaultValues();
                            $objCampo->setPlaceHolder("Informe o E-mail.");
                            $objCampo->addValidation(new MaxLengthValidation(255, I18N::getExpression("O tamanho máximo do E-mail deve ser de {0} caracteres.")));
                            $objCampo->addValidation(new RequiredValidation(I18N::getExpression("O E-mail é de preenchimento obrigatório.")));
                            //$objCampo->addValidation(new EmailValidation("Informe um endereço de e-mail válido."));
                            ?>

                            <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                 ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                                <?= $objCampo->renderLabel(); ?>
                                <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                    <?= $objCampo->render(); ?>
                                    <?= $objCampo->renderValidationBlock(); ?>

                                </div>


                            </div>

                            <?= $objForm->renderHorizontalLine(); ?>

                            <?php

                            $objCampo = $objForm->getPhoneInputInstance("telefone");
                            $objCampo->setLabel("Telefone");

                            $objCampo->setDefaultValues();
                            $objCampo->setPlaceHolder("Informe o Telefone.");
                            $objCampo->addValidation(new RequiredValidation(I18N::getExpression("O Telefone é de preenchimento obrigatório.")));

                            ?>

                            <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                 ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                                <?= $objCampo->renderLabel(); ?>
                                <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                    <?= $objCampo->render(); ?>
                                    <?= $objCampo->renderValidationBlock(); ?>

                                </div>


                            </div>

                            <?= $objForm->renderHorizontalLine(); ?>

                            <?php

                            $objCampo = $objForm->getPhoneInputInstance("celular");
                            $objCampo->setLabel("Celular");

                            $objCampo->setDefaultValues();
                            $objCampo->setPlaceHolder("Informe o Celular.");
                            $objCampo->addValidation(new RequiredValidation(I18N::getExpression("O Celular é de preenchimento obrigatório.")));

                            ?>

                            <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                 ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                                <?= $objCampo->renderLabel(); ?>
                                <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                    <?= $objCampo->render(); ?>
                                    <?= $objCampo->renderValidationBlock(); ?>

                                </div>

                            </div>

                            <?= $objForm->renderHorizontalLine(); ?>

                            <div class="col-sm-12 box box-nomargin box-collapsed omega-form-section-box">
                                <div class="box-header small blue-background">
                                    <div class="title">
                                        <div class='icon-book'></div>
                                        <?= I18N::getExpression("Informações Pessoais") ?>
                                    </div>
                                    <div class="actions">
                                        <a class="btn box-collapse btn-xs btn-link" href="javascript: void(0);"><i></i></a>
                                    </div>
                                </div>
                                <div class="box-content">

                                    <?php

                                    $objCampo = $objForm->getSelectInstance("tipoDocumentoId");
                                    $objCampo->setLabel(I18N::getExpression("Tipo de documento"));

                                    $objCampo->setPlaceHolder(I18N::getExpression("Selecione um tipo de documento da lista..."), false);
                                    $objCampo->setMatchExpression("{{nome}}");
                                    $objCampo->setCloseOnSelect(true);

                                    $propsFilter = new PropsFilter();
                                    $propsFilter->addFieldToFilter("nome");

                                    $selectChoiceContent = new SelectChoiceContent("tipoDocumentoId");
                                    $selectChoiceContent->setMainString(new AngularString("nome", "div", Select::getSelectHighLightFilter()));

                                    $objCampo->setFilter($propsFilter);
                                    $objCampo->setSelectChoiceContent($selectChoiceContent);
                                    $objCampo->addValidation(new RequiredValidation(I18N::getExpression("O Tipo de documento é de preenchimento obrigatório.")));

                                    ?>

                                    <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                         ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                                        <?= $objCampo->renderLabel(); ?>
                                        <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                            <?= $objCampo->render(); ?>
                                            <?= $objCampo->renderValidationBlock(); ?>

                                        </div>

                                        <div class='col-md-2'>

                                            <?= $objCampo->renderAddEntityButton(I18N::getExpression("Adicionar Tipo de documento")); ?>

                                        </div>

                                    </div>

                                    <?= $objForm->renderHorizontalLine(); ?>

                                    <?php

                                    $objCampo = $objForm->getTextInputInstance("numeroDocumento");
                                    $objCampo->setLabel("Número do documento");

                                    $objCampo->setDefaultValues();
                                    $objCampo->setPlaceHolder("Informe o Número do documento.");
                                    $objCampo->addValidation(new MaxLengthValidation(30, I18N::getExpression("O tamanho máximo do Número do documento deve ser de {0} caracteres.")));
                                    $objCampo->addValidation(new RequiredValidation(I18N::getExpression("O Número do documento é de preenchimento obrigatório.")));

                                    ?>

                                    <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                         ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                                        <?= $objCampo->renderLabel(); ?>
                                        <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                            <?= $objCampo->render(); ?>
                                            <?= $objCampo->renderValidationBlock(); ?>

                                        </div>


                                    </div>

                                    <?= $objForm->renderHorizontalLine(); ?>

                                    <?php

                                    $objCampo = $objForm->getSelectInstance("sexoId");
                                    $objCampo->setLabel(I18N::getExpression("Sexo"));

                                    $objCampo->setPlaceHolder(I18N::getExpression("Selecione um sexo da lista..."), false);
                                    $objCampo->setMatchExpression("{{nome}}");
                                    $objCampo->setCloseOnSelect(true);

                                    $propsFilter = new PropsFilter();
                                    $propsFilter->addFieldToFilter("nome");

                                    $selectChoiceContent = new SelectChoiceContent("sexoId");
                                    $selectChoiceContent->setMainString(new AngularString("nome", "div", Select::getSelectHighLightFilter()));

                                    $objCampo->setFilter($propsFilter);
                                    $objCampo->setSelectChoiceContent($selectChoiceContent);
                                    $objCampo->addValidation(new RequiredValidation(I18N::getExpression("O Sexo é de preenchimento obrigatório.")));

                                    ?>

                                    <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                         ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                                        <?= $objCampo->renderLabel(); ?>
                                        <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                            <?= $objCampo->render(); ?>
                                            <?= $objCampo->renderValidationBlock(); ?>

                                        </div>

                                    </div>

                                </div>

                            </div>

                            <?= $objForm->renderHorizontalLine(); ?>

                            <div class="col-sm-12 box box-nomargin box-collapsed omega-form-section-box">
                                <div class="box-header small blue-background">
                                    <div class="title">
                                        <div class='icon-map-marker'></div>
                                        <?= I18N::getExpression("Endereço") ?>
                                    </div>
                                    <div class="actions">
                                        <a class="btn box-collapse btn-xs btn-link" href="javascript: void(0);"><i></i></a>
                                    </div>
                                </div>
                                <div class="box-content">

                                    <?php

                                    $objCampo = $objForm->getTextInputInstance("logradouro");
                                    $objCampo->setLabel("Logradouro");

                                    $objCampo->setDefaultValues();
                                    $objCampo->setPlaceHolder("Informe o Logradouro.");
                                    $objCampo->addValidation(new MaxLengthValidation(255, I18N::getExpression("O tamanho máximo do Logradouro deve ser de {0} caracteres.")));
                                    $objCampo->addValidation(new RequiredValidation(I18N::getExpression("O Logradouro é de preenchimento obrigatório.")));

                                    ?>

                                    <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                         ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                                        <?= $objCampo->renderLabel(); ?>
                                        <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                            <?= $objCampo->render(); ?>
                                            <?= $objCampo->renderValidationBlock(); ?>

                                        </div>


                                    </div>

                                    <?= $objForm->renderHorizontalLine(); ?>

                                    <?php

                                    $objCampo = $objForm->getTextInputInstance("numero");
                                    $objCampo->setLabel("Número");

                                    $objCampo->setDefaultValues();
                                    $objCampo->setPlaceHolder("Informe o Número.");
                                    $objCampo->addValidation(new MaxLengthValidation(30, I18N::getExpression("O tamanho máximo do Número deve ser de {0} caracteres.")));
                                    $objCampo->addValidation(new RequiredValidation(I18N::getExpression("O Número é de preenchimento obrigatório.")));

                                    ?>

                                    <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                         ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                                        <?= $objCampo->renderLabel(); ?>
                                        <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                            <?= $objCampo->render(); ?>
                                            <?= $objCampo->renderValidationBlock(); ?>

                                        </div>


                                    </div>

                                    <?= $objForm->renderHorizontalLine(); ?>

                                    <?php

                                    $objCampo = $objForm->getTextInputInstance("complemento");
                                    $objCampo->setLabel("Complemento");

                                    $objCampo->setDefaultValues();
                                    $objCampo->setPlaceHolder("Informe o Complemento.");
                                    $objCampo->addValidation(new MaxLengthValidation(255, I18N::getExpression("O tamanho máximo do Complemento deve ser de {0} caracteres.")));
                                    $objCampo->addValidation(new RequiredValidation(I18N::getExpression("O Complemento é de preenchimento obrigatório.")));

                                    ?>

                                    <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                         ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                                        <?= $objCampo->renderLabel(); ?>
                                        <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                            <?= $objCampo->render(); ?>
                                            <?= $objCampo->renderValidationBlock(); ?>

                                        </div>


                                    </div>

                                    <?= $objForm->renderHorizontalLine(); ?>

                                    <?php

                                    $objCampo = $objForm->getSelectInstance("cidadeId");
                                    $objCampo->setLabel(I18N::getExpression("Cidade"));

                                    $objCampo->setPlaceHolder(I18N::getExpression("Selecione uma cidade da lista..."), false);
                                    $objCampo->setMatchExpression("{{nome}}");
                                    $objCampo->setCloseOnSelect(true);

                                    $propsFilter = new PropsFilter();
                                    $propsFilter->addFieldToFilter("nome");

                                    $selectChoiceContent = new SelectChoiceContent("cidadeId");
                                    $selectChoiceContent->setMainString(new AngularString("nome", "div", Select::getSelectHighLightFilter()));

                                    $objCampo->setFilter($propsFilter);
                                    $objCampo->setSelectChoiceContent($selectChoiceContent);
                                    $objCampo->addValidation(new RequiredValidation(I18N::getExpression("A Cidade é de preenchimento obrigatório.")));

                                    ?>

                                    <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                         ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                                        <?= $objCampo->renderLabel(); ?>
                                        <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                            <?= $objCampo->render(); ?>
                                            <?= $objCampo->renderValidationBlock(); ?>

                                        </div>

                                        <div class='col-md-2'>

                                            <?= $objCampo->renderAddEntityButton(I18N::getExpression("Adicionar Cidade")); ?>

                                        </div>

                                    </div>

                                    <?= $objForm->renderHorizontalLine(); ?>

                                    <?php

                                    $objCampo = $objForm->getSelectInstance("bairroId");
                                    $objCampo->setLabel(I18N::getExpression("Bairro"));

                                    $objCampo->setPlaceHolder(I18N::getExpression("Selecione um bairro da lista..."), false);
                                    $objCampo->setMatchExpression("{{nome}}");
                                    $objCampo->setCloseOnSelect(true);

                                    $propsFilter = new PropsFilter();
                                    $propsFilter->addFieldToFilter("nome");

                                    $selectChoiceContent = new SelectChoiceContent("bairroId");
                                    $selectChoiceContent->setMainString(new AngularString("nome", "div", Select::getSelectHighLightFilter()));

                                    $objCampo->setFilter($propsFilter);
                                    $objCampo->setSelectChoiceContent($selectChoiceContent);
                                    $objCampo->addValidation(new RequiredValidation(I18N::getExpression("O Bairro é de preenchimento obrigatório.")));

                                    ?>

                                    <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                         ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                                        <?= $objCampo->renderLabel(); ?>
                                        <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                            <?= $objCampo->render(); ?>
                                            <?= $objCampo->renderValidationBlock(); ?>

                                        </div>

                                        <div class='col-md-2'>

                                            <?= $objCampo->renderAddEntityButton(I18N::getExpression("Adicionar Bairro")); ?>

                                        </div>

                                    </div>

                                </div>
                            </div>

                            <?= $objForm->renderHorizontalLine(); ?>

                            <!--
                            <div class="col-sm-12 box box-nomargin box-collapsed omega-form-section-box">
                                <div class="box-header small blue-background">
                                    <div class="title">
                                        <div class='icon-plus'></div>
                                        <?= I18N::getExpression("Outras Informações") ?>
                                    </div>
                                    <div class="actions">
                                        <a class="btn box-collapse btn-xs btn-link" href="javascript: void(0);"><i></i></a>
                                    </div>
                                </div>
                                <div class="box-content">

                                    <?php

                            $objCampo = $objForm->getPhoneInputInstance("celularSms");
                            $objCampo->setLabel("Celular para SMS");

                            $objCampo->setDefaultValues();
                            $objCampo->setPlaceHolder("Informe o Celular para SMS.");
                            $objCampo->addValidation(new RequiredValidation(I18N::getExpression("O Celular para SMS é de preenchimento obrigatório.")));

                            ?>

                                    <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                         ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                                        <?= $objCampo->renderLabel(); ?>
                                        <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                            <?= $objCampo->render(); ?>
                                            <?= $objCampo->renderValidationBlock(); ?>

                                        </div>


                                    </div>

                                    <?php

                            $objCampo = $objForm->getSelectInstance("operadoraId");
                            $objCampo->setLabel(I18N::getExpression("Operadora"));

                            $objCampo->setPlaceHolder(I18N::getExpression("Selecione uma operadora da lista..."), false);
                            $objCampo->setMatchExpression("{{nome}}");
                            $objCampo->setCloseOnSelect(true);

                            $propsFilter = new PropsFilter();
                            $propsFilter->addFieldToFilter("nome");

                            $selectChoiceContent = new SelectChoiceContent("operadoraId");
                            $selectChoiceContent->setMainString(new AngularString("nome", "div", Select::getSelectHighLightFilter()));

                            $objCampo->setFilter($propsFilter);
                            $objCampo->setSelectChoiceContent($selectChoiceContent);
                            $objCampo->addValidation(new RequiredValidation(I18N::getExpression("A Operadora é de preenchimento obrigatório.")));

                            ?>

                                    <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                         ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                                        <?= $objCampo->renderLabel(); ?>
                                        <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                            <?= $objCampo->render(); ?>
                                            <?= $objCampo->renderValidationBlock(); ?>

                                        </div>

                                        <div class='col-md-2'>

                                            <?= $objCampo->renderAddEntityButton(I18N::getExpression("Adicionar Operadora")); ?>

                                        </div>

                                    </div>

                                    <?= $objForm->renderHorizontalLine(); ?>

                                    <?php

                            $objCampo = $objForm->getSwitchControlInstance("isConsumidorBoolean");
                            $objCampo->setLabel("É consumidor");

                            $objCampo->setDefaultValues();
                            $objCampo->setOnText(I18N::getExpression("SIM"));
                            $objCampo->setOffText(I18N::getExpression("NÃO"));
                            $objCampo->addValidation(new RequiredValidation(I18N::getExpression("O campo é consumidor é de preenchimento obrigatório.")));

                            ?>

                                    <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                         ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                                        <?= $objCampo->renderLabel(); ?>
                                        <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                            <?= $objCampo->render(); ?>
                                            <?= $objCampo->renderValidationBlock(); ?>

                                        </div>

                                    </div>

                                </div>

                            </div>
                            -->

                            <?= $objForm->renderHorizontalLine(); ?>

                            <div class='form-actions form-actions-padding-sm'>
                                <div class='row'>
                                    <div class='col-md-10 action-buttons-bar'>

                                        <?= $objForm->renderSubmitButton(); ?>

                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php

if (!Helper::isAngularModal())
{
    $arrDataServicesRelacionados = array("forms/tipoDocumento", "forms/sexo", "forms/operadora", "forms/pais", "forms/uf", "forms/cidade", "forms/bairro");
    $arrControllersRelacionados = array("forms/tipoDocumento", "forms/sexo", "forms/operadora", "forms/pais", "forms/uf", "forms/cidade", "forms/bairro");

    ?>

    <!--Definição do Controller Angular JS -->
    <script type="text/javascript">

        <?=Helper::incluirConteudoDataServices(true, $arrDataServicesRelacionados); ?>

    </script>

    <?= Helper::incluirControllerAngularJS(true, $arrControllersRelacionados); ?>

<?php } ?>

        
