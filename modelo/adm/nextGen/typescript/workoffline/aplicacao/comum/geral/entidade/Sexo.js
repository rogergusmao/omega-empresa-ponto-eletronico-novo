"use strict";
exports.__esModule = true;
var Sexo;
(function (Sexo) {
    Sexo[Sexo["NAO_DEFINIDO"] = null] = "NAO_DEFINIDO";
    Sexo[Sexo["MASCULINO"] = 1] = "MASCULINO";
    Sexo[Sexo["FEMININO"] = 2] = "FEMININO";
})(Sexo = exports.Sexo || (exports.Sexo = {}));
