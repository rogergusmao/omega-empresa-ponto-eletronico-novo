<?php

/*

Arquivo gerado através de gerador de código em 24/05/2017 as 09:58:11.
Para que o arquivo não seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

Tabela correspondente: pessoa
Sobrescrita de dados: ---SOBRESCRITA_NAO_AUTORIZADA---

*/

?>

<?php

class EXTDAO_Pessoa extends DAO_Pessoa
{
    public function __construct($configDAO = null)
    {
        parent::__construct($configDAO);
        $this->nomeClasse = "EXTDAO_Pessoa";
    }

    protected function getFormComboBoxesData()
    {
        $comboBoxesData = new stdClass();
        $listParameters = new ListParameters(static::LIST_MAPPING_TYPE_COMBOBOX);

        $objTipoDocumento = new EXTDAO_Tipo_documento();
        $comboBoxesData->fieldTipoDocumentoId = $objTipoDocumento->__getList($listParameters);

        $objSexo = new EXTDAO_Sexo();
        $comboBoxesData->fieldSexoId = $objSexo->__getList($listParameters);

        $objOperadora = new EXTDAO_Operadora();
        $comboBoxesData->fieldOperadoraId = $objOperadora->__getList($listParameters);

        $objCidade = new EXTDAO_Cidade();
        $comboBoxesData->fieldCidadeId = $objCidade->__getList($listParameters);

        if (!is_null($this->cidadeId))
        {
            $listParametersBairro = new ListParameters(static::LIST_MAPPING_TYPE_COMBOBOX);
            $listParametersBairro->addFilterParameters("cidadeId", $this->cidadeId);

            $objBairro = new EXTDAO_Bairro();
            $comboBoxesData->fieldBairroId = $objBairro->__getList($listParametersBairro);
        }

        return $comboBoxesData;
    }

    public static function verificarSeIsFuncionarioDaEmpresaLogada($idFuncionario)
    {
        if (Seguranca::getIdDaEmpresaLogada())
        {
            $idEmpresa = Seguranca::getIdDaEmpresaLogada();

            $objBanco = new Database();
            $objBanco->query("SELECT id FROM pessoa_empresa WHERE pessoa_id_INT={$idFuncionario} AND empresa_id_INT={$idEmpresa}");

            if ($objBanco->rows() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return true;
        }
    }

    public function salvarRotinaDoFuncionario()
    {
        $idPessoa = Helper::POST("pessoa_id_INT");
        $paginaRetornar = Helper::POST("origin_action");

        $objPessoaEmpresaRotina = new EXTDAO_Pessoa_empresa_rotina();
        $objBanco = new Database();

        $objBanco->query("SELECT per.id 
                          FROM pessoa_empresa_rotina AS per, pessoa_empresa AS pe                           
                          WHERE per.pessoa_empresa_id_INT=pe.id
                          AND pe.pessoa_id_INT={$idPessoa}");

        while ($dadosRotinasSalvas = $objBanco->fetchArray())
        {

            $arrIdsRotinaExcluir[$dadosRotinasSalvas[0]] = true;
        }

        $arrDatasRotina = $_POST["data_rotina"];
        foreach ($arrDatasRotina as $dadosRotina)
        {

            $dadosRotina = explode("_", $dadosRotina);

            if (count($dadosRotina) == 3)
            {

                $idPessoaEmpresa = $dadosRotina[0];
                $semana = $dadosRotina[1];
                $diaSemana = $dadosRotina[2];

                $objBanco = new Database();
                $objBanco->query("SELECT per.id 
                                  FROM pessoa_empresa_rotina AS per                       
                                  WHERE per.pessoa_empresa_id_INT={$idPessoaEmpresa}
                                  AND per.semana_INT={$semana}
                                  AND per.dia_semana_INT={$diaSemana}");

                if ($objBanco->rows() > 0)
                {

                    $idPessoaEmpresaRotina = $objBanco->getPrimeiraTuplaDoResultSet(0);
                    unset($arrIdsRotinaExcluir[$idPessoaEmpresaRotina]);
                }
                else
                {

                    $objPessoaEmpresaRotina->setPessoa_empresa_id_INT($idPessoaEmpresa);
                    $objPessoaEmpresaRotina->setSemana_INT($semana);
                    $objPessoaEmpresaRotina->setDia_semana_INT($diaSemana);
                    $objPessoaEmpresaRotina->formatarParaSQL();
                    $objPessoaEmpresaRotina->insert(true);
                }
            }
        }

        foreach ($arrIdsRotinaExcluir as $idPessoaEmpresaRotina => $confirmacao)
        {
            if ($confirmacao)
            {
                $objPessoaEmpresaRotina->delete($idPessoaEmpresaRotina, true);
            }
        }

        return array("location: {$paginaRetornar}&msgSucesso=Dados da Rotina salvos com sucesso!");
    }

    public function getEmpresasEmQueTrabalha()
    {

        if (is_numeric($this->getId()))
        {

            $objBanco = new Database();
            $objBanco->query("SELECT e.nome, p.nome FROM pessoa_empresa AS pe, empresa AS e, profissao AS p
                              WHERE pe.pessoa_id_INT={$this->getId()}
                              AND e.id IN (SELECT ep.empresa_id_INT FROM empresa_perfil AS ep WHERE ep.perfil_id_INT=1 OR ep.perfil_id_INT=4)
                              AND pe.empresa_id_INT=e.id
                              AND p.id=pe.profissao_id_INT
                              ORDER BY e.nome");

            $strRetorno = "";
            for ($i = 0; $dados = $objBanco->fetchArray(); $i++)
            {

                $strRetorno .= "{$dados[0]} - {$dados[1]}";

                if ($i < $objBanco->rows() - 1)
                {
                    $strRetorno .= "<br>";
                }
            }
        }

        return $strRetorno;
    }

    public function getEnderecoJSON()
    {
        $idPessoa = Helper::GET("pessoa_id_INT");

        if (is_numeric($idPessoa))
        {

            $objPessoa = new EXTDAO_Pessoa();
            $objPessoa->select($idPessoa);
            $objPessoa->formatarParaExibicao();

            echo json_encode(array(utf8_encode($objPessoa->getLogradouro()), $objPessoa->getNumero(), $objPessoa->getComplemento(), $objPessoa->getCidade_id_INT(), $objPessoa->getBairro_id_INT()));
        }
        else
        {

            echo "null";
        }
    }

    public static function getPessoaDoEmail($email, $db = null)
    {
        if ($db == null)
        {
            $db = new Database();
        }

        $q = "SELECT id "
            . " FROM pessoa "
            . " WHERE email = '$email' ";
        $db->query($q);

        return $db->getPrimeiraTuplaDoResultSet(0);
    }

    public function verificarEmail($email, $idPessoa = null)
    {

        if (!is_null($idPessoa))
        {
            $strComplemento = "AND id <> {$idPessoa}";
        }

        if (strlen($email))
        {
            $objBanco = new Database();
            $objBanco->query("SELECT id FROM pessoa WHERE email='{$email}' {$strComplemento} AND corporacao_id_INT = " . Seguranca::getIdDaCorporacaoLogada());

            if ($objBanco->rows() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return true;
        }
    }

    public function updateOperadora()
    {
        $vCelular = $this->getCelular();
        if (strlen($vCelular))
        {
            $vOperadora_id_INT = EXTDAO_Operadora::getIdOperadoraFromServiceHttp($vCelular);
            if (strlen($vOperadora_id_INT))
            {
                $this->setOperadora_id_INT($vOperadora_id_INT);

                return true;
            }
            else
            {
                return false;
            }
        }

        return false;
    }

    public static function setListAliasRelatedAttributes()
    {
        if (is_null(static::$listAliasRelatedAttributes))
        {
            static::$listAliasRelatedAttributes = new stdClass();
            static::$listAliasRelatedAttributes->tipo_documento__nome = "tipoDocumentoNome";
            static::$listAliasRelatedAttributes->sexo__nome = "sexoNome";
            static::$listAliasRelatedAttributes->cidade__nome = "cidadeNome";
            static::$listAliasRelatedAttributes->pessoa__id = "id";
            static::$listAliasRelatedAttributes->pessoa__identificador = "identificador";
            static::$listAliasRelatedAttributes->pessoa__corporacao_id_INT = "corporacaoId";
            static::$listAliasRelatedAttributes->pessoa__numero_documento = "numeroDocumento";
            static::$listAliasRelatedAttributes->pessoa__nome = "nome";
            static::$listAliasRelatedAttributes->pessoa__email = "email";
            static::$listAliasRelatedAttributes->pessoa__corporacao_id_INT = "corporacaoId";
        }
    }

    public function __getList($parameters = null, $idCorporacao = null)
    {
        if (is_null($parameters))
        {
            $parameters = json_decode(file_get_contents('php://input'));
        }
        try
        {
            $filterParameters = new stdClass();
            if (isset($parameters->filterParameters))
            {
                $filterParameters = $parameters->filterParameters;
            }
            if (is_null($idCorporacao))
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            ListParameters::addFilterParameterToObject($filterParameters, "corporacaoId", $idCorporacao);
            $sortingParameters = null;
            if (isset($parameters->sortingParameters))
            {
                $sortingParameters = $parameters->sortingParameters;
            }
            $paginationParameters = null;
            if (isset($parameters->paginationParameters))
            {
                $paginationParameters = $parameters->paginationParameters;
            }
            $listMappingType = static::getListMappingType($parameters->listMappingType);
            $mainTableAlias = "p";
            $whereClause = static::getWhereClauseForFilter($filterParameters, $mainTableAlias);
            $orderByClause = static::getOrderByClauseForFilter($sortingParameters, $mainTableAlias);
            $limitClause = static::getLimitClauseForFilter($paginationParameters);
            $queryWithoutLimit = "SELECT p.id FROM pessoa p {$whereClause}";
            $query = "SELECT * FROM
                                (
                                SELECT
                                    CASE WHEN u.id IS NOT NULL THEN
                                            1
                                        ELSE
                                            0
                                    END AS tem_usuario_associado,
                                    CASE WHEN u.id IS NOT NULL THEN
                                            u.nome
                                        ELSE
                                            pes.nome
                                    END AS pessoa__nome,
                                    CASE WHEN u.id IS NOT NULL THEN
                                            u.nome_normalizado
                                        ELSE
                                            pes.nome_normalizado
                                    END AS pessoa__nome_normalizado,
                                    CASE WHEN u.id IS NOT NULL THEN
                                            u.email
                                        ELSE
                                            pes.email
                                    END AS pessoa__email,
                                    td.nome AS tipo_documento__nome,
                                    s.nome AS sexo__nome,
                                    o.nome AS operadora__nome,
                                    c.nome AS cidade__nome,
                                    pes.id AS pessoa__id,
                                    pes.identificador AS pessoa__identificador,
                                    pes.numero_documento AS pessoa__numero_documento,
                                    pes.is_consumidor_BOOLEAN AS pessoa__is_consumidor_BOOLEAN,
                                    pes.telefone AS pessoa__telefone,
                                    pes.celular AS pessoa__celular,
                                    pes.celular_sms AS pessoa__celular_sms,
                                    pes.logradouro AS pessoa__logradouro,
                                    pes.numero AS pessoa__numero,
                                    pes.complemento AS pessoa__complemento,
                                    pes.corporacao_id_INT AS pessoa__corporacao_id_INT,
                                    u.id AS usuario_id_INT,
                                    pes.*
                                FROM
                                    pessoa pes
                                    LEFT JOIN pessoa_usuario pu ON pu.pessoa_id_INT = pes.id
                                    LEFT JOIN usuario u ON u.id = pu.usuario_id_INT AND u.id IN (SELECT uc.usuario_id_INT FROM usuario_corporacao uc WHERE uc.corporacao_id_INT=pes.corporacao_id_INT)
                                    LEFT JOIN tipo_documento td ON td.id = pes.tipo_documento_id_INT
                                    LEFT JOIN sexo s ON s.id = pes.sexo_id_INT
                                    LEFT JOIN operadora o ON o.id = pes.operadora_id_INT
                                    LEFT JOIN cidade c ON c.id = pes.cidade_id_INT
                                ) p {$whereClause} {$orderByClause} {$limitClause}";

            $msg = $this->database->queryMensagem($queryWithoutLimit);
            if ($msg != null && $msg->erro())
            {
                return $msg;
            }

            $totalNumberOfRecords = $this->database->rows();
            if ($totalNumberOfRecords == 0)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
            }
            $msg = $this->database->queryMensagem($query);

            //operacao realizaca com sucesso
            if ($msg == null)
            {
                if ($this->database->rows() == 0)
                {
                    return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                }
                $resultSet = $this->database->result;
                $objReturn = new stdClass();
                $objReturn->dataSet = static::mapResultSetDataToList($resultSet, $listMappingType, static::$listAliasTypes);
                $lastPage = ceil($totalNumberOfRecords / $paginationParameters->recordsPerPage);
                $objReturn->paginationParameters = new PaginationParams($paginationParameters->recordsPerPage, $paginationParameters->currentPage, $lastPage);
                return new Mensagem_generica($objReturn);
            }
            else
            {
                return $msg;
            }
        }
        catch (Exception $ex)
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
        }

    }

    public function __getRecord($parameters = null)
    {
        if (is_null($parameters))
        {
            $parameters = json_decode(file_get_contents('php://input'));
        }

        try
        {

            $idPessoa = static::getFilterValueFromFilterArray("id", $parameters->filterParameters);

            if ($this->verificarSePessoaTemUsuarioAssociado($idPessoa))
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ACESSO_NEGADO,
                    I18N::getExpression("Essa operação não é permitida! Pois essa pessoa possui usuário associado pelo email!"));
            }

            $returnObject = new stdClass();
            $whereClause = static::getWhereClauseForFilter($parameters->filterParameters);
            $query = "SELECT  id, identificador, nome, nome_normalizado, tipo_documento_id_INT, numero_documento, is_consumidor_BOOLEAN, sexo_id_INT, telefone, celular, operadora_id_INT, celular_sms, email, logradouro, logradouro_normalizado, numero, complemento, complemento_normalizado, cidade_id_INT, bairro_id_INT, latitude_INT, longitude_INT, foto, corporacao_id_INT, cadastro_SEC, cadastro_OFFSEC FROM pessoa {$whereClause}";
            $msg = $this->database->queryMensagem($query);
            if ($msg == null)
            {
                if ($this->database->rows() == 0)
                {
                    return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Não foi encontrado nenhum registro"));
                }
                $resultSet = $this->database->result;
                $resultObject = static::mapResultSetDataToRecord($resultSet);
                $returnObject->formData = $resultObject;
                $returnObject->comboBoxesData = null;
                if ($parameters->loadComboBoxesData)
                {
                    $this->setByObject($resultObject);
                    $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                }
                return new Mensagem_generica($returnObject);
            }
            else
            {
                return $msg;
            }

        }
        catch (Exception $ex)
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
        }

    }

    public function __actionAdd($parameters = null)
    {
        if (is_null($parameters))
        {
            $parameters = Helper::getPhpInputObject();
        }

        try
        {
            $this->setByObject($parameters);
            if(Helper::isNullOrEmpty($this->nome))
            {
                $this->nome = Helper::getNomeDeEntidadeAPartirDoEmail($this->email);
            }


            if($this->verificarEmail($this->email))
                return new Mensagem(
                    PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO,
                    "Ja existe uma pessoa com o email ".$this->email);

            $this->setDataCadastroAndOffsetInSecondsFromUser($parameters);

            $this->formatarParaSQL();
            $msg = $this->insert(true);

            if ($msg != null && $msg->erro())
            {
                return $msg;
            }
            return new Mensagem(null, I18N::getExpression("Pessoa adicionada com sucesso."));
        }
        catch (Exception $ex)
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
        }
    }

    public function __actionEdit($parameters = null)
    {
        if (is_null($parameters))
        {
            $parameters = Helper::getPhpInputObject();
        }

        try
        {
            $this->setByObject($parameters);
            if(Helper::isNullOrEmpty($this->nome))
            {
                $this->nome = Helper::getNomeDeEntidadeAPartirDoEmail($this->email);
            }

            $this->setAllFieldsToHumanFormat();

            $this->formatarParaSQL();
            $msg = $this->updateByInstanceUsingParameters($this->id, $parameters, true);
            if ($msg != null && $msg->erro())
            {
                return $msg;
            }

            return new Mensagem(null, I18N::getExpression("Pessoa editada com sucesso."));
        }
        catch (Exception $ex)
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
        }
    }

    public function __actionRemove($parameters = null)
    {
        $successulRemovalCount = 0;
        if (is_null($parameters))
        {
            $parameters = json_decode(file_get_contents('php://input'));
        }
        try
        {
            foreach ($parameters as $record)
            {
                if (is_numeric($record->id))
                {
                    if (!$this->verificarSePessoaTemUsuarioAssociado($record->id))
                    {
                        $msg = $this->delete($record->id, true);
                        if ($msg != null && $msg->erro())
                        {
                            $successulRemovalCount++;
                        }
                    }
                }
            }
            if ($successulRemovalCount > 0)
            {
                $successMessage = count($successulRemovalCount) > 1 ? I18N::getExpression("{0} Pessoas foram removidas com sucesso.", $successulRemovalCount) : I18N::getExpression("Pessoa removida com sucesso.");
                return new Mensagem(null, $successMessage);
            }
            else
            {
                $errorMessage = count($parameters) > 1 ? I18N::getExpression("Falha ao remover Pessoas.") : I18N::getExpression("Falha ao remover Pessoa.");
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, $errorMessage);
            }
        }
        catch (Exception $ex)
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
        }
    }

    public function verificarSePessoaTemUsuarioAssociado($idPessoa)
    {
        $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
        if (is_integer($idPessoa) && $idPessoa > 0)
        {
            $q = "SELECT u.id 
                    FROM pessoa pes
                        LEFT JOIN pessoa_usuario pu ON pu.pessoa_id_INT = pes.id
                        LEFT JOIN usuario u ON u.id = pu.usuario_id_INT AND u.id IN (SELECT uc.usuario_id_INT FROM usuario_corporacao uc WHERE uc.corporacao_id_INT = pes.corporacao_id_INT AND pes.corporacao_id_INT = {$idCorporacao})
                    WHERE pes.id={$idPessoa}";

            $this->database->query($q);
            return ($this->database->getPrimeiraTuplaDoResultSet(0) != null);
        }
        else
        {
            return false;
        }
    }

    public static function mapResultSetDataToListForGrid($resultSetData, $objAliasTypes = null)
    {
        $returnData = array();
        $formatForHuman = !is_null($objAliasTypes);
        for ($i = 0; $object = mysqli_fetch_object($resultSetData); $i++)
        {
            $currentObject = new stdClass();
            $currentObject->allowEdit = true;
            $currentObject->allowRemove = true;

            if ($object->tem_usuario_associado)
            {
                $currentObject->allowRemove = false;
                $currentObject->temUsuarioAssociado = true;
            }

            foreach ($object as $key => $value)
            {
                $attributeName = $key;
                $attributeNameForHuman = "{$attributeName}_forHuman";

                if ($formatForHuman)
                {
                    $attributeType = $objAliasTypes->$key;
                    $valueForHuman = static::formatarValorParaExibicao($value, $attributeType);
                }
                else
                {
                    $valueForHuman = $value;
                }

                $currentObject->$attributeName = $value;
                $currentObject->$attributeNameForHuman = $valueForHuman;
            }

            $currentObject->primaryKey = static::getPrimaryKeyObject($currentObject);
            $returnData[] = $currentObject;
        }

        return $returnData;
    }

    public function factory()
    {
        return new EXTDAO_Pessoa();
    }
}

?>
