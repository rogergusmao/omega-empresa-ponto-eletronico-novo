<?php


        /*

        Arquivo gerado através de gerador de código em 12/05/2018 as 14:52:59.
        Para que o arquivo não seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

        Tabela correspondente: wifi
        Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

        */
        


class DAO_Wifi extends Generic_DAO
                        {
                            const NOME_ENTIDADE = "wifi";

                            public static $nomeTabela = null;
                            public static $idSistemaTabela = false;
                            public static $databaseFieldsRelatedAttributes = null;
	                        public static $databaseFieldTypes = null;
	                        public static $databaseFieldNames = null;
	                        public static $listAliasTypes = null;
	                        public static $listAliasRelatedAttributes = null;
	                        
	                        public $id;
public $networkId;
public $rssi;
public $ssid;
public $usuario;
public $senha;
public $ip;
public $linkSpeed;
public $macAddress;
public $frequency;
public $bssid;
public $empresaId;
public $pessoaId;
public $dataSec;
public $dataOffsec;
public $contadorVerificacaoInt;
public $corporacaoId;

public $labelId;
public $labelNetworkId;
public $labelRssi;
public $labelSsid;
public $labelUsuario;
public $labelSenha;
public $labelIp;
public $labelLinkSpeed;
public $labelMacAddress;
public $labelFrequency;
public $labelBssid;
public $labelEmpresaId;
public $labelPessoaId;
public $labelDataSec;
public $labelDataOffsec;
public $labelContadorVerificacaoInt;
public $labelCorporacaoId;

public static $arrPK = array("id");

public function __construct($configDAO = null)
        {
            parent::__construct($configDAO);
            static::__setStaticValues($this->database);           
        }

public static function idSistemaTabela($databaseObject = null){
            if(!(static::$idSistemaTabela))
            {
                static::$idSistemaTabela = EXTDAO_Sistema_tabela::getIdDaSistemaTabela(static::nomeTabela(), $databaseObject);
            }
            return static::$idSistemaTabela;
        }

public function getIdSistemaTabela(){
            return static::idSistemaTabela();
        }

public static function nomeTabela(){
            if(is_null(static::$nomeTabela))
            {
                static::$nomeTabela = "wifi";
            }
            return static::$nomeTabela;
        }

public function getNomeTabela(){
            return static::nomeTabela();
        }

public static function __setStaticValues($databaseObject = null)
                   {    
                        static::nomeTabela();
                        
                        static::idSistemaTabela($databaseObject);
                   
                        static::setDatabaseFieldNames();
                        static::setDatabaseFieldTypes();
                        static::setDatabaseFieldsRelatedAttributeNames();
                        
                        static::setListAliasTypes();
                        static::setListAliasRelatedAttributes();
                   }

public function getExpressaoLabel()
                   {
                        return "{id}";
                   }

public static function setDatabaseFieldNames()
        {
            if(is_null(static::$databaseFieldNames))
            {
                static::$databaseFieldNames = new stdClass();
                static::$databaseFieldNames->id = "id";
static::$databaseFieldNames->networkId = "network_id";
static::$databaseFieldNames->rssi = "rssi";
static::$databaseFieldNames->ssid = "ssid";
static::$databaseFieldNames->usuario = "usuario";
static::$databaseFieldNames->senha = "senha";
static::$databaseFieldNames->ip = "ip";
static::$databaseFieldNames->linkSpeed = "link_speed";
static::$databaseFieldNames->macAddress = "mac_address";
static::$databaseFieldNames->frequency = "frequency";
static::$databaseFieldNames->bssid = "bssid";
static::$databaseFieldNames->empresaId = "empresa_id_INT";
static::$databaseFieldNames->pessoaId = "pessoa_id_INT";
static::$databaseFieldNames->dataSec = "data_SEC";
static::$databaseFieldNames->dataOffsec = "data_OFFSEC";
static::$databaseFieldNames->contadorVerificacaoInt = "contador_verificacao_INT";
static::$databaseFieldNames->corporacaoId = "corporacao_id_INT";

            }
                      
        }

public static function setDatabaseFieldTypes()
        {
            if(is_null(static::$databaseFieldTypes))
            {
                static::$databaseFieldTypes = new stdClass();
                static::$databaseFieldTypes->id = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->networkId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->rssi = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->ssid = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->usuario = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->senha = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->ip = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->linkSpeed = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->macAddress = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->frequency = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->bssid = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->empresaId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->pessoaId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->dataSec = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$databaseFieldTypes->dataOffsec = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$databaseFieldTypes->contadorVerificacaoInt = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->corporacaoId = static::TIPO_VARIAVEL_INTEGER;

            }
            
        }

public static function setDatabaseFieldsRelatedAttributeNames()
        {
            if(is_null(static::$databaseFieldsRelatedAttributes))
            {
                static::$databaseFieldsRelatedAttributes = new stdClass();
                static::$databaseFieldsRelatedAttributes->id = "id";
static::$databaseFieldsRelatedAttributes->network_id = "networkId";
static::$databaseFieldsRelatedAttributes->rssi = "rssi";
static::$databaseFieldsRelatedAttributes->ssid = "ssid";
static::$databaseFieldsRelatedAttributes->usuario = "usuario";
static::$databaseFieldsRelatedAttributes->senha = "senha";
static::$databaseFieldsRelatedAttributes->ip = "ip";
static::$databaseFieldsRelatedAttributes->link_speed = "linkSpeed";
static::$databaseFieldsRelatedAttributes->mac_address = "macAddress";
static::$databaseFieldsRelatedAttributes->frequency = "frequency";
static::$databaseFieldsRelatedAttributes->bssid = "bssid";
static::$databaseFieldsRelatedAttributes->empresa_id_INT = "empresaId";
static::$databaseFieldsRelatedAttributes->pessoa_id_INT = "pessoaId";
static::$databaseFieldsRelatedAttributes->data_SEC = "dataSec";
static::$databaseFieldsRelatedAttributes->data_OFFSEC = "dataOffsec";
static::$databaseFieldsRelatedAttributes->contador_verificacao_INT = "contadorVerificacaoInt";
static::$databaseFieldsRelatedAttributes->corporacao_id_INT = "corporacaoId";

            }           
        }

public function setAllFieldsToHumanFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["network_id"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["rssi"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["ssid"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["usuario"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["senha"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["ip"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["link_speed"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["mac_address"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["frequency"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["bssid"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["empresa_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["pessoa_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["data_SEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["data_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["contador_verificacao_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
}

public function setAllFieldsToDatabaseFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["network_id"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["rssi"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["ssid"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["usuario"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["senha"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["ip"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["link_speed"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["mac_address"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["frequency"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["bssid"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["empresa_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["pessoa_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["data_SEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["data_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["contador_verificacao_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
}

  public function getFkObjEmpresa() 
                {
                    if($this->objEmpresa == null)
                    {                        
                        $this->objEmpresa = new EXTDAO_Empresa_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getEmpresa_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objEmpresa->clear();
                    }
                    elseif($this->objEmpresa->getId() != $idFK)
                    {
                        $this->objEmpresa->select($idFK);
                    }
                    return $this->objEmpresa;
                }
  public function getFkObjPessoa() 
                {
                    if($this->objPessoa == null)
                    {                        
                        $this->objPessoa = new EXTDAO_Pessoa_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getPessoa_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objPessoa->clear();
                    }
                    elseif($this->objPessoa->getId() != $idFK)
                    {
                        $this->objPessoa->select($idFK);
                    }
                    return $this->objPessoa;
                }
  public function getFkObjCorporacao() 
                {
                    if($this->objCorporacao == null)
                    {                        
                        $this->objCorporacao = new EXTDAO_Corporacao_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getCorporacao_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objCorporacao->clear();
                    }
                    elseif($this->objCorporacao->getId() != $idFK)
                    {
                        $this->objCorporacao->select($idFK);
                    }
                    return $this->objCorporacao;
                }


public function setLabels()
        {$this->labelId = "id";
$this->labelNetworkId = "networkid";
$this->labelRssi = "rssi";
$this->labelSsid = "ssid";
$this->labelUsuario = "usuario";
$this->labelSenha = "senha";
$this->labelIp = "ip";
$this->labelLinkSpeed = "linkspeed";
$this->labelMacAddress = "macaddress";
$this->labelFrequency = "frequency";
$this->labelBssid = "bssid";
$this->labelEmpresaId = "empresaidINT";
$this->labelPessoaId = "pessoaidINT";
$this->labelDataSec = "";
$this->labelDataOffsec = "";
$this->labelContadorVerificacaoInt = "contadorverificacaoINT";
$this->labelCorporacaoId = "corporacaoidINT";
}

public function __actionAdd($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                $this->setByObject($parameters);
                $this->setDataCadastroAndOffsetInSecondsFromUser($parameters);
                
                $this->formatarParaSQL();
                $msg = $this->insert(true);
                
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                return new Mensagem(null, I18N::getExpression("Wifi adicionado com sucesso."));
                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionEdit($parameters = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try {

                $this->setByObject($parameters);
                $this->setAllFieldsToHumanFormat();
                
                $this->formatarParaSQL();
                $msg = $this->updateByInstanceUsingParameters($this->id, $parameters, true);
                if($msg != null && $msg->erro())
                {
                     return $msg;
                }
        
                return new Mensagem(null, I18N::getExpression("Wifi editado com sucesso."));
            }
            catch(Exception $ex)
            {                
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionRemove($parameters = null)
        {
            $successulRemovalCount = 0;
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                foreach($parameters as $record)
                {
                    if(is_numeric($record->id))
                    {
                        $msg = $this->delete($record->id, true);
                        if($msg == null || $msg->ok())
                        {
                            $successulRemovalCount++;
                        }
                    }
                }
                
                if($successulRemovalCount > 0)
                {
                    $successMessage = count($successulRemovalCount) > 1 ? I18N::getExpression("{0} Wifi foram removidos com sucesso.", $successulRemovalCount) : I18N::getExpression("Wifi removido com sucesso.");
                    return new Mensagem(null, $successMessage);
                }
                else
                {
                    $errorMessage = count($parameters) > 1 ? I18N::getExpression("Falha ao remover Wifi.") : I18N::getExpression("Falha ao remover Wifi.");
                    return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, $errorMessage);                    
                }
            }
            catch (Exception $ex) 
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }

public function __getRecord($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }
            
            try
            {            
                $returnObject = new stdClass();            
                $whereClause = static::getWhereClauseForFilter($parameters->filterParameters);
                $query = "SELECT  id, network_id, rssi, ssid, usuario, senha, ip, link_speed, mac_address, frequency, bssid, empresa_id_INT, pessoa_id_INT, data_SEC, data_OFFSEC, contador_verificacao_INT, corporacao_id_INT FROM wifi {$whereClause}";
    
                $msg = $this->database->queryMensagem($query);                
                if ($msg == null)
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Não foi encontrado nenhum registro"));
                    }                    
                    $resultSet = $this->database->result;
                    $resultObject = static::mapResultSetDataToRecord($resultSet);
                    
                    $returnObject->formData = $resultObject;
                    $returnObject->comboBoxesData = null;
                    if($parameters->loadComboBoxesData)
                    {
                        $this->setByObject($resultObject);
                        $returnObject->comboBoxesData =  $this->getFormComboBoxesData();
                    }                    
    
                    
    
                    return new Mensagem_generica($returnObject);
                } 
                else 
                {
                    return $msg;
                }
                
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }


        
        public function __getFilterComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
                
        public function __getFormComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
        
        protected function getFormComboBoxesData()
        {
            $comboBoxesData = new stdClass();
            $listParameters = new stdClass();
            $listParameters->listMappingType = static::LIST_MAPPING_TYPE_COMBOBOX;
            
            
                    
                    $objEmpresa = new EXTDAO_Empresa();
                    $comboBoxesData->fieldEmpresaId = $objEmpresa->__getList($listParameters);
                    
                    $objPessoa = new EXTDAO_Pessoa();
                    $comboBoxesData->fieldPessoaId = $objPessoa->__getList($listParameters);

            return $comboBoxesData;
        }
        
        


        
        public static function setListAliasTypes()
        {
            if(is_null(static::$listAliasTypes))
            {
                static::$listAliasTypes = new stdClass();
                static::$listAliasTypes->empresa__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->pessoa__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->wifi__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->wifi__network_id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->wifi__rssi = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->wifi__ssid = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->wifi__usuario = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->wifi__senha = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->wifi__ip = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->wifi__link_speed = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->wifi__mac_address = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->wifi__frequency = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->wifi__bssid = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->wifi__ind_celular_valido_BOOLEAN = static::TIPO_VARIAVEL_BOOLEAN;
static::$listAliasTypes->wifi__ind_celular_valido_BOOLEAN = static::TIPO_VARIAVEL_BOOLEAN;
static::$listAliasTypes->wifi__data_SEC = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$listAliasTypes->wifi__data_OFFSEC = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$listAliasTypes->wifi__contador_verificacao_INT = static::TIPO_VARIAVEL_INTEGER;
            }        
        }
        
        public static function setListAliasRelatedAttributes()
        {
            if(is_null(static::$listAliasRelatedAttributes))
            {
                static::$listAliasRelatedAttributes = new stdClass();
                static::$listAliasRelatedAttributes->empresa__id = "empresaId";
static::$listAliasRelatedAttributes->pessoa__id = "pessoaId";
static::$listAliasRelatedAttributes->wifi__id = "id";
static::$listAliasRelatedAttributes->wifi__network_id = "networkId";
static::$listAliasRelatedAttributes->wifi__rssi = "rssi";
static::$listAliasRelatedAttributes->wifi__ssid = "ssid";
static::$listAliasRelatedAttributes->wifi__usuario = "usuario";
static::$listAliasRelatedAttributes->wifi__senha = "senha";
static::$listAliasRelatedAttributes->wifi__ip = "ip";
static::$listAliasRelatedAttributes->wifi__link_speed = "linkSpeed";
static::$listAliasRelatedAttributes->wifi__mac_address = "macAddress";
static::$listAliasRelatedAttributes->wifi__frequency = "frequency";
static::$listAliasRelatedAttributes->wifi__bssid = "bssid";
static::$listAliasRelatedAttributes->wifi__ind_celular_valido_BOOLEAN = "indCelularValidoBoolean";
static::$listAliasRelatedAttributes->wifi__ind_celular_valido_BOOLEAN = "indCelularValidoBoolean";
static::$listAliasRelatedAttributes->wifi__data_SEC = "dataSec";
static::$listAliasRelatedAttributes->wifi__data_OFFSEC = "dataOffsec";
static::$listAliasRelatedAttributes->wifi__contador_verificacao_INT = "contadorVerificacaoInt";
            }         
        }

        public function __getList($parameters = null, $idCorporacao = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try
            {
                $filterParameters = new stdClass();
                if(isset($parameters->filterParameters))
                {
                    $filterParameters = $parameters->filterParameters;
                }
                                    
                            
            if(is_null($idCorporacao))
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            ListParameters::addFilterParameterToObject($filterParameters, "corporacaoId", $idCorporacao);           
                
                
                $sortingParameters = null;
                if(isset($parameters->sortingParameters))
                {
                    $sortingParameters = $parameters->sortingParameters;
                }
                
                $paginationParameters = null;
                if(isset($parameters->paginationParameters))
                {
                    $paginationParameters = $parameters->paginationParameters;
                }
                
                $listMappingType = static::getListMappingType($parameters->listMappingType);
                $mainTableAlias = "w";

                $whereClause = static::getWhereClauseForFilter($filterParameters, $mainTableAlias);
                $orderByClause = static::getOrderByClauseForFilter($sortingParameters, $mainTableAlias);
                $limitClause = static::getLimitClauseForFilter($paginationParameters);

                $queryWithoutLimit = "SELECT w.id FROM wifi w {$whereClause}";
                $query = "SELECT e.id AS empresa__id, p.id AS pessoa__id, w.id AS wifi__id, w.network_id AS wifi__network_id, w.rssi AS wifi__rssi, w.ssid AS wifi__ssid, w.usuario AS wifi__usuario, w.senha AS wifi__senha, w.ip AS wifi__ip, w.link_speed AS wifi__link_speed, w.mac_address AS wifi__mac_address, w.frequency AS wifi__frequency, w.bssid AS wifi__bssid, w.ind_celular_valido_BOOLEAN AS wifi__ind_celular_valido_BOOLEAN, w.ind_celular_valido_BOOLEAN AS wifi__ind_celular_valido_BOOLEAN, w.data_SEC AS wifi__data_SEC, w.data_OFFSEC AS wifi__data_OFFSEC, w.contador_verificacao_INT AS wifi__contador_verificacao_INT FROM wifi w LEFT JOIN empresa e ON e.id = w.empresa_id_INT LEFT JOIN pessoa p ON p.id = w.pessoa_id_INT {$whereClause} {$orderByClause} {$limitClause}";

                $msg = $this->database->queryMensagem($queryWithoutLimit);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                
                $totalNumberOfRecords = $this->database->rows();
                if($totalNumberOfRecords == 0)
                {
                    return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                }                        
                        
                $msg = $this->database->queryMensagem($query);
                
                //operacao realizaca com sucesso
                if ($msg == null) 
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                    }                        
                
                    $resultSet = $this->database->result;

                    $objReturn = new stdClass();
                    $objReturn->dataSet = static::mapResultSetDataToList($resultSet, $listMappingType, static::$listAliasTypes);

                    if(!Helper::isNullOrEmpty($paginationParameters->recordsPerPage) && !Helper::isNullOrEmpty($paginationParameters->currentPage))
                    {
                        $lastPage = ceil($totalNumberOfRecords / $paginationParameters->recordsPerPage);
                        $objReturn->paginationParameters = new PaginationParams($paginationParameters->recordsPerPage, $paginationParameters->currentPage, $lastPage);
                    }
                    
                    return new Mensagem_generica($objReturn);
                } 
                else
                {
                    return $msg;
                }                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }



public function getId()
            {
                return $this->id;
            }

public function getNetworkId()
            {
                return $this->networkId;
            }

public function getNetwork_id()
                {
                    return $this->networkId;
                }

public function getRssi()
            {
                return $this->rssi;
            }

public function getSsid()
            {
                return $this->ssid;
            }

public function getUsuario()
            {
                return $this->usuario;
            }

public function getSenha()
            {
                return $this->senha;
            }

public function getIp()
            {
                return $this->ip;
            }

public function getLinkSpeed()
            {
                return $this->linkSpeed;
            }

public function getLink_speed()
                {
                    return $this->linkSpeed;
                }

public function getMacAddress()
            {
                return $this->macAddress;
            }

public function getMac_address()
                {
                    return $this->macAddress;
                }

public function getFrequency()
            {
                return $this->frequency;
            }

public function getBssid()
            {
                return $this->bssid;
            }

public function getEmpresaId()
            {
                return $this->empresaId;
            }

public function getEmpresa_id_INT()
                {
                    return $this->empresaId;
                }

public function getPessoaId()
            {
                return $this->pessoaId;
            }

public function getPessoa_id_INT()
                {
                    return $this->pessoaId;
                }

public function getDataSec()
            {
                return $this->dataSec;
            }

public function getData_SEC()
                {
                    return $this->dataSec;
                }

public function getDataOffsec()
            {
                return $this->dataOffsec;
            }

public function getData_OFFSEC()
                {
                    return $this->dataOffsec;
                }

public function getContadorVerificacaoInt()
            {
                return $this->contadorVerificacaoInt;
            }

public function getContador_verificacao_INT()
                {
                    return $this->contadorVerificacaoInt;
                }

public function getCorporacaoId()
            {
                return $this->corporacaoId;
            }

public function getCorporacao_id_INT()
                {
                    return $this->corporacaoId;
                }



function setId($value)
            {
                $this->id = $value;
            }

function setNetworkId($value)
            {
                $this->networkId = $value;
            }

public function setNetwork_id($value)
                { 
                    $this->networkId = $value; 
                }

function setRssi($value)
            {
                $this->rssi = $value;
            }

function setSsid($value)
            {
                $this->ssid = $value;
            }

function setUsuario($value)
            {
                $this->usuario = $value;
            }

function setSenha($value)
            {
                $this->senha = $value;
            }

function setIp($value)
            {
                $this->ip = $value;
            }

function setLinkSpeed($value)
            {
                $this->linkSpeed = $value;
            }

public function setLink_speed($value)
                { 
                    $this->linkSpeed = $value; 
                }

function setMacAddress($value)
            {
                $this->macAddress = $value;
            }

public function setMac_address($value)
                { 
                    $this->macAddress = $value; 
                }

function setFrequency($value)
            {
                $this->frequency = $value;
            }

function setBssid($value)
            {
                $this->bssid = $value;
            }

function setEmpresaId($value)
            {
                $this->empresaId = $value;
            }

public function setEmpresa_id_INT($value)
                { 
                    $this->empresaId = $value; 
                }

function setPessoaId($value)
            {
                $this->pessoaId = $value;
            }

public function setPessoa_id_INT($value)
                { 
                    $this->pessoaId = $value; 
                }

function setDataSec($value)
            {
                $this->dataSec = $value;
            }

public function setData_SEC($value)
                { 
                    $this->dataSec = $value; 
                }

function setDataOffsec($value)
            {
                $this->dataOffsec = $value;
            }

public function setData_OFFSEC($value)
                { 
                    $this->dataOffsec = $value; 
                }

function setContadorVerificacaoInt($value)
            {
                $this->contadorVerificacaoInt = $value;
            }

public function setContador_verificacao_INT($value)
                { 
                    $this->contadorVerificacaoInt = $value; 
                }

function setCorporacaoId($value)
            {
                $this->corporacaoId = $value;
            }

public function setCorporacao_id_INT($value)
                { 
                    $this->corporacaoId = $value; 
                }



public function clear()
        {$this->id = null;
$this->networkId = null;
$this->rssi = null;
$this->ssid = null;
$this->usuario = null;
$this->senha = null;
$this->ip = null;
$this->linkSpeed = null;
$this->macAddress = null;
$this->frequency = null;
$this->bssid = null;
$this->empresaId = null;
if($this->objEmpresa != null) unset($this->objEmpresa);
$this->pessoaId = null;
if($this->objPessoa != null) unset($this->objPessoa);
$this->dataSec = null;
$this->dataOffsec = null;
$this->contadorVerificacaoInt = null;
$this->corporacaoId = null;
if($this->objCorporacao != null) unset($this->objCorporacao);
if(is_array($this->arrEstadosCampos)){
                        unset($this->arrEstadosCampos);
                     $this->arrEstadosCampos = array();
                    }
                }

public function formatarParaSQL()
        {
if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->id)){
$this->id = $this->formatarIntegerParaComandoSQL($this->id);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->id);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->networkId)){
$this->networkId = $this->formatarIntegerParaComandoSQL($this->networkId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->networkId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->rssi)){
$this->rssi = $this->formatarIntegerParaComandoSQL($this->rssi);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->rssi);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->ssid)){
$this->ssid = $this->formatarStringParaComandoSQL($this->ssid);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->ssid);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->usuario)){
$this->usuario = $this->formatarStringParaComandoSQL($this->usuario);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->usuario);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->senha)){
$this->senha = $this->formatarStringParaComandoSQL($this->senha);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->senha);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->ip)){
$this->ip = $this->formatarIntegerParaComandoSQL($this->ip);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->ip);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->linkSpeed)){
$this->linkSpeed = $this->formatarIntegerParaComandoSQL($this->linkSpeed);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->linkSpeed);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->macAddress)){
$this->macAddress = $this->formatarStringParaComandoSQL($this->macAddress);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->macAddress);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->frequency)){
$this->frequency = $this->formatarIntegerParaComandoSQL($this->frequency);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->frequency);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->bssid)){
$this->bssid = $this->formatarStringParaComandoSQL($this->bssid);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->bssid);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->empresaId)){
$this->empresaId = $this->formatarIntegerParaComandoSQL($this->empresaId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->empresaId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->pessoaId)){
$this->pessoaId = $this->formatarIntegerParaComandoSQL($this->pessoaId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->pessoaId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->dataSec)){
$this->dataSec = $this->formatarIntegerParaComandoSQL($this->dataSec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->dataSec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->dataOffsec)){
$this->dataOffsec = $this->formatarIntegerParaComandoSQL($this->dataOffsec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->dataOffsec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->contadorVerificacaoInt)){
$this->contadorVerificacaoInt = $this->formatarIntegerParaComandoSQL($this->contadorVerificacaoInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->contadorVerificacaoInt);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->corporacaoId)){
$this->corporacaoId = $this->formatarIntegerParaComandoSQL($this->corporacaoId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->corporacaoId);
}

}

public function formatarParaExibicao()
        {if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->ssid)){
$this->ssid = $this->formatarStringParaExibicao($this->ssid);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->ssid);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->usuario)){
$this->usuario = $this->formatarStringParaExibicao($this->usuario);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->usuario);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->senha)){
$this->senha = $this->formatarStringParaExibicao($this->senha);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->senha);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->macAddress)){
$this->macAddress = $this->formatarStringParaExibicao($this->macAddress);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->macAddress);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->bssid)){
$this->bssid = $this->formatarStringParaExibicao($this->bssid);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->bssid);
}

}

function select($id, $idCorporacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }   

            $sql = "SELECT id, network_id, rssi, ssid, usuario, senha, ip, link_speed, mac_address, frequency, bssid, empresa_id_INT, pessoa_id_INT, data_SEC, data_OFFSEC, contador_verificacao_INT, corporacao_id_INT FROM wifi WHERE id = $id AND corporacao_id_INT = $idCorporacao";

            $msg = $this->database->queryMensagem($sql);
            $result = $this->database->result;

            if($msg != null && $msg->erro() )
            { 
            		$this->clear();
            		return $msg;
        	} 
        	else if($this->database->rows() == 0)
        	{
                return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nenhum registro foi encontrado."));
            }
        

            $row = $this->database->fetchArray(MYSQLI_NUM);
            
            		$this->id = $row[0];
		$this->networkId = $row[1];
		$this->rssi = $row[2];
		$this->ssid = $row[3];
		$this->usuario = $row[4];
		$this->senha = $row[5];
		$this->ip = $row[6];
		$this->linkSpeed = $row[7];
		$this->macAddress = $row[8];
		$this->frequency = $row[9];
		$this->bssid = $row[10];
		$this->empresaId = $row[11];
		$this->pessoaId = $row[12];
		$this->dataSec = $row[13];
		$this->dataOffsec = $row[14];
		$this->contadorVerificacaoInt = $row[15];

            
        }

public function delete($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (is_null($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }

            $sql = "DELETE FROM wifi WHERE id= $id AND corporacao_id_INT = $idCorporacao";
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                    && $sincronizar
                        && static::idSistemaTabela() != null
                            && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_REMOVE,
                                null,
                                null,
                                $id,
                                $this->database);
            }

            return $msg;
        }

public function insert($sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null, $opcoes = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            
            
            if (is_null($opcoes))
            {
                $opcoes = $this->opcoesDAO;
            }

            if (!is_numeric($this->id))
            {
                $this->id = "null";
                if ($opcoes == null || $opcoes[Database::OPCAO_GERAR_ID] != false) 
                {
                    $this->id = EXTDAO_Sistema_sequencia::gerarId(static::nomeTabela());                    
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }    
                    else if($msg == null)
                    {
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
                else 
                {
                    $this->id = null;
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao));
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }
                    else if($msg == null)
                    {
                        $this->id = $this->database->getLastInsertId();
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
            }
            else 
            {
                $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }                        
                else if($msg == null)
                {
                   $msg = new Mensagem_token();
                   $msg->mValor = $this->id;
                }
            }
            if($msg == null || $msg->ok())
            {                
                if($sincronizar
                    && static::idSistemaTabela() != null
                    && isset($this->id)){
                    
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_INSERT,
                                null,
                                null,
                                $this->id,
                                $this->database); 
                	}
            } 
            else
            {
                $this->id = null;
            }
            
            return $msg;
            
        }

public function getSQLInsert($idCorporacao){ 

return "INSERT INTO wifi (id, network_id, rssi, ssid, usuario, senha, ip, link_speed, mac_address, frequency, bssid, empresa_id_INT, pessoa_id_INT, data_SEC, data_OFFSEC, contador_verificacao_INT, corporacao_id_INT) VALUES ( $this->id ,  $this->networkId ,  $this->rssi ,  $this->ssid ,  $this->usuario ,  $this->senha ,  $this->ip ,  $this->linkSpeed ,  $this->macAddress ,  $this->frequency ,  $this->bssid ,  $this->empresaId ,  $this->pessoaId ,  $this->dataSec ,  $this->dataOffsec ,  $this->contadorVerificacaoInt ,  $idCorporacao ) "; 

}




        
        public function updateByParameters($id, $objParametros, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao))
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();

            
                
                if (isset($objParametros->networkId)) 
                {
                    $arrUpdateFields[] = " network_id = {$objParametros->networkId} ";
                }


                
                if (isset($objParametros->rssi)) 
                {
                    $arrUpdateFields[] = " rssi = {$objParametros->rssi} ";
                }


                
                if (isset($objParametros->ssid)) 
                {
                    $arrUpdateFields[] = " ssid = {$objParametros->ssid} ";
                }


                
                if (isset($objParametros->usuario)) 
                {
                    $arrUpdateFields[] = " usuario = {$objParametros->usuario} ";
                }


                
                if (isset($objParametros->ip)) 
                {
                    $arrUpdateFields[] = " ip = {$objParametros->ip} ";
                }


                
                if (isset($objParametros->linkSpeed)) 
                {
                    $arrUpdateFields[] = " link_speed = {$objParametros->linkSpeed} ";
                }


                
                if (isset($objParametros->macAddress)) 
                {
                    $arrUpdateFields[] = " mac_address = {$objParametros->macAddress} ";
                }


                
                if (isset($objParametros->frequency)) 
                {
                    $arrUpdateFields[] = " frequency = {$objParametros->frequency} ";
                }


                
                if (isset($objParametros->bssid)) 
                {
                    $arrUpdateFields[] = " bssid = {$objParametros->bssid} ";
                }


                
                if (isset($objParametros->empresaId)) 
                {
                    $arrUpdateFields[] = " empresa_id_INT = {$objParametros->empresaId} ";
                }


                
                if (isset($objParametros->pessoaId)) 
                {
                    $arrUpdateFields[] = " pessoa_id_INT = {$objParametros->pessoaId} ";
                }


                
                if (isset($objParametros->dataSec)) 
                {
                    $arrUpdateFields[] = " data_SEC = {$objParametros->dataSec} ";
                }


                
                if (isset($objParametros->dataOffsec)) 
                {
                    $arrUpdateFields[] = " data_OFFSEC = {$objParametros->dataOffsec} ";
                }


                
                if (isset($objParametros->contadorVerificacaoInt)) 
                {
                    $arrUpdateFields[] = " contador_verificacao_INT = {$objParametros->contadorVerificacaoInt} ";
                }



            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE wifi SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";

            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null)
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function update($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($this->networkId)) 
                {                                      
                    $arrUpdateFields[] = " network_id = {$this->networkId} ";
                }


                
                if (isset($this->rssi)) 
                {                                      
                    $arrUpdateFields[] = " rssi = {$this->rssi} ";
                }


                
                if (isset($this->ssid)) 
                {                                      
                    $arrUpdateFields[] = " ssid = {$this->ssid} ";
                }


                
                if (isset($this->usuario)) 
                {                                      
                    $arrUpdateFields[] = " usuario = {$this->usuario} ";
                }


                
                if (isset($this->ip)) 
                {                                      
                    $arrUpdateFields[] = " ip = {$this->ip} ";
                }


                
                if (isset($this->linkSpeed)) 
                {                                      
                    $arrUpdateFields[] = " link_speed = {$this->linkSpeed} ";
                }


                
                if (isset($this->macAddress)) 
                {                                      
                    $arrUpdateFields[] = " mac_address = {$this->macAddress} ";
                }


                
                if (isset($this->frequency)) 
                {                                      
                    $arrUpdateFields[] = " frequency = {$this->frequency} ";
                }


                
                if (isset($this->bssid)) 
                {                                      
                    $arrUpdateFields[] = " bssid = {$this->bssid} ";
                }


                
                if (isset($this->empresaId)) 
                {                                      
                    $arrUpdateFields[] = " empresa_id_INT = {$this->empresaId} ";
                }


                
                if (isset($this->pessoaId)) 
                {                                      
                    $arrUpdateFields[] = " pessoa_id_INT = {$this->pessoaId} ";
                }


                
                if (isset($this->dataSec)) 
                {                                      
                    $arrUpdateFields[] = " data_SEC = {$this->dataSec} ";
                }


                
                if (isset($this->dataOffsec)) 
                {                                      
                    $arrUpdateFields[] = " data_OFFSEC = {$this->dataOffsec} ";
                }


                
                if (isset($this->contadorVerificacaoInt)) 
                {                                      
                    $arrUpdateFields[] = " contador_verificacao_INT = {$this->contadorVerificacaoInt} ";
                }


            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE wifi SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function updateByInstanceUsingParameters($id, $parameters, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($parameters->networkId)) 
                {                                      
                    $arrUpdateFields[] = " network_id = {$this->networkId} ";
                }
                
                if (isset($parameters->rssi)) 
                {                                      
                    $arrUpdateFields[] = " rssi = {$this->rssi} ";
                }
                
                if (isset($parameters->ssid)) 
                {                                      
                    $arrUpdateFields[] = " ssid = {$this->ssid} ";
                }
                
                if (isset($parameters->usuario)) 
                {                                      
                    $arrUpdateFields[] = " usuario = {$this->usuario} ";
                }
                
                if (isset($parameters->ip)) 
                {                                      
                    $arrUpdateFields[] = " ip = {$this->ip} ";
                }
                
                if (isset($parameters->linkSpeed)) 
                {                                      
                    $arrUpdateFields[] = " link_speed = {$this->linkSpeed} ";
                }
                
                if (isset($parameters->macAddress)) 
                {                                      
                    $arrUpdateFields[] = " mac_address = {$this->macAddress} ";
                }
                
                if (isset($parameters->frequency)) 
                {                                      
                    $arrUpdateFields[] = " frequency = {$this->frequency} ";
                }
                
                if (isset($parameters->bssid)) 
                {                                      
                    $arrUpdateFields[] = " bssid = {$this->bssid} ";
                }
                
                if (isset($parameters->empresaId)) 
                {                                      
                    $arrUpdateFields[] = " empresa_id_INT = {$this->empresaId} ";
                }
                
                if (isset($parameters->pessoaId)) 
                {                                      
                    $arrUpdateFields[] = " pessoa_id_INT = {$this->pessoaId} ";
                }
                
                if (isset($parameters->dataSec)) 
                {                                      
                    $arrUpdateFields[] = " data_SEC = {$this->dataSec} ";
                }
                
                if (isset($parameters->dataOffsec)) 
                {                                      
                    $arrUpdateFields[] = " data_OFFSEC = {$this->dataOffsec} ";
                }
                
                if (isset($parameters->contadorVerificacaoInt)) 
                {                                      
                    $arrUpdateFields[] = " contador_verificacao_INT = {$this->contadorVerificacaoInt} ";
                }
            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE wifi SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }

}

?>
