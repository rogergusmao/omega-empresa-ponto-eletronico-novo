<!--Definição da View Angular JS -->

<div class='row' id='content-wrapper' ng-controller="clienteController">
    <div class='col-xs-12'>
        <div class='row'>
            <div class='col-sm-12'>
                <div class='page-header'>
                    <h1 class='pull-left'>
                        <i class='icon-edit'></i>
                        <span>Formulário modelo</span>
                    </h1>

                    <div class='pull-right'>
                        <ul class='breadcrumb'>
                            <li>
                                <a href='index.html'>
                                    <i class='icon-bar-chart'></i>
                                </a>
                            </li>
                            <li class='separator'>
                                <i class='icon-angle-right'></i>
                            </li>
                            <li>
                                Cadastro de cliente
                            </li>
                            <li class='separator'>
                                <i class='icon-angle-right'></i>
                            </li>
                            <li class='active'>Formulário Modelo</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class='row'>
            <div class='col-sm-12'>
                <div class='box'>
                    <div class='box-header blue-background'>
                        <div class='title'>
                            <div class='icon-edit'></div>
                            Cadastro de Entidade
                        </div>
                        <div class='actions'>
                            <a class="btn box-remove btn-xs btn-link" href="#"><i class='icon-remove'></i>
                            </a>

                            <a class="btn box-collapse btn-xs btn-link" href="#"><i></i>
                            </a>
                        </div>
                    </div>
                    <div class='box-content'>

                        <form name="mainForm" class="form form-horizontal" style="margin-bottom: 0;">

                            <div class='form-group' ng-class="getFormControlCssClass(mainForm.fieldNome)">
                                <label class='col-md-2 control-label' for='inputText1'>Campo de texto</label>

                                <div class='col-md-5'>

                                    <input type='text'
                                           class='form-control'
                                           id="fieldNome"
                                           name="fieldNome"
                                           minlength="10"
                                           autocomplete="off"
                                           ui-validate="{ validacaoEduardo: 'validaEduardo($value)'}"
                                           ng-model="formData.nome"
                                           ng-disabled="disabledFields.fieldNome"
                                           ng-required="requiredFields.fieldNome"
                                           placeholder='Campo de Texto'>

                                    <span class='help-block'
                                          ng-show="showValidationMessage(mainForm.fieldNome, { minlength: 'O tamanho mínimo é de 10 caracteres', validacaoEduardo: 'Erro, o valor deve ser igual a Eduardo Alves', required: 'Campo Obrigatório'})">
                                            <p ng-repeat="messages in getValidationMessages(mainForm.fieldNome)">
                                                {{messages}}</p>
                                        </span>

                                </div>
                            </div>

                            <div class='form-group' ng-class="getFormControlCssClass(mainForm.fieldSenha)">
                                <label class='col-md-2 control-label' for='fieldSenha'>Campo de senha</label>

                                <div class='col-md-5'>

                                    <input type='password'
                                           class='form-control'
                                           autocomplete="off"
                                           name="fieldSenha"
                                           minLength="8"
                                           maxlength="20"
                                           ng-model="formData.senha"
                                           ng-disabled="disabledFields.fieldSenha"
                                           ng-required="requiredFields.fieldSenha"
                                           placeholder='Campo de Senha'
                                           value=''>

                                    <span class='help-block'
                                          ng-show="showValidationMessage(mainForm.fieldSenha, { minlength: 'O tamanho mínimo é de 8 caracteres', maxlength: 'O tamanho máximo é de 20 caracteres', required: 'Campo Obrigatório'})">
                                            <p ng-repeat="messages in getValidationMessages(mainForm.fieldSenha)">
                                                {{messages}}</p>
                                        </span>

                                </div>
                            </div>

                            <div class='form-group' ng-class="getFormControlCssClass(mainForm.fieldComentarios)">
                                <label class='col-md-2 control-label' for='fieldComentarios'>Textarea</label>

                                <div class='col-md-5'>

                                        <textarea
                                                class='form-control'
                                                name="fieldComentarios"
                                                ng-disabled="disabledFields.fieldComentarios"
                                                ng-required="requiredFields.fieldComentarios"
                                                ng-model="formData.comentarios"
                                                placeholder='Escreve um comentário'
                                                ui-validate="{ validacaoLinhas: 'validateMinNumberOfLines(\'10\', formData.comentarios)'}"
                                                rows='3'></textarea>

                                    <span class='help-block'
                                          ng-show="showValidationMessage(mainForm.fieldComentarios, { minlength: 'O tamanho mínimo é de 8 caracteres', validacaoLinhas: 'O texto sede ter no mínimo 10 quebras de linha.', required: 'Campo Obrigatório'})">
                                            <p ng-repeat="messages in getValidationMessages(mainForm.fieldComentarios)">
                                                {{messages}}</p>
                                        </span>

                                </div>
                            </div>

                            <div class='form-group' ng-class="getFormControlCssClass(mainForm.fieldPassaporte)">
                                <label class='col-md-2 control-label' for='fieldPassaporte'>Campo desativado</label>

                                <div class='col-md-5'>

                                    <input class='form-control'
                                           ng-disabled="disabledFields.fieldPassaporte"
                                           ng-required="requiredFields.fieldPassaporte"
                                           placeholder='Campo desativado.'
                                           name="fieldPassaporte"
                                           ng-model="formData.passaporte"
                                           type='text'>

                                </div>
                            </div>

                            <hr class='hr-normal'>

                            <div class='form-group'>
                                <label class='col-md-2 control-label'>Checkboxes</label>

                                <div class='col-md-10'>
                                    <div class='checkbox'>
                                        <label>
                                            <input type='checkbox' name="fieldPreferencias" value=''>
                                            sit totam labore sunt porro possimus cum officia earum deserunt
                                        </label>
                                    </div>
                                    <div class='checkbox'>
                                        <label>
                                            <input type='checkbox' value=''>
                                            sit culpa sequi rem dolorem
                                        </label>
                                    </div>
                                    <div class='checkbox'>
                                        <label>
                                            <input type='checkbox' value=''>
                                            autem mollitia nulla aliquam
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <hr class='hr-normal'>

                            <div class='form-group' ng-class="getFormControlCssClass(mainForm.fieldPessoa)">

                                <label class='col-md-2 control-label' for='fieldPessoa'>Pessoa</label>

                                <div class='col-md-5'>
                                    <ui-select
                                            ng-model="formData.pessoa"
                                            name="fieldPessoa"
                                            theme="bootstrap"
                                            ng-disabled="disabledFields.fieldPessoa"
                                            ng-required="requiredFields.fieldPessoa"
                                            ng-model="formData.pessoa"
                                            style="min-width: 300px;"
                                            title="Pessoa">
                                        <ui-select-match placeholder="Selecione uma pessoa da lista...">
                                            {{$select.selected.nome}}
                                        </ui-select-match>
                                        <ui-select-choices
                                                repeat="pessoa in fieldsParameters.fieldPessoa.dadosRemotos | propsFilter: {nome: $select.search, idade: $select.search}"
                                                refresh="getListaPesssoa()"
                                                refresh-delay="0">

                                            <div ng-bind-html="pessoa.nome | highlight: $select.search"></div>
                                            <small>
                                                E-mail: {{pessoa.email}}<br/>
                                                Idade: <span
                                                        ng-bind-html="''+pessoa.idade | highlight: $select.search"></span>
                                            </small>

                                        </ui-select-choices>
                                    </ui-select>

                                    <span class="help-block"
                                          ng-show="showValidationMessage(mainForm.fieldPessoa, { required: 'Campo Obrigatório'})">
                                            <p ng-repeat="messages in getValidationMessages(mainForm.fieldPessoa)">
                                                {{messages}}</p>
                                        </span>

                                </div>
                            </div>

                            <hr class='hr-normal'>

                            <div class='form-group' ng-class="getFormControlCssClass(mainForm.fieldPaises)">
                                <label class='col-md-2 control-label' for='inputSelectMulti'>Select Múltiplo</label>

                                <div class='col-md-5'>

                                    <ui-select
                                            name="fieldPaises"
                                            multiple
                                            sortable="true"
                                            theme="bootstrap"
                                            ng-model="formData.fieldPaises"
                                            ng-disabled="disabledFields.fieldPaises"
                                            ng-required="requiredFields.fieldPaises"
                                            close-on-select="false"
                                            ui-validate="{validacaoNumeroMinimo: 'validaNumeroMinimoSelectMultiplo(2, $value)'}"
                                            style="min-width: 300px;">
                                        <ui-select-match placeholder="Selecione um país...">{{$item.nome}} &lt;{{$item.codigo}}&gt;</ui-select-match>
                                        <ui-select-choices
                                                repeat="pais.nome as pais in fieldsParameters.fieldPaises.dadosRemotos | propsFilter: {nome: $select.search, codigo: $select.search}"
                                                refresh="getListaPaises()"
                                                refresh-delay="0">
                                            <div ng-bind-html="pais.nome | highlight: $select.search"></div>
                                            <small>
                                                Código: <span
                                                        ng-bind-html="''+pais.codigo | highlight: $select.search"></span>
                                            </small>
                                        </ui-select-choices>
                                    </ui-select>

                                    <span class='help-block'
                                          ng-show="showValidationMessage(mainForm.fieldPaises, { validacaoNumeroMinimo: 'Selecione pelo menos 2 países.', required: 'Campo Obrigatório'})">
                                            <p ng-repeat="messages in getValidationMessages(mainForm.fieldPaises)">
                                                {{messages}}</p>
                                        </span>

                                </div>
                            </div>

                            <!--<pre>Componente: {{mainForm.fieldPaises.$error | json}}</pre>-->

                            <hr class='hr-normal'>

                            <div class='form-group' ng-class="getFormControlCssClass(mainForm.fieldDataNascimento)">
                                <label class='col-md-2 control-label' for='fieldPessoa'>Data de Nascimento</label>

                                <div class='col-md-5'>

                                    <p class="input-group">

                                        <input type="text"
                                               class="form-control"
                                               ui-date-mask
                                               name="fieldDataNascimento"
                                               uib-datepicker-popup="{{formatoDeDataPadrao}}"
                                               ng-model="formData.dataNascimento"
                                               is-open="fieldsParameters.fieldDataNascimento.popupAberto"
                                               datepicker-options="fieldsParameters.fieldDataNascimento.datePickerParams"
                                               ng-required="requiredFields.fieldDataNascimento"
                                               ng-disabled="disabledFields.fieldDataNascimento"
                                               current-text="Hoje"
                                               close-text="Fechar"
                                               clear-text="Limpar"
                                        />

                                        <span class="input-group-btn">
                                                <button type="button" class="btn btn-default"
                                                        ng-click="fieldsParameters.fieldDataNascimento.popupAberto = true">
                                                    <i class="glyphicon glyphicon-calendar"></i></button>
                                            </span>

                                    </p>

                                    <span class='help-block'
                                          ng-show="showValidationMessage(mainForm.fieldDataNascimento, { required: 'Campo Obrigatório'})">
                                            <p ng-repeat="messages in getValidationMessages(mainForm.fieldDataNascimento)">
                                                {{messages}}</p>
                                        </span>

                                </div>
                            </div>

                            <hr class='hr-normal'>

                            <div class='form-group' ng-class="getFormControlCssClass(mainForm.fieldPeriodoDe)">
                                <label class='col-md-2 control-label' for='fieldPessoa'>Período - De</label>

                                <div class='col-md-5'>

                                    <p class="input-group">

                                        <input type="text"
                                               class="form-control"
                                               name="fieldPeriodoAte"
                                               uib-datepicker-popup="{{formatoDeDataPadrao}}"
                                               ng-model="formData.periodoDe"
                                               is-open="fieldsParameters.fieldPeriodoDe.popupAberto"
                                               datepicker-options="fieldsParameters.fieldPeriodoDe.datePickerParams"
                                               ng-required="requiredFields.fieldPeriodoDe"
                                               ng-disabled="disabledFields.fieldPeriodoDe"
                                               ng-blur="setDateRangeMinDate(formData.periodoDe, fieldsParameters.fieldPeriodoAte)"
                                               current-text="Hoje"
                                               close-text="Fechar"
                                               clear-text="Limpar"
                                        />

                                        <span class="input-group-btn">
                                                <button type="button" class="btn btn-default"
                                                        ng-click="fieldsParameters.fieldPeriodoDe.popupAberto = true"><i
                                                            class="glyphicon glyphicon-calendar"></i></button>
                                            </span>

                                    </p>

                                    <span class='help-block'
                                          ng-show="showValidationMessage(mainForm.fieldPeriodoDe, { required: 'Campo Obrigatório'})">
                                            <p ng-repeat="messages in getValidationMessages(mainForm.fieldPeriodoDe)">
                                                {{messages}}</p>
                                        </span>

                                </div>
                            </div>

                            <div class='form-group' ng-class="getFormControlCssClass(mainForm.fieldPeriodoAte)">
                                <label class='col-md-2 control-label' for='fieldPessoa'>Período - Até</label>

                                <div class='col-md-5'>

                                    <p class="input-group">

                                        <input type="text"
                                               class="form-control"
                                               name="fieldPeriodoDe"
                                               uib-datepicker-popup="{{formatoDeDataPadrao}}"
                                               ng-model="formData.periodoAte"
                                               is-open="fieldsParameters.fieldPeriodoAte.popupAberto"
                                               datepicker-options="fieldsParameters.fieldPeriodoAte.datePickerParams"
                                               ng-required="requiredFields.fieldPeriodoAte"
                                               ng-disabled="disabledFields.fieldPeriodoAte"
                                               ng-blur="setDateRangeMaxDate(formData.periodoAte, fieldsParameters.fieldPeriodoDe)"
                                               current-text="Hoje"
                                               close-text="Fechar"
                                               clear-text="Limpar"
                                        />

                                        <span class="input-group-btn">
                                                <button type="button" class="btn btn-default"
                                                        ng-click="fieldsParameters.fieldPeriodoAte.popupAberto = true">
                                                    <i class="glyphicon glyphicon-calendar"></i></button>
                                            </span>

                                    </p>

                                    <span class='help-block'
                                          ng-show="showValidationMessage(mainForm.fieldPeriodoDe, { required: 'Campo Obrigatório'})">
                                            <p ng-repeat="messages in getValidationMessages(mainForm.fieldPeriodoDe)">
                                                {{messages}}</p>
                                        </span>

                                </div>
                            </div>

                            <hr class='hr-normal'>

                            <div class='form-group' ng-class="getFormControlCssClass(mainForm.campoHora)">
                                <label class='col-md-2 control-label' for='campoHora'>Hora</label>

                                <div class='col-md-5'>

                                    <uib-timepicker
                                            name="campoHora"
                                            ng-model="formData.hora"
                                            ng-required="requiredFields.campoHora"
                                            ng-disabled="disabledFields.campoHora"
                                            hour-step="fieldsParameters.campoHora.parametrosTimePicker.stepHora"
                                            minute-step="fieldsParameters.campoHora.parametrosTimePicker.stepMinuto"
                                            show-meridian="fieldsParameters.campoHora.parametrosTimePicker.mostrarAmPm"></uib-timepicker>


                                    <span class='help-block'
                                          ng-show="showValidationMessage(mainForm.campoHora, { required: 'Campo Obrigatório'})">
                                            <p ng-repeat="messages in getValidationMessages(mainForm.campoHora)">
                                                {{messages}}</p>
                                        </span>

                                </div>
                            </div>


                            <hr class='hr-normal'>

                            <div class='form-group' ng-class="getFormControlCssClass(mainForm.campoPreco)">
                                <label class='col-md-2 control-label' for='fieldPassaporte'>Preço</label>

                                <div class='col-md-5'>

                                    <input class='form-control'
                                           ng-disabled="disabledFields.campoPreco"
                                           ng-required="requiredFields.campoPreco"
                                           placeholder=''
                                           ui-money-mask="2"
                                           name="campoPreco"
                                           ng-model="formData.preco"
                                           type='text'>

                                </div>
                            </div>

                            <span class='help-block'
                                  ng-show="showValidationMessage(mainForm.campoPreco, { required: 'Campo Obrigatório'})">
                                    <p ng-repeat="messages in getValidationMessages(mainForm.campoPreco)">
                                        {{messages}}</p>
                                </span>

                            <hr class='hr-normal'>

                            <div class='form-group' ng-class="getFormControlCssClass(mainForm.campoCep)">
                                <label class='col-md-2 control-label' for='fieldPassaporte'>CEP</label>

                                <div class='col-md-5'>

                                    <input class='form-control'
                                           ng-disabled="disabledFields.campoCep"
                                           ng-required="requiredFields.campoCep"
                                           placeholder=''
                                           ui-br-cep-mask
                                           name="campoCep"
                                           ng-model="formData.cep"
                                           type='text'>

                                </div>
                            </div>

                            <span class='help-block'
                                  ng-show="showValidationMessage(mainForm.campoCep, { required: 'Campo Obrigatório'})">
                                    <p ng-repeat="messages in getValidationMessages(mainForm.campoCep)">{{messages}}</p>
                                </span>

                            <hr class='hr-normal'>

                            <div class='form-group' ng-class="getFormControlCssClass(mainForm.campoCpf)">
                                <label class='col-md-2 control-label' for='fieldPassaporte'>CPF</label>

                                <div class='col-md-5'>

                                    <input class='form-control'
                                           ng-disabled="disabledFields.campoCpf"
                                           ng-required="requiredFields.campoCpf"
                                           placeholder=''
                                           ui-br-cpf-mask
                                           name="campoCpf"
                                           ng-model="formData.cpf"
                                           type='text'>

                                </div>
                            </div>

                            <span class='help-block'
                                  ng-show="showValidationMessage(mainForm.campoCpf, { required: 'Campo Obrigatório'})">
                                    <p ng-repeat="messages in getValidationMessages(mainForm.campoCpf)">{{messages}}</p>
                                </span>

                            <hr class='hr-normal'>

                            <div class='form-group' ng-class="getFormControlCssClass(mainForm.campoCnpj)">
                                <label class='col-md-2 control-label' for='fieldPassaporte'>CNPJ</label>

                                <div class='col-md-5'>

                                    <input class='form-control'
                                           ng-disabled="disabledFields.campoCnpj"
                                           ng-required="requiredFields.campoCnpj"
                                           placeholder=''
                                           ui-br-cnpj-mask
                                           name="campoCnpj"
                                           ng-model="formData.cnpj"
                                           type='text'>

                                </div>
                            </div>

                            <span class='help-block'
                                  ng-show="showValidationMessage(mainForm.campoCnpj, { required: 'Campo Obrigatório'})">
                                    <p ng-repeat="messages in getValidationMessages(mainForm.campoCnpj)">
                                        {{messages}}</p>
                                </span>

                            <hr class='hr-normal'>

                            <div class='form-group' ng-class="getFormControlCssClass(mainForm.campoTelefone)">
                                <label class='col-md-2 control-label' for='fieldPassaporte'>Telefone</label>

                                <div class='col-md-5'>

                                    <input class='form-control'
                                           ng-disabled="disabledFields.campoTelefone"
                                           ng-required="requiredFields.campoTelefone"
                                           placeholder=''
                                           ui-br-phone-number
                                           name="campoTelefone"
                                           ng-model="formData.telefone"
                                           type='text'>

                                </div>
                            </div>

                            <span class='help-block'
                                  ng-show="showValidationMessage(mainForm.campoTelefone, { required: 'Campo Obrigatório'})">
                                    <p ng-repeat="messages in getValidationMessages(mainForm.campoTelefone)">
                                        {{messages}}</p>
                                </span>

                            <hr class='hr-normal'>

                            <div class='form-group' ng-class="getFormControlCssClass(mainForm.campoPorcentagem)">
                                <label class='col-md-2 control-label' for='fieldPassaporte'>Procentagem</label>

                                <div class='col-md-5'>

                                    <input class='form-control'
                                           ng-disabled="disabledFields.campoPorcentagem"
                                           ng-required="requiredFields.campoPorcentagem"
                                           placeholder=''
                                           ui-percentage-mask
                                           name="campoPorcentagem"
                                           ng-model="formData.porcentagem"
                                           type='text'>

                                </div>
                            </div>

                            <span class='help-block'
                                  ng-show="showValidationMessage(mainForm.campoPorcentagem, { required: 'Campo Obrigatório'})">
                                    <p ng-repeat="messages in getValidationMessages(mainForm.campoPorcentagem)">
                                        {{messages}}</p>
                                </span>

                            <hr class='hr-normal'>

                            <div class='form-actions form-actions-padding-sm'>
                                <div class='row'>
                                    <div class='col-md-10 col-md-offset-2'>
                                        <button class='btn btn-primary' type='submit' ng-click="salvarAlteracoes()">
                                            <i class='icon-save'></i>
                                            Gravar Dados
                                        </button>
                                        <button class='btn' type='submit'>Cancelar</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--Definição do Controller Angular JS -->
<script language="javascript">

    <?php=Helper::incluirConteudoDataServices(); ?>

    omegaApp.controller('pageController', ['$scope', '$rootScope', '$window', '$interval', '$timeout', 'dataService',
                                           function ($scope, $rootScope, $window, $interval, $timeout, dataService)
                                           {

                                               <?=Helper::gerarObjetoJSDeVariaveisGET(); ?>
                                               <?=Helper::incluirConteudoControllerAngularJS(); ?>

                                           }]
    );

</script>

