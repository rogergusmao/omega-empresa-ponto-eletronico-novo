<?php


        /*

        Arquivo gerado através de gerador de código em 12/05/2018 as 14:38:01.
        Para que o arquivo não seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

        Tabela correspondente: registro_estado
        Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

        */
        


class DAO_Registro_estado extends Generic_DAO
                        {
                            const NOME_ENTIDADE = "registro_estado";

                            public static $nomeTabela = null;
                            public static $idSistemaTabela = false;
                            public static $databaseFieldsRelatedAttributes = null;
	                        public static $databaseFieldTypes = null;
	                        public static $databaseFieldNames = null;
	                        public static $listAliasTypes = null;
	                        public static $listAliasRelatedAttributes = null;
	                        
	                        public $id;
public $nome;
public $prazoDiasInt;
public $tipoRegistroId;

public $labelId;
public $labelNome;
public $labelPrazoDiasInt;
public $labelTipoRegistroId;

public static $arrPK = array("id");

public function __construct($configDAO = null)
        {
            parent::__construct($configDAO);
            static::__setStaticValues($this->database);           
        }

public static function idSistemaTabela($databaseObject = null){
            if(!(static::$idSistemaTabela))
            {
                static::$idSistemaTabela = EXTDAO_Sistema_tabela::getIdDaSistemaTabela(static::nomeTabela(), $databaseObject);
            }
            return static::$idSistemaTabela;
        }

public function getIdSistemaTabela(){
            return static::idSistemaTabela();
        }

public static function nomeTabela(){
            if(is_null(static::$nomeTabela))
            {
                static::$nomeTabela = "registro_estado";
            }
            return static::$nomeTabela;
        }

public function getNomeTabela(){
            return static::nomeTabela();
        }

public static function __setStaticValues($databaseObject = null)
                   {    
                        static::nomeTabela();
                        
                        static::idSistemaTabela($databaseObject);
                   
                        static::setDatabaseFieldNames();
                        static::setDatabaseFieldTypes();
                        static::setDatabaseFieldsRelatedAttributeNames();
                        
                        static::setListAliasTypes();
                        static::setListAliasRelatedAttributes();
                   }

public function getExpressaoLabel()
                   {
                        return "{nome}";
                   }

public static function setDatabaseFieldNames()
        {
            if(is_null(static::$databaseFieldNames))
            {
                static::$databaseFieldNames = new stdClass();
                static::$databaseFieldNames->id = "id";
static::$databaseFieldNames->nome = "nome";
static::$databaseFieldNames->prazoDiasInt = "prazo_dias_INT";
static::$databaseFieldNames->tipoRegistroId = "tipo_registro_id_INT";

            }
                      
        }

public static function setDatabaseFieldTypes()
        {
            if(is_null(static::$databaseFieldTypes))
            {
                static::$databaseFieldTypes = new stdClass();
                static::$databaseFieldTypes->id = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->nome = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->prazoDiasInt = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->tipoRegistroId = static::TIPO_VARIAVEL_INTEGER;

            }
            
        }

public static function setDatabaseFieldsRelatedAttributeNames()
        {
            if(is_null(static::$databaseFieldsRelatedAttributes))
            {
                static::$databaseFieldsRelatedAttributes = new stdClass();
                static::$databaseFieldsRelatedAttributes->id = "id";
static::$databaseFieldsRelatedAttributes->nome = "nome";
static::$databaseFieldsRelatedAttributes->prazo_dias_INT = "prazoDiasInt";
static::$databaseFieldsRelatedAttributes->tipo_registro_id_INT = "tipoRegistroId";

            }           
        }

public function setAllFieldsToHumanFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["nome"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["prazo_dias_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["tipo_registro_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
}

public function setAllFieldsToDatabaseFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["nome"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["prazo_dias_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["tipo_registro_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
}

  public function getFkObjTipo_registro() 
                {
                    if($this->objTipo_registro == null)
                    {                        
                        $this->objTipo_registro = new EXTDAO_Tipo_registro_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getTipo_registro_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objTipo_registro->clear();
                    }
                    elseif($this->objTipo_registro->getId() != $idFK)
                    {
                        $this->objTipo_registro->select($idFK);
                    }
                    return $this->objTipo_registro;
                }


public function setLabels()
        {$this->labelId = "";
$this->labelNome = "";
$this->labelPrazoDiasInt = "";
$this->labelTipoRegistroId = "";
}

public function __actionAdd($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                $this->setByObject($parameters);
                $this->setDataCadastroAndOffsetInSecondsFromUser($parameters);
                
                $this->formatarParaSQL();
                $msg = $this->insert(true);
                
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                return new Mensagem(null, I18N::getExpression("Registro estado adicionado com sucesso."));
                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionEdit($parameters = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try {

                $this->setByObject($parameters);
                $this->setAllFieldsToHumanFormat();
                
                $this->formatarParaSQL();
                $msg = $this->updateByInstanceUsingParameters($this->id, $parameters, true);
                if($msg != null && $msg->erro())
                {
                     return $msg;
                }
        
                return new Mensagem(null, I18N::getExpression("Registro estado editado com sucesso."));
            }
            catch(Exception $ex)
            {                
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionRemove($parameters = null)
        {
            $successulRemovalCount = 0;
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                foreach($parameters as $record)
                {
                    if(is_numeric($record->id))
                    {
                        $msg = $this->delete($record->id, true);
                        if($msg == null || $msg->ok())
                        {
                            $successulRemovalCount++;
                        }
                    }
                }
                
                if($successulRemovalCount > 0)
                {
                    $successMessage = count($successulRemovalCount) > 1 ? I18N::getExpression("{0} Registro estado foram removidos com sucesso.", $successulRemovalCount) : I18N::getExpression("Registro estado removido com sucesso.");
                    return new Mensagem(null, $successMessage);
                }
                else
                {
                    $errorMessage = count($parameters) > 1 ? I18N::getExpression("Falha ao remover Registro estado.") : I18N::getExpression("Falha ao remover Registro estado.");
                    return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, $errorMessage);                    
                }
            }
            catch (Exception $ex) 
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }

public function __getRecord($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }
            
            try
            {            
                $returnObject = new stdClass();            
                $whereClause = static::getWhereClauseForFilter($parameters->filterParameters);
                $query = "SELECT  id, nome, prazo_dias_INT, tipo_registro_id_INT FROM registro_estado {$whereClause}";
    
                $msg = $this->database->queryMensagem($query);                
                if ($msg == null)
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Não foi encontrado nenhum registro"));
                    }                    
                    $resultSet = $this->database->result;
                    $resultObject = static::mapResultSetDataToRecord($resultSet);
                    
                    $returnObject->formData = $resultObject;
                    $returnObject->comboBoxesData = null;
                    if($parameters->loadComboBoxesData)
                    {
                        $this->setByObject($resultObject);
                        $returnObject->comboBoxesData =  $this->getFormComboBoxesData();
                    }                    
    
                    
    
                    return new Mensagem_generica($returnObject);
                } 
                else 
                {
                    return $msg;
                }
                
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }


        
        public function __getFilterComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
                
        public function __getFormComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
        
        protected function getFormComboBoxesData()
        {
            $comboBoxesData = new stdClass();
            $listParameters = new stdClass();
            $listParameters->listMappingType = static::LIST_MAPPING_TYPE_COMBOBOX;
            
            
                    
                    $objTipoRegistro = new EXTDAO_Tipo_registro();
                    $comboBoxesData->fieldTipoRegistroId = $objTipoRegistro->__getList($listParameters);

            return $comboBoxesData;
        }
        
        


        
        public static function setListAliasTypes()
        {
            if(is_null(static::$listAliasTypes))
            {
                static::$listAliasTypes = new stdClass();
                static::$listAliasTypes->tipo_registro__nome = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->registro_estado__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->registro_estado__nome = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->registro_estado__prazo_dias_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->registro_estado__nome = static::TIPO_VARIAVEL_TEXT;
            }        
        }
        
        public static function setListAliasRelatedAttributes()
        {
            if(is_null(static::$listAliasRelatedAttributes))
            {
                static::$listAliasRelatedAttributes = new stdClass();
                static::$listAliasRelatedAttributes->tipo_registro__nome = "tipoRegistroNome";
static::$listAliasRelatedAttributes->registro_estado__id = "id";
static::$listAliasRelatedAttributes->registro_estado__nome = "nome";
static::$listAliasRelatedAttributes->registro_estado__prazo_dias_INT = "prazoDiasInt";
static::$listAliasRelatedAttributes->registro_estado__nome = "nome";
            }         
        }

        public function __getList($parameters = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try
            {
                $filterParameters = null;
                if(isset($parameters->filterParameters))
                {
                    $filterParameters = $parameters->filterParameters;
                }
                                    
                    
                
                $sortingParameters = null;
                if(isset($parameters->sortingParameters))
                {
                    $sortingParameters = $parameters->sortingParameters;
                }
                
                $paginationParameters = null;
                if(isset($parameters->paginationParameters))
                {
                    $paginationParameters = $parameters->paginationParameters;
                }
                
                $listMappingType = static::getListMappingType($parameters->listMappingType);
                $mainTableAlias = "re";

                $whereClause = static::getWhereClauseForFilter($filterParameters, $mainTableAlias);
                $orderByClause = static::getOrderByClauseForFilter($sortingParameters, $mainTableAlias);
                $limitClause = static::getLimitClauseForFilter($paginationParameters);

                $queryWithoutLimit = "SELECT re.id FROM registro_estado re {$whereClause}";
                $query = "SELECT tr.nome AS tipo_registro__nome, re.id AS registro_estado__id, re.nome AS registro_estado__nome, re.prazo_dias_INT AS registro_estado__prazo_dias_INT, re.nome AS registro_estado__nome FROM registro_estado re LEFT JOIN tipo_registro tr ON tr.id = re.tipo_registro_id_INT {$whereClause} {$orderByClause} {$limitClause}";

                $msg = $this->database->queryMensagem($queryWithoutLimit);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                
                $totalNumberOfRecords = $this->database->rows();
                if($totalNumberOfRecords == 0)
                {
                    return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                }                        
                        
                $msg = $this->database->queryMensagem($query);
                
                //operacao realizaca com sucesso
                if ($msg == null) 
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                    }                        
                
                    $resultSet = $this->database->result;

                    $objReturn = new stdClass();
                    $objReturn->dataSet = static::mapResultSetDataToList($resultSet, $listMappingType, static::$listAliasTypes);

                    if(!Helper::isNullOrEmpty($paginationParameters->recordsPerPage) && !Helper::isNullOrEmpty($paginationParameters->currentPage))
                    {
                        $lastPage = ceil($totalNumberOfRecords / $paginationParameters->recordsPerPage);
                        $objReturn->paginationParameters = new PaginationParams($paginationParameters->recordsPerPage, $paginationParameters->currentPage, $lastPage);
                    }
                    
                    return new Mensagem_generica($objReturn);
                } 
                else
                {
                    return $msg;
                }                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }



public function getId()
            {
                return $this->id;
            }

public function getNome()
            {
                return $this->nome;
            }

public function getPrazoDiasInt()
            {
                return $this->prazoDiasInt;
            }

public function getPrazo_dias_INT()
                {
                    return $this->prazoDiasInt;
                }

public function getTipoRegistroId()
            {
                return $this->tipoRegistroId;
            }

public function getTipo_registro_id_INT()
                {
                    return $this->tipoRegistroId;
                }



function setId($value)
            {
                $this->id = $value;
            }

function setNome($value)
            {
                $this->nome = $value;
            }

function setPrazoDiasInt($value)
            {
                $this->prazoDiasInt = $value;
            }

public function setPrazo_dias_INT($value)
                { 
                    $this->prazoDiasInt = $value; 
                }

function setTipoRegistroId($value)
            {
                $this->tipoRegistroId = $value;
            }

public function setTipo_registro_id_INT($value)
                { 
                    $this->tipoRegistroId = $value; 
                }



public function clear()
        {$this->id = null;
$this->nome = null;
$this->prazoDiasInt = null;
$this->tipoRegistroId = null;
if($this->objTipo_registro != null) unset($this->objTipo_registro);
if(is_array($this->arrEstadosCampos)){
                        unset($this->arrEstadosCampos);
                     $this->arrEstadosCampos = array();
                    }
                }

public function formatarParaSQL()
        {
if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->id)){
$this->id = $this->formatarIntegerParaComandoSQL($this->id);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->id);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->nome)){
$this->nome = $this->formatarStringParaComandoSQL($this->nome);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->nome);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->prazoDiasInt)){
$this->prazoDiasInt = $this->formatarIntegerParaComandoSQL($this->prazoDiasInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->prazoDiasInt);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->tipoRegistroId)){
$this->tipoRegistroId = $this->formatarIntegerParaComandoSQL($this->tipoRegistroId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->tipoRegistroId);
}

}

public function formatarParaExibicao()
        {if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->nome)){
$this->nome = $this->formatarStringParaExibicao($this->nome);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->nome);
}

}

function select($id, $idCorporacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }   

            $sql = "SELECT id, nome, prazo_dias_INT, tipo_registro_id_INT FROM registro_estado WHERE id = $id";

            $msg = $this->database->queryMensagem($sql);
            $result = $this->database->result;

            if($msg != null && $msg->erro() )
            { 
            		$this->clear();
            		return $msg;
        	} 
        	else if($this->database->rows() == 0)
        	{
                return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nenhum registro foi encontrado."));
            }
        

            $row = $this->database->fetchArray(MYSQLI_NUM);
            
            		$this->id = $row[0];
		$this->nome = $row[1];
		$this->prazoDiasInt = $row[2];
		$this->tipoRegistroId = $row[3];

            
        }

public function delete($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (is_null($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }

            $sql = "DELETE FROM registro_estado WHERE id= $id";
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                    && $sincronizar
                        && static::idSistemaTabela() != null
                            && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_REMOVE,
                                null,
                                null,
                                $id,
                                $this->database);
            }

            return $msg;
        }

public function insert($sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null, $opcoes = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            
            
            if (is_null($opcoes))
            {
                $opcoes = $this->opcoesDAO;
            }

            if (!is_numeric($this->id))
            {
                $this->id = "null";
                if ($opcoes == null || $opcoes[Database::OPCAO_GERAR_ID] != false) 
                {
                    $this->id = EXTDAO_Sistema_sequencia::gerarId(static::nomeTabela());                    
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }    
                    else if($msg == null)
                    {
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
                else 
                {
                    $this->id = null;
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao));
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }
                    else if($msg == null)
                    {
                        $this->id = $this->database->getLastInsertId();
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
            }
            else 
            {
                $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }                        
                else if($msg == null)
                {
                   $msg = new Mensagem_token();
                   $msg->mValor = $this->id;
                }
            }
            if($msg == null || $msg->ok())
            {                
                if($sincronizar
                    && static::idSistemaTabela() != null
                    && isset($this->id)){
                    
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_INSERT,
                                null,
                                null,
                                $this->id,
                                $this->database); 
                	}
            } 
            else
            {
                $this->id = null;
            }
            
            return $msg;
            
        }

public function getSQLInsert($idCorporacao){ 

return "INSERT INTO registro_estado (id, nome, prazo_dias_INT, tipo_registro_id_INT) VALUES ( $this->id ,  $this->nome ,  $this->prazoDiasInt ,  $this->tipoRegistroId ) "; 

}




        
        public function updateByParameters($id, $objParametros, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao))
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();

            
                
                if (isset($objParametros->nome)) 
                {
                    $arrUpdateFields[] = " nome = {$objParametros->nome} ";
                }


                
                if (isset($objParametros->prazoDiasInt)) 
                {
                    $arrUpdateFields[] = " prazo_dias_INT = {$objParametros->prazoDiasInt} ";
                }


                
                if (isset($objParametros->tipoRegistroId)) 
                {
                    $arrUpdateFields[] = " tipo_registro_id_INT = {$objParametros->tipoRegistroId} ";
                }



            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE registro_estado SET {$strUpdateFields} WHERE id = {$id}";

            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null)
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function update($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($this->nome)) 
                {                                      
                    $arrUpdateFields[] = " nome = {$this->nome} ";
                }


                
                if (isset($this->prazoDiasInt)) 
                {                                      
                    $arrUpdateFields[] = " prazo_dias_INT = {$this->prazoDiasInt} ";
                }


                
                if (isset($this->tipoRegistroId)) 
                {                                      
                    $arrUpdateFields[] = " tipo_registro_id_INT = {$this->tipoRegistroId} ";
                }


            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE registro_estado SET {$strUpdateFields} WHERE id = {$id}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function updateByInstanceUsingParameters($id, $parameters, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($parameters->nome)) 
                {                                      
                    $arrUpdateFields[] = " nome = {$this->nome} ";
                }
                
                if (isset($parameters->prazoDiasInt)) 
                {                                      
                    $arrUpdateFields[] = " prazo_dias_INT = {$this->prazoDiasInt} ";
                }
                
                if (isset($parameters->tipoRegistroId)) 
                {                                      
                    $arrUpdateFields[] = " tipo_registro_id_INT = {$this->tipoRegistroId} ";
                }
            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE registro_estado SET {$strUpdateFields} WHERE id = {$id}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }

}

?>
