"use strict";
exports.__esModule = true;
var Icone = (function () {
    function Icone(url) {
        this.url = url;
        this.setDefaultValues();
    }
    Icone.prototype.setDefaultValues = function () {
        this.origin = new google.maps.Point(0, 0);
        //this.labelOrigin = new google.maps.Point(0, -5);
    };
    return Icone;
}());
exports.Icone = Icone;
