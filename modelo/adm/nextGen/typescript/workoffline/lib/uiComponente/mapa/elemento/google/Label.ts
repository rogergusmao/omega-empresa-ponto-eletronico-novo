export class Label implements google.maps.MarkerLabel
{
    public color: string;
    public fontFamily: string;
    public fontSize: string;
    public fontWeight: string;
    public text: string;

    public constructor(text :string)
    {
        this.text = text;
        this.setDefaultValues();
    }

    public setDefaultValues()
    {
        this.fontWeight = 'bold';
        this.fontSize = '12px';
        this.fontFamily = '"Helvetica Neue", Helvetica, "Noto Sans", sans-serif, Arial, sans-serif',
        this.color = '#303641';
    }

}