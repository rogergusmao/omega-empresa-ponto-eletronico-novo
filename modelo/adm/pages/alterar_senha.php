<?php= Helper::carregarArquivoJavascript(1, "recursos/libs/pwd_strength", "pwd_strength") ?>

<table width="350" align="center" class="tabela_popup">
    <tr>
        <td height="11" colspan="2" align="left" valign="middle"><img src="imgs/padrao/texto_alterar_senha.jpg"/>
        </td>
    </tr>
    <tr>
        <td height="11" colspan="2" align="left" valign="middle"
            style="padding-left: 5px; padding-top: 10px; padding-bottom: 10px; color: #333333;"></td>
    </tr>
    <tr>
        <td height="22" align="left" valign="middle" style="padding-top: 5px; color: #333333;">

            Senha antiga:

        </td>

        <td height="22" align="left" valign="middle" style="padding-top: 5px; color: #333333;">

            <input type="password" maxlength="16" id="senhaAntiga" class="input_text"
                   onFocus="this.className='focus_text';" onBlur="this.className='input_text';"
                   onkeyup="runPassword(this.value, 'senha1');"/>

        </td>
    </tr>
    <tr>
        <td height="22" align="left" valign="middle" style="padding-top: 5px; color: #333333;">

            Informe sua nova senha:

        </td>

        <td height="22" align="left" valign="middle" style="padding-top: 5px; color: #333333;">

            <input type="password" maxlength="16" id="senhaNova" class="input_text"
                   onFocus="this.className='focus_text';" onBlur="this.className='input_text';"
                   onkeyup="runPassword(this.value, 'senha1');"/>

        </td>
    </tr>
    <tr>
        <td height="22" align="left" valign="middle" style="padding-top: 5px; color: #333333;">

            Confirme sua nova senha:

        </td>
        <td height="22" align="left" valign="middle" style="padding-top: 5px; color: #333333;">

            <input id="confirmaSenhaNova" type="password" maxlength="16" class="input_text"
                   onFocus="this.className='focus_text';" onBlur="this.className='input_text';"/>

        </td>
    </tr>
    <tr>

        <td height="22" align="left" valign="middle" style="padding-top: 5px; color: #333333;">

            <div style="width: 200px; padding: 5px 0px 5px 0px;">
                <div id="senha1_text"></div>
                <div id="senha1_bar"
                     style="font-size: 1px; height: 4px; width: 0px; border: 1px solid white;"></div>
            </div>

        </td>
        <td height="22" align="left" valign="middle" style="padding-top: 5px; color: #333333;">

            <div style="padding: 5px 5px 5px 5px; text-align: right;">
                <a onclick="alterarSenha(this)" class="botoes_form" style="cursor: pointer;">Salvar</a>
            </div>
        </td>

    </tr>
</table>


<script type="text/javascript">

    function alterarSenha(el)
    {
        try
        {
            if (!validarCampos())
            {
                return;
            }
            var $el = $(el);
            var senhaAntiga = $('#senhaAntiga').val();
            var senhaNova = $('#senhaNova').val();
            var confirmaSenhaNova = $('#confirmaSenhaNova').val();

            if (senhaNova !== confirmaSenhaNova)
            {
                bootbox.alert({
                    message: "A nova senha não confere com a do campo de confirmação."
                });
                return;
            }

            var post = {
                senha_antiga: $('#senhaAntiga').val(),
                senha_nova: $('#senhaNova').val()
            };

            adicionarLoading($el);
            $el.css('display', 'none');

            var funcaoASerChamada = function ()
            {
                try
                {
                    var retorno = arguments[0];
                    retorno = JSON.parse(retorno);
                    var codRetorno = retorno.mCodRetorno;

                    if (codRetorno == PROTOCOLO_SISTEMA.SIM)
                    {
                        bootbox.alert({
                            message: "Senha alterada com sucesso"
                        });

                    }
                    else
                    {
                        $el.css('display', 'inline-block');
                        removerLoading($el);
                        bootbox.alert({
                            message: retorno.mMensagem
                        });
                    }
                    return true;

                }
                catch (err)
                {
                    bootbox.alert({
                        message: err.stack
                    });
                    $el.css('display', 'inline-block');
                    return false;
                }
                finally
                {
                    removerLoading($el);
                }
            }
            carregarValorRemotoAsyncPost(
                'webservice.php?class=BO_SICOB&action=alterarSenha',
                null,
                funcaoASerChamada,
                post);

        }
        catch (err)
        {
            removerLoading($el);
            bootbox.alert({
                message: err.stack
            });
            return false;
        }
        finally
        {

        }
        return false;
    }

</script>
