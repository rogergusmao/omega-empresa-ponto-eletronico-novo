"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var React = require("react");
var recharts_1 = require("recharts");
var react_bootstrap_1 = require("react-bootstrap");
var uiComponente_1 = require("lib/uiComponente");
var DistribuicaoDeHoras = (function (_super) {
    __extends(DistribuicaoDeHoras, _super);
    function DistribuicaoDeHoras(props, reactContext) {
        var _this = _super.call(this, props, reactContext) || this;
        _this.dadosGrafico = new Array();
        return _this;
    }
    DistribuicaoDeHoras.prototype.fixLayout = function () {
        var maxHeight = uiComponente_1.DomContainer.getMaxAllowedHeight();
    };
    DistribuicaoDeHoras.prototype.setData = function (pessoas) {
        this.dadosGrafico = pessoas.map(function (pessoa) {
            var dados = {
                legenda: pessoa.nomePessoa + " (" + pessoa.getTempoTotalFormatado() + ")",
                valor: pessoa.getTempoTotal().asHours(),
                cor: pessoa.corBackground.getHexValue()
            };
            return dados;
        });
        this.setState({ dataRefresh: true });
    };
    DistribuicaoDeHoras.prototype.render = function () {
        var altura = this.props.altura ? this.props.altura : this.props.largura;
        if (this.dadosGrafico.length > 0) {
            return (<recharts_1.PieChart width={this.props.largura} height={altura}>
                    <recharts_1.Legend verticalAlign="top" align="left" height={36}/>
                    <recharts_1.Pie data={this.dadosGrafico} dataKey="valor" nameKey="legenda" cx="50%" cy="50%" innerRadius={0} isAnimationActive={false}>
                        {this.dadosGrafico.map(function (entry, index) { return <recharts_1.Cell key={"cell-" + index} fill={entry.cor}/>; })}
                    </recharts_1.Pie>
                </recharts_1.PieChart>);
        }
        else {
            return (<react_bootstrap_1.Alert bsStyle="warning" className="mb-0">
                        <strong></strong> Nenhum ponto encontrado no período
                    </react_bootstrap_1.Alert>);
        }
    };
    return DistribuicaoDeHoras;
}(uiComponente_1.ReactComponent));
exports.DistribuicaoDeHoras = DistribuicaoDeHoras;
