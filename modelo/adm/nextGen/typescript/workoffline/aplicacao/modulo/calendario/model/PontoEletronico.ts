import * as MomentJS from 'moment';

import { Model } from 'lib/model';
import { TipoPontoEletronico, Helper } from 'aplicacao/comum/geral';
import { PontoEletronico as PontoEletronicoRemoteModel } from 'aplicacao/modulo/calendario/remoteModel';

export class PontoEletronico extends Model
{
    public idPonto :number;
    public nomePessoa :string;
    public idPessoa :number;
    public dataPonto :MomentJS.Moment;
    public tipoPonto :TipoPontoEletronico;

    public static factory(remoteObject: PontoEletronicoRemoteModel)
    {
        let instance = new PontoEletronico();

        instance.idPonto = Helper.parseInt(remoteObject.idPonto);
        instance.nomePessoa = remoteObject.nomePessoa;
        instance.idPessoa = Helper.parseInt(remoteObject.idPessoa);
        instance.dataPonto = MomentJS(DateUtil.getDateObject(Helper.parseInt(remoteObject.dataSec) + Helper.parseInt(remoteObject.dataOffsec)));
        instance.tipoPonto = Helper.parseInt(remoteObject.isEntrada) == 1 ? TipoPontoEletronico.PONTO_ENTRADA : TipoPontoEletronico.PONTO_SAIDA;

        return instance;
    }

}