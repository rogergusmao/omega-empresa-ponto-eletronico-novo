<?php

include '../../recursos/languages/pt-br.php';
include '../../recursos/php/funcoes.php';
include '../../recursos/php/constants.php';
include '../../recursos/php/database_config.php';

$nomeScript = Helper::getNomeDoScriptAtual();

$vIsPrimeiraVez = Helper::GET("is_primeira_vez_BOOLEAN");

$obj = new EXTDAO_Operadora();
$registrosPesquisa = 1;
$obj->setByGet($registrosPesquisa);
$obj->formatarParaSQL();

$strCondicao = array();
$strGET = array();

if (!Helper::isNull($obj->getNome()))
{
    $strCondicao[] = "o.nome LIKE '%{$obj->getNome()}%'";
    $strGET[] = "nome1={$obj->getNome()}";
}

if (!Helper::isNull(Helper::GET("datetime_inicio1")))
{

    $datetimeInicio = Helper::GET("datetime_inicio1");

    $strCondicao[] = "msp.data_envio_DATETIME >={$datetimeInicio}";
    $strGET[] = "datetime_inicio1={$datetimeInicio}";
    $strDescricao[] = "&bull; <b>Data inicial:</b> {$datetimeInicio}";
}

if (!Helper::isNull(Helper::GET("datetime_fim1")))
{

    $datetimeFim = Helper::GET("datetime_fim1");

    $strCondicao[] = "msp.data_envio_DATETIME <={$datetimeFim}";
    $strGET[] = "datetime_fim1={$idadeAte}";
    $strDescricao[] = "&bull; <b>Data final:</b> {$datetimeFim} anos";
}

$consulta = "";

for ($i = 0; $i < count($strCondicao); $i++)
{

    $consulta .= " AND " . $strCondicao[$i];
}

for ($i = 0; $i < count($strGET); $i++)
{

    $varGET .= "&" . $strGET[$i];
}

if ($nomeScript == "index.php" && !isset($vIsPrimeiraVez))
{
    echo "<center>";
    Helper::imprimirMensagem("A janela de download abrirá em alguns segundos.\nCaso não abra automaticamente, <a class='link_padrao' href='pages/exportar_relatorio_operadora.php?is_primeira_vez_BOOLEAN=0&$varGET'>clique aqui</a>.");
    echo "</center>";
    Helper::mudarLocation("pages/exportar_relatorio_operadora.php?is_primeira_vez_BOOLEAN=0&$varGET");
    exit();
}

$consultaNumero = "SELECT COUNT(o.id)
        FROM operadora o
        WHERE o.excluido_BOOLEAN=0 {$consulta}";

$objBanco = new Database();

$objBanco->query($consultaNumero);

$vetorLabel = array($obj->label_id, $obj->label_nome, "Total SMS enviado", "Total SMS a enviar", "Total SMS com error", "Total");
$stringRetorno = "";

$consultaRegistros = "SELECT o.id
        FROM operadora o LEFT JOIN mensagem_sms_pessoa msp ON o.id = msp.operadora_id_INT
        WHERE o.excluido_BOOLEAN=0 {$consulta}";

$objBanco->query($consultaRegistros);

if ($objBanco->rows == 0)
{

    ?>
    <?= Helper::imprimirMensagem("Nenhuma operadora foi cadastrada até o momento.") ?>
    <?
}
else
{
    $stringRetorno .= Helper::getStrLinhaCSVDoVetorDeDados($vetorLabel);
    $objBancoAux = new Database();
    for ($i = 1; $regs = $objBanco->fetchArray(); $i++)
    {
        $vTotal = 0;
        $obj->select($regs[0]);
        $vIdOperadora = $regs[0];
        $obj->formatarParaExibicao();

        $classTr = ($i % 2) ? "tr_list_conteudo_impar" : "tr_list_conteudo_par";

        $consultaTotalEnviado = "SELECT DISTINCT o.id id, COUNT(msp.id) total_enviado
                FROM operadora o LEFT JOIN mensagem_sms_pessoa msp ON msp.operadora_id_INT = o.id
                WHERE o.excluido_BOOLEAN=0 AND msp.foi_enviado_BOOLEAN = '1'
                    AND o.id = $vIdOperadora ";
        $objBancoAux->query($consultaTotalEnviado);
        $vTotalEnviado = $objBancoAux->getPrimeiraTuplaDoResultSet("total_enviado");
        if (Helper::isNull($vTotalEnviado))
        {
            $vTotalEnviado = 0;
        }
        $vTotal += $vTotalEnviado;

        $consultaTotalAEnviar = "SELECT DISTINCT o.id, COUNT(msp.id) total_enviado
                FROM operadora o LEFT JOIN mensagem_sms_pessoa msp ON msp.operadora_id_INT = o.id
                WHERE o.excluido_BOOLEAN=0 AND msp.foi_enviado_BOOLEAN = '0' AND msp.numero_tentativa_INT < " . LIMITE_TENTATICA_ENVIO_SMS . "
                    AND o.id = $vIdOperadora";
        $objBancoAux->query($consultaTotalAEnviar);
        $vTotalAEnviar = $objBancoAux->getPrimeiraTuplaDoResultSet("total_enviado");
        if (Helper::isNull($vTotalAEnviar))
        {
            $vTotalAEnviar = 0;
        }
        $vTotal += $vTotalAEnviar;

        $consultaTotalComError = "SELECT DISTINCT o.id, COUNT(msp.id) total_enviado
                FROM operadora o LEFT JOIN mensagem_sms_pessoa msp ON msp.operadora_id_INT = o.id
                WHERE o.excluido_BOOLEAN=0 AND msp.foi_enviado_BOOLEAN = '0' AND msp.numero_tentativa_INT >= " . LIMITE_TENTATICA_ENVIO_SMS . "
                    AND o.id = $vIdOperadora";
        $objBancoAux->query($consultaTotalComError);
        $vTotalComError = $objBancoAux->getPrimeiraTuplaDoResultSet("total_enviado");
        if (Helper::isNull($vTotalComError))
        {
            $vTotalComError = 0;
        }
        $vTotal += $vTotalComError;

        $vetorTupla = array($obj->getId(), $obj->getNome(), $vTotalEnviado, $vTotalAEnviar, $vTotalComError, $vTotal);
        $stringRetorno .= Helper::getStrLinhaCSVDoVetorDeDados($vetorTupla);
    }
}
$objDownload = new Download("relatorio_operadora.csv");
print $objDownload->ds_download($stringRetorno);
?>

