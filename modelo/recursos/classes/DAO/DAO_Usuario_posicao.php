<?php


        /*

        Arquivo gerado através de gerador de código em 12/05/2018 as 14:52:26.
        Para que o arquivo não seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

        Tabela correspondente: usuario_posicao
        Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

        */
        


class DAO_Usuario_posicao extends Generic_DAO
                        {
                            const NOME_ENTIDADE = "usuario_posicao";

                            public static $nomeTabela = null;
                            public static $idSistemaTabela = false;
                            public static $databaseFieldsRelatedAttributes = null;
	                        public static $databaseFieldTypes = null;
	                        public static $databaseFieldNames = null;
	                        public static $listAliasTypes = null;
	                        public static $listAliasRelatedAttributes = null;
	                        
	                        public $id;
public $cadastroSec;
public $cadastroOffsec;
public $veiculoUsuarioId;
public $usuarioId;
public $latitudeInt;
public $longitudeInt;
public $velocidadeInt;
public $fotoInterna;
public $fotoExterna;
public $corporacaoId;
public $fonteInformacaoInt;

public $labelId;
public $labelCadastroSec;
public $labelCadastroOffsec;
public $labelVeiculoUsuarioId;
public $labelUsuarioId;
public $labelLatitudeInt;
public $labelLongitudeInt;
public $labelVelocidadeInt;
public $labelFotoInterna;
public $labelFotoExterna;
public $labelCorporacaoId;
public $labelFonteInformacaoInt;

public static $arrPK = array("id");

public function __construct($configDAO = null)
        {
            parent::__construct($configDAO);
            static::__setStaticValues($this->database);           
        }

public static function idSistemaTabela($databaseObject = null){
            if(!(static::$idSistemaTabela))
            {
                static::$idSistemaTabela = EXTDAO_Sistema_tabela::getIdDaSistemaTabela(static::nomeTabela(), $databaseObject);
            }
            return static::$idSistemaTabela;
        }

public function getIdSistemaTabela(){
            return static::idSistemaTabela();
        }

public static function nomeTabela(){
            if(is_null(static::$nomeTabela))
            {
                static::$nomeTabela = "usuario_posicao";
            }
            return static::$nomeTabela;
        }

public function getNomeTabela(){
            return static::nomeTabela();
        }

public static function __setStaticValues($databaseObject = null)
                   {    
                        static::nomeTabela();
                        
                        static::idSistemaTabela($databaseObject);
                   
                        static::setDatabaseFieldNames();
                        static::setDatabaseFieldTypes();
                        static::setDatabaseFieldsRelatedAttributeNames();
                        
                        static::setListAliasTypes();
                        static::setListAliasRelatedAttributes();
                   }

public function getExpressaoLabel()
                   {
                        return "{id}";
                   }

public static function setDatabaseFieldNames()
        {
            if(is_null(static::$databaseFieldNames))
            {
                static::$databaseFieldNames = new stdClass();
                static::$databaseFieldNames->id = "id";
static::$databaseFieldNames->cadastroSec = "cadastro_SEC";
static::$databaseFieldNames->cadastroOffsec = "cadastro_OFFSEC";
static::$databaseFieldNames->veiculoUsuarioId = "veiculo_usuario_id_INT";
static::$databaseFieldNames->usuarioId = "usuario_id_INT";
static::$databaseFieldNames->latitudeInt = "latitude_INT";
static::$databaseFieldNames->longitudeInt = "longitude_INT";
static::$databaseFieldNames->velocidadeInt = "velocidade_INT";
static::$databaseFieldNames->fotoInterna = "foto_interna";
static::$databaseFieldNames->fotoExterna = "foto_externa";
static::$databaseFieldNames->corporacaoId = "corporacao_id_INT";
static::$databaseFieldNames->fonteInformacaoInt = "fonte_informacao_INT";

            }
                      
        }

public static function setDatabaseFieldTypes()
        {
            if(is_null(static::$databaseFieldTypes))
            {
                static::$databaseFieldTypes = new stdClass();
                static::$databaseFieldTypes->id = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->cadastroSec = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$databaseFieldTypes->cadastroOffsec = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$databaseFieldTypes->veiculoUsuarioId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->usuarioId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->latitudeInt = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->longitudeInt = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->velocidadeInt = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->fotoInterna = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->fotoExterna = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->corporacaoId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->fonteInformacaoInt = static::TIPO_VARIAVEL_INTEGER;

            }
            
        }

public static function setDatabaseFieldsRelatedAttributeNames()
        {
            if(is_null(static::$databaseFieldsRelatedAttributes))
            {
                static::$databaseFieldsRelatedAttributes = new stdClass();
                static::$databaseFieldsRelatedAttributes->id = "id";
static::$databaseFieldsRelatedAttributes->cadastro_SEC = "cadastroSec";
static::$databaseFieldsRelatedAttributes->cadastro_OFFSEC = "cadastroOffsec";
static::$databaseFieldsRelatedAttributes->veiculo_usuario_id_INT = "veiculoUsuarioId";
static::$databaseFieldsRelatedAttributes->usuario_id_INT = "usuarioId";
static::$databaseFieldsRelatedAttributes->latitude_INT = "latitudeInt";
static::$databaseFieldsRelatedAttributes->longitude_INT = "longitudeInt";
static::$databaseFieldsRelatedAttributes->velocidade_INT = "velocidadeInt";
static::$databaseFieldsRelatedAttributes->foto_interna = "fotoInterna";
static::$databaseFieldsRelatedAttributes->foto_externa = "fotoExterna";
static::$databaseFieldsRelatedAttributes->corporacao_id_INT = "corporacaoId";
static::$databaseFieldsRelatedAttributes->fonte_informacao_INT = "fonteInformacaoInt";

            }           
        }

public function setAllFieldsToHumanFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["cadastro_SEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["cadastro_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["veiculo_usuario_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["usuario_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["latitude_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["longitude_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["velocidade_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["foto_interna"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["foto_externa"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["fonte_informacao_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
}

public function setAllFieldsToDatabaseFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["cadastro_SEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["cadastro_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["veiculo_usuario_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["usuario_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["latitude_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["longitude_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["velocidade_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["foto_interna"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["foto_externa"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["fonte_informacao_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
}

  public function getFkObjVeiculo_usuario() 
                {
                    if($this->objVeiculo_usuario == null)
                    {                        
                        $this->objVeiculo_usuario = new EXTDAO_Veiculo_usuario_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getVeiculo_usuario_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objVeiculo_usuario->clear();
                    }
                    elseif($this->objVeiculo_usuario->getId() != $idFK)
                    {
                        $this->objVeiculo_usuario->select($idFK);
                    }
                    return $this->objVeiculo_usuario;
                }
  public function getFkObjUsuario() 
                {
                    if($this->objUsuario == null)
                    {                        
                        $this->objUsuario = new EXTDAO_Usuario_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getUsuario_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objUsuario->clear();
                    }
                    elseif($this->objUsuario->getId() != $idFK)
                    {
                        $this->objUsuario->select($idFK);
                    }
                    return $this->objUsuario;
                }
  public function getFkObjCorporacao() 
                {
                    if($this->objCorporacao == null)
                    {                        
                        $this->objCorporacao = new EXTDAO_Corporacao_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getCorporacao_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objCorporacao->clear();
                    }
                    elseif($this->objCorporacao->getId() != $idFK)
                    {
                        $this->objCorporacao->select($idFK);
                    }
                    return $this->objCorporacao;
                }


public function setLabels()
        {$this->labelId = "id";
$this->labelCadastroSec = "";
$this->labelCadastroOffsec = "";
$this->labelVeiculoUsuarioId = "veiculousuarioidINT";
$this->labelUsuarioId = "usuarioidINT";
$this->labelLatitudeInt = "latitudeINT";
$this->labelLongitudeInt = "longitudeINT";
$this->labelVelocidadeInt = "velocidadeINT";
$this->labelFotoInterna = "fotointerna";
$this->labelFotoExterna = "fotoexterna";
$this->labelCorporacaoId = "corporacaoidINT";
$this->labelFonteInformacaoInt = "";
}

public function __actionAdd($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                $this->setByObject($parameters);
                $this->setDataCadastroAndOffsetInSecondsFromUser($parameters);
                
                $this->formatarParaSQL();
                $msg = $this->insert(true);
                
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                return new Mensagem(null, I18N::getExpression("Usuario posicao adicionado com sucesso."));
                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionEdit($parameters = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try {

                $this->setByObject($parameters);
                $this->setAllFieldsToHumanFormat();
                
                $this->formatarParaSQL();
                $msg = $this->updateByInstanceUsingParameters($this->id, $parameters, true);
                if($msg != null && $msg->erro())
                {
                     return $msg;
                }
        
                return new Mensagem(null, I18N::getExpression("Usuario posicao editado com sucesso."));
            }
            catch(Exception $ex)
            {                
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionRemove($parameters = null)
        {
            $successulRemovalCount = 0;
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                foreach($parameters as $record)
                {
                    if(is_numeric($record->id))
                    {
                        $msg = $this->delete($record->id, true);
                        if($msg == null || $msg->ok())
                        {
                            $successulRemovalCount++;
                        }
                    }
                }
                
                if($successulRemovalCount > 0)
                {
                    $successMessage = count($successulRemovalCount) > 1 ? I18N::getExpression("{0} Usuario posicao foram removidos com sucesso.", $successulRemovalCount) : I18N::getExpression("Usuario posicao removido com sucesso.");
                    return new Mensagem(null, $successMessage);
                }
                else
                {
                    $errorMessage = count($parameters) > 1 ? I18N::getExpression("Falha ao remover Usuario posicao.") : I18N::getExpression("Falha ao remover Usuario posicao.");
                    return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, $errorMessage);                    
                }
            }
            catch (Exception $ex) 
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }

public function __getRecord($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }
            
            try
            {            
                $returnObject = new stdClass();            
                $whereClause = static::getWhereClauseForFilter($parameters->filterParameters);
                $query = "SELECT  id, cadastro_SEC, cadastro_OFFSEC, veiculo_usuario_id_INT, usuario_id_INT, latitude_INT, longitude_INT, velocidade_INT, foto_interna, foto_externa, corporacao_id_INT, fonte_informacao_INT FROM usuario_posicao {$whereClause}";
    
                $msg = $this->database->queryMensagem($query);                
                if ($msg == null)
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Não foi encontrado nenhum registro"));
                    }                    
                    $resultSet = $this->database->result;
                    $resultObject = static::mapResultSetDataToRecord($resultSet);
                    
                    $returnObject->formData = $resultObject;
                    $returnObject->comboBoxesData = null;
                    if($parameters->loadComboBoxesData)
                    {
                        $this->setByObject($resultObject);
                        $returnObject->comboBoxesData =  $this->getFormComboBoxesData();
                    }                    
    
                    
    
                    return new Mensagem_generica($returnObject);
                } 
                else 
                {
                    return $msg;
                }
                
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }


        
        public function __getFilterComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
                
        public function __getFormComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
        
        protected function getFormComboBoxesData()
        {
            $comboBoxesData = new stdClass();
            $listParameters = new stdClass();
            $listParameters->listMappingType = static::LIST_MAPPING_TYPE_COMBOBOX;
            
            
                    
                    $objVeiculoUsuario = new EXTDAO_Veiculo_usuario();
                    $comboBoxesData->fieldVeiculoUsuarioId = $objVeiculoUsuario->__getList($listParameters);
                    
                    $objUsuario = new EXTDAO_Usuario();
                    $comboBoxesData->fieldUsuarioId = $objUsuario->__getList($listParameters);

            return $comboBoxesData;
        }
        
        


        
        public static function setListAliasTypes()
        {
            if(is_null(static::$listAliasTypes))
            {
                static::$listAliasTypes = new stdClass();
                static::$listAliasTypes->veiculo_usuario__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->usuario__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->usuario_posicao__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->usuario_posicao__cadastro_SEC = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$listAliasTypes->usuario_posicao__cadastro_OFFSEC = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$listAliasTypes->usuario_posicao__corporacao_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->usuario_posicao__cadastro_OFFSEC = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$listAliasTypes->usuario_posicao__latitude_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->usuario_posicao__longitude_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->usuario_posicao__velocidade_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->usuario_posicao__foto_interna = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->usuario_posicao__foto_externa = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->usuario_posicao__fonte_informacao_INT = static::TIPO_VARIAVEL_INTEGER;
            }        
        }
        
        public static function setListAliasRelatedAttributes()
        {
            if(is_null(static::$listAliasRelatedAttributes))
            {
                static::$listAliasRelatedAttributes = new stdClass();
                static::$listAliasRelatedAttributes->veiculo_usuario__id = "veiculoUsuarioId";
static::$listAliasRelatedAttributes->usuario__id = "usuarioId";
static::$listAliasRelatedAttributes->usuario_posicao__id = "id";
static::$listAliasRelatedAttributes->usuario_posicao__cadastro_SEC = "cadastroSec";
static::$listAliasRelatedAttributes->usuario_posicao__cadastro_OFFSEC = "cadastroOffsec";
static::$listAliasRelatedAttributes->usuario_posicao__corporacao_id_INT = "corporacaoId";
static::$listAliasRelatedAttributes->usuario_posicao__cadastro_OFFSEC = "cadastroOffsec";
static::$listAliasRelatedAttributes->usuario_posicao__latitude_INT = "latitudeInt";
static::$listAliasRelatedAttributes->usuario_posicao__longitude_INT = "longitudeInt";
static::$listAliasRelatedAttributes->usuario_posicao__velocidade_INT = "velocidadeInt";
static::$listAliasRelatedAttributes->usuario_posicao__foto_interna = "fotoInterna";
static::$listAliasRelatedAttributes->usuario_posicao__foto_externa = "fotoExterna";
static::$listAliasRelatedAttributes->usuario_posicao__fonte_informacao_INT = "fonteInformacaoInt";
            }         
        }

        public function __getList($parameters = null, $idCorporacao = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try
            {
                $filterParameters = new stdClass();
                if(isset($parameters->filterParameters))
                {
                    $filterParameters = $parameters->filterParameters;
                }
                                    
                            
            if(is_null($idCorporacao))
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            ListParameters::addFilterParameterToObject($filterParameters, "corporacaoId", $idCorporacao);           
                
                
                $sortingParameters = null;
                if(isset($parameters->sortingParameters))
                {
                    $sortingParameters = $parameters->sortingParameters;
                }
                
                $paginationParameters = null;
                if(isset($parameters->paginationParameters))
                {
                    $paginationParameters = $parameters->paginationParameters;
                }
                
                $listMappingType = static::getListMappingType($parameters->listMappingType);
                $mainTableAlias = "up";

                $whereClause = static::getWhereClauseForFilter($filterParameters, $mainTableAlias);
                $orderByClause = static::getOrderByClauseForFilter($sortingParameters, $mainTableAlias);
                $limitClause = static::getLimitClauseForFilter($paginationParameters);

                $queryWithoutLimit = "SELECT up.id FROM usuario_posicao up {$whereClause}";
                $query = "SELECT vu.id AS veiculo_usuario__id, u.id AS usuario__id, up.id AS usuario_posicao__id, up.cadastro_SEC AS usuario_posicao__cadastro_SEC, up.cadastro_OFFSEC AS usuario_posicao__cadastro_OFFSEC, up.corporacao_id_INT AS usuario_posicao__corporacao_id_INT, up.cadastro_OFFSEC AS usuario_posicao__cadastro_OFFSEC, up.latitude_INT AS usuario_posicao__latitude_INT, up.longitude_INT AS usuario_posicao__longitude_INT, up.velocidade_INT AS usuario_posicao__velocidade_INT, up.foto_interna AS usuario_posicao__foto_interna, up.foto_externa AS usuario_posicao__foto_externa, up.fonte_informacao_INT AS usuario_posicao__fonte_informacao_INT FROM usuario_posicao up LEFT JOIN veiculo_usuario vu ON vu.id = up.veiculo_usuario_id_INT LEFT JOIN usuario u ON u.id = up.usuario_id_INT {$whereClause} {$orderByClause} {$limitClause}";

                $msg = $this->database->queryMensagem($queryWithoutLimit);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                
                $totalNumberOfRecords = $this->database->rows();
                if($totalNumberOfRecords == 0)
                {
                    return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                }                        
                        
                $msg = $this->database->queryMensagem($query);
                
                //operacao realizaca com sucesso
                if ($msg == null) 
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                    }                        
                
                    $resultSet = $this->database->result;

                    $objReturn = new stdClass();
                    $objReturn->dataSet = static::mapResultSetDataToList($resultSet, $listMappingType, static::$listAliasTypes);

                    if(!Helper::isNullOrEmpty($paginationParameters->recordsPerPage) && !Helper::isNullOrEmpty($paginationParameters->currentPage))
                    {
                        $lastPage = ceil($totalNumberOfRecords / $paginationParameters->recordsPerPage);
                        $objReturn->paginationParameters = new PaginationParams($paginationParameters->recordsPerPage, $paginationParameters->currentPage, $lastPage);
                    }
                    
                    return new Mensagem_generica($objReturn);
                } 
                else
                {
                    return $msg;
                }                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }



public function getId()
            {
                return $this->id;
            }

public function getCadastroSec()
            {
                return $this->cadastroSec;
            }

public function getCadastro_SEC()
                {
                    return $this->cadastroSec;
                }

public function getCadastroOffsec()
            {
                return $this->cadastroOffsec;
            }

public function getCadastro_OFFSEC()
                {
                    return $this->cadastroOffsec;
                }

public function getVeiculoUsuarioId()
            {
                return $this->veiculoUsuarioId;
            }

public function getVeiculo_usuario_id_INT()
                {
                    return $this->veiculoUsuarioId;
                }

public function getUsuarioId()
            {
                return $this->usuarioId;
            }

public function getUsuario_id_INT()
                {
                    return $this->usuarioId;
                }

public function getLatitudeInt()
            {
                return $this->latitudeInt;
            }

public function getLatitude_INT()
                {
                    return $this->latitudeInt;
                }

public function getLongitudeInt()
            {
                return $this->longitudeInt;
            }

public function getLongitude_INT()
                {
                    return $this->longitudeInt;
                }

public function getVelocidadeInt()
            {
                return $this->velocidadeInt;
            }

public function getVelocidade_INT()
                {
                    return $this->velocidadeInt;
                }

public function getFotoInterna()
            {
                return $this->fotoInterna;
            }

public function getFoto_interna()
                {
                    return $this->fotoInterna;
                }

public function getFotoExterna()
            {
                return $this->fotoExterna;
            }

public function getFoto_externa()
                {
                    return $this->fotoExterna;
                }

public function getCorporacaoId()
            {
                return $this->corporacaoId;
            }

public function getCorporacao_id_INT()
                {
                    return $this->corporacaoId;
                }

public function getFonteInformacaoInt()
            {
                return $this->fonteInformacaoInt;
            }

public function getFonte_informacao_INT()
                {
                    return $this->fonteInformacaoInt;
                }



function setId($value)
            {
                $this->id = $value;
            }

function setCadastroSec($value)
            {
                $this->cadastroSec = $value;
            }

public function setCadastro_SEC($value)
                { 
                    $this->cadastroSec = $value; 
                }

function setCadastroOffsec($value)
            {
                $this->cadastroOffsec = $value;
            }

public function setCadastro_OFFSEC($value)
                { 
                    $this->cadastroOffsec = $value; 
                }

function setVeiculoUsuarioId($value)
            {
                $this->veiculoUsuarioId = $value;
            }

public function setVeiculo_usuario_id_INT($value)
                { 
                    $this->veiculoUsuarioId = $value; 
                }

function setUsuarioId($value)
            {
                $this->usuarioId = $value;
            }

public function setUsuario_id_INT($value)
                { 
                    $this->usuarioId = $value; 
                }

function setLatitudeInt($value)
            {
                $this->latitudeInt = $value;
            }

public function setLatitude_INT($value)
                { 
                    $this->latitudeInt = $value; 
                }

function setLongitudeInt($value)
            {
                $this->longitudeInt = $value;
            }

public function setLongitude_INT($value)
                { 
                    $this->longitudeInt = $value; 
                }

function setVelocidadeInt($value)
            {
                $this->velocidadeInt = $value;
            }

public function setVelocidade_INT($value)
                { 
                    $this->velocidadeInt = $value; 
                }

function setFotoInterna($value)
            {
                $this->fotoInterna = $value;
            }

public function setFoto_interna($value)
                { 
                    $this->fotoInterna = $value; 
                }

function setFotoExterna($value)
            {
                $this->fotoExterna = $value;
            }

public function setFoto_externa($value)
                { 
                    $this->fotoExterna = $value; 
                }

function setCorporacaoId($value)
            {
                $this->corporacaoId = $value;
            }

public function setCorporacao_id_INT($value)
                { 
                    $this->corporacaoId = $value; 
                }

function setFonteInformacaoInt($value)
            {
                $this->fonteInformacaoInt = $value;
            }

public function setFonte_informacao_INT($value)
                { 
                    $this->fonteInformacaoInt = $value; 
                }



public function clear()
        {$this->id = null;
$this->cadastroSec = null;
$this->cadastroOffsec = null;
$this->veiculoUsuarioId = null;
if($this->objVeiculo_usuario != null) unset($this->objVeiculo_usuario);
$this->usuarioId = null;
if($this->objUsuario != null) unset($this->objUsuario);
$this->latitudeInt = null;
$this->longitudeInt = null;
$this->velocidadeInt = null;
$this->fotoInterna = null;
$this->fotoExterna = null;
$this->corporacaoId = null;
if($this->objCorporacao != null) unset($this->objCorporacao);
$this->fonteInformacaoInt = null;
if(is_array($this->arrEstadosCampos)){
                        unset($this->arrEstadosCampos);
                     $this->arrEstadosCampos = array();
                    }
                }

public function formatarParaSQL()
        {
if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->id)){
$this->id = $this->formatarIntegerParaComandoSQL($this->id);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->id);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->cadastroSec)){
$this->cadastroSec = $this->formatarIntegerParaComandoSQL($this->cadastroSec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->cadastroSec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->cadastroOffsec)){
$this->cadastroOffsec = $this->formatarIntegerParaComandoSQL($this->cadastroOffsec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->cadastroOffsec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->veiculoUsuarioId)){
$this->veiculoUsuarioId = $this->formatarIntegerParaComandoSQL($this->veiculoUsuarioId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->veiculoUsuarioId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->usuarioId)){
$this->usuarioId = $this->formatarIntegerParaComandoSQL($this->usuarioId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->usuarioId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->latitudeInt)){
$this->latitudeInt = $this->formatarIntegerParaComandoSQL($this->latitudeInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->latitudeInt);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->longitudeInt)){
$this->longitudeInt = $this->formatarIntegerParaComandoSQL($this->longitudeInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->longitudeInt);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->velocidadeInt)){
$this->velocidadeInt = $this->formatarIntegerParaComandoSQL($this->velocidadeInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->velocidadeInt);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->fotoInterna)){
$this->fotoInterna = $this->formatarStringParaComandoSQL($this->fotoInterna);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->fotoInterna);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->fotoExterna)){
$this->fotoExterna = $this->formatarStringParaComandoSQL($this->fotoExterna);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->fotoExterna);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->corporacaoId)){
$this->corporacaoId = $this->formatarIntegerParaComandoSQL($this->corporacaoId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->corporacaoId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->fonteInformacaoInt)){
$this->fonteInformacaoInt = $this->formatarIntegerParaComandoSQL($this->fonteInformacaoInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->fonteInformacaoInt);
}

}

public function formatarParaExibicao()
        {if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->fotoInterna)){
$this->fotoInterna = $this->formatarStringParaExibicao($this->fotoInterna);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->fotoInterna);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->fotoExterna)){
$this->fotoExterna = $this->formatarStringParaExibicao($this->fotoExterna);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->fotoExterna);
}

}

function select($id, $idCorporacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }   

            $sql = "SELECT id, cadastro_SEC, cadastro_OFFSEC, veiculo_usuario_id_INT, usuario_id_INT, latitude_INT, longitude_INT, velocidade_INT, foto_interna, foto_externa, corporacao_id_INT, fonte_informacao_INT FROM usuario_posicao WHERE id = $id AND corporacao_id_INT = $idCorporacao";

            $msg = $this->database->queryMensagem($sql);
            $result = $this->database->result;

            if($msg != null && $msg->erro() )
            { 
            		$this->clear();
            		return $msg;
        	} 
        	else if($this->database->rows() == 0)
        	{
                return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nenhum registro foi encontrado."));
            }
        

            $row = $this->database->fetchArray(MYSQLI_NUM);
            
            		$this->id = $row[0];
		$this->cadastroSec = $row[1];
		$this->cadastroOffsec = $row[2];
		$this->veiculoUsuarioId = $row[3];
		$this->usuarioId = $row[4];
		$this->latitudeInt = $row[5];
		$this->longitudeInt = $row[6];
		$this->velocidadeInt = $row[7];
		$this->fotoInterna = $row[8];
		$this->fotoExterna = $row[9];
		$this->fonteInformacaoInt = $row[11];

            
        }

public function delete($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (is_null($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }

            $sql = "DELETE FROM usuario_posicao WHERE id= $id AND corporacao_id_INT = $idCorporacao";
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                    && $sincronizar
                        && static::idSistemaTabela() != null
                            && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_REMOVE,
                                null,
                                null,
                                $id,
                                $this->database);
            }

            return $msg;
        }

public function insert($sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null, $opcoes = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $this->defineDataCadastroInSecondsIfNotDefined();
$this->defineDataCadastroOffsetInSecondsIfNotDefined();

            
            if (is_null($opcoes))
            {
                $opcoes = $this->opcoesDAO;
            }

            if (!is_numeric($this->id))
            {
                $this->id = "null";
                if ($opcoes == null || $opcoes[Database::OPCAO_GERAR_ID] != false) 
                {
                    $this->id = EXTDAO_Sistema_sequencia::gerarId(static::nomeTabela());                    
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }    
                    else if($msg == null)
                    {
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
                else 
                {
                    $this->id = null;
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao));
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }
                    else if($msg == null)
                    {
                        $this->id = $this->database->getLastInsertId();
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
            }
            else 
            {
                $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }                        
                else if($msg == null)
                {
                   $msg = new Mensagem_token();
                   $msg->mValor = $this->id;
                }
            }
            if($msg == null || $msg->ok())
            {                
                if($sincronizar
                    && static::idSistemaTabela() != null
                    && isset($this->id)){
                    
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_INSERT,
                                null,
                                null,
                                $this->id,
                                $this->database); 
                	}
            } 
            else
            {
                $this->id = null;
            }
            
            return $msg;
            
        }

public function getSQLInsert($idCorporacao){ 

return "INSERT INTO usuario_posicao (id, cadastro_SEC, cadastro_OFFSEC, veiculo_usuario_id_INT, usuario_id_INT, latitude_INT, longitude_INT, velocidade_INT, foto_interna, foto_externa, corporacao_id_INT, fonte_informacao_INT) VALUES ( $this->id ,  $this->cadastroSec ,  $this->cadastroOffsec ,  $this->veiculoUsuarioId ,  $this->usuarioId ,  $this->latitudeInt ,  $this->longitudeInt ,  $this->velocidadeInt ,  $this->fotoInterna ,  $this->fotoExterna ,  $idCorporacao ,  $this->fonteInformacaoInt ) "; 

}




        
        public function updateByParameters($id, $objParametros, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao))
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();

            
                
                if (isset($objParametros->cadastroSec)) 
                {
                    $arrUpdateFields[] = " cadastro_SEC = {$objParametros->cadastroSec} ";
                }


                
                if (isset($objParametros->cadastroOffsec)) 
                {
                    $arrUpdateFields[] = " cadastro_OFFSEC = {$objParametros->cadastroOffsec} ";
                }


                
                if (isset($objParametros->veiculoUsuarioId)) 
                {
                    $arrUpdateFields[] = " veiculo_usuario_id_INT = {$objParametros->veiculoUsuarioId} ";
                }


                
                if (isset($objParametros->usuarioId)) 
                {
                    $arrUpdateFields[] = " usuario_id_INT = {$objParametros->usuarioId} ";
                }


                
                if (isset($objParametros->latitudeInt)) 
                {
                    $arrUpdateFields[] = " latitude_INT = {$objParametros->latitudeInt} ";
                }


                
                if (isset($objParametros->longitudeInt)) 
                {
                    $arrUpdateFields[] = " longitude_INT = {$objParametros->longitudeInt} ";
                }


                
                if (isset($objParametros->velocidadeInt)) 
                {
                    $arrUpdateFields[] = " velocidade_INT = {$objParametros->velocidadeInt} ";
                }


                
                if (isset($objParametros->fotoInterna)) 
                {
                    $arrUpdateFields[] = " foto_interna = {$objParametros->fotoInterna} ";
                }


                
                if (isset($objParametros->fotoExterna)) 
                {
                    $arrUpdateFields[] = " foto_externa = {$objParametros->fotoExterna} ";
                }


                
                if (isset($objParametros->fonteInformacaoInt)) 
                {
                    $arrUpdateFields[] = " fonte_informacao_INT = {$objParametros->fonteInformacaoInt} ";
                }



            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE usuario_posicao SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";

            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null)
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function update($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($this->cadastroSec)) 
                {                                      
                    $arrUpdateFields[] = " cadastro_SEC = {$this->cadastroSec} ";
                }


                
                if (isset($this->cadastroOffsec)) 
                {                                      
                    $arrUpdateFields[] = " cadastro_OFFSEC = {$this->cadastroOffsec} ";
                }


                
                if (isset($this->veiculoUsuarioId)) 
                {                                      
                    $arrUpdateFields[] = " veiculo_usuario_id_INT = {$this->veiculoUsuarioId} ";
                }


                
                if (isset($this->usuarioId)) 
                {                                      
                    $arrUpdateFields[] = " usuario_id_INT = {$this->usuarioId} ";
                }


                
                if (isset($this->latitudeInt)) 
                {                                      
                    $arrUpdateFields[] = " latitude_INT = {$this->latitudeInt} ";
                }


                
                if (isset($this->longitudeInt)) 
                {                                      
                    $arrUpdateFields[] = " longitude_INT = {$this->longitudeInt} ";
                }


                
                if (isset($this->velocidadeInt)) 
                {                                      
                    $arrUpdateFields[] = " velocidade_INT = {$this->velocidadeInt} ";
                }


                
                if (isset($this->fotoInterna)) 
                {                                      
                    $arrUpdateFields[] = " foto_interna = {$this->fotoInterna} ";
                }


                
                if (isset($this->fotoExterna)) 
                {                                      
                    $arrUpdateFields[] = " foto_externa = {$this->fotoExterna} ";
                }


                
                if (isset($this->fonteInformacaoInt)) 
                {                                      
                    $arrUpdateFields[] = " fonte_informacao_INT = {$this->fonteInformacaoInt} ";
                }


            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE usuario_posicao SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function updateByInstanceUsingParameters($id, $parameters, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($parameters->cadastroSec)) 
                {                                      
                    $arrUpdateFields[] = " cadastro_SEC = {$this->cadastroSec} ";
                }
                
                if (isset($parameters->cadastroOffsec)) 
                {                                      
                    $arrUpdateFields[] = " cadastro_OFFSEC = {$this->cadastroOffsec} ";
                }
                
                if (isset($parameters->veiculoUsuarioId)) 
                {                                      
                    $arrUpdateFields[] = " veiculo_usuario_id_INT = {$this->veiculoUsuarioId} ";
                }
                
                if (isset($parameters->usuarioId)) 
                {                                      
                    $arrUpdateFields[] = " usuario_id_INT = {$this->usuarioId} ";
                }
                
                if (isset($parameters->latitudeInt)) 
                {                                      
                    $arrUpdateFields[] = " latitude_INT = {$this->latitudeInt} ";
                }
                
                if (isset($parameters->longitudeInt)) 
                {                                      
                    $arrUpdateFields[] = " longitude_INT = {$this->longitudeInt} ";
                }
                
                if (isset($parameters->velocidadeInt)) 
                {                                      
                    $arrUpdateFields[] = " velocidade_INT = {$this->velocidadeInt} ";
                }
                
                if (isset($parameters->fotoInterna)) 
                {                                      
                    $arrUpdateFields[] = " foto_interna = {$this->fotoInterna} ";
                }
                
                if (isset($parameters->fotoExterna)) 
                {                                      
                    $arrUpdateFields[] = " foto_externa = {$this->fotoExterna} ";
                }
                
                if (isset($parameters->fonteInformacaoInt)) 
                {                                      
                    $arrUpdateFields[] = " fonte_informacao_INT = {$this->fonteInformacaoInt} ";
                }
            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE usuario_posicao SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }

}

?>
