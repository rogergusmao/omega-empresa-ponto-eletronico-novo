<?php


        /*

        Arquivo gerado através de gerador de código em 12/05/2018 as 14:45:53.
        Para que o arquivo não seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

        Tabela correspondente: empresa_atividade_compra
        Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

        */
        


class DAO_Empresa_atividade_compra extends Generic_DAO
                        {
                            const NOME_ENTIDADE = "empresa_atividade_compra";

                            public static $nomeTabela = null;
                            public static $idSistemaTabela = false;
                            public static $databaseFieldsRelatedAttributes = null;
	                        public static $databaseFieldTypes = null;
	                        public static $databaseFieldNames = null;
	                        public static $listAliasTypes = null;
	                        public static $listAliasRelatedAttributes = null;
	                        
	                        public $id;
public $empresaCompraId;
public $quantidadeFloat;
public $valorTotalFloat;
public $descontoFloat;
public $descricao;
public $atividadeId;
public $empresaId;
public $corporacaoId;

public $labelId;
public $labelEmpresaCompraId;
public $labelQuantidadeFloat;
public $labelValorTotalFloat;
public $labelDescontoFloat;
public $labelDescricao;
public $labelAtividadeId;
public $labelEmpresaId;
public $labelCorporacaoId;

public static $arrPK = array("id");

public function __construct($configDAO = null)
        {
            parent::__construct($configDAO);
            static::__setStaticValues($this->database);           
        }

public static function idSistemaTabela($databaseObject = null){
            if(!(static::$idSistemaTabela))
            {
                static::$idSistemaTabela = EXTDAO_Sistema_tabela::getIdDaSistemaTabela(static::nomeTabela(), $databaseObject);
            }
            return static::$idSistemaTabela;
        }

public function getIdSistemaTabela(){
            return static::idSistemaTabela();
        }

public static function nomeTabela(){
            if(is_null(static::$nomeTabela))
            {
                static::$nomeTabela = "empresa_atividade_compra";
            }
            return static::$nomeTabela;
        }

public function getNomeTabela(){
            return static::nomeTabela();
        }

public static function __setStaticValues($databaseObject = null)
                   {    
                        static::nomeTabela();
                        
                        static::idSistemaTabela($databaseObject);
                   
                        static::setDatabaseFieldNames();
                        static::setDatabaseFieldTypes();
                        static::setDatabaseFieldsRelatedAttributeNames();
                        
                        static::setListAliasTypes();
                        static::setListAliasRelatedAttributes();
                   }

public function getExpressaoLabel()
                   {
                        return "{id}";
                   }

public static function setDatabaseFieldNames()
        {
            if(is_null(static::$databaseFieldNames))
            {
                static::$databaseFieldNames = new stdClass();
                static::$databaseFieldNames->id = "id";
static::$databaseFieldNames->empresaCompraId = "empresa_compra_id_INT";
static::$databaseFieldNames->quantidadeFloat = "quantidade_FLOAT";
static::$databaseFieldNames->valorTotalFloat = "valor_total_FLOAT";
static::$databaseFieldNames->descontoFloat = "desconto_FLOAT";
static::$databaseFieldNames->descricao = "descricao";
static::$databaseFieldNames->atividadeId = "atividade_id_INT";
static::$databaseFieldNames->empresaId = "empresa_id_INT";
static::$databaseFieldNames->corporacaoId = "corporacao_id_INT";

            }
                      
        }

public static function setDatabaseFieldTypes()
        {
            if(is_null(static::$databaseFieldTypes))
            {
                static::$databaseFieldTypes = new stdClass();
                static::$databaseFieldTypes->id = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->empresaCompraId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->quantidadeFloat = static::TIPO_VARIAVEL_FLOAT;
static::$databaseFieldTypes->valorTotalFloat = static::TIPO_VARIAVEL_FLOAT;
static::$databaseFieldTypes->descontoFloat = static::TIPO_VARIAVEL_FLOAT;
static::$databaseFieldTypes->descricao = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->atividadeId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->empresaId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->corporacaoId = static::TIPO_VARIAVEL_INTEGER;

            }
            
        }

public static function setDatabaseFieldsRelatedAttributeNames()
        {
            if(is_null(static::$databaseFieldsRelatedAttributes))
            {
                static::$databaseFieldsRelatedAttributes = new stdClass();
                static::$databaseFieldsRelatedAttributes->id = "id";
static::$databaseFieldsRelatedAttributes->empresa_compra_id_INT = "empresaCompraId";
static::$databaseFieldsRelatedAttributes->quantidade_FLOAT = "quantidadeFloat";
static::$databaseFieldsRelatedAttributes->valor_total_FLOAT = "valorTotalFloat";
static::$databaseFieldsRelatedAttributes->desconto_FLOAT = "descontoFloat";
static::$databaseFieldsRelatedAttributes->descricao = "descricao";
static::$databaseFieldsRelatedAttributes->atividade_id_INT = "atividadeId";
static::$databaseFieldsRelatedAttributes->empresa_id_INT = "empresaId";
static::$databaseFieldsRelatedAttributes->corporacao_id_INT = "corporacaoId";

            }           
        }

public function setAllFieldsToHumanFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["empresa_compra_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["quantidade_FLOAT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["valor_total_FLOAT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["desconto_FLOAT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["descricao"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["atividade_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["empresa_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
}

public function setAllFieldsToDatabaseFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["empresa_compra_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["quantidade_FLOAT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["valor_total_FLOAT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["desconto_FLOAT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["descricao"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["atividade_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["empresa_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
}

  public function getFkObjEmpresa_compra() 
                {
                    if($this->objEmpresa_compra == null)
                    {                        
                        $this->objEmpresa_compra = new EXTDAO_Empresa_compra_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getEmpresa_compra_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objEmpresa_compra->clear();
                    }
                    elseif($this->objEmpresa_compra->getId() != $idFK)
                    {
                        $this->objEmpresa_compra->select($idFK);
                    }
                    return $this->objEmpresa_compra;
                }
  public function getFkObjAtividade() 
                {
                    if($this->objAtividade == null)
                    {                        
                        $this->objAtividade = new EXTDAO_Atividade_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getAtividade_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objAtividade->clear();
                    }
                    elseif($this->objAtividade->getId() != $idFK)
                    {
                        $this->objAtividade->select($idFK);
                    }
                    return $this->objAtividade;
                }
  public function getFkObjEmpresa() 
                {
                    if($this->objEmpresa == null)
                    {                        
                        $this->objEmpresa = new EXTDAO_Empresa_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getEmpresa_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objEmpresa->clear();
                    }
                    elseif($this->objEmpresa->getId() != $idFK)
                    {
                        $this->objEmpresa->select($idFK);
                    }
                    return $this->objEmpresa;
                }
  public function getFkObjCorporacao() 
                {
                    if($this->objCorporacao == null)
                    {                        
                        $this->objCorporacao = new EXTDAO_Corporacao_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getCorporacao_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objCorporacao->clear();
                    }
                    elseif($this->objCorporacao->getId() != $idFK)
                    {
                        $this->objCorporacao->select($idFK);
                    }
                    return $this->objCorporacao;
                }


public function setLabels()
        {$this->labelId = "";
$this->labelEmpresaCompraId = "";
$this->labelQuantidadeFloat = "";
$this->labelValorTotalFloat = "";
$this->labelDescontoFloat = "";
$this->labelDescricao = "";
$this->labelAtividadeId = "";
$this->labelEmpresaId = "";
$this->labelCorporacaoId = "";
}

public function __actionAdd($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                $this->setByObject($parameters);
                $this->setDataCadastroAndOffsetInSecondsFromUser($parameters);
                
                $this->formatarParaSQL();
                $msg = $this->insert(true);
                
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                return new Mensagem(null, I18N::getExpression("Empresa atividade compra adicionado com sucesso."));
                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionEdit($parameters = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try {

                $this->setByObject($parameters);
                $this->setAllFieldsToHumanFormat();
                
                $this->formatarParaSQL();
                $msg = $this->updateByInstanceUsingParameters($this->id, $parameters, true);
                if($msg != null && $msg->erro())
                {
                     return $msg;
                }
        
                return new Mensagem(null, I18N::getExpression("Empresa atividade compra editado com sucesso."));
            }
            catch(Exception $ex)
            {                
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionRemove($parameters = null)
        {
            $successulRemovalCount = 0;
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                foreach($parameters as $record)
                {
                    if(is_numeric($record->id))
                    {
                        $msg = $this->delete($record->id, true);
                        if($msg == null || $msg->ok())
                        {
                            $successulRemovalCount++;
                        }
                    }
                }
                
                if($successulRemovalCount > 0)
                {
                    $successMessage = count($successulRemovalCount) > 1 ? I18N::getExpression("{0} Empresa atividade compra foram removidos com sucesso.", $successulRemovalCount) : I18N::getExpression("Empresa atividade compra removido com sucesso.");
                    return new Mensagem(null, $successMessage);
                }
                else
                {
                    $errorMessage = count($parameters) > 1 ? I18N::getExpression("Falha ao remover Empresa atividade compra.") : I18N::getExpression("Falha ao remover Empresa atividade compra.");
                    return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, $errorMessage);                    
                }
            }
            catch (Exception $ex) 
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }

public function __getRecord($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }
            
            try
            {            
                $returnObject = new stdClass();            
                $whereClause = static::getWhereClauseForFilter($parameters->filterParameters);
                $query = "SELECT  id, empresa_compra_id_INT, quantidade_FLOAT, valor_total_FLOAT, desconto_FLOAT, descricao, atividade_id_INT, empresa_id_INT, corporacao_id_INT FROM empresa_atividade_compra {$whereClause}";
    
                $msg = $this->database->queryMensagem($query);                
                if ($msg == null)
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Não foi encontrado nenhum registro"));
                    }                    
                    $resultSet = $this->database->result;
                    $resultObject = static::mapResultSetDataToRecord($resultSet);
                    
                    $returnObject->formData = $resultObject;
                    $returnObject->comboBoxesData = null;
                    if($parameters->loadComboBoxesData)
                    {
                        $this->setByObject($resultObject);
                        $returnObject->comboBoxesData =  $this->getFormComboBoxesData();
                    }                    
    
                    
    
                    return new Mensagem_generica($returnObject);
                } 
                else 
                {
                    return $msg;
                }
                
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }


        
        public function __getFilterComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
                
        public function __getFormComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
        
        protected function getFormComboBoxesData()
        {
            $comboBoxesData = new stdClass();
            $listParameters = new stdClass();
            $listParameters->listMappingType = static::LIST_MAPPING_TYPE_COMBOBOX;
            
            
                    
                    $objEmpresaCompra = new EXTDAO_Empresa_compra();
                    $comboBoxesData->fieldEmpresaCompraId = $objEmpresaCompra->__getList($listParameters);
                    
                    $objAtividade = new EXTDAO_Atividade();
                    $comboBoxesData->fieldAtividadeId = $objAtividade->__getList($listParameters);
                    
                    $objEmpresa = new EXTDAO_Empresa();
                    $comboBoxesData->fieldEmpresaId = $objEmpresa->__getList($listParameters);

            return $comboBoxesData;
        }
        
        


        
        public static function setListAliasTypes()
        {
            if(is_null(static::$listAliasTypes))
            {
                static::$listAliasTypes = new stdClass();
                static::$listAliasTypes->empresa_compra__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->atividade__nome = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->empresa__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->empresa_atividade_compra__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->empresa_atividade_compra__protocolo_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->empresa_atividade_compra__quantidade_FLOAT = static::TIPO_VARIAVEL_FLOAT;
static::$listAliasTypes->empresa_atividade_compra__valor_total_FLOAT = static::TIPO_VARIAVEL_FLOAT;
static::$listAliasTypes->empresa_atividade_compra__desconto_FLOAT = static::TIPO_VARIAVEL_FLOAT;
static::$listAliasTypes->empresa_atividade_compra__descricao = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->empresa_atividade_compra__relatorio_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->empresa_atividade_compra__ind_celular_valido_BOOLEAN = static::TIPO_VARIAVEL_BOOLEAN;
            }        
        }
        
        public static function setListAliasRelatedAttributes()
        {
            if(is_null(static::$listAliasRelatedAttributes))
            {
                static::$listAliasRelatedAttributes = new stdClass();
                static::$listAliasRelatedAttributes->empresa_compra__id = "empresaCompraId";
static::$listAliasRelatedAttributes->atividade__nome = "atividadeNome";
static::$listAliasRelatedAttributes->empresa__id = "empresaId";
static::$listAliasRelatedAttributes->empresa_atividade_compra__id = "id";
static::$listAliasRelatedAttributes->empresa_atividade_compra__protocolo_INT = "protocoloInt";
static::$listAliasRelatedAttributes->empresa_atividade_compra__quantidade_FLOAT = "quantidadeFloat";
static::$listAliasRelatedAttributes->empresa_atividade_compra__valor_total_FLOAT = "valorTotalFloat";
static::$listAliasRelatedAttributes->empresa_atividade_compra__desconto_FLOAT = "descontoFloat";
static::$listAliasRelatedAttributes->empresa_atividade_compra__descricao = "descricao";
static::$listAliasRelatedAttributes->empresa_atividade_compra__relatorio_id_INT = "relatorioId";
static::$listAliasRelatedAttributes->empresa_atividade_compra__ind_celular_valido_BOOLEAN = "indCelularValidoBoolean";
            }         
        }

        public function __getList($parameters = null, $idCorporacao = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try
            {
                $filterParameters = new stdClass();
                if(isset($parameters->filterParameters))
                {
                    $filterParameters = $parameters->filterParameters;
                }
                                    
                            
            if(is_null($idCorporacao))
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            ListParameters::addFilterParameterToObject($filterParameters, "corporacaoId", $idCorporacao);           
                
                
                $sortingParameters = null;
                if(isset($parameters->sortingParameters))
                {
                    $sortingParameters = $parameters->sortingParameters;
                }
                
                $paginationParameters = null;
                if(isset($parameters->paginationParameters))
                {
                    $paginationParameters = $parameters->paginationParameters;
                }
                
                $listMappingType = static::getListMappingType($parameters->listMappingType);
                $mainTableAlias = "eac";

                $whereClause = static::getWhereClauseForFilter($filterParameters, $mainTableAlias);
                $orderByClause = static::getOrderByClauseForFilter($sortingParameters, $mainTableAlias);
                $limitClause = static::getLimitClauseForFilter($paginationParameters);

                $queryWithoutLimit = "SELECT eac.id FROM empresa_atividade_compra eac {$whereClause}";
                $query = "SELECT ec.id AS empresa_compra__id, a.nome AS atividade__nome, e.id AS empresa__id, eac.id AS empresa_atividade_compra__id, eac.protocolo_INT AS empresa_atividade_compra__protocolo_INT, eac.quantidade_FLOAT AS empresa_atividade_compra__quantidade_FLOAT, eac.valor_total_FLOAT AS empresa_atividade_compra__valor_total_FLOAT, eac.desconto_FLOAT AS empresa_atividade_compra__desconto_FLOAT, eac.descricao AS empresa_atividade_compra__descricao, eac.relatorio_id_INT AS empresa_atividade_compra__relatorio_id_INT, eac.ind_celular_valido_BOOLEAN AS empresa_atividade_compra__ind_celular_valido_BOOLEAN FROM empresa_atividade_compra eac LEFT JOIN empresa_compra ec ON ec.id = eac.empresa_compra_id_INT LEFT JOIN atividade a ON a.id = eac.atividade_id_INT LEFT JOIN empresa e ON e.id = eac.empresa_id_INT {$whereClause} {$orderByClause} {$limitClause}";

                $msg = $this->database->queryMensagem($queryWithoutLimit);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                
                $totalNumberOfRecords = $this->database->rows();
                if($totalNumberOfRecords == 0)
                {
                    return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                }                        
                        
                $msg = $this->database->queryMensagem($query);
                
                //operacao realizaca com sucesso
                if ($msg == null) 
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                    }                        
                
                    $resultSet = $this->database->result;

                    $objReturn = new stdClass();
                    $objReturn->dataSet = static::mapResultSetDataToList($resultSet, $listMappingType, static::$listAliasTypes);

                    if(!Helper::isNullOrEmpty($paginationParameters->recordsPerPage) && !Helper::isNullOrEmpty($paginationParameters->currentPage))
                    {
                        $lastPage = ceil($totalNumberOfRecords / $paginationParameters->recordsPerPage);
                        $objReturn->paginationParameters = new PaginationParams($paginationParameters->recordsPerPage, $paginationParameters->currentPage, $lastPage);
                    }
                    
                    return new Mensagem_generica($objReturn);
                } 
                else
                {
                    return $msg;
                }                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }



public function getId()
            {
                return $this->id;
            }

public function getEmpresaCompraId()
            {
                return $this->empresaCompraId;
            }

public function getEmpresa_compra_id_INT()
                {
                    return $this->empresaCompraId;
                }

public function getQuantidadeFloat()
            {
                return $this->quantidadeFloat;
            }

public function getQuantidade_FLOAT()
                {
                    return $this->quantidadeFloat;
                }

public function getValorTotalFloat()
            {
                return $this->valorTotalFloat;
            }

public function getValor_total_FLOAT()
                {
                    return $this->valorTotalFloat;
                }

public function getDescontoFloat()
            {
                return $this->descontoFloat;
            }

public function getDesconto_FLOAT()
                {
                    return $this->descontoFloat;
                }

public function getDescricao()
            {
                return $this->descricao;
            }

public function getAtividadeId()
            {
                return $this->atividadeId;
            }

public function getAtividade_id_INT()
                {
                    return $this->atividadeId;
                }

public function getEmpresaId()
            {
                return $this->empresaId;
            }

public function getEmpresa_id_INT()
                {
                    return $this->empresaId;
                }

public function getCorporacaoId()
            {
                return $this->corporacaoId;
            }

public function getCorporacao_id_INT()
                {
                    return $this->corporacaoId;
                }



function setId($value)
            {
                $this->id = $value;
            }

function setEmpresaCompraId($value)
            {
                $this->empresaCompraId = $value;
            }

public function setEmpresa_compra_id_INT($value)
                { 
                    $this->empresaCompraId = $value; 
                }

function setQuantidadeFloat($value)
            {
                $this->quantidadeFloat = $value;
            }

public function setQuantidade_FLOAT($value)
                { 
                    $this->quantidadeFloat = $value; 
                }

function setValorTotalFloat($value)
            {
                $this->valorTotalFloat = $value;
            }

public function setValor_total_FLOAT($value)
                { 
                    $this->valorTotalFloat = $value; 
                }

function setDescontoFloat($value)
            {
                $this->descontoFloat = $value;
            }

public function setDesconto_FLOAT($value)
                { 
                    $this->descontoFloat = $value; 
                }

function setDescricao($value)
            {
                $this->descricao = $value;
            }

function setAtividadeId($value)
            {
                $this->atividadeId = $value;
            }

public function setAtividade_id_INT($value)
                { 
                    $this->atividadeId = $value; 
                }

function setEmpresaId($value)
            {
                $this->empresaId = $value;
            }

public function setEmpresa_id_INT($value)
                { 
                    $this->empresaId = $value; 
                }

function setCorporacaoId($value)
            {
                $this->corporacaoId = $value;
            }

public function setCorporacao_id_INT($value)
                { 
                    $this->corporacaoId = $value; 
                }



public function clear()
        {$this->id = null;
$this->empresaCompraId = null;
if($this->objEmpresa_compra != null) unset($this->objEmpresa_compra);
$this->quantidadeFloat = null;
$this->valorTotalFloat = null;
$this->descontoFloat = null;
$this->descricao = null;
$this->atividadeId = null;
if($this->objAtividade != null) unset($this->objAtividade);
$this->empresaId = null;
if($this->objEmpresa != null) unset($this->objEmpresa);
$this->corporacaoId = null;
if($this->objCorporacao != null) unset($this->objCorporacao);
if(is_array($this->arrEstadosCampos)){
                        unset($this->arrEstadosCampos);
                     $this->arrEstadosCampos = array();
                    }
                }

public function formatarParaSQL()
        {
if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->id)){
$this->id = $this->formatarIntegerParaComandoSQL($this->id);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->id);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->empresaCompraId)){
$this->empresaCompraId = $this->formatarIntegerParaComandoSQL($this->empresaCompraId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->empresaCompraId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->quantidadeFloat)){
$this->quantidadeFloat = $this->formatarFloatParaComandoSQL($this->quantidadeFloat);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->quantidadeFloat);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->valorTotalFloat)){
$this->valorTotalFloat = $this->formatarFloatParaComandoSQL($this->valorTotalFloat);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->valorTotalFloat);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->descontoFloat)){
$this->descontoFloat = $this->formatarFloatParaComandoSQL($this->descontoFloat);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->descontoFloat);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->descricao)){
$this->descricao = $this->formatarStringParaComandoSQL($this->descricao);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->descricao);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->atividadeId)){
$this->atividadeId = $this->formatarIntegerParaComandoSQL($this->atividadeId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->atividadeId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->empresaId)){
$this->empresaId = $this->formatarIntegerParaComandoSQL($this->empresaId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->empresaId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->corporacaoId)){
$this->corporacaoId = $this->formatarIntegerParaComandoSQL($this->corporacaoId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->corporacaoId);
}

}

public function formatarParaExibicao()
        {if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->quantidadeFloat)){
$this->quantidadeFloat = $this->formatarFloatParaExibicao($this->quantidadeFloat);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->quantidadeFloat);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->valorTotalFloat)){
$this->valorTotalFloat = $this->formatarFloatParaExibicao($this->valorTotalFloat);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->valorTotalFloat);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->descontoFloat)){
$this->descontoFloat = $this->formatarFloatParaExibicao($this->descontoFloat);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->descontoFloat);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->descricao)){
$this->descricao = $this->formatarStringParaExibicao($this->descricao);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->descricao);
}

}

function select($id, $idCorporacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }   

            $sql = "SELECT id, empresa_compra_id_INT, quantidade_FLOAT, valor_total_FLOAT, desconto_FLOAT, descricao, atividade_id_INT, empresa_id_INT, corporacao_id_INT FROM empresa_atividade_compra WHERE id = $id AND corporacao_id_INT = $idCorporacao";

            $msg = $this->database->queryMensagem($sql);
            $result = $this->database->result;

            if($msg != null && $msg->erro() )
            { 
            		$this->clear();
            		return $msg;
        	} 
        	else if($this->database->rows() == 0)
        	{
                return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nenhum registro foi encontrado."));
            }
        

            $row = $this->database->fetchArray(MYSQLI_NUM);
            
            		$this->id = $row[0];
		$this->empresaCompraId = $row[1];
		$this->quantidadeFloat = $row[2];
		$this->valorTotalFloat = $row[3];
		$this->descontoFloat = $row[4];
		$this->descricao = $row[5];
		$this->atividadeId = $row[6];
		$this->empresaId = $row[7];

            
        }

public function delete($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (is_null($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }

            $sql = "DELETE FROM empresa_atividade_compra WHERE id= $id AND corporacao_id_INT = $idCorporacao";
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                    && $sincronizar
                        && static::idSistemaTabela() != null
                            && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_REMOVE,
                                null,
                                null,
                                $id,
                                $this->database);
            }

            return $msg;
        }

public function insert($sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null, $opcoes = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            
            
            if (is_null($opcoes))
            {
                $opcoes = $this->opcoesDAO;
            }

            if (!is_numeric($this->id))
            {
                $this->id = "null";
                if ($opcoes == null || $opcoes[Database::OPCAO_GERAR_ID] != false) 
                {
                    $this->id = EXTDAO_Sistema_sequencia::gerarId(static::nomeTabela());                    
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }    
                    else if($msg == null)
                    {
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
                else 
                {
                    $this->id = null;
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao));
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }
                    else if($msg == null)
                    {
                        $this->id = $this->database->getLastInsertId();
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
            }
            else 
            {
                $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }                        
                else if($msg == null)
                {
                   $msg = new Mensagem_token();
                   $msg->mValor = $this->id;
                }
            }
            if($msg == null || $msg->ok())
            {                
                if($sincronizar
                    && static::idSistemaTabela() != null
                    && isset($this->id)){
                    
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_INSERT,
                                null,
                                null,
                                $this->id,
                                $this->database); 
                	}
            } 
            else
            {
                $this->id = null;
            }
            
            return $msg;
            
        }

public function getSQLInsert($idCorporacao){ 

return "INSERT INTO empresa_atividade_compra (id, empresa_compra_id_INT, quantidade_FLOAT, valor_total_FLOAT, desconto_FLOAT, descricao, atividade_id_INT, empresa_id_INT, corporacao_id_INT) VALUES ( $this->id ,  $this->empresaCompraId ,  $this->quantidadeFloat ,  $this->valorTotalFloat ,  $this->descontoFloat ,  $this->descricao ,  $this->atividadeId ,  $this->empresaId ,  $idCorporacao ) "; 

}




        
        public function updateByParameters($id, $objParametros, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao))
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();

            
                
                if (isset($objParametros->empresaCompraId)) 
                {
                    $arrUpdateFields[] = " empresa_compra_id_INT = {$objParametros->empresaCompraId} ";
                }


                
                if (isset($objParametros->quantidadeFloat)) 
                {
                    $arrUpdateFields[] = " quantidade_FLOAT = {$objParametros->quantidadeFloat} ";
                }


                
                if (isset($objParametros->valorTotalFloat)) 
                {
                    $arrUpdateFields[] = " valor_total_FLOAT = {$objParametros->valorTotalFloat} ";
                }


                
                if (isset($objParametros->descontoFloat)) 
                {
                    $arrUpdateFields[] = " desconto_FLOAT = {$objParametros->descontoFloat} ";
                }


                
                if (isset($objParametros->descricao)) 
                {
                    $arrUpdateFields[] = " descricao = {$objParametros->descricao} ";
                }


                
                if (isset($objParametros->atividadeId)) 
                {
                    $arrUpdateFields[] = " atividade_id_INT = {$objParametros->atividadeId} ";
                }


                
                if (isset($objParametros->empresaId)) 
                {
                    $arrUpdateFields[] = " empresa_id_INT = {$objParametros->empresaId} ";
                }



            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE empresa_atividade_compra SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";

            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null)
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function update($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($this->empresaCompraId)) 
                {                                      
                    $arrUpdateFields[] = " empresa_compra_id_INT = {$this->empresaCompraId} ";
                }


                
                if (isset($this->quantidadeFloat)) 
                {                                      
                    $arrUpdateFields[] = " quantidade_FLOAT = {$this->quantidadeFloat} ";
                }


                
                if (isset($this->valorTotalFloat)) 
                {                                      
                    $arrUpdateFields[] = " valor_total_FLOAT = {$this->valorTotalFloat} ";
                }


                
                if (isset($this->descontoFloat)) 
                {                                      
                    $arrUpdateFields[] = " desconto_FLOAT = {$this->descontoFloat} ";
                }


                
                if (isset($this->descricao)) 
                {                                      
                    $arrUpdateFields[] = " descricao = {$this->descricao} ";
                }


                
                if (isset($this->atividadeId)) 
                {                                      
                    $arrUpdateFields[] = " atividade_id_INT = {$this->atividadeId} ";
                }


                
                if (isset($this->empresaId)) 
                {                                      
                    $arrUpdateFields[] = " empresa_id_INT = {$this->empresaId} ";
                }


            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE empresa_atividade_compra SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function updateByInstanceUsingParameters($id, $parameters, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($parameters->empresaCompraId)) 
                {                                      
                    $arrUpdateFields[] = " empresa_compra_id_INT = {$this->empresaCompraId} ";
                }
                
                if (isset($parameters->quantidadeFloat)) 
                {                                      
                    $arrUpdateFields[] = " quantidade_FLOAT = {$this->quantidadeFloat} ";
                }
                
                if (isset($parameters->valorTotalFloat)) 
                {                                      
                    $arrUpdateFields[] = " valor_total_FLOAT = {$this->valorTotalFloat} ";
                }
                
                if (isset($parameters->descontoFloat)) 
                {                                      
                    $arrUpdateFields[] = " desconto_FLOAT = {$this->descontoFloat} ";
                }
                
                if (isset($parameters->descricao)) 
                {                                      
                    $arrUpdateFields[] = " descricao = {$this->descricao} ";
                }
                
                if (isset($parameters->atividadeId)) 
                {                                      
                    $arrUpdateFields[] = " atividade_id_INT = {$this->atividadeId} ";
                }
                
                if (isset($parameters->empresaId)) 
                {                                      
                    $arrUpdateFields[] = " empresa_id_INT = {$this->empresaId} ";
                }
            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE empresa_atividade_compra SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }

}

?>
