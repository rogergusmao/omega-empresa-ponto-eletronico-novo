<?php

class BO_Mapa
{
    public function getDadosUsuarios($idCorporacao = null)
    {
        if ($idCorporacao == null)
        {
            $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
        }

        $q = "SELECT MAX(up.cadastro_SEC + up.cadastro_OFFSEC) AS dataHora, 
                  up.cadastro_SEC AS cadastroSec, 
                  up.cadastro_OFFSEC AS cadastroOffsec,
                  up.latitude_INT AS latitude, 
                  up.longitude_INT AS longitude, 
                  up.velocidade_INT AS velocidade, 
                  u.nome AS nomeUsuario,
                  p.sexo_id_INT AS idSexoUsuario,
                  u.id AS idUsuario,
                  up.fonte_informacao_INT fonteInformacao
                  FROM usuario_posicao up 
                    JOIN usuario u 
                      ON up.usuario_id_INT = u.id
                    LEFT JOIN pessoa_usuario pusu
                      ON pusu.usuario_id_INT=u.id
                    LEFT JOIN pessoa p 
                      ON pusu.pessoa_id_INT=p.id  
                  WHERE up.corporacao_id_INT = {$idCorporacao}
                    AND up.latitude_INT IS NOT NULL 
                    AND up.longitude_INT IS NOT NULL
                    AND up.veiculo_usuario_id_INT IS NULL
                  GROUP BY u.id";

        $db = new Database();
        $msg = $db->queryMensagem($q);
        if (Interface_mensagem::checkErro($msg))
        {
            return $msg;
        }

        return Helper::getResultSetToMatriz($db->result, 1, 0);
    }

    public function getDadosPontosEletronicos($idCorporacao = null)
    {
        if ($idCorporacao == null)
        {
            $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
        }

        $q = "SELECT p.data_SEC AS dataSec, 
                  p.data_OFFSEC AS dataOffsec, 
                  p.latitude_INT AS latitude, 
                  p.longitude_INT AS longitude, 
                  pe.nome AS nomePessoa, 
                  pe.id AS idPessoa,
                  is_entrada_BOOLEAN AS isEntrada
                  FROM ponto p      
                    JOIN pessoa pe
                      ON pe.id = p.pessoa_id_INT       
                    JOIN empresa e 
                      ON e.id = p.empresa_id_INT
                    JOIN profissao pr
                      ON pr.id = p.profissao_id_INT          
                  WHERE p.corporacao_id_INT = {$idCorporacao}
                    AND p.latitude_INT IS NOT NULL 
                    AND p.longitude_INT IS NOT NULL";

        $db = new Database();
        $msg = $db->queryMensagem($q);
        if (Interface_mensagem::checkErro($msg))
        {
            return $msg;
        }

        return Helper::getResultSetToMatriz($db->result, 1, 0);
    }

    public function getDadosEmpresas($idCorporacao = null)
    {
        if ($idCorporacao == null)
        {
            $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
        }

        $q = "SELECT e.latitude_INT AS latitude, 
                     e.longitude_INT AS longitude, 
                     e.nome AS nomeEmpresa, 
                     e.id AS idEmpresa
                  FROM empresa e                     
                  WHERE e.corporacao_id_INT = {$idCorporacao}
                    AND e.latitude_INT IS NOT NULL 
                    AND e.longitude_INT IS NOT NULL";

        $db = new Database();
        $msg = $db->queryMensagem($q);
        if (Interface_mensagem::checkErro($msg))
        {
            return $msg;
        }

        return Helper::getResultSetToMatriz($db->result, 1, 0);
    }

    public function getDadosWifi($idCorporacao = null)
    {
        if ($idCorporacao == null)
        {
            $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
        }

        $q = "SELECT w.ssid ssid
                , wr.data_inicio_SEC dataInicioSec
                , wr.data_inicio_OFFSEC dataInicioOffsec
                , wr.data_fim_SEC dataFimSec
                , wr.data_fim_OFFSEC dataFimOffsec
                , w.empresa_id_INT idEmpresa
                , wr.usuario_id_INT idUsuario
            FROM wifi w  
                JOIN wifi_registro wr 
                    ON w.id = wr.wifi_id_INT
            WHERE w.corporacao_id_INT = {$idCorporacao}
            GROUP BY w.id
            ORDER BY  wr.data_inicio_SEC DESC";

        $db = new Database();
        $msg = $db->queryMensagem($q);
        if (Interface_mensagem::checkErro($msg))
        {
            return $msg;
        }

        $dados = Helper::getResultSetToMatriz($db->result, 1, 0);
        return $dados;
    }
}