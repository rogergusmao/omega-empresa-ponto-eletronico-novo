/*

 Arquivo gerado atrav�s de gerador de c�digo em 07/01/2018 as 16:46:19.
 Para que o arquivo n�o seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

 Tabela correspondente: empresa
 Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

 */

returnObject.getAllComboBoxesDataForEmpresaFilter = function ()
{
    return $http({
        url: baseURLAdm + "/webservice.php?class=EXTDAO_Empresa&jsonOutput=true&action=__getFilterComboBoxesData",
        method: "POST",
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    });

};

returnObject.getEmpresaList = function (listParams)
{
    listParams.listMappingType = Constants.ListMappingType.GRID;
    return $http.post(baseURLAdm + "/webservice.php?class=EXTDAO_Empresa&action=__getList", listParams);
};

returnObject.removeRecords = function (recordsPKs)
{
    return $http.post(baseURLAdm + "/webservice.php?class=EXTDAO_Empresa&action=remove", recordsPKs);
};

returnObject.getListOfOperadoraIdForEmpresaList = function ()
{
    var params = {filterData: null, listMappingType: Constants.ListMappingType.COMBOBOX};
    return $http.post(baseURLAdm + "/webservice.php?class=EXTDAO_Operadora&action=__getList", params);
};
returnObject.getListOfTipoDocumentoIdForEmpresaList = function ()
{
    var params = {filterData: null, listMappingType: Constants.ListMappingType.COMBOBOX};
    return $http.post(baseURLAdm + "/webservice.php?class=EXTDAO_Tipo_documento&action=__getList", params);
};
returnObject.getListOfTipoEmpresaIdForEmpresaList = function ()
{
    var params = {filterData: null, listMappingType: Constants.ListMappingType.COMBOBOX};
    return $http.post(baseURLAdm + "/webservice.php?class=EXTDAO_Tipo_empresa&action=__getList", params);
};
returnObject.getListOfBairroIdForEmpresaList = function ()
{
    var params = {filterData: null, listMappingType: Constants.ListMappingType.COMBOBOX};
    return $http.post(baseURLAdm + "/webservice.php?class=EXTDAO_Bairro&action=__getList", params);
};
returnObject.getListOfCidadeIdForEmpresaList = function ()
{
    var params = {filterData: null, listMappingType: Constants.ListMappingType.COMBOBOX};
    return $http.post(baseURLAdm + "/webservice.php?class=EXTDAO_Cidade&action=__getList", params);
};
                

