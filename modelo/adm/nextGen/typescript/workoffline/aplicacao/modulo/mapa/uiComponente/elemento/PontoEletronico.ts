import { Marcador } from "lib/uiComponente";
import { PontoEletronico as ModelPontoEletronico } from 'aplicacao/modulo/mapa/model'
import { ContextoMapa } from "lib/contexto";
import { ModalFactory } from "aplicacao/modulo/mapa/uiFactory";
import { TipoPontoEletronico } from "aplicacao/comum/geral";

export class PontoEletronico extends Marcador
{
    public modelPontoEletronico :ModelPontoEletronico;
    public constructor(contexto :ContextoMapa, pontoEletronico :ModelPontoEletronico)
    {
        super(contexto, pontoEletronico.latitude, pontoEletronico.longitude, 'ponto');
        this.modelPontoEletronico = pontoEletronico;
    }

    public setDefaultIconeObject()
    {
        switch (this.modelPontoEletronico.tipoPonto)
        {
            case TipoPontoEletronico.PONTO_ENTRADA:
                super.setDefaultIconeObject(this.contexto.getDefaultMarkerFolder() + "map_entrance.png");
                break;

            case TipoPontoEletronico.PONTO_SAIDA:
                super.setDefaultIconeObject(this.contexto.getDefaultMarkerFolder() + "map_exit.png");
                break;
        }
    }

    public addDefaultClickListener()
    {
        super.addClickListener(() =>
        {
            var janelaModal = ModalFactory.getModalDetalhesPontoEletronico(this.contexto, this.modelPontoEletronico);      
            janelaModal.handleShow();          
        });
    }

}
