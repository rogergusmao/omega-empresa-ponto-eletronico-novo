<?php


        /*

        Arquivo gerado através de gerador de código em 12/05/2018 as 14:50:56.
        Para que o arquivo não seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

        Tabela correspondente: sistema_registro_sincronizador
        Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

        */
        


class DAO_Sistema_registro_sincronizador extends Generic_DAO
                        {
                            const NOME_ENTIDADE = "sistema_registro_sincronizador";

                            public static $nomeTabela = null;
                            public static $idSistemaTabela = false;
                            public static $databaseFieldsRelatedAttributes = null;
	                        public static $databaseFieldTypes = null;
	                        public static $databaseFieldNames = null;
	                        public static $listAliasTypes = null;
	                        public static $listAliasRelatedAttributes = null;
	                        
	                        public $id;
public $sistemaTabelaId;
public $idTabelaInt;
public $tipoOperacaoBancoId;
public $usuarioId;
public $corporacaoId;
public $isFromAndroidBoolean;
public $imei;
public $sistemaProdutoInt;
public $sistemaProjetosVersaoInt;

public $labelId;
public $labelSistemaTabelaId;
public $labelIdTabelaInt;
public $labelTipoOperacaoBancoId;
public $labelUsuarioId;
public $labelCorporacaoId;
public $labelIsFromAndroidBoolean;
public $labelImei;
public $labelSistemaProdutoInt;
public $labelSistemaProjetosVersaoInt;

public static $arrPK = array("id");

public function __construct($configDAO = null)
        {
            parent::__construct($configDAO);
            static::__setStaticValues($this->database);           
        }

public static function idSistemaTabela($databaseObject = null){
            if(!(static::$idSistemaTabela))
            {
                static::$idSistemaTabela = EXTDAO_Sistema_tabela::getIdDaSistemaTabela(static::nomeTabela(), $databaseObject);
            }
            return static::$idSistemaTabela;
        }

public function getIdSistemaTabela(){
            return static::idSistemaTabela();
        }

public static function nomeTabela(){
            if(is_null(static::$nomeTabela))
            {
                static::$nomeTabela = "sistema_registro_sincronizador";
            }
            return static::$nomeTabela;
        }

public function getNomeTabela(){
            return static::nomeTabela();
        }

public static function __setStaticValues($databaseObject = null)
                   {    
                        static::nomeTabela();
                        
                        static::idSistemaTabela($databaseObject);
                   
                        static::setDatabaseFieldNames();
                        static::setDatabaseFieldTypes();
                        static::setDatabaseFieldsRelatedAttributeNames();
                        
                        static::setListAliasTypes();
                        static::setListAliasRelatedAttributes();
                   }

public function getExpressaoLabel()
                   {
                        return "{id}";
                   }

public static function setDatabaseFieldNames()
        {
            if(is_null(static::$databaseFieldNames))
            {
                static::$databaseFieldNames = new stdClass();
                static::$databaseFieldNames->id = "id";
static::$databaseFieldNames->sistemaTabelaId = "sistema_tabela_id_INT";
static::$databaseFieldNames->idTabelaInt = "id_tabela_INT";
static::$databaseFieldNames->tipoOperacaoBancoId = "tipo_operacao_banco_id_INT";
static::$databaseFieldNames->usuarioId = "usuario_id_INT";
static::$databaseFieldNames->corporacaoId = "corporacao_id_INT";
static::$databaseFieldNames->isFromAndroidBoolean = "is_from_android_BOOLEAN";
static::$databaseFieldNames->imei = "imei";
static::$databaseFieldNames->sistemaProdutoInt = "sistema_produto_INT";
static::$databaseFieldNames->sistemaProjetosVersaoInt = "sistema_projetos_versao_INT";

            }
                      
        }

public static function setDatabaseFieldTypes()
        {
            if(is_null(static::$databaseFieldTypes))
            {
                static::$databaseFieldTypes = new stdClass();
                static::$databaseFieldTypes->id = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->sistemaTabelaId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->idTabelaInt = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->tipoOperacaoBancoId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->usuarioId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->corporacaoId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->isFromAndroidBoolean = static::TIPO_VARIAVEL_BOOLEAN;
static::$databaseFieldTypes->imei = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->sistemaProdutoInt = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->sistemaProjetosVersaoInt = static::TIPO_VARIAVEL_INTEGER;

            }
            
        }

public static function setDatabaseFieldsRelatedAttributeNames()
        {
            if(is_null(static::$databaseFieldsRelatedAttributes))
            {
                static::$databaseFieldsRelatedAttributes = new stdClass();
                static::$databaseFieldsRelatedAttributes->id = "id";
static::$databaseFieldsRelatedAttributes->sistema_tabela_id_INT = "sistemaTabelaId";
static::$databaseFieldsRelatedAttributes->id_tabela_INT = "idTabelaInt";
static::$databaseFieldsRelatedAttributes->tipo_operacao_banco_id_INT = "tipoOperacaoBancoId";
static::$databaseFieldsRelatedAttributes->usuario_id_INT = "usuarioId";
static::$databaseFieldsRelatedAttributes->corporacao_id_INT = "corporacaoId";
static::$databaseFieldsRelatedAttributes->is_from_android_BOOLEAN = "isFromAndroidBoolean";
static::$databaseFieldsRelatedAttributes->imei = "imei";
static::$databaseFieldsRelatedAttributes->sistema_produto_INT = "sistemaProdutoInt";
static::$databaseFieldsRelatedAttributes->sistema_projetos_versao_INT = "sistemaProjetosVersaoInt";

            }           
        }

public function setAllFieldsToHumanFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["sistema_tabela_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["id_tabela_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["tipo_operacao_banco_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["usuario_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["is_from_android_BOOLEAN"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["imei"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["sistema_produto_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["sistema_projetos_versao_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
}

public function setAllFieldsToDatabaseFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["sistema_tabela_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["id_tabela_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["tipo_operacao_banco_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["usuario_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["is_from_android_BOOLEAN"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["imei"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["sistema_produto_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["sistema_projetos_versao_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
}

  public function getFkObjSistema_tabela() 
                {
                    if($this->objSistema_tabela == null)
                    {                        
                        $this->objSistema_tabela = new EXTDAO_Sistema_tabela_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getSistema_tabela_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objSistema_tabela->clear();
                    }
                    elseif($this->objSistema_tabela->getId() != $idFK)
                    {
                        $this->objSistema_tabela->select($idFK);
                    }
                    return $this->objSistema_tabela;
                }
  public function getFkObjTipo_operacao_banco() 
                {
                    if($this->objTipo_operacao_banco == null)
                    {                        
                        $this->objTipo_operacao_banco = new EXTDAO_Tipo_operacao_banco_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getTipo_operacao_banco_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objTipo_operacao_banco->clear();
                    }
                    elseif($this->objTipo_operacao_banco->getId() != $idFK)
                    {
                        $this->objTipo_operacao_banco->select($idFK);
                    }
                    return $this->objTipo_operacao_banco;
                }
  public function getFkObjUsuario() 
                {
                    if($this->objUsuario == null)
                    {                        
                        $this->objUsuario = new EXTDAO_Usuario_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getUsuario_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objUsuario->clear();
                    }
                    elseif($this->objUsuario->getId() != $idFK)
                    {
                        $this->objUsuario->select($idFK);
                    }
                    return $this->objUsuario;
                }
  public function getFkObjCorporacao() 
                {
                    if($this->objCorporacao == null)
                    {                        
                        $this->objCorporacao = new EXTDAO_Corporacao_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getCorporacao_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objCorporacao->clear();
                    }
                    elseif($this->objCorporacao->getId() != $idFK)
                    {
                        $this->objCorporacao->select($idFK);
                    }
                    return $this->objCorporacao;
                }


public function setLabels()
        {$this->labelId = "id";
$this->labelSistemaTabelaId = "sistematabelaidINT";
$this->labelIdTabelaInt = "idtabelaINT";
$this->labelTipoOperacaoBancoId = "tipooperacaobancoidINT";
$this->labelUsuarioId = "usuarioidINT";
$this->labelCorporacaoId = "corporacaoidINT";
$this->labelIsFromAndroidBoolean = "isfromandroidBOOLEAN";
$this->labelImei = "imei";
$this->labelSistemaProdutoInt = "sistemaprodutoINT";
$this->labelSistemaProjetosVersaoInt = "sistemaprojetosversaoINT";
}

public function __actionAdd($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                $this->setByObject($parameters);
                $this->setDataCadastroAndOffsetInSecondsFromUser($parameters);
                
                $this->formatarParaSQL();
                $msg = $this->insert(true);
                
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                return new Mensagem(null, I18N::getExpression("Sistema registro sincronizador adicionado com sucesso."));
                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionEdit($parameters = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try {

                $this->setByObject($parameters);
                $this->setAllFieldsToHumanFormat();
                
                $this->formatarParaSQL();
                $msg = $this->updateByInstanceUsingParameters($this->id, $parameters, true);
                if($msg != null && $msg->erro())
                {
                     return $msg;
                }
        
                return new Mensagem(null, I18N::getExpression("Sistema registro sincronizador editado com sucesso."));
            }
            catch(Exception $ex)
            {                
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionRemove($parameters = null)
        {
            $successulRemovalCount = 0;
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                foreach($parameters as $record)
                {
                    if(is_numeric($record->id))
                    {
                        $msg = $this->delete($record->id, true);
                        if($msg == null || $msg->ok())
                        {
                            $successulRemovalCount++;
                        }
                    }
                }
                
                if($successulRemovalCount > 0)
                {
                    $successMessage = count($successulRemovalCount) > 1 ? I18N::getExpression("{0} Sistema registro sincronizador foram removidos com sucesso.", $successulRemovalCount) : I18N::getExpression("Sistema registro sincronizador removido com sucesso.");
                    return new Mensagem(null, $successMessage);
                }
                else
                {
                    $errorMessage = count($parameters) > 1 ? I18N::getExpression("Falha ao remover Sistema registro sincronizador.") : I18N::getExpression("Falha ao remover Sistema registro sincronizador.");
                    return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, $errorMessage);                    
                }
            }
            catch (Exception $ex) 
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }

public function __getRecord($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }
            
            try
            {            
                $returnObject = new stdClass();            
                $whereClause = static::getWhereClauseForFilter($parameters->filterParameters);
                $query = "SELECT  id, sistema_tabela_id_INT, id_tabela_INT, tipo_operacao_banco_id_INT, usuario_id_INT, corporacao_id_INT, is_from_android_BOOLEAN, imei, sistema_produto_INT, sistema_projetos_versao_INT FROM sistema_registro_sincronizador {$whereClause}";
    
                $msg = $this->database->queryMensagem($query);                
                if ($msg == null)
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Não foi encontrado nenhum registro"));
                    }                    
                    $resultSet = $this->database->result;
                    $resultObject = static::mapResultSetDataToRecord($resultSet);
                    
                    $returnObject->formData = $resultObject;
                    $returnObject->comboBoxesData = null;
                    if($parameters->loadComboBoxesData)
                    {
                        $this->setByObject($resultObject);
                        $returnObject->comboBoxesData =  $this->getFormComboBoxesData();
                    }                    
    
                    
    
                    return new Mensagem_generica($returnObject);
                } 
                else 
                {
                    return $msg;
                }
                
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }


        
        public function __getFilterComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
                
        public function __getFormComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
        
        protected function getFormComboBoxesData()
        {
            $comboBoxesData = new stdClass();
            $listParameters = new stdClass();
            $listParameters->listMappingType = static::LIST_MAPPING_TYPE_COMBOBOX;
            
            
                    
                    $objSistemaTabela = new EXTDAO_Sistema_tabela();
                    $comboBoxesData->fieldSistemaTabelaId = $objSistemaTabela->__getList($listParameters);
                    
                    $objUsuario = new EXTDAO_Usuario();
                    $comboBoxesData->fieldUsuarioId = $objUsuario->__getList($listParameters);

            return $comboBoxesData;
        }
        
        


        
        public static function setListAliasTypes()
        {
            if(is_null(static::$listAliasTypes))
            {
                static::$listAliasTypes = new stdClass();
                static::$listAliasTypes->sistema_tabela__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->usuario__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->sistema_registro_sincronizador__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->sistema_registro_sincronizador__atributos_chave_unica = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->sistema_registro_sincronizador__id_tabela_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->sistema_registro_sincronizador__tipo_operacao_banco_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->sistema_registro_sincronizador__cadastro_OFFSEC = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$listAliasTypes->sistema_registro_sincronizador__is_from_android_BOOLEAN = static::TIPO_VARIAVEL_BOOLEAN;
static::$listAliasTypes->sistema_registro_sincronizador__imei = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->sistema_registro_sincronizador__sistema_produto_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->sistema_registro_sincronizador__sistema_projetos_versao_INT = static::TIPO_VARIAVEL_INTEGER;
            }        
        }
        
        public static function setListAliasRelatedAttributes()
        {
            if(is_null(static::$listAliasRelatedAttributes))
            {
                static::$listAliasRelatedAttributes = new stdClass();
                static::$listAliasRelatedAttributes->sistema_tabela__id = "sistemaTabelaId";
static::$listAliasRelatedAttributes->usuario__id = "usuarioId";
static::$listAliasRelatedAttributes->sistema_registro_sincronizador__id = "id";
static::$listAliasRelatedAttributes->sistema_registro_sincronizador__atributos_chave_unica = "atributosChaveUnica";
static::$listAliasRelatedAttributes->sistema_registro_sincronizador__id_tabela_INT = "idTabelaInt";
static::$listAliasRelatedAttributes->sistema_registro_sincronizador__tipo_operacao_banco_id_INT = "tipoOperacaoBancoId";
static::$listAliasRelatedAttributes->sistema_registro_sincronizador__cadastro_OFFSEC = "cadastroOffsec";
static::$listAliasRelatedAttributes->sistema_registro_sincronizador__is_from_android_BOOLEAN = "isFromAndroidBoolean";
static::$listAliasRelatedAttributes->sistema_registro_sincronizador__imei = "imei";
static::$listAliasRelatedAttributes->sistema_registro_sincronizador__sistema_produto_INT = "sistemaProdutoInt";
static::$listAliasRelatedAttributes->sistema_registro_sincronizador__sistema_projetos_versao_INT = "sistemaProjetosVersaoInt";
            }         
        }

        public function __getList($parameters = null, $idCorporacao = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try
            {
                $filterParameters = new stdClass();
                if(isset($parameters->filterParameters))
                {
                    $filterParameters = $parameters->filterParameters;
                }
                                    
                            
            if(is_null($idCorporacao))
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            ListParameters::addFilterParameterToObject($filterParameters, "corporacaoId", $idCorporacao);           
                
                
                $sortingParameters = null;
                if(isset($parameters->sortingParameters))
                {
                    $sortingParameters = $parameters->sortingParameters;
                }
                
                $paginationParameters = null;
                if(isset($parameters->paginationParameters))
                {
                    $paginationParameters = $parameters->paginationParameters;
                }
                
                $listMappingType = static::getListMappingType($parameters->listMappingType);
                $mainTableAlias = "srs";

                $whereClause = static::getWhereClauseForFilter($filterParameters, $mainTableAlias);
                $orderByClause = static::getOrderByClauseForFilter($sortingParameters, $mainTableAlias);
                $limitClause = static::getLimitClauseForFilter($paginationParameters);

                $queryWithoutLimit = "SELECT srs.id FROM sistema_registro_sincronizador srs {$whereClause}";
                $query = "SELECT st.id AS sistema_tabela__id, u.id AS usuario__id, srs.id AS sistema_registro_sincronizador__id, srs.atributos_chave_unica AS sistema_registro_sincronizador__atributos_chave_unica, srs.id_tabela_INT AS sistema_registro_sincronizador__id_tabela_INT, srs.tipo_operacao_banco_id_INT AS sistema_registro_sincronizador__tipo_operacao_banco_id_INT, srs.cadastro_OFFSEC AS sistema_registro_sincronizador__cadastro_OFFSEC, srs.is_from_android_BOOLEAN AS sistema_registro_sincronizador__is_from_android_BOOLEAN, srs.imei AS sistema_registro_sincronizador__imei, srs.sistema_produto_INT AS sistema_registro_sincronizador__sistema_produto_INT, srs.sistema_projetos_versao_INT AS sistema_registro_sincronizador__sistema_projetos_versao_INT FROM sistema_registro_sincronizador srs LEFT JOIN sistema_tabela st ON st.id = srs.sistema_tabela_id_INT LEFT JOIN usuario u ON u.id = srs.usuario_id_INT {$whereClause} {$orderByClause} {$limitClause}";

                $msg = $this->database->queryMensagem($queryWithoutLimit);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                
                $totalNumberOfRecords = $this->database->rows();
                if($totalNumberOfRecords == 0)
                {
                    return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                }                        
                        
                $msg = $this->database->queryMensagem($query);
                
                //operacao realizaca com sucesso
                if ($msg == null) 
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                    }                        
                
                    $resultSet = $this->database->result;

                    $objReturn = new stdClass();
                    $objReturn->dataSet = static::mapResultSetDataToList($resultSet, $listMappingType, static::$listAliasTypes);

                    if(!Helper::isNullOrEmpty($paginationParameters->recordsPerPage) && !Helper::isNullOrEmpty($paginationParameters->currentPage))
                    {
                        $lastPage = ceil($totalNumberOfRecords / $paginationParameters->recordsPerPage);
                        $objReturn->paginationParameters = new PaginationParams($paginationParameters->recordsPerPage, $paginationParameters->currentPage, $lastPage);
                    }
                    
                    return new Mensagem_generica($objReturn);
                } 
                else
                {
                    return $msg;
                }                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }



public function getId()
            {
                return $this->id;
            }

public function getSistemaTabelaId()
            {
                return $this->sistemaTabelaId;
            }

public function getSistema_tabela_id_INT()
                {
                    return $this->sistemaTabelaId;
                }

public function getIdTabelaInt()
            {
                return $this->idTabelaInt;
            }

public function getId_tabela_INT()
                {
                    return $this->idTabelaInt;
                }

public function getTipoOperacaoBancoId()
            {
                return $this->tipoOperacaoBancoId;
            }

public function getTipo_operacao_banco_id_INT()
                {
                    return $this->tipoOperacaoBancoId;
                }

public function getUsuarioId()
            {
                return $this->usuarioId;
            }

public function getUsuario_id_INT()
                {
                    return $this->usuarioId;
                }

public function getCorporacaoId()
            {
                return $this->corporacaoId;
            }

public function getCorporacao_id_INT()
                {
                    return $this->corporacaoId;
                }

public function getIsFromAndroidBoolean()
            {
                return $this->isFromAndroidBoolean;
            }

public function getIs_from_android_BOOLEAN()
                {
                    return $this->isFromAndroidBoolean;
                }

public function getImei()
            {
                return $this->imei;
            }

public function getSistemaProdutoInt()
            {
                return $this->sistemaProdutoInt;
            }

public function getSistema_produto_INT()
                {
                    return $this->sistemaProdutoInt;
                }

public function getSistemaProjetosVersaoInt()
            {
                return $this->sistemaProjetosVersaoInt;
            }

public function getSistema_projetos_versao_INT()
                {
                    return $this->sistemaProjetosVersaoInt;
                }



function setId($value)
            {
                $this->id = $value;
            }

function setSistemaTabelaId($value)
            {
                $this->sistemaTabelaId = $value;
            }

public function setSistema_tabela_id_INT($value)
                { 
                    $this->sistemaTabelaId = $value; 
                }

function setIdTabelaInt($value)
            {
                $this->idTabelaInt = $value;
            }

public function setId_tabela_INT($value)
                { 
                    $this->idTabelaInt = $value; 
                }

function setTipoOperacaoBancoId($value)
            {
                $this->tipoOperacaoBancoId = $value;
            }

public function setTipo_operacao_banco_id_INT($value)
                { 
                    $this->tipoOperacaoBancoId = $value; 
                }

function setUsuarioId($value)
            {
                $this->usuarioId = $value;
            }

public function setUsuario_id_INT($value)
                { 
                    $this->usuarioId = $value; 
                }

function setCorporacaoId($value)
            {
                $this->corporacaoId = $value;
            }

public function setCorporacao_id_INT($value)
                { 
                    $this->corporacaoId = $value; 
                }

function setIsFromAndroidBoolean($value)
            {
                $this->isFromAndroidBoolean = $value;
            }

public function setIs_from_android_BOOLEAN($value)
                { 
                    $this->isFromAndroidBoolean = $value; 
                }

function setImei($value)
            {
                $this->imei = $value;
            }

function setSistemaProdutoInt($value)
            {
                $this->sistemaProdutoInt = $value;
            }

public function setSistema_produto_INT($value)
                { 
                    $this->sistemaProdutoInt = $value; 
                }

function setSistemaProjetosVersaoInt($value)
            {
                $this->sistemaProjetosVersaoInt = $value;
            }

public function setSistema_projetos_versao_INT($value)
                { 
                    $this->sistemaProjetosVersaoInt = $value; 
                }



public function clear()
        {$this->id = null;
$this->sistemaTabelaId = null;
if($this->objSistema_tabela != null) unset($this->objSistema_tabela);
$this->idTabelaInt = null;
$this->tipoOperacaoBancoId = null;
if($this->objTipo_operacao_banco != null) unset($this->objTipo_operacao_banco);
$this->usuarioId = null;
if($this->objUsuario != null) unset($this->objUsuario);
$this->corporacaoId = null;
if($this->objCorporacao != null) unset($this->objCorporacao);
$this->isFromAndroidBoolean = null;
$this->imei = null;
$this->sistemaProdutoInt = null;
$this->sistemaProjetosVersaoInt = null;
if(is_array($this->arrEstadosCampos)){
                        unset($this->arrEstadosCampos);
                     $this->arrEstadosCampos = array();
                    }
                }

public function formatarParaSQL()
        {
if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->id)){
$this->id = $this->formatarIntegerParaComandoSQL($this->id);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->id);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->sistemaTabelaId)){
$this->sistemaTabelaId = $this->formatarIntegerParaComandoSQL($this->sistemaTabelaId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->sistemaTabelaId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->idTabelaInt)){
$this->idTabelaInt = $this->formatarIntegerParaComandoSQL($this->idTabelaInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->idTabelaInt);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->tipoOperacaoBancoId)){
$this->tipoOperacaoBancoId = $this->formatarIntegerParaComandoSQL($this->tipoOperacaoBancoId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->tipoOperacaoBancoId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->usuarioId)){
$this->usuarioId = $this->formatarIntegerParaComandoSQL($this->usuarioId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->usuarioId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->corporacaoId)){
$this->corporacaoId = $this->formatarIntegerParaComandoSQL($this->corporacaoId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->corporacaoId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->isFromAndroidBoolean)){
$this->isFromAndroidBoolean = $this->formatarBooleanParaComandoSQL($this->isFromAndroidBoolean);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->isFromAndroidBoolean);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->imei)){
$this->imei = $this->formatarStringParaComandoSQL($this->imei);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->imei);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->sistemaProdutoInt)){
$this->sistemaProdutoInt = $this->formatarIntegerParaComandoSQL($this->sistemaProdutoInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->sistemaProdutoInt);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->sistemaProjetosVersaoInt)){
$this->sistemaProjetosVersaoInt = $this->formatarIntegerParaComandoSQL($this->sistemaProjetosVersaoInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->sistemaProjetosVersaoInt);
}

}

public function formatarParaExibicao()
        {if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->isFromAndroidBoolean)){
$this->isFromAndroidBoolean = $this->formatarBooleanParaExibicao($this->isFromAndroidBoolean);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->isFromAndroidBoolean);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->imei)){
$this->imei = $this->formatarStringParaExibicao($this->imei);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->imei);
}

}

function select($id, $idCorporacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }   

            $sql = "SELECT id, sistema_tabela_id_INT, id_tabela_INT, tipo_operacao_banco_id_INT, usuario_id_INT, corporacao_id_INT, is_from_android_BOOLEAN, imei, sistema_produto_INT, sistema_projetos_versao_INT FROM sistema_registro_sincronizador WHERE id = $id AND corporacao_id_INT = $idCorporacao";

            $msg = $this->database->queryMensagem($sql);
            $result = $this->database->result;

            if($msg != null && $msg->erro() )
            { 
            		$this->clear();
            		return $msg;
        	} 
        	else if($this->database->rows() == 0)
        	{
                return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nenhum registro foi encontrado."));
            }
        

            $row = $this->database->fetchArray(MYSQLI_NUM);
            
            		$this->id = $row[0];
		$this->sistemaTabelaId = $row[1];
		$this->idTabelaInt = $row[2];
		$this->tipoOperacaoBancoId = $row[3];
		$this->usuarioId = $row[4];
		$this->isFromAndroidBoolean = $row[6];
		$this->imei = $row[7];
		$this->sistemaProdutoInt = $row[8];
		$this->sistemaProjetosVersaoInt = $row[9];

            
        }

public function delete($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (is_null($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }

            $sql = "DELETE FROM sistema_registro_sincronizador WHERE id= $id AND corporacao_id_INT = $idCorporacao";
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                    && $sincronizar
                        && static::idSistemaTabela() != null
                            && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_REMOVE,
                                null,
                                null,
                                $id,
                                $this->database);
            }

            return $msg;
        }

public function insert($sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null, $opcoes = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            
            
            if (is_null($opcoes))
            {
                $opcoes = $this->opcoesDAO;
            }

            if (!is_numeric($this->id))
            {
                $this->id = "null";
                if ($opcoes == null || $opcoes[Database::OPCAO_GERAR_ID] != false) 
                {
                    $this->id = EXTDAO_Sistema_sequencia::gerarId(static::nomeTabela());                    
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }    
                    else if($msg == null)
                    {
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
                else 
                {
                    $this->id = null;
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao));
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }
                    else if($msg == null)
                    {
                        $this->id = $this->database->getLastInsertId();
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
            }
            else 
            {
                $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }                        
                else if($msg == null)
                {
                   $msg = new Mensagem_token();
                   $msg->mValor = $this->id;
                }
            }
            if($msg == null || $msg->ok())
            {                
                if($sincronizar
                    && static::idSistemaTabela() != null
                    && isset($this->id)){
                    
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_INSERT,
                                null,
                                null,
                                $this->id,
                                $this->database); 
                	}
            } 
            else
            {
                $this->id = null;
            }
            
            return $msg;
            
        }

public function getSQLInsert($idCorporacao){ 

return "INSERT INTO sistema_registro_sincronizador (id, sistema_tabela_id_INT, id_tabela_INT, tipo_operacao_banco_id_INT, usuario_id_INT, corporacao_id_INT, is_from_android_BOOLEAN, imei, sistema_produto_INT, sistema_projetos_versao_INT) VALUES ( $this->id ,  $this->sistemaTabelaId ,  $this->idTabelaInt ,  $this->tipoOperacaoBancoId ,  $this->usuarioId ,  $idCorporacao ,  $this->isFromAndroidBoolean ,  $this->imei ,  $this->sistemaProdutoInt ,  $this->sistemaProjetosVersaoInt ) "; 

}




        
        public function updateByParameters($id, $objParametros, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao))
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();

            
                
                if (isset($objParametros->sistemaTabelaId)) 
                {
                    $arrUpdateFields[] = " sistema_tabela_id_INT = {$objParametros->sistemaTabelaId} ";
                }


                
                if (isset($objParametros->idTabelaInt)) 
                {
                    $arrUpdateFields[] = " id_tabela_INT = {$objParametros->idTabelaInt} ";
                }


                
                if (isset($objParametros->tipoOperacaoBancoId)) 
                {
                    $arrUpdateFields[] = " tipo_operacao_banco_id_INT = {$objParametros->tipoOperacaoBancoId} ";
                }


                
                if (isset($objParametros->usuarioId)) 
                {
                    $arrUpdateFields[] = " usuario_id_INT = {$objParametros->usuarioId} ";
                }


                
                if (isset($objParametros->isFromAndroidBoolean)) 
                {
                    $arrUpdateFields[] = " is_from_android_BOOLEAN = {$objParametros->isFromAndroidBoolean} ";
                }


                
                if (isset($objParametros->imei)) 
                {
                    $arrUpdateFields[] = " imei = {$objParametros->imei} ";
                }


                
                if (isset($objParametros->sistemaProdutoInt)) 
                {
                    $arrUpdateFields[] = " sistema_produto_INT = {$objParametros->sistemaProdutoInt} ";
                }


                
                if (isset($objParametros->sistemaProjetosVersaoInt)) 
                {
                    $arrUpdateFields[] = " sistema_projetos_versao_INT = {$objParametros->sistemaProjetosVersaoInt} ";
                }



            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE sistema_registro_sincronizador SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";

            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null)
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function update($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($this->sistemaTabelaId)) 
                {                                      
                    $arrUpdateFields[] = " sistema_tabela_id_INT = {$this->sistemaTabelaId} ";
                }


                
                if (isset($this->idTabelaInt)) 
                {                                      
                    $arrUpdateFields[] = " id_tabela_INT = {$this->idTabelaInt} ";
                }


                
                if (isset($this->tipoOperacaoBancoId)) 
                {                                      
                    $arrUpdateFields[] = " tipo_operacao_banco_id_INT = {$this->tipoOperacaoBancoId} ";
                }


                
                if (isset($this->usuarioId)) 
                {                                      
                    $arrUpdateFields[] = " usuario_id_INT = {$this->usuarioId} ";
                }


                
                if (isset($this->isFromAndroidBoolean)) 
                {                                      
                    $arrUpdateFields[] = " is_from_android_BOOLEAN = {$this->isFromAndroidBoolean} ";
                }


                
                if (isset($this->imei)) 
                {                                      
                    $arrUpdateFields[] = " imei = {$this->imei} ";
                }


                
                if (isset($this->sistemaProdutoInt)) 
                {                                      
                    $arrUpdateFields[] = " sistema_produto_INT = {$this->sistemaProdutoInt} ";
                }


                
                if (isset($this->sistemaProjetosVersaoInt)) 
                {                                      
                    $arrUpdateFields[] = " sistema_projetos_versao_INT = {$this->sistemaProjetosVersaoInt} ";
                }


            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE sistema_registro_sincronizador SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function updateByInstanceUsingParameters($id, $parameters, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($parameters->sistemaTabelaId)) 
                {                                      
                    $arrUpdateFields[] = " sistema_tabela_id_INT = {$this->sistemaTabelaId} ";
                }
                
                if (isset($parameters->idTabelaInt)) 
                {                                      
                    $arrUpdateFields[] = " id_tabela_INT = {$this->idTabelaInt} ";
                }
                
                if (isset($parameters->tipoOperacaoBancoId)) 
                {                                      
                    $arrUpdateFields[] = " tipo_operacao_banco_id_INT = {$this->tipoOperacaoBancoId} ";
                }
                
                if (isset($parameters->usuarioId)) 
                {                                      
                    $arrUpdateFields[] = " usuario_id_INT = {$this->usuarioId} ";
                }
                
                if (isset($parameters->isFromAndroidBoolean)) 
                {                                      
                    $arrUpdateFields[] = " is_from_android_BOOLEAN = {$this->isFromAndroidBoolean} ";
                }
                
                if (isset($parameters->imei)) 
                {                                      
                    $arrUpdateFields[] = " imei = {$this->imei} ";
                }
                
                if (isset($parameters->sistemaProdutoInt)) 
                {                                      
                    $arrUpdateFields[] = " sistema_produto_INT = {$this->sistemaProdutoInt} ";
                }
                
                if (isset($parameters->sistemaProjetosVersaoInt)) 
                {                                      
                    $arrUpdateFields[] = " sistema_projetos_versao_INT = {$this->sistemaProjetosVersaoInt} ";
                }
            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE sistema_registro_sincronizador SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }

}

?>
