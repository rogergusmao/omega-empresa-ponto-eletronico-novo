import { Time } from "lib/geral";

export class PeriodoTempo
{
    public horaInicial: Time;
    public horaFinal: Time;

    public constructor(horaInicial: Time, horaFinal: Time)
    {
        this.horaInicial = horaInicial;
        this.horaFinal = horaFinal;
    }

}