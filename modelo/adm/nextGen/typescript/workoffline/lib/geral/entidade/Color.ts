import { Helper } from 'lib/geral';

export class Color
{
    public redChannel: number;
    public greenChannel: number;
    public blueChannel: number;
    public alphaChannel?: number;

    public constructor(hexValue :string);
    public constructor(redChannel :number | string, greenChannel :number, blueChannel :number);
    public constructor(redChannel :number | string, greenChannel :number, blueChannel :number, alphaChannel :number);
    public constructor(redChannelOrHex :number | string, greenChannel? :number, blueChannel? :number, alphaChannel? :number)
    {
        if((redChannelOrHex as any) instanceof String)
        {
            var channels = Color.convertFromHex(redChannelOrHex as string);

            if(channels != null)
            {
                this.redChannel = channels.r;
                this.greenChannel = channels.g;
                this.blueChannel = channels.b;
                this.alphaChannel = channels.a;
            }
        }
        else
        {
            this.redChannel = redChannelOrHex as number;
            this.greenChannel = greenChannel;
            this.blueChannel = blueChannel;
            this.alphaChannel = alphaChannel || null;
        }

    }

    public getHexValue() :string
    {
        if(this.alphaChannel != null)
        {
            return Color.convertToHex(this.redChannel, this.greenChannel, this.blueChannel, this.alphaChannel);
        }
        else
        {
            return Color.convertToHex(this.redChannel, this.greenChannel, this.blueChannel);
        }
    }

    private static convertToHex(r :number, g :number, b :number, a? :number) :string
    {
        let pieces = new Array<string>();

        let redHex = r.toString(16);
        pieces.push(redHex.length == 1 ? "0" + redHex : redHex);

        let greenHex = g.toString(16);
        pieces.push(greenHex.length == 1 ? "0" + greenHex : greenHex);

        let blueHex = b.toString(16);
        pieces.push(blueHex.length == 1 ? "0" + blueHex : blueHex);

        if(a != null)
        {
            let alphaHex = a.toString(16);
            pieces.push(alphaHex.length == 1 ? "0" + alphaHex : alphaHex);
        }

        return `#${pieces.join('')}`;

    }

    private static convertFromHex(hexValue :string) : { r: number, g: number, b: number, a?: number}
    {
        if(Helper.isNullOrEmpty(hexValue))
        {
            return null;
        }
        else
        {
            if(!hexValue.startsWith('#'))
            {
                hexValue = `#${hexValue}`;
            }

            if(hexValue.length == 5)
            {
                //exemplo: "03FC" para "0033FFCC"
                var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])([a-f\d])$/i;
                hexValue = hexValue.replace(shorthandRegex, function(m, r, g, b, a) 
                {
                    return r + r + g + g + b + b + a + a;
                });
            }
            else if(hexValue.length == 4)
            {
                //exemplo: "03FC" para "0033FFCC"
                var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
                hexValue = hexValue.replace(shorthandRegex, function(m, r, g, b) 
                {
                    return r + r + g + g + b + b;
                });
            }

            if(hexValue.length == 9)
            {
                var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hexValue);
                return result ? {
                    r: parseInt(result[1], 16),
                    g: parseInt(result[2], 16),
                    b: parseInt(result[3], 16),
                    a: parseInt(result[4], 16),
                } : null;
            }
            else if(hexValue.length == 7)
            {
                var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hexValue);
                return result ? {
                    r: parseInt(result[1], 16),
                    g: parseInt(result[2], 16),
                    b: parseInt(result[3], 16),
                    a: null
                } : null;
            }
            else
            {
                return null;
            }

        }
        
    }

}