/*

 Arquivo gerado atrav�s de gerador de c�digo em 07/01/2018 as 15:54:08.
 Para que o arquivo n�o seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

 Tabela correspondente: bairro
 Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

 */

returnObject.getBairro = function (filterParameters)
{

    var parameters = {
        filterParameters: filterParameters,
        loadComboBoxesData: true
    };

    return $http({
        url: baseURLAdm + "/webservice.php?class=EXTDAO_Bairro&action=__getRecord",
        method: "POST",
        data: parameters,
        headers: {
            'Content-Type': 'application/json'
        }
    });

};

returnObject.addBairro = function (formData)
{

    return $http({
        url: baseURLAdm + "/webservice.php?class=EXTDAO_Bairro&jsonOutput=true&action=add",
        method: "POST",
        data: formData,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    });

};

returnObject.editBairro = function (formData)
{

    return $http({
        url: baseURLAdm + "/webservice.php?class=EXTDAO_Bairro&jsonOutput=true&action=edit",
        method: "POST",
        data: formData,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    });

};

returnObject.getAllComboBoxesDataForBairroForm = function ()
{
    return $http({
        url: baseURLAdm + "/webservice.php?class=EXTDAO_Bairro&jsonOutput=true&action=__getFormComboBoxesData",
        method: "POST",
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    });

};

returnObject.getListOfCidadeForBairroForm = function ()
{
    var params = {filterParameters: null, listMappingType: Constants.ListMappingType.COMBOBOX, hideLoading: true};
    return $http.post(baseURLAdm + "/webservice.php?class=EXTDAO_Cidade&action=__getList", params);
};
                

