"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var model_1 = require("lib/model");
var geral_1 = require("aplicacao/comum/geral");
var Usuario = (function (_super) {
    __extends(Usuario, _super);
    function Usuario(id, nome, isOnline, cor) {
        var _this = _super.call(this, 'Usuario') || this;
        cor = cor || geral_1.Helper.getRandomColor(Usuario.Color.DefaultDarkLuma, Usuario.Color.DefaultLightLuma);
        _this.id = new model_1.SearchableAttribute(id, true, true);
        _this.nome = new model_1.SearchableAttribute(nome, true);
        _this.isOnline = new model_1.SearchableAttribute(isOnline, false);
        _this.cor = new model_1.SearchableAttribute(cor, false);
        return _this;
    }
    Usuario.prototype.isOnlineClass = function () {
        return this.isOnline ? 'is-online' : '';
    };
    return Usuario;
}(model_1.SearchableModel));
Usuario.Color = { DefaultDarkLuma: 120, DefaultLightLuma: 120 };
exports.Usuario = Usuario;
