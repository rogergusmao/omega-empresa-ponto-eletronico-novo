<?php
include_once '../recursos/php/configuracao_biblioteca_compartilhada.php';

include_once '../client_area/imports/header.php';
include_once '../recursos/php/funcoes.php';
include_once '../client_area/imports/instancias.php';
include_once '../adm/imports/lingua.php';

if (isset($_POST["class"])) {

    $classe = Helper::POST("class");
    $action = Helper::POST("action");
    $registroDaOperacao = Helper::POST("id1");

} elseif (isset($_GET["class"])) {

    $classe = Helper::GET("class");
    $action = Helper::GET("action");
    $registroDaOperacao = Helper::GET("id1");

} else {

    exit();

}

if (in_array($action, Seguranca_Pagamento::$actionsComAutenticacao)) {

    Helper::includePHP(false, "client_area/imports/sessao.php");

} else {

    session_name(IDENTIFICADOR_SESSAO_CLIENTE);
    session_start();

}

$obj = call_user_func_array(array($classe, "factory"), array());

//---------------------------------
//*
//*
// INICIAR TRANSACAO
//*
//*
//---------------------------------


$objBanco = new Database();
$objBanco->iniciarTransacao();

if ($action == "add") {

    $retorno = $obj->__actionAdd();

} elseif ($action == "add_ajax") {

    $retorno = $obj->__actionAddAjax();

} elseif ($action == "edit") {

    $retorno = $obj->__actionEdit();

} elseif ($action == "edit_ajax") {

    $retorno = $obj->__actionEdit();

} elseif ($action == "remove") {

    $retorno = $obj->__actionRemove();

} elseif ($action == "login") {

    $retorno = $obj->__actionLogin(Helper::POST("txtLogin"), Helper::POST("txtSenha"));

} elseif ($action == "logout") {

    $retorno = $obj->__actionLogout();

} else {

    //chama a funcao de nome "$action"
    $retorno = call_user_func(array($obj, $action));

}

//---------------------------------
//*
//*
// FINALIZAR TRANSACAO (COMMIT OU ROLLBACK EM CASO DE ERRO)
//*
//*
//---------------------------------

if ($retorno[0]) {

    if (strpos($retorno[0], "msgErro") !== false) {

        $objBanco->rollbackTransacao();

    } else {

        $objBanco->commitTransacao();

    }

    header($retorno[0]);

} else {

    $objBanco->commitTransacao();

}

?>
