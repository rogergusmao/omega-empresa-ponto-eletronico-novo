

        /*

        Arquivo gerado atrav�s de gerador de c�digo em 07/09/2017 as 02:40:36.
        Para que o arquivo n�o seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

        Tabela correspondente: usuario_tipo_menu
        Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

        */

omegaApp.generatedFormUsuarioTipoMenuController = function($scope, $rootScope, $window, $interval, $timeout, dataService, Upload, vm) {try { vm.fieldsTypes = {};

vm.formData = {
        usuarioTipoId: null,
areaMenu: null,
areaMenuId: null,
};

vm.disabledFields = {
        fieldUsuarioTipoId: false,
fieldAreaMenu: false,
fieldAreaMenuId: false,
};

vm.requiredFields = {
        fieldUsuarioTipoId: true,
fieldAreaMenu: true,
fieldAreaMenuId: true,
};

vm.relatedEntities =
        {
        };

vm.fieldsParameters = {
        fieldUsuarioTipoId: {remoteData: null},
fieldAreaMenu: null,
fieldAreaMenuId: {remoteData: null},
};

vm.submitForm = function(formInstance)
        {
            if($rootScope.validateForm(formInstance))
            {
                var formData = angular.merge({}, vm.formData, GeneralUtil.getRelatedEntitiesData(vm.relatedEntities));
                if (!GeneralUtil.isNullOrEmpty(vm.formData.id))
                {
                    dataService.editUsuarioTipoMenu(formData).then(function(returnContent)
                    {
                        if (GeneralUtil.validateRemoteResponse(returnContent))
                        {
                            $rootScope.showDialog(vm, 'after-usuarioTipoMenu-update-template.html');
                        }
                        else
                        {
                            $rootScope.instantErrorMessages.push(returnContent.data.mMensagem);
                        }
                    });
                }
                else
                {
                    dataService.addUsuarioTipoMenu(formData).then(function(returnContent)
                    {
                        if (GeneralUtil.validateRemoteResponse(returnContent))
                        {
                            $rootScope.showDialog(vm, 'after-usuarioTipoMenu-insert-template.html');
                        }
                        else
                        {
                            $rootScope.instantErrorMessages.push(returnContent.data.mMensagem);
                        }
                    });
                }
            }

        };

if (!GeneralUtil.isNullOrEmpty(__GET) && !GeneralUtil.isNullOrEmpty(__GET.id))
        {

            var filterData = { id: __GET.id };
            var filterOperators = { id: Constants.FilterOperator.EQUALS };
            var parameters = GeneralUtil.getFilterParameters(filterData, filterOperators);

            dataService.getUsuarioTipoMenu(parameters).then(function (returnContent)
            {
                if (GeneralUtil.validateRemoteResponse(returnContent))
                {
                    vm.formData = returnContent.data.mObj;
                }
                else
                {
                    $rootScope.instantErrorMessages.push(returnContent.data.mMensagem);
                }

            });
        }

vm.openUsuarioTipoIdModal = function()
                {
                    $rootScope.uibModalInstance = $rootScope.showModalDialog('forms', 'UsuarioTipo');
                    
                    //atualizando valores da lista
                    var unregisterModal = $rootScope.$watch(function() { return $rootScope.uibModalInstance }, function () {
    
                        if($rootScope.uibModalInstance == null)
                        {
                            vm.getListOfUsuarioTipoId();
                            vm.formData.usuarioTipoId = null;
                            unregisterModal();
                        }
    
                    });
                    
                };
                vm.openAreaMenuIdModal = function()
                {
                    $rootScope.uibModalInstance = $rootScope.showModalDialog('forms', 'AreaMenu');
                    
                    //atualizando valores da lista
                    var unregisterModal = $rootScope.$watch(function() { return $rootScope.uibModalInstance }, function () {
    
                        if($rootScope.uibModalInstance == null)
                        {
                            vm.getListOfAreaMenuId();
                            vm.formData.areaMenuId = null;
                            unregisterModal();
                        }
    
                    });
                    
                };
                

vm.getListOfUsuarioTipoId = function()
                {
                    dataService.getListOfUsuarioTipo().then(function (returnContent)
                    {
                        if (GeneralUtil.validateRemoteResponse(returnContent))
                        {
                            vm.fieldsParameters.fieldUsuarioTipoId.remoteData = returnContent.data.mObj.gridData;
                        }
                        else
                        {
                            $rootScope.instantErrorMessages.push(returnContent.data.mMensagem);
                        }

                    });

                };
                vm.getListOfAreaMenuId = function()
                {
                    dataService.getListOfAreaMenu().then(function (returnContent)
                    {
                        if (GeneralUtil.validateRemoteResponse(returnContent))
                        {
                            vm.fieldsParameters.fieldAreaMenuId.remoteData = returnContent.data.mObj.gridData;
                        }
                        else
                        {
                            $rootScope.instantErrorMessages.push(returnContent.data.mMensagem);
                        }

                    });

                };
                

vm.loadListOfUsuarioTipoMenu = function()
        {
            document.location.href = 'index.php?tipo=lists&page=usuarioTipoMenu';
        };

        vm.addNewUsuarioTipoMenu = function()
        {
            document.location.href = document.location.href;
        };

}
                        catch(ex)
                        {
                            $rootScope.instantErrorMessages.push(new Message('Erro no controller generatedFormUsuarioTipoMenuController', ex.message));
                        }

                }