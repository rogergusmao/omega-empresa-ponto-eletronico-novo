<?php

    class Menu extends FlattyMenu
    {
        public function getArrayDoConteudoDoMenuCompleto()
        {
            $arrMenu = array(

                "PONTO_ELETRONICO" => array(

                    "config" => new FlattyMenuConfig("Ponto Eletr�nico", 'icon-time'),

                    "RELATORIO_DE_PONTO" => new FlattyMenuItem("Relat�rio de Ponto", "lists", "ponto"),
                    "MAPA_INTERATIVO" => new FlattyMenuItem("Mapa Interativo", "nextGen", "mapaInterativo", "icon-map-marker"),
                    "CALENDARIO" => new FlattyMenuItem("Visualizar Calend�rio", "nextGen", "calendarioDePontos", "icon-calendar"),

                    "PESSOAS" => array(

                        "config" => new FlattyMenuConfig("Pessoas", false, true),
                        "CADASTRAR_PESSOA" => new FlattyMenuItem("Cadastrar Pessoas", "forms", "pessoa"),
                        "GERENCIAR_PESSOAS" => new FlattyMenuItem("Gerenciar Pessoas", "lists", "pessoa"),

                    ),

                    "EMPRESAS" => array(

                        "config" => new FlattyMenuConfig("Empresas", false, true),
                        "CADASTRAR_EMPRESA" => new FlattyMenuItem("Cadastrar Empresa", "forms", "empresa"),
                        "GERENCIAR_EMPRESAS" => new FlattyMenuItem("Gerenciar Empresas", "lists", "empresa"),

                    )

                ),
                "CADASTROS" => array(

                    "config" => new FlattyMenuConfig("Cadastros", 'icon-file-text'),
                    "CADASTROS_GERAIS" => array(

                        "config" => new FlattyMenuConfig("Cadastros Gerais"),
                        "BAIRROS" => array(

                            "config" => new FlattyMenuConfig("Bairros"),
                            "CADASTRAR_BAIRRO" => new FlattyMenuItem("Cadastrar Bairro", "forms", "bairro"),
                            "GERENCIAR_BAIRROS" => new FlattyMenuItem("Gerenciar Bairros", "lists", "bairro"),

                        ),

                        "CIDADES" => array(

                            "config" => new FlattyMenuConfig("Cidades"),
                            "CADASTRAR_CIDADE" => new FlattyMenuItem("Cadastrar Cidade", "forms", "cidade"),
                            "GERENCIAR_CIDADES" => new FlattyMenuItem("Gerenciar Cidades", "lists", "cidade"),

                        ),

                        "ESTADOS" => array(

                            "config" => new FlattyMenuConfig("Estados"),
                            "CADASTRAR_ESTADO" => new FlattyMenuItem("Cadastrar Estado", "forms", "uf"),
                            "GERENCIAR_ESTADOS" => new FlattyMenuItem("Gerenciar Estados", "lists", "uf")

                        ),

                        "PAISES" => array(

                            "config" => new FlattyMenuConfig("Pa�ses"),
                            "CADASTRAR_PAIS" => new FlattyMenuItem("Cadastrar Pa�s", "forms", "pais"),
                            "GERENCIAR_PAISES" => new FlattyMenuItem("Gerenciar Paises", "lists", "pais")

                        ),

                        "PROFISSOES" => array(

                            "config" => new FlattyMenuConfig("Profiss�es"),
                            "CADASTRAR_PROFISSAO" => new FlattyMenuItem("Cadastrar Profiss�o", "forms", "profissao"),
                            "GERENCIAR_PROFISSOES" => new FlattyMenuItem("Gerenciar Profiss�es", "lists", "profissao")

                        ),

                        "TIPOS_DOCUMENTO" => array(

                            "config" => new FlattyMenuConfig("Tipos de Documentos"),
                            "CADASTRAR_TIPO_DOCUMENTO" => new FlattyMenuItem("Cadastrar Tipo de Documento", "forms", "tipoDocumento"),
                            "GERENCIAR_TIPOS_DOCUMENTO" => new FlattyMenuItem("Gerenciar Tipos de Documento", "lists", "tipoDocumento")

                        ),

                        "TIPOS_EMPRESA" => array(

                            "config" => new FlattyMenuConfig("Tipos de Empresa"),
                            "CADASTRAR_TIPO_EMPRESA" => new FlattyMenuItem("Cadastrar Tipo de Empresa", "forms", "tipoEmpresa"),
                            "GERENCIAR_TIPOS_EMPRESA" => new FlattyMenuItem("Gerenciar Tipos de Empresa", "lists", "tipoEmpresa")

                        ),

                        "PERMISSOES_SISTEMA_WEB" => array(

                            "config" => new FlattyMenuConfig("Permiss�es do Sistema Web"),
                            "CADASTRAR_TIPO_USUARIO" => new FlattyMenuItem("Cadastrar Tipo de Usu�rio", "forms", "usuarioTipo"),
                            "GERENCIAR_TIPOS_USUARIO" => new FlattyMenuItem("Gerenciar Tipos de Usu�rio", "lists", "usuarioTipo")

                        )

                    ),

                    "USUARIOS" => array(

                        "config" => new FlattyMenuConfig("Usu�rios", false, true),
                        "CADASTRAR_USUARIO" => new FlattyMenuItem("Cadastrar Usu�rio" , "forms", "usuario"),
                        "GERENCIAR_USUARIOS" => new FlattyMenuItem("Gerenciar Usu�rios", "lists", "usuario"),

                    )

                )

            );

            $arrMenuSistema = array();
            $arrMenu = array_merge($arrMenu, $arrMenuSistema);

            return $arrMenu;
        }
    }

?>