<div class='row' id='content-wrapper'>

    <div class='col-xs-12 omega-list-view'>

        <div class='row'>
            <div class='col-sm-12'>
                <div class='page-header'>
                    <h1 class='pull-left'>
                        <i class='icon-table'></i>
                        <span><?php= I18N::getExpression("Dashboard"); ?></span>
                    </h1>
                </div>
            </div>
        </div>

        <?php

        try
        {
            $msgDashboard = Servicos_web_ponto_eletronico::getDashboard();

            $totalEntrada = 0;
            $totalSaida = 0;

            if ($msgDashboard != null && Interface_mensagem::checkOk($msgDashboard))
            {
                $totalEntrada = $msgDashboard->mObj->totalEntrada;
                $totalSaida = $msgDashboard->mObj->totalSaida;
            }
            else
            {
                if ($msgDashboard != null && Interface_mensagem::checkErro($msgDashboard))
                {
                    InterfaceReportarErro::reportarMensagem($msgDashboard);
                }
            }
        }
        catch (Exception $ex)
        {
            SingletonLog::writeLogException($ex);
        }

        ?>

        <div class='row'>
            <div class='col-sm-12 col-md-12'>
                <div class='box'>
                    <div class='box-header'>
                        <div class='title'>
                            <i class='icon-group'></i>
                            <?= I18N::getExpression("Pontos batidos nas últimas 24 horas") ?>
                        </div>
                        <div class='actions'>
                            <a class="btn box-collapse btn-xs btn-link" href="#"><i></i></a>
                        </div>
                    </div>
                    <div class='row'>
                        <div class='col-sm-12'>
                            <div class='box-content box-statistic'>
                                <h3 class='title text-success'>
                                    <?= $totalEntrada ?>
                                </h3>
                                <small>
                                    <?= I18N::getExpression("Entrada") ?>
                                </small>
                                <div class='text-success icon-long-arrow-left align-right'></div>
                            </div>
                            <div class='box-content box-statistic'>
                                <h3 class='title text-error'>
                                    <?= $totalSaida ?>
                                </h3>
                                <small>
                                    <?= I18N::getExpression("Saída") ?>
                                </small>
                                <div class='text-error icon-long-arrow-right align-right'></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--

        <div class='row'>

            <div class='col-sm-12'>

                <div class="box">

                    <div class='box-header'>
                        <div class='title'>
                            <i class='icon-inbox'></i>
                            Orders
                        </div>
                        <div class='actions'>
                            <a class="btn box-remove btn-xs btn-link" href="#"><i class='icon-remove'></i>
                            </a>

                            <a class="btn box-collapse btn-xs btn-link" href="#"><i></i>
                            </a>
                        </div>
                    </div>
                    <div class='box-content'>
                        <div id='stats-chart1'></div>
                    </div>

                </div>

            </div>

        </div>

        <div class='row'>

            <div class='col-sm-12'>

                <div class="box">

                    <div class='box-header'>
                        <div class='title'>
                            <i class='icon-group'></i>
                            Users
                        </div>
                        <div class='actions'>
                            <a class="btn box-remove btn-xs btn-link" href="#"><i class='icon-remove'></i>
                            </a>

                            <a class="btn box-collapse btn-xs btn-link" href="#"><i></i>
                            </a>
                        </div>
                    </div>
                    <div class='box-content'>
                        <div id='stats-chart2'></div>
                    </div>

                </div>

            </div>

        </div>

        <div class='row'>
            <div class='col-sm-12 col-md-6'>
                <div class='box'>
                    <div class='box-header'>
                        <div class='title'>
                            <div class='icon-inbox'></div>
                            Orders
                        </div>
                        <div class='actions'>
                            <a class="btn box-remove btn-xs btn-link" href="#"><i class='icon-remove'></i>
                            </a>

                            <a class="btn box-collapse btn-xs btn-link" href="#"><i></i>
                            </a>
                        </div>
                    </div>
                    <div class='row'>
                        <div class='col-sm-6'>
                            <div class='box-content box-statistic'>
                                <h3 class='title text-error'>191</h3>
                                <small>New</small>
                                <div class='text-error icon-inbox align-right'></div>
                            </div>
                            <div class='box-content box-statistic'>
                                <h3 class='title text-warning'>311</h3>
                                <small>In process</small>
                                <div class='text-warning icon-check align-right'></div>
                            </div>
                            <div class='box-content box-statistic'>
                                <h3 class='title text-info'>3</h3>
                                <small>Pending</small>
                                <div class='text-info icon-time align-right'></div>
                            </div>
                        </div>
                        <div class='col-sm-6'>
                            <div class='box-content box-statistic'>
                                <h3 class='title text-primary'>3</h3>
                                <small>Shipped</small>
                                <div class='text-primary icon-truck align-right'></div>
                            </div>
                            <div class='box-content box-statistic'>
                                <h3 class='title text-success'>981</h3>
                                <small>Completed</small>
                                <div class='text-success icon-flag align-right'></div>
                            </div>
                            <div class='box-content box-statistic'>
                                <h3 class='title text-muted'>0</h3>
                                <small>Canceled</small>
                                <div class='text-muted icon-remove align-right'></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class='col-sm-6 col-md-3'>
                <div class='box'>
                    <div class='box-header'>
                        <div class='title'>
                            <i class='icon-group'></i>
                            Visitors
                        </div>
                        <div class='actions'>
                            <a class="btn box-remove btn-xs btn-link" href="#"><i class='icon-remove'></i>
                            </a>

                            <a class="btn box-collapse btn-xs btn-link" href="#"><i></i>
                            </a>
                        </div>
                    </div>
                    <div class='row'>
                        <div class='col-sm-12'>
                            <div class='box-content box-statistic'>
                                <h3 class='title text-error'>9100</h3>
                                <small>Unique</small>
                                <div class='text-error icon-user align-right'></div>
                            </div>
                            <div class='box-content box-statistic'>
                                <h3 class='title text-warning'>41 000</h3>
                                <small>Pageviews</small>
                                <div class='text-warning icon-book align-right'></div>
                            </div>
                            <div class='box-content box-statistic'>
                                <h3 class='title text-primary'>12:21</h3>
                                <small>Average time</small>
                                <div class='text-primary icon-time align-right'></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class='col-sm-6 col-md-3'>
                <div class='box'>
                    <div class='box-header'>
                        <div class='title'>
                            <i class='icon-comments'></i>
                            Comments
                        </div>
                        <div class='actions'>
                            <a class="btn box-remove btn-xs btn-link" href="#"><i class='icon-remove'></i>
                            </a>

                            <a class="btn box-collapse btn-xs btn-link" href="#"><i></i>
                            </a>
                        </div>
                    </div>
                    <div class='row'>
                        <div class='col-sm-12'>
                            <div class='box-content box-statistic'>
                                <h3 class='title text-error'>91</h3>
                                <small>New</small>
                                <div class='text-error icon-plus align-right'></div>
                            </div>
                            <div class='box-content box-statistic'>
                                <h3 class='title text-success'>1</h3>
                                <small>Approved</small>
                                <div class='text-success icon-ok align-right'></div>
                            </div>
                            <div class='box-content box-statistic'>
                                <h3 class='title text-info'>123</h3>
                                <small>Pending</small>
                                <div class='text-info icon-time align-right'></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        -->

    </div>

</div>

<!--
<style type="text/css">

    .header {

        border-bottom: 3px solid #009AD9;
        line-height: normal;
        vertical-align: middle;
        padding-top: 40px;
        padding-bottom: 40px;
        font-size: 30px;
        font-weight: bold;
        letter-spacing: -2px;
        line-height: 38px;
        font-family: arial, Helvetica, sans-serif;

    }

    .footer {

        height: 150px;
        border-top: 3px solid #009AD9;
        line-height: normal;
        margin-left: auto;
        margin-right: auto;
        width: 100%;
        font-family: arial, Helvetica, sans-serif;
        font-size: 14px;
        font-weight: bold;

    }

    .footer table td {

        text-align: center;
        width: 500px;

    }

    .footer img {

        height: 130px;
        width: 130px;

    }

    .container {
        height: 650px;
        width: 900px;
        position: static;
        margin-left: auto;
        margin-right: auto;
        text-align: center;
    }

    ul.thumb {
        float: left;
        list-style: none;
        padding: 10px;
        width: 950px;

    }

    ul.thumb li > a {

        display: block;
        margin-bottom: 20px;
        min-height: 200px;

    }

    ul.thumb li {
        margin: 15px;
        margin-left: 0px;
        padding: 0px;
        padding-bottom: 5px;
        float: left;
        position: relative;
        width: 210px;
        height: 210px;
        margin-bottom: 50px;
    }

    ul.thumb li img {
        width: 200px;
        height: 200px;
        border: 1px solid #ddd;
        padding: 5px;
        background: #f0f0f0;
        position: absolute;
        left: 0;
        top: 0;
        -ms-interpolation-mode: bicubic;
    }

    ul.thumb li img.hover {
        background: url(thumb_bg.png) no-repeat center center;
        border: none;
    }

    #main_view {
        float: left;
        padding: 9px 0;
        margin-left: -10px;
    }

    ul.thumb li h2 {
        font-size: 1em;
        font: normal 10px Verdana, Arial, Helvetica, sans-serif;
        font-weight: normal;
        text-transform: uppercase;
        padding: 10px;
        margin-top: 20px;
        background: #f0f0f0;
        width: 200px;
        min-width: 200px;
        border: 1px solid #ddd;
        text-align: center;
        height: 85px;
        min-height: 85px;
        vertical-align: middle;
        display: table-cell;

    }

    ul.thumb li h2 a {
        height: 100%;
        text-decoration: none;
        color: #777;
        vertical-align: middle;
        font-size: 13px;
        font-weight: bold;
    }

    ul.thumb li span {
        /*--Used to crop image--*/
        width: 202px;
        height: 202px;
        overflow: hidden;
        display: block;
    }

</style>

<script type="text/javascript">

    $(document).ready(function () {

        //Larger thumbnail preview 

        $("ul.thumb li").hover(function () {
            $(this).css({'z-index': '10'});
            $(this).find('img').addClass("hover").stop()
                .animate({
                    marginTop: '-150px',
                    marginLeft: '-150px',
                    top: '50%',
                    left: '50%',
                    width: '256px',
                    height: '256px',
                    padding: '20px'
                }, 200);

        }, function () {
            $(this).css({'z-index': '0'});
            $(this).find('img').removeClass("hover").stop()
                .animate({
                    marginTop: '0',
                    marginLeft: '0',
                    top: '0',
                    left: '0',
                    width: '200px',
                    height: '200px',
                    padding: '5px'
                }, 400);
        });

        //Swap Image on Click
        $("ul.thumb li a").click(function () {

            //var mainImage = $(this).attr("href"); //Find Image Name
            //$("#main_view img").attr({ src: mainImage });
            //return false;		

        });

    });


</script>

<div class="container">

    <ul class="thumb">
        <li style="z-index: 0;">
            <a href="index.php?tipo=forms&page=empresa"><span><img
                        style="margin-top: 0px; margin-left: 0px; top: 0px; left: 0px; width: 200px; height: 200px; padding: 5px; overflow: hidden;"
                        class="" src="imgs/especifico/empresa.png" alt=""></span></a>

            <h2><a href="index.php?tipo=forms&page=empresa">Cadastrar Empresa</a></h2>
        </li>
        <li style="z-index: 0;">
            <a href="index.php?tipo=lists&page=empresa"><span><img
                        style="margin-top: 0px; margin-left: 0px; top: 0px; left: 0px; width: 200px; height: 200px; padding: 5px;"
                        class="" src="imgs/especifico/lista_de_empresas.png" alt=""></span></a>

            <h2><a href="index.php?tipo=lists&page=empresa">Visualizar Lista de Empresas</a></h2>
        </li>
        <li style="z-index: 0;">
            <a href="index.php?tipo=forms&page=pessoa"><span><img
                        style="margin-top: 0px; margin-left: 0px; top: 0px; left: 0px; width: 200px; height: 200px; padding: 5px; overflow: hidden;"
                        class="" src="imgs/especifico/pessoa.png" alt=""></span></a>

            <h2><a href="index.php?tipo=forms&page=empresa">Cadastrar Pessoa</a></h2>
        </li>
        <li style="z-index: 0;">
            <a href="index.php?tipo=lists&page=pessoa"><span><img
                        style="overflow: hidden; margin-top: 0px; margin-left: 0px; top: 0px; left: 0px; width: 200px; height: 200px; padding: 5px;"
                        class="" src="imgs/especifico/lista_de_pessoas.png" alt=""></span></a>

            <h2><a href="index.php?tipo=lists&page=pessoa">Visualizar Lista de Pessoas</a></h2>
        </li>

    </ul>
</div>-->
