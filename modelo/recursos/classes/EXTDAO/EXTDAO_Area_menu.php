<?php

/*

Arquivo gerado através de gerador de código em 06/09/2017 as 21:16:46.
Para que o arquivo não seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

Tabela correspondente: area_menu
Sobrescrita de dados: ---SOBRESCRITA_NAO_AUTORIZADA---

*/
?>

<?php

class EXTDAO_Area_menu extends DAO_Area_menu
{
    public function __construct($configDAO)
    {
        parent::__construct($configDAO);
        $this->nomeClasse = "EXTDAO_Area_menu";
    }

    public function factory()
    {
        return new EXTDAO_Area_menu();
    }
}

?>
