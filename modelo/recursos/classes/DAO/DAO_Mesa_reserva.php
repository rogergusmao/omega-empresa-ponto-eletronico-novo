<?php


        /*

        Arquivo gerado através de gerador de código em 12/05/2018 as 14:13:44.
        Para que o arquivo não seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

        Tabela correspondente: mesa_reserva
        Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

        */
        


class DAO_Mesa_reserva extends Generic_DAO
                        {
                            const NOME_ENTIDADE = "mesa_reserva";

                            public static $nomeTabela = null;
                            public static $idSistemaTabela = false;
                            public static $databaseFieldsRelatedAttributes = null;
	                        public static $databaseFieldTypes = null;
	                        public static $databaseFieldNames = null;
	                        public static $listAliasTypes = null;
	                        public static $listAliasRelatedAttributes = null;
	                        
	                        public $id;
public $mesaId;
public $cadastroSec;
public $cadastroOffsec;
public $pessoaId;
public $reservaSec;
public $reservaOffsec;
public $confimadoSec;
public $confirmadoOffsec;
public $confirmadorUsuarioId;
public $celular;
public $protocoloInt;
public $corporacaoId;

public $labelId;
public $labelMesaId;
public $labelCadastroSec;
public $labelCadastroOffsec;
public $labelPessoaId;
public $labelReservaSec;
public $labelReservaOffsec;
public $labelConfimadoSec;
public $labelConfirmadoOffsec;
public $labelConfirmadorUsuarioId;
public $labelCelular;
public $labelProtocoloInt;
public $labelCorporacaoId;

public static $arrPK = array("id");

public function __construct($configDAO = null)
        {
            parent::__construct($configDAO);
            static::__setStaticValues($this->database);           
        }

public static function idSistemaTabela($databaseObject = null){
            if(!(static::$idSistemaTabela))
            {
                static::$idSistemaTabela = EXTDAO_Sistema_tabela::getIdDaSistemaTabela(static::nomeTabela(), $databaseObject);
            }
            return static::$idSistemaTabela;
        }

public function getIdSistemaTabela(){
            return static::idSistemaTabela();
        }

public static function nomeTabela(){
            if(is_null(static::$nomeTabela))
            {
                static::$nomeTabela = "mesa_reserva";
            }
            return static::$nomeTabela;
        }

public function getNomeTabela(){
            return static::nomeTabela();
        }

public static function __setStaticValues($databaseObject = null)
                   {    
                        static::nomeTabela();
                        
                        static::idSistemaTabela($databaseObject);
                   
                        static::setDatabaseFieldNames();
                        static::setDatabaseFieldTypes();
                        static::setDatabaseFieldsRelatedAttributeNames();
                        
                        static::setListAliasTypes();
                        static::setListAliasRelatedAttributes();
                   }

public function getExpressaoLabel()
                   {
                        return "{id}";
                   }

public static function setDatabaseFieldNames()
        {
            if(is_null(static::$databaseFieldNames))
            {
                static::$databaseFieldNames = new stdClass();
                static::$databaseFieldNames->id = "id";
static::$databaseFieldNames->mesaId = "mesa_id_INT";
static::$databaseFieldNames->cadastroSec = "cadastro_SEC";
static::$databaseFieldNames->cadastroOffsec = "cadastro_OFFSEC";
static::$databaseFieldNames->pessoaId = "pessoa_id_INT";
static::$databaseFieldNames->reservaSec = "reserva_SEC";
static::$databaseFieldNames->reservaOffsec = "reserva_OFFSEC";
static::$databaseFieldNames->confimadoSec = "confimado_SEC";
static::$databaseFieldNames->confirmadoOffsec = "confirmado_OFFSEC";
static::$databaseFieldNames->confirmadorUsuarioId = "confirmador_usuario_id_INT";
static::$databaseFieldNames->celular = "celular";
static::$databaseFieldNames->protocoloInt = "protocolo_INT";
static::$databaseFieldNames->corporacaoId = "corporacao_id_INT";

            }
                      
        }

public static function setDatabaseFieldTypes()
        {
            if(is_null(static::$databaseFieldTypes))
            {
                static::$databaseFieldTypes = new stdClass();
                static::$databaseFieldTypes->id = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->mesaId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->cadastroSec = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$databaseFieldTypes->cadastroOffsec = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$databaseFieldTypes->pessoaId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->reservaSec = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$databaseFieldTypes->reservaOffsec = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$databaseFieldTypes->confimadoSec = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$databaseFieldTypes->confirmadoOffsec = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$databaseFieldTypes->confirmadorUsuarioId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->celular = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->protocoloInt = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->corporacaoId = static::TIPO_VARIAVEL_INTEGER;

            }
            
        }

public static function setDatabaseFieldsRelatedAttributeNames()
        {
            if(is_null(static::$databaseFieldsRelatedAttributes))
            {
                static::$databaseFieldsRelatedAttributes = new stdClass();
                static::$databaseFieldsRelatedAttributes->id = "id";
static::$databaseFieldsRelatedAttributes->mesa_id_INT = "mesaId";
static::$databaseFieldsRelatedAttributes->cadastro_SEC = "cadastroSec";
static::$databaseFieldsRelatedAttributes->cadastro_OFFSEC = "cadastroOffsec";
static::$databaseFieldsRelatedAttributes->pessoa_id_INT = "pessoaId";
static::$databaseFieldsRelatedAttributes->reserva_SEC = "reservaSec";
static::$databaseFieldsRelatedAttributes->reserva_OFFSEC = "reservaOffsec";
static::$databaseFieldsRelatedAttributes->confimado_SEC = "confimadoSec";
static::$databaseFieldsRelatedAttributes->confirmado_OFFSEC = "confirmadoOffsec";
static::$databaseFieldsRelatedAttributes->confirmador_usuario_id_INT = "confirmadorUsuarioId";
static::$databaseFieldsRelatedAttributes->celular = "celular";
static::$databaseFieldsRelatedAttributes->protocolo_INT = "protocoloInt";
static::$databaseFieldsRelatedAttributes->corporacao_id_INT = "corporacaoId";

            }           
        }

public function setAllFieldsToHumanFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["mesa_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["cadastro_SEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["cadastro_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["pessoa_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["reserva_SEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["reserva_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["confimado_SEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["confirmado_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["confirmador_usuario_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["celular"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["protocolo_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
}

public function setAllFieldsToDatabaseFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["mesa_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["cadastro_SEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["cadastro_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["pessoa_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["reserva_SEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["reserva_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["confimado_SEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["confirmado_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["confirmador_usuario_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["celular"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["protocolo_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
}

  public function getFkObjMesa() 
                {
                    if($this->objMesa == null)
                    {                        
                        $this->objMesa = new EXTDAO_Mesa_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getMesa_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objMesa->clear();
                    }
                    elseif($this->objMesa->getId() != $idFK)
                    {
                        $this->objMesa->select($idFK);
                    }
                    return $this->objMesa;
                }
  public function getFkObjPessoa() 
                {
                    if($this->objPessoa == null)
                    {                        
                        $this->objPessoa = new EXTDAO_Pessoa_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getPessoa_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objPessoa->clear();
                    }
                    elseif($this->objPessoa->getId() != $idFK)
                    {
                        $this->objPessoa->select($idFK);
                    }
                    return $this->objPessoa;
                }
  public function getFkObjConfirmador_usuario() 
                {
                    if($this->objConfirmador_usuario == null)
                    {                        
                        $this->objConfirmador_usuario = new EXTDAO_Confirmador_usuario_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getConfirmador_usuario_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objConfirmador_usuario->clear();
                    }
                    elseif($this->objConfirmador_usuario->getId() != $idFK)
                    {
                        $this->objConfirmador_usuario->select($idFK);
                    }
                    return $this->objConfirmador_usuario;
                }
  public function getFkObjCorporacao() 
                {
                    if($this->objCorporacao == null)
                    {                        
                        $this->objCorporacao = new EXTDAO_Corporacao_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getCorporacao_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objCorporacao->clear();
                    }
                    elseif($this->objCorporacao->getId() != $idFK)
                    {
                        $this->objCorporacao->select($idFK);
                    }
                    return $this->objCorporacao;
                }


public function setLabels()
        {$this->labelId = "";
$this->labelMesaId = "";
$this->labelCadastroSec = "";
$this->labelCadastroOffsec = "";
$this->labelPessoaId = "";
$this->labelReservaSec = "";
$this->labelReservaOffsec = "";
$this->labelConfimadoSec = "";
$this->labelConfirmadoOffsec = "";
$this->labelConfirmadorUsuarioId = "";
$this->labelCelular = "";
$this->labelProtocoloInt = "";
$this->labelCorporacaoId = "";
}

public function __actionAdd($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                $this->setByObject($parameters);
                $this->setDataCadastroAndOffsetInSecondsFromUser($parameters);
                
                $this->formatarParaSQL();
                $msg = $this->insert(true);
                
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                return new Mensagem(null, I18N::getExpression("Mesa reserva adicionado com sucesso."));
                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionEdit($parameters = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try {

                $this->setByObject($parameters);
                $this->setAllFieldsToHumanFormat();
                
                $this->formatarParaSQL();
                $msg = $this->updateByInstanceUsingParameters($this->id, $parameters, true);
                if($msg != null && $msg->erro())
                {
                     return $msg;
                }
        
                return new Mensagem(null, I18N::getExpression("Mesa reserva editado com sucesso."));
            }
            catch(Exception $ex)
            {                
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionRemove($parameters = null)
        {
            $successulRemovalCount = 0;
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                foreach($parameters as $record)
                {
                    if(is_numeric($record->id))
                    {
                        $msg = $this->delete($record->id, true);
                        if($msg == null || $msg->ok())
                        {
                            $successulRemovalCount++;
                        }
                    }
                }
                
                if($successulRemovalCount > 0)
                {
                    $successMessage = count($successulRemovalCount) > 1 ? I18N::getExpression("{0} Mesa reserva foram removidos com sucesso.", $successulRemovalCount) : I18N::getExpression("Mesa reserva removido com sucesso.");
                    return new Mensagem(null, $successMessage);
                }
                else
                {
                    $errorMessage = count($parameters) > 1 ? I18N::getExpression("Falha ao remover Mesa reserva.") : I18N::getExpression("Falha ao remover Mesa reserva.");
                    return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, $errorMessage);                    
                }
            }
            catch (Exception $ex) 
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }

public function __getRecord($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }
            
            try
            {            
                $returnObject = new stdClass();            
                $whereClause = static::getWhereClauseForFilter($parameters->filterParameters);
                $query = "SELECT  id, mesa_id_INT, cadastro_SEC, cadastro_OFFSEC, pessoa_id_INT, reserva_SEC, reserva_OFFSEC, confimado_SEC, confirmado_OFFSEC, confirmador_usuario_id_INT, celular, protocolo_INT, corporacao_id_INT FROM mesa_reserva {$whereClause}";
    
                $msg = $this->database->queryMensagem($query);                
                if ($msg == null)
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Não foi encontrado nenhum registro"));
                    }                    
                    $resultSet = $this->database->result;
                    $resultObject = static::mapResultSetDataToRecord($resultSet);
                    
                    $returnObject->formData = $resultObject;
                    $returnObject->comboBoxesData = null;
                    if($parameters->loadComboBoxesData)
                    {
                        $this->setByObject($resultObject);
                        $returnObject->comboBoxesData =  $this->getFormComboBoxesData();
                    }                    
    
                    
    
                    return new Mensagem_generica($returnObject);
                } 
                else 
                {
                    return $msg;
                }
                
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }


        
        public function __getFilterComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
                
        public function __getFormComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
        
        protected function getFormComboBoxesData()
        {
            $comboBoxesData = new stdClass();
            $listParameters = new stdClass();
            $listParameters->listMappingType = static::LIST_MAPPING_TYPE_COMBOBOX;
            
            
                    
                    $objMesa = new EXTDAO_Mesa();
                    $comboBoxesData->fieldMesaId = $objMesa->__getList($listParameters);
                    
                    $objPessoa = new EXTDAO_Pessoa();
                    $comboBoxesData->fieldPessoaId = $objPessoa->__getList($listParameters);

            return $comboBoxesData;
        }
        
        


        
        public static function setListAliasTypes()
        {
            if(is_null(static::$listAliasTypes))
            {
                static::$listAliasTypes = new stdClass();
                static::$listAliasTypes->mesa__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->pessoa__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->mesa_reserva__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->mesa_reserva__corporacao_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->mesa_reserva__cadastro_SEC = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$listAliasTypes->mesa_reserva__ind_celular_valido_BOOLEAN = static::TIPO_VARIAVEL_BOOLEAN;
static::$listAliasTypes->mesa_reserva__reserva_SEC = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$listAliasTypes->mesa_reserva__confimado_SEC = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$listAliasTypes->mesa_reserva__confirmador_usuario_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->mesa_reserva__celular = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->mesa_reserva__protocolo_INT = static::TIPO_VARIAVEL_INTEGER;
            }        
        }
        
        public static function setListAliasRelatedAttributes()
        {
            if(is_null(static::$listAliasRelatedAttributes))
            {
                static::$listAliasRelatedAttributes = new stdClass();
                static::$listAliasRelatedAttributes->mesa__id = "mesaId";
static::$listAliasRelatedAttributes->pessoa__id = "pessoaId";
static::$listAliasRelatedAttributes->mesa_reserva__id = "id";
static::$listAliasRelatedAttributes->mesa_reserva__corporacao_id_INT = "corporacaoId";
static::$listAliasRelatedAttributes->mesa_reserva__cadastro_SEC = "cadastroSec";
static::$listAliasRelatedAttributes->mesa_reserva__ind_celular_valido_BOOLEAN = "indCelularValidoBoolean";
static::$listAliasRelatedAttributes->mesa_reserva__reserva_SEC = "reservaSec";
static::$listAliasRelatedAttributes->mesa_reserva__confimado_SEC = "confimadoSec";
static::$listAliasRelatedAttributes->mesa_reserva__confirmador_usuario_id_INT = "confirmadorUsuarioId";
static::$listAliasRelatedAttributes->mesa_reserva__celular = "celular";
static::$listAliasRelatedAttributes->mesa_reserva__protocolo_INT = "protocoloInt";
            }         
        }

        public function __getList($parameters = null, $idCorporacao = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try
            {
                $filterParameters = new stdClass();
                if(isset($parameters->filterParameters))
                {
                    $filterParameters = $parameters->filterParameters;
                }
                                    
                            
            if(is_null($idCorporacao))
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            ListParameters::addFilterParameterToObject($filterParameters, "corporacaoId", $idCorporacao);           
                
                
                $sortingParameters = null;
                if(isset($parameters->sortingParameters))
                {
                    $sortingParameters = $parameters->sortingParameters;
                }
                
                $paginationParameters = null;
                if(isset($parameters->paginationParameters))
                {
                    $paginationParameters = $parameters->paginationParameters;
                }
                
                $listMappingType = static::getListMappingType($parameters->listMappingType);
                $mainTableAlias = "mr";

                $whereClause = static::getWhereClauseForFilter($filterParameters, $mainTableAlias);
                $orderByClause = static::getOrderByClauseForFilter($sortingParameters, $mainTableAlias);
                $limitClause = static::getLimitClauseForFilter($paginationParameters);

                $queryWithoutLimit = "SELECT mr.id FROM mesa_reserva mr {$whereClause}";
                $query = "SELECT m.id AS mesa__id, p.id AS pessoa__id, mr.id AS mesa_reserva__id, mr.corporacao_id_INT AS mesa_reserva__corporacao_id_INT, mr.cadastro_SEC AS mesa_reserva__cadastro_SEC, mr.ind_celular_valido_BOOLEAN AS mesa_reserva__ind_celular_valido_BOOLEAN, mr.reserva_SEC AS mesa_reserva__reserva_SEC, mr.confimado_SEC AS mesa_reserva__confimado_SEC, mr.confirmador_usuario_id_INT AS mesa_reserva__confirmador_usuario_id_INT, mr.celular AS mesa_reserva__celular, mr.protocolo_INT AS mesa_reserva__protocolo_INT FROM mesa_reserva mr LEFT JOIN mesa m ON m.id = mr.mesa_id_INT LEFT JOIN pessoa p ON p.id = mr.pessoa_id_INT {$whereClause} {$orderByClause} {$limitClause}";

                $msg = $this->database->queryMensagem($queryWithoutLimit);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                
                $totalNumberOfRecords = $this->database->rows();
                if($totalNumberOfRecords == 0)
                {
                    return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                }                        
                        
                $msg = $this->database->queryMensagem($query);
                
                //operacao realizaca com sucesso
                if ($msg == null) 
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                    }                        
                
                    $resultSet = $this->database->result;

                    $objReturn = new stdClass();
                    $objReturn->dataSet = static::mapResultSetDataToList($resultSet, $listMappingType, static::$listAliasTypes);

                    if(!Helper::isNullOrEmpty($paginationParameters->recordsPerPage) && !Helper::isNullOrEmpty($paginationParameters->currentPage))
                    {
                        $lastPage = ceil($totalNumberOfRecords / $paginationParameters->recordsPerPage);
                        $objReturn->paginationParameters = new PaginationParams($paginationParameters->recordsPerPage, $paginationParameters->currentPage, $lastPage);
                    }
                    
                    return new Mensagem_generica($objReturn);
                } 
                else
                {
                    return $msg;
                }                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }



public function getId()
            {
                return $this->id;
            }

public function getMesaId()
            {
                return $this->mesaId;
            }

public function getMesa_id_INT()
                {
                    return $this->mesaId;
                }

public function getCadastroSec()
            {
                return $this->cadastroSec;
            }

public function getCadastro_SEC()
                {
                    return $this->cadastroSec;
                }

public function getCadastroOffsec()
            {
                return $this->cadastroOffsec;
            }

public function getCadastro_OFFSEC()
                {
                    return $this->cadastroOffsec;
                }

public function getPessoaId()
            {
                return $this->pessoaId;
            }

public function getPessoa_id_INT()
                {
                    return $this->pessoaId;
                }

public function getReservaSec()
            {
                return $this->reservaSec;
            }

public function getReserva_SEC()
                {
                    return $this->reservaSec;
                }

public function getReservaOffsec()
            {
                return $this->reservaOffsec;
            }

public function getReserva_OFFSEC()
                {
                    return $this->reservaOffsec;
                }

public function getConfimadoSec()
            {
                return $this->confimadoSec;
            }

public function getConfimado_SEC()
                {
                    return $this->confimadoSec;
                }

public function getConfirmadoOffsec()
            {
                return $this->confirmadoOffsec;
            }

public function getConfirmado_OFFSEC()
                {
                    return $this->confirmadoOffsec;
                }

public function getConfirmadorUsuarioId()
            {
                return $this->confirmadorUsuarioId;
            }

public function getConfirmador_usuario_id_INT()
                {
                    return $this->confirmadorUsuarioId;
                }

public function getCelular()
            {
                return $this->celular;
            }

public function getProtocoloInt()
            {
                return $this->protocoloInt;
            }

public function getProtocolo_INT()
                {
                    return $this->protocoloInt;
                }

public function getCorporacaoId()
            {
                return $this->corporacaoId;
            }

public function getCorporacao_id_INT()
                {
                    return $this->corporacaoId;
                }



function setId($value)
            {
                $this->id = $value;
            }

function setMesaId($value)
            {
                $this->mesaId = $value;
            }

public function setMesa_id_INT($value)
                { 
                    $this->mesaId = $value; 
                }

function setCadastroSec($value)
            {
                $this->cadastroSec = $value;
            }

public function setCadastro_SEC($value)
                { 
                    $this->cadastroSec = $value; 
                }

function setCadastroOffsec($value)
            {
                $this->cadastroOffsec = $value;
            }

public function setCadastro_OFFSEC($value)
                { 
                    $this->cadastroOffsec = $value; 
                }

function setPessoaId($value)
            {
                $this->pessoaId = $value;
            }

public function setPessoa_id_INT($value)
                { 
                    $this->pessoaId = $value; 
                }

function setReservaSec($value)
            {
                $this->reservaSec = $value;
            }

public function setReserva_SEC($value)
                { 
                    $this->reservaSec = $value; 
                }

function setReservaOffsec($value)
            {
                $this->reservaOffsec = $value;
            }

public function setReserva_OFFSEC($value)
                { 
                    $this->reservaOffsec = $value; 
                }

function setConfimadoSec($value)
            {
                $this->confimadoSec = $value;
            }

public function setConfimado_SEC($value)
                { 
                    $this->confimadoSec = $value; 
                }

function setConfirmadoOffsec($value)
            {
                $this->confirmadoOffsec = $value;
            }

public function setConfirmado_OFFSEC($value)
                { 
                    $this->confirmadoOffsec = $value; 
                }

function setConfirmadorUsuarioId($value)
            {
                $this->confirmadorUsuarioId = $value;
            }

public function setConfirmador_usuario_id_INT($value)
                { 
                    $this->confirmadorUsuarioId = $value; 
                }

function setCelular($value)
            {
                $this->celular = $value;
            }

function setProtocoloInt($value)
            {
                $this->protocoloInt = $value;
            }

public function setProtocolo_INT($value)
                { 
                    $this->protocoloInt = $value; 
                }

function setCorporacaoId($value)
            {
                $this->corporacaoId = $value;
            }

public function setCorporacao_id_INT($value)
                { 
                    $this->corporacaoId = $value; 
                }



public function clear()
        {$this->id = null;
$this->mesaId = null;
if($this->objMesa != null) unset($this->objMesa);
$this->cadastroSec = null;
$this->cadastroOffsec = null;
$this->pessoaId = null;
if($this->objPessoa != null) unset($this->objPessoa);
$this->reservaSec = null;
$this->reservaOffsec = null;
$this->confimadoSec = null;
$this->confirmadoOffsec = null;
$this->confirmadorUsuarioId = null;
if($this->objConfirmador_usuario != null) unset($this->objConfirmador_usuario);
$this->celular = null;
$this->protocoloInt = null;
$this->corporacaoId = null;
if($this->objCorporacao != null) unset($this->objCorporacao);
if(is_array($this->arrEstadosCampos)){
                        unset($this->arrEstadosCampos);
                     $this->arrEstadosCampos = array();
                    }
                }

public function formatarParaSQL()
        {
if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->id)){
$this->id = $this->formatarIntegerParaComandoSQL($this->id);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->id);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->mesaId)){
$this->mesaId = $this->formatarIntegerParaComandoSQL($this->mesaId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->mesaId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->cadastroSec)){
$this->cadastroSec = $this->formatarIntegerParaComandoSQL($this->cadastroSec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->cadastroSec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->cadastroOffsec)){
$this->cadastroOffsec = $this->formatarIntegerParaComandoSQL($this->cadastroOffsec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->cadastroOffsec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->pessoaId)){
$this->pessoaId = $this->formatarIntegerParaComandoSQL($this->pessoaId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->pessoaId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->reservaSec)){
$this->reservaSec = $this->formatarIntegerParaComandoSQL($this->reservaSec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->reservaSec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->reservaOffsec)){
$this->reservaOffsec = $this->formatarIntegerParaComandoSQL($this->reservaOffsec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->reservaOffsec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->confimadoSec)){
$this->confimadoSec = $this->formatarIntegerParaComandoSQL($this->confimadoSec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->confimadoSec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->confirmadoOffsec)){
$this->confirmadoOffsec = $this->formatarIntegerParaComandoSQL($this->confirmadoOffsec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->confirmadoOffsec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->confirmadorUsuarioId)){
$this->confirmadorUsuarioId = $this->formatarIntegerParaComandoSQL($this->confirmadorUsuarioId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->confirmadorUsuarioId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->celular)){
$this->celular = $this->formatarStringParaComandoSQL($this->celular);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->celular);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->protocoloInt)){
$this->protocoloInt = $this->formatarIntegerParaComandoSQL($this->protocoloInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->protocoloInt);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->corporacaoId)){
$this->corporacaoId = $this->formatarIntegerParaComandoSQL($this->corporacaoId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->corporacaoId);
}

}

public function formatarParaExibicao()
        {if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->celular)){
$this->celular = $this->formatarStringParaExibicao($this->celular);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->celular);
}

}

function select($id, $idCorporacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }   

            $sql = "SELECT id, mesa_id_INT, cadastro_SEC, cadastro_OFFSEC, pessoa_id_INT, reserva_SEC, reserva_OFFSEC, confimado_SEC, confirmado_OFFSEC, confirmador_usuario_id_INT, celular, protocolo_INT, corporacao_id_INT FROM mesa_reserva WHERE id = $id AND corporacao_id_INT = $idCorporacao";

            $msg = $this->database->queryMensagem($sql);
            $result = $this->database->result;

            if($msg != null && $msg->erro() )
            { 
            		$this->clear();
            		return $msg;
        	} 
        	else if($this->database->rows() == 0)
        	{
                return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nenhum registro foi encontrado."));
            }
        

            $row = $this->database->fetchArray(MYSQLI_NUM);
            
            		$this->id = $row[0];
		$this->mesaId = $row[1];
		$this->cadastroSec = $row[2];
		$this->cadastroOffsec = $row[3];
		$this->pessoaId = $row[4];
		$this->reservaSec = $row[5];
		$this->reservaOffsec = $row[6];
		$this->confimadoSec = $row[7];
		$this->confirmadoOffsec = $row[8];
		$this->confirmadorUsuarioId = $row[9];
		$this->celular = $row[10];
		$this->protocoloInt = $row[11];

            
        }

public function delete($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (is_null($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }

            $sql = "DELETE FROM mesa_reserva WHERE id= $id AND corporacao_id_INT = $idCorporacao";
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                    && $sincronizar
                        && static::idSistemaTabela() != null
                            && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_REMOVE,
                                null,
                                null,
                                $id,
                                $this->database);
            }

            return $msg;
        }

public function insert($sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null, $opcoes = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $this->defineDataCadastroInSecondsIfNotDefined();
$this->defineDataCadastroOffsetInSecondsIfNotDefined();

            
            if (is_null($opcoes))
            {
                $opcoes = $this->opcoesDAO;
            }

            if (!is_numeric($this->id))
            {
                $this->id = "null";
                if ($opcoes == null || $opcoes[Database::OPCAO_GERAR_ID] != false) 
                {
                    $this->id = EXTDAO_Sistema_sequencia::gerarId(static::nomeTabela());                    
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }    
                    else if($msg == null)
                    {
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
                else 
                {
                    $this->id = null;
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao));
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }
                    else if($msg == null)
                    {
                        $this->id = $this->database->getLastInsertId();
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
            }
            else 
            {
                $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }                        
                else if($msg == null)
                {
                   $msg = new Mensagem_token();
                   $msg->mValor = $this->id;
                }
            }
            if($msg == null || $msg->ok())
            {                
                if($sincronizar
                    && static::idSistemaTabela() != null
                    && isset($this->id)){
                    
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_INSERT,
                                null,
                                null,
                                $this->id,
                                $this->database); 
                	}
            } 
            else
            {
                $this->id = null;
            }
            
            return $msg;
            
        }

public function getSQLInsert($idCorporacao){ 

return "INSERT INTO mesa_reserva (id, mesa_id_INT, cadastro_SEC, cadastro_OFFSEC, pessoa_id_INT, reserva_SEC, reserva_OFFSEC, confimado_SEC, confirmado_OFFSEC, confirmador_usuario_id_INT, celular, protocolo_INT, corporacao_id_INT) VALUES ( $this->id ,  $this->mesaId ,  $this->cadastroSec ,  $this->cadastroOffsec ,  $this->pessoaId ,  $this->reservaSec ,  $this->reservaOffsec ,  $this->confimadoSec ,  $this->confirmadoOffsec ,  $this->confirmadorUsuarioId ,  $this->celular ,  $this->protocoloInt ,  $idCorporacao ) "; 

}




        
        public function updateByParameters($id, $objParametros, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao))
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();

            
                
                if (isset($objParametros->mesaId)) 
                {
                    $arrUpdateFields[] = " mesa_id_INT = {$objParametros->mesaId} ";
                }


                
                if (isset($objParametros->cadastroSec)) 
                {
                    $arrUpdateFields[] = " cadastro_SEC = {$objParametros->cadastroSec} ";
                }


                
                if (isset($objParametros->cadastroOffsec)) 
                {
                    $arrUpdateFields[] = " cadastro_OFFSEC = {$objParametros->cadastroOffsec} ";
                }


                
                if (isset($objParametros->pessoaId)) 
                {
                    $arrUpdateFields[] = " pessoa_id_INT = {$objParametros->pessoaId} ";
                }


                
                if (isset($objParametros->reservaSec)) 
                {
                    $arrUpdateFields[] = " reserva_SEC = {$objParametros->reservaSec} ";
                }


                
                if (isset($objParametros->reservaOffsec)) 
                {
                    $arrUpdateFields[] = " reserva_OFFSEC = {$objParametros->reservaOffsec} ";
                }


                
                if (isset($objParametros->confimadoSec)) 
                {
                    $arrUpdateFields[] = " confimado_SEC = {$objParametros->confimadoSec} ";
                }


                
                if (isset($objParametros->confirmadoOffsec)) 
                {
                    $arrUpdateFields[] = " confirmado_OFFSEC = {$objParametros->confirmadoOffsec} ";
                }


                
                if (isset($objParametros->confirmadorUsuarioId)) 
                {
                    $arrUpdateFields[] = " confirmador_usuario_id_INT = {$objParametros->confirmadorUsuarioId} ";
                }


                
                if (isset($objParametros->celular)) 
                {
                    $arrUpdateFields[] = " celular = {$objParametros->celular} ";
                }


                
                if (isset($objParametros->protocoloInt)) 
                {
                    $arrUpdateFields[] = " protocolo_INT = {$objParametros->protocoloInt} ";
                }



            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE mesa_reserva SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";

            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null)
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function update($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($this->mesaId)) 
                {                                      
                    $arrUpdateFields[] = " mesa_id_INT = {$this->mesaId} ";
                }


                
                if (isset($this->cadastroSec)) 
                {                                      
                    $arrUpdateFields[] = " cadastro_SEC = {$this->cadastroSec} ";
                }


                
                if (isset($this->cadastroOffsec)) 
                {                                      
                    $arrUpdateFields[] = " cadastro_OFFSEC = {$this->cadastroOffsec} ";
                }


                
                if (isset($this->pessoaId)) 
                {                                      
                    $arrUpdateFields[] = " pessoa_id_INT = {$this->pessoaId} ";
                }


                
                if (isset($this->reservaSec)) 
                {                                      
                    $arrUpdateFields[] = " reserva_SEC = {$this->reservaSec} ";
                }


                
                if (isset($this->reservaOffsec)) 
                {                                      
                    $arrUpdateFields[] = " reserva_OFFSEC = {$this->reservaOffsec} ";
                }


                
                if (isset($this->confimadoSec)) 
                {                                      
                    $arrUpdateFields[] = " confimado_SEC = {$this->confimadoSec} ";
                }


                
                if (isset($this->confirmadoOffsec)) 
                {                                      
                    $arrUpdateFields[] = " confirmado_OFFSEC = {$this->confirmadoOffsec} ";
                }


                
                if (isset($this->confirmadorUsuarioId)) 
                {                                      
                    $arrUpdateFields[] = " confirmador_usuario_id_INT = {$this->confirmadorUsuarioId} ";
                }


                
                if (isset($this->celular)) 
                {                                      
                    $arrUpdateFields[] = " celular = {$this->celular} ";
                }


                
                if (isset($this->protocoloInt)) 
                {                                      
                    $arrUpdateFields[] = " protocolo_INT = {$this->protocoloInt} ";
                }


            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE mesa_reserva SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function updateByInstanceUsingParameters($id, $parameters, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($parameters->mesaId)) 
                {                                      
                    $arrUpdateFields[] = " mesa_id_INT = {$this->mesaId} ";
                }
                
                if (isset($parameters->cadastroSec)) 
                {                                      
                    $arrUpdateFields[] = " cadastro_SEC = {$this->cadastroSec} ";
                }
                
                if (isset($parameters->cadastroOffsec)) 
                {                                      
                    $arrUpdateFields[] = " cadastro_OFFSEC = {$this->cadastroOffsec} ";
                }
                
                if (isset($parameters->pessoaId)) 
                {                                      
                    $arrUpdateFields[] = " pessoa_id_INT = {$this->pessoaId} ";
                }
                
                if (isset($parameters->reservaSec)) 
                {                                      
                    $arrUpdateFields[] = " reserva_SEC = {$this->reservaSec} ";
                }
                
                if (isset($parameters->reservaOffsec)) 
                {                                      
                    $arrUpdateFields[] = " reserva_OFFSEC = {$this->reservaOffsec} ";
                }
                
                if (isset($parameters->confimadoSec)) 
                {                                      
                    $arrUpdateFields[] = " confimado_SEC = {$this->confimadoSec} ";
                }
                
                if (isset($parameters->confirmadoOffsec)) 
                {                                      
                    $arrUpdateFields[] = " confirmado_OFFSEC = {$this->confirmadoOffsec} ";
                }
                
                if (isset($parameters->confirmadorUsuarioId)) 
                {                                      
                    $arrUpdateFields[] = " confirmador_usuario_id_INT = {$this->confirmadorUsuarioId} ";
                }
                
                if (isset($parameters->celular)) 
                {                                      
                    $arrUpdateFields[] = " celular = {$this->celular} ";
                }
                
                if (isset($parameters->protocoloInt)) 
                {                                      
                    $arrUpdateFields[] = " protocolo_INT = {$this->protocoloInt} ";
                }
            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE mesa_reserva SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }

}

?>
