/// <reference path="../../../__referencias/google.maps.markerwithlabel.d.ts" />

import { MapaElemento, Icone, Label, ElementoVisualizacao } from 'lib/uiComponente';
import { ContextoMapa } from 'lib/contexto';
import { GeoLocationHelper, Helper } from 'lib/geral';

export class Marcador extends MapaElemento
{
    public latitude :number;
    public longitude :number;

    public iconeObject :Icone;
    public labelObject :Label;
    
    protected marker :MarkerWithLabel;
    public constructor(contexto :ContextoMapa, latitude :number, longitude :number, tipoElemento :ElementoVisualizacao.Tipo)
    {
        super(contexto, [new google.maps.LatLng(latitude, longitude)], tipoElemento);

        if(!GeoLocationHelper.isValidCoordinate(latitude, longitude))
        {
            throw new Error("Coordenadas inválidas: " + latitude + ", " +  longitude);
        }

        this.latitude = latitude;
        this.longitude = longitude;
        this.marker = new MarkerWithLabel();
    }

    public setDefaultIconeObject() :void;
    public setDefaultIconeObject(iconeUrl :string) :void;
    public setDefaultIconeObject(iconeUrl? :string) :void
    {
        iconeUrl = iconeUrl || this.getDefaultIconePath();
        this.iconeObject = new Icone(iconeUrl);
    }

    public setVisible(visible: boolean)
    {
        this.marker.setVisible(visible);
    }

    public setIconeObject(icone :Icone)
    {
        this.iconeObject = icone;
    }

    public setDefaultLabelObject(texto :string) :void
    {
        this.labelObject = new Label(texto);
    }

    public setLabelObject(label :Label)
    {
        this.labelObject = label;
    }

    public getDefaultIconePath() :string
    {
        return this.contexto.getDefaultMarkerFolder() + "map_mainroad.png";
    }

    public getGoogleMarkerInstance()
    {
        return this.marker;
    }

    public async tryToSetHtmlElement()
    {
        if(this.isRendered())
        {
            var identificador = MapaElemento.getNextIdentifier();
            var tituloOriginal = this.marker.getTitle();

            this.marker.setTitle(identificador);
            await Helper.sleep(3000);

            var jQuerySet = this.contexto.mapObject.$('div[title=' + identificador + ']');
            if(jQuerySet.length > 0)
            {
                this.htmlElement = jQuerySet[0];
            }

            this.marker.setTitle(tituloOriginal);

        }

    }

    public isRendered() : boolean
    {
        return this.marker != null && this.marker.getMap() != null;
    }

    public async render()
    {
        if(this.iconeObject == null)
        {
            this.setDefaultIconeObject();
        }

        this.marker = new MarkerWithLabel({
            icon: this.iconeObject,
            position: new google.maps.LatLng(this.latitude, this.longitude),
            draggable: false,
            map: this.getGoogleMapInstance(),
            labelContent: this.labelObject.text,
            labelInBackground: true,
            labelAnchor: this.iconeObject.labelOrigin,
            labelClass: "map-marker-label " + MapaElemento.getNextIdentifier(),
            labelVisible: this.labelObject != null
        });

    }

    public removeFromGoogleMap()
    {
        this.marker.setMap(null);
    }

    public updateState()
    {
        this.render();
    }

    public addClickListener(handler : (...args: any[]) => void)
    {
        this.marker.addListener('click', handler);
    }

}