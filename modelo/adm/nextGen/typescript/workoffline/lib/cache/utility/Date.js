"use strict";
exports.__esModule = true;
var Utility_Date = (function () {
    function Utility_Date() {
    }
    Utility_Date.getTimeInSeconds = function () {
        // Get the current date
        var time = new Date();
        return (time.getTime() / 1000);
    };
    return Utility_Date;
}());
exports.Utility_Date = Utility_Date;
