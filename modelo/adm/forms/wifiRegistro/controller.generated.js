

        /*

        Arquivo gerado atrav�s de gerador de c�digo em 07/09/2017 as 12:49:44.
        Para que o arquivo n�o seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

        Tabela correspondente: wifi_registro
        Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

        */

omegaApp.generatedFormWifiRegistroController = function($scope, $rootScope, $window, $interval, $timeout, dataService, Upload, vm) {try { vm.fieldsTypes = {};

vm.formData = {
        wifiId: null,
usuarioId: null,
dataInicioSec: null,
dataInicioOffsec: null,
dataFimSec: null,
dataFimOffsec: null,
};

vm.disabledFields = {
        fieldWifiId: false,
fieldUsuarioId: false,
fieldDataInicioSec: false,
fieldDataInicioOffsec: false,
fieldDataFimSec: false,
fieldDataFimOffsec: false,
};

vm.requiredFields = {
        fieldWifiId: true,
fieldUsuarioId: true,
fieldDataInicioSec: true,
fieldDataInicioOffsec: true,
fieldDataFimSec: true,
fieldDataFimOffsec: true,
};

vm.relatedEntities =
        {
        };

vm.fieldsParameters = {
        fieldWifiId: {remoteData: null},
fieldUsuarioId: {remoteData: null},
fieldDataInicioSec: {popupAberto: false, datePickerParams: angular.copy(FieldsParametersUtil.defaultDatePickerParams)},
fieldDataInicioOffsec: null,
fieldDataFimSec: {popupAberto: false, datePickerParams: angular.copy(FieldsParametersUtil.defaultDatePickerParams)},
fieldDataFimOffsec: null,
};

vm.submitForm = function(formInstance)
        {
            if($rootScope.validateForm(formInstance))
            {
                var formData = angular.merge({}, vm.formData, GeneralUtil.getRelatedEntitiesData(vm.relatedEntities));
                if (!GeneralUtil.isNullOrEmpty(vm.formData.id))
                {
                    dataService.editWifiRegistro(formData).then(function(returnContent)
                    {
                        if (GeneralUtil.validateRemoteResponse(returnContent))
                        {
                            $rootScope.showDialog(vm, 'after-wifiRegistro-update-template.html');
                        }
                        else
                        {
                            $rootScope.instantErrorMessages.push(returnContent.data.mMensagem);
                        }
                    });
                }
                else
                {
                    dataService.addWifiRegistro(formData).then(function(returnContent)
                    {
                        if (GeneralUtil.validateRemoteResponse(returnContent))
                        {
                            $rootScope.showDialog(vm, 'after-wifiRegistro-insert-template.html');
                        }
                        else
                        {
                            $rootScope.instantErrorMessages.push(returnContent.data.mMensagem);
                        }
                    });
                }
            }

        };

if (!GeneralUtil.isNullOrEmpty(__GET) && !GeneralUtil.isNullOrEmpty(__GET.id))
        {

            var filterData = { id: __GET.id };
            var filterOperators = { id: Constants.FilterOperator.EQUALS };
            var parameters = GeneralUtil.getFilterParameters(filterData, filterOperators);

            dataService.getWifiRegistro(parameters).then(function (returnContent)
            {
                if (GeneralUtil.validateRemoteResponse(returnContent))
                {
                    vm.formData = returnContent.data.mObj;
                }
                else
                {
                    $rootScope.instantErrorMessages.push(returnContent.data.mMensagem);
                }

            });
        }

vm.openWifiIdModal = function()
                {
                    $rootScope.uibModalInstance = $rootScope.showModalDialog('forms', 'Wifi');
                    
                    //atualizando valores da lista
                    var unregisterModal = $rootScope.$watch(function() { return $rootScope.uibModalInstance }, function () {
    
                        if($rootScope.uibModalInstance == null)
                        {
                            vm.getListOfWifiId();
                            vm.formData.wifiId = null;
                            unregisterModal();
                        }
    
                    });
                    
                };
                vm.openUsuarioIdModal = function()
                {
                    $rootScope.uibModalInstance = $rootScope.showModalDialog('forms', 'Usuario');
                    
                    //atualizando valores da lista
                    var unregisterModal = $rootScope.$watch(function() { return $rootScope.uibModalInstance }, function () {
    
                        if($rootScope.uibModalInstance == null)
                        {
                            vm.getListOfUsuarioId();
                            vm.formData.usuarioId = null;
                            unregisterModal();
                        }
    
                    });
                    
                };
                

vm.getListOfWifiId = function()
                {
                    dataService.getListOfWifi().then(function (returnContent)
                    {
                        if (GeneralUtil.validateRemoteResponse(returnContent))
                        {
                            vm.fieldsParameters.fieldWifiId.remoteData = returnContent.data.mObj.gridData;
                        }
                        else
                        {
                            $rootScope.instantErrorMessages.push(returnContent.data.mMensagem);
                        }

                    });

                };
                vm.getListOfUsuarioId = function()
                {
                    dataService.getListOfUsuario().then(function (returnContent)
                    {
                        if (GeneralUtil.validateRemoteResponse(returnContent))
                        {
                            vm.fieldsParameters.fieldUsuarioId.remoteData = returnContent.data.mObj.gridData;
                        }
                        else
                        {
                            $rootScope.instantErrorMessages.push(returnContent.data.mMensagem);
                        }

                    });

                };
                

vm.loadListOfWifiRegistro = function()
        {
            document.location.href = 'index.php?tipo=lists&page=wifiRegistro';
        };

        vm.addNewWifiRegistro = function()
        {
            document.location.href = document.location.href;
        };

}
                        catch(ex)
                        {
                            $rootScope.instantErrorMessages.push(new Message('Erro no controller generatedFormWifiRegistroController', ex.message));
                        }

                }