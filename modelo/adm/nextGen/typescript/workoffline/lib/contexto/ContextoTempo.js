"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var contexto_1 = require("lib/contexto");
var ContextoTempo = (function (_super) {
    __extends(ContextoTempo, _super);
    function ContextoTempo(domRootSelector, mapContainerSelector, currentDateTime) {
        var _this = _super.call(this, domRootSelector, mapContainerSelector) || this;
        _this.currentDateTime = currentDateTime;
        return _this;
    }
    return ContextoTempo;
}(contexto_1.ContextoMapa));
exports.ContextoTempo = ContextoTempo;
