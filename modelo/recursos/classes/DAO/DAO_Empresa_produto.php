<?php


        /*

        Arquivo gerado através de gerador de código em 12/05/2018 as 14:35:15.
        Para que o arquivo não seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

        Tabela correspondente: empresa_produto
        Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

        */
        


class DAO_Empresa_produto extends Generic_DAO
                        {
                            const NOME_ENTIDADE = "empresa_produto";

                            public static $nomeTabela = null;
                            public static $idSistemaTabela = false;
                            public static $databaseFieldsRelatedAttributes = null;
	                        public static $databaseFieldTypes = null;
	                        public static $databaseFieldNames = null;
	                        public static $listAliasTypes = null;
	                        public static $listAliasRelatedAttributes = null;
	                        
	                        public $id;
public $empresaId;
public $cadastroSec;
public $cadastroOffsec;
public $precoCustoFloat;
public $precoVendaFloat;
public $estoqueAtualInt;
public $estoqueMinimoInt;
public $prazoReposicaoEstoqueDiasInt;
public $codigoBarra;
public $produtoId;
public $corporacaoId;

public $labelId;
public $labelEmpresaId;
public $labelCadastroSec;
public $labelCadastroOffsec;
public $labelPrecoCustoFloat;
public $labelPrecoVendaFloat;
public $labelEstoqueAtualInt;
public $labelEstoqueMinimoInt;
public $labelPrazoReposicaoEstoqueDiasInt;
public $labelCodigoBarra;
public $labelProdutoId;
public $labelCorporacaoId;

public static $arrPK = array("id");

public function __construct($configDAO = null)
        {
            parent::__construct($configDAO);
            static::__setStaticValues($this->database);           
        }

public static function idSistemaTabela($databaseObject = null){
            if(!(static::$idSistemaTabela))
            {
                static::$idSistemaTabela = EXTDAO_Sistema_tabela::getIdDaSistemaTabela(static::nomeTabela(), $databaseObject);
            }
            return static::$idSistemaTabela;
        }

public function getIdSistemaTabela(){
            return static::idSistemaTabela();
        }

public static function nomeTabela(){
            if(is_null(static::$nomeTabela))
            {
                static::$nomeTabela = "empresa_produto";
            }
            return static::$nomeTabela;
        }

public function getNomeTabela(){
            return static::nomeTabela();
        }

public static function __setStaticValues($databaseObject = null)
                   {    
                        static::nomeTabela();
                        
                        static::idSistemaTabela($databaseObject);
                   
                        static::setDatabaseFieldNames();
                        static::setDatabaseFieldTypes();
                        static::setDatabaseFieldsRelatedAttributeNames();
                        
                        static::setListAliasTypes();
                        static::setListAliasRelatedAttributes();
                   }

public function getExpressaoLabel()
                   {
                        return "{id}";
                   }

public static function setDatabaseFieldNames()
        {
            if(is_null(static::$databaseFieldNames))
            {
                static::$databaseFieldNames = new stdClass();
                static::$databaseFieldNames->id = "id";
static::$databaseFieldNames->empresaId = "empresa_id_INT";
static::$databaseFieldNames->cadastroSec = "cadastro_SEC";
static::$databaseFieldNames->cadastroOffsec = "cadastro_OFFSEC";
static::$databaseFieldNames->precoCustoFloat = "preco_custo_FLOAT";
static::$databaseFieldNames->precoVendaFloat = "preco_venda_FLOAT";
static::$databaseFieldNames->estoqueAtualInt = "estoque_atual_INT";
static::$databaseFieldNames->estoqueMinimoInt = "estoque_minimo_INT";
static::$databaseFieldNames->prazoReposicaoEstoqueDiasInt = "prazo_reposicao_estoque_dias_INT";
static::$databaseFieldNames->codigoBarra = "codigo_barra";
static::$databaseFieldNames->produtoId = "produto_id_INT";
static::$databaseFieldNames->corporacaoId = "corporacao_id_INT";

            }
                      
        }

public static function setDatabaseFieldTypes()
        {
            if(is_null(static::$databaseFieldTypes))
            {
                static::$databaseFieldTypes = new stdClass();
                static::$databaseFieldTypes->id = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->empresaId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->cadastroSec = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$databaseFieldTypes->cadastroOffsec = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$databaseFieldTypes->precoCustoFloat = static::TIPO_VARIAVEL_FLOAT;
static::$databaseFieldTypes->precoVendaFloat = static::TIPO_VARIAVEL_FLOAT;
static::$databaseFieldTypes->estoqueAtualInt = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->estoqueMinimoInt = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->prazoReposicaoEstoqueDiasInt = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->codigoBarra = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->produtoId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->corporacaoId = static::TIPO_VARIAVEL_INTEGER;

            }
            
        }

public static function setDatabaseFieldsRelatedAttributeNames()
        {
            if(is_null(static::$databaseFieldsRelatedAttributes))
            {
                static::$databaseFieldsRelatedAttributes = new stdClass();
                static::$databaseFieldsRelatedAttributes->id = "id";
static::$databaseFieldsRelatedAttributes->empresa_id_INT = "empresaId";
static::$databaseFieldsRelatedAttributes->cadastro_SEC = "cadastroSec";
static::$databaseFieldsRelatedAttributes->cadastro_OFFSEC = "cadastroOffsec";
static::$databaseFieldsRelatedAttributes->preco_custo_FLOAT = "precoCustoFloat";
static::$databaseFieldsRelatedAttributes->preco_venda_FLOAT = "precoVendaFloat";
static::$databaseFieldsRelatedAttributes->estoque_atual_INT = "estoqueAtualInt";
static::$databaseFieldsRelatedAttributes->estoque_minimo_INT = "estoqueMinimoInt";
static::$databaseFieldsRelatedAttributes->prazo_reposicao_estoque_dias_INT = "prazoReposicaoEstoqueDiasInt";
static::$databaseFieldsRelatedAttributes->codigo_barra = "codigoBarra";
static::$databaseFieldsRelatedAttributes->produto_id_INT = "produtoId";
static::$databaseFieldsRelatedAttributes->corporacao_id_INT = "corporacaoId";

            }           
        }

public function setAllFieldsToHumanFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["empresa_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["cadastro_SEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["cadastro_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["preco_custo_FLOAT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["preco_venda_FLOAT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["estoque_atual_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["estoque_minimo_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["prazo_reposicao_estoque_dias_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["codigo_barra"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["produto_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
}

public function setAllFieldsToDatabaseFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["empresa_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["cadastro_SEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["cadastro_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["preco_custo_FLOAT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["preco_venda_FLOAT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["estoque_atual_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["estoque_minimo_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["prazo_reposicao_estoque_dias_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["codigo_barra"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["produto_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
}

  public function getFkObjEmpresa() 
                {
                    if($this->objEmpresa == null)
                    {                        
                        $this->objEmpresa = new EXTDAO_Empresa_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getEmpresa_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objEmpresa->clear();
                    }
                    elseif($this->objEmpresa->getId() != $idFK)
                    {
                        $this->objEmpresa->select($idFK);
                    }
                    return $this->objEmpresa;
                }
  public function getFkObjProduto() 
                {
                    if($this->objProduto == null)
                    {                        
                        $this->objProduto = new EXTDAO_Produto_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getProduto_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objProduto->clear();
                    }
                    elseif($this->objProduto->getId() != $idFK)
                    {
                        $this->objProduto->select($idFK);
                    }
                    return $this->objProduto;
                }
  public function getFkObjCorporacao() 
                {
                    if($this->objCorporacao == null)
                    {                        
                        $this->objCorporacao = new EXTDAO_Corporacao_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getCorporacao_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objCorporacao->clear();
                    }
                    elseif($this->objCorporacao->getId() != $idFK)
                    {
                        $this->objCorporacao->select($idFK);
                    }
                    return $this->objCorporacao;
                }


public function setLabels()
        {$this->labelId = "id";
$this->labelEmpresaId = "empresaidINT";
$this->labelCadastroSec = "";
$this->labelCadastroOffsec = "";
$this->labelPrecoCustoFloat = "";
$this->labelPrecoVendaFloat = "";
$this->labelEstoqueAtualInt = "";
$this->labelEstoqueMinimoInt = "";
$this->labelPrazoReposicaoEstoqueDiasInt = "";
$this->labelCodigoBarra = "";
$this->labelProdutoId = "";
$this->labelCorporacaoId = "corporacaoidINT";
}

public function __actionAdd($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                $this->setByObject($parameters);
                $this->setDataCadastroAndOffsetInSecondsFromUser($parameters);
                
                $this->formatarParaSQL();
                $msg = $this->insert(true);
                
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                return new Mensagem(null, I18N::getExpression("Empresa produto adicionado com sucesso."));
                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionEdit($parameters = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try {

                $this->setByObject($parameters);
                $this->setAllFieldsToHumanFormat();
                
                $this->formatarParaSQL();
                $msg = $this->updateByInstanceUsingParameters($this->id, $parameters, true);
                if($msg != null && $msg->erro())
                {
                     return $msg;
                }
        
                return new Mensagem(null, I18N::getExpression("Empresa produto editado com sucesso."));
            }
            catch(Exception $ex)
            {                
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionRemove($parameters = null)
        {
            $successulRemovalCount = 0;
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                foreach($parameters as $record)
                {
                    if(is_numeric($record->id))
                    {
                        $msg = $this->delete($record->id, true);
                        if($msg == null || $msg->ok())
                        {
                            $successulRemovalCount++;
                        }
                    }
                }
                
                if($successulRemovalCount > 0)
                {
                    $successMessage = count($successulRemovalCount) > 1 ? I18N::getExpression("{0} Empresa produto foram removidos com sucesso.", $successulRemovalCount) : I18N::getExpression("Empresa produto removido com sucesso.");
                    return new Mensagem(null, $successMessage);
                }
                else
                {
                    $errorMessage = count($parameters) > 1 ? I18N::getExpression("Falha ao remover Empresa produto.") : I18N::getExpression("Falha ao remover Empresa produto.");
                    return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, $errorMessage);                    
                }
            }
            catch (Exception $ex) 
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }

public function __getRecord($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }
            
            try
            {            
                $returnObject = new stdClass();            
                $whereClause = static::getWhereClauseForFilter($parameters->filterParameters);
                $query = "SELECT  id, empresa_id_INT, cadastro_SEC, cadastro_OFFSEC, preco_custo_FLOAT, preco_venda_FLOAT, estoque_atual_INT, estoque_minimo_INT, prazo_reposicao_estoque_dias_INT, codigo_barra, produto_id_INT, corporacao_id_INT FROM empresa_produto {$whereClause}";
    
                $msg = $this->database->queryMensagem($query);                
                if ($msg == null)
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Não foi encontrado nenhum registro"));
                    }                    
                    $resultSet = $this->database->result;
                    $resultObject = static::mapResultSetDataToRecord($resultSet);
                    
                    $returnObject->formData = $resultObject;
                    $returnObject->comboBoxesData = null;
                    if($parameters->loadComboBoxesData)
                    {
                        $this->setByObject($resultObject);
                        $returnObject->comboBoxesData =  $this->getFormComboBoxesData();
                    }                    
    
                    
    
                    return new Mensagem_generica($returnObject);
                } 
                else 
                {
                    return $msg;
                }
                
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }


        
        public function __getFilterComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
                
        public function __getFormComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
        
        protected function getFormComboBoxesData()
        {
            $comboBoxesData = new stdClass();
            $listParameters = new stdClass();
            $listParameters->listMappingType = static::LIST_MAPPING_TYPE_COMBOBOX;
            
            
                    
                    $objEmpresa = new EXTDAO_Empresa();
                    $comboBoxesData->fieldEmpresaId = $objEmpresa->__getList($listParameters);

            return $comboBoxesData;
        }
        
        


        
        public static function setListAliasTypes()
        {
            if(is_null(static::$listAliasTypes))
            {
                static::$listAliasTypes = new stdClass();
                static::$listAliasTypes->empresa__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->empresa_produto__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->empresa_produto__ind_celular_valido_BOOLEAN = static::TIPO_VARIAVEL_BOOLEAN;
static::$listAliasTypes->empresa_produto__cadastro_SEC = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$listAliasTypes->empresa_produto__cadastro_OFFSEC = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
            }        
        }
        
        public static function setListAliasRelatedAttributes()
        {
            if(is_null(static::$listAliasRelatedAttributes))
            {
                static::$listAliasRelatedAttributes = new stdClass();
                static::$listAliasRelatedAttributes->empresa__id = "empresaId";
static::$listAliasRelatedAttributes->empresa_produto__id = "id";
static::$listAliasRelatedAttributes->empresa_produto__ind_celular_valido_BOOLEAN = "indCelularValidoBoolean";
static::$listAliasRelatedAttributes->empresa_produto__cadastro_SEC = "cadastroSec";
static::$listAliasRelatedAttributes->empresa_produto__cadastro_OFFSEC = "cadastroOffsec";
            }         
        }

        public function __getList($parameters = null, $idCorporacao = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try
            {
                $filterParameters = new stdClass();
                if(isset($parameters->filterParameters))
                {
                    $filterParameters = $parameters->filterParameters;
                }
                                    
                            
            if(is_null($idCorporacao))
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            ListParameters::addFilterParameterToObject($filterParameters, "corporacaoId", $idCorporacao);           
                
                
                $sortingParameters = null;
                if(isset($parameters->sortingParameters))
                {
                    $sortingParameters = $parameters->sortingParameters;
                }
                
                $paginationParameters = null;
                if(isset($parameters->paginationParameters))
                {
                    $paginationParameters = $parameters->paginationParameters;
                }
                
                $listMappingType = static::getListMappingType($parameters->listMappingType);
                $mainTableAlias = "ep";

                $whereClause = static::getWhereClauseForFilter($filterParameters, $mainTableAlias);
                $orderByClause = static::getOrderByClauseForFilter($sortingParameters, $mainTableAlias);
                $limitClause = static::getLimitClauseForFilter($paginationParameters);

                $queryWithoutLimit = "SELECT ep.id FROM empresa_produto ep {$whereClause}";
                $query = "SELECT e.id AS empresa__id, ep.id AS empresa_produto__id, ep.ind_celular_valido_BOOLEAN AS empresa_produto__ind_celular_valido_BOOLEAN, ep.cadastro_SEC AS empresa_produto__cadastro_SEC, ep.cadastro_OFFSEC AS empresa_produto__cadastro_OFFSEC FROM empresa_produto ep LEFT JOIN empresa e ON e.id = ep.empresa_id_INT {$whereClause} {$orderByClause} {$limitClause}";

                $msg = $this->database->queryMensagem($queryWithoutLimit);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                
                $totalNumberOfRecords = $this->database->rows();
                if($totalNumberOfRecords == 0)
                {
                    return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                }                        
                        
                $msg = $this->database->queryMensagem($query);
                
                //operacao realizaca com sucesso
                if ($msg == null) 
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                    }                        
                
                    $resultSet = $this->database->result;

                    $objReturn = new stdClass();
                    $objReturn->dataSet = static::mapResultSetDataToList($resultSet, $listMappingType, static::$listAliasTypes);

                    if(!Helper::isNullOrEmpty($paginationParameters->recordsPerPage) && !Helper::isNullOrEmpty($paginationParameters->currentPage))
                    {
                        $lastPage = ceil($totalNumberOfRecords / $paginationParameters->recordsPerPage);
                        $objReturn->paginationParameters = new PaginationParams($paginationParameters->recordsPerPage, $paginationParameters->currentPage, $lastPage);
                    }
                    
                    return new Mensagem_generica($objReturn);
                } 
                else
                {
                    return $msg;
                }                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }



public function getId()
            {
                return $this->id;
            }

public function getEmpresaId()
            {
                return $this->empresaId;
            }

public function getEmpresa_id_INT()
                {
                    return $this->empresaId;
                }

public function getCadastroSec()
            {
                return $this->cadastroSec;
            }

public function getCadastro_SEC()
                {
                    return $this->cadastroSec;
                }

public function getCadastroOffsec()
            {
                return $this->cadastroOffsec;
            }

public function getCadastro_OFFSEC()
                {
                    return $this->cadastroOffsec;
                }

public function getPrecoCustoFloat()
            {
                return $this->precoCustoFloat;
            }

public function getPreco_custo_FLOAT()
                {
                    return $this->precoCustoFloat;
                }

public function getPrecoVendaFloat()
            {
                return $this->precoVendaFloat;
            }

public function getPreco_venda_FLOAT()
                {
                    return $this->precoVendaFloat;
                }

public function getEstoqueAtualInt()
            {
                return $this->estoqueAtualInt;
            }

public function getEstoque_atual_INT()
                {
                    return $this->estoqueAtualInt;
                }

public function getEstoqueMinimoInt()
            {
                return $this->estoqueMinimoInt;
            }

public function getEstoque_minimo_INT()
                {
                    return $this->estoqueMinimoInt;
                }

public function getPrazoReposicaoEstoqueDiasInt()
            {
                return $this->prazoReposicaoEstoqueDiasInt;
            }

public function getPrazo_reposicao_estoque_dias_INT()
                {
                    return $this->prazoReposicaoEstoqueDiasInt;
                }

public function getCodigoBarra()
            {
                return $this->codigoBarra;
            }

public function getCodigo_barra()
                {
                    return $this->codigoBarra;
                }

public function getProdutoId()
            {
                return $this->produtoId;
            }

public function getProduto_id_INT()
                {
                    return $this->produtoId;
                }

public function getCorporacaoId()
            {
                return $this->corporacaoId;
            }

public function getCorporacao_id_INT()
                {
                    return $this->corporacaoId;
                }



function setId($value)
            {
                $this->id = $value;
            }

function setEmpresaId($value)
            {
                $this->empresaId = $value;
            }

public function setEmpresa_id_INT($value)
                { 
                    $this->empresaId = $value; 
                }

function setCadastroSec($value)
            {
                $this->cadastroSec = $value;
            }

public function setCadastro_SEC($value)
                { 
                    $this->cadastroSec = $value; 
                }

function setCadastroOffsec($value)
            {
                $this->cadastroOffsec = $value;
            }

public function setCadastro_OFFSEC($value)
                { 
                    $this->cadastroOffsec = $value; 
                }

function setPrecoCustoFloat($value)
            {
                $this->precoCustoFloat = $value;
            }

public function setPreco_custo_FLOAT($value)
                { 
                    $this->precoCustoFloat = $value; 
                }

function setPrecoVendaFloat($value)
            {
                $this->precoVendaFloat = $value;
            }

public function setPreco_venda_FLOAT($value)
                { 
                    $this->precoVendaFloat = $value; 
                }

function setEstoqueAtualInt($value)
            {
                $this->estoqueAtualInt = $value;
            }

public function setEstoque_atual_INT($value)
                { 
                    $this->estoqueAtualInt = $value; 
                }

function setEstoqueMinimoInt($value)
            {
                $this->estoqueMinimoInt = $value;
            }

public function setEstoque_minimo_INT($value)
                { 
                    $this->estoqueMinimoInt = $value; 
                }

function setPrazoReposicaoEstoqueDiasInt($value)
            {
                $this->prazoReposicaoEstoqueDiasInt = $value;
            }

public function setPrazo_reposicao_estoque_dias_INT($value)
                { 
                    $this->prazoReposicaoEstoqueDiasInt = $value; 
                }

function setCodigoBarra($value)
            {
                $this->codigoBarra = $value;
            }

public function setCodigo_barra($value)
                { 
                    $this->codigoBarra = $value; 
                }

function setProdutoId($value)
            {
                $this->produtoId = $value;
            }

public function setProduto_id_INT($value)
                { 
                    $this->produtoId = $value; 
                }

function setCorporacaoId($value)
            {
                $this->corporacaoId = $value;
            }

public function setCorporacao_id_INT($value)
                { 
                    $this->corporacaoId = $value; 
                }



public function clear()
        {$this->id = null;
$this->empresaId = null;
if($this->objEmpresa != null) unset($this->objEmpresa);
$this->cadastroSec = null;
$this->cadastroOffsec = null;
$this->precoCustoFloat = null;
$this->precoVendaFloat = null;
$this->estoqueAtualInt = null;
$this->estoqueMinimoInt = null;
$this->prazoReposicaoEstoqueDiasInt = null;
$this->codigoBarra = null;
$this->produtoId = null;
if($this->objProduto != null) unset($this->objProduto);
$this->corporacaoId = null;
if($this->objCorporacao != null) unset($this->objCorporacao);
if(is_array($this->arrEstadosCampos)){
                        unset($this->arrEstadosCampos);
                     $this->arrEstadosCampos = array();
                    }
                }

public function formatarParaSQL()
        {
if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->id)){
$this->id = $this->formatarIntegerParaComandoSQL($this->id);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->id);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->empresaId)){
$this->empresaId = $this->formatarIntegerParaComandoSQL($this->empresaId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->empresaId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->cadastroSec)){
$this->cadastroSec = $this->formatarIntegerParaComandoSQL($this->cadastroSec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->cadastroSec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->cadastroOffsec)){
$this->cadastroOffsec = $this->formatarIntegerParaComandoSQL($this->cadastroOffsec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->cadastroOffsec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->precoCustoFloat)){
$this->precoCustoFloat = $this->formatarFloatParaComandoSQL($this->precoCustoFloat);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->precoCustoFloat);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->precoVendaFloat)){
$this->precoVendaFloat = $this->formatarFloatParaComandoSQL($this->precoVendaFloat);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->precoVendaFloat);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->estoqueAtualInt)){
$this->estoqueAtualInt = $this->formatarIntegerParaComandoSQL($this->estoqueAtualInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->estoqueAtualInt);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->estoqueMinimoInt)){
$this->estoqueMinimoInt = $this->formatarIntegerParaComandoSQL($this->estoqueMinimoInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->estoqueMinimoInt);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->prazoReposicaoEstoqueDiasInt)){
$this->prazoReposicaoEstoqueDiasInt = $this->formatarIntegerParaComandoSQL($this->prazoReposicaoEstoqueDiasInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->prazoReposicaoEstoqueDiasInt);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->codigoBarra)){
$this->codigoBarra = $this->formatarStringParaComandoSQL($this->codigoBarra);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->codigoBarra);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->produtoId)){
$this->produtoId = $this->formatarIntegerParaComandoSQL($this->produtoId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->produtoId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->corporacaoId)){
$this->corporacaoId = $this->formatarIntegerParaComandoSQL($this->corporacaoId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->corporacaoId);
}

}

public function formatarParaExibicao()
        {if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->precoCustoFloat)){
$this->precoCustoFloat = $this->formatarFloatParaExibicao($this->precoCustoFloat);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->precoCustoFloat);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->precoVendaFloat)){
$this->precoVendaFloat = $this->formatarFloatParaExibicao($this->precoVendaFloat);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->precoVendaFloat);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->codigoBarra)){
$this->codigoBarra = $this->formatarStringParaExibicao($this->codigoBarra);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->codigoBarra);
}

}

function select($id, $idCorporacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }   

            $sql = "SELECT id, empresa_id_INT, cadastro_SEC, cadastro_OFFSEC, preco_custo_FLOAT, preco_venda_FLOAT, estoque_atual_INT, estoque_minimo_INT, prazo_reposicao_estoque_dias_INT, codigo_barra, produto_id_INT, corporacao_id_INT FROM empresa_produto WHERE id = $id AND corporacao_id_INT = $idCorporacao";

            $msg = $this->database->queryMensagem($sql);
            $result = $this->database->result;

            if($msg != null && $msg->erro() )
            { 
            		$this->clear();
            		return $msg;
        	} 
        	else if($this->database->rows() == 0)
        	{
                return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nenhum registro foi encontrado."));
            }
        

            $row = $this->database->fetchArray(MYSQLI_NUM);
            
            		$this->id = $row[0];
		$this->empresaId = $row[1];
		$this->cadastroSec = $row[2];
		$this->cadastroOffsec = $row[3];
		$this->precoCustoFloat = $row[4];
		$this->precoVendaFloat = $row[5];
		$this->estoqueAtualInt = $row[6];
		$this->estoqueMinimoInt = $row[7];
		$this->prazoReposicaoEstoqueDiasInt = $row[8];
		$this->codigoBarra = $row[9];
		$this->produtoId = $row[10];

            
        }

public function delete($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (is_null($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }

            $sql = "DELETE FROM empresa_produto WHERE id= $id AND corporacao_id_INT = $idCorporacao";
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                    && $sincronizar
                        && static::idSistemaTabela() != null
                            && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_REMOVE,
                                null,
                                null,
                                $id,
                                $this->database);
            }

            return $msg;
        }

public function insert($sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null, $opcoes = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $this->defineDataCadastroInSecondsIfNotDefined();
$this->defineDataCadastroOffsetInSecondsIfNotDefined();

            
            if (is_null($opcoes))
            {
                $opcoes = $this->opcoesDAO;
            }

            if (!is_numeric($this->id))
            {
                $this->id = "null";
                if ($opcoes == null || $opcoes[Database::OPCAO_GERAR_ID] != false) 
                {
                    $this->id = EXTDAO_Sistema_sequencia::gerarId(static::nomeTabela());                    
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }    
                    else if($msg == null)
                    {
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
                else 
                {
                    $this->id = null;
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao));
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }
                    else if($msg == null)
                    {
                        $this->id = $this->database->getLastInsertId();
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
            }
            else 
            {
                $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }                        
                else if($msg == null)
                {
                   $msg = new Mensagem_token();
                   $msg->mValor = $this->id;
                }
            }
            if($msg == null || $msg->ok())
            {                
                if($sincronizar
                    && static::idSistemaTabela() != null
                    && isset($this->id)){
                    
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_INSERT,
                                null,
                                null,
                                $this->id,
                                $this->database); 
                	}
            } 
            else
            {
                $this->id = null;
            }
            
            return $msg;
            
        }

public function getSQLInsert($idCorporacao){ 

return "INSERT INTO empresa_produto (id, empresa_id_INT, cadastro_SEC, cadastro_OFFSEC, preco_custo_FLOAT, preco_venda_FLOAT, estoque_atual_INT, estoque_minimo_INT, prazo_reposicao_estoque_dias_INT, codigo_barra, produto_id_INT, corporacao_id_INT) VALUES ( $this->id ,  $this->empresaId ,  $this->cadastroSec ,  $this->cadastroOffsec ,  $this->precoCustoFloat ,  $this->precoVendaFloat ,  $this->estoqueAtualInt ,  $this->estoqueMinimoInt ,  $this->prazoReposicaoEstoqueDiasInt ,  $this->codigoBarra ,  $this->produtoId ,  $idCorporacao ) "; 

}




        
        public function updateByParameters($id, $objParametros, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao))
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();

            
                
                if (isset($objParametros->empresaId)) 
                {
                    $arrUpdateFields[] = " empresa_id_INT = {$objParametros->empresaId} ";
                }


                
                if (isset($objParametros->cadastroSec)) 
                {
                    $arrUpdateFields[] = " cadastro_SEC = {$objParametros->cadastroSec} ";
                }


                
                if (isset($objParametros->cadastroOffsec)) 
                {
                    $arrUpdateFields[] = " cadastro_OFFSEC = {$objParametros->cadastroOffsec} ";
                }


                
                if (isset($objParametros->precoCustoFloat)) 
                {
                    $arrUpdateFields[] = " preco_custo_FLOAT = {$objParametros->precoCustoFloat} ";
                }


                
                if (isset($objParametros->precoVendaFloat)) 
                {
                    $arrUpdateFields[] = " preco_venda_FLOAT = {$objParametros->precoVendaFloat} ";
                }


                
                if (isset($objParametros->estoqueAtualInt)) 
                {
                    $arrUpdateFields[] = " estoque_atual_INT = {$objParametros->estoqueAtualInt} ";
                }


                
                if (isset($objParametros->estoqueMinimoInt)) 
                {
                    $arrUpdateFields[] = " estoque_minimo_INT = {$objParametros->estoqueMinimoInt} ";
                }


                
                if (isset($objParametros->prazoReposicaoEstoqueDiasInt)) 
                {
                    $arrUpdateFields[] = " prazo_reposicao_estoque_dias_INT = {$objParametros->prazoReposicaoEstoqueDiasInt} ";
                }


                
                if (isset($objParametros->codigoBarra)) 
                {
                    $arrUpdateFields[] = " codigo_barra = {$objParametros->codigoBarra} ";
                }


                
                if (isset($objParametros->produtoId)) 
                {
                    $arrUpdateFields[] = " produto_id_INT = {$objParametros->produtoId} ";
                }



            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE empresa_produto SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";

            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null)
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function update($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($this->empresaId)) 
                {                                      
                    $arrUpdateFields[] = " empresa_id_INT = {$this->empresaId} ";
                }


                
                if (isset($this->cadastroSec)) 
                {                                      
                    $arrUpdateFields[] = " cadastro_SEC = {$this->cadastroSec} ";
                }


                
                if (isset($this->cadastroOffsec)) 
                {                                      
                    $arrUpdateFields[] = " cadastro_OFFSEC = {$this->cadastroOffsec} ";
                }


                
                if (isset($this->precoCustoFloat)) 
                {                                      
                    $arrUpdateFields[] = " preco_custo_FLOAT = {$this->precoCustoFloat} ";
                }


                
                if (isset($this->precoVendaFloat)) 
                {                                      
                    $arrUpdateFields[] = " preco_venda_FLOAT = {$this->precoVendaFloat} ";
                }


                
                if (isset($this->estoqueAtualInt)) 
                {                                      
                    $arrUpdateFields[] = " estoque_atual_INT = {$this->estoqueAtualInt} ";
                }


                
                if (isset($this->estoqueMinimoInt)) 
                {                                      
                    $arrUpdateFields[] = " estoque_minimo_INT = {$this->estoqueMinimoInt} ";
                }


                
                if (isset($this->prazoReposicaoEstoqueDiasInt)) 
                {                                      
                    $arrUpdateFields[] = " prazo_reposicao_estoque_dias_INT = {$this->prazoReposicaoEstoqueDiasInt} ";
                }


                
                if (isset($this->codigoBarra)) 
                {                                      
                    $arrUpdateFields[] = " codigo_barra = {$this->codigoBarra} ";
                }


                
                if (isset($this->produtoId)) 
                {                                      
                    $arrUpdateFields[] = " produto_id_INT = {$this->produtoId} ";
                }


            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE empresa_produto SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function updateByInstanceUsingParameters($id, $parameters, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($parameters->empresaId)) 
                {                                      
                    $arrUpdateFields[] = " empresa_id_INT = {$this->empresaId} ";
                }
                
                if (isset($parameters->cadastroSec)) 
                {                                      
                    $arrUpdateFields[] = " cadastro_SEC = {$this->cadastroSec} ";
                }
                
                if (isset($parameters->cadastroOffsec)) 
                {                                      
                    $arrUpdateFields[] = " cadastro_OFFSEC = {$this->cadastroOffsec} ";
                }
                
                if (isset($parameters->precoCustoFloat)) 
                {                                      
                    $arrUpdateFields[] = " preco_custo_FLOAT = {$this->precoCustoFloat} ";
                }
                
                if (isset($parameters->precoVendaFloat)) 
                {                                      
                    $arrUpdateFields[] = " preco_venda_FLOAT = {$this->precoVendaFloat} ";
                }
                
                if (isset($parameters->estoqueAtualInt)) 
                {                                      
                    $arrUpdateFields[] = " estoque_atual_INT = {$this->estoqueAtualInt} ";
                }
                
                if (isset($parameters->estoqueMinimoInt)) 
                {                                      
                    $arrUpdateFields[] = " estoque_minimo_INT = {$this->estoqueMinimoInt} ";
                }
                
                if (isset($parameters->prazoReposicaoEstoqueDiasInt)) 
                {                                      
                    $arrUpdateFields[] = " prazo_reposicao_estoque_dias_INT = {$this->prazoReposicaoEstoqueDiasInt} ";
                }
                
                if (isset($parameters->codigoBarra)) 
                {                                      
                    $arrUpdateFields[] = " codigo_barra = {$this->codigoBarra} ";
                }
                
                if (isset($parameters->produtoId)) 
                {                                      
                    $arrUpdateFields[] = " produto_id_INT = {$this->produtoId} ";
                }
            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE empresa_produto SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }

}

?>
