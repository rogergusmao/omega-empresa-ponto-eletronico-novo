<?php


        /*

        Arquivo gerado através de gerador de código em 07/09/2017 as 01:32:16.
        Para que o arquivo não seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

        Tabela correspondente: usuario_tipo_privilegio
        Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

        */
?>

<!--Definição da View Angular JS -->
                   <?

                    $objForm = new FormEnvironment("mainForm", "formUsuarioTipoPrivilegioController", "vm");
                    $objForm->setLayoutOneColumn();

                   ?><div class='row' id='content-wrapper' ng-controller="<?=$objForm->getAngularControllerAttributeValue() ?>">

                            <!--Definição do template do dialog exibido após inserção -->
                   <script type="text/ng-template" id="after-usuarioTipoPrivilegio-insert-template.html">
                        <div class="modal-header">
                            <h4 class="modal-title">
                                <span class="glyphicon"></span>
                                <?=I18N::getExpression("Usuario tipo privilegio adicionado com sucesso!"); ?>
                            </h4>
                        </div>
                        <div class="modal-body">
                            <span class="help-block"><?=I18N::getExpression("O que deseja fazer agora?"); ?></span>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" ng-click="vm.loadListOfUsuarioTipoPrivilegio()">
                                <?=I18N::getExpression("Listar usuario tipo privilegio"); ?>
                            </button>
                            <button type="button" class="btn btn-default" ng-click="closeMessageDialog()">
                                <?=I18N::getExpression("Permanecer na tela"); ?>
                            </button>
                            <button type="button" class="btn btn-default" ng-click="vm.addNewUsuarioTipoPrivilegio()">
                                <?=I18N::getExpression("Adicionar usuario tipo privilegio"); ?>
                            </button>
                        </div>
                    </script>

                    <!--Definição do template do exibido dialog após update -->
                    <script type="text/ng-template" id="after-usuarioTipoPrivilegio-update-template.html">
                        <div class="modal-header">
                            <h4 class="modal-title">
                                <span class="glyphicon"></span>
                                <?=I18N::getExpression("Usuario tipo privilegio alterado com sucesso!"); ?>

                            </h4>
                        </div>
                        <div class="modal-body">
                            <span class="help-block"><?=I18N::getExpression("O que deseja fazer agora?"); ?></span>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" ng-click="vm.loadListOfUsuarioTipoPrivilegio()">
                                <?=I18N::getExpression("Listar usuario tipo privilegio"); ?>
                            </button>
                            <button type="button" class="btn btn-default" ng-click="closeMessageDialog()">
                                <?=I18N::getExpression("Permanecer na tela"); ?>
                            </button>
                            <button type="button" class="btn btn-default" ng-click="vm.addNewUsuarioTipoPrivilegio()">
                                <?=I18N::getExpression("Adicionar usuario tipo privilegio"); ?>
                            </button>
                        </div>
                    </script>

                            <div class='col-xs-12'>

                                <div class='row'>
                                    <div class='col-sm-12'>
                                        <div class='page-header'>
                                            <h1 class='pull-left'>
                                                <i class='icon-edit'></i>
                                                <span>
                                                    <?=I18N::getExpression("Cadastro de usuario tipo privilegio"); ?>
                                                </span>
                                            </h1>
                                            <div class='pull-right close-button-container'>
                                                <?=$objForm->renderCloseModalButtonIfExists(); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <?=$objForm->getFormDebugPanel(); ?>

                                <div class='row'>
                                    <div class='col-sm-12'>
                                        <div class='box'>
                                            <div class='box-header blue-background'>
                                                <div class='title'>
                                                    <div class='icon-edit'></div>
                                                    <?=I18N::getExpression("Dados do usuario tipo privilegio"); ?>
                                                </div>
                                                <div class='actions'>
                                                    <a class="btn box-collapse btn-xs btn-link" href="#"><i></i></a>
                                                </div>
                                            </div>
                                            <div class='box-content'>

                                            <form name="<?=$objForm->getFormName() ?>" class="form form-horizontal" style="margin-bottom: 0;">

                                                

                                                        <?php

                                                            $objCampo = $objForm->getSelectInstance("usuarioTipoId");
                            $objCampo->setLabel(I18N::getExpression("UsuariotipoidINT"));
                            
                            $objCampo->setPlaceHolder(I18N::getExpression("Selecione uma usuariotipoidINT da lista..."), false);
                            $objCampo->setMatchExpression("{{id}}");
                            $objCampo->setCloseOnSelect(true);
                            $objCampo->setRefreshDelay(0);

                            $propsFilter = new PropsFilter();
$propsFilter->addFieldToFilter("id");

                            $selectChoiceContent = new SelectChoiceContent("usuarioTipoId");$selectChoiceContent->setMainString(new AngularString("id", "div", Select::getSelectHighLightFilter()));


                            $objCampo->setFilter($propsFilter);
                            $objCampo->setSelectChoiceContent($selectChoiceContent);$objCampo->addValidation(new RequiredValidation(I18N::getExpression("A UsuariotipoidINT é de preenchimento obrigatório.")));

                                                        ?>

                                                        <div class='<?=$objCampo->getFormGroupCssClass() ?>' ng-class="<?=$objCampo->getFormGroupAngularClass(); ?>">

                                                            <?=$objCampo->renderLabel(); ?>
                                                            <div class='<?=$objCampo->getContainerCssClass(); ?>'>

                                                                <?=$objCampo->render(); ?>
                                                                <?=$objCampo->renderValidationBlock(); ?>

                                                            </div>

                                                            <div class='col-md-2'>

                            <?=$objCampo->renderAddEntityButton(I18N::getExpression("Adicionar UsuariotipoidINT")); ?>

                        </div>

                                                        </div>

                                                        <?=$objForm->renderHorizontalLine(); ?>

                                                        

                                                        <?php

                                                            $objCampo = $objForm->getTextInputInstance("identificadorFuncionalidade");
                            $objCampo->setLabel("Identificadorfuncionalidade");
                            
                            $objCampo->setDefaultValues();
                            $objCampo->setPlaceHolder("Informe a Identificadorfuncionalidade.");$objCampo->addValidation(new MaxLengthValidation(255, I18N::getExpression("O tamanho máximo da Identificadorfuncionalidade deve ser de {0} caracteres.")));$objCampo->addValidation(new RequiredValidation(I18N::getExpression("A Identificadorfuncionalidade é de preenchimento obrigatório.")));

                                                        ?>

                                                        <div class='<?=$objCampo->getFormGroupCssClass() ?>' ng-class="<?=$objCampo->getFormGroupAngularClass(); ?>">

                                                            <?=$objCampo->renderLabel(); ?>
                                                            <div class='<?=$objCampo->getContainerCssClass(); ?>'>

                                                                <?=$objCampo->render(); ?>
                                                                <?=$objCampo->renderValidationBlock(); ?>

                                                            </div>

                                                            

                                                        </div>

                                                        <?=$objForm->renderHorizontalLine(); ?>

                                                                <div class='form-actions form-actions-padding-sm'>
                                        <div class='row'>
                                            <div class='col-md-10 col-md-offset-2'>

                                                <?=$objForm->renderSubmitButton(); ?>

                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php

        if(!Helper::isAngularModal()){

            $arrDataServicesRelacionados = array("forms/usuarioTipo");
            $arrControllersRelacionados = array("forms/usuarioTipo");

        ?>

            <!--Definição do Controller Angular JS -->
            <script type="text/javascript">

                 <?=Helper::incluirConteudoDataServices(true, $arrDataServicesRelacionados); ?>

            </script>

            <?=Helper::incluirControllerAngularJS(true, $arrControllersRelacionados); ?>

        <?php } ?>

        
