"use strict";
exports.__esModule = true;
var PeriodoTempo = (function () {
    function PeriodoTempo(horaInicial, horaFinal) {
        this.horaInicial = horaInicial;
        this.horaFinal = horaFinal;
    }
    return PeriodoTempo;
}());
exports.PeriodoTempo = PeriodoTempo;
