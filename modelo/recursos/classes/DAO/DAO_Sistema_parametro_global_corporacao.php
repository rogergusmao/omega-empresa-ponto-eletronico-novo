<?php


        /*

        Arquivo gerado através de gerador de código em 12/05/2018 as 14:50:28.
        Para que o arquivo não seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

        Tabela correspondente: sistema_parametro_global_corporacao
        Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

        */
        


class DAO_Sistema_parametro_global_corporacao extends Generic_DAO
                        {
                            const NOME_ENTIDADE = "sistema_parametro_global_corporacao";

                            public static $nomeTabela = null;
                            public static $idSistemaTabela = false;
                            public static $databaseFieldsRelatedAttributes = null;
	                        public static $databaseFieldTypes = null;
	                        public static $databaseFieldNames = null;
	                        public static $listAliasTypes = null;
	                        public static $listAliasRelatedAttributes = null;
	                        
	                        public $id;
public $nome;
public $valorString;
public $valorInt;
public $valorDatetime;
public $valorSec;
public $valorOffsec;
public $valorDate;
public $valorFloat;
public $valorBoolean;
public $corporacaoId;

public $labelId;
public $labelNome;
public $labelValorString;
public $labelValorInt;
public $labelValorDatetime;
public $labelValorSec;
public $labelValorOffsec;
public $labelValorDate;
public $labelValorFloat;
public $labelValorBoolean;
public $labelCorporacaoId;

public static $arrPK = array("id");

public function __construct($configDAO = null)
        {
            parent::__construct($configDAO);
            static::__setStaticValues($this->database);           
        }

public static function idSistemaTabela($databaseObject = null){
            if(!(static::$idSistemaTabela))
            {
                static::$idSistemaTabela = EXTDAO_Sistema_tabela::getIdDaSistemaTabela(static::nomeTabela(), $databaseObject);
            }
            return static::$idSistemaTabela;
        }

public function getIdSistemaTabela(){
            return static::idSistemaTabela();
        }

public static function nomeTabela(){
            if(is_null(static::$nomeTabela))
            {
                static::$nomeTabela = "sistema_parametro_global_corporacao";
            }
            return static::$nomeTabela;
        }

public function getNomeTabela(){
            return static::nomeTabela();
        }

public static function __setStaticValues($databaseObject = null)
                   {    
                        static::nomeTabela();
                        
                        static::idSistemaTabela($databaseObject);
                   
                        static::setDatabaseFieldNames();
                        static::setDatabaseFieldTypes();
                        static::setDatabaseFieldsRelatedAttributeNames();
                        
                        static::setListAliasTypes();
                        static::setListAliasRelatedAttributes();
                   }

public function getExpressaoLabel()
                   {
                        return "{nome}";
                   }

public static function setDatabaseFieldNames()
        {
            if(is_null(static::$databaseFieldNames))
            {
                static::$databaseFieldNames = new stdClass();
                static::$databaseFieldNames->id = "id";
static::$databaseFieldNames->nome = "nome";
static::$databaseFieldNames->valorString = "valor_string";
static::$databaseFieldNames->valorInt = "valor_INT";
static::$databaseFieldNames->valorDatetime = "valor_DATETIME";
static::$databaseFieldNames->valorSec = "valor_SEC";
static::$databaseFieldNames->valorOffsec = "valor_OFFSEC";
static::$databaseFieldNames->valorDate = "valor_DATE";
static::$databaseFieldNames->valorFloat = "valor_FLOAT";
static::$databaseFieldNames->valorBoolean = "valor_BOOLEAN";
static::$databaseFieldNames->corporacaoId = "corporacao_id_INT";

            }
                      
        }

public static function setDatabaseFieldTypes()
        {
            if(is_null(static::$databaseFieldTypes))
            {
                static::$databaseFieldTypes = new stdClass();
                static::$databaseFieldTypes->id = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->nome = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->valorString = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->valorInt = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->valorDatetime = static::TIPO_VARIAVEL_DATETIME;
static::$databaseFieldTypes->valorSec = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$databaseFieldTypes->valorOffsec = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$databaseFieldTypes->valorDate = static::TIPO_VARIAVEL_DATE;
static::$databaseFieldTypes->valorFloat = static::TIPO_VARIAVEL_FLOAT;
static::$databaseFieldTypes->valorBoolean = static::TIPO_VARIAVEL_BOOLEAN;
static::$databaseFieldTypes->corporacaoId = static::TIPO_VARIAVEL_INTEGER;

            }
            
        }

public static function setDatabaseFieldsRelatedAttributeNames()
        {
            if(is_null(static::$databaseFieldsRelatedAttributes))
            {
                static::$databaseFieldsRelatedAttributes = new stdClass();
                static::$databaseFieldsRelatedAttributes->id = "id";
static::$databaseFieldsRelatedAttributes->nome = "nome";
static::$databaseFieldsRelatedAttributes->valor_string = "valorString";
static::$databaseFieldsRelatedAttributes->valor_INT = "valorInt";
static::$databaseFieldsRelatedAttributes->valor_DATETIME = "valorDatetime";
static::$databaseFieldsRelatedAttributes->valor_SEC = "valorSec";
static::$databaseFieldsRelatedAttributes->valor_OFFSEC = "valorOffsec";
static::$databaseFieldsRelatedAttributes->valor_DATE = "valorDate";
static::$databaseFieldsRelatedAttributes->valor_FLOAT = "valorFloat";
static::$databaseFieldsRelatedAttributes->valor_BOOLEAN = "valorBoolean";
static::$databaseFieldsRelatedAttributes->corporacao_id_INT = "corporacaoId";

            }           
        }

public function setAllFieldsToHumanFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["nome"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["valor_string"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["valor_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["valor_DATETIME"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["valor_SEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["valor_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["valor_DATE"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["valor_FLOAT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["valor_BOOLEAN"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
}

public function setAllFieldsToDatabaseFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["nome"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["valor_string"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["valor_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["valor_DATETIME"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["valor_SEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["valor_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["valor_DATE"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["valor_FLOAT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["valor_BOOLEAN"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
}

  public function getFkObjCorporacao() 
                {
                    if($this->objCorporacao == null)
                    {                        
                        $this->objCorporacao = new EXTDAO_Corporacao_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getCorporacao_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objCorporacao->clear();
                    }
                    elseif($this->objCorporacao->getId() != $idFK)
                    {
                        $this->objCorporacao->select($idFK);
                    }
                    return $this->objCorporacao;
                }


public function setLabels()
        {$this->labelId = "";
$this->labelNome = "";
$this->labelValorString = "";
$this->labelValorInt = "";
$this->labelValorDatetime = "";
$this->labelValorSec = "";
$this->labelValorOffsec = "";
$this->labelValorDate = "";
$this->labelValorFloat = "";
$this->labelValorBoolean = "";
$this->labelCorporacaoId = "";
}

public function __actionAdd($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                $this->setByObject($parameters);
                $this->setDataCadastroAndOffsetInSecondsFromUser($parameters);
                
                $this->formatarParaSQL();
                $msg = $this->insert(true);
                
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                return new Mensagem(null, I18N::getExpression("Sistema parametro global corporacao adicionado com sucesso."));
                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionEdit($parameters = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try {

                $this->setByObject($parameters);
                $this->setAllFieldsToHumanFormat();
                
                $this->formatarParaSQL();
                $msg = $this->updateByInstanceUsingParameters($this->id, $parameters, true);
                if($msg != null && $msg->erro())
                {
                     return $msg;
                }
        
                return new Mensagem(null, I18N::getExpression("Sistema parametro global corporacao editado com sucesso."));
            }
            catch(Exception $ex)
            {                
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionRemove($parameters = null)
        {
            $successulRemovalCount = 0;
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                foreach($parameters as $record)
                {
                    if(is_numeric($record->id))
                    {
                        $msg = $this->delete($record->id, true);
                        if($msg == null || $msg->ok())
                        {
                            $successulRemovalCount++;
                        }
                    }
                }
                
                if($successulRemovalCount > 0)
                {
                    $successMessage = count($successulRemovalCount) > 1 ? I18N::getExpression("{0} Sistema parametro global corporacao foram removidos com sucesso.", $successulRemovalCount) : I18N::getExpression("Sistema parametro global corporacao removido com sucesso.");
                    return new Mensagem(null, $successMessage);
                }
                else
                {
                    $errorMessage = count($parameters) > 1 ? I18N::getExpression("Falha ao remover Sistema parametro global corporacao.") : I18N::getExpression("Falha ao remover Sistema parametro global corporacao.");
                    return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, $errorMessage);                    
                }
            }
            catch (Exception $ex) 
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }

public function __getRecord($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }
            
            try
            {            
                $returnObject = new stdClass();            
                $whereClause = static::getWhereClauseForFilter($parameters->filterParameters);
                $query = "SELECT  id, nome, valor_string, valor_INT, valor_DATETIME, valor_SEC, valor_OFFSEC, valor_DATE, valor_FLOAT, valor_BOOLEAN, corporacao_id_INT FROM sistema_parametro_global_corporacao {$whereClause}";
    
                $msg = $this->database->queryMensagem($query);                
                if ($msg == null)
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Não foi encontrado nenhum registro"));
                    }                    
                    $resultSet = $this->database->result;
                    $resultObject = static::mapResultSetDataToRecord($resultSet);
                    
                    $returnObject->formData = $resultObject;
                    $returnObject->comboBoxesData = null;
                    if($parameters->loadComboBoxesData)
                    {
                        $this->setByObject($resultObject);
                        $returnObject->comboBoxesData =  $this->getFormComboBoxesData();
                    }                    
    
                    
    
                    return new Mensagem_generica($returnObject);
                } 
                else 
                {
                    return $msg;
                }
                
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }


        
        public function __getFilterComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
                
        public function __getFormComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
        
        protected function getFormComboBoxesData()
        {
            $comboBoxesData = new stdClass();
            $listParameters = new stdClass();
            $listParameters->listMappingType = static::LIST_MAPPING_TYPE_COMBOBOX;
            
            

            return $comboBoxesData;
        }
        
        


        
        public static function setListAliasTypes()
        {
            if(is_null(static::$listAliasTypes))
            {
                static::$listAliasTypes = new stdClass();
                static::$listAliasTypes->sistema_parametro_global_corporacao__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->sistema_parametro_global_corporacao__nome = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->sistema_parametro_global_corporacao__valor_string = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->sistema_parametro_global_corporacao__valor_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->sistema_parametro_global_corporacao__valor_DATETIME = static::TIPO_VARIAVEL_DATETIME;
static::$listAliasTypes->sistema_parametro_global_corporacao__valor_SEC = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$listAliasTypes->sistema_parametro_global_corporacao__valor_OFFSEC = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$listAliasTypes->sistema_parametro_global_corporacao__valor_DATE = static::TIPO_VARIAVEL_DATE;
static::$listAliasTypes->sistema_parametro_global_corporacao__valor_FLOAT = static::TIPO_VARIAVEL_FLOAT;
static::$listAliasTypes->sistema_parametro_global_corporacao__valor_BOOLEAN = static::TIPO_VARIAVEL_BOOLEAN;
            }        
        }
        
        public static function setListAliasRelatedAttributes()
        {
            if(is_null(static::$listAliasRelatedAttributes))
            {
                static::$listAliasRelatedAttributes = new stdClass();
                static::$listAliasRelatedAttributes->sistema_parametro_global_corporacao__id = "id";
static::$listAliasRelatedAttributes->sistema_parametro_global_corporacao__nome = "nome";
static::$listAliasRelatedAttributes->sistema_parametro_global_corporacao__valor_string = "valorString";
static::$listAliasRelatedAttributes->sistema_parametro_global_corporacao__valor_INT = "valorInt";
static::$listAliasRelatedAttributes->sistema_parametro_global_corporacao__valor_DATETIME = "valorDatetime";
static::$listAliasRelatedAttributes->sistema_parametro_global_corporacao__valor_SEC = "valorSec";
static::$listAliasRelatedAttributes->sistema_parametro_global_corporacao__valor_OFFSEC = "valorOffsec";
static::$listAliasRelatedAttributes->sistema_parametro_global_corporacao__valor_DATE = "valorDate";
static::$listAliasRelatedAttributes->sistema_parametro_global_corporacao__valor_FLOAT = "valorFloat";
static::$listAliasRelatedAttributes->sistema_parametro_global_corporacao__valor_BOOLEAN = "valorBoolean";
            }         
        }

        public function __getList($parameters = null, $idCorporacao = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try
            {
                $filterParameters = new stdClass();
                if(isset($parameters->filterParameters))
                {
                    $filterParameters = $parameters->filterParameters;
                }
                                    
                            
            if(is_null($idCorporacao))
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            ListParameters::addFilterParameterToObject($filterParameters, "corporacaoId", $idCorporacao);           
                
                
                $sortingParameters = null;
                if(isset($parameters->sortingParameters))
                {
                    $sortingParameters = $parameters->sortingParameters;
                }
                
                $paginationParameters = null;
                if(isset($parameters->paginationParameters))
                {
                    $paginationParameters = $parameters->paginationParameters;
                }
                
                $listMappingType = static::getListMappingType($parameters->listMappingType);
                $mainTableAlias = "spgc";

                $whereClause = static::getWhereClauseForFilter($filterParameters, $mainTableAlias);
                $orderByClause = static::getOrderByClauseForFilter($sortingParameters, $mainTableAlias);
                $limitClause = static::getLimitClauseForFilter($paginationParameters);

                $queryWithoutLimit = "SELECT spgc.id FROM sistema_parametro_global_corporacao spgc {$whereClause}";
                $query = "SELECT spgc.id AS sistema_parametro_global_corporacao__id, spgc.nome AS sistema_parametro_global_corporacao__nome, spgc.valor_string AS sistema_parametro_global_corporacao__valor_string, spgc.valor_INT AS sistema_parametro_global_corporacao__valor_INT, spgc.valor_DATETIME AS sistema_parametro_global_corporacao__valor_DATETIME, spgc.valor_SEC AS sistema_parametro_global_corporacao__valor_SEC, spgc.valor_OFFSEC AS sistema_parametro_global_corporacao__valor_OFFSEC, spgc.valor_DATE AS sistema_parametro_global_corporacao__valor_DATE, spgc.valor_FLOAT AS sistema_parametro_global_corporacao__valor_FLOAT, spgc.valor_BOOLEAN AS sistema_parametro_global_corporacao__valor_BOOLEAN FROM sistema_parametro_global_corporacao spgc  {$whereClause} {$orderByClause} {$limitClause}";

                $msg = $this->database->queryMensagem($queryWithoutLimit);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                
                $totalNumberOfRecords = $this->database->rows();
                if($totalNumberOfRecords == 0)
                {
                    return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                }                        
                        
                $msg = $this->database->queryMensagem($query);
                
                //operacao realizaca com sucesso
                if ($msg == null) 
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                    }                        
                
                    $resultSet = $this->database->result;

                    $objReturn = new stdClass();
                    $objReturn->dataSet = static::mapResultSetDataToList($resultSet, $listMappingType, static::$listAliasTypes);

                    if(!Helper::isNullOrEmpty($paginationParameters->recordsPerPage) && !Helper::isNullOrEmpty($paginationParameters->currentPage))
                    {
                        $lastPage = ceil($totalNumberOfRecords / $paginationParameters->recordsPerPage);
                        $objReturn->paginationParameters = new PaginationParams($paginationParameters->recordsPerPage, $paginationParameters->currentPage, $lastPage);
                    }
                    
                    return new Mensagem_generica($objReturn);
                } 
                else
                {
                    return $msg;
                }                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }



public function getId()
            {
                return $this->id;
            }

public function getNome()
            {
                return $this->nome;
            }

public function getValorString()
            {
                return $this->valorString;
            }

public function getValor_string()
                {
                    return $this->valorString;
                }

public function getValorInt()
            {
                return $this->valorInt;
            }

public function getValor_INT()
                {
                    return $this->valorInt;
                }

public function getValorDatetime()
            {
                return $this->valorDatetime;
            }

public function getValor_DATETIME()
                {
                    return $this->valorDatetime;
                }

public function getValorSec()
            {
                return $this->valorSec;
            }

public function getValor_SEC()
                {
                    return $this->valorSec;
                }

public function getValorOffsec()
            {
                return $this->valorOffsec;
            }

public function getValor_OFFSEC()
                {
                    return $this->valorOffsec;
                }

public function getValorDate()
            {
                return $this->valorDate;
            }

public function getValor_DATE()
                {
                    return $this->valorDate;
                }

public function getValorFloat()
            {
                return $this->valorFloat;
            }

public function getValor_FLOAT()
                {
                    return $this->valorFloat;
                }

public function getValorBoolean()
            {
                return $this->valorBoolean;
            }

public function getValor_BOOLEAN()
                {
                    return $this->valorBoolean;
                }

public function getCorporacaoId()
            {
                return $this->corporacaoId;
            }

public function getCorporacao_id_INT()
                {
                    return $this->corporacaoId;
                }



function setId($value)
            {
                $this->id = $value;
            }

function setNome($value)
            {
                $this->nome = $value;
            }

function setValorString($value)
            {
                $this->valorString = $value;
            }

public function setValor_string($value)
                { 
                    $this->valorString = $value; 
                }

function setValorInt($value)
            {
                $this->valorInt = $value;
            }

public function setValor_INT($value)
                { 
                    $this->valorInt = $value; 
                }

function setValorDatetime($value)
            {
                $this->valorDatetime = $value;
            }

public function setValor_DATETIME($value)
                { 
                    $this->valorDatetime = $value; 
                }

function setValorSec($value)
            {
                $this->valorSec = $value;
            }

public function setValor_SEC($value)
                { 
                    $this->valorSec = $value; 
                }

function setValorOffsec($value)
            {
                $this->valorOffsec = $value;
            }

public function setValor_OFFSEC($value)
                { 
                    $this->valorOffsec = $value; 
                }

function setValorDate($value)
            {
                $this->valorDate = $value;
            }

public function setValor_DATE($value)
                { 
                    $this->valorDate = $value; 
                }

function setValorFloat($value)
            {
                $this->valorFloat = $value;
            }

public function setValor_FLOAT($value)
                { 
                    $this->valorFloat = $value; 
                }

function setValorBoolean($value)
            {
                $this->valorBoolean = $value;
            }

public function setValor_BOOLEAN($value)
                { 
                    $this->valorBoolean = $value; 
                }

function setCorporacaoId($value)
            {
                $this->corporacaoId = $value;
            }

public function setCorporacao_id_INT($value)
                { 
                    $this->corporacaoId = $value; 
                }



public function clear()
        {$this->id = null;
$this->nome = null;
$this->valorString = null;
$this->valorInt = null;
$this->valorDatetime = null;
$this->valorSec = null;
$this->valorOffsec = null;
$this->valorDate = null;
$this->valorFloat = null;
$this->valorBoolean = null;
$this->corporacaoId = null;
if($this->objCorporacao != null) unset($this->objCorporacao);
if(is_array($this->arrEstadosCampos)){
                        unset($this->arrEstadosCampos);
                     $this->arrEstadosCampos = array();
                    }
                }

public function formatarParaSQL()
        {
if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->id)){
$this->id = $this->formatarIntegerParaComandoSQL($this->id);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->id);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->nome)){
$this->nome = $this->formatarStringParaComandoSQL($this->nome);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->nome);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->valorString)){
$this->valorString = $this->formatarStringParaComandoSQL($this->valorString);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->valorString);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->valorInt)){
$this->valorInt = $this->formatarIntegerParaComandoSQL($this->valorInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->valorInt);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->valorDatetime)){
$this->valorDatetime = $this->formatarDataTimeParaComandoSQL($this->valorDatetime);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->valorDatetime);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->valorSec)){
$this->valorSec = $this->formatarIntegerParaComandoSQL($this->valorSec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->valorSec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->valorOffsec)){
$this->valorOffsec = $this->formatarIntegerParaComandoSQL($this->valorOffsec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->valorOffsec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->valorDate)){
$this->valorDate = $this->formatarDataParaExibicao($this->valorDate);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->valorDate);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->valorFloat)){
$this->valorFloat = $this->formatarFloatParaComandoSQL($this->valorFloat);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->valorFloat);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->valorBoolean)){
$this->valorBoolean = $this->formatarBooleanParaComandoSQL($this->valorBoolean);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->valorBoolean);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->corporacaoId)){
$this->corporacaoId = $this->formatarIntegerParaComandoSQL($this->corporacaoId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->corporacaoId);
}

}

public function formatarParaExibicao()
        {if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->nome)){
$this->nome = $this->formatarStringParaExibicao($this->nome);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->nome);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->valorString)){
$this->valorString = $this->formatarStringParaExibicao($this->valorString);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->valorString);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->valorDatetime)){
$this->valorDatetime = $this->formatarDataTimeParaExibicao($this->valorDatetime);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->valorDatetime);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->valorDate)){
$this->valorDate = $this->formatarDataParaExibicao($this->valorDate);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->valorDate);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->valorFloat)){
$this->valorFloat = $this->formatarFloatParaExibicao($this->valorFloat);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->valorFloat);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->valorBoolean)){
$this->valorBoolean = $this->formatarBooleanParaExibicao($this->valorBoolean);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->valorBoolean);
}

}

function select($id, $idCorporacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }   

            $sql = "SELECT id, nome, valor_string, valor_INT, valor_DATETIME, valor_SEC, valor_OFFSEC, valor_DATE, valor_FLOAT, valor_BOOLEAN, corporacao_id_INT FROM sistema_parametro_global_corporacao WHERE id = $id AND corporacao_id_INT = $idCorporacao";

            $msg = $this->database->queryMensagem($sql);
            $result = $this->database->result;

            if($msg != null && $msg->erro() )
            { 
            		$this->clear();
            		return $msg;
        	} 
        	else if($this->database->rows() == 0)
        	{
                return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nenhum registro foi encontrado."));
            }
        

            $row = $this->database->fetchArray(MYSQLI_NUM);
            
            		$this->id = $row[0];
		$this->nome = $row[1];
		$this->valorString = $row[2];
		$this->valorInt = $row[3];
		$this->valorDatetime = $row[4];
		$this->valorSec = $row[5];
		$this->valorOffsec = $row[6];
		$this->valorDate = $row[7];
		$this->valorFloat = $row[8];
		$this->valorBoolean = $row[9];

            
        }

public function delete($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (is_null($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }

            $sql = "DELETE FROM sistema_parametro_global_corporacao WHERE id= $id AND corporacao_id_INT = $idCorporacao";
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                    && $sincronizar
                        && static::idSistemaTabela() != null
                            && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_REMOVE,
                                null,
                                null,
                                $id,
                                $this->database);
            }

            return $msg;
        }

public function insert($sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null, $opcoes = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            
            
            if (is_null($opcoes))
            {
                $opcoes = $this->opcoesDAO;
            }

            if (!is_numeric($this->id))
            {
                $this->id = "null";
                if ($opcoes == null || $opcoes[Database::OPCAO_GERAR_ID] != false) 
                {
                    $this->id = EXTDAO_Sistema_sequencia::gerarId(static::nomeTabela());                    
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }    
                    else if($msg == null)
                    {
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
                else 
                {
                    $this->id = null;
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao));
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }
                    else if($msg == null)
                    {
                        $this->id = $this->database->getLastInsertId();
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
            }
            else 
            {
                $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }                        
                else if($msg == null)
                {
                   $msg = new Mensagem_token();
                   $msg->mValor = $this->id;
                }
            }
            if($msg == null || $msg->ok())
            {                
                if($sincronizar
                    && static::idSistemaTabela() != null
                    && isset($this->id)){
                    
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_INSERT,
                                null,
                                null,
                                $this->id,
                                $this->database); 
                	}
            } 
            else
            {
                $this->id = null;
            }
            
            return $msg;
            
        }

public function getSQLInsert($idCorporacao){ 

return "INSERT INTO sistema_parametro_global_corporacao (id, nome, valor_string, valor_INT, valor_DATETIME, valor_SEC, valor_OFFSEC, valor_DATE, valor_FLOAT, valor_BOOLEAN, corporacao_id_INT) VALUES ( $this->id ,  $this->nome ,  $this->valorString ,  $this->valorInt ,  $this->valorDatetime ,  $this->valorSec ,  $this->valorOffsec ,  $this->valorDate ,  $this->valorFloat ,  $this->valorBoolean ,  $idCorporacao ) "; 

}




        
        public function updateByParameters($id, $objParametros, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao))
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();

            
                
                if (isset($objParametros->nome)) 
                {
                    $arrUpdateFields[] = " nome = {$objParametros->nome} ";
                }


                
                if (isset($objParametros->valorString)) 
                {
                    $arrUpdateFields[] = " valor_string = {$objParametros->valorString} ";
                }


                
                if (isset($objParametros->valorInt)) 
                {
                    $arrUpdateFields[] = " valor_INT = {$objParametros->valorInt} ";
                }


                
                if (isset($objParametros->valorDatetime)) 
                {
                    $arrUpdateFields[] = " valor_DATETIME = {$objParametros->valorDatetime} ";
                }


                
                if (isset($objParametros->valorSec)) 
                {
                    $arrUpdateFields[] = " valor_SEC = {$objParametros->valorSec} ";
                }


                
                if (isset($objParametros->valorOffsec)) 
                {
                    $arrUpdateFields[] = " valor_OFFSEC = {$objParametros->valorOffsec} ";
                }


                
                if (isset($objParametros->valorDate)) 
                {
                    $arrUpdateFields[] = " valor_DATE = {$objParametros->valorDate} ";
                }


                
                if (isset($objParametros->valorFloat)) 
                {
                    $arrUpdateFields[] = " valor_FLOAT = {$objParametros->valorFloat} ";
                }


                
                if (isset($objParametros->valorBoolean)) 
                {
                    $arrUpdateFields[] = " valor_BOOLEAN = {$objParametros->valorBoolean} ";
                }



            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE sistema_parametro_global_corporacao SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";

            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null)
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function update($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($this->nome)) 
                {                                      
                    $arrUpdateFields[] = " nome = {$this->nome} ";
                }


                
                if (isset($this->valorString)) 
                {                                      
                    $arrUpdateFields[] = " valor_string = {$this->valorString} ";
                }


                
                if (isset($this->valorInt)) 
                {                                      
                    $arrUpdateFields[] = " valor_INT = {$this->valorInt} ";
                }


                
                if (isset($this->valorDatetime)) 
                {                                      
                    $arrUpdateFields[] = " valor_DATETIME = {$this->valorDatetime} ";
                }


                
                if (isset($this->valorSec)) 
                {                                      
                    $arrUpdateFields[] = " valor_SEC = {$this->valorSec} ";
                }


                
                if (isset($this->valorOffsec)) 
                {                                      
                    $arrUpdateFields[] = " valor_OFFSEC = {$this->valorOffsec} ";
                }


                
                if (isset($this->valorDate)) 
                {                                      
                    $arrUpdateFields[] = " valor_DATE = {$this->valorDate} ";
                }


                
                if (isset($this->valorFloat)) 
                {                                      
                    $arrUpdateFields[] = " valor_FLOAT = {$this->valorFloat} ";
                }


                
                if (isset($this->valorBoolean)) 
                {                                      
                    $arrUpdateFields[] = " valor_BOOLEAN = {$this->valorBoolean} ";
                }


            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE sistema_parametro_global_corporacao SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function updateByInstanceUsingParameters($id, $parameters, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($parameters->nome)) 
                {                                      
                    $arrUpdateFields[] = " nome = {$this->nome} ";
                }
                
                if (isset($parameters->valorString)) 
                {                                      
                    $arrUpdateFields[] = " valor_string = {$this->valorString} ";
                }
                
                if (isset($parameters->valorInt)) 
                {                                      
                    $arrUpdateFields[] = " valor_INT = {$this->valorInt} ";
                }
                
                if (isset($parameters->valorDatetime)) 
                {                                      
                    $arrUpdateFields[] = " valor_DATETIME = {$this->valorDatetime} ";
                }
                
                if (isset($parameters->valorSec)) 
                {                                      
                    $arrUpdateFields[] = " valor_SEC = {$this->valorSec} ";
                }
                
                if (isset($parameters->valorOffsec)) 
                {                                      
                    $arrUpdateFields[] = " valor_OFFSEC = {$this->valorOffsec} ";
                }
                
                if (isset($parameters->valorDate)) 
                {                                      
                    $arrUpdateFields[] = " valor_DATE = {$this->valorDate} ";
                }
                
                if (isset($parameters->valorFloat)) 
                {                                      
                    $arrUpdateFields[] = " valor_FLOAT = {$this->valorFloat} ";
                }
                
                if (isset($parameters->valorBoolean)) 
                {                                      
                    $arrUpdateFields[] = " valor_BOOLEAN = {$this->valorBoolean} ";
                }
            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE sistema_parametro_global_corporacao SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }

}

?>
