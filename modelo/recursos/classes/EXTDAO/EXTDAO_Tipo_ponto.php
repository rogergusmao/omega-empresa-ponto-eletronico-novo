<?php

/*

Arquivo gerado através de gerador de código em 06/09/2017 as 21:00:39.
Para que o arquivo não seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

Tabela correspondente: tipo_ponto
Sobrescrita de dados: ---SOBRESCRITA_NAO_AUTORIZADA---

*/
?>

<?php

class EXTDAO_Tipo_ponto extends DAO_Tipo_ponto
{
    public function __construct($configDAO)
    {
        parent::__construct($configDAO);
        $this->nomeClasse = "EXTDAO_Tipo_ponto";
    }

    public function factory()
    {
        return new EXTDAO_Tipo_ponto();
    }
}

?>
