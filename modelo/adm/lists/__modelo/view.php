<!--Definição da View Angular JS -->
<?php
$objGrid = new GridEnvironment("filterForm", "listClienteController", "vm");
$objGrid->setDefaultValues();
?>

<div class='row' id='content-wrapper' ng-controller="<?= $objGrid->getAngularControllerAttributeValue() ?>">
    <div class='col-xs-12'>
        <div class='row'>
            <div class='col-sm-12'>
                <div class='page-header'>
                    <h1 class='pull-left'>
                        <i class='icon-edit'></i>
                        <span>
                                <?= I18N::getExpression("Lista de Clientes"); ?>
                            </span>
                    </h1>
                </div>
            </div>
        </div>

        <?
        global $objFilter;
        $objFilter = $objGrid->getFilterEnvironment();
        $objFilter->setLayoutTwoColumns();
        ?>

        <?= Helper::includeSubView("filter_view"); ?>

        <div class="row">
            <div class="col-sm-12" ng-if="!isNullOrEmpty(<?= $objGrid->getAngularControllerAs() ?>.gridData)">

                <div class="box bordered-box blue-border">
                    <div class="box-header blue-background">
                        <div class="title">Lista de Clientes</div>
                        <?= $objGrid->renderListActionButtons(); ?>
                    </div>

                    <div class='collapse navbar-collapse navbar-ex1-collapse blue-background'>

                        <?= $objGrid->renderHighlightAndFilterPanel(); ?>

                        <ul class='nav navbar-nav navbar-form navbar-right'>
                            <button class='btn btn-default' ng-show="vm.selectedRecords.length > 1"
                                    ng-click="removeAllSelectedRecords('<?= I18N::getExpression("Deseja remover todos os registros selecionados?") ?>', vm)">
                                Apagar Selecionados
                            </button>
                        </ul>

                    </div>

                    <div class="box-content box-no-padding">
                        <div class="responsive-table">
                            <div class="scrollable-area dataTables_wrapper">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>
                                            <?= $objGrid->renderSelectAllRecordsCheckbox(); ?>
                                        </th>

                                        <?= $objGrid->renderColumnHeader('id', 'Id'); ?>
                                        <?= $objGrid->renderColumnHeader('nome', 'Nome'); ?>
                                        <?= $objGrid->renderColumnHeader('valor', 'Valor'); ?>
                                        <?= $objGrid->renderColumnHeader('dataNascimento', 'Data de Nascimento'); ?>
                                        <?= $objGrid->renderColumnHeader('sexo', 'Sexo'); ?>

                                        <th>
                                            <?= I18N::getExpression("Opções") ?>
                                        </th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr ng-repeat="record in <?= $objGrid->getAngularControllerAs() ?>.gridData track by $index">
                                        <td>
                                            <?= $objGrid->renderRecordCheckbox('record'); ?>
                                        </td>
                                        <td>
                                            <?= $objGrid->renderColumnContent('record', 'id'); ?>
                                        </td>
                                        <td>
                                            <?= $objGrid->renderColumnContent('record', 'nome'); ?>
                                        </td>
                                        <td>
                                            <?= $objGrid->renderColumnContent('record', 'valor'); ?>
                                        </td>
                                        <td>
                                            <?= $objGrid->renderColumnContent('record', 'dataNascimento'); ?>
                                        </td>
                                        <td>
                                            <?= $objGrid->renderColumnContent('record', 'sexo'); ?>
                                        </td>
                                        <td>
                                            <?= $objGrid->renderEditButton('record'); ?>
                                            <?= $objGrid->renderDeleteButton('record'); ?>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>

                                <div class="row datatables-bottom">

                                    <?= $objGrid->renderPositionText(); ?>
                                    <?= $objGrid->renderPaginationBar(); ?>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--Definição do Controller Angular JS -->
<script type="text/javascript">

    <?=Helper::incluirConteudoDataServices(); ?>

</script>

<?= Helper::incluirControllerAngularJS(); ?>

