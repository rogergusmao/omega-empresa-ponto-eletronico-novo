<?php


        /*

        Arquivo gerado através de gerador de código em 12/05/2018 as 14:50:25.
        Para que o arquivo não seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

        Tabela correspondente: sistema_campo_atributo
        Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

        */
        


class DAO_Sistema_campo_atributo extends Generic_DAO
                        {
                            const NOME_ENTIDADE = "sistema_campo_atributo";

                            public static $nomeTabela = null;
                            public static $idSistemaTabela = false;
                            public static $databaseFieldsRelatedAttributes = null;
	                        public static $databaseFieldTypes = null;
	                        public static $databaseFieldNames = null;
	                        public static $listAliasTypes = null;
	                        public static $listAliasRelatedAttributes = null;
	                        
	                        public $id;
public $sistemaCampoId;
public $sistemaAtributoId;
public $seqInt;

public $labelId;
public $labelSistemaCampoId;
public $labelSistemaAtributoId;
public $labelSeqInt;

public static $arrPK = array("id");

public function __construct($configDAO = null)
        {
            parent::__construct($configDAO);
            static::__setStaticValues($this->database);           
        }

public static function idSistemaTabela($databaseObject = null){
            if(!(static::$idSistemaTabela))
            {
                static::$idSistemaTabela = EXTDAO_Sistema_tabela::getIdDaSistemaTabela(static::nomeTabela(), $databaseObject);
            }
            return static::$idSistemaTabela;
        }

public function getIdSistemaTabela(){
            return static::idSistemaTabela();
        }

public static function nomeTabela(){
            if(is_null(static::$nomeTabela))
            {
                static::$nomeTabela = "sistema_campo_atributo";
            }
            return static::$nomeTabela;
        }

public function getNomeTabela(){
            return static::nomeTabela();
        }

public static function __setStaticValues($databaseObject = null)
                   {    
                        static::nomeTabela();
                        
                        static::idSistemaTabela($databaseObject);
                   
                        static::setDatabaseFieldNames();
                        static::setDatabaseFieldTypes();
                        static::setDatabaseFieldsRelatedAttributeNames();
                        
                        static::setListAliasTypes();
                        static::setListAliasRelatedAttributes();
                   }

public function getExpressaoLabel()
                   {
                        return "{id}";
                   }

public static function setDatabaseFieldNames()
        {
            if(is_null(static::$databaseFieldNames))
            {
                static::$databaseFieldNames = new stdClass();
                static::$databaseFieldNames->id = "id";
static::$databaseFieldNames->sistemaCampoId = "sistema_campo_id_INT";
static::$databaseFieldNames->sistemaAtributoId = "sistema_atributo_id_INT";
static::$databaseFieldNames->seqInt = "seq_INT";

            }
                      
        }

public static function setDatabaseFieldTypes()
        {
            if(is_null(static::$databaseFieldTypes))
            {
                static::$databaseFieldTypes = new stdClass();
                static::$databaseFieldTypes->id = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->sistemaCampoId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->sistemaAtributoId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->seqInt = static::TIPO_VARIAVEL_INTEGER;

            }
            
        }

public static function setDatabaseFieldsRelatedAttributeNames()
        {
            if(is_null(static::$databaseFieldsRelatedAttributes))
            {
                static::$databaseFieldsRelatedAttributes = new stdClass();
                static::$databaseFieldsRelatedAttributes->id = "id";
static::$databaseFieldsRelatedAttributes->sistema_campo_id_INT = "sistemaCampoId";
static::$databaseFieldsRelatedAttributes->sistema_atributo_id_INT = "sistemaAtributoId";
static::$databaseFieldsRelatedAttributes->seq_INT = "seqInt";

            }           
        }

public function setAllFieldsToHumanFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["sistema_campo_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["sistema_atributo_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["seq_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
}

public function setAllFieldsToDatabaseFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["sistema_campo_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["sistema_atributo_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["seq_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
}

  public function getFkObjSistema_campo() 
                {
                    if($this->objSistema_campo == null)
                    {                        
                        $this->objSistema_campo = new EXTDAO_Sistema_campo_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getSistema_campo_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objSistema_campo->clear();
                    }
                    elseif($this->objSistema_campo->getId() != $idFK)
                    {
                        $this->objSistema_campo->select($idFK);
                    }
                    return $this->objSistema_campo;
                }
  public function getFkObjSistema_atributo() 
                {
                    if($this->objSistema_atributo == null)
                    {                        
                        $this->objSistema_atributo = new EXTDAO_Sistema_atributo_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getSistema_atributo_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objSistema_atributo->clear();
                    }
                    elseif($this->objSistema_atributo->getId() != $idFK)
                    {
                        $this->objSistema_atributo->select($idFK);
                    }
                    return $this->objSistema_atributo;
                }


public function setLabels()
        {$this->labelId = "";
$this->labelSistemaCampoId = "";
$this->labelSistemaAtributoId = "";
$this->labelSeqInt = "";
}

public function __actionAdd($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                $this->setByObject($parameters);
                $this->setDataCadastroAndOffsetInSecondsFromUser($parameters);
                
                $this->formatarParaSQL();
                $msg = $this->insert(true);
                
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                return new Mensagem(null, I18N::getExpression("Sistema campo atributo adicionado com sucesso."));
                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionEdit($parameters = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try {

                $this->setByObject($parameters);
                $this->setAllFieldsToHumanFormat();
                
                $this->formatarParaSQL();
                $msg = $this->updateByInstanceUsingParameters($this->id, $parameters, true);
                if($msg != null && $msg->erro())
                {
                     return $msg;
                }
        
                return new Mensagem(null, I18N::getExpression("Sistema campo atributo editado com sucesso."));
            }
            catch(Exception $ex)
            {                
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionRemove($parameters = null)
        {
            $successulRemovalCount = 0;
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                foreach($parameters as $record)
                {
                    if(is_numeric($record->id))
                    {
                        $msg = $this->delete($record->id, true);
                        if($msg == null || $msg->ok())
                        {
                            $successulRemovalCount++;
                        }
                    }
                }
                
                if($successulRemovalCount > 0)
                {
                    $successMessage = count($successulRemovalCount) > 1 ? I18N::getExpression("{0} Sistema campo atributo foram removidos com sucesso.", $successulRemovalCount) : I18N::getExpression("Sistema campo atributo removido com sucesso.");
                    return new Mensagem(null, $successMessage);
                }
                else
                {
                    $errorMessage = count($parameters) > 1 ? I18N::getExpression("Falha ao remover Sistema campo atributo.") : I18N::getExpression("Falha ao remover Sistema campo atributo.");
                    return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, $errorMessage);                    
                }
            }
            catch (Exception $ex) 
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }

public function __getRecord($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }
            
            try
            {            
                $returnObject = new stdClass();            
                $whereClause = static::getWhereClauseForFilter($parameters->filterParameters);
                $query = "SELECT  id, sistema_campo_id_INT, sistema_atributo_id_INT, seq_INT FROM sistema_campo_atributo {$whereClause}";
    
                $msg = $this->database->queryMensagem($query);                
                if ($msg == null)
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Não foi encontrado nenhum registro"));
                    }                    
                    $resultSet = $this->database->result;
                    $resultObject = static::mapResultSetDataToRecord($resultSet);
                    
                    $returnObject->formData = $resultObject;
                    $returnObject->comboBoxesData = null;
                    if($parameters->loadComboBoxesData)
                    {
                        $this->setByObject($resultObject);
                        $returnObject->comboBoxesData =  $this->getFormComboBoxesData();
                    }                    
    
                    
    
                    return new Mensagem_generica($returnObject);
                } 
                else 
                {
                    return $msg;
                }
                
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }


        
        public function __getFilterComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
                
        public function __getFormComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
        
        protected function getFormComboBoxesData()
        {
            $comboBoxesData = new stdClass();
            $listParameters = new stdClass();
            $listParameters->listMappingType = static::LIST_MAPPING_TYPE_COMBOBOX;
            
            
                    
                    $objSistemaCampo = new EXTDAO_Sistema_campo();
                    $comboBoxesData->fieldSistemaCampoId = $objSistemaCampo->__getList($listParameters);
                    
                    $objSistemaAtributo = new EXTDAO_Sistema_atributo();
                    $comboBoxesData->fieldSistemaAtributoId = $objSistemaAtributo->__getList($listParameters);

            return $comboBoxesData;
        }
        
        


        
        public static function setListAliasTypes()
        {
            if(is_null(static::$listAliasTypes))
            {
                static::$listAliasTypes = new stdClass();
                static::$listAliasTypes->sistema_campo__nome = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->sistema_atributo__nome = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->sistema_campo_atributo__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->sistema_campo_atributo__sistema_tipo_campo_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->sistema_campo_atributo__seq_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->sistema_campo_atributo__seq_INT = static::TIPO_VARIAVEL_INTEGER;
            }        
        }
        
        public static function setListAliasRelatedAttributes()
        {
            if(is_null(static::$listAliasRelatedAttributes))
            {
                static::$listAliasRelatedAttributes = new stdClass();
                static::$listAliasRelatedAttributes->sistema_campo__nome = "sistemaCampoNome";
static::$listAliasRelatedAttributes->sistema_atributo__nome = "sistemaAtributoNome";
static::$listAliasRelatedAttributes->sistema_campo_atributo__id = "id";
static::$listAliasRelatedAttributes->sistema_campo_atributo__sistema_tipo_campo_id_INT = "sistemaTipoCampoId";
static::$listAliasRelatedAttributes->sistema_campo_atributo__seq_INT = "seqInt";
static::$listAliasRelatedAttributes->sistema_campo_atributo__seq_INT = "seqInt";
            }         
        }

        public function __getList($parameters = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try
            {
                $filterParameters = null;
                if(isset($parameters->filterParameters))
                {
                    $filterParameters = $parameters->filterParameters;
                }
                                    
                    
                
                $sortingParameters = null;
                if(isset($parameters->sortingParameters))
                {
                    $sortingParameters = $parameters->sortingParameters;
                }
                
                $paginationParameters = null;
                if(isset($parameters->paginationParameters))
                {
                    $paginationParameters = $parameters->paginationParameters;
                }
                
                $listMappingType = static::getListMappingType($parameters->listMappingType);
                $mainTableAlias = "sca";

                $whereClause = static::getWhereClauseForFilter($filterParameters, $mainTableAlias);
                $orderByClause = static::getOrderByClauseForFilter($sortingParameters, $mainTableAlias);
                $limitClause = static::getLimitClauseForFilter($paginationParameters);

                $queryWithoutLimit = "SELECT sca.id FROM sistema_campo_atributo sca {$whereClause}";
                $query = "SELECT sc.nome AS sistema_campo__nome, sa.nome AS sistema_atributo__nome, sca.id AS sistema_campo_atributo__id, sca.sistema_tipo_campo_id_INT AS sistema_campo_atributo__sistema_tipo_campo_id_INT, sca.seq_INT AS sistema_campo_atributo__seq_INT, sca.seq_INT AS sistema_campo_atributo__seq_INT FROM sistema_campo_atributo sca LEFT JOIN sistema_campo sc ON sc.id = sca.sistema_campo_id_INT LEFT JOIN sistema_atributo sa ON sa.id = sca.sistema_atributo_id_INT {$whereClause} {$orderByClause} {$limitClause}";

                $msg = $this->database->queryMensagem($queryWithoutLimit);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                
                $totalNumberOfRecords = $this->database->rows();
                if($totalNumberOfRecords == 0)
                {
                    return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                }                        
                        
                $msg = $this->database->queryMensagem($query);
                
                //operacao realizaca com sucesso
                if ($msg == null) 
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                    }                        
                
                    $resultSet = $this->database->result;

                    $objReturn = new stdClass();
                    $objReturn->dataSet = static::mapResultSetDataToList($resultSet, $listMappingType, static::$listAliasTypes);

                    if(!Helper::isNullOrEmpty($paginationParameters->recordsPerPage) && !Helper::isNullOrEmpty($paginationParameters->currentPage))
                    {
                        $lastPage = ceil($totalNumberOfRecords / $paginationParameters->recordsPerPage);
                        $objReturn->paginationParameters = new PaginationParams($paginationParameters->recordsPerPage, $paginationParameters->currentPage, $lastPage);
                    }
                    
                    return new Mensagem_generica($objReturn);
                } 
                else
                {
                    return $msg;
                }                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }



public function getId()
            {
                return $this->id;
            }

public function getSistemaCampoId()
            {
                return $this->sistemaCampoId;
            }

public function getSistema_campo_id_INT()
                {
                    return $this->sistemaCampoId;
                }

public function getSistemaAtributoId()
            {
                return $this->sistemaAtributoId;
            }

public function getSistema_atributo_id_INT()
                {
                    return $this->sistemaAtributoId;
                }

public function getSeqInt()
            {
                return $this->seqInt;
            }

public function getSeq_INT()
                {
                    return $this->seqInt;
                }



function setId($value)
            {
                $this->id = $value;
            }

function setSistemaCampoId($value)
            {
                $this->sistemaCampoId = $value;
            }

public function setSistema_campo_id_INT($value)
                { 
                    $this->sistemaCampoId = $value; 
                }

function setSistemaAtributoId($value)
            {
                $this->sistemaAtributoId = $value;
            }

public function setSistema_atributo_id_INT($value)
                { 
                    $this->sistemaAtributoId = $value; 
                }

function setSeqInt($value)
            {
                $this->seqInt = $value;
            }

public function setSeq_INT($value)
                { 
                    $this->seqInt = $value; 
                }



public function clear()
        {$this->id = null;
$this->sistemaCampoId = null;
if($this->objSistema_campo != null) unset($this->objSistema_campo);
$this->sistemaAtributoId = null;
if($this->objSistema_atributo != null) unset($this->objSistema_atributo);
$this->seqInt = null;
if(is_array($this->arrEstadosCampos)){
                        unset($this->arrEstadosCampos);
                     $this->arrEstadosCampos = array();
                    }
                }

public function formatarParaSQL()
        {
if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->id)){
$this->id = $this->formatarIntegerParaComandoSQL($this->id);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->id);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->sistemaCampoId)){
$this->sistemaCampoId = $this->formatarIntegerParaComandoSQL($this->sistemaCampoId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->sistemaCampoId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->sistemaAtributoId)){
$this->sistemaAtributoId = $this->formatarIntegerParaComandoSQL($this->sistemaAtributoId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->sistemaAtributoId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->seqInt)){
$this->seqInt = $this->formatarIntegerParaComandoSQL($this->seqInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->seqInt);
}

}

public function formatarParaExibicao()
        {}

function select($id, $idCorporacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }   

            $sql = "SELECT id, sistema_campo_id_INT, sistema_atributo_id_INT, seq_INT FROM sistema_campo_atributo WHERE id = $id";

            $msg = $this->database->queryMensagem($sql);
            $result = $this->database->result;

            if($msg != null && $msg->erro() )
            { 
            		$this->clear();
            		return $msg;
        	} 
        	else if($this->database->rows() == 0)
        	{
                return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nenhum registro foi encontrado."));
            }
        

            $row = $this->database->fetchArray(MYSQLI_NUM);
            
            		$this->id = $row[0];
		$this->sistemaCampoId = $row[1];
		$this->sistemaAtributoId = $row[2];
		$this->seqInt = $row[3];

            
        }

public function delete($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (is_null($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }

            $sql = "DELETE FROM sistema_campo_atributo WHERE id= $id";
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                    && $sincronizar
                        && static::idSistemaTabela() != null
                            && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_REMOVE,
                                null,
                                null,
                                $id,
                                $this->database);
            }

            return $msg;
        }

public function insert($sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null, $opcoes = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            
            
            if (is_null($opcoes))
            {
                $opcoes = $this->opcoesDAO;
            }

            if (!is_numeric($this->id))
            {
                $this->id = "null";
                if ($opcoes == null || $opcoes[Database::OPCAO_GERAR_ID] != false) 
                {
                    $this->id = EXTDAO_Sistema_sequencia::gerarId(static::nomeTabela());                    
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }    
                    else if($msg == null)
                    {
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
                else 
                {
                    $this->id = null;
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao));
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }
                    else if($msg == null)
                    {
                        $this->id = $this->database->getLastInsertId();
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
            }
            else 
            {
                $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }                        
                else if($msg == null)
                {
                   $msg = new Mensagem_token();
                   $msg->mValor = $this->id;
                }
            }
            if($msg == null || $msg->ok())
            {                
                if($sincronizar
                    && static::idSistemaTabela() != null
                    && isset($this->id)){
                    
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_INSERT,
                                null,
                                null,
                                $this->id,
                                $this->database); 
                	}
            } 
            else
            {
                $this->id = null;
            }
            
            return $msg;
            
        }

public function getSQLInsert($idCorporacao){ 

return "INSERT INTO sistema_campo_atributo (id, sistema_campo_id_INT, sistema_atributo_id_INT, seq_INT) VALUES ( $this->id ,  $this->sistemaCampoId ,  $this->sistemaAtributoId ,  $this->seqInt ) "; 

}




        
        public function updateByParameters($id, $objParametros, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao))
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();

            
                
                if (isset($objParametros->sistemaCampoId)) 
                {
                    $arrUpdateFields[] = " sistema_campo_id_INT = {$objParametros->sistemaCampoId} ";
                }


                
                if (isset($objParametros->sistemaAtributoId)) 
                {
                    $arrUpdateFields[] = " sistema_atributo_id_INT = {$objParametros->sistemaAtributoId} ";
                }


                
                if (isset($objParametros->seqInt)) 
                {
                    $arrUpdateFields[] = " seq_INT = {$objParametros->seqInt} ";
                }



            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE sistema_campo_atributo SET {$strUpdateFields} WHERE id = {$id}";

            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null)
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function update($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($this->sistemaCampoId)) 
                {                                      
                    $arrUpdateFields[] = " sistema_campo_id_INT = {$this->sistemaCampoId} ";
                }


                
                if (isset($this->sistemaAtributoId)) 
                {                                      
                    $arrUpdateFields[] = " sistema_atributo_id_INT = {$this->sistemaAtributoId} ";
                }


                
                if (isset($this->seqInt)) 
                {                                      
                    $arrUpdateFields[] = " seq_INT = {$this->seqInt} ";
                }


            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE sistema_campo_atributo SET {$strUpdateFields} WHERE id = {$id}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function updateByInstanceUsingParameters($id, $parameters, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($parameters->sistemaCampoId)) 
                {                                      
                    $arrUpdateFields[] = " sistema_campo_id_INT = {$this->sistemaCampoId} ";
                }
                
                if (isset($parameters->sistemaAtributoId)) 
                {                                      
                    $arrUpdateFields[] = " sistema_atributo_id_INT = {$this->sistemaAtributoId} ";
                }
                
                if (isset($parameters->seqInt)) 
                {                                      
                    $arrUpdateFields[] = " seq_INT = {$this->seqInt} ";
                }
            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE sistema_campo_atributo SET {$strUpdateFields} WHERE id = {$id}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }

}

?>
