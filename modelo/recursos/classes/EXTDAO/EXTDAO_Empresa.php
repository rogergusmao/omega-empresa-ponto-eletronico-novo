<?php

/*

Arquivo gerado atravÃ©s de gerador de cÃ³digo em 24/05/2017 as 09:45:02.
Para que o arquivo nÃ£o seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

Tabela correspondente: empresa
Sobrescrita de dados: ---SOBRESCRITA_NAO_AUTORIZADA---

*/

?>

<?php

class EXTDAO_Empresa extends DAO_Empresa
{
    public function __construct($configDAO = null)
    {
        parent::__construct($configDAO);
        $this->nomeClasse = "EXTDAO_Empresa";
    }

    public function factory()
    {
        return new EXTDAO_Empresa();
    }


    public function __actionAdd($parameters = null)
    {
        if (is_null($parameters))
        {
            $parameters = Helper::getPhpInputObject();
        }

        try
        {
            $this->setByObject($parameters);
            if(Helper::isNullOrEmpty($this->nome))
            {
                $this->nome = Helper::getNomeDeEntidadeAPartirDoEmail($this->email);
            }

            if($this->verificarEmail($this->email))
                return new Mensagem(
                    PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO,
                    "Ja existe uma empresa com o email ".$this->email);


            $this->setDataCadastroAndOffsetInSecondsFromUser($parameters);

            $this->formatarParaSQL();
            $msg = $this->insert(true);

            if ($msg != null && $msg->erro())
            {
                return $msg;
            }
            return new Mensagem(null, I18N::getExpression("Empresa adicionada com sucesso."));
        }
        catch (Exception $ex)
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
        }
    }

    public function __actionEdit($parameters = null)
    {
        if (is_null($parameters))
        {
            $parameters = Helper::getPhpInputObject();
        }

        try
        {
            $this->setByObject($parameters);
            if(Helper::isNullOrEmpty($this->nome))
            {
                $this->nome = Helper::getNomeDeEntidadeAPartirDoEmail($this->email);
            }

            $this->setAllFieldsToHumanFormat();

            $this->formatarParaSQL();
            $msg = $this->updateByInstanceUsingParameters($this->id, $parameters, true);
            if ($msg != null && $msg->erro())
            {
                return $msg;
            }

            return new Mensagem(null, I18N::getExpression("Empresa editada com sucesso."));
        }
        catch (Exception $ex)
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
        }
    }

    public static function getIdEmpresaIndefinida(Database $db, $idCorporacao, $idUsuarioLogado)
    {
        $nome = I18N::getExpression("EMPRESA");
        $email = I18N::getExpression("EMAIL");

        $q = "SELECT id FROM empresa WHERE corporacao_id_INT=$idCorporacao AND email = " . Helper::formatarStringParaComandoSQL($email);

        $db->queryMensagem($q);
        $id = $db->getPrimeiraTuplaDoResultSet(0);
        if (!strlen($id))
        {
            $obj = new EXTDAO_Empresa($db);
            $obj->setNome($nome);
            $obj->setEmail($email);
            $obj->formatarParaSQL();
            $msg = $obj->insert(true, $idCorporacao, $idUsuarioLogado);
            if (Interface_mensagem::checkOk($msg))
            {
                return $msg->mValor;
            }
            else
            {
                HelperLog::logErro(null, $msg);
                return null;
            }
        }
        else
        {
            return $id;
        }
    }

    public function verificarEmail($email, $idEmpresa = null)
    {

        if (!is_null($idEmpresa))
        {
            $strComplemento = "AND id <> {$idEmpresa}";
        }

        if (strlen($email))
        {
            $objBanco = new Database();
            $objBanco->query("SELECT id FROM empresa WHERE email='{$email}' {$strComplemento} AND corporacao_id_INT = " . Seguranca::getIdDaCorporacaoLogada());

            if ($objBanco->rows() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return true;
        }
    }

}

?>
