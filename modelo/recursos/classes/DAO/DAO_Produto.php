<?php


        /*

        Arquivo gerado através de gerador de código em 12/05/2018 as 14:49:09.
        Para que o arquivo não seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

        Tabela correspondente: produto
        Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

        */
        


class DAO_Produto extends Generic_DAO
                        {
                            const NOME_ENTIDADE = "produto";

                            public static $nomeTabela = null;
                            public static $idSistemaTabela = false;
                            public static $databaseFieldsRelatedAttributes = null;
	                        public static $databaseFieldTypes = null;
	                        public static $databaseFieldNames = null;
	                        public static $listAliasTypes = null;
	                        public static $listAliasRelatedAttributes = null;
	                        
	                        public $id;
public $identificador;
public $nome;
public $nomeNormalizado;
public $descricao;
public $produtoUnidadeMedidaId;
public $idOmegaInt;
public $cadastroSec;
public $cadastroOffsec;
public $precoCustoFloat;
public $prazoReposicaoEstoqueDiasInt;
public $codigoBarra;
public $corporacaoId;

public $labelId;
public $labelIdentificador;
public $labelNome;
public $labelNomeNormalizado;
public $labelDescricao;
public $labelProdutoUnidadeMedidaId;
public $labelIdOmegaInt;
public $labelCadastroSec;
public $labelCadastroOffsec;
public $labelPrecoCustoFloat;
public $labelPrazoReposicaoEstoqueDiasInt;
public $labelCodigoBarra;
public $labelCorporacaoId;

public static $arrPK = array("id");

public function __construct($configDAO = null)
        {
            parent::__construct($configDAO);
            static::__setStaticValues($this->database);           
        }

public static function idSistemaTabela($databaseObject = null){
            if(!(static::$idSistemaTabela))
            {
                static::$idSistemaTabela = EXTDAO_Sistema_tabela::getIdDaSistemaTabela(static::nomeTabela(), $databaseObject);
            }
            return static::$idSistemaTabela;
        }

public function getIdSistemaTabela(){
            return static::idSistemaTabela();
        }

public static function nomeTabela(){
            if(is_null(static::$nomeTabela))
            {
                static::$nomeTabela = "produto";
            }
            return static::$nomeTabela;
        }

public function getNomeTabela(){
            return static::nomeTabela();
        }

public static function __setStaticValues($databaseObject = null)
                   {    
                        static::nomeTabela();
                        
                        static::idSistemaTabela($databaseObject);
                   
                        static::setDatabaseFieldNames();
                        static::setDatabaseFieldTypes();
                        static::setDatabaseFieldsRelatedAttributeNames();
                        
                        static::setListAliasTypes();
                        static::setListAliasRelatedAttributes();
                   }

public function getExpressaoLabel()
                   {
                        return "{nome}";
                   }

public static function setDatabaseFieldNames()
        {
            if(is_null(static::$databaseFieldNames))
            {
                static::$databaseFieldNames = new stdClass();
                static::$databaseFieldNames->id = "id";
static::$databaseFieldNames->identificador = "identificador";
static::$databaseFieldNames->nome = "nome";
static::$databaseFieldNames->nomeNormalizado = "nome_normalizado";
static::$databaseFieldNames->descricao = "descricao";
static::$databaseFieldNames->produtoUnidadeMedidaId = "produto_unidade_medida_id_INT";
static::$databaseFieldNames->idOmegaInt = "id_omega_INT";
static::$databaseFieldNames->cadastroSec = "cadastro_SEC";
static::$databaseFieldNames->cadastroOffsec = "cadastro_OFFSEC";
static::$databaseFieldNames->precoCustoFloat = "preco_custo_FLOAT";
static::$databaseFieldNames->prazoReposicaoEstoqueDiasInt = "prazo_reposicao_estoque_dias_INT";
static::$databaseFieldNames->codigoBarra = "codigo_barra";
static::$databaseFieldNames->corporacaoId = "corporacao_id_INT";

            }
                      
        }

public static function setDatabaseFieldTypes()
        {
            if(is_null(static::$databaseFieldTypes))
            {
                static::$databaseFieldTypes = new stdClass();
                static::$databaseFieldTypes->id = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->identificador = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->nome = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->nomeNormalizado = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->descricao = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->produtoUnidadeMedidaId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->idOmegaInt = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->cadastroSec = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$databaseFieldTypes->cadastroOffsec = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$databaseFieldTypes->precoCustoFloat = static::TIPO_VARIAVEL_FLOAT;
static::$databaseFieldTypes->prazoReposicaoEstoqueDiasInt = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->codigoBarra = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->corporacaoId = static::TIPO_VARIAVEL_INTEGER;

            }
            
        }

public static function setDatabaseFieldsRelatedAttributeNames()
        {
            if(is_null(static::$databaseFieldsRelatedAttributes))
            {
                static::$databaseFieldsRelatedAttributes = new stdClass();
                static::$databaseFieldsRelatedAttributes->id = "id";
static::$databaseFieldsRelatedAttributes->identificador = "identificador";
static::$databaseFieldsRelatedAttributes->nome = "nome";
static::$databaseFieldsRelatedAttributes->nome_normalizado = "nomeNormalizado";
static::$databaseFieldsRelatedAttributes->descricao = "descricao";
static::$databaseFieldsRelatedAttributes->produto_unidade_medida_id_INT = "produtoUnidadeMedidaId";
static::$databaseFieldsRelatedAttributes->id_omega_INT = "idOmegaInt";
static::$databaseFieldsRelatedAttributes->cadastro_SEC = "cadastroSec";
static::$databaseFieldsRelatedAttributes->cadastro_OFFSEC = "cadastroOffsec";
static::$databaseFieldsRelatedAttributes->preco_custo_FLOAT = "precoCustoFloat";
static::$databaseFieldsRelatedAttributes->prazo_reposicao_estoque_dias_INT = "prazoReposicaoEstoqueDiasInt";
static::$databaseFieldsRelatedAttributes->codigo_barra = "codigoBarra";
static::$databaseFieldsRelatedAttributes->corporacao_id_INT = "corporacaoId";

            }           
        }

public function setAllFieldsToHumanFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["identificador"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["nome"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["nome_normalizado"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["descricao"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["produto_unidade_medida_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["id_omega_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["cadastro_SEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["cadastro_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["preco_custo_FLOAT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["prazo_reposicao_estoque_dias_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["codigo_barra"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
}

public function setAllFieldsToDatabaseFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["identificador"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["nome"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["nome_normalizado"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["descricao"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["produto_unidade_medida_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["id_omega_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["cadastro_SEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["cadastro_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["preco_custo_FLOAT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["prazo_reposicao_estoque_dias_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["codigo_barra"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
}

  public function getFkObjProduto_unidade_medida() 
                {
                    if($this->objProduto_unidade_medida == null)
                    {                        
                        $this->objProduto_unidade_medida = new EXTDAO_Produto_unidade_medida_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getProduto_unidade_medida_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objProduto_unidade_medida->clear();
                    }
                    elseif($this->objProduto_unidade_medida->getId() != $idFK)
                    {
                        $this->objProduto_unidade_medida->select($idFK);
                    }
                    return $this->objProduto_unidade_medida;
                }
  public function getFkObjCorporacao() 
                {
                    if($this->objCorporacao == null)
                    {                        
                        $this->objCorporacao = new EXTDAO_Corporacao_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getCorporacao_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objCorporacao->clear();
                    }
                    elseif($this->objCorporacao->getId() != $idFK)
                    {
                        $this->objCorporacao->select($idFK);
                    }
                    return $this->objCorporacao;
                }


public function setLabels()
        {$this->labelId = "";
$this->labelIdentificador = "";
$this->labelNome = "";
$this->labelNomeNormalizado = "";
$this->labelDescricao = "";
$this->labelProdutoUnidadeMedidaId = "";
$this->labelIdOmegaInt = "";
$this->labelCadastroSec = "";
$this->labelCadastroOffsec = "";
$this->labelPrecoCustoFloat = "";
$this->labelPrazoReposicaoEstoqueDiasInt = "";
$this->labelCodigoBarra = "";
$this->labelCorporacaoId = "";
}

public function __actionAdd($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                $this->setByObject($parameters);
                $this->setDataCadastroAndOffsetInSecondsFromUser($parameters);
                
                $this->formatarParaSQL();
                $msg = $this->insert(true);
                
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                return new Mensagem(null, I18N::getExpression("Produto adicionado com sucesso."));
                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionEdit($parameters = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try {

                $this->setByObject($parameters);
                $this->setAllFieldsToHumanFormat();
                
                $this->formatarParaSQL();
                $msg = $this->updateByInstanceUsingParameters($this->id, $parameters, true);
                if($msg != null && $msg->erro())
                {
                     return $msg;
                }
        
                return new Mensagem(null, I18N::getExpression("Produto editado com sucesso."));
            }
            catch(Exception $ex)
            {                
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionRemove($parameters = null)
        {
            $successulRemovalCount = 0;
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                foreach($parameters as $record)
                {
                    if(is_numeric($record->id))
                    {
                        $msg = $this->delete($record->id, true);
                        if($msg == null || $msg->ok())
                        {
                            $successulRemovalCount++;
                        }
                    }
                }
                
                if($successulRemovalCount > 0)
                {
                    $successMessage = count($successulRemovalCount) > 1 ? I18N::getExpression("{0} Produto foram removidos com sucesso.", $successulRemovalCount) : I18N::getExpression("Produto removido com sucesso.");
                    return new Mensagem(null, $successMessage);
                }
                else
                {
                    $errorMessage = count($parameters) > 1 ? I18N::getExpression("Falha ao remover Produto.") : I18N::getExpression("Falha ao remover Produto.");
                    return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, $errorMessage);                    
                }
            }
            catch (Exception $ex) 
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }

public function __getRecord($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }
            
            try
            {            
                $returnObject = new stdClass();            
                $whereClause = static::getWhereClauseForFilter($parameters->filterParameters);
                $query = "SELECT  id, identificador, nome, nome_normalizado, descricao, produto_unidade_medida_id_INT, id_omega_INT, cadastro_SEC, cadastro_OFFSEC, preco_custo_FLOAT, prazo_reposicao_estoque_dias_INT, codigo_barra, corporacao_id_INT FROM produto {$whereClause}";
    
                $msg = $this->database->queryMensagem($query);                
                if ($msg == null)
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Não foi encontrado nenhum registro"));
                    }                    
                    $resultSet = $this->database->result;
                    $resultObject = static::mapResultSetDataToRecord($resultSet);
                    
                    $returnObject->formData = $resultObject;
                    $returnObject->comboBoxesData = null;
                    if($parameters->loadComboBoxesData)
                    {
                        $this->setByObject($resultObject);
                        $returnObject->comboBoxesData =  $this->getFormComboBoxesData();
                    }                    
    
                    
    
                    return new Mensagem_generica($returnObject);
                } 
                else 
                {
                    return $msg;
                }
                
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }


        
        public function __getFilterComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
                
        public function __getFormComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
        
        protected function getFormComboBoxesData()
        {
            $comboBoxesData = new stdClass();
            $listParameters = new stdClass();
            $listParameters->listMappingType = static::LIST_MAPPING_TYPE_COMBOBOX;
            
            
                    
                    $objProdutoUnidadeMedida = new EXTDAO_Produto_unidade_medida();
                    $comboBoxesData->fieldProdutoUnidadeMedidaId = $objProdutoUnidadeMedida->__getList($listParameters);

            return $comboBoxesData;
        }
        
        


        
        public static function setListAliasTypes()
        {
            if(is_null(static::$listAliasTypes))
            {
                static::$listAliasTypes = new stdClass();
                static::$listAliasTypes->produto_unidade_medida__nome = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->produto__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->produto__identificador = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->produto__nome = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->produto__descricao = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->produto__is_float_BOOLEAN = static::TIPO_VARIAVEL_BOOLEAN;
static::$listAliasTypes->produto__id_omega_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->produto__cadastro_SEC = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$listAliasTypes->produto__preco_custo_FLOAT = static::TIPO_VARIAVEL_FLOAT;
static::$listAliasTypes->produto__prazo_reposicao_estoque_dias_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->produto__codigo_barra = static::TIPO_VARIAVEL_TEXT;
            }        
        }
        
        public static function setListAliasRelatedAttributes()
        {
            if(is_null(static::$listAliasRelatedAttributes))
            {
                static::$listAliasRelatedAttributes = new stdClass();
                static::$listAliasRelatedAttributes->produto_unidade_medida__nome = "produtoUnidadeMedidaNome";
static::$listAliasRelatedAttributes->produto__id = "id";
static::$listAliasRelatedAttributes->produto__identificador = "identificador";
static::$listAliasRelatedAttributes->produto__nome = "nome";
static::$listAliasRelatedAttributes->produto__descricao = "descricao";
static::$listAliasRelatedAttributes->produto__is_float_BOOLEAN = "isFloatBoolean";
static::$listAliasRelatedAttributes->produto__id_omega_INT = "idOmegaInt";
static::$listAliasRelatedAttributes->produto__cadastro_SEC = "cadastroSec";
static::$listAliasRelatedAttributes->produto__preco_custo_FLOAT = "precoCustoFloat";
static::$listAliasRelatedAttributes->produto__prazo_reposicao_estoque_dias_INT = "prazoReposicaoEstoqueDiasInt";
static::$listAliasRelatedAttributes->produto__codigo_barra = "codigoBarra";
            }         
        }

        public function __getList($parameters = null, $idCorporacao = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try
            {
                $filterParameters = new stdClass();
                if(isset($parameters->filterParameters))
                {
                    $filterParameters = $parameters->filterParameters;
                }
                                    
                            
            if(is_null($idCorporacao))
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            ListParameters::addFilterParameterToObject($filterParameters, "corporacaoId", $idCorporacao);           
                
                
                $sortingParameters = null;
                if(isset($parameters->sortingParameters))
                {
                    $sortingParameters = $parameters->sortingParameters;
                }
                
                $paginationParameters = null;
                if(isset($parameters->paginationParameters))
                {
                    $paginationParameters = $parameters->paginationParameters;
                }
                
                $listMappingType = static::getListMappingType($parameters->listMappingType);
                $mainTableAlias = "p";

                $whereClause = static::getWhereClauseForFilter($filterParameters, $mainTableAlias);
                $orderByClause = static::getOrderByClauseForFilter($sortingParameters, $mainTableAlias);
                $limitClause = static::getLimitClauseForFilter($paginationParameters);

                $queryWithoutLimit = "SELECT p.id FROM produto p {$whereClause}";
                $query = "SELECT pum.nome AS produto_unidade_medida__nome, p.id AS produto__id, p.identificador AS produto__identificador, p.nome AS produto__nome, p.descricao AS produto__descricao, p.is_float_BOOLEAN AS produto__is_float_BOOLEAN, p.id_omega_INT AS produto__id_omega_INT, p.cadastro_SEC AS produto__cadastro_SEC, p.preco_custo_FLOAT AS produto__preco_custo_FLOAT, p.prazo_reposicao_estoque_dias_INT AS produto__prazo_reposicao_estoque_dias_INT, p.codigo_barra AS produto__codigo_barra FROM produto p LEFT JOIN produto_unidade_medida pum ON pum.id = p.produto_unidade_medida_id_INT {$whereClause} {$orderByClause} {$limitClause}";

                $msg = $this->database->queryMensagem($queryWithoutLimit);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                
                $totalNumberOfRecords = $this->database->rows();
                if($totalNumberOfRecords == 0)
                {
                    return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                }                        
                        
                $msg = $this->database->queryMensagem($query);
                
                //operacao realizaca com sucesso
                if ($msg == null) 
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                    }                        
                
                    $resultSet = $this->database->result;

                    $objReturn = new stdClass();
                    $objReturn->dataSet = static::mapResultSetDataToList($resultSet, $listMappingType, static::$listAliasTypes);

                    if(!Helper::isNullOrEmpty($paginationParameters->recordsPerPage) && !Helper::isNullOrEmpty($paginationParameters->currentPage))
                    {
                        $lastPage = ceil($totalNumberOfRecords / $paginationParameters->recordsPerPage);
                        $objReturn->paginationParameters = new PaginationParams($paginationParameters->recordsPerPage, $paginationParameters->currentPage, $lastPage);
                    }
                    
                    return new Mensagem_generica($objReturn);
                } 
                else
                {
                    return $msg;
                }                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }



public function getId()
            {
                return $this->id;
            }

public function getIdentificador()
            {
                return $this->identificador;
            }

public function getNome()
            {
                return $this->nome;
            }

public function getNomeNormalizado()
            {
                return $this->nomeNormalizado;
            }

public function getNome_normalizado()
                {
                    return $this->nomeNormalizado;
                }

public function getDescricao()
            {
                return $this->descricao;
            }

public function getProdutoUnidadeMedidaId()
            {
                return $this->produtoUnidadeMedidaId;
            }

public function getProduto_unidade_medida_id_INT()
                {
                    return $this->produtoUnidadeMedidaId;
                }

public function getIdOmegaInt()
            {
                return $this->idOmegaInt;
            }

public function getId_omega_INT()
                {
                    return $this->idOmegaInt;
                }

public function getCadastroSec()
            {
                return $this->cadastroSec;
            }

public function getCadastro_SEC()
                {
                    return $this->cadastroSec;
                }

public function getCadastroOffsec()
            {
                return $this->cadastroOffsec;
            }

public function getCadastro_OFFSEC()
                {
                    return $this->cadastroOffsec;
                }

public function getPrecoCustoFloat()
            {
                return $this->precoCustoFloat;
            }

public function getPreco_custo_FLOAT()
                {
                    return $this->precoCustoFloat;
                }

public function getPrazoReposicaoEstoqueDiasInt()
            {
                return $this->prazoReposicaoEstoqueDiasInt;
            }

public function getPrazo_reposicao_estoque_dias_INT()
                {
                    return $this->prazoReposicaoEstoqueDiasInt;
                }

public function getCodigoBarra()
            {
                return $this->codigoBarra;
            }

public function getCodigo_barra()
                {
                    return $this->codigoBarra;
                }

public function getCorporacaoId()
            {
                return $this->corporacaoId;
            }

public function getCorporacao_id_INT()
                {
                    return $this->corporacaoId;
                }



function setId($value)
            {
                $this->id = $value;
            }

function setIdentificador($value)
            {
                $this->identificador = $value;
            }

function setNome($value)
            {
                $this->nome = $value;
            }

function setNomeNormalizado($value)
            {
                $this->nomeNormalizado = $value;
            }

public function setNome_normalizado($value)
                { 
                    $this->nomeNormalizado = $value; 
                }

function setDescricao($value)
            {
                $this->descricao = $value;
            }

function setProdutoUnidadeMedidaId($value)
            {
                $this->produtoUnidadeMedidaId = $value;
            }

public function setProduto_unidade_medida_id_INT($value)
                { 
                    $this->produtoUnidadeMedidaId = $value; 
                }

function setIdOmegaInt($value)
            {
                $this->idOmegaInt = $value;
            }

public function setId_omega_INT($value)
                { 
                    $this->idOmegaInt = $value; 
                }

function setCadastroSec($value)
            {
                $this->cadastroSec = $value;
            }

public function setCadastro_SEC($value)
                { 
                    $this->cadastroSec = $value; 
                }

function setCadastroOffsec($value)
            {
                $this->cadastroOffsec = $value;
            }

public function setCadastro_OFFSEC($value)
                { 
                    $this->cadastroOffsec = $value; 
                }

function setPrecoCustoFloat($value)
            {
                $this->precoCustoFloat = $value;
            }

public function setPreco_custo_FLOAT($value)
                { 
                    $this->precoCustoFloat = $value; 
                }

function setPrazoReposicaoEstoqueDiasInt($value)
            {
                $this->prazoReposicaoEstoqueDiasInt = $value;
            }

public function setPrazo_reposicao_estoque_dias_INT($value)
                { 
                    $this->prazoReposicaoEstoqueDiasInt = $value; 
                }

function setCodigoBarra($value)
            {
                $this->codigoBarra = $value;
            }

public function setCodigo_barra($value)
                { 
                    $this->codigoBarra = $value; 
                }

function setCorporacaoId($value)
            {
                $this->corporacaoId = $value;
            }

public function setCorporacao_id_INT($value)
                { 
                    $this->corporacaoId = $value; 
                }



public function clear()
        {$this->id = null;
$this->identificador = null;
$this->nome = null;
$this->nomeNormalizado = null;
$this->descricao = null;
$this->produtoUnidadeMedidaId = null;
if($this->objProduto_unidade_medida != null) unset($this->objProduto_unidade_medida);
$this->idOmegaInt = null;
$this->cadastroSec = null;
$this->cadastroOffsec = null;
$this->precoCustoFloat = null;
$this->prazoReposicaoEstoqueDiasInt = null;
$this->codigoBarra = null;
$this->corporacaoId = null;
if($this->objCorporacao != null) unset($this->objCorporacao);
if(is_array($this->arrEstadosCampos)){
                        unset($this->arrEstadosCampos);
                     $this->arrEstadosCampos = array();
                    }
                }

public function formatarParaSQL()
        {
$this->nomeNormalizado = $this->formatarStringNormalizadaParaComandoSQL($this->nome);
if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->id)){
$this->id = $this->formatarIntegerParaComandoSQL($this->id);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->id);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->identificador)){
$this->identificador = $this->formatarStringParaComandoSQL($this->identificador);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->identificador);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->nome)){
$this->nome = $this->formatarStringParaComandoSQL($this->nome);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->nome);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->descricao)){
$this->descricao = $this->formatarStringParaComandoSQL($this->descricao);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->descricao);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->produtoUnidadeMedidaId)){
$this->produtoUnidadeMedidaId = $this->formatarIntegerParaComandoSQL($this->produtoUnidadeMedidaId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->produtoUnidadeMedidaId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->idOmegaInt)){
$this->idOmegaInt = $this->formatarIntegerParaComandoSQL($this->idOmegaInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->idOmegaInt);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->cadastroSec)){
$this->cadastroSec = $this->formatarIntegerParaComandoSQL($this->cadastroSec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->cadastroSec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->cadastroOffsec)){
$this->cadastroOffsec = $this->formatarIntegerParaComandoSQL($this->cadastroOffsec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->cadastroOffsec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->precoCustoFloat)){
$this->precoCustoFloat = $this->formatarFloatParaComandoSQL($this->precoCustoFloat);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->precoCustoFloat);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->prazoReposicaoEstoqueDiasInt)){
$this->prazoReposicaoEstoqueDiasInt = $this->formatarIntegerParaComandoSQL($this->prazoReposicaoEstoqueDiasInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->prazoReposicaoEstoqueDiasInt);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->codigoBarra)){
$this->codigoBarra = $this->formatarStringParaComandoSQL($this->codigoBarra);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->codigoBarra);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->corporacaoId)){
$this->corporacaoId = $this->formatarIntegerParaComandoSQL($this->corporacaoId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->corporacaoId);
}

}

public function formatarParaExibicao()
        {if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->identificador)){
$this->identificador = $this->formatarStringParaExibicao($this->identificador);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->identificador);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->nome)){
$this->nome = $this->formatarStringParaExibicao($this->nome);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->nome);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->nomeNormalizado)){
$this->nomeNormalizado = $this->formatarStringParaExibicao($this->nomeNormalizado);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->nomeNormalizado);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->descricao)){
$this->descricao = $this->formatarStringParaExibicao($this->descricao);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->descricao);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->precoCustoFloat)){
$this->precoCustoFloat = $this->formatarFloatParaExibicao($this->precoCustoFloat);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->precoCustoFloat);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->codigoBarra)){
$this->codigoBarra = $this->formatarStringParaExibicao($this->codigoBarra);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->codigoBarra);
}

}

function select($id, $idCorporacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }   

            $sql = "SELECT id, identificador, nome, nome_normalizado, descricao, produto_unidade_medida_id_INT, id_omega_INT, cadastro_SEC, cadastro_OFFSEC, preco_custo_FLOAT, prazo_reposicao_estoque_dias_INT, codigo_barra, corporacao_id_INT FROM produto WHERE id = $id AND corporacao_id_INT = $idCorporacao";

            $msg = $this->database->queryMensagem($sql);
            $result = $this->database->result;

            if($msg != null && $msg->erro() )
            { 
            		$this->clear();
            		return $msg;
        	} 
        	else if($this->database->rows() == 0)
        	{
                return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nenhum registro foi encontrado."));
            }
        

            $row = $this->database->fetchArray(MYSQLI_NUM);
            
            		$this->id = $row[0];
		$this->identificador = $row[1];
		$this->nome = $row[2];
		$this->nomeNormalizado = $row[3];
		$this->descricao = $row[4];
		$this->produtoUnidadeMedidaId = $row[5];
		$this->idOmegaInt = $row[6];
		$this->cadastroSec = $row[7];
		$this->cadastroOffsec = $row[8];
		$this->precoCustoFloat = $row[9];
		$this->prazoReposicaoEstoqueDiasInt = $row[10];
		$this->codigoBarra = $row[11];

            
        }

public function delete($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (is_null($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }

            $sql = "DELETE FROM produto WHERE id= $id AND corporacao_id_INT = $idCorporacao";
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                    && $sincronizar
                        && static::idSistemaTabela() != null
                            && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_REMOVE,
                                null,
                                null,
                                $id,
                                $this->database);
            }

            return $msg;
        }

public function insert($sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null, $opcoes = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $this->defineDataCadastroInSecondsIfNotDefined();
$this->defineDataCadastroOffsetInSecondsIfNotDefined();

            
            if (is_null($opcoes))
            {
                $opcoes = $this->opcoesDAO;
            }

            if (!is_numeric($this->id))
            {
                $this->id = "null";
                if ($opcoes == null || $opcoes[Database::OPCAO_GERAR_ID] != false) 
                {
                    $this->id = EXTDAO_Sistema_sequencia::gerarId(static::nomeTabela());                    
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }    
                    else if($msg == null)
                    {
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
                else 
                {
                    $this->id = null;
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao));
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }
                    else if($msg == null)
                    {
                        $this->id = $this->database->getLastInsertId();
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
            }
            else 
            {
                $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }                        
                else if($msg == null)
                {
                   $msg = new Mensagem_token();
                   $msg->mValor = $this->id;
                }
            }
            if($msg == null || $msg->ok())
            {                
                if($sincronizar
                    && static::idSistemaTabela() != null
                    && isset($this->id)){
                    
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_INSERT,
                                null,
                                null,
                                $this->id,
                                $this->database); 
                	}
            } 
            else
            {
                $this->id = null;
            }
            
            return $msg;
            
        }

public function getSQLInsert($idCorporacao){ 

return "INSERT INTO produto (id, identificador, nome, nome_normalizado, descricao, produto_unidade_medida_id_INT, id_omega_INT, cadastro_SEC, cadastro_OFFSEC, preco_custo_FLOAT, prazo_reposicao_estoque_dias_INT, codigo_barra, corporacao_id_INT) VALUES ( $this->id ,  $this->identificador ,  $this->nome ,  $this->nomeNormalizado ,  $this->descricao ,  $this->produtoUnidadeMedidaId ,  $this->idOmegaInt ,  $this->cadastroSec ,  $this->cadastroOffsec ,  $this->precoCustoFloat ,  $this->prazoReposicaoEstoqueDiasInt ,  $this->codigoBarra ,  $idCorporacao ) "; 

}




        
        public function updateByParameters($id, $objParametros, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao))
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();

            
                
                if (isset($objParametros->identificador)) 
                {
                    $arrUpdateFields[] = " identificador = {$objParametros->identificador} ";
                }


                
                if (isset($objParametros->nome)) 
                {
                    $arrUpdateFields[] = " nome = {$objParametros->nome} ";
                }


                
                if (isset($objParametros->nomeNormalizado)) 
                {
                    $arrUpdateFields[] = " nome_normalizado = {$objParametros->nomeNormalizado} ";
                }


                
                if (isset($objParametros->descricao)) 
                {
                    $arrUpdateFields[] = " descricao = {$objParametros->descricao} ";
                }


                
                if (isset($objParametros->produtoUnidadeMedidaId)) 
                {
                    $arrUpdateFields[] = " produto_unidade_medida_id_INT = {$objParametros->produtoUnidadeMedidaId} ";
                }


                
                if (isset($objParametros->idOmegaInt)) 
                {
                    $arrUpdateFields[] = " id_omega_INT = {$objParametros->idOmegaInt} ";
                }


                
                if (isset($objParametros->cadastroSec)) 
                {
                    $arrUpdateFields[] = " cadastro_SEC = {$objParametros->cadastroSec} ";
                }


                
                if (isset($objParametros->cadastroOffsec)) 
                {
                    $arrUpdateFields[] = " cadastro_OFFSEC = {$objParametros->cadastroOffsec} ";
                }


                
                if (isset($objParametros->precoCustoFloat)) 
                {
                    $arrUpdateFields[] = " preco_custo_FLOAT = {$objParametros->precoCustoFloat} ";
                }


                
                if (isset($objParametros->prazoReposicaoEstoqueDiasInt)) 
                {
                    $arrUpdateFields[] = " prazo_reposicao_estoque_dias_INT = {$objParametros->prazoReposicaoEstoqueDiasInt} ";
                }


                
                if (isset($objParametros->codigoBarra)) 
                {
                    $arrUpdateFields[] = " codigo_barra = {$objParametros->codigoBarra} ";
                }



            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE produto SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";

            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null)
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function update($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($this->identificador)) 
                {                                      
                    $arrUpdateFields[] = " identificador = {$this->identificador} ";
                }


                
                if (isset($this->nome)) 
                {                                      
                    $arrUpdateFields[] = " nome = {$this->nome} ";
                }


                
                if (isset($this->nomeNormalizado)) 
                {                                      
                    $arrUpdateFields[] = " nome_normalizado = {$this->nomeNormalizado} ";
                }


                
                if (isset($this->descricao)) 
                {                                      
                    $arrUpdateFields[] = " descricao = {$this->descricao} ";
                }


                
                if (isset($this->produtoUnidadeMedidaId)) 
                {                                      
                    $arrUpdateFields[] = " produto_unidade_medida_id_INT = {$this->produtoUnidadeMedidaId} ";
                }


                
                if (isset($this->idOmegaInt)) 
                {                                      
                    $arrUpdateFields[] = " id_omega_INT = {$this->idOmegaInt} ";
                }


                
                if (isset($this->cadastroSec)) 
                {                                      
                    $arrUpdateFields[] = " cadastro_SEC = {$this->cadastroSec} ";
                }


                
                if (isset($this->cadastroOffsec)) 
                {                                      
                    $arrUpdateFields[] = " cadastro_OFFSEC = {$this->cadastroOffsec} ";
                }


                
                if (isset($this->precoCustoFloat)) 
                {                                      
                    $arrUpdateFields[] = " preco_custo_FLOAT = {$this->precoCustoFloat} ";
                }


                
                if (isset($this->prazoReposicaoEstoqueDiasInt)) 
                {                                      
                    $arrUpdateFields[] = " prazo_reposicao_estoque_dias_INT = {$this->prazoReposicaoEstoqueDiasInt} ";
                }


                
                if (isset($this->codigoBarra)) 
                {                                      
                    $arrUpdateFields[] = " codigo_barra = {$this->codigoBarra} ";
                }


            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE produto SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function updateByInstanceUsingParameters($id, $parameters, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($parameters->identificador)) 
                {                                      
                    $arrUpdateFields[] = " identificador = {$this->identificador} ";
                }
                
                if (isset($parameters->nome)) 
                {                                      
                    $arrUpdateFields[] = " nome = {$this->nome} ";
                }
                
                if (isset($parameters->nomeNormalizado)) 
                {                                      
                    $arrUpdateFields[] = " nome_normalizado = {$this->nomeNormalizado} ";
                }
                
                if (isset($parameters->descricao)) 
                {                                      
                    $arrUpdateFields[] = " descricao = {$this->descricao} ";
                }
                
                if (isset($parameters->produtoUnidadeMedidaId)) 
                {                                      
                    $arrUpdateFields[] = " produto_unidade_medida_id_INT = {$this->produtoUnidadeMedidaId} ";
                }
                
                if (isset($parameters->idOmegaInt)) 
                {                                      
                    $arrUpdateFields[] = " id_omega_INT = {$this->idOmegaInt} ";
                }
                
                if (isset($parameters->cadastroSec)) 
                {                                      
                    $arrUpdateFields[] = " cadastro_SEC = {$this->cadastroSec} ";
                }
                
                if (isset($parameters->cadastroOffsec)) 
                {                                      
                    $arrUpdateFields[] = " cadastro_OFFSEC = {$this->cadastroOffsec} ";
                }
                
                if (isset($parameters->precoCustoFloat)) 
                {                                      
                    $arrUpdateFields[] = " preco_custo_FLOAT = {$this->precoCustoFloat} ";
                }
                
                if (isset($parameters->prazoReposicaoEstoqueDiasInt)) 
                {                                      
                    $arrUpdateFields[] = " prazo_reposicao_estoque_dias_INT = {$this->prazoReposicaoEstoqueDiasInt} ";
                }
                
                if (isset($parameters->codigoBarra)) 
                {                                      
                    $arrUpdateFields[] = " codigo_barra = {$this->codigoBarra} ";
                }
            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE produto SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }

}

?>
