import { Model } from "lib/model";
import { TipoPontoEletronico } from "aplicacao/comum/geral";
import { GeoLocationHelper, Helper } from "lib/geral";
import { PontoEletronico as PontoEletronicoRemoteModel } from "aplicacao/modulo/mapa/remoteModel";

export class PontoEletronico extends Model
{
    public latitude :number;
    public longitude :number;
    public nomePessoa :string;
    public idPessoa :number;
    public dataPonto :Date;
    public tipoPonto :TipoPontoEletronico;

    public static factory(remoteObject: PontoEletronicoRemoteModel)
    {
        let instance = new PontoEletronico();

        instance.latitude = GeoLocationHelper.getFloatValue(remoteObject.latitude);
        instance.longitude = GeoLocationHelper.getFloatValue(remoteObject.longitude);
        instance.nomePessoa = remoteObject.nomePessoa;
        instance.idPessoa = Helper.parseInt(remoteObject.idPessoa);
        instance.dataPonto = DateUtil.getDateObject(Helper.parseInt(remoteObject.dataSec) + Helper.parseInt(remoteObject.dataOffsec));
        instance.tipoPonto = Helper.parseInt(remoteObject.isEntrada) == 1 ? TipoPontoEletronico.PONTO_ENTRADA : TipoPontoEletronico.PONTO_SAIDA;

        return instance;
    }

}