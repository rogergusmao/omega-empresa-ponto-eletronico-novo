"use strict";
exports.__esModule = true;
var LocalStorage_1 = require("../../LocalStorage");
var JSON_1 = require("../../../serializer/JSON");
var JSON_2 = require("../../../unserializer/JSON");
var TagTree_1 = require("./TagTree/TagTree");
var Cache_Backend_LocalStorage = (function () {
    function Cache_Backend_LocalStorage() {
        this.TAG_TREE_KEY = "_tagthree";
        this.BACKEND_TYPE = "LocalStorage";
        this.BACKEND_KEY_PREFIX = "_key_prefix";
        this._localStorage = new LocalStorage_1.Cache_LocalStorage(new JSON_1.Serializer_JSON(), new JSON_2.Unserializer_JSON());
    }
    Cache_Backend_LocalStorage.prototype.setConfig = function (config) {
        this._config = config;
    };
    // Add cache record to the backend cache
    Cache_Backend_LocalStorage.prototype.add = function (key, data, tagKey) {
        // Get the internal tag three
        var tagTree = this.getTagTree();
        // Add the new key to the tag three
        tagTree.addKeyToTag(tagKey, key);
        // Add the data to the local storage
        this._localStorage.add(this.getUniqueKey(tagKey, key), data);
        // Save the tag three to the local cache
        this.saveTagTree();
        return true;
    };
    Cache_Backend_LocalStorage.prototype.get = function (key, tagKey) {
        // Get the internal tag three
        var tagTree = this.getTagTree();
        if (!tagTree.isValidKeyAndTagKeyCombination(key, tagKey)) {
            return null;
        }
        // Get the data from the local storage
        return this._localStorage.get(this.getUniqueKey(tagKey, key));
    };
    // Remove cache record if available
    Cache_Backend_LocalStorage.prototype.remove = function (key, tagKey) {
        // Get the internal tag three
        var tagTree = this.getTagTree();
        // Check if the tag three contains the key
        if (!tagTree.isValidKeyAndTagKeyCombination(key, tagKey)) {
            return false;
        }
        // Remove the key from the tag
        tagTree.removeKeyFromTag(key, tagKey);
        // Check if the tag is empty
        if (tagTree.tagEmpty(tagKey)) {
            tagTree.clear(tagKey);
        }
        // Remove the cache record from local storage
        this._localStorage.remove(this.getUniqueKey(tagKey, key));
        // Save the tag three to the local cache
        this.saveTagTree();
        return true;
    };
    // Remove all cache records matching the tag
    Cache_Backend_LocalStorage.prototype.clear = function (tagKey) {
        // Get the internal tag three
        var tagTree = this.getTagTree();
        if (!tagTree.tagExists(tagKey)) {
            return false;
        }
        // Clear the tag keys
        tagTree.clear(tagKey);
        // Save the tag three to the local cache
        this.saveTagTree();
        return true;
    };
    // Get the backend class name
    Cache_Backend_LocalStorage.prototype.getType = function () {
        return this.BACKEND_TYPE;
    };
    Cache_Backend_LocalStorage.prototype.getTagTree = function () {
        if (this._tagTree != undefined) {
            return this._tagTree;
        }
        var tagTree = this._localStorage.get(this.TAG_TREE_KEY);
        if (tagTree == null) {
            tagTree = this.buildTagTree();
        }
        else {
            tagTree = TagTree_1.Cache_Backend_LocalStorage_TagTree.fromObject(tagTree);
        }
        this._tagTree = tagTree;
        return tagTree;
    };
    Cache_Backend_LocalStorage.prototype.buildTagTree = function () {
        var tagTree = new TagTree_1.Cache_Backend_LocalStorage_TagTree();
        this._localStorage.add(this.TAG_TREE_KEY, tagTree);
        return tagTree;
    };
    Cache_Backend_LocalStorage.prototype.saveTagTree = function () {
        this._localStorage.add(this.TAG_TREE_KEY, this._tagTree);
    };
    Cache_Backend_LocalStorage.prototype.getUniqueKey = function (tagKey, key) {
        return this.BACKEND_KEY_PREFIX + tagKey + key;
    };
    Cache_Backend_LocalStorage.prototype.serializeObject = function (data) {
        return JSON.stringify(data);
    };
    Object.defineProperty(Cache_Backend_LocalStorage.prototype, "config", {
        // Get the cache config
        get: function () {
            return this._config;
        },
        enumerable: true,
        configurable: true
    });
    return Cache_Backend_LocalStorage;
}());
exports.Cache_Backend_LocalStorage = Cache_Backend_LocalStorage;
