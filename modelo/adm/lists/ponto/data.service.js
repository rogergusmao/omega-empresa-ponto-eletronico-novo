/*

 Arquivo gerado atrav�s de gerador de c�digo em 07/01/2018 as 15:54:12.
 Para que o arquivo n�o seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

 Tabela correspondente: ponto
 Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

 */

returnObject.getAllComboBoxesDataForPontoFilter = function ()
{
    return $http({
        url: baseURLAdm + "/webservice.php?class=EXTDAO_Ponto&jsonOutput=true&action=__getFilterComboBoxesData",
        method: "POST",
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    });

};

returnObject.getPontoList = function (listParams)
{
    listParams.listMappingType = Constants.ListMappingType.GRID;
    return $http.post(baseURLAdm + "/webservice.php?class=EXTDAO_Ponto&action=__getList", listParams);
};

returnObject.removeRecords = function (recordsPKs)
{
    return $http.post(baseURLAdm + "/webservice.php?class=EXTDAO_Ponto&action=remove", recordsPKs);
};

returnObject.getListOfPessoaIdForPontoList = function ()
{
    var params = {filterData: null, listMappingType: Constants.ListMappingType.COMBOBOX};
    return $http.post(baseURLAdm + "/webservice.php?class=EXTDAO_Pessoa&action=__getList", params);
};
returnObject.getListOfEmpresaIdForPontoList = function ()
{
    var params = {filterData: null, listMappingType: Constants.ListMappingType.COMBOBOX};
    return $http.post(baseURLAdm + "/webservice.php?class=EXTDAO_Empresa&action=__getList", params);
};
returnObject.getListOfProfissaoIdForPontoList = function ()
{
    var params = {filterData: null, listMappingType: Constants.ListMappingType.COMBOBOX};
    return $http.post(baseURLAdm + "/webservice.php?class=EXTDAO_Profissao&action=__getList", params);
};
returnObject.getListOfUsuarioIdForPontoList = function ()
{
    var params = {filterData: null, listMappingType: Constants.ListMappingType.COMBOBOX};
    return $http.post(baseURLAdm + "/webservice.php?class=EXTDAO_Usuario&action=__getList", params);
};
returnObject.getListOfTipoPontoIdForPontoList = function ()
{
    var params = {filterData: null, listMappingType: Constants.ListMappingType.COMBOBOX};
    return $http.post(baseURLAdm + "/webservice.php?class=EXTDAO_Tipo_ponto&action=__getList", params);
};
                

