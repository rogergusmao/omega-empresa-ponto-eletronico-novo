export abstract class MapaTemplate
{
    public constructor(nomeTemplate: string)
    {
        this.nomeTemplate = nomeTemplate;
    }

    public nomeTemplate: string;
    public abstract getStyles() : any;

}