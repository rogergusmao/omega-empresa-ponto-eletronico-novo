<?php

/*
*
* -------------------------------------------------------
* NOME DO FORMUL�RIO: empresa_perfil
* DATA DE GERA��O:    02.07.2012
* ARQUIVO:            empresa_perfil.php
* TABELA MYSQL:       empresa_perfil
* BANCO DE DADOS:     ponto_eletronico
* -------------------------------------------------------
*
*/

if (isset($_GET["contador"]))
{

    $numeroRegistroInterno = Helper::GET("contador");
}

if (isset($identificadorRelacionamento) && is_numeric($identificadorRelacionamento))
{

    $objEmpresa_perfil = new EXTDAO_Empresa_perfil();
    $objEmpresa_perfil->select($identificadorRelacionamento);
}
else
{

    $objEmpresa_perfil = new EXTDAO_Empresa_perfil();
}

$objArgEmpresa_perfil = new Generic_Argument();
$objEmpresa_perfil->formatarParaExibicao();

?>

<input type="hidden" name="empresa_perfil_id_<?= $numeroRegistroInterno ?>"
       id="empresa_perfil_id_<?= $numeroRegistroInterno ?>" value="<?= $objEmpresa_perfil->getId(); ?>">

<table class="tabela_form">

    <tr class="tr_form">


        <?

        $objArgEmpresa_perfil = new Generic_Argument();

        $objArgEmpresa_perfil->numeroDoRegistro = "";
        $objArgEmpresa_perfil->label = $objEmpresa_perfil->label_perfil_id_INT;
        $objArgEmpresa_perfil->valor = $objEmpresa_perfil->getPerfil_id_INT();
        $objArgEmpresa_perfil->classeCss = "input_text";
        $objArgEmpresa_perfil->classeCssFocus = "focus_text";
        $objArgEmpresa_perfil->obrigatorio = false;
        $objArgEmpresa_perfil->largura = 200;
        $objArgEmpresa_perfil->nome = "empresa_perfil_perfil_id_INT_{$numeroRegistroInterno}";
        $objArgEmpresa_perfil->id = "empresa_perfil_perfil_id_INT_{$numeroRegistroInterno}";

        $objEmpresa_perfil->addInfoCampos("perfil_id_INT", $objArgEmpresa_perfil->label, "TEXTO", $objArgEmpresa_perfil->obrigatorio);

        ?>

        <td class="td_form_label"><?= $objArgEmpresa_perfil->getLabel() ?></td>
        <td class="td_form_campo">
            <?= $objEmpresa_perfil->objPerfil->getComboBox($objArgEmpresa_perfil); ?>
        </td>


    <tr>
        <td colspan="4" class="td_botao_remover_da_lista"><input class="botoes_form" type="button"
                                                                 value="Remover Relacionamento Da Empresa Com O Grupo"
                                                                 onclick="javascript:removerDivAjaxEmLista(this);"></td>
    </tr>
</table><br/>

