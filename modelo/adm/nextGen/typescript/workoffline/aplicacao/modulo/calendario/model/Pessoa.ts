import * as Moment from 'moment';
import * as MomentDurationFormat from 'moment-duration-format';

import { Model } from 'lib/model';
import { Color, RandomColor } from 'lib/geral';
import { Evento, PontoEletronico } from 'aplicacao/modulo/calendario/model';

export class Pessoa extends Model
{
    public idPessoa :number;
    public nomePessoa :string;   
    public corBackground: Color;
    public corForeground: Color;
    public eventos: Array<Evento>

    public addEvento(pontoInicial: PontoEletronico, pontoFinal? :PontoEletronico)
    {
        let evento = new Evento()            
        evento.titulo = pontoInicial.nomePessoa;
        evento.idPessoa = pontoInicial.idPessoa;
        evento.pontoInicial = pontoInicial;
        evento.pontoFinal = pontoFinal;
        evento.corBackgroundEvento = this.corBackground;
        evento.corForegroudEvento = this.corForeground;

        this.eventos.push(evento);
    }

    public getTempoTotal() :Moment.Duration
    {
        let totalTempoPonto = Moment.duration(0, 'second');
        for(let evento of this.eventos)
        {
            if(evento.pontoFinal != null)
            {
                let diferenca = Moment(evento.pontoFinal.dataPonto).diff(Moment(evento.pontoInicial.dataPonto));
                totalTempoPonto = totalTempoPonto.add(diferenca);            
            }    
        }

        return totalTempoPonto;
                
    }

    public getTempoTotalFormatado() :string
    {
        let duracao = this.getTempoTotal();
        return `${duracao.format('hh:mm')}`;
    }

    public static factory(pontoInicial: PontoEletronico, pontoFinal? :PontoEletronico)
    {
        let instance = new Pessoa();
        var corAleatoria = new RandomColor();

        instance.nomePessoa = pontoInicial.nomePessoa;
        instance.idPessoa = pontoInicial.idPessoa;
        
        instance.corBackground = corAleatoria.mainColor;
        instance.corForeground = corAleatoria.foregroundColor;     
        instance.eventos = Array<Evento>();

        return instance;
    }

}