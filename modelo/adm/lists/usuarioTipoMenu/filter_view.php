<?php


        /*

        Arquivo gerado através de gerador de código em 07/09/2017 as 02:40:36.
        Para que o arquivo não seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

        Tabela correspondente: usuario_tipo_menu
        Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

        */
?>

<?

                   global $objFilter;

                   ?>

                    <?=$objFilter->getFormDebugPanel(); ?>
                    <div class='row'>
                            <div class='col-sm-12'>
                                <div class='box'>
                                    <div class='box-header blue-background'>
                                        <div class='title'>
                                            <div class='icon-edit'></div>
                                            <?=I18N::getExpression("Filtro"); ?>
                                        </div>
                                        <div class='actions'>
                                            <a class="btn box-collapse btn-xs btn-link" href="#"><i></i></a>
                                        </div>
                                    </div>
                                    <div class='box-content'>

                                        <form name="<?=$objFilter->getFormName() ?>" class="form form-horizontal" style="margin-bottom: 0;">

                                            <fieldset>

                <?

                    $objCampo = $objFilter->getNumericInputInstance("id");
                            $objCampo->setLabel("Id");
                            
                            $objCampo->setDefaultValues();
                            $objCampo->setPlaceHolder(I18N::getExpression("Informe a Id."));$objCampo->setAllowNegative(false);
                                 $objCampo->setDecimalPlaces(0);
                                 $objCampo->setHideThousandsSeparator(true);

                ?>

                <div class='<?=$objCampo->getFormGroupCssClass() ?>' ng-class="<?=$objCampo->getFormGroupAngularClass(); ?>">

                    <?=$objCampo->renderLabel(); ?>
                    <div class='<?=$objCampo->getContainerCssClass(); ?>'>

                        <?=$objCampo->render(); ?>
                        <?=$objCampo->renderValidationBlock(); ?>

                    </div>
                </div>

                <?=$objFilter->renderHorizontalLine(); ?>

                

                <?

                    $objCampo = $objFilter->getSelectInstance("usuarioTipoId");
                            $objCampo->setLabel(I18N::getExpression("UsuariotipoidINT"));
                            
                            $objCampo->setPlaceHolder(I18N::getExpression("Selecione uma usuariotipoidINT da lista..."), false);
                            $objCampo->setMatchExpression("{{id}}");
                            $objCampo->setCloseOnSelect(true);
                            $objCampo->setRefreshDelay(0);

                            $propsFilter = new PropsFilter();
$propsFilter->addFieldToFilter("id");

                            $selectChoiceContent = new SelectChoiceContent("usuarioTipoId");$selectChoiceContent->setMainString(new AngularString("id", "div", Select::getSelectHighLightFilter()));


                            $objCampo->setFilter($propsFilter);
                            $objCampo->setSelectChoiceContent($selectChoiceContent);$objCampo->addValidation(new RequiredValidation(I18N::getExpression("A UsuariotipoidINT é de preenchimento obrigatório.")));

                ?>

                <div class='<?=$objCampo->getFormGroupCssClass() ?>' ng-class="<?=$objCampo->getFormGroupAngularClass(); ?>">

                    <?=$objCampo->renderLabel(); ?>
                    <div class='<?=$objCampo->getContainerCssClass(); ?>'>

                        <?=$objCampo->render(); ?>
                        <?=$objCampo->renderValidationBlock(); ?>

                    </div>
                </div>

                <?=$objFilter->renderHorizontalLine(); ?>

                

                <?

                    $objCampo = $objFilter->getTextInputInstance("areaMenu");
                            $objCampo->setLabel("Areamenu");
                            
                            $objCampo->setDefaultValues();
                            $objCampo->setPlaceHolder("Informe a Areamenu.");$objCampo->addValidation(new MaxLengthValidation(255, I18N::getExpression("O tamanho máximo da Areamenu deve ser de {0} caracteres.")));$objCampo->addValidation(new RequiredValidation(I18N::getExpression("A Areamenu é de preenchimento obrigatório.")));

                ?>

                <div class='<?=$objCampo->getFormGroupCssClass() ?>' ng-class="<?=$objCampo->getFormGroupAngularClass(); ?>">

                    <?=$objCampo->renderLabel(); ?>
                    <div class='<?=$objCampo->getContainerCssClass(); ?>'>

                        <?=$objCampo->render(); ?>
                        <?=$objCampo->renderValidationBlock(); ?>

                    </div>
                </div>

                <?=$objFilter->renderHorizontalLine(); ?>

                

                <?

                    $objCampo = $objFilter->getSelectInstance("areaMenuId");
                            $objCampo->setLabel(I18N::getExpression("AreamenuidINT"));
                            
                            $objCampo->setPlaceHolder(I18N::getExpression("Selecione uma areamenuidINT da lista..."), false);
                            $objCampo->setMatchExpression("");
                            $objCampo->setCloseOnSelect(true);
                            $objCampo->setRefreshDelay(0);

                            
                            

                            
                            $objCampo->addValidation(new RequiredValidation(I18N::getExpression("A AreamenuidINT é de preenchimento obrigatório.")));

                ?>

                <div class='<?=$objCampo->getFormGroupCssClass() ?>' ng-class="<?=$objCampo->getFormGroupAngularClass(); ?>">

                    <?=$objCampo->renderLabel(); ?>
                    <div class='<?=$objCampo->getContainerCssClass(); ?>'>

                        <?=$objCampo->render(); ?>
                        <?=$objCampo->renderValidationBlock(); ?>

                    </div>
                </div>

                <?=$objFilter->renderHorizontalLine(); ?>

                </fieldset>

                    <div class='form-actions form-actions-padding-sm'>
                        <div class='row'>
                            <div class='col-md-11 col-md-offset-1'>

                                <?= $objFilter->renderSubmitButton(); ?>
                                <?= $objFilter->renderClearButton(); ?>

                            </div>
                        </div>
                    </div>
                </form></div>
                        </div>
                    </div>
                </div>
            
