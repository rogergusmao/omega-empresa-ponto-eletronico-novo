import { RemoteModel } from 'lib/model';

export class WifiNetwork extends RemoteModel
{
    public ssid :string;
    public dataInicioSec :string;
    public dataInicioOffsec : string;
    public dataFimSec :string;
    public dataFimOffsec :string;
    public idEmpresa :string;
    public idUsuario :string;
}