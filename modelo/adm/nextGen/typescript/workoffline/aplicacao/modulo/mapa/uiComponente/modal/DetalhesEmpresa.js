"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var React = require("react");
var uiComponente_1 = require("lib/uiComponente");
var DetalhesEmpresa = (function (_super) {
    __extends(DetalhesEmpresa, _super);
    function DetalhesEmpresa() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    DetalhesEmpresa.prototype.setDefaultBody = function () {
        var modelEmpresa = this.props.modelEmpresa;
        var listItems = modelEmpresa.wifiNetworks.map(function (wifiNetwork) {
            return (<p><b>{wifiNetwork.ssid}</b> 
                (De {DateUtil.getFormattedDateTime(wifiNetwork.dataInicio)} a {DateUtil.getFormattedDateTime(wifiNetwork.dataFim)}</p>);
        });
        if (listItems.length > 0) {
            listItems = [<div className="lista-redes-wifi">Redes Wifi detectadas:</div>].concat(listItems);
        }
        this.bodyContent = (<div className="corpo-modal-detalhes">              
                <p><b>Nome da Empresa:</b>{modelEmpresa.nomeEmpresa}</p>                 
                <p><b>Identificador da Empresa:</b> {modelEmpresa.idEmpresa}</p>
                <p><b>Nome da Empresa:</b> {modelEmpresa.nomeEmpresa}</p>
                {listItems}  
            </div>);
    };
    return DetalhesEmpresa;
}(uiComponente_1.PainelFlutuante));
exports.DetalhesEmpresa = DetalhesEmpresa;
