<?php


        /*

        Arquivo gerado através de gerador de código em 12/05/2018 as 14:17:54.
        Para que o arquivo não seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

        Tabela correspondente: tarefa_item
        Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

        */
        


class DAO_Tarefa_item extends Generic_DAO
                        {
                            const NOME_ENTIDADE = "tarefa_item";

                            public static $nomeTabela = null;
                            public static $idSistemaTabela = false;
                            public static $databaseFieldsRelatedAttributes = null;
	                        public static $databaseFieldTypes = null;
	                        public static $databaseFieldNames = null;
	                        public static $listAliasTypes = null;
	                        public static $listAliasRelatedAttributes = null;
	                        
	                        public $id;
public $descricao;
public $dataConclusaoSec;
public $dataConclusaoOffsec;
public $tarefaId;
public $cadastroSec;
public $cadastroOffsec;
public $criadoPeloUsuarioId;
public $executadaPeloUsuarioId;
public $corporacaoId;
public $seqInt;
public $latitudeInt;
public $longitudeInt;
public $latitudeRealInt;
public $longitudeRealInt;

public $labelId;
public $labelDescricao;
public $labelDataConclusaoSec;
public $labelDataConclusaoOffsec;
public $labelTarefaId;
public $labelCadastroSec;
public $labelCadastroOffsec;
public $labelCriadoPeloUsuarioId;
public $labelExecutadaPeloUsuarioId;
public $labelCorporacaoId;
public $labelSeqInt;
public $labelLatitudeInt;
public $labelLongitudeInt;
public $labelLatitudeRealInt;
public $labelLongitudeRealInt;

public static $arrPK = array("id");

public function __construct($configDAO = null)
        {
            parent::__construct($configDAO);
            static::__setStaticValues($this->database);           
        }

public static function idSistemaTabela($databaseObject = null){
            if(!(static::$idSistemaTabela))
            {
                static::$idSistemaTabela = EXTDAO_Sistema_tabela::getIdDaSistemaTabela(static::nomeTabela(), $databaseObject);
            }
            return static::$idSistemaTabela;
        }

public function getIdSistemaTabela(){
            return static::idSistemaTabela();
        }

public static function nomeTabela(){
            if(is_null(static::$nomeTabela))
            {
                static::$nomeTabela = "tarefa_item";
            }
            return static::$nomeTabela;
        }

public function getNomeTabela(){
            return static::nomeTabela();
        }

public static function __setStaticValues($databaseObject = null)
                   {    
                        static::nomeTabela();
                        
                        static::idSistemaTabela($databaseObject);
                   
                        static::setDatabaseFieldNames();
                        static::setDatabaseFieldTypes();
                        static::setDatabaseFieldsRelatedAttributeNames();
                        
                        static::setListAliasTypes();
                        static::setListAliasRelatedAttributes();
                   }

public function getExpressaoLabel()
                   {
                        return "{id}";
                   }

public static function setDatabaseFieldNames()
        {
            if(is_null(static::$databaseFieldNames))
            {
                static::$databaseFieldNames = new stdClass();
                static::$databaseFieldNames->id = "id";
static::$databaseFieldNames->descricao = "descricao";
static::$databaseFieldNames->dataConclusaoSec = "data_conclusao_SEC";
static::$databaseFieldNames->dataConclusaoOffsec = "data_conclusao_OFFSEC";
static::$databaseFieldNames->tarefaId = "tarefa_id_INT";
static::$databaseFieldNames->cadastroSec = "cadastro_SEC";
static::$databaseFieldNames->cadastroOffsec = "cadastro_OFFSEC";
static::$databaseFieldNames->criadoPeloUsuarioId = "criado_pelo_usuario_id_INT";
static::$databaseFieldNames->executadaPeloUsuarioId = "executada_pelo_usuario_id_INT";
static::$databaseFieldNames->corporacaoId = "corporacao_id_INT";
static::$databaseFieldNames->seqInt = "seq_INT";
static::$databaseFieldNames->latitudeInt = "latitude_INT";
static::$databaseFieldNames->longitudeInt = "longitude_INT";
static::$databaseFieldNames->latitudeRealInt = "latitude_real_INT";
static::$databaseFieldNames->longitudeRealInt = "longitude_real_INT";

            }
                      
        }

public static function setDatabaseFieldTypes()
        {
            if(is_null(static::$databaseFieldTypes))
            {
                static::$databaseFieldTypes = new stdClass();
                static::$databaseFieldTypes->id = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->descricao = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->dataConclusaoSec = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$databaseFieldTypes->dataConclusaoOffsec = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$databaseFieldTypes->tarefaId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->cadastroSec = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$databaseFieldTypes->cadastroOffsec = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$databaseFieldTypes->criadoPeloUsuarioId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->executadaPeloUsuarioId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->corporacaoId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->seqInt = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->latitudeInt = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->longitudeInt = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->latitudeRealInt = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->longitudeRealInt = static::TIPO_VARIAVEL_INTEGER;

            }
            
        }

public static function setDatabaseFieldsRelatedAttributeNames()
        {
            if(is_null(static::$databaseFieldsRelatedAttributes))
            {
                static::$databaseFieldsRelatedAttributes = new stdClass();
                static::$databaseFieldsRelatedAttributes->id = "id";
static::$databaseFieldsRelatedAttributes->descricao = "descricao";
static::$databaseFieldsRelatedAttributes->data_conclusao_SEC = "dataConclusaoSec";
static::$databaseFieldsRelatedAttributes->data_conclusao_OFFSEC = "dataConclusaoOffsec";
static::$databaseFieldsRelatedAttributes->tarefa_id_INT = "tarefaId";
static::$databaseFieldsRelatedAttributes->cadastro_SEC = "cadastroSec";
static::$databaseFieldsRelatedAttributes->cadastro_OFFSEC = "cadastroOffsec";
static::$databaseFieldsRelatedAttributes->criado_pelo_usuario_id_INT = "criadoPeloUsuarioId";
static::$databaseFieldsRelatedAttributes->executada_pelo_usuario_id_INT = "executadaPeloUsuarioId";
static::$databaseFieldsRelatedAttributes->corporacao_id_INT = "corporacaoId";
static::$databaseFieldsRelatedAttributes->seq_INT = "seqInt";
static::$databaseFieldsRelatedAttributes->latitude_INT = "latitudeInt";
static::$databaseFieldsRelatedAttributes->longitude_INT = "longitudeInt";
static::$databaseFieldsRelatedAttributes->latitude_real_INT = "latitudeRealInt";
static::$databaseFieldsRelatedAttributes->longitude_real_INT = "longitudeRealInt";

            }           
        }

public function setAllFieldsToHumanFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["descricao"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["data_conclusao_SEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["data_conclusao_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["tarefa_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["cadastro_SEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["cadastro_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["criado_pelo_usuario_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["executada_pelo_usuario_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["seq_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["latitude_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["longitude_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["latitude_real_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["longitude_real_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
}

public function setAllFieldsToDatabaseFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["descricao"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["data_conclusao_SEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["data_conclusao_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["tarefa_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["cadastro_SEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["cadastro_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["criado_pelo_usuario_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["executada_pelo_usuario_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["seq_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["latitude_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["longitude_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["latitude_real_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["longitude_real_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
}

  public function getFkObjTarefa() 
                {
                    if($this->objTarefa == null)
                    {                        
                        $this->objTarefa = new EXTDAO_Tarefa_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getTarefa_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objTarefa->clear();
                    }
                    elseif($this->objTarefa->getId() != $idFK)
                    {
                        $this->objTarefa->select($idFK);
                    }
                    return $this->objTarefa;
                }
  public function getFkObjCriado_pelo_usuario() 
                {
                    if($this->objCriado_pelo_usuario == null)
                    {                        
                        $this->objCriado_pelo_usuario = new EXTDAO_Criado_pelo_usuario_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getCriado_pelo_usuario_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objCriado_pelo_usuario->clear();
                    }
                    elseif($this->objCriado_pelo_usuario->getId() != $idFK)
                    {
                        $this->objCriado_pelo_usuario->select($idFK);
                    }
                    return $this->objCriado_pelo_usuario;
                }
  public function getFkObjExecutada_pelo_usuario() 
                {
                    if($this->objExecutada_pelo_usuario == null)
                    {                        
                        $this->objExecutada_pelo_usuario = new EXTDAO_Executada_pelo_usuario_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getExecutada_pelo_usuario_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objExecutada_pelo_usuario->clear();
                    }
                    elseif($this->objExecutada_pelo_usuario->getId() != $idFK)
                    {
                        $this->objExecutada_pelo_usuario->select($idFK);
                    }
                    return $this->objExecutada_pelo_usuario;
                }
  public function getFkObjCorporacao() 
                {
                    if($this->objCorporacao == null)
                    {                        
                        $this->objCorporacao = new EXTDAO_Corporacao_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getCorporacao_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objCorporacao->clear();
                    }
                    elseif($this->objCorporacao->getId() != $idFK)
                    {
                        $this->objCorporacao->select($idFK);
                    }
                    return $this->objCorporacao;
                }


public function setLabels()
        {$this->labelId = "";
$this->labelDescricao = "";
$this->labelDataConclusaoSec = "";
$this->labelDataConclusaoOffsec = "";
$this->labelTarefaId = "";
$this->labelCadastroSec = "";
$this->labelCadastroOffsec = "";
$this->labelCriadoPeloUsuarioId = "";
$this->labelExecutadaPeloUsuarioId = "";
$this->labelCorporacaoId = "";
$this->labelSeqInt = "";
$this->labelLatitudeInt = "";
$this->labelLongitudeInt = "";
$this->labelLatitudeRealInt = "";
$this->labelLongitudeRealInt = "";
}

public function __actionAdd($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                $this->setByObject($parameters);
                $this->setDataCadastroAndOffsetInSecondsFromUser($parameters);
                
                $this->formatarParaSQL();
                $msg = $this->insert(true);
                
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                return new Mensagem(null, I18N::getExpression("Tarefa item adicionado com sucesso."));
                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionEdit($parameters = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try {

                $this->setByObject($parameters);
                $this->setAllFieldsToHumanFormat();
                
                $this->formatarParaSQL();
                $msg = $this->updateByInstanceUsingParameters($this->id, $parameters, true);
                if($msg != null && $msg->erro())
                {
                     return $msg;
                }
        
                return new Mensagem(null, I18N::getExpression("Tarefa item editado com sucesso."));
            }
            catch(Exception $ex)
            {                
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionRemove($parameters = null)
        {
            $successulRemovalCount = 0;
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                foreach($parameters as $record)
                {
                    if(is_numeric($record->id))
                    {
                        $msg = $this->delete($record->id, true);
                        if($msg == null || $msg->ok())
                        {
                            $successulRemovalCount++;
                        }
                    }
                }
                
                if($successulRemovalCount > 0)
                {
                    $successMessage = count($successulRemovalCount) > 1 ? I18N::getExpression("{0} Tarefa item foram removidos com sucesso.", $successulRemovalCount) : I18N::getExpression("Tarefa item removido com sucesso.");
                    return new Mensagem(null, $successMessage);
                }
                else
                {
                    $errorMessage = count($parameters) > 1 ? I18N::getExpression("Falha ao remover Tarefa item.") : I18N::getExpression("Falha ao remover Tarefa item.");
                    return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, $errorMessage);                    
                }
            }
            catch (Exception $ex) 
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }

public function __getRecord($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }
            
            try
            {            
                $returnObject = new stdClass();            
                $whereClause = static::getWhereClauseForFilter($parameters->filterParameters);
                $query = "SELECT  id, descricao, data_conclusao_SEC, data_conclusao_OFFSEC, tarefa_id_INT, cadastro_SEC, cadastro_OFFSEC, criado_pelo_usuario_id_INT, executada_pelo_usuario_id_INT, corporacao_id_INT, seq_INT, latitude_INT, longitude_INT, latitude_real_INT, longitude_real_INT FROM tarefa_item {$whereClause}";
    
                $msg = $this->database->queryMensagem($query);                
                if ($msg == null)
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Não foi encontrado nenhum registro"));
                    }                    
                    $resultSet = $this->database->result;
                    $resultObject = static::mapResultSetDataToRecord($resultSet);
                    
                    $returnObject->formData = $resultObject;
                    $returnObject->comboBoxesData = null;
                    if($parameters->loadComboBoxesData)
                    {
                        $this->setByObject($resultObject);
                        $returnObject->comboBoxesData =  $this->getFormComboBoxesData();
                    }                    
    
                    
    
                    return new Mensagem_generica($returnObject);
                } 
                else 
                {
                    return $msg;
                }
                
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }


        
        public function __getFilterComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
                
        public function __getFormComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
        
        protected function getFormComboBoxesData()
        {
            $comboBoxesData = new stdClass();
            $listParameters = new stdClass();
            $listParameters->listMappingType = static::LIST_MAPPING_TYPE_COMBOBOX;
            
            
                    
                    $objTarefa = new EXTDAO_Tarefa();
                    $comboBoxesData->fieldTarefaId = $objTarefa->__getList($listParameters);

            return $comboBoxesData;
        }
        
        


        
        public static function setListAliasTypes()
        {
            if(is_null(static::$listAliasTypes))
            {
                static::$listAliasTypes = new stdClass();
                static::$listAliasTypes->tarefa__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->tarefa_item__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->tarefa_item__descricao = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->tarefa_item__data_conclusao_SEC = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$listAliasTypes->tarefa_item__corporacao_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->tarefa_item__cadastro_SEC = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$listAliasTypes->tarefa_item__criado_pelo_usuario_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->tarefa_item__executada_pelo_usuario_id_INT = static::TIPO_VARIAVEL_INTEGER;
            }        
        }
        
        public static function setListAliasRelatedAttributes()
        {
            if(is_null(static::$listAliasRelatedAttributes))
            {
                static::$listAliasRelatedAttributes = new stdClass();
                static::$listAliasRelatedAttributes->tarefa__id = "tarefaId";
static::$listAliasRelatedAttributes->tarefa_item__id = "id";
static::$listAliasRelatedAttributes->tarefa_item__descricao = "descricao";
static::$listAliasRelatedAttributes->tarefa_item__data_conclusao_SEC = "dataConclusaoSec";
static::$listAliasRelatedAttributes->tarefa_item__corporacao_id_INT = "corporacaoId";
static::$listAliasRelatedAttributes->tarefa_item__cadastro_SEC = "cadastroSec";
static::$listAliasRelatedAttributes->tarefa_item__criado_pelo_usuario_id_INT = "criadoPeloUsuarioId";
static::$listAliasRelatedAttributes->tarefa_item__executada_pelo_usuario_id_INT = "executadaPeloUsuarioId";
            }         
        }

        public function __getList($parameters = null, $idCorporacao = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try
            {
                $filterParameters = new stdClass();
                if(isset($parameters->filterParameters))
                {
                    $filterParameters = $parameters->filterParameters;
                }
                                    
                            
            if(is_null($idCorporacao))
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            ListParameters::addFilterParameterToObject($filterParameters, "corporacaoId", $idCorporacao);           
                
                
                $sortingParameters = null;
                if(isset($parameters->sortingParameters))
                {
                    $sortingParameters = $parameters->sortingParameters;
                }
                
                $paginationParameters = null;
                if(isset($parameters->paginationParameters))
                {
                    $paginationParameters = $parameters->paginationParameters;
                }
                
                $listMappingType = static::getListMappingType($parameters->listMappingType);
                $mainTableAlias = "ti";

                $whereClause = static::getWhereClauseForFilter($filterParameters, $mainTableAlias);
                $orderByClause = static::getOrderByClauseForFilter($sortingParameters, $mainTableAlias);
                $limitClause = static::getLimitClauseForFilter($paginationParameters);

                $queryWithoutLimit = "SELECT ti.id FROM tarefa_item ti {$whereClause}";
                $query = "SELECT t.id AS tarefa__id, ti.id AS tarefa_item__id, ti.descricao AS tarefa_item__descricao, ti.data_conclusao_SEC AS tarefa_item__data_conclusao_SEC, ti.corporacao_id_INT AS tarefa_item__corporacao_id_INT, ti.cadastro_SEC AS tarefa_item__cadastro_SEC, ti.criado_pelo_usuario_id_INT AS tarefa_item__criado_pelo_usuario_id_INT, ti.executada_pelo_usuario_id_INT AS tarefa_item__executada_pelo_usuario_id_INT FROM tarefa_item ti LEFT JOIN tarefa t ON t.id = ti.tarefa_id_INT {$whereClause} {$orderByClause} {$limitClause}";

                $msg = $this->database->queryMensagem($queryWithoutLimit);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                
                $totalNumberOfRecords = $this->database->rows();
                if($totalNumberOfRecords == 0)
                {
                    return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                }                        
                        
                $msg = $this->database->queryMensagem($query);
                
                //operacao realizaca com sucesso
                if ($msg == null) 
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                    }                        
                
                    $resultSet = $this->database->result;

                    $objReturn = new stdClass();
                    $objReturn->dataSet = static::mapResultSetDataToList($resultSet, $listMappingType, static::$listAliasTypes);

                    if(!Helper::isNullOrEmpty($paginationParameters->recordsPerPage) && !Helper::isNullOrEmpty($paginationParameters->currentPage))
                    {
                        $lastPage = ceil($totalNumberOfRecords / $paginationParameters->recordsPerPage);
                        $objReturn->paginationParameters = new PaginationParams($paginationParameters->recordsPerPage, $paginationParameters->currentPage, $lastPage);
                    }
                    
                    return new Mensagem_generica($objReturn);
                } 
                else
                {
                    return $msg;
                }                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }



public function getId()
            {
                return $this->id;
            }

public function getDescricao()
            {
                return $this->descricao;
            }

public function getDataConclusaoSec()
            {
                return $this->dataConclusaoSec;
            }

public function getData_conclusao_SEC()
                {
                    return $this->dataConclusaoSec;
                }

public function getDataConclusaoOffsec()
            {
                return $this->dataConclusaoOffsec;
            }

public function getData_conclusao_OFFSEC()
                {
                    return $this->dataConclusaoOffsec;
                }

public function getTarefaId()
            {
                return $this->tarefaId;
            }

public function getTarefa_id_INT()
                {
                    return $this->tarefaId;
                }

public function getCadastroSec()
            {
                return $this->cadastroSec;
            }

public function getCadastro_SEC()
                {
                    return $this->cadastroSec;
                }

public function getCadastroOffsec()
            {
                return $this->cadastroOffsec;
            }

public function getCadastro_OFFSEC()
                {
                    return $this->cadastroOffsec;
                }

public function getCriadoPeloUsuarioId()
            {
                return $this->criadoPeloUsuarioId;
            }

public function getCriado_pelo_usuario_id_INT()
                {
                    return $this->criadoPeloUsuarioId;
                }

public function getExecutadaPeloUsuarioId()
            {
                return $this->executadaPeloUsuarioId;
            }

public function getExecutada_pelo_usuario_id_INT()
                {
                    return $this->executadaPeloUsuarioId;
                }

public function getCorporacaoId()
            {
                return $this->corporacaoId;
            }

public function getCorporacao_id_INT()
                {
                    return $this->corporacaoId;
                }

public function getSeqInt()
            {
                return $this->seqInt;
            }

public function getSeq_INT()
                {
                    return $this->seqInt;
                }

public function getLatitudeInt()
            {
                return $this->latitudeInt;
            }

public function getLatitude_INT()
                {
                    return $this->latitudeInt;
                }

public function getLongitudeInt()
            {
                return $this->longitudeInt;
            }

public function getLongitude_INT()
                {
                    return $this->longitudeInt;
                }

public function getLatitudeRealInt()
            {
                return $this->latitudeRealInt;
            }

public function getLatitude_real_INT()
                {
                    return $this->latitudeRealInt;
                }

public function getLongitudeRealInt()
            {
                return $this->longitudeRealInt;
            }

public function getLongitude_real_INT()
                {
                    return $this->longitudeRealInt;
                }



function setId($value)
            {
                $this->id = $value;
            }

function setDescricao($value)
            {
                $this->descricao = $value;
            }

function setDataConclusaoSec($value)
            {
                $this->dataConclusaoSec = $value;
            }

public function setData_conclusao_SEC($value)
                { 
                    $this->dataConclusaoSec = $value; 
                }

function setDataConclusaoOffsec($value)
            {
                $this->dataConclusaoOffsec = $value;
            }

public function setData_conclusao_OFFSEC($value)
                { 
                    $this->dataConclusaoOffsec = $value; 
                }

function setTarefaId($value)
            {
                $this->tarefaId = $value;
            }

public function setTarefa_id_INT($value)
                { 
                    $this->tarefaId = $value; 
                }

function setCadastroSec($value)
            {
                $this->cadastroSec = $value;
            }

public function setCadastro_SEC($value)
                { 
                    $this->cadastroSec = $value; 
                }

function setCadastroOffsec($value)
            {
                $this->cadastroOffsec = $value;
            }

public function setCadastro_OFFSEC($value)
                { 
                    $this->cadastroOffsec = $value; 
                }

function setCriadoPeloUsuarioId($value)
            {
                $this->criadoPeloUsuarioId = $value;
            }

public function setCriado_pelo_usuario_id_INT($value)
                { 
                    $this->criadoPeloUsuarioId = $value; 
                }

function setExecutadaPeloUsuarioId($value)
            {
                $this->executadaPeloUsuarioId = $value;
            }

public function setExecutada_pelo_usuario_id_INT($value)
                { 
                    $this->executadaPeloUsuarioId = $value; 
                }

function setCorporacaoId($value)
            {
                $this->corporacaoId = $value;
            }

public function setCorporacao_id_INT($value)
                { 
                    $this->corporacaoId = $value; 
                }

function setSeqInt($value)
            {
                $this->seqInt = $value;
            }

public function setSeq_INT($value)
                { 
                    $this->seqInt = $value; 
                }

function setLatitudeInt($value)
            {
                $this->latitudeInt = $value;
            }

public function setLatitude_INT($value)
                { 
                    $this->latitudeInt = $value; 
                }

function setLongitudeInt($value)
            {
                $this->longitudeInt = $value;
            }

public function setLongitude_INT($value)
                { 
                    $this->longitudeInt = $value; 
                }

function setLatitudeRealInt($value)
            {
                $this->latitudeRealInt = $value;
            }

public function setLatitude_real_INT($value)
                { 
                    $this->latitudeRealInt = $value; 
                }

function setLongitudeRealInt($value)
            {
                $this->longitudeRealInt = $value;
            }

public function setLongitude_real_INT($value)
                { 
                    $this->longitudeRealInt = $value; 
                }



public function clear()
        {$this->id = null;
$this->descricao = null;
$this->dataConclusaoSec = null;
$this->dataConclusaoOffsec = null;
$this->tarefaId = null;
if($this->objTarefa != null) unset($this->objTarefa);
$this->cadastroSec = null;
$this->cadastroOffsec = null;
$this->criadoPeloUsuarioId = null;
if($this->objCriado_pelo_usuario != null) unset($this->objCriado_pelo_usuario);
$this->executadaPeloUsuarioId = null;
if($this->objExecutada_pelo_usuario != null) unset($this->objExecutada_pelo_usuario);
$this->corporacaoId = null;
if($this->objCorporacao != null) unset($this->objCorporacao);
$this->seqInt = null;
$this->latitudeInt = null;
$this->longitudeInt = null;
$this->latitudeRealInt = null;
$this->longitudeRealInt = null;
if(is_array($this->arrEstadosCampos)){
                        unset($this->arrEstadosCampos);
                     $this->arrEstadosCampos = array();
                    }
                }

public function formatarParaSQL()
        {
if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->id)){
$this->id = $this->formatarIntegerParaComandoSQL($this->id);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->id);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->descricao)){
$this->descricao = $this->formatarStringParaComandoSQL($this->descricao);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->descricao);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->dataConclusaoSec)){
$this->dataConclusaoSec = $this->formatarIntegerParaComandoSQL($this->dataConclusaoSec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->dataConclusaoSec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->dataConclusaoOffsec)){
$this->dataConclusaoOffsec = $this->formatarIntegerParaComandoSQL($this->dataConclusaoOffsec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->dataConclusaoOffsec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->tarefaId)){
$this->tarefaId = $this->formatarIntegerParaComandoSQL($this->tarefaId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->tarefaId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->cadastroSec)){
$this->cadastroSec = $this->formatarIntegerParaComandoSQL($this->cadastroSec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->cadastroSec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->cadastroOffsec)){
$this->cadastroOffsec = $this->formatarIntegerParaComandoSQL($this->cadastroOffsec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->cadastroOffsec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->criadoPeloUsuarioId)){
$this->criadoPeloUsuarioId = $this->formatarIntegerParaComandoSQL($this->criadoPeloUsuarioId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->criadoPeloUsuarioId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->executadaPeloUsuarioId)){
$this->executadaPeloUsuarioId = $this->formatarIntegerParaComandoSQL($this->executadaPeloUsuarioId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->executadaPeloUsuarioId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->corporacaoId)){
$this->corporacaoId = $this->formatarIntegerParaComandoSQL($this->corporacaoId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->corporacaoId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->seqInt)){
$this->seqInt = $this->formatarIntegerParaComandoSQL($this->seqInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->seqInt);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->latitudeInt)){
$this->latitudeInt = $this->formatarIntegerParaComandoSQL($this->latitudeInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->latitudeInt);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->longitudeInt)){
$this->longitudeInt = $this->formatarIntegerParaComandoSQL($this->longitudeInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->longitudeInt);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->latitudeRealInt)){
$this->latitudeRealInt = $this->formatarIntegerParaComandoSQL($this->latitudeRealInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->latitudeRealInt);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->longitudeRealInt)){
$this->longitudeRealInt = $this->formatarIntegerParaComandoSQL($this->longitudeRealInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->longitudeRealInt);
}

}

public function formatarParaExibicao()
        {if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->descricao)){
$this->descricao = $this->formatarStringParaExibicao($this->descricao);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->descricao);
}

}

function select($id, $idCorporacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }   

            $sql = "SELECT id, descricao, data_conclusao_SEC, data_conclusao_OFFSEC, tarefa_id_INT, cadastro_SEC, cadastro_OFFSEC, criado_pelo_usuario_id_INT, executada_pelo_usuario_id_INT, corporacao_id_INT, seq_INT, latitude_INT, longitude_INT, latitude_real_INT, longitude_real_INT FROM tarefa_item WHERE id = $id AND corporacao_id_INT = $idCorporacao";

            $msg = $this->database->queryMensagem($sql);
            $result = $this->database->result;

            if($msg != null && $msg->erro() )
            { 
            		$this->clear();
            		return $msg;
        	} 
        	else if($this->database->rows() == 0)
        	{
                return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nenhum registro foi encontrado."));
            }
        

            $row = $this->database->fetchArray(MYSQLI_NUM);
            
            		$this->id = $row[0];
		$this->descricao = $row[1];
		$this->dataConclusaoSec = $row[2];
		$this->dataConclusaoOffsec = $row[3];
		$this->tarefaId = $row[4];
		$this->cadastroSec = $row[5];
		$this->cadastroOffsec = $row[6];
		$this->criadoPeloUsuarioId = $row[7];
		$this->executadaPeloUsuarioId = $row[8];
		$this->seqInt = $row[10];
		$this->latitudeInt = $row[11];
		$this->longitudeInt = $row[12];
		$this->latitudeRealInt = $row[13];
		$this->longitudeRealInt = $row[14];

            
        }

public function delete($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (is_null($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }

            $sql = "DELETE FROM tarefa_item WHERE id= $id AND corporacao_id_INT = $idCorporacao";
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                    && $sincronizar
                        && static::idSistemaTabela() != null
                            && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_REMOVE,
                                null,
                                null,
                                $id,
                                $this->database);
            }

            return $msg;
        }

public function insert($sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null, $opcoes = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $this->defineDataCadastroInSecondsIfNotDefined();
$this->defineDataCadastroOffsetInSecondsIfNotDefined();

            
            if (is_null($opcoes))
            {
                $opcoes = $this->opcoesDAO;
            }

            if (!is_numeric($this->id))
            {
                $this->id = "null";
                if ($opcoes == null || $opcoes[Database::OPCAO_GERAR_ID] != false) 
                {
                    $this->id = EXTDAO_Sistema_sequencia::gerarId(static::nomeTabela());                    
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }    
                    else if($msg == null)
                    {
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
                else 
                {
                    $this->id = null;
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao));
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }
                    else if($msg == null)
                    {
                        $this->id = $this->database->getLastInsertId();
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
            }
            else 
            {
                $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }                        
                else if($msg == null)
                {
                   $msg = new Mensagem_token();
                   $msg->mValor = $this->id;
                }
            }
            if($msg == null || $msg->ok())
            {                
                if($sincronizar
                    && static::idSistemaTabela() != null
                    && isset($this->id)){
                    
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_INSERT,
                                null,
                                null,
                                $this->id,
                                $this->database); 
                	}
            } 
            else
            {
                $this->id = null;
            }
            
            return $msg;
            
        }

public function getSQLInsert($idCorporacao){ 

return "INSERT INTO tarefa_item (id, descricao, data_conclusao_SEC, data_conclusao_OFFSEC, tarefa_id_INT, cadastro_SEC, cadastro_OFFSEC, criado_pelo_usuario_id_INT, executada_pelo_usuario_id_INT, corporacao_id_INT, seq_INT, latitude_INT, longitude_INT, latitude_real_INT, longitude_real_INT) VALUES ( $this->id ,  $this->descricao ,  $this->dataConclusaoSec ,  $this->dataConclusaoOffsec ,  $this->tarefaId ,  $this->cadastroSec ,  $this->cadastroOffsec ,  $this->criadoPeloUsuarioId ,  $this->executadaPeloUsuarioId ,  $idCorporacao ,  $this->seqInt ,  $this->latitudeInt ,  $this->longitudeInt ,  $this->latitudeRealInt ,  $this->longitudeRealInt ) "; 

}




        
        public function updateByParameters($id, $objParametros, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao))
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();

            
                
                if (isset($objParametros->descricao)) 
                {
                    $arrUpdateFields[] = " descricao = {$objParametros->descricao} ";
                }


                
                if (isset($objParametros->dataConclusaoSec)) 
                {
                    $arrUpdateFields[] = " data_conclusao_SEC = {$objParametros->dataConclusaoSec} ";
                }


                
                if (isset($objParametros->dataConclusaoOffsec)) 
                {
                    $arrUpdateFields[] = " data_conclusao_OFFSEC = {$objParametros->dataConclusaoOffsec} ";
                }


                
                if (isset($objParametros->tarefaId)) 
                {
                    $arrUpdateFields[] = " tarefa_id_INT = {$objParametros->tarefaId} ";
                }


                
                if (isset($objParametros->cadastroSec)) 
                {
                    $arrUpdateFields[] = " cadastro_SEC = {$objParametros->cadastroSec} ";
                }


                
                if (isset($objParametros->cadastroOffsec)) 
                {
                    $arrUpdateFields[] = " cadastro_OFFSEC = {$objParametros->cadastroOffsec} ";
                }


                
                if (isset($objParametros->criadoPeloUsuarioId)) 
                {
                    $arrUpdateFields[] = " criado_pelo_usuario_id_INT = {$objParametros->criadoPeloUsuarioId} ";
                }


                
                if (isset($objParametros->executadaPeloUsuarioId)) 
                {
                    $arrUpdateFields[] = " executada_pelo_usuario_id_INT = {$objParametros->executadaPeloUsuarioId} ";
                }


                
                if (isset($objParametros->seqInt)) 
                {
                    $arrUpdateFields[] = " seq_INT = {$objParametros->seqInt} ";
                }


                
                if (isset($objParametros->latitudeInt)) 
                {
                    $arrUpdateFields[] = " latitude_INT = {$objParametros->latitudeInt} ";
                }


                
                if (isset($objParametros->longitudeInt)) 
                {
                    $arrUpdateFields[] = " longitude_INT = {$objParametros->longitudeInt} ";
                }


                
                if (isset($objParametros->latitudeRealInt)) 
                {
                    $arrUpdateFields[] = " latitude_real_INT = {$objParametros->latitudeRealInt} ";
                }


                
                if (isset($objParametros->longitudeRealInt)) 
                {
                    $arrUpdateFields[] = " longitude_real_INT = {$objParametros->longitudeRealInt} ";
                }



            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE tarefa_item SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";

            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null)
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function update($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($this->descricao)) 
                {                                      
                    $arrUpdateFields[] = " descricao = {$this->descricao} ";
                }


                
                if (isset($this->dataConclusaoSec)) 
                {                                      
                    $arrUpdateFields[] = " data_conclusao_SEC = {$this->dataConclusaoSec} ";
                }


                
                if (isset($this->dataConclusaoOffsec)) 
                {                                      
                    $arrUpdateFields[] = " data_conclusao_OFFSEC = {$this->dataConclusaoOffsec} ";
                }


                
                if (isset($this->tarefaId)) 
                {                                      
                    $arrUpdateFields[] = " tarefa_id_INT = {$this->tarefaId} ";
                }


                
                if (isset($this->cadastroSec)) 
                {                                      
                    $arrUpdateFields[] = " cadastro_SEC = {$this->cadastroSec} ";
                }


                
                if (isset($this->cadastroOffsec)) 
                {                                      
                    $arrUpdateFields[] = " cadastro_OFFSEC = {$this->cadastroOffsec} ";
                }


                
                if (isset($this->criadoPeloUsuarioId)) 
                {                                      
                    $arrUpdateFields[] = " criado_pelo_usuario_id_INT = {$this->criadoPeloUsuarioId} ";
                }


                
                if (isset($this->executadaPeloUsuarioId)) 
                {                                      
                    $arrUpdateFields[] = " executada_pelo_usuario_id_INT = {$this->executadaPeloUsuarioId} ";
                }


                
                if (isset($this->seqInt)) 
                {                                      
                    $arrUpdateFields[] = " seq_INT = {$this->seqInt} ";
                }


                
                if (isset($this->latitudeInt)) 
                {                                      
                    $arrUpdateFields[] = " latitude_INT = {$this->latitudeInt} ";
                }


                
                if (isset($this->longitudeInt)) 
                {                                      
                    $arrUpdateFields[] = " longitude_INT = {$this->longitudeInt} ";
                }


                
                if (isset($this->latitudeRealInt)) 
                {                                      
                    $arrUpdateFields[] = " latitude_real_INT = {$this->latitudeRealInt} ";
                }


                
                if (isset($this->longitudeRealInt)) 
                {                                      
                    $arrUpdateFields[] = " longitude_real_INT = {$this->longitudeRealInt} ";
                }


            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE tarefa_item SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function updateByInstanceUsingParameters($id, $parameters, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($parameters->descricao)) 
                {                                      
                    $arrUpdateFields[] = " descricao = {$this->descricao} ";
                }
                
                if (isset($parameters->dataConclusaoSec)) 
                {                                      
                    $arrUpdateFields[] = " data_conclusao_SEC = {$this->dataConclusaoSec} ";
                }
                
                if (isset($parameters->dataConclusaoOffsec)) 
                {                                      
                    $arrUpdateFields[] = " data_conclusao_OFFSEC = {$this->dataConclusaoOffsec} ";
                }
                
                if (isset($parameters->tarefaId)) 
                {                                      
                    $arrUpdateFields[] = " tarefa_id_INT = {$this->tarefaId} ";
                }
                
                if (isset($parameters->cadastroSec)) 
                {                                      
                    $arrUpdateFields[] = " cadastro_SEC = {$this->cadastroSec} ";
                }
                
                if (isset($parameters->cadastroOffsec)) 
                {                                      
                    $arrUpdateFields[] = " cadastro_OFFSEC = {$this->cadastroOffsec} ";
                }
                
                if (isset($parameters->criadoPeloUsuarioId)) 
                {                                      
                    $arrUpdateFields[] = " criado_pelo_usuario_id_INT = {$this->criadoPeloUsuarioId} ";
                }
                
                if (isset($parameters->executadaPeloUsuarioId)) 
                {                                      
                    $arrUpdateFields[] = " executada_pelo_usuario_id_INT = {$this->executadaPeloUsuarioId} ";
                }
                
                if (isset($parameters->seqInt)) 
                {                                      
                    $arrUpdateFields[] = " seq_INT = {$this->seqInt} ";
                }
                
                if (isset($parameters->latitudeInt)) 
                {                                      
                    $arrUpdateFields[] = " latitude_INT = {$this->latitudeInt} ";
                }
                
                if (isset($parameters->longitudeInt)) 
                {                                      
                    $arrUpdateFields[] = " longitude_INT = {$this->longitudeInt} ";
                }
                
                if (isset($parameters->latitudeRealInt)) 
                {                                      
                    $arrUpdateFields[] = " latitude_real_INT = {$this->latitudeRealInt} ";
                }
                
                if (isset($parameters->longitudeRealInt)) 
                {                                      
                    $arrUpdateFields[] = " longitude_real_INT = {$this->longitudeRealInt} ";
                }
            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE tarefa_item SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }

}

?>
