<?php


        /*

        Arquivo gerado através de gerador de código em 12/05/2018 as 14:50:30.
        Para que o arquivo não seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

        Tabela correspondente: registro_estado_corporacao
        Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

        */
        


class DAO_Registro_estado_corporacao extends Generic_DAO
                        {
                            const NOME_ENTIDADE = "registro_estado_corporacao";

                            public static $nomeTabela = null;
                            public static $idSistemaTabela = false;
                            public static $databaseFieldsRelatedAttributes = null;
	                        public static $databaseFieldTypes = null;
	                        public static $databaseFieldNames = null;
	                        public static $listAliasTypes = null;
	                        public static $listAliasRelatedAttributes = null;
	                        
	                        public $id;
public $nome;
public $responsavelUsuarioId;
public $responsavelCategoriaPermissaoId;
public $prazoDiasInt;
public $tipoRegistroId;
public $corporacaoId;

public $labelId;
public $labelNome;
public $labelResponsavelUsuarioId;
public $labelResponsavelCategoriaPermissaoId;
public $labelPrazoDiasInt;
public $labelTipoRegistroId;
public $labelCorporacaoId;

public static $arrPK = array("id");

public function __construct($configDAO = null)
        {
            parent::__construct($configDAO);
            static::__setStaticValues($this->database);           
        }

public static function idSistemaTabela($databaseObject = null){
            if(!(static::$idSistemaTabela))
            {
                static::$idSistemaTabela = EXTDAO_Sistema_tabela::getIdDaSistemaTabela(static::nomeTabela(), $databaseObject);
            }
            return static::$idSistemaTabela;
        }

public function getIdSistemaTabela(){
            return static::idSistemaTabela();
        }

public static function nomeTabela(){
            if(is_null(static::$nomeTabela))
            {
                static::$nomeTabela = "registro_estado_corporacao";
            }
            return static::$nomeTabela;
        }

public function getNomeTabela(){
            return static::nomeTabela();
        }

public static function __setStaticValues($databaseObject = null)
                   {    
                        static::nomeTabela();
                        
                        static::idSistemaTabela($databaseObject);
                   
                        static::setDatabaseFieldNames();
                        static::setDatabaseFieldTypes();
                        static::setDatabaseFieldsRelatedAttributeNames();
                        
                        static::setListAliasTypes();
                        static::setListAliasRelatedAttributes();
                   }

public function getExpressaoLabel()
                   {
                        return "{nome}";
                   }

public static function setDatabaseFieldNames()
        {
            if(is_null(static::$databaseFieldNames))
            {
                static::$databaseFieldNames = new stdClass();
                static::$databaseFieldNames->id = "id";
static::$databaseFieldNames->nome = "nome";
static::$databaseFieldNames->responsavelUsuarioId = "responsavel_usuario_id_INT";
static::$databaseFieldNames->responsavelCategoriaPermissaoId = "responsavel_categoria_permissao_id_INT";
static::$databaseFieldNames->prazoDiasInt = "prazo_dias_INT";
static::$databaseFieldNames->tipoRegistroId = "tipo_registro_id_INT";
static::$databaseFieldNames->corporacaoId = "corporacao_id_INT";

            }
                      
        }

public static function setDatabaseFieldTypes()
        {
            if(is_null(static::$databaseFieldTypes))
            {
                static::$databaseFieldTypes = new stdClass();
                static::$databaseFieldTypes->id = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->nome = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->responsavelUsuarioId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->responsavelCategoriaPermissaoId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->prazoDiasInt = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->tipoRegistroId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->corporacaoId = static::TIPO_VARIAVEL_INTEGER;

            }
            
        }

public static function setDatabaseFieldsRelatedAttributeNames()
        {
            if(is_null(static::$databaseFieldsRelatedAttributes))
            {
                static::$databaseFieldsRelatedAttributes = new stdClass();
                static::$databaseFieldsRelatedAttributes->id = "id";
static::$databaseFieldsRelatedAttributes->nome = "nome";
static::$databaseFieldsRelatedAttributes->responsavel_usuario_id_INT = "responsavelUsuarioId";
static::$databaseFieldsRelatedAttributes->responsavel_categoria_permissao_id_INT = "responsavelCategoriaPermissaoId";
static::$databaseFieldsRelatedAttributes->prazo_dias_INT = "prazoDiasInt";
static::$databaseFieldsRelatedAttributes->tipo_registro_id_INT = "tipoRegistroId";
static::$databaseFieldsRelatedAttributes->corporacao_id_INT = "corporacaoId";

            }           
        }

public function setAllFieldsToHumanFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["nome"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["responsavel_usuario_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["responsavel_categoria_permissao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["prazo_dias_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["tipo_registro_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
}

public function setAllFieldsToDatabaseFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["nome"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["responsavel_usuario_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["responsavel_categoria_permissao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["prazo_dias_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["tipo_registro_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
}

  public function getFkObjResponsavel_usuario() 
                {
                    if($this->objResponsavel_usuario == null)
                    {                        
                        $this->objResponsavel_usuario = new EXTDAO_Responsavel_usuario_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getResponsavel_usuario_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objResponsavel_usuario->clear();
                    }
                    elseif($this->objResponsavel_usuario->getId() != $idFK)
                    {
                        $this->objResponsavel_usuario->select($idFK);
                    }
                    return $this->objResponsavel_usuario;
                }
  public function getFkObjResponsavel_categoria_permissao() 
                {
                    if($this->objResponsavel_categoria_permissao == null)
                    {                        
                        $this->objResponsavel_categoria_permissao = new EXTDAO_Responsavel_categoria_permissao_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getResponsavel_categoria_permissao_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objResponsavel_categoria_permissao->clear();
                    }
                    elseif($this->objResponsavel_categoria_permissao->getId() != $idFK)
                    {
                        $this->objResponsavel_categoria_permissao->select($idFK);
                    }
                    return $this->objResponsavel_categoria_permissao;
                }
  public function getFkObjTipo_registro() 
                {
                    if($this->objTipo_registro == null)
                    {                        
                        $this->objTipo_registro = new EXTDAO_Tipo_registro_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getTipo_registro_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objTipo_registro->clear();
                    }
                    elseif($this->objTipo_registro->getId() != $idFK)
                    {
                        $this->objTipo_registro->select($idFK);
                    }
                    return $this->objTipo_registro;
                }
  public function getFkObjCorporacao() 
                {
                    if($this->objCorporacao == null)
                    {                        
                        $this->objCorporacao = new EXTDAO_Corporacao_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getCorporacao_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objCorporacao->clear();
                    }
                    elseif($this->objCorporacao->getId() != $idFK)
                    {
                        $this->objCorporacao->select($idFK);
                    }
                    return $this->objCorporacao;
                }


public function setLabels()
        {$this->labelId = "";
$this->labelNome = "";
$this->labelResponsavelUsuarioId = "";
$this->labelResponsavelCategoriaPermissaoId = "";
$this->labelPrazoDiasInt = "";
$this->labelTipoRegistroId = "";
$this->labelCorporacaoId = "";
}

public function __actionAdd($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                $this->setByObject($parameters);
                $this->setDataCadastroAndOffsetInSecondsFromUser($parameters);
                
                $this->formatarParaSQL();
                $msg = $this->insert(true);
                
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                return new Mensagem(null, I18N::getExpression("Registro estado corporacao adicionado com sucesso."));
                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionEdit($parameters = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try {

                $this->setByObject($parameters);
                $this->setAllFieldsToHumanFormat();
                
                $this->formatarParaSQL();
                $msg = $this->updateByInstanceUsingParameters($this->id, $parameters, true);
                if($msg != null && $msg->erro())
                {
                     return $msg;
                }
        
                return new Mensagem(null, I18N::getExpression("Registro estado corporacao editado com sucesso."));
            }
            catch(Exception $ex)
            {                
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionRemove($parameters = null)
        {
            $successulRemovalCount = 0;
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                foreach($parameters as $record)
                {
                    if(is_numeric($record->id))
                    {
                        $msg = $this->delete($record->id, true);
                        if($msg == null || $msg->ok())
                        {
                            $successulRemovalCount++;
                        }
                    }
                }
                
                if($successulRemovalCount > 0)
                {
                    $successMessage = count($successulRemovalCount) > 1 ? I18N::getExpression("{0} Registro estado corporacao foram removidos com sucesso.", $successulRemovalCount) : I18N::getExpression("Registro estado corporacao removido com sucesso.");
                    return new Mensagem(null, $successMessage);
                }
                else
                {
                    $errorMessage = count($parameters) > 1 ? I18N::getExpression("Falha ao remover Registro estado corporacao.") : I18N::getExpression("Falha ao remover Registro estado corporacao.");
                    return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, $errorMessage);                    
                }
            }
            catch (Exception $ex) 
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }

public function __getRecord($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }
            
            try
            {            
                $returnObject = new stdClass();            
                $whereClause = static::getWhereClauseForFilter($parameters->filterParameters);
                $query = "SELECT  id, nome, responsavel_usuario_id_INT, responsavel_categoria_permissao_id_INT, prazo_dias_INT, tipo_registro_id_INT, corporacao_id_INT FROM registro_estado_corporacao {$whereClause}";
    
                $msg = $this->database->queryMensagem($query);                
                if ($msg == null)
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Não foi encontrado nenhum registro"));
                    }                    
                    $resultSet = $this->database->result;
                    $resultObject = static::mapResultSetDataToRecord($resultSet);
                    
                    $returnObject->formData = $resultObject;
                    $returnObject->comboBoxesData = null;
                    if($parameters->loadComboBoxesData)
                    {
                        $this->setByObject($resultObject);
                        $returnObject->comboBoxesData =  $this->getFormComboBoxesData();
                    }                    
    
                    
    
                    return new Mensagem_generica($returnObject);
                } 
                else 
                {
                    return $msg;
                }
                
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }


        
        public function __getFilterComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
                
        public function __getFormComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
        
        protected function getFormComboBoxesData()
        {
            $comboBoxesData = new stdClass();
            $listParameters = new stdClass();
            $listParameters->listMappingType = static::LIST_MAPPING_TYPE_COMBOBOX;
            
            
                    
                    $objTipoRegistro = new EXTDAO_Tipo_registro();
                    $comboBoxesData->fieldTipoRegistroId = $objTipoRegistro->__getList($listParameters);

            return $comboBoxesData;
        }
        
        


        
        public static function setListAliasTypes()
        {
            if(is_null(static::$listAliasTypes))
            {
                static::$listAliasTypes = new stdClass();
                static::$listAliasTypes->tipo_registro__nome = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->registro_estado_corporacao__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->registro_estado_corporacao__nome = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->registro_estado_corporacao__responsavel_usuario_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->registro_estado_corporacao__responsavel_categoria_permissao_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->registro_estado_corporacao__prazo_dias_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->registro_estado_corporacao__nome = static::TIPO_VARIAVEL_TEXT;
            }        
        }
        
        public static function setListAliasRelatedAttributes()
        {
            if(is_null(static::$listAliasRelatedAttributes))
            {
                static::$listAliasRelatedAttributes = new stdClass();
                static::$listAliasRelatedAttributes->tipo_registro__nome = "tipoRegistroNome";
static::$listAliasRelatedAttributes->registro_estado_corporacao__id = "id";
static::$listAliasRelatedAttributes->registro_estado_corporacao__nome = "nome";
static::$listAliasRelatedAttributes->registro_estado_corporacao__responsavel_usuario_id_INT = "responsavelUsuarioId";
static::$listAliasRelatedAttributes->registro_estado_corporacao__responsavel_categoria_permissao_id_INT = "responsavelCategoriaPermissaoId";
static::$listAliasRelatedAttributes->registro_estado_corporacao__prazo_dias_INT = "prazoDiasInt";
static::$listAliasRelatedAttributes->registro_estado_corporacao__nome = "nome";
            }         
        }

        public function __getList($parameters = null, $idCorporacao = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try
            {
                $filterParameters = new stdClass();
                if(isset($parameters->filterParameters))
                {
                    $filterParameters = $parameters->filterParameters;
                }
                                    
                            
            if(is_null($idCorporacao))
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            ListParameters::addFilterParameterToObject($filterParameters, "corporacaoId", $idCorporacao);           
                
                
                $sortingParameters = null;
                if(isset($parameters->sortingParameters))
                {
                    $sortingParameters = $parameters->sortingParameters;
                }
                
                $paginationParameters = null;
                if(isset($parameters->paginationParameters))
                {
                    $paginationParameters = $parameters->paginationParameters;
                }
                
                $listMappingType = static::getListMappingType($parameters->listMappingType);
                $mainTableAlias = "rec";

                $whereClause = static::getWhereClauseForFilter($filterParameters, $mainTableAlias);
                $orderByClause = static::getOrderByClauseForFilter($sortingParameters, $mainTableAlias);
                $limitClause = static::getLimitClauseForFilter($paginationParameters);

                $queryWithoutLimit = "SELECT rec.id FROM registro_estado_corporacao rec {$whereClause}";
                $query = "SELECT tr.nome AS tipo_registro__nome, rec.id AS registro_estado_corporacao__id, rec.nome AS registro_estado_corporacao__nome, rec.responsavel_usuario_id_INT AS registro_estado_corporacao__responsavel_usuario_id_INT, rec.responsavel_categoria_permissao_id_INT AS registro_estado_corporacao__responsavel_categoria_permissao_id_INT, rec.prazo_dias_INT AS registro_estado_corporacao__prazo_dias_INT, rec.nome AS registro_estado_corporacao__nome FROM registro_estado_corporacao rec LEFT JOIN tipo_registro tr ON tr.id = rec.tipo_registro_id_INT {$whereClause} {$orderByClause} {$limitClause}";

                $msg = $this->database->queryMensagem($queryWithoutLimit);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                
                $totalNumberOfRecords = $this->database->rows();
                if($totalNumberOfRecords == 0)
                {
                    return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                }                        
                        
                $msg = $this->database->queryMensagem($query);
                
                //operacao realizaca com sucesso
                if ($msg == null) 
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                    }                        
                
                    $resultSet = $this->database->result;

                    $objReturn = new stdClass();
                    $objReturn->dataSet = static::mapResultSetDataToList($resultSet, $listMappingType, static::$listAliasTypes);

                    if(!Helper::isNullOrEmpty($paginationParameters->recordsPerPage) && !Helper::isNullOrEmpty($paginationParameters->currentPage))
                    {
                        $lastPage = ceil($totalNumberOfRecords / $paginationParameters->recordsPerPage);
                        $objReturn->paginationParameters = new PaginationParams($paginationParameters->recordsPerPage, $paginationParameters->currentPage, $lastPage);
                    }
                    
                    return new Mensagem_generica($objReturn);
                } 
                else
                {
                    return $msg;
                }                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }



public function getId()
            {
                return $this->id;
            }

public function getNome()
            {
                return $this->nome;
            }

public function getResponsavelUsuarioId()
            {
                return $this->responsavelUsuarioId;
            }

public function getResponsavel_usuario_id_INT()
                {
                    return $this->responsavelUsuarioId;
                }

public function getResponsavelCategoriaPermissaoId()
            {
                return $this->responsavelCategoriaPermissaoId;
            }

public function getResponsavel_categoria_permissao_id_INT()
                {
                    return $this->responsavelCategoriaPermissaoId;
                }

public function getPrazoDiasInt()
            {
                return $this->prazoDiasInt;
            }

public function getPrazo_dias_INT()
                {
                    return $this->prazoDiasInt;
                }

public function getTipoRegistroId()
            {
                return $this->tipoRegistroId;
            }

public function getTipo_registro_id_INT()
                {
                    return $this->tipoRegistroId;
                }

public function getCorporacaoId()
            {
                return $this->corporacaoId;
            }

public function getCorporacao_id_INT()
                {
                    return $this->corporacaoId;
                }



function setId($value)
            {
                $this->id = $value;
            }

function setNome($value)
            {
                $this->nome = $value;
            }

function setResponsavelUsuarioId($value)
            {
                $this->responsavelUsuarioId = $value;
            }

public function setResponsavel_usuario_id_INT($value)
                { 
                    $this->responsavelUsuarioId = $value; 
                }

function setResponsavelCategoriaPermissaoId($value)
            {
                $this->responsavelCategoriaPermissaoId = $value;
            }

public function setResponsavel_categoria_permissao_id_INT($value)
                { 
                    $this->responsavelCategoriaPermissaoId = $value; 
                }

function setPrazoDiasInt($value)
            {
                $this->prazoDiasInt = $value;
            }

public function setPrazo_dias_INT($value)
                { 
                    $this->prazoDiasInt = $value; 
                }

function setTipoRegistroId($value)
            {
                $this->tipoRegistroId = $value;
            }

public function setTipo_registro_id_INT($value)
                { 
                    $this->tipoRegistroId = $value; 
                }

function setCorporacaoId($value)
            {
                $this->corporacaoId = $value;
            }

public function setCorporacao_id_INT($value)
                { 
                    $this->corporacaoId = $value; 
                }



public function clear()
        {$this->id = null;
$this->nome = null;
$this->responsavelUsuarioId = null;
if($this->objResponsavel_usuario != null) unset($this->objResponsavel_usuario);
$this->responsavelCategoriaPermissaoId = null;
if($this->objResponsavel_categoria_permissao != null) unset($this->objResponsavel_categoria_permissao);
$this->prazoDiasInt = null;
$this->tipoRegistroId = null;
if($this->objTipo_registro != null) unset($this->objTipo_registro);
$this->corporacaoId = null;
if($this->objCorporacao != null) unset($this->objCorporacao);
if(is_array($this->arrEstadosCampos)){
                        unset($this->arrEstadosCampos);
                     $this->arrEstadosCampos = array();
                    }
                }

public function formatarParaSQL()
        {
if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->id)){
$this->id = $this->formatarIntegerParaComandoSQL($this->id);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->id);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->nome)){
$this->nome = $this->formatarStringParaComandoSQL($this->nome);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->nome);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->responsavelUsuarioId)){
$this->responsavelUsuarioId = $this->formatarIntegerParaComandoSQL($this->responsavelUsuarioId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->responsavelUsuarioId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->responsavelCategoriaPermissaoId)){
$this->responsavelCategoriaPermissaoId = $this->formatarIntegerParaComandoSQL($this->responsavelCategoriaPermissaoId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->responsavelCategoriaPermissaoId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->prazoDiasInt)){
$this->prazoDiasInt = $this->formatarIntegerParaComandoSQL($this->prazoDiasInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->prazoDiasInt);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->tipoRegistroId)){
$this->tipoRegistroId = $this->formatarIntegerParaComandoSQL($this->tipoRegistroId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->tipoRegistroId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->corporacaoId)){
$this->corporacaoId = $this->formatarIntegerParaComandoSQL($this->corporacaoId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->corporacaoId);
}

}

public function formatarParaExibicao()
        {if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->nome)){
$this->nome = $this->formatarStringParaExibicao($this->nome);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->nome);
}

}

function select($id, $idCorporacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }   

            $sql = "SELECT id, nome, responsavel_usuario_id_INT, responsavel_categoria_permissao_id_INT, prazo_dias_INT, tipo_registro_id_INT, corporacao_id_INT FROM registro_estado_corporacao WHERE id = $id AND corporacao_id_INT = $idCorporacao";

            $msg = $this->database->queryMensagem($sql);
            $result = $this->database->result;

            if($msg != null && $msg->erro() )
            { 
            		$this->clear();
            		return $msg;
        	} 
        	else if($this->database->rows() == 0)
        	{
                return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nenhum registro foi encontrado."));
            }
        

            $row = $this->database->fetchArray(MYSQLI_NUM);
            
            		$this->id = $row[0];
		$this->nome = $row[1];
		$this->responsavelUsuarioId = $row[2];
		$this->responsavelCategoriaPermissaoId = $row[3];
		$this->prazoDiasInt = $row[4];
		$this->tipoRegistroId = $row[5];

            
        }

public function delete($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (is_null($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }

            $sql = "DELETE FROM registro_estado_corporacao WHERE id= $id AND corporacao_id_INT = $idCorporacao";
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                    && $sincronizar
                        && static::idSistemaTabela() != null
                            && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_REMOVE,
                                null,
                                null,
                                $id,
                                $this->database);
            }

            return $msg;
        }

public function insert($sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null, $opcoes = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            
            
            if (is_null($opcoes))
            {
                $opcoes = $this->opcoesDAO;
            }

            if (!is_numeric($this->id))
            {
                $this->id = "null";
                if ($opcoes == null || $opcoes[Database::OPCAO_GERAR_ID] != false) 
                {
                    $this->id = EXTDAO_Sistema_sequencia::gerarId(static::nomeTabela());                    
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }    
                    else if($msg == null)
                    {
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
                else 
                {
                    $this->id = null;
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao));
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }
                    else if($msg == null)
                    {
                        $this->id = $this->database->getLastInsertId();
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
            }
            else 
            {
                $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }                        
                else if($msg == null)
                {
                   $msg = new Mensagem_token();
                   $msg->mValor = $this->id;
                }
            }
            if($msg == null || $msg->ok())
            {                
                if($sincronizar
                    && static::idSistemaTabela() != null
                    && isset($this->id)){
                    
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_INSERT,
                                null,
                                null,
                                $this->id,
                                $this->database); 
                	}
            } 
            else
            {
                $this->id = null;
            }
            
            return $msg;
            
        }

public function getSQLInsert($idCorporacao){ 

return "INSERT INTO registro_estado_corporacao (id, nome, responsavel_usuario_id_INT, responsavel_categoria_permissao_id_INT, prazo_dias_INT, tipo_registro_id_INT, corporacao_id_INT) VALUES ( $this->id ,  $this->nome ,  $this->responsavelUsuarioId ,  $this->responsavelCategoriaPermissaoId ,  $this->prazoDiasInt ,  $this->tipoRegistroId ,  $idCorporacao ) "; 

}




        
        public function updateByParameters($id, $objParametros, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao))
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();

            
                
                if (isset($objParametros->nome)) 
                {
                    $arrUpdateFields[] = " nome = {$objParametros->nome} ";
                }


                
                if (isset($objParametros->responsavelUsuarioId)) 
                {
                    $arrUpdateFields[] = " responsavel_usuario_id_INT = {$objParametros->responsavelUsuarioId} ";
                }


                
                if (isset($objParametros->responsavelCategoriaPermissaoId)) 
                {
                    $arrUpdateFields[] = " responsavel_categoria_permissao_id_INT = {$objParametros->responsavelCategoriaPermissaoId} ";
                }


                
                if (isset($objParametros->prazoDiasInt)) 
                {
                    $arrUpdateFields[] = " prazo_dias_INT = {$objParametros->prazoDiasInt} ";
                }


                
                if (isset($objParametros->tipoRegistroId)) 
                {
                    $arrUpdateFields[] = " tipo_registro_id_INT = {$objParametros->tipoRegistroId} ";
                }



            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE registro_estado_corporacao SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";

            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null)
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function update($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($this->nome)) 
                {                                      
                    $arrUpdateFields[] = " nome = {$this->nome} ";
                }


                
                if (isset($this->responsavelUsuarioId)) 
                {                                      
                    $arrUpdateFields[] = " responsavel_usuario_id_INT = {$this->responsavelUsuarioId} ";
                }


                
                if (isset($this->responsavelCategoriaPermissaoId)) 
                {                                      
                    $arrUpdateFields[] = " responsavel_categoria_permissao_id_INT = {$this->responsavelCategoriaPermissaoId} ";
                }


                
                if (isset($this->prazoDiasInt)) 
                {                                      
                    $arrUpdateFields[] = " prazo_dias_INT = {$this->prazoDiasInt} ";
                }


                
                if (isset($this->tipoRegistroId)) 
                {                                      
                    $arrUpdateFields[] = " tipo_registro_id_INT = {$this->tipoRegistroId} ";
                }


            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE registro_estado_corporacao SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function updateByInstanceUsingParameters($id, $parameters, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($parameters->nome)) 
                {                                      
                    $arrUpdateFields[] = " nome = {$this->nome} ";
                }
                
                if (isset($parameters->responsavelUsuarioId)) 
                {                                      
                    $arrUpdateFields[] = " responsavel_usuario_id_INT = {$this->responsavelUsuarioId} ";
                }
                
                if (isset($parameters->responsavelCategoriaPermissaoId)) 
                {                                      
                    $arrUpdateFields[] = " responsavel_categoria_permissao_id_INT = {$this->responsavelCategoriaPermissaoId} ";
                }
                
                if (isset($parameters->prazoDiasInt)) 
                {                                      
                    $arrUpdateFields[] = " prazo_dias_INT = {$this->prazoDiasInt} ";
                }
                
                if (isset($parameters->tipoRegistroId)) 
                {                                      
                    $arrUpdateFields[] = " tipo_registro_id_INT = {$this->tipoRegistroId} ";
                }
            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE registro_estado_corporacao SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }

}

?>
