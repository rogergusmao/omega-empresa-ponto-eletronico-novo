import { RemoteModel } from 'lib/model';
import { PontoEletronico } from 'aplicacao/modulo/calendario/remoteModel';

export class DadosIniciais extends RemoteModel
{
    public pontos :Array<PontoEletronico>;
}