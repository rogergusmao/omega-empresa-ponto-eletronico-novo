"use strict";
exports.__esModule = true;
var Cache_Config = (function () {
    function Cache_Config(prefix) {
        if (prefix === void 0) { prefix = Cache_Config.DEFAULT_PREFIX; }
        this._prefix = prefix;
    }
    Object.defineProperty(Cache_Config.prototype, "prefix", {
        get: function () {
            return this._prefix;
        },
        set: function (prefix) {
            this._prefix = prefix;
        },
        enumerable: true,
        configurable: true
    });
    return Cache_Config;
}());
Cache_Config.DEFAULT_PREFIX = "";
exports.Cache_Config = Cache_Config;
