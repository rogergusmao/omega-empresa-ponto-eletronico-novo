"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var contexto_1 = require("lib/contexto");
var uiTemplate_1 = require("lib/uiTemplate");
var uiComponente_1 = require("lib/uiComponente");
var ContextoMapa = (function (_super) {
    __extends(ContextoMapa, _super);
    function ContextoMapa(domRootSelector, mapContainerSelector) {
        var _this = _super.call(this, domRootSelector) || this;
        _this.mapContainerSelector = mapContainerSelector;
        return _this;
    }
    ContextoMapa.prototype.initializeMap = function () {
        this.mapObject = new uiComponente_1.Mapa(this);
        this.mapObject.setTemplate(new uiTemplate_1.Default());
        this.mapObject.setDefaultValues();
        this.mapObject.render();
        this.mapObject.addResizeListener();
    };
    return ContextoMapa;
}(contexto_1.ContextoGenerico));
exports.ContextoMapa = ContextoMapa;
