"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var Mapper_1 = require("../../../../mapper/Mapper");
var Cache_Backend_LocalStorage_Tag = (function (_super) {
    __extends(Cache_Backend_LocalStorage_Tag, _super);
    function Cache_Backend_LocalStorage_Tag(tag, keys) {
        if (keys === void 0) { keys = []; }
        var _this = _super.call(this) || this;
        _this._tag = tag;
        _this._keys = keys;
        return _this;
    }
    Cache_Backend_LocalStorage_Tag.prototype.add = function (key) {
        if (this.keyExists(key)) {
            return false;
        }
        // Add the key to the list of keys
        this._keys.push(key);
        return true;
    };
    Cache_Backend_LocalStorage_Tag.prototype.remove = function (key) {
        // Check if the given key exists
        if (!this.keyExists(key)) {
            return false;
        }
        // Get the index of the keys
        var index = this._keys.indexOf(key, 0);
        // Remove the key from the list of keys
        this._keys.splice(index, 1);
        return true;
    };
    Object.defineProperty(Cache_Backend_LocalStorage_Tag.prototype, "tag", {
        get: function () {
            return this._tag;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Cache_Backend_LocalStorage_Tag.prototype, "keys", {
        get: function () {
            return this._keys;
        },
        enumerable: true,
        configurable: true
    });
    Cache_Backend_LocalStorage_Tag.prototype.keyExists = function (key) {
        return (this._keys.indexOf(key, 0) != -1);
    };
    Cache_Backend_LocalStorage_Tag.prototype.tagEmpty = function () {
        return (this._keys.length <= 0);
    };
    Cache_Backend_LocalStorage_Tag.fromObject = function (object) {
        _super.fromObject.call(this, object);
        return new Cache_Backend_LocalStorage_Tag(object._tag, object._keys);
    };
    return Cache_Backend_LocalStorage_Tag;
}(Mapper_1.Mapper));
exports.Cache_Backend_LocalStorage_Tag = Cache_Backend_LocalStorage_Tag;
