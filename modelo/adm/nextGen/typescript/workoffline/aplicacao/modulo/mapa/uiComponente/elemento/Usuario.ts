import { Marcador } from "lib/uiComponente";
import { Usuario as ModelUsuario } from 'aplicacao/modulo/mapa/model'
import { ContextoMapa } from "lib/contexto";
import { ModalFactory } from "aplicacao/modulo/mapa/uiFactory";
import { Sexo } from "aplicacao/comum/geral";

export class Usuario extends Marcador
{
    public modelUsuario :ModelUsuario;
    public constructor(contexto :ContextoMapa, modelUsuario: ModelUsuario)
    {
        super(contexto, modelUsuario.latitude, modelUsuario.longitude, 'usuario');
        this.modelUsuario = modelUsuario;
        this.setDefaultLabelObject(modelUsuario.nomeUsuario);
    }

    public setDefaultIconeObject()
    {
        switch (this.modelUsuario.sexoUsuario)
        {
            case Sexo.MASCULINO:
            case Sexo.NAO_DEFINIDO:
                super.setDefaultIconeObject(this.contexto.getDefaultMarkerFolder()  + "map_male.png");
                break;

            case Sexo.FEMININO:
                super.setDefaultIconeObject(this.contexto.getDefaultMarkerFolder() + "map_female.png");
                break;
        }
    }

    public addDefaultClickListener()
    {
        super.addClickListener(() =>
        {
            var janelaModal = ModalFactory.getModalDetalhesUsuario(this.contexto, this.modelUsuario);
            janelaModal.handleShow();                
        });
    }

}