<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Servicos_web
 *
 * @author home
 */
class Servicos_web_ponto_eletronico
{
    //Como chamar algum webservice da classe:
    //http://127.0.0.1/hospedagem/10001/adm/actions.php?class=Servicos_web&action=

    public function __construct()
    {
    }

    public static function cadastrarUsuarioPontoEletronico()
    {
        $emailResponsavel = Helper::POSTGET('email_responsavel');
        $senhaResponsavel = Helper::POSTGET('senha_responsavel');
        $nome = Helper::POSTGET('nome');
        $email = Helper::POSTGET('email');
        $corporacao = Helper::POSTGET('corporacao');
        $senha = Helper::POSTGET('senha');
        $idPrograma = Helper::POSTGET('idPrograma');
        $imei = Helper::POSTGET('imei');

        $mensagem = BO_Usuario::cadastrarUsuario(
            $emailResponsavel,
            $senhaResponsavel,
            $nome,
            $email,
            $corporacao,
            $senha,
            $idPrograma,
            $imei);
        return $mensagem;
    }

    public static function editaClientePeloSICOB()
    {
        $campo = Helper::POSTGET("campo");

        $objCrypt = new Crypt();
        $arrChavesValores = $objCrypt->decryptGet($campo);

        $mensagem = BO_Usuario::editaUsuarioPeloSICOB($arrChavesValores["email"], $arrChavesValores["senha"], $arrChavesValores["corporacao"], $arrChavesValores["nome"]);
        return $mensagem;
    }

    public static function cadastraUsuarioECorporacaoPontoEletronico()
    {
//            http://127.0.0.1/PontoEletronico/10001/public_html/adm/actions.php?class=Servicos_web_sihop&action=cadastraUsuarioECorporacao&email=teste%40teste.com.br&corporacao=teste&senha=123456&nome=teste&
        $email = Helper::POSTGET("email", true);
        $corporacao = Helper::POSTGET("corporacao", true);
        $senha = Helper::POSTGET("senha", true);
        $nome = Helper::POSTGET("nome", true);
        $idPrograma = Helper::POSTGET("id_programa", true);
        $idCorporacao = Helper::POSTGET("id_corporacao", true);
        $imei = Helper::POSTGET("imei", true);

        $mensagem = BO_Usuario::cadastraUsuarioECorporacao(
            $nome,
            $email,
            $corporacao,
            $senha,
            $idPrograma,
            $imei,
            $idCorporacao);
        return $mensagem;
    }

    public static function loginMobile()
    {
        $corporacao = Helper::POSTGET("corporacao");
        $email = Helper::POSTGET("email");
        $senha = Helper::POSTGET("senha");
        $carregarInformacoesUsuario = Helper::POSTGET("carregarInformacoesUsuario") == "true" ? true : false;
        $carregarPermissoes = Helper::POSTGET("carregarPermissao") == "true" ? true : false;

        return BO_Usuario::loginEGetInformacoesDoUsuario(
            $email,
            $corporacao,
            $senha,
            $carregarPermissoes,
            $carregarInformacoesUsuario);
    }

    public static function factory()
    {
        return new Servicos_web_ponto_eletronico();
    }

    public static function isCorporacaoExistente()
    {
        $corporacao = Helper::POSTGET("corporacao");

        if (!empty($corporacao))
        {
            $vIdCorporacao = EXTDAO_Corporacao::getIdDaCorporacao($corporacao);
            //cadastrando usuario ainda nao existente
            if ($vIdCorporacao == null)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO);
            }
            else
            {
                return new Mensagem(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO);
            }
        }
        else
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_PARAMETRO_INVALIDO);
        }
    }

    //Chamado pelo App Ponto Eletronico
    public function alterarSenhaApp()
    {
        $novaSenha = Helper::POSTGET("nova_senha");
        $email = Helper::POSTGET("usuario");
        $idCorporacao = Helper::POSTGET("id_corporacao");

        if (strlen($email) && strlen($novaSenha) && strlen($idCorporacao))
        {
            $boUsuario = new BO_Usuario();
            return $boUsuario->alterarSenhaPeloSicobPorEmail($email, $idCorporacao, Helper::POSTGET("senha"), $novaSenha);
        }
        else
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_PARAMETRO_INVALIDO);
        }
    }

    private static function checkUsuarioCorporacao()
    {
        $idCorporacao = Helper::POSTGET("id_corporacao");
        $idUsuario = Helper::POSTGET("usuario");
        $senha = Helper::POSTGET("senha");
        $vObjSeguranca = new Seguranca();
        $msg = $vObjSeguranca->verificaSenha(
            array("id" => $idUsuario, "id_corporacao" => $idCorporacao),
            $senha,
            null,
            false);
        if ($msg != null && !$msg->ok())
        {
            return Mensagem::factoryAcessoNegado();
        }
        else
        {
            return null;
        }
    }

    public static function isAdministradorDaCorporacao()
    {
        $msg = self::checkUsuarioCorporacao();
        if ($msg != null)
        {
            return $msg;
        }
        //nome do usuario
        $vIdUsuario = Helper::POSTGET("usuario");
        $vIdCorporacao = Helper::POSTGET("id_corporacao");

        if (strlen($vIdUsuario) && strlen($vIdCorporacao) > 0)
        {
            $msg = BO_Usuario::isUsuarioAdministradorDaCorporacao($vIdUsuario, $vIdCorporacao);
            return $msg;
        }
        else
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_PARAMETRO_INVALIDO);
        }
    }

    public static function removerTodasAsPermissoesDaCategoriaDePermissao()
    {
        $msg = self::checkUsuarioCorporacao();
        if ($msg != null)
        {
            return $msg;
        }

        $idCategoriaPermissao = Helper::POSTGET("id_categoria_permissao");
        if (strlen($idCategoriaPermissao) > 0)
        {
            $db = new Database();
            $query = "SELECT DISTINCT pcp.id
                            FROM permissao p, permissao_categoria_permissao pcp
                            WHERE pcp.permissao_id_INT = p.id AND
                            pcp.categoria_permissao_id_INT = {$idCategoriaPermissao}";

            $db->queryMensagem($query);
            $vVetorIdPermissao = Helper::getResultSetToArrayDeUmCampo($db->result);

            $vObjPCP = new EXTDAO_Permissao_categoria_permissao($db);
            foreach ($vVetorIdPermissao as $key => $vIdPermissao)
            {
                $vObjPCP->delete($vIdPermissao);
            }
            return new Mensagem(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO,
                                I18N::getExpression("Todas as permiss�es foram removidas"));
        }
        else
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_PARAMETRO_INVALIDO);
        }
    }

//    public static function receiveCadastroUsuarioECorporacao()
//    {
//        //nome do usuario
//        $nome = Helper::POSTGET("p_nome");
//        $nomeSemAcento = Helper::POSTGET("p_nome_sem_acento");
//        $email = Helper::POSTGET("p_email");
//        //nome da corporacao ainda nao cadastrada
//        $corporacao = Helper::POSTGET("p_corporacao");
//        $corporacaoSemAcento = Helper::POSTGET("p_corporacao_sem_acento");
//        $senha = Helper::POSTGET("p_senha");
//        $pIMEI = Helper::POSTGET("imei");
//        $idPrograma = Helper::POSTGET("id_programa");
//
//        if (strlen($nome) > 0 &&
//            strlen($email) > 0 &&
//            strlen($corporacao) > 0 &&
//            strlen($senha) > 0 &&
//            strlen($corporacaoSemAcento) &&
//            strlen($nomeSemAcento)
//        ) {
//            $vIdUsuarioSistemaTabela = DatabaseSincronizador::getIdDaSistemaTabela("usuario");
//            $vIdUsuarioCorporacaoSistemaTabela = DatabaseSincronizador::getIdDaSistemaTabela("usuario_corporacao");
//            $vIdUsuario = DatabaseSincronizador::getIdDoUsuario($email);
//            if ($vIdUsuario != null) {
//                $vObjSeguranca = new Seguranca();
//                if (!$vObjSeguranca->verificaUsuarioESenha($vIdUsuario, $senha)) {
//                    echo "ERROR: senha invalida do usuario ja existente";
//                    return;
//                }
//            }
//
//            $vIdCorporacao = null;
//            if (DatabaseSincronizador::checkExistenciaCorporacao($corporacao)) {
//                echo "ERROR: corporacao duplicado";
//                return;
//            }
//            //cadastrando usuario ainda nao existente
//            if ($vIdUsuario == null) {
//                $vUsuario = new EXTDAO_Usuario();
//                $vUsuario->setEmail($email);
//
//                $vUsuario->setNome($nome);
//                $vUsuario->setNome_normalizado($nomeSemAcento);
//                $objCriptografia = new Crypt();
//                $vUsuario->setSenha($objCriptografia->crypt($senha));
//                $vUsuario->setStatus_BOOLEAN("1");
//                $vUsuario->formatarParaSQL();
//                $vUsuario->insert();
//                $vIdUsuario = $vUsuario->getIdDoUltimoRegistroInserido();
//
//                if (!strlen($vIdUsuario)) {
//                    echo "FALSE 0";
//                    return;
//                }
//            }
//
//            $vCorporacao = new EXTDAO_Corporacao();
//            $vCorporacao->setNome($corporacao);
//            $vCorporacao->setNome_normalizado($corporacaoSemAcento);
//            $vCorporacao->formatarParaSQL();
//            $vCorporacao->insert();
//            $vIdCorporacao = $vCorporacao->getIdDoUltimoRegistroInserido();
//
//            DatabaseSincronizador::procedureAddUsuarioTipo(
//                $vIdUsuario, $vIdCorporacao, $vIdUsuario, $pIMEI, $idPrograma);
//
//            EXTDAO_Sistema_registro_sincronizador::insertTupla(
//                $vIdUsuarioSistemaTabela, $vIdUsuario, $vIdCorporacao, '1', DatabaseSincronizador::$INDEX_OPERACAO_INSERT, $pIMEI, $idPrograma, $vIdUsuario);
//
//            if (strcmp($vIdCorporacao, "null") == 0) {
//                $vIdCorporacao = null;
//            }
//
//
//            if ($vIdCorporacao == null) {
//                echo "ERROR: insercao da corporacao";
//                return;
//            }
//            DatabaseSincronizador::insertTuplasDosServicosDoUsuario(
//                $vIdUsuario, $vIdCorporacao, $vIdUsuario, $pIMEI, $idPrograma);
//
//            DatabaseSincronizador::insertTuplasDoTipoDeDocumento(
//                $vIdUsuario, $vIdCorporacao, $vIdUsuario, $pIMEI, $idPrograma);
//            //cadastra a tupla usuario corporacao
//            $vUsuarioCorporacao = new EXTDAO_Usuario_corporacao();
//            $vUsuarioCorporacao->setUsuario_id_INT($vIdUsuario);
//            $vUsuarioCorporacao->setStatus_BOOLEAN("1");
//            $vUsuarioCorporacao->setIs_adm_BOOLEAN("1");
//            $vUsuarioCorporacao->formatarParaSQL();
//            $vUsuarioCorporacao->setCorporacao_id_INT($vUsuarioCorporacao->formatarDados($vIdCorporacao));
//            $vUsuarioCorporacao->insert(false, $vIdCorporacao);
//            $vNewIdUsuarioCorporacao = $vUsuarioCorporacao->getIdDoUltimoRegistroInserido($vIdCorporacao);
//
//            EXTDAO_Sistema_registro_sincronizador::insertTupla(
//                $vIdUsuarioCorporacaoSistemaTabela, $vIdUsuario, $vIdCorporacao, '1', DatabaseSincronizador::$INDEX_OPERACAO_INSERT, $pIMEI, $idPrograma, $vNewIdUsuarioCorporacao);
//
//            DatabaseSincronizador::sendInformacoesParaEmail($email, ASSUNTO_CADASTRO_USUARIO, CONTEUDO_ASSUNTO_CADASTRO_USUARIO);
//            echo "TRUE";
//            return;
//        } else {
//            echo "ERROR: parametro invalido";
//            return;
//        }
//    }

    public static function autoLogin()
    {
        $seguranca = Registry::get('Seguranca');
        if ($seguranca->autoLogin())
        {
            $autoLoginCrypt = Helper::COOKIEPOSTGET(ConstanteSeguranca::CHAVE_AUTO_LOGIN);

            $obj = new  stdClass();
            $session = SessionRedis::getSingleton();

            $obj->SS_AL = $autoLoginCrypt;
            $obj->SS_CHAVE = $session->getSessionKey();

            //se passar pelo sess�o � porque foi poss�vel o autologin
            return new Mensagem_protocolo($obj);
        }
        else
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ACESSO_NEGADO,
                                I18N::getExpression("Por favor, realize o login novamente"));
        }
    }

    public static function relatoDeErro()
    {
        $relato = Helper::POSTGET('relato');
        $drawable = Helper::POSTGET('drawable');
        $tagTela = Helper::POSTGET('tag_tela');

        if (!empty($relato))
        {
            $configuracaoSite = Registry::get('ConfiguracaoSite');
            $pathConteudo = $configuracaoSite->PATH_CONTEUDO;
            $pathConteudo .= "/ouvidoria/erro_relatado_android/";
            if (!is_dir($pathConteudo))
            {
                Helper::mkdir($pathConteudo, 0777, true);
            }
            $arquivo = Helper::getDiaEHoraNomeArquivo();
            $pathArquivo = $pathConteudo . $arquivo . ".log";

            try
            {
                $obj = new stdClass();
                $obj->relato = $relato;
                $obj->drawable = $drawable;
                $obj->tagTela = $tagTela;
                $json = Helper::jsonEncode($obj);
                Helper::appendOrCreateFile($pathArquivo, $json);
            }
            catch (Exception $exception)
            {
                HelperLog::logErro($exception);
            }

            //se passar pelo sess�o � porque foi poss�vel o autologin
            return new Mensagem(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO);
        }
        else
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_PARAMETRO_INVALIDO);
        }
    }

    public static function sugestaoDeMelhoria()
    {
        $relato = Helper::POSTGET('sugestao');
        $drawable = Helper::POSTGET('drawable');
        $tagTela = Helper::POSTGET('tag_tela');

        if (!empty($relato))
        {
            $configuracaoSite = Registry::get('ConfiguracaoSite');
            $pathConteudo = $configuracaoSite->PATH_CONTEUDO;
            $pathConteudo .= "/ouvidoria/sugestao_melhoria/";
            if (!is_dir($pathConteudo))
            {
                Helper::mkdir($pathConteudo, 0777, true);
            }
            $arquivo = Helper::getDiaEHoraNomeArquivo();
            $pathArquivo = $pathConteudo . $arquivo . ".log";

            try
            {
                $obj = new stdClass();
                $obj->relato = $relato;
                $obj->drawable = $drawable;
                $obj->tagTela = $tagTela;
                $json = Helper::jsonEncode($obj);
                Helper::appendOrCreateFile($pathArquivo, $json);
            }
            catch (Exception $exception)
            {
                HelperLog::logErro($exception);
            }

            //se passar pelo sess�o � porque foi poss�vel o autologin
            return new Mensagem(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO);
        }
        else
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_PARAMETRO_INVALIDO);
        }
    }

    public static function getDashboard()
    {
        //$objUsuarioCorporacao->setCadastro_SEC(Helper::getUTCNowSec());
        //$objUsuarioCorporacao->setCadastro_OFFSEC(Helper::getTimezoneOffsetSec());
        $ontem = Helper::getUTCNowSec() - (24 * 60 * 60);
        $q = "SELECT is_entrada_BOOLEAN, count(*) 
          FROM ponto 
          WHERE data_SEC >= $ontem"
            . " AND corporacao_id_INT=" . Seguranca::getIdDaCorporacaoLogada() . " 
            GROUP BY is_entrada_BOOLEAN";

        $db = new Database();
        $msg = $db->queryMensagem($q);
        if (Interface_mensagem::checkErro($msg))
        {
            return $msg;
        }
        $res = Helper::getResultSetToMatriz($db->result, 0, 1);
        if (count($res) == 0)
        {
            return null;
        }
        $ret = new stdClass();
        for ($i = 0; $i < count($res); $i++)
        {
            //se for a quantidade total de pontos de entrada
            if ($res[$i][0] == 1)
            {
                $ret->totalEntrada = $res[$i][1];
            }
            else
            {
                if ($res[$i][0] == 0)
                {
                    $ret->totalSaida = $res[$i][1];
                }
            }
        }
        return new Mensagem_protocolo($ret);
    }

    public static function cadastrarClienteNaAssinaturaSicob()
    {
        return BO_SICOB::cadastrarClienteNaAssinatura(
            Helper::POSTGET("email"),
            Helper::POSTGET("senha"),
            Helper::POSTGET("nome"));
    }

    public static function alterarSenhaWeb($senhaNova, $senhaAtual)
    {
        if (strlen($senhaNova))
        {
            $idUsuario = Seguranca::getId();
            $idCorporacao = Seguranca::getIdDaCorporacaoLogada();

            $boUsuario = new BO_Usuario();
            $msg = $boUsuario->alterarSenhaPeloSicob($idUsuario, $idCorporacao, $senhaAtual, $senhaNova);

            return $msg;
        }
        else
        {
            return new Mensagem(
                PROTOCOLO_SISTEMA::ACESSO_NEGADO,
                I18N::getExpression("Voc� deve digitar a nova senha desejada!"));
        }
    }

    public function loginWeb()
    {
        $corporacao = Helper::POSTGET("corporacao");
        $email = Helper::POSTGET("email");
        $senha = Helper::POSTGET("senha");

        if (strlen($email) && strlen($corporacao) && strlen($senha))
        {
            $db = new Database();
            $seguranca = new Seguranca();

            return $seguranca->loginWeb($email, $corporacao, $senha, $db);
        }
        else
        {
            return Mensagem::factoryParametroInvalido();
        }
    }

    public static function verificaExistenciaDoUsuarioNoSicobFromWeb($emailUsuario)
    {
        if (strlen($emailUsuario))
        {
            return BO_SICOB::verificaExistenciaDoCliente($emailUsuario);
        }
        else
        {
            return new Mensagem(
                PROTOCOLO_SISTEMA::ERRO_PARAMETRO_INVALIDO,
                I18N::getExpression("Par�metros incorretos."));
        }
    }

    public static function trocarEmailDoClientePeloSistema($idUsuario, $nomeNovo, $emailNovo, $senhaDecrypt)
    {
        if ($idUsuario && $nomeNovo && $emailNovo && $senhaDecrypt)
        {
            $mensagem = BO_Usuario::trocarEmailDoClientePeloSistema($idUsuario, $nomeNovo, $emailNovo, $senhaDecrypt);
            return $mensagem;
        }
        else
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_PARAMETRO_INVALIDO, I18N::getExpression("Par�metros inv�lidos"));
        }
    }

    public static function cadastrarClienteNaAssinaturaSicobFromWeb($parameters, $db)
    {
        if ($parameters->nome && $parameters->email && $parameters->senha)
        {
            return BO_SICOB::cadastrarClienteNaAssinatura($parameters->nome, $parameters->email, $parameters->senha, $db);
        }
        else
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_PARAMETRO_INVALIDO, I18N::getExpression("Par�metros inv�lidos"));
        }
    }

    public static function teste()
    {
        $t = Helper::POSTGET("tabela");
        $totGerar = Helper::POSTGET("tot_gerar");
        $id = EXTDAO_Sistema_sequencia::gerarId($t, $totGerar);
        echo "ID: $id.";
        exit();
    }

    public static function rastreamento()
    {
        $idUsuario = Helper::POSTGET("usuario");
        $idCorporacao = Helper::POSTGET("id_corporacao");
        $vListaStrTupla = Helper::POSTGET("lista_tupla");
        if (strlen($idUsuario)
            && strlen($idCorporacao)
            && strlen($vListaStrTupla))
        {
            $bo = new BO_Usuario_posicao();
            return $bo->salvarPosicoesNoBanco(
                $idUsuario
                , $idCorporacao
                , $vListaStrTupla
            );
        }
        else
        {
            return Mensagem::factoryParametroInvalido();
        }
    }


//    public static function getDadosMapaV2(){
//
//        $idCorporacao = Helper::POSTGET("id_corporacao");
//        if(strlen($idCorporacao )) {
//
//            $bo = new BO_Usuario_posicao();
//            return $bo->procedimentoGetDadosMapa($idCorporacao);
//        } else return Mensagem::factoryParametroInvalido();
//    }

    public static function getDadosMapa()
    {
        $idCorporacao = Helper::POSTGET("id_corporacao");
        if(Helper::isNullOrEmpty($idCorporacao))
        {
            $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
        }

        if (strlen($idCorporacao))
        {
            $ret = new stdClass();
            $boMapa = new BO_Mapa();

            $ret->pontos = $boMapa->getDadosPontosEletronicos($idCorporacao);
            $ret->empresas = $boMapa->getDadosEmpresas($idCorporacao);
            $ret->usuarios = $boMapa->getDadosUsuarios($idCorporacao);
            $ret->wifi = $boMapa->getDadosWifi($idCorporacao);

            return new Mensagem_generica($ret);
        }
        else
        {
            return Mensagem::factoryParametroInvalido();
        }
    }

    public static function getDadosCalendario()
    {
        $idCorporacao = Helper::POSTGET("id_corporacao");
        if(Helper::isNullOrEmpty($idCorporacao))
        {
            $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
        }

        if (is_numeric($idCorporacao))
        {
            $ret = new stdClass();
            $boCalendario = new BO_Calendario();

            $dataInicialSec = $_POST["dataInicialSec"];
            $dataFinalSec = $_POST["dataFinalSec"];

            $ret->pontos = $boCalendario->getDadosPontosEletronicos($idCorporacao, $dataInicialSec, $dataFinalSec);
            return new Mensagem_generica($ret);
        }
        else
        {
            return Mensagem::factoryParametroInvalido();
        }
    }

    public static function gravarPontoEletronicoParaCorrecao()
    {
        $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
        if (is_numeric($idCorporacao))
        {
            $idPontoReferencia = $_POST["idPontoReferencia"];
            $idPessoa = $_POST["idPessoa"];
            $timestampPonto = $_POST["utcTimestampPonto"];
            $timezoneOffsetPonto = $_POST["utcTimezoneOffset"];
            $tipoPonto = $_POST["tipoPonto"];

            if(is_numeric($idPontoReferencia) && is_numeric($idPessoa)
                && is_numeric($timestampPonto) && is_numeric($timezoneOffsetPonto) && is_numeric($tipoPonto))
            {
                $boCalendario = new BO_Calendario();
                $mensagemSalvarPonto = $boCalendario->gravarPontoEletronicoParaCorrecao($idCorporacao,
                                                                                          $idPontoReferencia,
                                                                                          $idPessoa,
                                                                                          $timestampPonto,
                                                                                          $timezoneOffsetPonto,
                                                                                          $tipoPonto);
            }
            else
            {
                return Mensagem::factoryParametroInvalido();
            }

            return $mensagemSalvarPonto;
        }
        else
        {
            return Mensagem::factoryParametroInvalido();
        }
    }



}
