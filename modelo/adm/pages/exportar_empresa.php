<?php

include '../../recursos/languages/pt-br.php';
include '../../recursos/php/funcoes.php';
include '../../recursos/php/constants.php';
include '../../recursos/php/database_config.php';
include '../../adm/imports/sessao.php';

$nomeScript = Helper::getNomeDoScriptAtual();

$vIsPrimeiraVez = Helper::GET("is_primeira_vez_BOOLEAN");

$registrosPorPagina = REGISTROS_POR_PAGINA;
$registrosPesquisa = 1;

$obj = new EXTDAO_Empresa();
$obj->setByGet($registrosPesquisa);
$obj->formatarParaSQL();

$strCondicao = array();
$strGET = array();

if (!Helper::isNull($obj->getNome()))
{

    $strCondicao[] = "nome LIKE '%{$obj->getNome()}%'";
    $strGET[] = "nome1={$obj->getNome()}";
}

if (!Helper::isNull($obj->getTelefone1()))
{

    $strCondicao[] = "telefone11={$obj->getTelefone1()}";
    $strGET[] = "telefone11={$obj->getTelefone1()}";
}

if (!Helper::isNull($obj->getTelefone2()))
{

    $strCondicao[] = "telefone21={$obj->getTelefone2()}";
    $strGET[] = "telefone21={$obj->getTelefone2()}";
}

if (!Helper::isNull($obj->getFax()))
{

    $strCondicao[] = "fax LIKE '%{$obj->getFax()}%'";
    $strGET[] = "fax1={$obj->getFax()}";
}

if (!Helper::isNull($obj->getCelular()))
{

    $strCondicao[] = "celular LIKE '%{$obj->getCelular()}%'";
    $strGET[] = "celular1={$obj->getCelular()}";
}

if (!Helper::isNull($obj->getOperadora_id_INT()))
{

    $strCondicao[] = "operadora_id_INT1={$obj->getOperadora_id_INT()}";
    $strGET[] = "operadora_id_INT1={$obj->getOperadora_id_INT()}";
}

if (!Helper::isNull($obj->getEmail()))
{

    $strCondicao[] = "email1={$obj->getEmail()}";
    $strGET[] = "email1={$obj->getEmail()}";
}

if (!Helper::isNull($obj->getTipo_documento_id_INT()))
{

    $strCondicao[] = "tipo_documento_id_INT1={$obj->getTipo_documento_id_INT()}";
    $strGET[] = "tipo_documento_id_INT1={$obj->getTipo_documento_id_INT()}";
}

if (!Helper::isNull($obj->getNumero_documento()))
{

    $strCondicao[] = "numero_documento LIKE '%{$obj->getNumero_documento()}%'";
    $strGET[] = "numero_documento1={$obj->getNumero_documento()}";
}

if (!Helper::isNull($obj->getTipo_empresa_id_INT()))
{

    $strCondicao[] = "tipo_empresa_id_INT1={$obj->getTipo_empresa_id_INT()}";
    $strGET[] = "tipo_empresa_id_INT1={$obj->getTipo_empresa_id_INT()}";
}

if (!Helper::isNull($obj->getLogradouro()))
{

    $strCondicao[] = "logradouro LIKE '%{$obj->getLogradouro()}%'";
    $strGET[] = "logradouro1={$obj->getLogradouro()}";
}

if (!Helper::isNull($obj->getNumero()))
{

    $strCondicao[] = "numero LIKE '%{$obj->getNumero()}%'";
    $strGET[] = "numero1={$obj->getNumero()}";
}

if (!Helper::isNull($obj->getComplemento()))
{

    $strCondicao[] = "complemento LIKE '%{$obj->getComplemento()}%'";
    $strGET[] = "complemento1={$obj->getComplemento()}";
}

if (!Helper::isNull($obj->getBairro_id_INT()))
{

    $strCondicao[] = "bairro_id_INT1={$obj->getBairro_id_INT()}";
    $strGET[] = "bairro_id_INT1={$obj->getBairro_id_INT()}";
}

if (!Helper::isNull($obj->getCidade_id_INT()))
{

    $strCondicao[] = "cidade_id_INT1={$obj->getCidade_id_INT()}";
    $strGET[] = "cidade_id_INT1={$obj->getCidade_id_INT()}";
}

$consulta = "";

for ($i = 0; $i < count($strCondicao); $i++)
{

    $consulta .= " AND " . $strCondicao[$i];
}

for ($i = 0; $i < count($strGET); $i++)
{

    $varGET .= "&" . $strGET[$i];
}

if (strlen($consulta))
{
    $consulta .= " AND corporacao_id_INT = " . Seguranca::getIdDaCorporacaoLogada();
}
else
{
    $consulta .= " corporacao_id_INT = " . Seguranca::getIdDaCorporacaoLogada();
}

$strWhere = "";
if (strlen($consulta) > 0)
{
    $strWhere .= "WHERE $consulta ";
}
$consultaNumero = "SELECT COUNT(id) FROM empresa {$strWhere}";

if (strlen($consulta) > 0)
{
    if ($nomeScript == "index.php" && !isset($vIsPrimeiraVez))
    {
        echo "<center>";
        Helper::imprimirMensagem("A janela de download abrirá em alguns segundos.\nCaso não abra automaticamente, <a class='link_padrao' href='pages/exportar_empresa.php?is_primeira_vez_BOOLEAN=0&$varGET'>clique aqui</a>.");
        echo "</center>";

        Helper::mudarLocation("pages/exportar_empresa.php?is_primeira_vez_BOOLEAN=0&$varGET");
        exit();
    }
}

$objBanco = new Database();

$objBanco->query($consultaNumero);
$numeroRegistros = $objBanco->getPrimeiraTuplaDoResultSet(0);

$limites = Helper::getLimitesRegsPaginacao($registrosPorPagina, $numeroRegistros);
$consultaWhere = "";
if (strlen($consulta) > 0)
{
    $consultaWhere = "WHERE {$consulta}";
}

$consultaRegistros = "SELECT id FROM empresa {$consultaWhere} ORDER BY nome LIMIT {$limites[0]},{$limites[1]}";

$objBanco->query($consultaRegistros);

$stringRetorno = "";
$vetorCabecalho = array("Id no Sistema", "Nome", "Tipo do Documento", "Número do Documento", "Telefone Fixo 1", "Telefone Fixo 2", "Fax",
    "Telefone Celular", "Operadora", "Celular Para Envio de SMS", "Email", "Ramo da Empresa", "Logradouro", "Número", "Complemento", "País", "Estado", "Cidade", "Bairro");
$stringRetorno .= Helper::getStrLinhaCSVDoVetorDeDados($vetorCabecalho);

for ($i = 1; $regs = $objBanco->fetchArray(); $i++)
{
    $vId = $regs[0];
    $vObjEmpresa = new EXTDAO_Empresa();
    $vObjEmpresa->select($vId);

    $vNome = $vObjEmpresa->getNome();
    $vTipoDocumento = "";
    $vNumeroDocumento = "";
    $vTelefone1 = "";
    $vTelefone2 = "";
    $vFax = "";
    $vCelular = "";
    $vOperadora = "";
    $vCelularSMS = "";
    $vEmail = "";
    $vTipoEmpresa = "";
    $vLogradouro = "";
    $vNumero = "";
    $vComplemento = "";
    $vPais = "";
    $vEstado = "";
    $vCidade = "";
    $vBairro = "";

    $vTipoDocumentoIdInt = $vObjEmpresa->getTipo_documento_id_INT();
    if (strlen($vTipoDocumentoIdInt))
    {
        $vObjTipoDocumento = new EXTDAO_Tipo_documento();
        $vObjTipoDocumento->select($vTipoDocumentoIdInt);
        $vTipoDocumento = $vObjTipoDocumento->getNome();
        $vNumeroDocumento = $vObjEmpresa->getNumero_documento();
    }

    if (strlen($vObjEmpresa->getTelefone1()))
    {
        $vTelefone1 = $vObjEmpresa->getTelefone1();
    }
    if (strlen($vObjEmpresa->getTelefone2()))
    {
        $vTelefone2 = $vObjEmpresa->getTelefone2();
    }
    if (strlen($vObjEmpresa->getFax()))
    {
        $vFax = $vObjEmpresa->getFax();
    }
    if (strlen($vObjEmpresa->getCelular()))
    {
        $vCelular = $vObjEmpresa->getCelular();
    }
    if (strlen($vObjEmpresa->getCelular_sms()))
    {
        $vCelularSMS = $vObjEmpresa->getCelular_sms();
    }
    if (strlen($vObjEmpresa->getOperadora_id_INT()))
    {
        $vObjOperadora = new EXTDAO_Operadora();
        $vObjOperadora->select($vObjEmpresa->getOperadora_id_INT());
        $vOperadora = $vObjOperadora->getNome();
    }
    if (strlen($vObjEmpresa->getTipo_empresa_id_INT()))
    {
        $vObjTipoEmpresa = new EXTDAO_Tipo_empresa();
        $vObjTipoEmpresa->select($vObjEmpresa->getTipo_empresa_id_INT());
        $vTipoEmpresa = $vObjTipoEmpresa->getNome();
    }
    if (strlen($vObjEmpresa->getEmail()))
    {
        $vEmail = $vObjEmpresa->getEmail();
    }
    if (strlen($vObjEmpresa->getLogradouro()))
    {
        $vLogradouro = $vObjEmpresa->getLogradouro();
    }
    if (strlen($vObjEmpresa->getNumero()))
    {
        $vNumero = $vObjEmpresa->getNumero();
    }
    if (strlen($vObjEmpresa->getComplemento()))
    {
        $vComplemento = $vObjEmpresa->getComplemento();
    }
    if (strlen($vObjEmpresa->getBairro_id_INT()))
    {
        $vObjBairro = new EXTDAO_Bairro();
        $vObjBairro->select($vObjEmpresa->getBairro_id_INT());
        $vBairro = $vObjBairro->getNome();
    }
    if (strlen($vObjEmpresa->getCidade_id_INT()))
    {
        $vObjCidade = new EXTDAO_Cidade();
        $vObjCidade->select($vObjEmpresa->getCidade_id_INT());
        $vCidade = $vObjCidade->getNome();
        if (strlen($vObjCidade->getUf_id_INT()))
        {
            $vObjEstado = new EXTDAO_Uf();
            $vObjEstado->select($vObjCidade->getUf_id_INT());
            $vEstado = $vObjEstado->getNome();
            $vIdPais = $vObjEstado->getPais_id_INT();
            if (strlen($vIdPais))
            {

                $vObjPais = new EXTDAO_Pais();
                $vObjPais->select($vIdPais);
                $vPais = $vObjPais->getNome();
            }
        }
    }
    $vetorTupla = array($vId, $vNome, $vTipoDocumento, $vNumeroDocumento, $vTelefone1, $vTelefone2, $vFax,
        $vCelular, $vOperadora, $vCelularSMS, $vEmail, $vLogradouro, $vNumero, $vComplemento, $vPais, $vEstado,
        $vCidade, $vBairro);
    $stringRetorno .= Helper::getStrLinhaCSVDoVetorDeDados($vetorTupla);
}

$objDownload = new Download("relatorio_empresa.csv");
print $objDownload->ds_download($stringRetorno);
?>
