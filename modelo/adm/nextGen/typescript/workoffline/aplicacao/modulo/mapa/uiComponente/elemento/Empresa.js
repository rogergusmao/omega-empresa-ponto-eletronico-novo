"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var uiComponente_1 = require("lib/uiComponente");
var uiFactory_1 = require("aplicacao/modulo/mapa/uiFactory");
var Empresa = (function (_super) {
    __extends(Empresa, _super);
    function Empresa(contexto, modelEmpresa) {
        var _this = _super.call(this, contexto, modelEmpresa.latitude, modelEmpresa.longitude, 'empresa') || this;
        _this.modelEmpresa = modelEmpresa;
        _this.setDefaultLabelObject(modelEmpresa.nomeEmpresa);
        return _this;
    }
    Empresa.prototype.setDefaultIconeObject = function () {
        if (this.modelEmpresa.wifiNetworks.length > 0) {
            _super.prototype.setDefaultIconeObject.call(this, this.contexto.getDefaultMarkerFolder() + "map_wifi.png");
        }
        else {
            _super.prototype.setDefaultIconeObject.call(this, this.contexto.getDefaultMarkerFolder() + "map_company.png");
        }
    };
    Empresa.prototype.addDefaultClickListener = function () {
        var _this = this;
        _super.prototype.addClickListener.call(this, function () {
            var janelaModal = uiFactory_1.ModalFactory.getModalDetalhesEmpresa(_this.contexto, _this.modelEmpresa);
            janelaModal.handleShow();
        });
    };
    return Empresa;
}(uiComponente_1.Marcador));
exports.Empresa = Empresa;
