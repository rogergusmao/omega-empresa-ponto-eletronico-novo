"use strict";
exports.__esModule = true;
var React = require("react");
var uiComponente_1 = require("aplicacao/modulo/mapa/uiComponente");
var ModalFactory = (function () {
    function ModalFactory() {
    }
    ModalFactory.getModalDetalhesUsuario = function (contexto, usuario) {
        var modalWindow = <uiComponente_1.DetalhesUsuario modelUsuario={usuario} setDefaultBody setDefaultHeader/>;
        return contexto.renderReactComponent(modalWindow, '#modal-container');
    };
    ModalFactory.getModalDetalhesEmpresa = function (contexto, empresa) {
        var modalWindow = <uiComponente_1.DetalhesEmpresa modelEmpresa={empresa} setDefaultBody setDefaultHeader/>;
        return contexto.renderReactComponent(modalWindow, '#modal-container');
    };
    ModalFactory.getModalDetalhesPontoEletronico = function (contexto, pontoEletronico) {
        var modalWindow = <uiComponente_1.DetalhesPontoEletronico modelPontoEletronico={pontoEletronico} setDefaultBody setDefaultHeader/>;
        return contexto.renderReactComponent(modalWindow, '#modal-container');
    };
    return ModalFactory;
}());
exports.ModalFactory = ModalFactory;
