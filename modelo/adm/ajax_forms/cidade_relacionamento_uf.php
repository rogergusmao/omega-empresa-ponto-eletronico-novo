<?php

/*
*
* -------------------------------------------------------
* NOME DO FORMUL�RIO: cidade
* DATA DE GERA��O:    30.09.2011
* ARQUIVO:            cidade.php
* TABELA MYSQL:       cidade
* BANCO DE DADOS:     clinica_odontologica
* -------------------------------------------------------
*
*/

if (isset($_GET["contador"]))
{

    $numeroRegistroInterno = Helper::GET("contador");
}

if (isset($identificadorRelacionamento) && is_numeric($identificadorRelacionamento))
{

    $objCidade = new EXTDAO_Cidade();
    $objCidade->select($identificadorRelacionamento);
}
else
{

    $objCidade = new EXTDAO_Cidade();
}

$objArgCidade = new Generic_Argument();
$objCidade->formatarParaExibicao();

?>

<input type="hidden" name="cidade_id_<?= $numeroRegistroInterno ?>" id="cidade_id_<?= $numeroRegistroInterno ?>"
       value="<?= $objCidade->getId(); ?>">

<table class="tabela_form">

    <tr class="tr_form">


        <?

        $objArgCidade = new Generic_Argument();

        $objArgCidade->numeroDoRegistro = "";
        $objArgCidade->label = $objCidade->label_nome;
        $objArgCidade->valor = $objCidade->getNome();
        $objArgCidade->classeCss = "input_text";
        $objArgCidade->classeCssFocus = "focus_text";
        $objArgCidade->obrigatorio = true;
        $objArgCidade->largura = 200;
        $objArgCidade->nome = "cidade_nome_{$numeroRegistroInterno}";
        $objArgCidade->id = "cidade_nome_{$numeroRegistroInterno}";

        ?>

        <td class="td_form_label"><?= $objArgCidade->getLabel() ?></td>
        <td class="td_form_campo"><?= $objCidade->campoTexto($objArgCidade); ?></td>


        <td class="td_form_label"></td>
        <td class="td_form_campo"></td>
    </tr>
    <tr>
        <td colspan="4" class="td_botao_remover_da_lista"><input class="botoes_form" type="button"
                                                                 value="Remover Cidade"
                                                                 onclick="javascript:removerDivAjaxEmLista(this);"></td>
    </tr>
</table><br/>

