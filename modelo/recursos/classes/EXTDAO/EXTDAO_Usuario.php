<?php

/*

Arquivo gerado através de gerador de código em 01/06/2017 as 15:18:33.
Para que o arquivo não seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

Tabela correspondente: usuario
Sobrescrita de dados: ---SOBRESCRITA_NAO_AUTORIZADA---

*/



class EXTDAO_Usuario extends DAO_Usuario
{
    const CHAVE_CACHE_STRING_UTILIZACAO_USUARIOS_CORPORACAO = "utilizacaoUsuariosCorporacao";

    public function __construct($configDAO = null)
    {
        parent::__construct($configDAO);
    }

    public static function forcarRefreshDaStringDeUtilizacaoDeUsuariosDaCorporacao()
    {
        Helper::clearSession(static::CHAVE_CACHE_STRING_UTILIZACAO_USUARIOS_CORPORACAO);
        static::getStringDeUtilizacaoDeUsuariosDaCorporacao();
    }

    public static function getStringDeUtilizacaoDeUsuariosDaCorporacao()
    {
        $stringSession = Helper::SESSION(static::CHAVE_CACHE_STRING_UTILIZACAO_USUARIOS_CORPORACAO);
        if (!is_null($stringSession))
        {
            return $stringSession;
        }
        else
        {
            $caracteristicasPacoteUsuarioLogado = EXTDAO_Usuario::getCaracteristicasDoPacote();
            if ($caracteristicasPacoteUsuarioLogado->ok())
            {
                $dadosPacote = $caracteristicasPacoteUsuarioLogado->mObj;
                $usuariosOcupados = $dadosPacote->numeroUsuarioOcupado;
                $usuariosTotal = $dadosPacote->totalUsuario;
                $string = "{$usuariosOcupados}/{$usuariosTotal}";
            }
            else
            {
                $string = I18N::getExpression("N/D");
            }

            Helper::setSession(static::CHAVE_CACHE_STRING_UTILIZACAO_USUARIOS_CORPORACAO, $string);
            return $string;
        }
    }

    function setEmail($val)
    {
        $this->email = Helper::strtolowerlatin1($val);
    }

    public static function getNumeroDeUsuariosCadastrados($idCorporacao)
    {
        $q = "SELECT COUNT(u.id) 
                      FROM usuario u 
                          JOIN usuario_corporacao uc 
                            ON uc.usuario_id_INT = u.id
                      WHERE uc.corporacao_id_INT = {$idCorporacao}";
        $db = new Database();
        $db->query($q);

        return $db->getPrimeiraTuplaDoResultSet(0);
    }

    public function __actionRemove($parameters = null)
    {
        $successulRemovalCount = 0;
        if (is_null($parameters))
        {
            $parameters = Helper::getPhpInputObject();
        }

        try
        {
            $msgProtocolo = BO_SICOB::getCaracteristicasDoPacoteDaCorporacao();
            if ($msgProtocolo->mCodRetorno != PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO)
            {
                $mensagem = I18N::getExpression("Falha ao executar operação de exclusão. Por favor tente novamente mais tarde.");
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, $mensagem);
            }
            else
            {
                $protocolo = $msgProtocolo->mObj;
            }

            foreach ($parameters as $record)
            {
                if (is_numeric($record->id))
                {
                    $objUsuarioAtual = new EXTDAO_Usuario($this->database);
                    $objUsuarioAtual->select($record->id);
                    $email = $objUsuarioAtual->getEmail();

                    if (strcmp($email, $protocolo->emailUsuarioPrincipal) == 0)
                    {
                        $mensagem = I18N::getExpression("Não é permitido remover o usuário dono do grupo.");

                        return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, $mensagem);
                    }
                    else
                    {
                        $msg = $this->delete($record->id, true);
                        if ($msg == null || $msg->ok())
                        {
                            $successulRemovalCount++;
                        }
                    }
                }
            }

            if ($successulRemovalCount > 0)
            {
                static::forcarRefreshDaStringDeUtilizacaoDeUsuariosDaCorporacao();
                $successMessage = count($successulRemovalCount) > 1 ? I18N::getExpression("{0} Usuários foram removidos com sucesso.", $successulRemovalCount) : I18N::getExpression("Usuário removido com sucesso.");
                return new Mensagem(null, $successMessage);
            }
            else
            {
                $errorMessage = count($parameters) > 1 ? I18N::getExpression("Falha ao remover Sexos.") : I18N::getExpression("Falha ao remover Sexo.");
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, $errorMessage);
            }





        }
        catch (Exception $ex)
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
        }
    }

    public static function getIdUsuario($email, $db = null)
    {
        try
        {
            $email = Helper::strtolowerlatin1($email);
            $q = "SELECT id
                      FROM usuario
                      WHERE email='{$email}'";

            if ($db == null)
            {
                $db = new Database();
            }

            $db->query($q);
            $id = $db->getPrimeiraTuplaDoResultSet(0);

            return $id;
        }
        catch (Exception $ex)
        {
            return null;
        }
    }

    protected function getFormComboBoxesData($dadosUsuario = null)
    {
        $comboBoxesData = new stdClass();
        $listParameters = new ListParameters(static::LIST_MAPPING_TYPE_COMBOBOX);

        $objUsuarioTipo = new EXTDAO_Usuario_tipo($this->database);
        $comboBoxesData->fieldUsuarioTipoId = $objUsuarioTipo->__getList($listParameters);

        $objUsuarioTipo = new EXTDAO_Empresa($this->database);
        $comboBoxesData->fieldEmpresaId = $objUsuarioTipo->__getList($listParameters);

        $objTipoDocumento = new EXTDAO_Tipo_documento($this->database);
        $comboBoxesData->fieldPessoaTipoDocumentoId = $objTipoDocumento->__getList($listParameters);

        $objSexo = new EXTDAO_Sexo($this->database);
        $comboBoxesData->fieldPessoaSexoId = $objSexo->__getList($listParameters);

        $objCidade = new EXTDAO_Cidade($this->database);
        $comboBoxesData->fieldPessoaCidadeId = $objCidade->__getList($listParameters);

        if (!is_null($dadosUsuario) && !is_null($dadosUsuario->pessoaCidadeId))
        {
            $listParametersBairro = new ListParameters(static::LIST_MAPPING_TYPE_COMBOBOX);
            $listParametersBairro->addFilterParameters("cidadeId", $dadosUsuario->pessoaCidadeId);

            $objBairro = new EXTDAO_Bairro($this->database);
            $comboBoxesData->fieldPessoaBairroId = $objBairro->__getList($listParametersBairro);
        }

        return $comboBoxesData;
    }

    public static function getFirstIdUsuario($db = null)
    {
        $q = "SELECT id FROM usuario";
        if ($db == null)
        {
            $db = new Database();
        }

        $db->query($q);
        $id = $db->getPrimeiraTuplaDoResultSet(0);

        return $id;
    }

    public static function verificarSeIsUsuarioDaEmpresaLogada($idUsuario)
    {
        if (Seguranca::getIdDaEmpresaLogada())
        {
            $idEmpresa = Seguranca::getIdDaEmpresaLogada();
            $objBanco = new Database();
            $objBanco->query("SELECT id FROM usuario_empresa WHERE usuario_id_INT={$idUsuario} AND empresa_id_INT={$idEmpresa}");

            if ($objBanco->rows() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return true;
        }
    }

    public function lembrarSenha()
    {
        try
        {
            $dadosPost = json_decode(file_get_contents('php://input'));
            $vEmailCadastrado = Helper::strtolowerlatin1($dadosPost->email);

            $this->database->query("SELECT id, nome, email, senha
                                FROM usuario
                                WHERE email='{$vEmailCadastrado}'");

            if ($this->database->rows() == 1)
            {
                $nomeUsuario = $this->database->getPrimeiraTuplaDoResultSet(1);
                $emailDestino = $this->database->getPrimeiraTuplaDoResultSet(2);
                $senha = $this->databse->getPrimeiraTuplaDoResultSet(3);

                $objCrypt = new Crypt();
                $senhaDescriptografada = $objCrypt->decrypt($senha);

                $mensagem = "--- Lembrete de senha de acesso ao " . TITULO_PAGINAS . " ---\n
        					 Endereço para Acesso: " . ENDERECO_DE_ACESSO . "
        					 Usuário: {$emailDestino}
        					 Nome   : {$nomeUsuario} 
        					 Senha  : {$senhaDescriptografada}";

                $s = HelperAPCRedis::getSingleton();
                $s = new HelperAPCRedis();
                $html = null;
                if ($s->exists("TEMPLATE_RELEMBRAR_SENHA"))
                {
                    $html = $s->get("TEMPLATE_RELEMBRAR_SENHA");
                }
                else
                {
                    $html = file_get_contents(Helper::formatarPath(Helper::acharRaiz() . "/conteudo/email_template/relembrar_senha.html"));
                    $s->add("TEMPLATE_RELEMBRAR_SENHA", $html);
                }
                $mensagem = $html;
                $mensagem = str_replace(":TITULO", I18N::getExpression("Relembrando sua senha"), $mensagem);
                $mensagem = str_replace(":LABEL_EMAIL", I18N::getExpression("Email"), $mensagem);
                $mensagem = str_replace(":EMAIL1", $emailDestino, $mensagem);
                $mensagem = str_replace(":EMAIL", $emailDestino, $mensagem);
                $mensagem = str_replace(":LABEL_SENHA", I18N::getExpression("Senha"), $mensagem);
                $mensagem = str_replace(":SENHA", $senhaDescriptografada, $mensagem);

                $objEmail = new Email_Sender(EMAIL_PADRAO_REMETENTE_MENSAGENS, $emailDestino);
                $objEmail->setAssunto(ASSUNTO_DO_EMAIL_DE_REENVIO_SENHA);
                $objEmail->setConteudo($mensagem, true);
                $objEmail->enviarEmail();
                $exp = I18N::getExpression("Lembrete de senha de acesso ao " . TITULO_PAGINAS . " \n
        					 Endereço para Acesso: {0} ", ENDERECO_DE_ACESSO);

                $mensagem = str_replace(":TEXTO", $exp, $mensagem);

                $objEmail = new Email_Sender(EMAIL_PADRAO_REMETENTE_MENSAGENS, $emailDestino);
                $objEmail->setAssunto(I18N::getExpression(ASSUNTO_DO_EMAIL_DE_REENVIO_SENHA));
                $objEmail->setConteudo($mensagem, true);
                $objEmail->enviarEmail();

                Helper::imprimirCabecalhoParaFormatarAction();
                Helper::imprimirMensagem(MENSAGEM_EMAIL_REENVIO_SENHA_SUCESSO, MENSAGEM_OK);
                Helper::imprimirComandoJavascriptComTimer(COMANDO_FECHAR_DIALOG, TEMPO_PADRAO_FECHAR_DIALOG, false);

                return;
            }
            elseif ($this->database->rows() == 0)
            {
                Helper::imprimirCabecalhoParaFormatarAction();
                Helper::imprimirMensagem(MENSAGEM_EMAIL_REENVIO_SENHA_ERRO, MENSAGEM_ERRO);
                Helper::imprimirComandoJavascriptComTimer(COMANDO_FECHAR_DIALOG, TEMPO_PADRAO_FECHAR_DIALOG, false);

                return;
            }
        }
        catch (Exception $ex)
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
        }
    }

    public static function lembrarSenhaEGrupo()
    {
        try
        {
            $dadosPost = Helper::getPhpInputObject();
            $pEmailCadastrado = Helper::strtolowerlatin1($dadosPost->email);

            $vObjDatabase = new Database();
            $vObjDatabase->query("SELECT id, nome, email, senha
                                    FROM usuario
                                    WHERE email='{$pEmailCadastrado}'");

            if ($vObjDatabase->rows() == 1)
            {
                $idUsuario = $vObjDatabase->getPrimeiraTuplaDoResultSet(0);
                $nomeUsuario = $vObjDatabase->getPrimeiraTuplaDoResultSet(1);
                $emailDestino = $vObjDatabase->getPrimeiraTuplaDoResultSet(2);
                $senha = $vObjDatabase->getPrimeiraTuplaDoResultSet(3);

                $objCrypt = new Crypt();
                $senhaDescriptografada = $objCrypt->decrypt($senha);

                $mensagem = "
                        --- Lembrete de senha de acesso ao " . TITULO_PAGINAS . " ---\n
                                Nome: {$nomeUsuario}
                                Endereço para Acesso: " . ENDERECO_DE_ACESSO . "
                                Usuário: {$emailDestino}
                                Senha  : {$senhaDescriptografada}
                                                                        ";

                $mensagemUsuarioCorporacao = DatabaseSincronizador::getMensagemUsuarioCorporacao($idUsuario);

                $objEmail = new Email_Sender(EMAIL_PADRAO_REMETENTE_MENSAGENS, $emailDestino);
                $objEmail->setAssunto(ASSUNTO_DO_EMAIL_DE_REENVIO_SENHA);
                $objEmail->setConteudo($mensagem . $mensagemUsuarioCorporacao, false);

                if ($objEmail->enviarEmail())
                {
                    return new Mensagem(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO, I18N::getExpression("Sua senha foi enviada com sucesso."));
                }
                else
                {
                    return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, I18N::getExpression("Falha ao enviar o email, tente novamente mais tarde."));
                }
            }
            elseif ($vObjDatabase->database->rows() == 0)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, I18N::getExpression("O email desejado não está cadastrado no sistema."));
            }
        }
        catch (Exception $ex)
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
        }
    }

    public function alterarEmailAndSenhaUsuario($parameters = null)
    {
        try
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            $msgAlterarEmail = $this->alterarEmailUsuario($parameters);

            //só faz a atualização caso já exista no SICOB
            if($msgAlterarEmail != null && $msgAlterarEmail->mCodRetorno == PROTOCOLO_SISTEMA::USUARIO_JA_EXISTE_NO_SICOB_E_NO_SISTEMA)
            {
                $idUsuario = $parameters->idUsuario;
                $emailNovo = Helper::strtolowerlatin1($parameters->emailNovo);
                $senhaNova = $parameters->senha;

                $objAntigo = new EXTDAO_Usuario($this->database);
                $objAntigo->select($idUsuario);

                $msgEdicao = Servicos_web_ponto_eletronico::trocarEmailDoClientePeloSistema($idUsuario, $objAntigo->getNome(), $emailNovo, $senhaNova);
                if ($msgEdicao != null && !$msgEdicao->ok())
                {
                    $mensagemErroEdicaoSicob = I18N::getExpression("Falha ao executar a edição do usuário. Por favor tente novamente mais tarde, se persistir o problema contacte a central de atendimento");
                    return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, $mensagemErroEdicaoSicob);
                }
                else
                {
                    $mensagemSucesso = I18N::getExpression("E-mail do usuário alterado com sucesso.");
                    return new Mensagem(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO, $mensagemSucesso);
                }
            }

        }
        catch (Exception $ex)
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
        }
    }

    public function alterarEmailUsuario($parameters = null)
    {
        try
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            $idUsuario = $parameters->idUsuario;
            $emailNovo = Helper::strtolowerlatin1($parameters->emailNovo);

            $emailAdministradorDaConta = null;
            if(!Helper::isNullOrEmpty($idUsuario) && !Helper::isNullOrEmpty($emailNovo))
            {
                $msgProtocolo = BO_SICOB::getCaracteristicasDoPacoteDaCorporacao();
                if ($msgProtocolo != null && !$msgProtocolo->ok())
                {
                    $mensagemErroRecuperarCaracteristicasPacote = I18N::getExpression("Falha ao recuperar dados do seu pacote. Por favor tente novamente mais tarde.");
                    return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, $mensagemErroRecuperarCaracteristicasPacote);
                }
                else
                {
                    $protocolo = $msgProtocolo->mObj;
                    $emailAdministradorDaConta = $protocolo->emailUsuarioPrincipal;
                }

                $objAntigo = new EXTDAO_Usuario($this->database);
                $objAntigo->select($idUsuario);

                $emailAntigo = $objAntigo->getEmail();
                if($emailAntigo != $emailAdministradorDaConta)
                {
                    if($emailNovo != $emailAdministradorDaConta)
                    {
                        $idDoUsuarioExistente = $this->getIdDoUsuarioSeExistente($emailNovo);
                        if ($idDoUsuarioExistente != null && $idDoUsuarioExistente != $idUsuario)
                        {
                            $mensagemErroUsuarioJaExistente = I18N::getExpression("O endereço de e-mail desejado já está sendo utilizado por outro usuário, clique em lembrar senha na pagina de login para recurperar o acesso atraves desse email, se for o caso.");
                            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, $mensagemErroUsuarioJaExistente);
                        }
                        else
                        {
                            $msgVerificaoUsuarioSICOB = Servicos_web_ponto_eletronico::verificaExistenciaDoUsuarioNoSicobFromWeb($emailNovo);

                            //caso não exista, prossegue com edição sem alterar a senha
                            if ($msgVerificaoUsuarioSICOB != null && $msgVerificaoUsuarioSICOB->mCodRetorno == PROTOCOLO_SISTEMA::NAO)
                            {
                                $objCrypt = new Crypt();
                                $senhaAntiga = $objCrypt->decrypt($objAntigo->getSenha());

                                $msgEdicao = Servicos_web_ponto_eletronico::trocarEmailDoClientePeloSistema($idUsuario, $objAntigo->getNome(), $emailNovo, $senhaAntiga);
                                if ($msgEdicao != null && !$msgEdicao->ok())
                                {
                                    if($msgEdicao->mCodRetorno == PROTOCOLO_SISTEMA::ACESSO_NEGADO)
                                    {
                                        if(!strlen($msgEdicao->mMensagem)){
                                            $msgEdicao->mMensagem = I18N::getExpression("A troca de email para esse usuário não é permitida!");
                                        }
                                        return new Mensagem(
                                            PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO,
                                            $msgEdicao->mMensagem);
                                    }
                                    $mensagemErroEdicaoSicob = I18N::getExpression("Falha ao executar a edição do usuário. Por favor tente novamente mais tarde.");
                                    return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, $mensagemErroEdicaoSicob);
                                }
                                else
                                {
                                    $mensagemSucesso = I18N::getExpression("E-mail do usuário alterado com sucesso.");
                                    return new Mensagem(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO, $mensagemSucesso);
                                }
                            }
                            //usuário já existe no SICOB, abre campos na view para inserir a senha
                            else
                            {
                                $mensagemErroEdicaoSicob = I18N::getExpression("O usuário já cadastrado, defina nova senha de acesso.");
                                return new Mensagem(PROTOCOLO_SISTEMA::USUARIO_JA_EXISTE_NO_SICOB_E_NO_SISTEMA, $mensagemErroEdicaoSicob);
                            }

                        }
                    }
                    else
                    {
                        $mensagemErroEdicaoSicob = I18N::getExpression("Falha ao alterar e-mail do usuário. O e-mail informado já é utilizado pelo administrador da conta.");
                        return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, $mensagemErroEdicaoSicob);
                    }

                }
                else
                {
                    $mensagemErroEdicaoSicob = I18N::getExpression("Falha ao alterar e-mail do usuário. O usuário administrador da conta não pode alterar o e-mail cadastrado.");
                    return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, $mensagemErroEdicaoSicob);
                }

            }

        }
        catch (Exception $ex)
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
        }

    }

    public function alterarSenha($parameters = null)
    {
        if (is_null($parameters))
        {
            $parameters = new stdClass();
            $parameters->senhaNova = Helper::POSTGET("senhaNova");
            $parameters->senhaAtual = Helper::POST("senhaAtual");
        }

        try
        {
            return Servicos_web_ponto_eletronico::alterarSenhaWeb($parameters->senhaNova, $parameters->senhaAtual);
        }
        catch (Exception $ex)
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
        }
    }

    public function validarEmailUsuario($parameters = null)
    {
        if (is_null($parameters))
        {
            $parameters = Helper::getPhpInputObject();
        }

        try
        {
            if (!Helper::isNullOrEmpty($parameters->email))
            {
                $retornoCorporacao = $this->verificarExistenciaDeUsuarioNaCorporacaoPorEmail($parameters);

                if ($retornoCorporacao->mCodRetorno == PROTOCOLO_SISTEMA::NAO)
                {
                    //TODO-EDUARDO: Remover linha abaixo
                    //return $retornoCorporacao;

                    $retornoSICOB = Servicos_web_ponto_eletronico::verificaExistenciaDoUsuarioNoSicobFromWeb($parameters->email);
                    if($retornoSICOB->mCodRetorno == PROTOCOLO_SISTEMA::SIM)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::SIM, I18N::getExpression("Endereço de e-mail já utilizado."));
                    }
                    else
                    {
                        return $retornoSICOB;
                    }

                }
                else
                {
                    return $retornoCorporacao;
                }
            }
            else
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, I18N::getExpression("O e-mail deve ser informado."));
            }
        }
        catch (Exception $ex)
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
        }
    }

    public function verificarExistenciaDeUsuarioNaCorporacaoPorEmail($parameters)
    {
        if (is_null($parameters))
        {
            $parameters = Helper::getPhpInputObject();
        }

        $parameters->corporacaoId = Seguranca::getIdDaCorporacaoLogada();

        if (!Helper::isNullOrEmpty($parameters->email))
        {
            $query = "SELECT
                            u.nome 
                        FROM
                            usuario u
                            JOIN usuario_corporacao uc ON uc.usuario_id_INT = u.id 
                        WHERE
                            u.email = '{$parameters->email}' 
                            AND uc.corporacao_id_INT = {$parameters->corporacaoId} 
                            ";

            if (!Helper::isNullOrEmpty($parameters->idUsuario))
            {
                $query .= " AND u.id <> {$parameters->idUsuario}";
            }

            $msgDB = $this->database->queryMensagem($query);
            if ($msgDB == null)
            {
                if ($this->database->rows() == 0)
                {
                    return new Mensagem(PROTOCOLO_SISTEMA::NAO, I18N::getExpression("Nao encontrou nenhum registro"));
                }
                else
                {
                    $nomeUsuario = $this->database->getPrimeiraTuplaDoResultSet(0);
                    $mensagemRetorno = I18N::getExpression("Já existe um usuário com e-mail {0}: {1}", $parameters->email, $nomeUsuario);
                    return new Mensagem(PROTOCOLO_SISTEMA::SIM, $mensagemRetorno);
                }
            }
            else
            {
                return $msgDB;
            }
        }
        else
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, I18N::getExpression("Erro durante a validação do e-mail informado."));
        }
    }

    public function getListOfCategoriaPermissaoAndroid($filterParameters = null)
    {
        if (is_null($filterParameters))
        {
            $filterParameters = Helper::getPhpInputObject();
        }

        try
        {
            $filterParameters->corporacaoId = Seguranca::getIdDaCorporacaoLogada();

            $mainTableAlias = "cp";
            $whereClause = EXTDAO_Categoria_permissao::getWhereClauseForFilter($filterParameters, $mainTableAlias);

            $query = "SELECT cp.id AS categoriaPermissaoId, cp.nome AS categoriaPermissaoNome
                        FROM categoria_permissao cp {$whereClause}";

            $fieldTypes = new stdClass();
            $fieldTypes->categoriaPermissaoId = INT;
            $fieldTypes->categoriaPermissaoNome = TEXT;

            $msg = $this->database->queryMensagem($query);
            if ($msg == null) //operacao realizada com sucesso
            {
                if ($this->database->rows() == 0)
                {
                    return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                }

                $resultSet = $this->database->result;
                $resultList = $this->mapGenericResultSetDataToList($resultSet);

                $objReturn = new stdClass();
                $objReturn->dataSet = $this->getResponseObjectWithTypes($resultList, $fieldTypes);

                return new Mensagem_generica($objReturn);
            }
            else
            {
                return $msg;
            }
        }
        catch (Exception $ex)
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
        }
    }

    public function getListOfServicosAndroid($filterParameters = null)
    {
        try
        {
            if (isset($filterParameters->idUsuario) && !is_null($filterParameters->idUsuario))
            {
                $query = "SELECT s.id AS servicoId, s.nome AS servicoNome, us.status_BOOLEAN AS usuarioServicoStatus
                              FROM servico AS s 
                                LEFT OUTER JOIN usuario_servico AS us ON us.servico_id_INT=s.id 
                              WHERE us.usuario_id_INT={$filterParameters->idUsuario}";
            }
            else
            {
                $query = "SELECT s.id AS servicoId, s.nome AS servicoNome, null AS usuarioServicoStatus FROM servico AS s";
            }

            $fieldTypes = new stdClass();
            $fieldTypes->servicoId = INT;
            $fieldTypes->servicoNome = TEXT;
            $fieldTypes->usuarioServicoStatus = BOOLEAN;

            $msg = $this->database->queryMensagem($query);
            if ($msg == null) //operacao realizada com sucesso
            {
                if ($this->database->rows() == 0)
                {
                    return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Não encontrou nenhum registro"));
                }

                $resultSet = $this->database->result;

                $objReturn = new stdClass();
                $objReturn = static::mapGenericResultSetDataToList($resultSet);

                return new Mensagem_generica($objReturn);
            }
            else
            {
                return $msg;
            }
        }
        catch (Exception $ex)
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
        }
    }

    public function __getAllInitialDataForUsuarioForm()
    {
        try
        {
            $objRetorno = new stdClass();
            $objRetorno->listaDeItensDoMenuSistemaWeb = $this->getListOfItemMenuSistemaWeb()->mObj;
            $objRetorno->listaDeServicosAndroid = $this->getListOfServicosAndroid()->mObj;
            $objRetorno->comboBoxesData = $this->getFormComboBoxesData();

            static::appendTypesProperty($objRetorno, static::$databaseFieldTypes);
            return new Mensagem_generica($objRetorno, PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO);
        }
        catch (Exception $ex)
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
        }
    }

    public function getDadosCompletosDoUsuarioAndEstruturasAdicionais($idUsuario)
    {
        try
        {
            $msgDadosUsuario = $this->getDadosCompletosDoUsuario($idUsuario);

            //caso tenha recuperado o usuário com sucesso, carrega também os dados adicionais
            if ($msgDadosUsuario != null && $msgDadosUsuario->mCodRetorno == PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO)
            {
                $dadosCadastraisUsuario = $msgDadosUsuario->mObj;
                $filterParametersDadosAdicionais = new stdClass();
                $filterParametersDadosAdicionais->idUsuario = $dadosCadastraisUsuario->id;

                if (Helper::isNullOrEmpty($dadosCadastraisUsuario->isAtivo))
                {
                    $dadosCadastraisUsuario->isAtivo = false;
                }

                if (Helper::isNullOrEmpty($dadosCadastraisUsuario->isAdministradorAndroid))
                {
                    $dadosCadastraisUsuario->isAdministradorAndroid = false;
                }

                $objRetorno = new stdClass();
                $objRetorno->formData = $dadosCadastraisUsuario;
                $objRetorno->listaDeItensDoMenuSistemaWeb = $this->getListOfItemMenuSistemaWeb()->mObj;
                $objRetorno->listaDeItensDoMenuSistemaWebDoUsuario = $this->getListOfAreasDoMenuWebDoUsuario($idUsuario);
                $objRetorno->listaDeServicosAndroid = $this->getListOfServicosAndroid()->mObj;
                $objRetorno->listaDeServicosAndroidDoUsuario = $this->getListOfServicosAndroid($filterParametersDadosAdicionais)->mObj;

                $objRetorno->comboBoxesData = $this->getFormComboBoxesData($dadosCadastraisUsuario);

                return new Mensagem_generica($objRetorno, PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO);
            }
            else
            {
                return $msgDadosUsuario;
            }
        }
        catch (Exception $ex)
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
        }
    }

    public function getDadosCompletosDoUsuario($idUsuario)
    {
//        if (is_null($filterParameters))
//        {
//            $filterParameters = Helper::getPhpInputObject();
//        }

        try
        {
            $emailAdministradorDaConta = null;
            $msgProtocolo = BO_SICOB::getCaracteristicasDoPacoteDaCorporacao();
            if ($msgProtocolo != null && !$msgProtocolo->ok())
            {
                HelperLog::logErro(null, $msgProtocolo, "Falha durante a criação do usuário, ao consultar os dados do pacote da corporacao");
                $mensagemErroRecuperarCaracteristicasPacote = I18N::getExpression("Falha ao recuperar dados do usuário. Por favor tente novamente mais tarde.");
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, $mensagemErroRecuperarCaracteristicasPacote);
                //return $msgProtocolo;
            }
            else
            {
                $protocolo = $msgProtocolo->mObj;
                $emailAdministradorDaConta = $protocolo->emailUsuarioPrincipal;
            }


            $idCorporacao = Seguranca::getIdDaCorporacaoLogada();

            $query = "SELECT 
                     u.id AS id,
                     u.nome AS nome,
                     u.email AS email,
                     u.status_BOOLEAN AS isAtivo,
                     
                     uc.id AS usuarioCorporacaoId,
                     uc.is_adm_BOOLEAN AS isAdministradorAndroid,
                     0 isAdministradorWeb,
                     utc.usuario_tipo_id_INT AS usuarioTipoId,
                     ucp.categoria_permissao_id_INT AS categoriaPermissaoId,
                     ue.empresa_id_INT AS empresaId,
                     
                     p.telefone AS pessoaTelefone,
                     p.celular AS pessoaCelular,
                     p.sexo_id_INT AS pessoaSexoId,
                     p.tipo_documento_id_INT AS pessoaTipoDocumentoId,
                     p.numero_documento AS pessoaNumeroDocumento,
                     p.logradouro AS pessoaLogradouro,
                     p.numero AS pessoaNumero,
                     p.complemento AS pessoaComplemento,                         
                     p.bairro_id_INT AS pessoaBairroId,                         
                     p.cidade_id_INT AS pessoaCidadeId                         
                        FROM usuario AS u
                            JOIN usuario_corporacao AS uc ON uc.usuario_id_INT=u.id  AND uc.status_BOOLEAN=1 AND uc.corporacao_id_INT={$idCorporacao}     
                            LEFT JOIN usuario_empresa AS ue ON ue.usuario_id_INT=u.id AND ue.corporacao_id_INT={$idCorporacao}    
                            LEFT JOIN pessoa_usuario AS pu ON pu.usuario_id_INT=u.id AND pu.corporacao_id_INT={$idCorporacao}  
                            LEFT JOIN pessoa AS p ON p.id=pu.pessoa_id_INT
                            LEFT JOIN usuario_tipo_corporacao utc ON u.id=utc.usuario_id_INT AND utc.corporacao_id_INT={$idCorporacao}               
                            LEFT JOIN usuario_categoria_permissao ucp ON u.id=ucp.usuario_id_INT AND utc.corporacao_id_INT={$idCorporacao}                         
                        WHERE u.id={$idUsuario}";

            $fieldTypes = new stdClass();

            //usuário e permissões
            $fieldTypes->id = static::TIPO_VARIAVEL_INTEGER;
            $fieldTypes->nome = static::TIPO_VARIAVEL_TEXT;
            $fieldTypes->email = static::TIPO_VARIAVEL_TEXT;
            $fieldTypes->isAtivo = static::TIPO_VARIAVEL_BOOLEAN;
            $fieldTypes->usuarioCorporacaoId = static::TIPO_VARIAVEL_INTEGER;
            $fieldTypes->isAdministradorAndroid = static::TIPO_VARIAVEL_BOOLEAN;
            $fieldTypes->isAdministradorWeb = static::TIPO_VARIAVEL_BOOLEAN;
            $fieldTypes->usuarioTipoId = static::TIPO_VARIAVEL_INTEGER;
            $fieldTypes->categoriaPermissaoId = static::TIPO_VARIAVEL_INTEGER;
            $fieldTypes->empresaId = static::TIPO_VARIAVEL_INTEGER;
            $fieldTypes->isAdministradorGlobalDaConta = static::TIPO_VARIAVEL_BOOLEAN;

            //pessoa associada (se existir)
            $fieldTypes->pessoaTelefone = static::TIPO_VARIAVEL_TEXT;
            $fieldTypes->pessoaCelular = static::TIPO_VARIAVEL_TEXT;
            $fieldTypes->pessoaSexoId = static::TIPO_VARIAVEL_INTEGER;
            $fieldTypes->pessoaTipoDocumentoId = static::TIPO_VARIAVEL_INTEGER;
            $fieldTypes->pessoaNumeroDocumento = static::TIPO_VARIAVEL_TEXT;
            $fieldTypes->pessoaLogradouro = static::TIPO_VARIAVEL_TEXT;
            $fieldTypes->pessoaNumero = static::TIPO_VARIAVEL_TEXT;
            $fieldTypes->pessoaComplemento = static::TIPO_VARIAVEL_TEXT;
            $fieldTypes->pessoaBairroId = static::TIPO_VARIAVEL_INTEGER;
            $fieldTypes->pessoaCidadeId = static::TIPO_VARIAVEL_INTEGER;

            $msg = $this->database->queryMensagem($query);

            //operacao realizada com sucesso
            if ($msg == null)
            {
                if ($this->database->rows() == 0)
                {
                    return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Não foi encontrado nenhum registro de usuário correspondente."));
                }

                $resultSet = $this->database->result;
                $resultObject = static::mapGenericResultSetDataToRecord($resultSet, $fieldTypes);
                $resultObject->isAdministradorGlobalDaConta = ($resultObject->email == $emailAdministradorDaConta);

                return new Mensagem_generica($resultObject, PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO);
            }
            else
            {
                return $msg;
            }
        }
        catch (Exception $ex)
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
        }
    }

    public function getListOfItemMenuSistemaWeb()
    {
        try
        {
            $arrRetorno = array();
            $objMenu = new Menu();
            $arrConteudoMenu = $objMenu->getArrayDoConteudoDoMenuCompleto();

            static::montarEstuturaDoMenuParaCadastroDoUsuario( $arrConteudoMenu, $arrRetorno);
            return new Mensagem_generica($arrRetorno);
        }
        catch (Exception $ex)
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
        }
    }

    public static function montarEstuturaDoMenuParaCadastroDoUsuario($itemMenu, &$estruturaRetorno, $nivel = 1, $chaveDoPai = null, $chaveDoAvo = null)
    {
        if($nivel == 1)
        {
            $chaveDoPai = "MENU_COMPLETO";
            $raizMenu = new stdClass();
            $raizMenu->descricaoItem = I18N::getExpression("Menu Completo");
            $raizMenu->chaveItem = $chaveDoPai;
            $raizMenu->itemPai = null;
            $raizMenu->nivel = 0;
            $estruturaRetorno[] = $raizMenu;
        }

        foreach ($itemMenu as $chaveDoItem => $subItemMenu)
        {
            if ($chaveDoItem == "config")
            {
                $itemMenuRetorno = new stdClass();
                $itemMenuRetorno->descricaoItem = I18N::getExpression($subItemMenu->getLabel());
                $itemMenuRetorno->chaveItem = $chaveDoPai;
                $itemMenuRetorno->itemPai = $chaveDoAvo;
                $itemMenuRetorno->nivel = $nivel-1;
                $estruturaRetorno[] = $itemMenuRetorno;
            }
            else if (is_array($subItemMenu))
            {
                static::montarEstuturaDoMenuParaCadastroDoUsuario( $subItemMenu, $estruturaRetorno, $nivel + 1, $chaveDoItem, $chaveDoPai);
            }
            else
            {
                $itemMenuRetorno = new stdClass();
                $itemMenuRetorno->descricaoItem = I18N::getExpression($subItemMenu->getLabel());
                $itemMenuRetorno->chaveItem = $chaveDoItem;
                $itemMenuRetorno->itemPai = $chaveDoPai;
                $itemMenuRetorno->nivel = $nivel;
                $estruturaRetorno[] = $itemMenuRetorno;
            }
        }

        if($nivel == 1)
        {
            Helper::corrigirObjetoParaJsonEncode($estruturaRetorno);
        }

    }

    public function getListOfAreasDoMenuWebDoUsuario($idUsuario)
    {
        try
        {

            $query = "SELECT area_menu AS usuarioMenuArea
                        FROM usuario_menu AS um
                        WHERE um.usuario_id_INT={$idUsuario}";

//                $fieldTypes = new stdClass();
//                $fieldTypes->usuarioMenuArea = TEXT;
            $msg = $this->database->queryMensagem($query);

            if ($msg == null)
            {
                $resultSet = $this->database->result;
                $areasDoUsuarioComPermissao = $this->mapGenericResultSetDataToList($resultSet);

                $arrChavesAreasDoUsuarioComPermissao = array();
                for ($i = 0; $i < count($areasDoUsuarioComPermissao); $i++)
                {
                    $arrChavesAreasDoUsuarioComPermissao[] = $areasDoUsuarioComPermissao[$i]->usuarioMenuArea;
                }

                $msgMenuCompleto = $this->getListOfItemMenuSistemaWeb();

                $arrRetorno = array();
                if ($msgMenuCompleto->ok())
                {
                    for ($i = 0; $i < count($msgMenuCompleto->mObj); $i++)
                    {
                        $itemMenuAtual = $msgMenuCompleto->mObj[$i];
                        $chaveItem = $itemMenuAtual->chaveItem;

                        if (in_array($chaveItem, $arrChavesAreasDoUsuarioComPermissao))
                        {
                            $arrRetorno[] = clone $itemMenuAtual;
                        }
                    }

                    return new Mensagem_generica($arrRetorno);
                }
                else
                {
                    return $msgMenuCompleto;
                }
            }
            else
            {
                return $msg;
            }

        }
        catch (Exception $ex)
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
        }
    }

    public function getUsuarioTipoDoUsuario($pIdCorporacao = null)
    {
        try
        {
            $vIdUsuario = $this->getId();
            if ($pIdCorporacao == null)
            {
                $pIdCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }

            if (!strlen($pIdCorporacao))
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, I18N::getExpression("Corporação inválida."));
            }

            if (strlen($vIdUsuario))
            {
                $this->database->query("SELECT DISTINCT ut.id AS usuarioTipoId
                                            FROM usuario_tipo ut JOIN usuario_tipo_corporacao utc ON ut.id = utc.usuario_tipo_id_INT
                                            WHERE utc.corporacao_id_INT = {$pIdCorporacao} AND 
                                                utc.usuario_id_INT = {$vIdUsuario}");

                return $this->database->getPrimeiraTuplaDoResultSet("usuarioTipoId");
            }
            else
            {
                return null;
            }
        }
        catch (Exception $ex)
        {
            return null;
        }
    }

    public function getCategoriaPermissao()
    {
        try
        {
            $vIdUsuario = $this->getId();
            $vIdCorporacao = Seguranca::getIdDaCorporacaoLogada();
            if (strlen($vIdUsuario))
            {
                $this->database->query("SELECT ucp.categoria_permissao_id_INT AS categoriaPermissaoId
                                            FROM usuario_categoria_permissao ucp
                                            WHERE ucp.usuario_id_INT = {$vIdUsuario} AND
                                                ucp.corporacao_id_INT = {$vIdCorporacao}");

                if ($this->database->rows() > 0)
                {
                    return $this->database->getPrimeiraTuplaDoResultSet("categoriaPermissaoId");
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }
        catch (Exception $ex)
        {
            return null;
        }
    }

    public static function getCaracteristicasDoPacote()
    {
        try
        {
            $mensagem = BO_SICOB::getCaracteristicasDoPacoteDaCorporacao();
            if ($mensagem == null || !$mensagem->ok())
            {
                $mensagemRetorno = I18N::getExpression("Falha durante a verificação das características do pacote do seu grupo. Por favor tente novamente mais tarde.");

                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, $mensagemRetorno);
            }

            return $mensagem;
        }
        catch (Exception $ex)
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
        }
    }

    public function getIdDoUsuarioSeExistente($pEmail)
    {

        if (!strlen($pEmail))
        {
            return null;
        }

        $pEmail = Helper::strtolowerlatin1($pEmail);
        $this->database->query("SELECT u.id 
                                        FROM usuario u 
                                        WHERE u.email = '{$pEmail}'");

        $idUsuario = $this->database->getPrimeiraTuplaDoResultSet(0);
        if (strlen($idUsuario))
        {
            return $idUsuario;
        }
        else
        {
            return null;
        }
    }

    public function getIdDoUsuarioCorporacaoSeExistente($pIdUsuario)
    {
        $vIdCorporacao = Seguranca::getIdDaCorporacaoLogada();
        $this->database->query("SELECT uc.id usuario_corporacao_id AS usuarioCorporacaoId
                                        FROM usuario u LEFT JOIN usuario_corporacao uc ON u.id = uc.usuario_id_INT
                                        WHERE uc.corporacao_id_INT = {$vIdCorporacao} AND 
                                            u.id = {$pIdUsuario}");

        $vIdUsuarioCorporacao = $this->database->getPrimeiraTuplaDoResultSet("usuarioCorporacaoId");
        if (strlen($vIdUsuarioCorporacao))
        {
            return $vIdUsuarioCorporacao;
        }
        else
        {
            return null;
        }
    }

    public function inicializaObjetoPessoa()
    {

        $dadosPost = Helper::getPhpInputObject();

        $objPessoa = new EXTDAO_Pessoa($this->database);
        $objPessoa->setAllFieldsToHumanFormat();

        $objPessoa->setNome($dadosPost->nome);
        $objPessoa->setEmail($dadosPost->email);

        $objPessoa->setSexoId($dadosPost->pessoaSexoId);
        $objPessoa->setTelefone($dadosPost->pessoaTelefone);
        $objPessoa->setCelular($dadosPost->pessoaCelular);
        $objPessoa->setTipoDocumentoId($dadosPost->pessoaTipoDocumentoId);
        $objPessoa->setNumeroDocumento($dadosPost->pessoaNumeroDocumento);

        $objPessoa->setLogradouro($dadosPost->pessoaLogradouro);
        $objPessoa->setNumero($dadosPost->pessoaNumero);
        $objPessoa->setComplemento($dadosPost->pessoaComplemento);

        $objPessoa->setCidadeId($dadosPost->pessoaCidadeId);
        $objPessoa->setBairroId($dadosPost->pessoaBairroId);

        $objPessoa->formatarParaSQL();
        return $objPessoa;
    }

    public function getIdDaPessoaAssociada()
    {

        $objBanco = new Database();
        $objBanco->query("SELECT pessoa_id_INT AS pessoaId FROM pessoa_usuario WHERE usuario_id_INT={$this->getId()}");

        if ($objBanco->rows() > 0)
        {
            $idPessoa = $objBanco->getPrimeiraTuplaDoResultSet("pessoaId");
        }
        else
        {
            $idPessoa = null;
        }

        return $idPessoa;
    }

    public function gravarRegistroUsuarioPessoa($idPessoaAssociada)
    {
        try
        {
            $objPessoaUsuario = new EXTDAO_Pessoa_usuario($this->database);
            $objBanco = new Database();
            $objBanco->query("SELECT id AS pessoaUsuarioId FROM pessoa_usuario WHERE usuario_id_INT={$this->getId()}");

            if ($objBanco->rows() > 0)
            {
                $idPessoaUsuario = $objBanco->getPrimeiraTuplaDoResultSet("pessoaUsuarioId");
            }

            if (is_numeric($idPessoaAssociada))
            {
                if (is_numeric($idPessoaUsuario))
                {
                    $objPessoaUsuario->setUsuario_id_INT($this->getId());
                    $objPessoaUsuario->setPessoa_id_INT($idPessoaAssociada);
                    $objPessoaUsuario->formatarParaSQL();

                    $msgUpdate = $objPessoaUsuario->update($idPessoaUsuario, true);
                    if ($msgUpdate != null && !$msgUpdate->ok())
                    {
                        return $msgUpdate;
                    }
                }
                else
                {
                    $objPessoaUsuario->setUsuario_id_INT($this->getId());
                    $objPessoaUsuario->setPessoa_id_INT($idPessoaAssociada);
                    $objPessoaUsuario->formatarParaSQL();

                    $msgInsert = $objPessoaUsuario->insert(true);
                    if ($msgInsert != null && !$msgInsert->ok())
                    {
                        return $msgInsert;
                    }
                }
            }
            elseif (is_numeric($idPessoaUsuario))
            {
                $msgDelete = $objPessoaUsuario->delete($idPessoaUsuario, true);
                if ($msgDelete != null && !$msgDelete->ok())
                {
                    return $msgDelete;
                }
            }
        }
        catch (Exception $ex)
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
        }

        return new Mensagem(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO, null);
    }

    public function gravarCorporacaoDoUsuarioSeNecessario($pIdUsuario)
    {
        try
        {
            if (!strlen($pIdUsuario))
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, I18N::getExpression("Usuário inválido."));
            }

            $vIdCorporacao = Seguranca::getIdDaCorporacaoLogada();
            $obj = new EXTDAO_Usuario_corporacao($this->database);

            if (!strlen($vIdCorporacao))
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, I18N::getExpression("Corporação inválida."));
            }

            $vDatabase = new Database();
            $vDatabase->query("SELECT id 
                                       FROM usuario_corporacao
                                       WHERE usuario_id_INT = {$pIdUsuario} AND 
                                       corporacao_id_INT = {$vIdCorporacao}");

            $vIdUsuarioCorporacao = $vDatabase->getPrimeiraTuplaDoResultSet("id");

            if (!strlen($vIdUsuarioCorporacao))
            {
                $obj->setUsuario_id_INT($pIdUsuario);
                $obj->setCorporacao_id_INT($vIdCorporacao);

                $obj->setStatus_BOOLEAN("1");
                $obj->setIs_adm_BOOLEAN("0");
                $obj->formatarParaSQL();

                $msgInsert = $obj->insert(true);
                if ($msgInsert != null && !$msgInsert->ok())
                {
                    return $msgInsert;
                }
            }
            else
            {
                $obj->select($vIdUsuarioCorporacao);
                $obj->setStatus_BOOLEAN("1");
                $obj->formatarParaSQL();

                $msgUpdate = $obj->update($vIdUsuarioCorporacao, true);
                if ($msgUpdate != null && !$msgUpdate->ok())
                {
                    return $msgUpdate;
                }
            }
        }
        catch (Exception $ex)
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
        }

        return new Mensagem(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO, null);
    }

    public function gravarPermissoesDeAcesso($pIdUsuario = false)
    {
        try
        {
            $dadosPost = Helper::getPhpInputObject();

            if (!$pIdUsuario)
            {
                $pIdUsuario = $this->getId();
            }

            if (!strlen($pIdUsuario))
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, I18N::getExpression("Usuário inválido."));
            }

            $vIdCorporacao = Seguranca::getIdDaCorporacaoLogada();
            if (!strlen($vIdCorporacao))
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, I18N::getExpression("Corporação inválida."));
            }

            $this->database->query("SELECT id
                                            FROM usuario_privilegio 
                                            WHERE usuario_id_INT={$pIdUsuario} AND
                                            corporacao_id_INT={$vIdCorporacao}");

            $vVetorIdUsuarioPrivilegio = Helper::getResultSetToArrayDeUmCampo($this->database->result);
            foreach ($vVetorIdUsuarioPrivilegio as $vIdUsuarioPrivilegio)
            {
                $vObjUsuarioPrivilegio = new EXTDAO_Usuario_privilegio($this->database);
                $vObjUsuarioPrivilegio->delete($vIdUsuarioPrivilegio, true);
            }

            $arrayDePermissoes = $dadosPost->funcionalidades;
            foreach ($arrayDePermissoes as $valor)
            {
                $vObjUsuarioPrivilegio = new EXTDAO_Usuario_privilegio($this->database);
                $vObjUsuarioPrivilegio->setUsuario_id_INT($pIdUsuario);
                $vObjUsuarioPrivilegio->setIdentificador_funcionalidade($valor);
                $vObjUsuarioPrivilegio->setCorporacao_id_INT(Seguranca::getIdDaCorporacaoLogada());
                $vObjUsuarioPrivilegio->formatarParaSQL();

                $msgInsert = $vObjUsuarioPrivilegio->insert(true);
                if ($msgInsert != null && !$msgInsert->ok())
                {
                    return $msgInsert;
                }
            }
        }
        catch (Exception $ex)
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
        }

        return new Mensagem(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO, null);
    }

    public function gravarTipoDeUsuarioDoUsuario($pIsEdit, $pIdUsuario, $pIdTipoUsuario)
    {
        try
        {
            if (!strlen($pIdUsuario))
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, I18N::getExpression("Usuário inválido."));
            }

            if (!strlen($pIdTipoUsuario))
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, I18N::getExpression("Tipo de usuário inválido."));
            }

            $vIdCorporacao = Seguranca::getIdDaCorporacaoLogada();
            if (!strlen($vIdCorporacao))
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, I18N::getExpression("Corporação inválida."));
            }

            if (!$pIsEdit)
            {
                $objUsuarioTipoUsuario = new EXTDAO_Usuario_tipo_corporacao($this->database);
                $objUsuarioTipoUsuario->setUsuario_id_INT($pIdUsuario);
                $objUsuarioTipoUsuario->setUsuario_tipo_id_INT($pIdTipoUsuario);
                $objUsuarioTipoUsuario->setCorporacao_id_INT($vIdCorporacao);
                $objUsuarioTipoUsuario->formatarParaSQL();

                $msgInsert = $objUsuarioTipoUsuario->insert(true);
                if ($msgInsert != null && !$msgInsert->ok())
                {
                    return $msgInsert;
                }
            }
            else
            {
                $objBanco = new Database();
                $objBanco->queryMensagem("SELECT uct.id AS usuarioTipoCorporacaoId
                              FROM usuario_tipo_corporacao uct
                              WHERE uct.corporacao_id_INT = {$vIdCorporacao}
                              AND uct.usuario_id_INT = {$pIdUsuario}");

                $vIdUsuarioTipoCorporacao = $objBanco->getPrimeiraTuplaDoResultSet("usuarioTipoCorporacaoId");
                $objUsuarioTipoCorporacao = new EXTDAO_Usuario_tipo_corporacao($this->database);

                if (strlen($vIdUsuarioTipoCorporacao))
                {
                    $objUsuarioTipoCorporacao->select($vIdUsuarioTipoCorporacao);
                    if (!strlen($pIdTipoUsuario))
                    {
                        $msgDelete = $objUsuarioTipoCorporacao->delete($vIdUsuarioTipoCorporacao, true);
                        if ($msgDelete != null && !$msgDelete->ok())
                        {
                            return $msgDelete;
                        }
                    }
                    else
                    {
                        $objUsuarioTipoCorporacao->setUsuario_tipo_id_INT($pIdTipoUsuario);
                        $objUsuarioTipoCorporacao->formatarParaSQL();

                        $this->usuarioTipoId = $vIdUsuarioTipoCorporacao;

                        $msgUpdate = $objUsuarioTipoCorporacao->update($vIdUsuarioTipoCorporacao, true);
                        if ($msgUpdate != null && !$msgUpdate->ok())
                        {
                            return $msgUpdate;
                        }
                    }
                }
                else
                {
                    $objUsuarioTipoCorporacao->setUsuario_tipo_id_INT($pIdTipoUsuario);
                    $objUsuarioTipoCorporacao->setUsuario_id_INT($pIdUsuario);
                    $objUsuarioTipoCorporacao->setCorporacao_id_INT(Seguranca::getIdDaCorporacaoLogada());
                    $objUsuarioTipoCorporacao->formatarParaSQL();

                    $msgInsert = $objUsuarioTipoCorporacao->insert(true);
                    if ($msgInsert != null && !$msgInsert->ok())
                    {
                        return $msgInsert;
                    }
                }
            }
        }
        catch (Exception $ex)
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
        }

        return new Mensagem(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO, null);
    }

    public function gravarEmpresaDoUsuario($pIsEdit, $pIdUsuario, $pIdEmpresa)
    {
        try
        {
            if (!strlen($pIdUsuario))
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, I18N::getExpression("Usuário inválido."));
            }

            $vIdCorporacao = Seguranca::getIdDaCorporacaoLogada();
            if (!strlen($vIdCorporacao))
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, I18N::getExpression("Corporação inválida."));
            }

            $objBanco = new Database();
            $objBanco->query("SELECT ue.id AS usuarioEmpresaId
                          FROM usuario_empresa AS ue
                          WHERE ue.corporacao_id_INT = {$vIdCorporacao}
                          AND ue.usuario_id_INT = {$pIdUsuario}");

            $vIdUsuarioEmpresaAntiga = $objBanco->rows() > 0 ? $objBanco->getPrimeiraTuplaDoResultSet("usuarioEmpresaId") : null;

            if (!$pIsEdit)
            {
                if (is_numeric($pIdEmpresa))
                {
                    $objUsuarioEmpresa = new EXTDAO_Usuario_empresa($this->database);
                    $objUsuarioEmpresa->setUsuario_id_INT($pIdUsuario);
                    $objUsuarioEmpresa->setEmpresa_id_INT($pIdEmpresa);
                    $objUsuarioEmpresa->setCorporacao_id_INT(Seguranca::getIdDaCorporacaoLogada());
                    $objUsuarioEmpresa->formatarParaSQL();

                    $msgInsert = $objUsuarioEmpresa->insert(true);
                    if ($msgInsert != null && !$msgInsert->ok())
                    {
                        return $msgInsert;
                    }
                }
            }
            else
            {
                $objUsuarioEmpresa = new EXTDAO_Usuario_empresa($this->database);
                if (strlen($vIdUsuarioEmpresaAntiga))
                {
                    if (is_numeric($pIdEmpresa))
                    {
                        $objUsuarioEmpresa->select($vIdUsuarioEmpresaAntiga);
                        $objUsuarioEmpresa->setEmpresa_id_INT($pIdEmpresa);
                        $objUsuarioEmpresa->formatarParaSQL();

                        $this->empresaId = $pIdEmpresa;

                        $msgUpdate = $objUsuarioEmpresa->update($vIdUsuarioEmpresaAntiga, true);
                        if ($msgUpdate != null && !$msgUpdate->ok())
                        {
                            return $msgUpdate;
                        }
                    }
                    else
                    {
                        $msgDelete = $objUsuarioEmpresa->delete($vIdUsuarioEmpresaAntiga, true);
                        if ($msgDelete != null && !$msgDelete->ok())
                        {
                            return $msgDelete;
                        }
                    }
                }
                else
                {
                    if (is_numeric($pIdEmpresa))
                    {
                        $objUsuarioEmpresa->setEmpresa_id_INT($pIdEmpresa);
                        $objUsuarioEmpresa->setUsuario_id_INT($pIdUsuario);
                        $objUsuarioEmpresa->setCorporacao_id_INT(Seguranca::getIdDaCorporacaoLogada());
                        $objUsuarioEmpresa->formatarParaSQL();

                        $msgInsert = $objUsuarioEmpresa->insert(true);
                        if ($msgInsert != null && !$msgInsert->ok())
                        {
                            return $msgInsert;
                        }
                    }
                }
            }
        }
        catch (Exception $ex)
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
        }

        return new Mensagem(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO, null);
    }

    public function gravarIsAdministradorSistemaAndroid($pIdUsuario, $pIsAdmAndroid)
    {
        try
        {
            $vIdCorporacao = Seguranca::getIdDaCorporacaoLogada();
            if (!strlen($vIdCorporacao))
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, I18N::getExpression("Corporação inválida."));
            }

            $objBanco = new Database();
            $objBanco->query("SELECT id 
                            FROM usuario_corporacao 
                            WHERE corporacao_id_INT={$vIdCorporacao}
                            AND usuario_id_INT={$pIdUsuario}");

            if ($objBanco->rows() > 0)
            {
                while ($dados = $objBanco->fetchArray())
                {
                    $idUsuarioCorporacao = $dados[0];
                    $objUsuarioCorporacao = new EXTDAO_Usuario_corporacao($this->database);
                    $objUsuarioCorporacao->select($idUsuarioCorporacao);
                    $objUsuarioCorporacao->setIs_adm_BOOLEAN($pIsAdmAndroid);

                    $objUsuarioCorporacao->formatarParaSQL();
                    $objUsuarioCorporacao->update($idUsuarioCorporacao, true);
                }
            }
            else
            {
                $objUsuarioCorporacao = new EXTDAO_Usuario_corporacao($this->database);
                $objUsuarioCorporacao->setUsuario_id_INT($pIdUsuario);
                $objUsuarioCorporacao->setIs_adm_BOOLEAN($pIsAdmAndroid);

                $objUsuarioCorporacao->formatarParaSQL();

                $msgInsert = $objUsuarioCorporacao->insert();
                if ($msgInsert != null && !$msgInsert->ok())
                {
                    return $msgInsert;
                }
            }
        }
        catch (Exception $ex)
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
        }

        return new Mensagem(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO, null);
    }

    public function gravarIsAdministradorSistemaWeb($pIdUsuario, $pIsAdmWeb)
    {
        return new Mensagem(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO, null);
        try
        {
            $vIdCorporacao = Seguranca::getIdDaCorporacaoLogada();
            if (!strlen($vIdCorporacao))
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, I18N::getExpression("Corporação inválida."));
            }

            $objBanco = new Database();
            $objBanco->query("SELECT id 
                            FROM usuario_corporacao 
                            WHERE corporacao_id_INT={$vIdCorporacao}
                            AND usuario_id_INT={$pIdUsuario}");

            if ($objBanco->rows() > 0)
            {
                while ($dados = $objBanco->fetchArray())
                {
                    $idUsuarioCorporacao = $dados[0];
                    $objUsuarioCorporacao = new EXTDAO_Usuario_corporacao($this->database);
                    $objUsuarioCorporacao->select($idUsuarioCorporacao);
                    $objUsuarioCorporacao->setIs_adm_BOOLEAN($pIsAdmWeb);

                    $objUsuarioCorporacao->formatarParaSQL();
                    $objUsuarioCorporacao->update($idUsuarioCorporacao, true);
                }
            }
            else
            {
                $objUsuarioCorporacao = new EXTDAO_Usuario_corporacao($this->database);
                $objUsuarioCorporacao->setUsuario_id_INT($pIdUsuario);
                $objUsuarioCorporacao->setIs_adm_BOOLEAN($pIsAdmWeb);

                $objUsuarioCorporacao->formatarParaSQL();

                $msgInsert = $objUsuarioCorporacao->insert();
                if ($msgInsert != null && !$msgInsert->ok())
                {
                    return $msgInsert;
                }
            }
        }
        catch (Exception $ex)
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
        }

        return new Mensagem(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO, null);
    }

    public function gravarCategoriaPermissaoDoUsuario($pIsEdit, $pIdUsuario, $pIdCategoriaPermissao)
    {
        try
        {
            if (!strlen($pIdUsuario))
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, I18N::getExpression("Usuário inválido."));
            }

            if (!$pIsEdit && strlen($pIdCategoriaPermissao))
            {
                $objUsuarioCategoriaPermissao = new EXTDAO_Usuario_categoria_permissao($this->database);
                $objUsuarioCategoriaPermissao->setUsuario_id_INT($pIdUsuario);
                $objUsuarioCategoriaPermissao->setCategoria_permissao_id_INT($pIdCategoriaPermissao);
                $objUsuarioCategoriaPermissao->setCorporacao_id_INT(Seguranca::getIdDaCorporacaoLogada());
                $objUsuarioCategoriaPermissao->formatarParaSQL();

                $msgInsert = $objUsuarioCategoriaPermissao->insert(true);
                if ($msgInsert != null && !$msgInsert->ok())
                {
                    return $msgInsert;
                }
            }
            else
            {
                $vIdCorporacao = Seguranca::getIdDaCorporacaoLogada();
                $objBanco = new Database();
                $objBanco->query("SELECT uct.id AS id, categoria_permissao_id_INT AS categoria_permissao
                              FROM usuario_categoria_permissao uct
                              WHERE uct.corporacao_id_INT={$vIdCorporacao}
                              AND uct.usuario_id_INT={$pIdUsuario}");

                if ($objBanco->rows() > 0)
                {
                    $vIdUsuarioCategoriaPermissao = $objBanco->getPrimeiraTuplaDoResultSet("id");
                }

                $objUsuarioCategoriaPermissao = new EXTDAO_Usuario_categoria_permissao($this->database);
                if (!is_null($vIdUsuarioCategoriaPermissao) && is_numeric($vIdUsuarioCategoriaPermissao))
                {
                    $objUsuarioCategoriaPermissao->select($vIdUsuarioCategoriaPermissao);

                    if (!strlen($pIdCategoriaPermissao))
                    {
                        $msgDelete = $objUsuarioCategoriaPermissao->delete($vIdUsuarioCategoriaPermissao, true);
                        if ($msgDelete != null && !$msgDelete->ok())
                        {
                            return $msgDelete;
                        }
                    }
                    else
                    {
                        $objUsuarioCategoriaPermissao->setCategoria_permissao_id_INT($pIdCategoriaPermissao);
                        $objUsuarioCategoriaPermissao->formatarParaSQL();

                        $msgUpdate = $objUsuarioCategoriaPermissao->update($vIdUsuarioCategoriaPermissao, true);
                        if ($msgUpdate != null && !$msgUpdate->ok())
                        {
                            return $msgUpdate;
                        }
                    }
                }
                elseif ($pIdCategoriaPermissao)
                {
                    $objUsuarioCategoriaPermissao->setCategoria_permissao_id_INT($pIdCategoriaPermissao);
                    $objUsuarioCategoriaPermissao->setUsuario_id_INT($pIdUsuario);
                    $objUsuarioCategoriaPermissao->setCorporacao_id_INT(Seguranca::getIdDaCorporacaoLogada());
                    $objUsuarioCategoriaPermissao->formatarParaSQL();

                    $msgInsert = $objUsuarioCategoriaPermissao->insert(true);
                    if ($msgInsert != null && !$msgInsert->ok())
                    {
                        return $msgInsert;
                    }
                }
            }
        }
        catch (Exception $ex)
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
        }

        return new Mensagem(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO, null);
    }

    public function gravarServicosDoUsuario($arrServicosAtivos)
    {
        try
        {
            $arrIdServicosAtivos = array();
            if (is_array($arrServicosAtivos))
            {
                for ($i = 0; $i < count($arrServicosAtivos); $i++)
                {
                    $arrIdServicosAtivos[] = $arrServicosAtivos[$i]->servicoId;
                }
            }

            $objBanco = new Database();
            $objBanco2 = new Database();

            $objBanco2->query("SELECT id AS servicoId FROM servico");
            while ($dadosServicos = $objBanco2->fetchArray())
            {
                $idServico = $dadosServicos["servicoId"];
                $objBanco->query("SELECT id, status_BOOLEAN FROM usuario_servico WHERE usuario_id_INT={$this->getId()} AND servico_id_INT={$idServico}");

                if (in_array($idServico, $arrIdServicosAtivos))
                {
                    $status = "1";
                }
                else
                {
                    $status = "0";
                }

                if ($objBanco->rows() == 0)
                {
                    $objUsuarioServico = new EXTDAO_Usuario_servico($this->database);
                    $objUsuarioServico->setUsuario_id_INT($this->getId());
                    $objUsuarioServico->setServico_id_INT($idServico);
                    $objUsuarioServico->setStatus_BOOLEAN($status);
                    $objUsuarioServico->formatarParaSQL();

                    $msgInsert = $objUsuarioServico->insert(true);
                    if ($msgInsert != null && !$msgInsert->ok())
                    {
                        return $msgInsert;
                    }
                }
                else
                {
                    $idUsuarioServico = $objBanco->getPrimeiraTuplaDoResultSet(0);
                    $statusAtualServico = $objBanco->getPrimeiraTuplaDoResultSet(1);

                    if ($statusAtualServico != $status)
                    {
                        $objUsuarioServico = new EXTDAO_Usuario_servico($this->database);
                        $objUsuarioServico->setStatus_BOOLEAN($status);

                        $msgUpdate = $objUsuarioServico->update($idUsuarioServico, true);
                        if ($msgUpdate != null && !$msgUpdate->ok())
                        {
                            return $msgUpdate;
                        }
                    }
                }
            }
        }
        catch (Exception $ex)
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
        }

        return new Mensagem(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO, null);
    }

    public function gravarPermissoesDoMenu($pIdUsuario, $arrayDePermissoes)
    {
        try
        {
            if (!strlen($pIdUsuario))
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, I18N::getExpression("Usuário inválido."));
            }

            $idCorporacaoLogada = Seguranca::getIdDaCorporacaoLogada();

            $arrayChavesNovasPermissoes = array();
            foreach($arrayDePermissoes as $objPermissao)
            {
                $arrayChavesNovasPermissoes[] = $objPermissao->chaveItem;
            }

            //copia array
            $arrayChavesPermissoesInsert = array();
            $arrayIdsPermissoesDelete = array();

            $this->database->query("SELECT id AS id, area_menu AS areaMenu
                                            FROM usuario_menu
                                            WHERE usuario_id_INT={$pIdUsuario}
                                            AND corporacao_id_INT={$idCorporacaoLogada}");

            $arrayAreasAtuaisUsuario = Helper::getResultSetToArrayDeObjetos($this->database->result);
            $arrayAreasAtuaisPermanecer = array();

            foreach ($arrayAreasAtuaisUsuario as $objAreaAtualUsuario)
            {
                if(!in_array($objAreaAtualUsuario->areaMenu, $arrayChavesNovasPermissoes))
                {
                    $arrayIdsPermissoesDelete[] = $objAreaAtualUsuario->id;
                }
                else
                {
                    $arrayAreasAtuaisPermanecer[] = $objAreaAtualUsuario->areaMenu;
                }
            }

            foreach($arrayChavesNovasPermissoes as $areaNovaPermissao)
            {
                if(!in_array($areaNovaPermissao,$arrayAreasAtuaisPermanecer))
                {
                    $arrayChavesPermissoesInsert[] = $areaNovaPermissao;
                }
            }

            foreach($arrayChavesPermissoesInsert as $itemInsert)
            {
                $vObjUsuarioMenu = new EXTDAO_Usuario_menu($this->database);
                $vObjUsuarioMenu->setUsuario_id_INT($pIdUsuario);
                $vObjUsuarioMenu->setArea_menu($itemInsert);
                $vObjUsuarioMenu->setCorporacao_id_INT($idCorporacaoLogada);
                $vObjUsuarioMenu->formatarParaSQL();
                $vObjUsuarioMenu->insert(true);
            }

            foreach($arrayIdsPermissoesDelete as $idDelete)
            {
                $vObjUsuarioMenu = new EXTDAO_Usuario_tipo_menu($this->database);
                $vObjUsuarioMenu->delete($idDelete, true);
            }

            if ($pIdUsuario == Seguranca::getId())
            {
                $objMenu = new Menu();
                Helper::setSession(FlattyMenu::CHAVE_DE_CACHE_DO_MENU, $objMenu->getArrayDoMenuDoUsuarioLogado());
            }
        }
        catch (Exception $ex)
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
        }

        return new Mensagem(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO, null);
    }

    public function __actionAdd($parameters = null)
    {
        try
        {
            if (is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            $retornoValidacaoEmail = $this->validarEmailUsuario($parameters);
            if($retornoValidacaoEmail->mCodRetorno == PROTOCOLO_SISTEMA::NAO)
            {
                $this->setByObject($parameters);
                $this->formatarParaSQL();

                $retornoServicoCadastro = Servicos_web_ponto_eletronico::cadastrarClienteNaAssinaturaSicobFromWeb(
                        $parameters,
                        $this->getDatabase());

                if($retornoServicoCadastro->mCodRetorno == PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO)
                {
                    $idDoUsuario = $this->getIdDoUsuarioSeExistente($parameters->email);
                    if(is_numeric($idDoUsuario))
                    {
                        $retornoCompletoDados = $this->getDadosCompletosDoUsuarioAndEstruturasAdicionais($idDoUsuario);
                        return $retornoCompletoDados;
                    }
                    else
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, I18N::getExpression("Erro ao recuperar o identificador usuário cadastrado."));
                    }
                }
                else
                {
                    return $retornoServicoCadastro;
                }

            }
            else
            {
                return $retornoValidacaoEmail;
            }

        }
        catch (Exception $ex)
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
        }
    }

    //Fluxo de cadastro em método único - utilizado anteriormente
    public function __actionAddCompleta($parameters = null)
    {
        try
        {
            if (is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            $this->setByObject($parameters);
            $this->formatarParaSQL();

            $email = $this->getEmail();
            $idDoUsuario = $this->getIdDoUsuarioSeExistente($email);
            if ($idDoUsuario != null)
            {
                $mensagemErroEdicaoSicob = I18N::getExpression("O e-mail desejado já está sendo utilizado por outro usuário, clique em lembrar senha na pagina de login para recurperar o acesso atraves desse email, se for o caso.");
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, $mensagemErroEdicaoSicob);
            }

            $this->formatarParaSQL();

            $msg = $this->insert(true);
            if ($msg != null && $msg->erro())
            {
                return $msg;
            }

            $this->select($this->getId());

            $objPessoa = $this->inicializaObjetoPessoa();
            $objPessoa->setCadastroSec($this->cadastroSec);
            $objPessoa->setCadastroOffsec($this->cadastroOffsec);

            $msgInsertPessoa = $objPessoa->insert();
            if ($msgInsertPessoa != null && !$msgInsertPessoa->ok())
            {
                return $msgInsertPessoa;
            }

            $idPessoa = $objPessoa->getIdDoUltimoRegistroInserido();
            $msgOperacao = $this->gravarRegistroUsuarioPessoa($idPessoa);
            if ($msgOperacao != null && !$msgOperacao->ok())
            {
                return $msgOperacao;
            }

            $msgOperacao = $this->gravarServicosDoUsuario($parameters->servicosAndroidSelecionados);
            if ($msgOperacao != null && !$msgOperacao->ok())
            {
                return $msgOperacao;
            }

            $msgOperacao = $this->gravarCategoriaPermissaoDoUsuario(false, $this->getId(), $parameters->categoriaPermissaoAndroidId);
            if ($msgOperacao != null && !$msgOperacao->ok())
            {
                return $msgOperacao;
            }

            $msgOperacao = $this->gravarTipoDeUsuarioDoUsuario(false, $this->getId(), $parameters->usuarioTipoId);
            if ($msgOperacao != null && !$msgOperacao->ok())
            {
                return $msgOperacao;
            }

            $msgOperacao = $this->gravarEmpresaDoUsuario(false, $this->getId(), $parameters->empresaId);
            if ($msgOperacao != null && !$msgOperacao->ok())
            {
                return $msgOperacao;
            }

            $msgOperacao = $this->gravarPermissoesDoMenu($this->getId());
            if ($msgOperacao != null && !$msgOperacao->ok())
            {
                return $msgOperacao;
            }

            $msgOperacao = $this->gravarCorporacaoDoUsuarioSeNecessario($this->getId());
            if ($msgOperacao != null && !$msgOperacao->ok())
            {
                return $msgOperacao;
            }

            $msgOperacao = $this->gravarIsAdministradorSistemaAndroid($this->getId(), $parameters->isAdministradorAndroid);
            if ($msgOperacao != null && !$msgOperacao->ok())
            {
                return $msgOperacao;
            }

            $msgOperacao = $this->gravarIsAdministradorSistemaWeb($this->getId(), $parameters->isAdministradorWeb);
            if ($msgOperacao != null && !$msgOperacao->ok())
            {
                return $msgOperacao;
            }

            //força refresh do cache
            static::forcarRefreshDaStringDeUtilizacaoDeUsuariosDaCorporacao();

            return new Mensagem(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO, I18N::getExpression("Usuário cadastrado com sucesso."));
        }
        catch (Exception $ex)
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
        }
    }

    public function __actionEdit($parameters = null)
    {
        try
        {
            if (is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            $msgProtocolo = BO_SICOB::getCaracteristicasDoPacoteDaCorporacao();
            if ($msgProtocolo != null && !$msgProtocolo->ok())
            {
                $mensagemErroRecuperarCaracteristicasPacote = I18N::getExpression("Falha ao recuperar dados do seu pacote. Por favor tente novamente mais tarde, se persistir o problema contacte a central de atendimento");
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, $mensagemErroRecuperarCaracteristicasPacote);
            }
            else
            {
                $protocoloCaracteristicasPacote = $msgProtocolo->mObj;
            }

            $parameters->statusBoolean = $parameters->isAtivo;

            //caso seja o administrador, força determinados campos a serem removidos dos parâmetros por segurança
            if (strcmp($parameters->email, $protocoloCaracteristicasPacote->emailUsuarioPrincipal) == 0)
            {
                unset($parameters->statusBoolean, $parameters->isAdministradorWeb, $parameters->usuarioTipoId, $parameters->itensMenuSelecionados, $parameters->email);
            }
            else
            {
                unset($parameters->email, $parameters->senha);
            }

            $this->setByObject($parameters);
            $this->formatarParaSQL();

            $msgUpdate = $this->updateByInstanceUsingParameters($this->getId(), $parameters,true);
            if ($msgUpdate != null && !$msgUpdate->ok())
            {
                return $msgUpdate;
            }

            $this->select($this->getId());
            $objPessoa = $this->inicializaObjetoPessoa();

            if (($idPessoa = $this->getIdDaPessoaAssociada()) !== null)
            {
                $objPessoa->formatarParaSQL();
                $msgUpdate = $objPessoa->update($idPessoa);
                if ($msgUpdate != null && !$msgUpdate->ok())
                {
                    return $msgUpdate;
                }
            }
            else
            {
                $objPessoa->setCadastroSec($this->cadastroSec);
                $objPessoa->setCadastroOffsec($this->cadastroOffsec);

                $msgInsert = $objPessoa->insert();
                if ($msgInsert != null && !$msgInsert->ok())
                {
                    return $msgInsert;
                }

                $idPessoa = $objPessoa->getIdDoUltimoRegistroInserido();
            }

            $msgOperacao = $this->gravarRegistroUsuarioPessoa($idPessoa);
            if ($msgOperacao != null && !$msgOperacao->ok())
            {
                return $msgOperacao;
            }

            if(isset($parameters->servicosAndroidSelecionados))
            {
                $msgOperacao = $this->gravarServicosDoUsuario($parameters->servicosAndroidSelecionados);
                if ($msgOperacao != null && !$msgOperacao->ok())
                {
                    return $msgOperacao;
                }
            }

            if(isset($parameters->categoriaPermissaoAndroidId))
            {
                $msgOperacao = $this->gravarCategoriaPermissaoDoUsuario(true, $this->getId(), $parameters->categoriaPermissaoAndroidId);
                if ($msgOperacao != null && !$msgOperacao->ok())
                {
                    return $msgOperacao;
                }
            }

            if(isset($parameters->usuarioTipoId))
            {
                $msgOperacao = $this->gravarTipoDeUsuarioDoUsuario(true, $this->getId(), $parameters->usuarioTipoId);
                if ($msgOperacao != null && !$msgOperacao->ok())
                {
                    return $msgOperacao;
                }
            }

            if(isset($parameters->empresaId))
            {
                $msgOperacao = $this->gravarEmpresaDoUsuario(true, $this->getId(), $parameters->empresaId);
                if ($msgOperacao != null && !$msgOperacao->ok())
                {
                    return $msgOperacao;
                }
            }

            if(isset($parameters->itensMenuSelecionados))
            {
                $msgOperacao = $this->gravarPermissoesDoMenu($this->getId(), $parameters->itensMenuSelecionados);
                if ($msgOperacao != null && !$msgOperacao->ok())
                {
                    return $msgOperacao;
                }
            }

            $msgOperacao = $this->gravarCorporacaoDoUsuarioSeNecessario($this->getId());
            if ($msgOperacao != null && !$msgOperacao->ok())
            {
                return $msgOperacao;
            }

            if(isset($parameters->isAdministradorAndroid))
            {
                $msgOperacao = $this->gravarIsAdministradorSistemaAndroid($this->getId(), $parameters->isAdministradorAndroid);
                if ($msgOperacao != null && !$msgOperacao->ok())
                {
                    return $msgOperacao;
                }
            }

            if(isset($parameters->isAdministradorWeb))
            {
                $msgOperacao = $this->gravarIsAdministradorSistemaWeb($this->getId(), $parameters->isAdministradorWeb);
                if ($msgOperacao != null && !$msgOperacao->ok())
                {
                    return $msgOperacao;
                }
            }

            return new Mensagem(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO, I18N::getExpression("Usuário alterado com sucesso."));
        }
        catch (Exception $ex)
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
        }
    }

    public function getIdDoUsuarioAdministrador()
    {
        try
        {
            $msgProtocolo = BO_SICOB::getCaracteristicasDoPacoteDaCorporacao();
            if ($msgProtocolo != null && !$msgProtocolo->ok())
            {
                $mensagemErroRecuperarCaracteristicasPacote = I18N::getExpression("Falha ao recuperar dados do seu pacote. Por favor tente novamente mais tarde.");
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, $mensagemErroRecuperarCaracteristicasPacote);
            }
            else
            {
                $protocolo = $msgProtocolo->mObj;
                $emailAdministradorDaConta = $protocolo->emailUsuarioPrincipal;
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();

                $msg = $this->database->queryMensagem("SELECT u.id AS idUsuarioAdministrador
                                                                FROM usuario u 
                                                                JOIN usuario_corporacao uc
                                                                  ON uc.usuario_id_INT=u.id
                                                                  AND uc.corporacao_id_INT={$idCorporacao}
                                                                WHERE u.email='{$emailAdministradorDaConta}'");
                if ($msg != null && $msg->erro())
                {
                    return $msg;
                }
                else if($this->database->rows() > 0)
                {
                    $objRetornoDB = Helper::getPrimeiroRegistroDoResultSetAsObject($this->database->result);
                    return new Mensagem_generica($objRetornoDB->idUsuarioAdministrador, PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO);
                }
                else
                {
                    $mensagemRegistroNaoEncontrado = I18N::getExpression("Falha ao recuperar dados. Por favor tente novamente mais tarde.");
                    return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, $mensagemRegistroNaoEncontrado);
                }

            }

        }
        catch (Exception $ex)
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
        }

    }

    public function __getList($parameters = null)
    {
        if (is_null($parameters))
        {
            $parameters = Helper::getPhpInputObject();
        }

        try
        {
            $msg = $this->getIdDoUsuarioAdministrador();
            if ($msg != null && $msg->erro())
            {
                return $msg;
            }

            $idUsuarioAdministrador = $msg->mObj;
            $idCorporacao = Seguranca::getIdDaCorporacaoLogada();

            $filterParameters = null;
            if (isset($parameters->filterParameters))
            {
                $filterParameters = $parameters->filterParameters;
            }

            $sortingParameters = null;
            if (isset($parameters->sortingParameters))
            {
                $sortingParameters = $parameters->sortingParameters;
            }

            $paginationParameters = null;
            if (isset($parameters->paginationParameters))
            {
                $paginationParameters = $parameters->paginationParameters;
            }

            $listMappingType = static::getListMappingType($parameters->listMappingType);
            $mainTableAlias = "u";

            $whereClause = static::getWhereClauseForFilter($filterParameters, $mainTableAlias);
            $orderByClause = static::getOrderByClauseForFilter($sortingParameters, $mainTableAlias);
            $limitClause = static::getLimitClauseForFilter($paginationParameters);

            $queryWithoutLimit = "SELECT u.id FROM usuario u {$whereClause}";
            $query = "SELECT u.id AS usuario__id, 
                      u.nome AS usuario__nome, 
                      u.email AS usuario__email,
                      u.senha AS usuario__senha, 
                      u.status_BOOLEAN AS usuario__status_BOOLEAN, 
                      u.pagina_inicial AS usuario__pagina_inicial, 
                      u.cadastro_SEC AS usuario__cadastro_SEC, 
                      u.cadastro_OFFSEC AS usuario__cadastro_OFFSEC 
                      FROM usuario u
                      JOIN usuario_corporacao uc ON uc.usuario_id_INT=u.id AND uc.corporacao_id_INT={$idCorporacao}
                      {$whereClause} {$orderByClause} {$limitClause}";

            $msg = $this->database->queryMensagem($queryWithoutLimit);
            if ($msg != null && $msg->erro())
            {
                return $msg;
            }

            $totalNumberOfRecords = $this->database->rows();
            if ($totalNumberOfRecords == 0)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
            }

            $msg = $this->database->queryMensagem($query);

            //operacao realizaca com sucesso
            if ($msg == null)
            {
                if ($this->database->rows() == 0)
                {
                    return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                }

                $resultSet = $this->database->result;

                $objReturn = new stdClass();
                $objReturn->dataSet = static::mapResultSetDataToList($resultSet, $listMappingType, static::$listAliasTypes);

                foreach($objReturn->dataSet as $record)
                {
                    if($record->usuario__id == $idUsuarioAdministrador)
                    {
                        $record->allowEdit = false;
                        $record->allowRemove = false;
                    }

                    $record->usuario_tipo__status_BOOLEAN_forHuman = Helper::formatarBooleanParaExibicao($record->usuario_tipo__status_BOOLEAN, "Ativo", "Inativo");
                }


                if (!Helper::isNullOrEmpty($paginationParameters->recordsPerPage) && !Helper::isNullOrEmpty($paginationParameters->currentPage))
                {
                    $lastPage = ceil($totalNumberOfRecords / $paginationParameters->recordsPerPage);
                    $objReturn->paginationParameters = new PaginationParams($paginationParameters->recordsPerPage, $paginationParameters->currentPage, $lastPage);
                }

                return new Mensagem_generica($objReturn);
            }
            else
            {
                return $msg;
            }
        }
        catch (Exception $ex)
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
        }
    }

    public static function enviaEmailParaOUsuario(
        $emailCadastrado,
        $assunto,
        $conteudoAssunto,
        $db = null)
    {

        if (!ENVIAR_EMAIL)
        {
            return true;
        }
        $emailCadastrado = Helper::strtolowerlatin1($emailCadastrado);

        if ($db == null)
        {
            $objDatabase = new Database();
        }
        else
        {
            $objDatabase = $db;
        }

        $objDatabase->query("SELECT id, nome, email, senha
                                            FROM usuario
                                            WHERE email='{$emailCadastrado}'");

        if ($objDatabase->rows() == 1)
        {
            $idUsuario = $objDatabase->getPrimeiraTuplaDoResultSet(0);
            $nome = ucwords($objDatabase->getPrimeiraTuplaDoResultSet(1));
            $emailDestino = $objDatabase->getPrimeiraTuplaDoResultSet(2);
            $senha = $objDatabase->getPrimeiraTuplaDoResultSet(3);

            $objCrypt = new Crypt();
            $senhaDescriptografada = $objCrypt->decrypt($senha);

            $mensagem = " Sr(a). " . ucwords($nome) . "
                    $conteudoAssunto
                    --- Lembrete de senha de acesso ao " . TITULO_PAGINAS . " ---\n
                    Endereço para Acesso: " . ENDERECO_DE_ACESSO . "
                    Usuário: {$emailDestino}
                    Senha  : {$senhaDescriptografada}
                    ";

            $mensagemUsuarioCorporacao = DatabaseSincronizador::getMensagemUsuarioCorporacao($idUsuario);
            $objEmail = new Email_Sender(EMAIL_PADRAO_REMETENTE_MENSAGENS, $emailDestino);
            $objEmail->setAssunto($assunto);
            $objEmail->setConteudo($mensagem . $mensagemUsuarioCorporacao, false);

            if ($objEmail->enviarEmail())
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    public static function mapResultSetDataToListForGrid($resultSetData, $objAliasTypes = null)
    {
        $returnData = array();
        $formatForHuman = !is_null($objAliasTypes);
        for ($i = 0; $object = mysqli_fetch_object($resultSetData); $i++)
        {
            $currentObject = new stdClass();
            $currentObject->allowEdit = true;
            $currentObject->allowRemove = true;

            foreach ($object as $key => $value)
            {
                $attributeName = $key;
                $attributeNameForHuman = "{$attributeName}_forHuman";

                if ($formatForHuman)
                {
                    if ($attributeName == "usuario__status_BOOLEAN")
                    {
                        $valueForHuman = Helper::formatarBooleanParaExibicao($value, "Ativo", "Inativo");
                    }
                    else
                    {
                        $attributeType = $objAliasTypes->$key;
                        $valueForHuman = static::formatarValorParaExibicao($value, $attributeType);
                    }
                }
                else
                {
                    $valueForHuman = $value;
                }

                $currentObject->$attributeName = $value;
                $currentObject->$attributeNameForHuman = $valueForHuman;
            }

            $currentObject->primaryKey = static::getPrimaryKeyObject($currentObject);
            $returnData[] = $currentObject;
        }

        return $returnData;
    }

    public function setByPost()
    {
        $this->id = $this->formatarDados(Helper::strtoupperlatin1(Helper::POST("id")));
        $this->nome = $this->formatarDados(Helper::strtoupperlatin1(Helper::POST("nome")));
        $this->nomeNormalizado = $this->formatarDados(Helper::retiraAcento($this->nome));
        $this->email = Helper::strtolowerlatin1($this->formatarDados(Helper::strtolowerlatin1(Helper::POST("email"))));
        $this->senha = $this->formatarDados(Helper::strtoupperlatin1(Helper::POST("senha")));
        $this->status = "1";
        $this->paginaInicial = $this->formatarDados(Helper::strtoupperlatin1(Helper::POST("pagina_inicial")));
    }

    public function setByGet($numReg)
    {
        $this->id = $this->formatarDados(Helper::strtoupperlatin1(Helper::GET("id")));
        $this->nome = $this->formatarDados(Helper::strtoupperlatin1(Helper::GET("nome")));
        $this->nomeNormalizado = $this->formatarDados(Helper::retiraAcento($this->nome));
        $this->email = Helper::strtolowerlatin1($this->formatarDados(Helper::strtolowerlatin1(Helper::GET("email"))));
        $this->senha = $this->formatarDados(Helper::strtoupperlatin1(Helper::GET("senha")));
        $this->status = "1";
        $this->paginaInicial = $this->formatarDados(Helper::strtoupperlatin1(Helper::GET("pagina_inicial")));
    }

    public static function factory()
    {
        return new EXTDAO_Usuario();
    }

    public function getUsuario_tipo_id_INT($idCorporacao = null)
    {
        if ($idCorporacao == null)
        {
            $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
        }
        $idUsuarioTipo = EXTDAO_Usuario_tipo_corporacao::getIdUsuarioTipoCorporacao($this->getId(), $idCorporacao);

        return $idUsuarioTipo;
    }

    public static function getSenhaUsuarioDoEmail($email, $db = null)
    {
        if ($db == null)
        {
            $db = new Database();
        }
        $db->query("SELECT senha"
                   . " FROM usuario"
                   . " WHERE email = '$email'");

        return $db->getPrimeiraTuplaDoResultSet(0);
    }

    public function alterarSenhaAvulso($idUsuarioLogado, $senha)
    {
        try
        {
            $objCriptografia = new Crypt();

            $senhaC = $objCriptografia->crypt($senha);
            $this->database->queryMensagem("UPDATE usuario SET senha='{$senhaC}' WHERE id={$idUsuarioLogado}");

            return new Mensagem(
                PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO,
                I18N::getExpression("Sua senha foi alterada com sucesso!"));
        }
        catch (Exception $ex)
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
        }
    }
}
