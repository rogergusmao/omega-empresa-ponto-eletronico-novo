/*

 Arquivo gerado atrav�s de gerador de c�digo em 07/01/2018 as 15:54:12.
 Para que o arquivo n�o seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

 Tabela correspondente: ponto
 Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

 */

returnObject.getPonto = function (filterParameters)
{

    var parameters = {
        filterParameters: filterParameters,
        loadComboBoxesData: true
    };

    return $http({
        url: baseURLAdm + "/webservice.php?class=EXTDAO_Ponto&action=__getRecord",
        method: "POST",
        data: parameters,
        headers: {
            'Content-Type': 'application/json'
        }
    });

};

returnObject.addPonto = function (formData)
{

    return $http({
        url: baseURLAdm + "/webservice.php?class=EXTDAO_Ponto&jsonOutput=true&action=add",
        method: "POST",
        data: formData,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    });

};

returnObject.editPonto = function (formData)
{

    return $http({
        url: baseURLAdm + "/webservice.php?class=EXTDAO_Ponto&jsonOutput=true&action=edit",
        method: "POST",
        data: formData,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    });

};

returnObject.getAllComboBoxesDataForPontoForm = function ()
{
    return $http({
        url: baseURLAdm + "/webservice.php?class=EXTDAO_Ponto&jsonOutput=true&action=__getFormComboBoxesData",
        method: "POST",
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    });

};

returnObject.getListOfPessoaForPontoForm = function ()
{
    var params = {filterParameters: null, listMappingType: Constants.ListMappingType.COMBOBOX, hideLoading: true};
    return $http.post(baseURLAdm + "/webservice.php?class=EXTDAO_Pessoa&action=__getList", params);
};
returnObject.getListOfEmpresaForPontoForm = function ()
{
    var params = {filterParameters: null, listMappingType: Constants.ListMappingType.COMBOBOX, hideLoading: true};
    return $http.post(baseURLAdm + "/webservice.php?class=EXTDAO_Empresa&action=__getList", params);
};
returnObject.getListOfProfissaoForPontoForm = function ()
{
    var params = {filterParameters: null, listMappingType: Constants.ListMappingType.COMBOBOX, hideLoading: true};
    return $http.post(baseURLAdm + "/webservice.php?class=EXTDAO_Profissao&action=__getList", params);
};
returnObject.getListOfUsuarioForPontoForm = function ()
{
    var params = {filterParameters: null, listMappingType: Constants.ListMappingType.COMBOBOX, hideLoading: true};
    return $http.post(baseURLAdm + "/webservice.php?class=EXTDAO_Usuario&action=__getList", params);
};
returnObject.getListOfTipoPontoForPontoForm = function ()
{
    var params = {filterParameters: null, listMappingType: Constants.ListMappingType.COMBOBOX, hideLoading: true};
    return $http.post(baseURLAdm + "/webservice.php?class=EXTDAO_Tipo_ponto&action=__getList", params);
};
                

