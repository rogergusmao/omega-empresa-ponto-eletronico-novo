<?php

/*

Arquivo gerado através de gerador de código em 07/01/2018 as 15:54:14.
Para que o arquivo não seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

Tabela correspondente: usuario_tipo
Sobrescrita de dados: ---SOBRESCRITA_NAO_AUTORIZADA---

*/
?>

<!--Definição da View Angular JS -->
<?

$objForm = new FormEnvironment("mainForm", "formUsuarioTipoController", "vm");
$objForm->setLayoutOneColumn();

?>
<div class='row' id='content-wrapper' ng-controller="<?= $objForm->getAngularControllerAttributeValue() ?>">

    <?= $objForm->setModalEnvironmentIfApplicable(); ?>
    <?= $objForm->setEditionVariablesIfApplicable(); ?>

    <!--Definição do template do dialog exibido após inserção -->
    <script type="text/ng-template" id="after-usuarioTipo-insert-template.html">
        <div class="modal-header">
            <h4 class="modal-title">
                <span class="glyphicon"></span>
                <?= I18N::getExpression("Tipo de usuário adicionado com sucesso!"); ?>
            </h4>
        </div>
        <div class="modal-body">
            <span class="help-block"><?= I18N::getExpression("O que deseja fazer agora?"); ?></span>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" ng-click="vm.loadListOfUsuarioTipo()">
                <?= I18N::getExpression("Listar tipos de usuário"); ?>
            </button>
            <button type="button" class="btn" ng-click="closeMessageDialog()">
                <?= I18N::getExpression("Permanecer na tela"); ?>
            </button>
            <button type="button" class="btn btn-success" ng-click="vm.addNewUsuarioTipo()">
                <?= I18N::getExpression("Adicionar tipo de usuário"); ?>
            </button>
        </div>
    </script>

    <!--Definição do template do exibido dialog após update -->
    <script type="text/ng-template" id="after-usuarioTipo-update-template.html">
        <div class="modal-header">
            <h4 class="modal-title">
                <span class="glyphicon"></span>
                <?= I18N::getExpression("Tipo de usuário alterado com sucesso!"); ?>

            </h4>
        </div>
        <div class="modal-body">
            <span class="help-block"><?= I18N::getExpression("O que deseja fazer agora?"); ?></span>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" ng-click="vm.loadListOfUsuarioTipo()">
                <?= I18N::getExpression("Listar tipos de usuário"); ?>
            </button>
            <button type="button" class="btn" ng-click="closeMessageDialog()">
                <?= I18N::getExpression("Permanecer na tela"); ?>
            </button>
            <button type="button" class="btn btn-success" ng-click="vm.addNewUsuarioTipo()">
                <?= I18N::getExpression("Adicionar tipo de usuário"); ?>
            </button>
        </div>
    </script>

    <div class='col-xs-12'>

        <div class='row'>
            <div class='col-sm-12'>
                <div class='page-header'>
                    <h1 class='pull-left'>
                        <i class='icon-edit'></i>
                        <span><?= I18N::getExpression("Cadastro de Tipo de Usuário"); ?></span>
                    </h1>
                    <div class='pull-right close-button-container'>
                        <?= $objForm->renderCloseModalButtonIfExists(); ?>
                    </div>
                </div>
            </div>
        </div>

        <?= $objForm->getFormDebugPanel(); ?>
        <?= $objForm->getModalMessagesPanel(); ?>

        <div class='row'>
            <div class='col-sm-12'>
                <div class='box'>
                    <div class='box-header blue-background'>
                        <div class='title'>
                            <div class='icon-edit'></div>
                            <?= I18N::getExpression("Dados do Tipo de Usuário"); ?>
                        </div>
                        <div class='actions hidden'>
                            <a class="btn box-collapse btn-xs btn-link" href="javascript:void(0);"><i></i></a>
                        </div>
                    </div>
                    <div class='box-content'>

                        <form name="<?= $objForm->getFormName() ?>" class="form form-horizontal"
                              style="margin-bottom: 0;" novalidate>


                            <?php

                            $objCampo = $objForm->getTextInputInstance("nome", TextInput::TRANSFORMATION_DIRECTIVE_UPPER);
                            $objCampo->setLabel(I18N::getExpression("Nome"));

                            $objCampo->setDefaultValues();
                            $objCampo->setPlaceHolder("Informe o Nome.");
                            $objCampo->addValidation(new MaxLengthValidation(255, I18N::getExpression("O tamanho máximo do Nome deve ser de {0} caracteres.")));
                            $objCampo->addValidation(new RequiredValidation(I18N::getExpression("O Nome é de preenchimento obrigatório.")));

                            ?>

                            <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                 ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                                <?= $objCampo->renderLabel(); ?>
                                <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                    <?= $objCampo->render(); ?>
                                    <?= $objCampo->renderValidationBlock(); ?>

                                </div>

                            </div>

                            <?= $objForm->renderHorizontalLine(); ?>

                            <?php

                            $objCampo = $objForm->getTextInputInstance("nomeVisivel", TextInput::TRANSFORMATION_DIRECTIVE_UPPER);
                            $objCampo->setLabel(I18N::getExpression("Nome visível"));

                            $objCampo->setDefaultValues();
                            $objCampo->setPlaceHolder("Informe o Nome visível.");
                            $objCampo->addValidation(new MaxLengthValidation(255, I18N::getExpression("O tamanho máximo do Nome visível deve ser de {0} caracteres.")));
                            $objCampo->addValidation(new RequiredValidation(I18N::getExpression("O Nome visível é de preenchimento obrigatório.")));

                            ?>

                            <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                 ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                                <?= $objCampo->renderLabel(); ?>
                                <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                    <?= $objCampo->render(); ?>
                                    <?= $objCampo->renderValidationBlock(); ?>

                                </div>


                            </div>

                            <?= $objForm->renderHorizontalLine(); ?>



                            <?php

                            $objCampo = $objForm->getSwitchControlInstance("statusBoolean");
                            $objCampo->setLabel(I18N::getExpression("Status"));

                            $objCampo->setDefaultValues();
                            $objCampo->setOnText(I18N::getExpression("SIM"));
                            $objCampo->setOffText(I18N::getExpression("NÃO"));

                            ?>

                            <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                 ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                                <?= $objCampo->renderLabel(); ?>
                                <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                    <?= $objCampo->render(); ?>
                                    <?= $objCampo->renderValidationBlock(); ?>

                                </div>

                            </div>

                            <?= $objForm->renderHorizontalLine(); ?>

                            <?php

                            $objListaEsquerda = $objForm->getCheckboxListInstance("itensMenuSelecionados", "vm.listaItensMenuWebEsquerda", "itemMenuWeb", "descricaoItem");
                            $objListaEsquerda->setLabel(I18N::getExpression("Permissões do Menu"));
                            $objListaEsquerda->setItemLabelAngularClass("::vm.getMenuWebItemClass(itemMenuWeb.nivel)");
                            $objListaEsquerda->setChangeListener("changeMenuItem(itemMenuWeb, checked)");
                            $objListaEsquerda->setContainerCssClass("col-md-5");
                            $objListaEsquerda->setItemContainerCssClass("col-sm-12");

                            $objListaDireita = $objForm->getCheckboxListInstance("itensMenuSelecionados", "vm.listaItensMenuWebDireita", "itemMenuWeb", "descricaoItem");
                            $objListaDireita->setItemLabelAngularClass("::vm.getMenuWebItemClass(itemMenuWeb.nivel)");
                            $objListaDireita->setChangeListener("changeMenuItem(itemMenuWeb, checked)");
                            $objListaDireita->setContainerCssClass("col-md-5");
                            $objListaDireita->setItemContainerCssClass("col-sm-12");
                            ?>

                            <div class='<?= $objListaEsquerda->getFormGroupCssClass() ?>'
                                 ng-class="<?= $objListaEsquerda->getFormGroupAngularClass() ?>">

                                <?= $objListaEsquerda->renderLabel(); ?>
                                <div class='<?= $objListaEsquerda->getContainerCssClass(); ?>'>

                                    <?= $objListaEsquerda->render(); ?>
                                    <?= $objListaEsquerda->renderValidationBlock(); ?>

                                </div>

                                <div class='<?= $objListaDireita->getContainerCssClass(); ?>'>

                                    <?= $objListaDireita->render(); ?>
                                    <?= $objListaDireita->renderValidationBlock(); ?>

                                </div>

                            </div>

                            <div class='form-actions form-actions-padding-sm'>
                                <div class='row'>
                                    <div class='col-md-10 action-buttons-bar'>

                                        <?= $objForm->renderSubmitButton(); ?>

                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php

if (!Helper::isAngularModal())
{
    $arrDataServicesRelacionados = null;
    $arrControllersRelacionados = null;

    ?>

    <!--Definição do Controller Angular JS -->
    <script type="text/javascript">

        <?=Helper::incluirConteudoDataServices(true, $arrDataServicesRelacionados); ?>

    </script>

    <?= Helper::incluirControllerAngularJS(true, $arrControllersRelacionados); ?>

<?php } ?>

        
