/*

Arquivo gerado atrav�s de gerador de c�digo em 19/01/2018 as 19:58:41.
Para que o arquivo n�o seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

Tabela correspondente: empresa
Sobrescrita de dados: ---SOBRESCRITA_NAO_AUTORIZADA---

*/

omegaApp.controller('listEmpresaController',

    ['$scope', '$rootScope', '$element', '$window', '$interval', '$timeout', 'dataService', 'Upload',

     function ($scope, $rootScope, $element, $window, $interval, $timeout, dataService, Upload)
     {

         var vm = this;
         var generatedTasks = omegaApp.generatedListEmpresaController($scope, $rootScope, $element, $window, $interval, $timeout, dataService, Upload, vm);

         try
         {
             //remover chamada caso queira sobrescrever carregamento de dados inicial
             ApplicationUtil.runGeneratedTasks(generatedTasks);

             vm.filterOperators = {
                 id: Constants.FilterOperator.EQUALS,
                 nome: Constants.FilterOperator.CONTAINS,
                 email: Constants.FilterOperator.CONTAINS,
                 tipoDocumentoId: Constants.FilterOperator.EQUALS,
                 numeroDocumento: Constants.FilterOperator.CONTAINS,
                 tipoEmpresaId: Constants.FilterOperator.EQUALS,
                 logradouro: Constants.FilterOperator.CONTAINS,
                 cidadeId: Constants.FilterOperator.EQUALS
             };

         }
         catch (ex)
         {
             ApplicationUtil.showErrorMessage(ex.message, 'Erro no controller listEmpresaController', vm);
         }

     }]
);