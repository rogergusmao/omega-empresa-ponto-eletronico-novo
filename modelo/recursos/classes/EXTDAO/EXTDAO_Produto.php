<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Produto
    * NOME DA CLASSE DAO: 
    * DATA DE GERAÃ‡ÃƒO:    
    * ARQUIVO:            EXTDAO_Produto.php
    * TABELA MYSQL:       produto
    * BANCO DE DADOS:     
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÃ‡ÃƒO DA CLASSE
    // **********************

    class EXTDAO_Produto extends DAO_Produto
    {

        public function __construct($configDAO){

            parent::__construct($configDAO);

            	$this->nomeClasse = "EXTDAO_Produto";


            $this->setLabels();






        }

        public function setLabels(){

			$this->label_id = "";
			$this->label_identificador = "";
			$this->label_nome = "";
			$this->label_nome_normalizado = "";
			$this->label_descricao = "";
			$this->label_produto_unidade_medida_id_INT = "";
			$this->label_id_omega_INT = "";
			$this->label_cadastro_SEC = "";
			$this->label_cadastro_OFFSEC = "";
			$this->label_preco_custo_FLOAT = "";
			$this->label_prazo_reposicao_estoque_dias_INT = "";
			$this->label_codigo_barra = "";
			$this->label_corporacao_id_INT = "";


        }





        public function factory(){

            return new EXTDAO_Produto();

        }



	}

    
