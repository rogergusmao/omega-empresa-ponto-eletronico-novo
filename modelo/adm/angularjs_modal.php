<?php

require_once '../recursos/php/configuracao_biblioteca_compartilhada.php';

require_once $workspaceRaiz . DIRETORIO_BIBLIOTECAS_COMPARTILHADAS . '/imports/header.php';

ConfiguracaoCorporacaoDinamica::init(getWebservicesSemCache());

include_once '../recursos/php/funcoes.php';

$script = new AngularJsModalPadrao();
$script->render();

include_once '../recursos/php/on_finish.php';
