<?php


        /*

        Arquivo gerado através de gerador de código em 12/05/2018 as 14:44:35.
        Para que o arquivo não seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

        Tabela correspondente: acesso
        Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

        */
        


class DAO_Acesso extends Generic_DAO
                        {
                            const NOME_ENTIDADE = "acesso";

                            public static $nomeTabela = null;
                            public static $idSistemaTabela = false;
                            public static $databaseFieldsRelatedAttributes = null;
	                        public static $databaseFieldTypes = null;
	                        public static $databaseFieldNames = null;
	                        public static $listAliasTypes = null;
	                        public static $listAliasRelatedAttributes = null;
	                        
	                        public $id;
public $usuarioId;
public $dataLoginSec;
public $dataLoginOffsec;
public $dataLogoutSec;
public $dataLogoutOffsec;

public $labelId;
public $labelUsuarioId;
public $labelDataLoginSec;
public $labelDataLoginOffsec;
public $labelDataLogoutSec;
public $labelDataLogoutOffsec;

public static $arrPK = array("id");

public function __construct($configDAO = null)
        {
            parent::__construct($configDAO);
            static::__setStaticValues($this->database);           
        }

public static function idSistemaTabela($databaseObject = null){
            if(!(static::$idSistemaTabela))
            {
                static::$idSistemaTabela = EXTDAO_Sistema_tabela::getIdDaSistemaTabela(static::nomeTabela(), $databaseObject);
            }
            return static::$idSistemaTabela;
        }

public function getIdSistemaTabela(){
            return static::idSistemaTabela();
        }

public static function nomeTabela(){
            if(is_null(static::$nomeTabela))
            {
                static::$nomeTabela = "acesso";
            }
            return static::$nomeTabela;
        }

public function getNomeTabela(){
            return static::nomeTabela();
        }

public static function __setStaticValues($databaseObject = null)
                   {    
                        static::nomeTabela();
                        
                        static::idSistemaTabela($databaseObject);
                   
                        static::setDatabaseFieldNames();
                        static::setDatabaseFieldTypes();
                        static::setDatabaseFieldsRelatedAttributeNames();
                        
                        static::setListAliasTypes();
                        static::setListAliasRelatedAttributes();
                   }

public function getExpressaoLabel()
                   {
                        return "{id}";
                   }

public static function setDatabaseFieldNames()
        {
            if(is_null(static::$databaseFieldNames))
            {
                static::$databaseFieldNames = new stdClass();
                static::$databaseFieldNames->id = "id";
static::$databaseFieldNames->usuarioId = "usuario_id_INT";
static::$databaseFieldNames->dataLoginSec = "data_login_SEC";
static::$databaseFieldNames->dataLoginOffsec = "data_login_OFFSEC";
static::$databaseFieldNames->dataLogoutSec = "data_logout_SEC";
static::$databaseFieldNames->dataLogoutOffsec = "data_logout_OFFSEC";

            }
                      
        }

public static function setDatabaseFieldTypes()
        {
            if(is_null(static::$databaseFieldTypes))
            {
                static::$databaseFieldTypes = new stdClass();
                static::$databaseFieldTypes->id = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->usuarioId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->dataLoginSec = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$databaseFieldTypes->dataLoginOffsec = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$databaseFieldTypes->dataLogoutSec = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$databaseFieldTypes->dataLogoutOffsec = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;

            }
            
        }

public static function setDatabaseFieldsRelatedAttributeNames()
        {
            if(is_null(static::$databaseFieldsRelatedAttributes))
            {
                static::$databaseFieldsRelatedAttributes = new stdClass();
                static::$databaseFieldsRelatedAttributes->id = "id";
static::$databaseFieldsRelatedAttributes->usuario_id_INT = "usuarioId";
static::$databaseFieldsRelatedAttributes->data_login_SEC = "dataLoginSec";
static::$databaseFieldsRelatedAttributes->data_login_OFFSEC = "dataLoginOffsec";
static::$databaseFieldsRelatedAttributes->data_logout_SEC = "dataLogoutSec";
static::$databaseFieldsRelatedAttributes->data_logout_OFFSEC = "dataLogoutOffsec";

            }           
        }

public function setAllFieldsToHumanFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["usuario_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["data_login_SEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["data_login_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["data_logout_SEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["data_logout_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
}

public function setAllFieldsToDatabaseFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["usuario_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["data_login_SEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["data_login_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["data_logout_SEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["data_logout_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
}

  public function getFkObjUsuario() 
                {
                    if($this->objUsuario == null)
                    {                        
                        $this->objUsuario = new EXTDAO_Usuario_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getUsuario_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objUsuario->clear();
                    }
                    elseif($this->objUsuario->getId() != $idFK)
                    {
                        $this->objUsuario->select($idFK);
                    }
                    return $this->objUsuario;
                }


public function setLabels()
        {$this->labelId = "id";
$this->labelUsuarioId = "usuarioidINT";
$this->labelDataLoginSec = "login sec";
$this->labelDataLoginOffsec = "login offsec";
$this->labelDataLogoutSec = "logout sec";
$this->labelDataLogoutOffsec = "logout offsec";
}

public function __actionAdd($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                $this->setByObject($parameters);
                $this->setDataCadastroAndOffsetInSecondsFromUser($parameters);
                
                $this->formatarParaSQL();
                $msg = $this->insert(true);
                
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                return new Mensagem(null, I18N::getExpression("Acesso adicionado com sucesso."));
                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionEdit($parameters = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try {

                $this->setByObject($parameters);
                $this->setAllFieldsToHumanFormat();
                
                $this->formatarParaSQL();
                $msg = $this->updateByInstanceUsingParameters($this->id, $parameters, true);
                if($msg != null && $msg->erro())
                {
                     return $msg;
                }
        
                return new Mensagem(null, I18N::getExpression("Acesso editado com sucesso."));
            }
            catch(Exception $ex)
            {                
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionRemove($parameters = null)
        {
            $successulRemovalCount = 0;
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                foreach($parameters as $record)
                {
                    if(is_numeric($record->id))
                    {
                        $msg = $this->delete($record->id, true);
                        if($msg == null || $msg->ok())
                        {
                            $successulRemovalCount++;
                        }
                    }
                }
                
                if($successulRemovalCount > 0)
                {
                    $successMessage = count($successulRemovalCount) > 1 ? I18N::getExpression("{0} Acesso foram removidos com sucesso.", $successulRemovalCount) : I18N::getExpression("Acesso removido com sucesso.");
                    return new Mensagem(null, $successMessage);
                }
                else
                {
                    $errorMessage = count($parameters) > 1 ? I18N::getExpression("Falha ao remover Acesso.") : I18N::getExpression("Falha ao remover Acesso.");
                    return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, $errorMessage);                    
                }
            }
            catch (Exception $ex) 
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }

public function __getRecord($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }
            
            try
            {            
                $returnObject = new stdClass();            
                $whereClause = static::getWhereClauseForFilter($parameters->filterParameters);
                $query = "SELECT  id, usuario_id_INT, data_login_SEC, data_login_OFFSEC, data_logout_SEC, data_logout_OFFSEC FROM acesso {$whereClause}";
    
                $msg = $this->database->queryMensagem($query);                
                if ($msg == null)
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Não foi encontrado nenhum registro"));
                    }                    
                    $resultSet = $this->database->result;
                    $resultObject = static::mapResultSetDataToRecord($resultSet);
                    
                    $returnObject->formData = $resultObject;
                    $returnObject->comboBoxesData = null;
                    if($parameters->loadComboBoxesData)
                    {
                        $this->setByObject($resultObject);
                        $returnObject->comboBoxesData =  $this->getFormComboBoxesData();
                    }                    
    
                    
    
                    return new Mensagem_generica($returnObject);
                } 
                else 
                {
                    return $msg;
                }
                
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }


        
        public function __getFilterComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
                
        public function __getFormComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
        
        protected function getFormComboBoxesData()
        {
            $comboBoxesData = new stdClass();
            $listParameters = new stdClass();
            $listParameters->listMappingType = static::LIST_MAPPING_TYPE_COMBOBOX;
            
            
                    
                    $objUsuario = new EXTDAO_Usuario();
                    $comboBoxesData->fieldUsuarioId = $objUsuario->__getList($listParameters);

            return $comboBoxesData;
        }
        
        


        
        public static function setListAliasTypes()
        {
            if(is_null(static::$listAliasTypes))
            {
                static::$listAliasTypes = new stdClass();
                static::$listAliasTypes->usuario__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->acesso__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->acesso__cadastro_OFFSEC = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$listAliasTypes->acesso__data_login_SEC = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$listAliasTypes->acesso__data_login_OFFSEC = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$listAliasTypes->acesso__data_logout_SEC = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$listAliasTypes->acesso__data_logout_OFFSEC = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
            }        
        }
        
        public static function setListAliasRelatedAttributes()
        {
            if(is_null(static::$listAliasRelatedAttributes))
            {
                static::$listAliasRelatedAttributes = new stdClass();
                static::$listAliasRelatedAttributes->usuario__id = "usuarioId";
static::$listAliasRelatedAttributes->acesso__id = "id";
static::$listAliasRelatedAttributes->acesso__cadastro_OFFSEC = "cadastroOffsec";
static::$listAliasRelatedAttributes->acesso__data_login_SEC = "dataLoginSec";
static::$listAliasRelatedAttributes->acesso__data_login_OFFSEC = "dataLoginOffsec";
static::$listAliasRelatedAttributes->acesso__data_logout_SEC = "dataLogoutSec";
static::$listAliasRelatedAttributes->acesso__data_logout_OFFSEC = "dataLogoutOffsec";
            }         
        }

        public function __getList($parameters = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try
            {
                $filterParameters = null;
                if(isset($parameters->filterParameters))
                {
                    $filterParameters = $parameters->filterParameters;
                }
                                    
                    
                
                $sortingParameters = null;
                if(isset($parameters->sortingParameters))
                {
                    $sortingParameters = $parameters->sortingParameters;
                }
                
                $paginationParameters = null;
                if(isset($parameters->paginationParameters))
                {
                    $paginationParameters = $parameters->paginationParameters;
                }
                
                $listMappingType = static::getListMappingType($parameters->listMappingType);
                $mainTableAlias = "a";

                $whereClause = static::getWhereClauseForFilter($filterParameters, $mainTableAlias);
                $orderByClause = static::getOrderByClauseForFilter($sortingParameters, $mainTableAlias);
                $limitClause = static::getLimitClauseForFilter($paginationParameters);

                $queryWithoutLimit = "SELECT a.id FROM acesso a {$whereClause}";
                $query = "SELECT u.id AS usuario__id, a.id AS acesso__id, a.cadastro_OFFSEC AS acesso__cadastro_OFFSEC, a.data_login_SEC AS acesso__data_login_SEC, a.data_login_OFFSEC AS acesso__data_login_OFFSEC, a.data_logout_SEC AS acesso__data_logout_SEC, a.data_logout_OFFSEC AS acesso__data_logout_OFFSEC FROM acesso a LEFT JOIN usuario u ON u.id = a.usuario_id_INT {$whereClause} {$orderByClause} {$limitClause}";

                $msg = $this->database->queryMensagem($queryWithoutLimit);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                
                $totalNumberOfRecords = $this->database->rows();
                if($totalNumberOfRecords == 0)
                {
                    return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                }                        
                        
                $msg = $this->database->queryMensagem($query);
                
                //operacao realizaca com sucesso
                if ($msg == null) 
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                    }                        
                
                    $resultSet = $this->database->result;

                    $objReturn = new stdClass();
                    $objReturn->dataSet = static::mapResultSetDataToList($resultSet, $listMappingType, static::$listAliasTypes);

                    if(!Helper::isNullOrEmpty($paginationParameters->recordsPerPage) && !Helper::isNullOrEmpty($paginationParameters->currentPage))
                    {
                        $lastPage = ceil($totalNumberOfRecords / $paginationParameters->recordsPerPage);
                        $objReturn->paginationParameters = new PaginationParams($paginationParameters->recordsPerPage, $paginationParameters->currentPage, $lastPage);
                    }
                    
                    return new Mensagem_generica($objReturn);
                } 
                else
                {
                    return $msg;
                }                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }



public function getId()
            {
                return $this->id;
            }

public function getUsuarioId()
            {
                return $this->usuarioId;
            }

public function getUsuario_id_INT()
                {
                    return $this->usuarioId;
                }

public function getDataLoginSec()
            {
                return $this->dataLoginSec;
            }

public function getData_login_SEC()
                {
                    return $this->dataLoginSec;
                }

public function getDataLoginOffsec()
            {
                return $this->dataLoginOffsec;
            }

public function getData_login_OFFSEC()
                {
                    return $this->dataLoginOffsec;
                }

public function getDataLogoutSec()
            {
                return $this->dataLogoutSec;
            }

public function getData_logout_SEC()
                {
                    return $this->dataLogoutSec;
                }

public function getDataLogoutOffsec()
            {
                return $this->dataLogoutOffsec;
            }

public function getData_logout_OFFSEC()
                {
                    return $this->dataLogoutOffsec;
                }



function setId($value)
            {
                $this->id = $value;
            }

function setUsuarioId($value)
            {
                $this->usuarioId = $value;
            }

public function setUsuario_id_INT($value)
                { 
                    $this->usuarioId = $value; 
                }

function setDataLoginSec($value)
            {
                $this->dataLoginSec = $value;
            }

public function setData_login_SEC($value)
                { 
                    $this->dataLoginSec = $value; 
                }

function setDataLoginOffsec($value)
            {
                $this->dataLoginOffsec = $value;
            }

public function setData_login_OFFSEC($value)
                { 
                    $this->dataLoginOffsec = $value; 
                }

function setDataLogoutSec($value)
            {
                $this->dataLogoutSec = $value;
            }

public function setData_logout_SEC($value)
                { 
                    $this->dataLogoutSec = $value; 
                }

function setDataLogoutOffsec($value)
            {
                $this->dataLogoutOffsec = $value;
            }

public function setData_logout_OFFSEC($value)
                { 
                    $this->dataLogoutOffsec = $value; 
                }



public function clear()
        {$this->id = null;
$this->usuarioId = null;
if($this->objUsuario != null) unset($this->objUsuario);
$this->dataLoginSec = null;
$this->dataLoginOffsec = null;
$this->dataLogoutSec = null;
$this->dataLogoutOffsec = null;
if(is_array($this->arrEstadosCampos)){
                        unset($this->arrEstadosCampos);
                     $this->arrEstadosCampos = array();
                    }
                }

public function formatarParaSQL()
        {
if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->id)){
$this->id = $this->formatarIntegerParaComandoSQL($this->id);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->id);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->usuarioId)){
$this->usuarioId = $this->formatarIntegerParaComandoSQL($this->usuarioId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->usuarioId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->dataLoginSec)){
$this->dataLoginSec = $this->formatarIntegerParaComandoSQL($this->dataLoginSec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->dataLoginSec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->dataLoginOffsec)){
$this->dataLoginOffsec = $this->formatarIntegerParaComandoSQL($this->dataLoginOffsec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->dataLoginOffsec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->dataLogoutSec)){
$this->dataLogoutSec = $this->formatarIntegerParaComandoSQL($this->dataLogoutSec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->dataLogoutSec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->dataLogoutOffsec)){
$this->dataLogoutOffsec = $this->formatarIntegerParaComandoSQL($this->dataLogoutOffsec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->dataLogoutOffsec);
}

}

public function formatarParaExibicao()
        {}

function select($id, $idCorporacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }   

            $sql = "SELECT id, usuario_id_INT, data_login_SEC, data_login_OFFSEC, data_logout_SEC, data_logout_OFFSEC FROM acesso WHERE id = $id";

            $msg = $this->database->queryMensagem($sql);
            $result = $this->database->result;

            if($msg != null && $msg->erro() )
            { 
            		$this->clear();
            		return $msg;
        	} 
        	else if($this->database->rows() == 0)
        	{
                return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nenhum registro foi encontrado."));
            }
        

            $row = $this->database->fetchArray(MYSQLI_NUM);
            
            		$this->id = $row[0];
		$this->usuarioId = $row[1];
		$this->dataLoginSec = $row[2];
		$this->dataLoginOffsec = $row[3];
		$this->dataLogoutSec = $row[4];
		$this->dataLogoutOffsec = $row[5];

            
        }

public function delete($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (is_null($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }

            $sql = "DELETE FROM acesso WHERE id= $id";
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                    && $sincronizar
                        && static::idSistemaTabela() != null
                            && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_REMOVE,
                                null,
                                null,
                                $id,
                                $this->database);
            }

            return $msg;
        }

public function insert($sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null, $opcoes = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            
            
            if (is_null($opcoes))
            {
                $opcoes = $this->opcoesDAO;
            }

            if (!is_numeric($this->id))
            {
                $this->id = "null";
                if ($opcoes == null || $opcoes[Database::OPCAO_GERAR_ID] != false) 
                {
                    $this->id = EXTDAO_Sistema_sequencia::gerarId(static::nomeTabela());                    
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }    
                    else if($msg == null)
                    {
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
                else 
                {
                    $this->id = null;
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao));
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }
                    else if($msg == null)
                    {
                        $this->id = $this->database->getLastInsertId();
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
            }
            else 
            {
                $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }                        
                else if($msg == null)
                {
                   $msg = new Mensagem_token();
                   $msg->mValor = $this->id;
                }
            }
            if($msg == null || $msg->ok())
            {                
                if($sincronizar
                    && static::idSistemaTabela() != null
                    && isset($this->id)){
                    
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_INSERT,
                                null,
                                null,
                                $this->id,
                                $this->database); 
                	}
            } 
            else
            {
                $this->id = null;
            }
            
            return $msg;
            
        }

public function getSQLInsert($idCorporacao){ 

return "INSERT INTO acesso (id, usuario_id_INT, data_login_SEC, data_login_OFFSEC, data_logout_SEC, data_logout_OFFSEC) VALUES ( $this->id ,  $this->usuarioId ,  $this->dataLoginSec ,  $this->dataLoginOffsec ,  $this->dataLogoutSec ,  $this->dataLogoutOffsec ) "; 

}




        
        public function updateByParameters($id, $objParametros, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao))
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();

            
                
                if (isset($objParametros->usuarioId)) 
                {
                    $arrUpdateFields[] = " usuario_id_INT = {$objParametros->usuarioId} ";
                }


                
                if (isset($objParametros->dataLoginSec)) 
                {
                    $arrUpdateFields[] = " data_login_SEC = {$objParametros->dataLoginSec} ";
                }


                
                if (isset($objParametros->dataLoginOffsec)) 
                {
                    $arrUpdateFields[] = " data_login_OFFSEC = {$objParametros->dataLoginOffsec} ";
                }


                
                if (isset($objParametros->dataLogoutSec)) 
                {
                    $arrUpdateFields[] = " data_logout_SEC = {$objParametros->dataLogoutSec} ";
                }


                
                if (isset($objParametros->dataLogoutOffsec)) 
                {
                    $arrUpdateFields[] = " data_logout_OFFSEC = {$objParametros->dataLogoutOffsec} ";
                }



            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE acesso SET {$strUpdateFields} WHERE id = {$id}";

            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null)
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function update($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($this->usuarioId)) 
                {                                      
                    $arrUpdateFields[] = " usuario_id_INT = {$this->usuarioId} ";
                }


                
                if (isset($this->dataLoginSec)) 
                {                                      
                    $arrUpdateFields[] = " data_login_SEC = {$this->dataLoginSec} ";
                }


                
                if (isset($this->dataLoginOffsec)) 
                {                                      
                    $arrUpdateFields[] = " data_login_OFFSEC = {$this->dataLoginOffsec} ";
                }


                
                if (isset($this->dataLogoutSec)) 
                {                                      
                    $arrUpdateFields[] = " data_logout_SEC = {$this->dataLogoutSec} ";
                }


                
                if (isset($this->dataLogoutOffsec)) 
                {                                      
                    $arrUpdateFields[] = " data_logout_OFFSEC = {$this->dataLogoutOffsec} ";
                }


            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE acesso SET {$strUpdateFields} WHERE id = {$id}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function updateByInstanceUsingParameters($id, $parameters, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($parameters->usuarioId)) 
                {                                      
                    $arrUpdateFields[] = " usuario_id_INT = {$this->usuarioId} ";
                }
                
                if (isset($parameters->dataLoginSec)) 
                {                                      
                    $arrUpdateFields[] = " data_login_SEC = {$this->dataLoginSec} ";
                }
                
                if (isset($parameters->dataLoginOffsec)) 
                {                                      
                    $arrUpdateFields[] = " data_login_OFFSEC = {$this->dataLoginOffsec} ";
                }
                
                if (isset($parameters->dataLogoutSec)) 
                {                                      
                    $arrUpdateFields[] = " data_logout_SEC = {$this->dataLogoutSec} ";
                }
                
                if (isset($parameters->dataLogoutOffsec)) 
                {                                      
                    $arrUpdateFields[] = " data_logout_OFFSEC = {$this->dataLogoutOffsec} ";
                }
            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE acesso SET {$strUpdateFields} WHERE id = {$id}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }

}

?>
