"use strict";
exports.__esModule = true;
var Serializer_JSON = (function () {
    function Serializer_JSON() {
    }
    Serializer_JSON.prototype.serialize = function (data) {
        return JSON.stringify(data);
    };
    return Serializer_JSON;
}());
exports.Serializer_JSON = Serializer_JSON;
