"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var Mapper_1 = require("../../../mapper/Mapper");
var Cache_DSO_TTL_DSO = (function (_super) {
    __extends(Cache_DSO_TTL_DSO, _super);
    function Cache_DSO_TTL_DSO(lifetime, data) {
        var _this = _super.call(this) || this;
        _this._lifetime = lifetime;
        _this._data = data;
        return _this;
    }
    Object.defineProperty(Cache_DSO_TTL_DSO.prototype, "lifetime", {
        get: function () {
            return this._lifetime;
        },
        set: function (lifetime) {
            this._lifetime = lifetime;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Cache_DSO_TTL_DSO.prototype, "data", {
        get: function () {
            return this._data;
        },
        set: function (data) {
            this._data = data;
        },
        enumerable: true,
        configurable: true
    });
    Cache_DSO_TTL_DSO.fromObject = function (object) {
        _super.fromObject.call(this, object);
        return new Cache_DSO_TTL_DSO(object._lifetime, object._data);
    };
    return Cache_DSO_TTL_DSO;
}(Mapper_1.Mapper));
exports.Cache_DSO_TTL_DSO = Cache_DSO_TTL_DSO;
