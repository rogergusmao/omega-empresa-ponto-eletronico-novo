/*

Arquivo gerado atrav�s de gerador de c�digo em 21/01/2018 as 18:27:39.
Para que o arquivo n�o seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

Tabela correspondente: pessoa
Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

*/

omegaApp.generatedFormPessoaController = function ($scope, $rootScope, $element, $window, $interval, $timeout, dataService, Upload, vm)
{
    try
    {
        vm.rootControllerElement = $element;
        vm.fieldsTypes = {};

        vm.isModalView = false;
        vm.isEdition = false;
        vm.entityIdForEdition = null;

        vm.messages = {
            error: [],
            warning: [],
            info: [],
            success: []
        };

        vm.formData = {
            identificador: null,
            nome: null,
            tipoDocumentoId: null,
            numeroDocumento: null,
            isConsumidorBoolean: false,
            sexoId: null,
            telefone: null,
            celular: null,
            operadoraId: null,
            celularSms: null,
            email: null,
            logradouro: null,
            numero: null,
            complemento: null,
            cidadeId: null,
            bairroId: null
        };

        vm.disabledFields = {
            fieldIdentificador: false,
            fieldNome: false,
            fieldTipoDocumentoId: false,
            fieldNumeroDocumento: false,
            fieldIsConsumidorBoolean: false,
            fieldSexoId: false,
            fieldTelefone: false,
            fieldCelular: false,
            fieldOperadoraId: false,
            fieldCelularSms: false,
            fieldEmail: false,
            fieldLogradouro: false,
            fieldNumero: false,
            fieldComplemento: false,
            fieldCidadeId: false,
            fieldBairroId: false
        };

        vm.requiredFields = {
            fieldIdentificador: true,
            fieldNome: true,
            fieldTipoDocumentoId: true,
            fieldNumeroDocumento: true,
            fieldIsConsumidorBoolean: true,
            fieldSexoId: true,
            fieldTelefone: true,
            fieldCelular: true,
            fieldOperadoraId: true,
            fieldCelularSms: true,
            fieldEmail: true,
            fieldLogradouro: true,
            fieldNumero: true,
            fieldComplemento: true,
            fieldCidadeId: true,
            fieldBairroId: true
        };

        vm.relatedEntities =
            {};

        vm.fieldsParameters = {
            fieldIdentificador: null,
            fieldNome: null,
            fieldTipoDocumentoId: {remoteData: null},
            fieldNumeroDocumento: null,
            fieldIsConsumidorBoolean: null,
            fieldSexoId: {remoteData: null},
            fieldTelefone: null,
            fieldCelular: null,
            fieldOperadoraId: {remoteData: null},
            fieldCelularSms: null,
            fieldEmail: null,
            fieldLogradouro: null,
            fieldNumero: null,
            fieldComplemento: null,
            fieldCidadeId: {remoteData: null},
            fieldBairroId: {remoteData: null}
        };

        vm.arrForeignKeys = ['tipoDocumentoId', 'sexoId', 'operadoraId', 'cidadeId', 'bairroId', 'corporacaoId'];

        var relationshipObject = {
            tipoDocumentoId: 'fieldTipoDocumentoId',
            sexoId: 'fieldSexoId',
            operadoraId: 'fieldOperadoraId',
            cidadeId: 'fieldCidadeId',
            bairroId: 'fieldBairroId'
        };

        vm.isInitialLoadFinished = false;
        var initialLoading = function ()
        {
            $timeout(function ()
            {
                if (AngularControllerUtil.isEditionMode(vm))
                {

                    var filterParameters = AngularControllerUtil.getFilterParametersForEdition(vm);
                    dataService.getPessoa(filterParameters).then(function (responseContent)
                    {
                        if (RemoteDataUtil.validateRemoteResponse(responseContent, [PROTOCOLO_SISTEMA.OPERACAO_REALIZADA_COM_SUCESSO]))
                        {
                            var responseObject = RemoteDataUtil.getResponseObject(responseContent);
                            RemoteDataUtil.refactorInitialFormLoadRemoteResponseData(vm, responseObject, relationshipObject);
                            OmegaFormUtil.expandSectionPanels(vm);
                        }
                        else
                        {
                            RemoteDataUtil.defaultRemoteErrorHandling(responseContent, vm);
                        }

                        $timeout(function ()
                        {
                            vm.isInitialLoadFinished = true;
                        }, 100);

                    });
                }
                else
                {
                    dataService.getAllComboBoxesDataForPessoaForm().then(function (returnContent)
                    {
                        if (RemoteDataUtil.validateRemoteResponse(returnContent))
                        {
                            var responseObject = RemoteDataUtil.getResponseObject(returnContent);
                            RemoteDataUtil.refactorInitialFormLoadRemoteResponseData(vm, responseObject, relationshipObject);
                        }
                        else
                        {
                            LogUtil.logRemoteResponseError(returnContent);
                        }

                        $timeout(function ()
                        {
                            vm.isInitialLoadFinished = true;
                        }, 100);

                    });
                }

            }, 200);

        };

        vm.submitForm = function (formInstance)
        {
            if ($rootScope.validateForm(formInstance, vm))
            {
                var postData = RemoteDataUtil.refactorRemoteRequest(vm.formData, vm.relatedEntities);
                if (AngularControllerUtil.isEditionMode(vm))
                {
                    dataService.editPessoa(postData).then(function (returnContent)
                    {
                        if (RemoteDataUtil.validateRemoteResponse(returnContent))
                        {
                            var successMessage = RemoteDataUtil.getResponseMessageString(returnContent);
                            $rootScope.defaultAfterUpdateAction(vm, 'after-pessoa-update-template.html', successMessage);
                        }
                        else
                        {
                            RemoteDataUtil.defaultRemoteErrorHandling(returnContent, vm);
                        }
                    });
                }
                else
                {
                    dataService.addPessoa(postData).then(function (returnContent)
                    {
                        if (RemoteDataUtil.validateRemoteResponse(returnContent))
                        {
                            var successMessage = RemoteDataUtil.getResponseMessageString(returnContent);
                            $rootScope.defaultAfterInsertAction(vm, 'after-pessoa-insert-template.html', successMessage);
                        }
                        else
                        {
                            RemoteDataUtil.defaultRemoteErrorHandling(returnContent, vm);
                        }
                    });
                }
            }

        };

        vm.openTipoDocumentoIdModal = function ()
        {
            var modalIndex = $rootScope.getNextModalIndex();
            $rootScope.registerModalInstance($rootScope.showModalDialog('forms', 'tipoDocumento', modalIndex));

            //atualizando valores da lista
            var unregisterModal = $rootScope.$watch(function ()
            {
                return $rootScope.uibModalInstance[modalIndex]
            }, function ()
            {

                if ($rootScope.uibModalInstance[modalIndex] == null)
                {
                    vm.getListOfTipoDocumentoId();
                    vm.formData.tipoDocumentoId = null;
                    unregisterModal();
                }

            });

        };
        vm.openSexoIdModal = function ()
        {
            var modalIndex = $rootScope.getNextModalIndex();
            $rootScope.registerModalInstance($rootScope.showModalDialog('forms', 'sexo', modalIndex));

            //atualizando valores da lista
            var unregisterModal = $rootScope.$watch(function ()
            {
                return $rootScope.uibModalInstance[modalIndex]
            }, function ()
            {

                if ($rootScope.uibModalInstance[modalIndex] == null)
                {
                    vm.getListOfSexoId();
                    vm.formData.sexoId = null;
                    unregisterModal();
                }

            });

        };
        vm.openOperadoraIdModal = function ()
        {
            var modalIndex = $rootScope.getNextModalIndex();
            $rootScope.registerModalInstance($rootScope.showModalDialog('forms', 'operadora', modalIndex));

            //atualizando valores da lista
            var unregisterModal = $rootScope.$watch(function ()
            {
                return $rootScope.uibModalInstance[modalIndex]
            }, function ()
            {

                if ($rootScope.uibModalInstance[modalIndex] == null)
                {
                    vm.getListOfOperadoraId();
                    vm.formData.operadoraId = null;
                    unregisterModal();
                }

            });

        };
        vm.openCidadeIdModal = function ()
        {
            var modalIndex = $rootScope.getNextModalIndex();
            $rootScope.registerModalInstance($rootScope.showModalDialog('forms', 'cidade', modalIndex));

            //atualizando valores da lista
            var unregisterModal = $rootScope.$watch(function ()
            {
                return $rootScope.uibModalInstance[modalIndex]
            }, function ()
            {

                if ($rootScope.uibModalInstance[modalIndex] == null)
                {
                    vm.getListOfCidadeId();
                    vm.formData.cidadeId = null;
                    unregisterModal();
                }

            });

        };
        vm.openBairroIdModal = function ()
        {
            var modalIndex = $rootScope.getNextModalIndex();
            $rootScope.registerModalInstance($rootScope.showModalDialog('forms', 'bairro', modalIndex));

            //atualizando valores da lista
            var unregisterModal = $rootScope.$watch(function ()
            {
                return $rootScope.uibModalInstance[modalIndex]
            }, function ()
            {

                if ($rootScope.uibModalInstance[modalIndex] == null)
                {
                    vm.getListOfBairroId();
                    vm.formData.bairroId = null;
                    unregisterModal();
                }

            });

        };

        vm.getListOfTipoDocumentoId = function ()
        {
            if (!vm.isInitialLoadFinished)
            {
                return;
            }

            dataService.getListOfTipoDocumentoForPessoaForm().then(function (returnContent)
            {
                if (RemoteDataUtil.validateRemoteResponse(returnContent))
                {
                    var remoteDataSet = RemoteDataUtil.getResponseObjectDataSetOrEmptyArray(returnContent);
                    vm.fieldsParameters.fieldTipoDocumentoId.remoteData = remoteDataSet;
                }
                else
                {
                    LogUtil.logRemoteResponseError(returnContent);
                }

            });

        };
        vm.getListOfSexoId = function ()
        {
            if (!vm.isInitialLoadFinished)
            {
                return;
            }

            dataService.getListOfSexoForPessoaForm().then(function (returnContent)
            {
                if (RemoteDataUtil.validateRemoteResponse(returnContent))
                {
                    var remoteDataSet = RemoteDataUtil.getResponseObjectDataSetOrEmptyArray(returnContent);
                    vm.fieldsParameters.fieldSexoId.remoteData = remoteDataSet;
                }
                else
                {
                    LogUtil.logRemoteResponseError(returnContent);
                }

            });

        };
        vm.getListOfOperadoraId = function ()
        {
            if (!vm.isInitialLoadFinished)
            {
                return;
            }

            dataService.getListOfOperadoraForPessoaForm().then(function (returnContent)
            {
                if (RemoteDataUtil.validateRemoteResponse(returnContent))
                {
                    var remoteDataSet = RemoteDataUtil.getResponseObjectDataSetOrEmptyArray(returnContent);
                    vm.fieldsParameters.fieldOperadoraId.remoteData = remoteDataSet;
                }
                else
                {
                    LogUtil.logRemoteResponseError(returnContent);
                }

            });

        };
        vm.getListOfCidadeId = function ()
        {
            if (!vm.isInitialLoadFinished)
            {
                return;
            }

            dataService.getListOfCidadeForPessoaForm().then(function (returnContent)
            {
                if (RemoteDataUtil.validateRemoteResponse(returnContent))
                {
                    var remoteDataSet = RemoteDataUtil.getResponseObjectDataSetOrEmptyArray(returnContent);
                    vm.fieldsParameters.fieldCidadeId.remoteData = remoteDataSet;
                }
                else
                {
                    LogUtil.logRemoteResponseError(returnContent);
                }

            });

        };
        vm.getListOfBairroId = function ()
        {
            if (!vm.isInitialLoadFinished)
            {
                return;
            }

            dataService.getListOfBairroForPessoaForm().then(function (returnContent)
            {
                if (RemoteDataUtil.validateRemoteResponse(returnContent))
                {
                    var remoteDataSet = RemoteDataUtil.getResponseObjectDataSetOrEmptyArray(returnContent);
                    vm.fieldsParameters.fieldBairroId.remoteData = remoteDataSet;
                }
                else
                {
                    LogUtil.logRemoteResponseError(returnContent);
                }

            });

        };

        vm.loadListOfPessoa = function ()
        {
            document.location.href = 'index.php?tipo=lists&page=pessoa';
        };

        vm.addNewPessoa = function ()
        {
            document.location.href = document.location.href;
        };

        return {

            scope: this,
            initFunction: initialLoading

        };

    }
    catch (ex)
    {
        ApplicationUtil.showErrorMessage(ex.message, 'Erro no controller generatedFormPessoaController', vm);
    }

}