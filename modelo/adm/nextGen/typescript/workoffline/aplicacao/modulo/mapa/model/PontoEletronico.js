"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var model_1 = require("lib/model");
var geral_1 = require("aplicacao/comum/geral");
var geral_2 = require("lib/geral");
var PontoEletronico = (function (_super) {
    __extends(PontoEletronico, _super);
    function PontoEletronico() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    PontoEletronico.factory = function (remoteObject) {
        var instance = new PontoEletronico();
        instance.latitude = geral_2.GeoLocationHelper.getFloatValue(remoteObject.latitude);
        instance.longitude = geral_2.GeoLocationHelper.getFloatValue(remoteObject.longitude);
        instance.nomePessoa = remoteObject.nomePessoa;
        instance.idPessoa = geral_2.Helper.parseInt(remoteObject.idPessoa);
        instance.dataPonto = DateUtil.getDateObject(geral_2.Helper.parseInt(remoteObject.dataSec) + geral_2.Helper.parseInt(remoteObject.dataOffsec));
        instance.tipoPonto = geral_2.Helper.parseInt(remoteObject.isEntrada) == 1 ? geral_1.TipoPontoEletronico.PONTO_ENTRADA : geral_1.TipoPontoEletronico.PONTO_SAIDA;
        return instance;
    };
    return PontoEletronico;
}(model_1.Model));
exports.PontoEletronico = PontoEletronico;
