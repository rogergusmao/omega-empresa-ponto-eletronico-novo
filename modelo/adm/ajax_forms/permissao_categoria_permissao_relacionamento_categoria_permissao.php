<?php

/*
*
* -------------------------------------------------------
* NOME DO FORMUL�RIO: permissao_categoria_permissao
* DATA DE GERA��O:    25.06.2012
* ARQUIVO:            permissao_categoria_permissao.php
* TABELA MYSQL:       permissao_categoria_permissao
* BANCO DE DADOS:     ponto_eletronico
* -------------------------------------------------------
*
*/

if (isset($_GET["contador"]))
{

    $numeroRegistroInterno = Helper::GET("contador");
}

if (isset($identificadorRelacionamento) && is_numeric($identificadorRelacionamento))
{

    $objPermissao_categoria_permissao = new EXTDAO_Permissao_categoria_permissao();
    $objPermissao_categoria_permissao->select($identificadorRelacionamento);
}
else
{

    $objPermissao_categoria_permissao = new EXTDAO_Permissao_categoria_permissao();
}

$objArgPermissao_categoria_permissao = new Generic_Argument();
$objPermissao_categoria_permissao->formatarParaExibicao();

?>

<input type="hidden" name="permissao_categoria_permissao_id_<?= $numeroRegistroInterno ?>"
       id="permissao_categoria_permissao_id_<?= $numeroRegistroInterno ?>"
       value="<?= $objPermissao_categoria_permissao->getId(); ?>">

<table class="tabela_form">

    <tr class="tr_form">


        <?

        $objArgPermissao_categoria_permissao = new Generic_Argument();

        $objArgPermissao_categoria_permissao->numeroDoRegistro = "";
        $objArgPermissao_categoria_permissao->label = $objPermissao_categoria_permissao->label_permissao_id_INT;
        $objArgPermissao_categoria_permissao->valor = $objPermissao_categoria_permissao->getPermissao_id_INT();
        $objArgPermissao_categoria_permissao->classeCss = "input_text";
        $objArgPermissao_categoria_permissao->classeCssFocus = "focus_text";
        $objArgPermissao_categoria_permissao->obrigatorio = false;
        $objArgPermissao_categoria_permissao->largura = 200;
        $objArgPermissao_categoria_permissao->nome = "permissao_categoria_permissao_permissao_id_INT_{$numeroRegistroInterno}";
        $objArgPermissao_categoria_permissao->id = "permissao_categoria_permissao_permissao_id_INT_{$numeroRegistroInterno}";

        $objPermissao_categoria_permissao->addInfoCampos("permissao_id_INT", $objArgPermissao_categoria_permissao->label, "TEXTO", $objArgPermissao_categoria_permissao->obrigatorio);

        ?>

        <td class="td_form_label"><?= $objArgPermissao_categoria_permissao->getLabel() ?></td>
        <td class="td_form_campo">
            <?= $objPermissao_categoria_permissao->objPermissao->getComboBox($objArgPermissao_categoria_permissao); ?>
        </td>


        <?

        $objArgPermissao_categoria_permissao = new Generic_Argument();

        $objArgPermissao_categoria_permissao->numeroDoRegistro = "";
        $objArgPermissao_categoria_permissao->label = $objPermissao_categoria_permissao->label_corporacao_id_INT;
        $objArgPermissao_categoria_permissao->valor = $objPermissao_categoria_permissao->getCorporacao_id_INT();
        $objArgPermissao_categoria_permissao->classeCss = "input_text";
        $objArgPermissao_categoria_permissao->classeCssFocus = "focus_text";
        $objArgPermissao_categoria_permissao->obrigatorio = false;
        $objArgPermissao_categoria_permissao->largura = 200;
        $objArgPermissao_categoria_permissao->nome = "permissao_categoria_permissao_corporacao_id_INT_{$numeroRegistroInterno}";
        $objArgPermissao_categoria_permissao->id = "permissao_categoria_permissao_corporacao_id_INT_{$numeroRegistroInterno}";

        $objPermissao_categoria_permissao->addInfoCampos("corporacao_id_INT", $objArgPermissao_categoria_permissao->label, "TEXTO", $objArgPermissao_categoria_permissao->obrigatorio);

        ?>

        <td class="td_form_label"><?= $objArgPermissao_categoria_permissao->getLabel() ?></td>
        <td class="td_form_campo">
            <?= $objPermissao_categoria_permissao->objCorporacao->getComboBox($objArgPermissao_categoria_permissao); ?>
        </td>


    </tr>
    <tr>
        <td colspan="4" class="td_botao_remover_da_lista"><input class="botoes_form" type="button"
                                                                 value="Remover Permiss�o Da Categoria De Permiss�o"
                                                                 onclick="javascript:removerDivAjaxEmLista(this);"></td>
    </tr>
</table><br/>

