<?php

/*

Arquivo gerado atravÃ©s de gerador de cÃ³digo em 24/05/2017 as 07:53:28.
Para que o arquivo nÃ£o seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

Tabela correspondente: uf
Sobrescrita de dados: ---SOBRESCRITA_NAO_AUTORIZADA---

*/
?>

<?php

class EXTDAO_Uf extends DAO_Uf
{
    public function __construct($configDAO = null)
    {
        parent::__construct($configDAO);
        $this->nomeClasse = "EXTDAO_Uf";
    }

    public function factory()
    {
        return new EXTDAO_Uf();
    }

    public static function setListAliasTypes()
    {
        if (is_null(static::$listAliasTypes))
        {
            static::$listAliasTypes = new stdClass();
            static::$listAliasTypes->pais__nome = static::TIPO_VARIAVEL_TEXT;
            static::$listAliasTypes->uf__id = static::TIPO_VARIAVEL_INTEGER;
            static::$listAliasTypes->uf__nome = static::TIPO_VARIAVEL_TEXT;
            static::$listAliasTypes->uf__sigla = static::TIPO_VARIAVEL_TEXT;
            static::$listAliasTypes->uf__corporacao_id_INT = static::TIPO_VARIAVEL_INTEGER;
        }
    }

    public static function setListAliasRelatedAttributes()
    {
        if (is_null(static::$listAliasRelatedAttributes))
        {
            static::$listAliasRelatedAttributes = new stdClass();
            static::$listAliasRelatedAttributes->pais__nome = "paisNome";
            static::$listAliasRelatedAttributes->uf__id = "id";
            static::$listAliasRelatedAttributes->uf__nome = "nome";
            static::$listAliasRelatedAttributes->uf__sigla = "sigla";
            static::$listAliasRelatedAttributes->uf__corporacao_id_INT = "corporacaoId";
        }
    }

    public function __getList($parameters = null, $idCorporacao = null)
    {
        if (is_null($parameters))
        {
            $parameters = Helper::getPhpInputObject();
        }

        try
        {
            $filterParameters = new stdClass();
            if (isset($parameters->filterParameters))
            {
                $filterParameters = $parameters->filterParameters;
            }

            if (is_null($idCorporacao))
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            ListParameters::addFilterParameterToObject($filterParameters, "corporacaoId", $idCorporacao);

            $sortingParameters = null;
            if (isset($parameters->sortingParameters))
            {
                $sortingParameters = $parameters->sortingParameters;
            }

            $paginationParameters = null;
            if (isset($parameters->paginationParameters))
            {
                $paginationParameters = $parameters->paginationParameters;
            }

            $listMappingType = static::getListMappingType($parameters->listMappingType);
            $mainTableAlias = "u";

            $whereClause = static::getWhereClauseForFilter($filterParameters, $mainTableAlias);
            $orderByClause = static::getOrderByClauseForFilter($sortingParameters, $mainTableAlias);
            $limitClause = static::getLimitClauseForFilter($paginationParameters);

            $queryWithoutLimit = "SELECT u.id FROM uf u {$whereClause}";
            $query = "SELECT p.nome AS pais__nome, u.id AS uf__id, u.nome AS uf__nome, u.sigla AS uf__sigla, u.corporacao_id_INT AS uf__corporacao_id_INT FROM uf u LEFT JOIN pais p ON p.id = u.pais_id_INT {$whereClause} {$orderByClause} {$limitClause}";

            $msg = $this->database->queryMensagem($queryWithoutLimit);
            if ($msg != null && $msg->erro())
            {
                return $msg;
            }

            $totalNumberOfRecords = $this->database->rows();
            if ($totalNumberOfRecords == 0)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
            }

            $msg = $this->database->queryMensagem($query);

            //operacao realizaca com sucesso
            if ($msg == null)
            {
                if ($this->database->rows() == 0)
                {
                    return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                }

                $resultSet = $this->database->result;

                $objReturn = new stdClass();
                $objReturn->dataSet = static::mapResultSetDataToList($resultSet, $listMappingType, static::$listAliasTypes);

                if (!Helper::isNullOrEmpty($paginationParameters->recordsPerPage) && !Helper::isNullOrEmpty($paginationParameters->currentPage))
                {
                    $lastPage = ceil($totalNumberOfRecords / $paginationParameters->recordsPerPage);
                    $objReturn->paginationParameters = new PaginationParams($paginationParameters->recordsPerPage, $paginationParameters->currentPage, $lastPage);
                }

                return new Mensagem_generica($objReturn);
            }
            else
            {
                return $msg;
            }
        }
        catch (Exception $ex)
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
        }
    }


}

?>
