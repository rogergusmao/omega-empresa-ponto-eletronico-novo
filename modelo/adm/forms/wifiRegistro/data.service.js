

        /*

        Arquivo gerado atrav�s de gerador de c�digo em 07/09/2017 as 12:49:44.
        Para que o arquivo n�o seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

        Tabela correspondente: wifi_registro
        Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

        */

returnObject.getWifiRegistro = function(parameters){

            return $http({
                url: baseURLAdm + "/webservice.php?class=EXTDAO_Wifi_registro&action=__getRecord",
                method: "POST",
                data: parameters,
                headers: {
                    'Content-Type': 'application/json'
                }
            });

        };



        returnObject.addWifiRegistro = function(formData){

            return $http({
                url: baseURLAdm + "/webservice.php?class=EXTDAO_Wifi_registro&jsonOutput=true&action=add",
                method: "POST",
                data: formData,
                headers: {
                    'Content-Type':'application/x-www-form-urlencoded'
                }
            });

        };

        returnObject.editWifiRegistro = function(formData){

            return $http({
                url: baseURLAdm + "/webservice.php?class=EXTDAO_Wifi_registro&jsonOutput=true&action=edit",
                method: "POST",
                data: formData,
                headers: {
                    'Content-Type':'application/x-www-form-urlencoded'
                }
            });

        };

returnObject.getListOfWifi = function()
                {
                    var params = { filterParameters: null, isRelatedEntity: true };
                    return $http.post(baseURLAdm + "/webservice.php?class=EXTDAO_Wifi&action=__getList", params);
                };
                returnObject.getListOfUsuario = function()
                {
                    var params = { filterParameters: null, isRelatedEntity: true };
                    return $http.post(baseURLAdm + "/webservice.php?class=EXTDAO_Usuario&action=__getList", params);
                };
                

