"use strict";
exports.__esModule = true;
var geral_1 = require("lib/geral");
var RandomColor = (function () {
    function RandomColor(includeAlpha) {
        this.mainColor;
        this.foregroundColor;
        var randomRed = RandomColor.getRandomChannelValue();
        var randomGreen = RandomColor.getRandomChannelValue();
        var randomBlue = RandomColor.getRandomChannelValue();
        if (includeAlpha) {
            var randomAlpha = RandomColor.getRandomChannelValue();
            this.mainColor = new geral_1.Color(randomRed, randomGreen, randomBlue, randomAlpha);
        }
        else {
            this.mainColor = new geral_1.Color(randomRed, randomGreen, randomBlue);
        }
        //cor principal escura
        if (0.299 * randomRed +
            0.587 * randomGreen +
            0.114 * randomBlue <= 128) {
            this.foregroundColor = new geral_1.Color(255, 255, 255);
        }
        else {
            this.foregroundColor = new geral_1.Color(0, 0, 0);
        }
    }
    RandomColor.getRandomChannelValue = function () {
        return Math.round(Math.random() * 255);
    };
    RandomColor.prototype.convertToHex = function (r, g, b, a) {
        var pieces = new Array();
        var redHex = r.toString(16);
        pieces.push(redHex.length == 1 ? "0" + redHex : redHex);
        var greenHex = g.toString(16);
        pieces.push(greenHex.length == 1 ? "0" + greenHex : greenHex);
        var blueHex = b.toString(16);
        pieces.push(blueHex.length == 1 ? "0" + blueHex : blueHex);
        if (a != null) {
            var alphaHex = a.toString(16);
            pieces.push(alphaHex.length == 1 ? "0" + alphaHex : alphaHex);
        }
        return "#" + pieces.join('');
    };
    return RandomColor;
}());
exports.RandomColor = RandomColor;
