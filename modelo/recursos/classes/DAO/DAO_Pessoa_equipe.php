<?php


        /*

        Arquivo gerado através de gerador de código em 12/05/2018 as 14:48:52.
        Para que o arquivo não seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

        Tabela correspondente: pessoa_equipe
        Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

        */
        


class DAO_Pessoa_equipe extends Generic_DAO
                        {
                            const NOME_ENTIDADE = "pessoa_equipe";

                            public static $nomeTabela = null;
                            public static $idSistemaTabela = false;
                            public static $databaseFieldsRelatedAttributes = null;
	                        public static $databaseFieldTypes = null;
	                        public static $databaseFieldNames = null;
	                        public static $listAliasTypes = null;
	                        public static $listAliasRelatedAttributes = null;
	                        
	                        public $id;
public $empresaEquipeId;
public $usuarioId;
public $pessoaId;
public $corporacaoId;

public $labelId;
public $labelEmpresaEquipeId;
public $labelUsuarioId;
public $labelPessoaId;
public $labelCorporacaoId;

public static $arrPK = array("id");

public function __construct($configDAO = null)
        {
            parent::__construct($configDAO);
            static::__setStaticValues($this->database);           
        }

public static function idSistemaTabela($databaseObject = null){
            if(!(static::$idSistemaTabela))
            {
                static::$idSistemaTabela = EXTDAO_Sistema_tabela::getIdDaSistemaTabela(static::nomeTabela(), $databaseObject);
            }
            return static::$idSistemaTabela;
        }

public function getIdSistemaTabela(){
            return static::idSistemaTabela();
        }

public static function nomeTabela(){
            if(is_null(static::$nomeTabela))
            {
                static::$nomeTabela = "pessoa_equipe";
            }
            return static::$nomeTabela;
        }

public function getNomeTabela(){
            return static::nomeTabela();
        }

public static function __setStaticValues($databaseObject = null)
                   {    
                        static::nomeTabela();
                        
                        static::idSistemaTabela($databaseObject);
                   
                        static::setDatabaseFieldNames();
                        static::setDatabaseFieldTypes();
                        static::setDatabaseFieldsRelatedAttributeNames();
                        
                        static::setListAliasTypes();
                        static::setListAliasRelatedAttributes();
                   }

public function getExpressaoLabel()
                   {
                        return "{id}";
                   }

public static function setDatabaseFieldNames()
        {
            if(is_null(static::$databaseFieldNames))
            {
                static::$databaseFieldNames = new stdClass();
                static::$databaseFieldNames->id = "id";
static::$databaseFieldNames->empresaEquipeId = "empresa_equipe_id_INT";
static::$databaseFieldNames->usuarioId = "usuario_id_INT";
static::$databaseFieldNames->pessoaId = "pessoa_id_INT";
static::$databaseFieldNames->corporacaoId = "corporacao_id_INT";

            }
                      
        }

public static function setDatabaseFieldTypes()
        {
            if(is_null(static::$databaseFieldTypes))
            {
                static::$databaseFieldTypes = new stdClass();
                static::$databaseFieldTypes->id = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->empresaEquipeId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->usuarioId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->pessoaId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->corporacaoId = static::TIPO_VARIAVEL_INTEGER;

            }
            
        }

public static function setDatabaseFieldsRelatedAttributeNames()
        {
            if(is_null(static::$databaseFieldsRelatedAttributes))
            {
                static::$databaseFieldsRelatedAttributes = new stdClass();
                static::$databaseFieldsRelatedAttributes->id = "id";
static::$databaseFieldsRelatedAttributes->empresa_equipe_id_INT = "empresaEquipeId";
static::$databaseFieldsRelatedAttributes->usuario_id_INT = "usuarioId";
static::$databaseFieldsRelatedAttributes->pessoa_id_INT = "pessoaId";
static::$databaseFieldsRelatedAttributes->corporacao_id_INT = "corporacaoId";

            }           
        }

public function setAllFieldsToHumanFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["empresa_equipe_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["usuario_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["pessoa_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
}

public function setAllFieldsToDatabaseFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["empresa_equipe_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["usuario_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["pessoa_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
}

  public function getFkObjEmpresa_equipe() 
                {
                    if($this->objEmpresa_equipe == null)
                    {                        
                        $this->objEmpresa_equipe = new EXTDAO_Empresa_equipe_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getEmpresa_equipe_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objEmpresa_equipe->clear();
                    }
                    elseif($this->objEmpresa_equipe->getId() != $idFK)
                    {
                        $this->objEmpresa_equipe->select($idFK);
                    }
                    return $this->objEmpresa_equipe;
                }
  public function getFkObjUsuario() 
                {
                    if($this->objUsuario == null)
                    {                        
                        $this->objUsuario = new EXTDAO_Usuario_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getUsuario_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objUsuario->clear();
                    }
                    elseif($this->objUsuario->getId() != $idFK)
                    {
                        $this->objUsuario->select($idFK);
                    }
                    return $this->objUsuario;
                }
  public function getFkObjPessoa() 
                {
                    if($this->objPessoa == null)
                    {                        
                        $this->objPessoa = new EXTDAO_Pessoa_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getPessoa_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objPessoa->clear();
                    }
                    elseif($this->objPessoa->getId() != $idFK)
                    {
                        $this->objPessoa->select($idFK);
                    }
                    return $this->objPessoa;
                }
  public function getFkObjCorporacao() 
                {
                    if($this->objCorporacao == null)
                    {                        
                        $this->objCorporacao = new EXTDAO_Corporacao_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getCorporacao_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objCorporacao->clear();
                    }
                    elseif($this->objCorporacao->getId() != $idFK)
                    {
                        $this->objCorporacao->select($idFK);
                    }
                    return $this->objCorporacao;
                }


public function setLabels()
        {$this->labelId = "";
$this->labelEmpresaEquipeId = "";
$this->labelUsuarioId = "";
$this->labelPessoaId = "";
$this->labelCorporacaoId = "";
}

public function __actionAdd($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                $this->setByObject($parameters);
                $this->setDataCadastroAndOffsetInSecondsFromUser($parameters);
                
                $this->formatarParaSQL();
                $msg = $this->insert(true);
                
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                return new Mensagem(null, I18N::getExpression("Pessoa equipe adicionado com sucesso."));
                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionEdit($parameters = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try {

                $this->setByObject($parameters);
                $this->setAllFieldsToHumanFormat();
                
                $this->formatarParaSQL();
                $msg = $this->updateByInstanceUsingParameters($this->id, $parameters, true);
                if($msg != null && $msg->erro())
                {
                     return $msg;
                }
        
                return new Mensagem(null, I18N::getExpression("Pessoa equipe editado com sucesso."));
            }
            catch(Exception $ex)
            {                
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionRemove($parameters = null)
        {
            $successulRemovalCount = 0;
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                foreach($parameters as $record)
                {
                    if(is_numeric($record->id))
                    {
                        $msg = $this->delete($record->id, true);
                        if($msg == null || $msg->ok())
                        {
                            $successulRemovalCount++;
                        }
                    }
                }
                
                if($successulRemovalCount > 0)
                {
                    $successMessage = count($successulRemovalCount) > 1 ? I18N::getExpression("{0} Pessoa equipe foram removidos com sucesso.", $successulRemovalCount) : I18N::getExpression("Pessoa equipe removido com sucesso.");
                    return new Mensagem(null, $successMessage);
                }
                else
                {
                    $errorMessage = count($parameters) > 1 ? I18N::getExpression("Falha ao remover Pessoa equipe.") : I18N::getExpression("Falha ao remover Pessoa equipe.");
                    return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, $errorMessage);                    
                }
            }
            catch (Exception $ex) 
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }

public function __getRecord($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }
            
            try
            {            
                $returnObject = new stdClass();            
                $whereClause = static::getWhereClauseForFilter($parameters->filterParameters);
                $query = "SELECT  id, empresa_equipe_id_INT, usuario_id_INT, pessoa_id_INT, corporacao_id_INT FROM pessoa_equipe {$whereClause}";
    
                $msg = $this->database->queryMensagem($query);                
                if ($msg == null)
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Não foi encontrado nenhum registro"));
                    }                    
                    $resultSet = $this->database->result;
                    $resultObject = static::mapResultSetDataToRecord($resultSet);
                    
                    $returnObject->formData = $resultObject;
                    $returnObject->comboBoxesData = null;
                    if($parameters->loadComboBoxesData)
                    {
                        $this->setByObject($resultObject);
                        $returnObject->comboBoxesData =  $this->getFormComboBoxesData();
                    }                    
    
                    
    
                    return new Mensagem_generica($returnObject);
                } 
                else 
                {
                    return $msg;
                }
                
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }


        
        public function __getFilterComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
                
        public function __getFormComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
        
        protected function getFormComboBoxesData()
        {
            $comboBoxesData = new stdClass();
            $listParameters = new stdClass();
            $listParameters->listMappingType = static::LIST_MAPPING_TYPE_COMBOBOX;
            
            
                    
                    $objEmpresaEquipe = new EXTDAO_Empresa_equipe();
                    $comboBoxesData->fieldEmpresaEquipeId = $objEmpresaEquipe->__getList($listParameters);
                    
                    $objUsuario = new EXTDAO_Usuario();
                    $comboBoxesData->fieldUsuarioId = $objUsuario->__getList($listParameters);
                    
                    $objPessoa = new EXTDAO_Pessoa();
                    $comboBoxesData->fieldPessoaId = $objPessoa->__getList($listParameters);

            return $comboBoxesData;
        }
        
        


        
        public static function setListAliasTypes()
        {
            if(is_null(static::$listAliasTypes))
            {
                static::$listAliasTypes = new stdClass();
                static::$listAliasTypes->empresa_equipe__nome = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->usuario__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->pessoa__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->pessoa_equipe__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->pessoa_equipe__corporacao_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->pessoa_equipe__cadastro_OFFSEC = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$listAliasTypes->pessoa_equipe__ind_celular_valido_BOOLEAN = static::TIPO_VARIAVEL_BOOLEAN;
            }        
        }
        
        public static function setListAliasRelatedAttributes()
        {
            if(is_null(static::$listAliasRelatedAttributes))
            {
                static::$listAliasRelatedAttributes = new stdClass();
                static::$listAliasRelatedAttributes->empresa_equipe__nome = "empresaEquipeNome";
static::$listAliasRelatedAttributes->usuario__id = "usuarioId";
static::$listAliasRelatedAttributes->pessoa__id = "pessoaId";
static::$listAliasRelatedAttributes->pessoa_equipe__id = "id";
static::$listAliasRelatedAttributes->pessoa_equipe__corporacao_id_INT = "corporacaoId";
static::$listAliasRelatedAttributes->pessoa_equipe__cadastro_OFFSEC = "cadastroOffsec";
static::$listAliasRelatedAttributes->pessoa_equipe__ind_celular_valido_BOOLEAN = "indCelularValidoBoolean";
            }         
        }

        public function __getList($parameters = null, $idCorporacao = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try
            {
                $filterParameters = new stdClass();
                if(isset($parameters->filterParameters))
                {
                    $filterParameters = $parameters->filterParameters;
                }
                                    
                            
            if(is_null($idCorporacao))
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            ListParameters::addFilterParameterToObject($filterParameters, "corporacaoId", $idCorporacao);           
                
                
                $sortingParameters = null;
                if(isset($parameters->sortingParameters))
                {
                    $sortingParameters = $parameters->sortingParameters;
                }
                
                $paginationParameters = null;
                if(isset($parameters->paginationParameters))
                {
                    $paginationParameters = $parameters->paginationParameters;
                }
                
                $listMappingType = static::getListMappingType($parameters->listMappingType);
                $mainTableAlias = "pe";

                $whereClause = static::getWhereClauseForFilter($filterParameters, $mainTableAlias);
                $orderByClause = static::getOrderByClauseForFilter($sortingParameters, $mainTableAlias);
                $limitClause = static::getLimitClauseForFilter($paginationParameters);

                $queryWithoutLimit = "SELECT pe.id FROM pessoa_equipe pe {$whereClause}";
                $query = "SELECT ee.nome AS empresa_equipe__nome, u.id AS usuario__id, p.id AS pessoa__id, pe.id AS pessoa_equipe__id, pe.corporacao_id_INT AS pessoa_equipe__corporacao_id_INT, pe.cadastro_OFFSEC AS pessoa_equipe__cadastro_OFFSEC, pe.ind_celular_valido_BOOLEAN AS pessoa_equipe__ind_celular_valido_BOOLEAN FROM pessoa_equipe pe LEFT JOIN empresa_equipe ee ON ee.id = pe.empresa_equipe_id_INT LEFT JOIN usuario u ON u.id = pe.usuario_id_INT LEFT JOIN pessoa p ON p.id = pe.pessoa_id_INT {$whereClause} {$orderByClause} {$limitClause}";

                $msg = $this->database->queryMensagem($queryWithoutLimit);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                
                $totalNumberOfRecords = $this->database->rows();
                if($totalNumberOfRecords == 0)
                {
                    return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                }                        
                        
                $msg = $this->database->queryMensagem($query);
                
                //operacao realizaca com sucesso
                if ($msg == null) 
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                    }                        
                
                    $resultSet = $this->database->result;

                    $objReturn = new stdClass();
                    $objReturn->dataSet = static::mapResultSetDataToList($resultSet, $listMappingType, static::$listAliasTypes);

                    if(!Helper::isNullOrEmpty($paginationParameters->recordsPerPage) && !Helper::isNullOrEmpty($paginationParameters->currentPage))
                    {
                        $lastPage = ceil($totalNumberOfRecords / $paginationParameters->recordsPerPage);
                        $objReturn->paginationParameters = new PaginationParams($paginationParameters->recordsPerPage, $paginationParameters->currentPage, $lastPage);
                    }
                    
                    return new Mensagem_generica($objReturn);
                } 
                else
                {
                    return $msg;
                }                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }



public function getId()
            {
                return $this->id;
            }

public function getEmpresaEquipeId()
            {
                return $this->empresaEquipeId;
            }

public function getEmpresa_equipe_id_INT()
                {
                    return $this->empresaEquipeId;
                }

public function getUsuarioId()
            {
                return $this->usuarioId;
            }

public function getUsuario_id_INT()
                {
                    return $this->usuarioId;
                }

public function getPessoaId()
            {
                return $this->pessoaId;
            }

public function getPessoa_id_INT()
                {
                    return $this->pessoaId;
                }

public function getCorporacaoId()
            {
                return $this->corporacaoId;
            }

public function getCorporacao_id_INT()
                {
                    return $this->corporacaoId;
                }



function setId($value)
            {
                $this->id = $value;
            }

function setEmpresaEquipeId($value)
            {
                $this->empresaEquipeId = $value;
            }

public function setEmpresa_equipe_id_INT($value)
                { 
                    $this->empresaEquipeId = $value; 
                }

function setUsuarioId($value)
            {
                $this->usuarioId = $value;
            }

public function setUsuario_id_INT($value)
                { 
                    $this->usuarioId = $value; 
                }

function setPessoaId($value)
            {
                $this->pessoaId = $value;
            }

public function setPessoa_id_INT($value)
                { 
                    $this->pessoaId = $value; 
                }

function setCorporacaoId($value)
            {
                $this->corporacaoId = $value;
            }

public function setCorporacao_id_INT($value)
                { 
                    $this->corporacaoId = $value; 
                }



public function clear()
        {$this->id = null;
$this->empresaEquipeId = null;
if($this->objEmpresa_equipe != null) unset($this->objEmpresa_equipe);
$this->usuarioId = null;
if($this->objUsuario != null) unset($this->objUsuario);
$this->pessoaId = null;
if($this->objPessoa != null) unset($this->objPessoa);
$this->corporacaoId = null;
if($this->objCorporacao != null) unset($this->objCorporacao);
if(is_array($this->arrEstadosCampos)){
                        unset($this->arrEstadosCampos);
                     $this->arrEstadosCampos = array();
                    }
                }

public function formatarParaSQL()
        {
if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->id)){
$this->id = $this->formatarIntegerParaComandoSQL($this->id);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->id);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->empresaEquipeId)){
$this->empresaEquipeId = $this->formatarIntegerParaComandoSQL($this->empresaEquipeId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->empresaEquipeId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->usuarioId)){
$this->usuarioId = $this->formatarIntegerParaComandoSQL($this->usuarioId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->usuarioId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->pessoaId)){
$this->pessoaId = $this->formatarIntegerParaComandoSQL($this->pessoaId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->pessoaId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->corporacaoId)){
$this->corporacaoId = $this->formatarIntegerParaComandoSQL($this->corporacaoId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->corporacaoId);
}

}

public function formatarParaExibicao()
        {}

function select($id, $idCorporacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }   

            $sql = "SELECT id, empresa_equipe_id_INT, usuario_id_INT, pessoa_id_INT, corporacao_id_INT FROM pessoa_equipe WHERE id = $id AND corporacao_id_INT = $idCorporacao";

            $msg = $this->database->queryMensagem($sql);
            $result = $this->database->result;

            if($msg != null && $msg->erro() )
            { 
            		$this->clear();
            		return $msg;
        	} 
        	else if($this->database->rows() == 0)
        	{
                return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nenhum registro foi encontrado."));
            }
        

            $row = $this->database->fetchArray(MYSQLI_NUM);
            
            		$this->id = $row[0];
		$this->empresaEquipeId = $row[1];
		$this->usuarioId = $row[2];
		$this->pessoaId = $row[3];

            
        }

public function delete($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (is_null($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }

            $sql = "DELETE FROM pessoa_equipe WHERE id= $id AND corporacao_id_INT = $idCorporacao";
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                    && $sincronizar
                        && static::idSistemaTabela() != null
                            && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_REMOVE,
                                null,
                                null,
                                $id,
                                $this->database);
            }

            return $msg;
        }

public function insert($sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null, $opcoes = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            
            
            if (is_null($opcoes))
            {
                $opcoes = $this->opcoesDAO;
            }

            if (!is_numeric($this->id))
            {
                $this->id = "null";
                if ($opcoes == null || $opcoes[Database::OPCAO_GERAR_ID] != false) 
                {
                    $this->id = EXTDAO_Sistema_sequencia::gerarId(static::nomeTabela());                    
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }    
                    else if($msg == null)
                    {
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
                else 
                {
                    $this->id = null;
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao));
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }
                    else if($msg == null)
                    {
                        $this->id = $this->database->getLastInsertId();
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
            }
            else 
            {
                $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }                        
                else if($msg == null)
                {
                   $msg = new Mensagem_token();
                   $msg->mValor = $this->id;
                }
            }
            if($msg == null || $msg->ok())
            {                
                if($sincronizar
                    && static::idSistemaTabela() != null
                    && isset($this->id)){
                    
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_INSERT,
                                null,
                                null,
                                $this->id,
                                $this->database); 
                	}
            } 
            else
            {
                $this->id = null;
            }
            
            return $msg;
            
        }

public function getSQLInsert($idCorporacao){ 

return "INSERT INTO pessoa_equipe (id, empresa_equipe_id_INT, usuario_id_INT, pessoa_id_INT, corporacao_id_INT) VALUES ( $this->id ,  $this->empresaEquipeId ,  $this->usuarioId ,  $this->pessoaId ,  $idCorporacao ) "; 

}




        
        public function updateByParameters($id, $objParametros, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao))
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();

            
                
                if (isset($objParametros->empresaEquipeId)) 
                {
                    $arrUpdateFields[] = " empresa_equipe_id_INT = {$objParametros->empresaEquipeId} ";
                }


                
                if (isset($objParametros->usuarioId)) 
                {
                    $arrUpdateFields[] = " usuario_id_INT = {$objParametros->usuarioId} ";
                }


                
                if (isset($objParametros->pessoaId)) 
                {
                    $arrUpdateFields[] = " pessoa_id_INT = {$objParametros->pessoaId} ";
                }



            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE pessoa_equipe SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";

            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null)
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function update($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($this->empresaEquipeId)) 
                {                                      
                    $arrUpdateFields[] = " empresa_equipe_id_INT = {$this->empresaEquipeId} ";
                }


                
                if (isset($this->usuarioId)) 
                {                                      
                    $arrUpdateFields[] = " usuario_id_INT = {$this->usuarioId} ";
                }


                
                if (isset($this->pessoaId)) 
                {                                      
                    $arrUpdateFields[] = " pessoa_id_INT = {$this->pessoaId} ";
                }


            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE pessoa_equipe SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function updateByInstanceUsingParameters($id, $parameters, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($parameters->empresaEquipeId)) 
                {                                      
                    $arrUpdateFields[] = " empresa_equipe_id_INT = {$this->empresaEquipeId} ";
                }
                
                if (isset($parameters->usuarioId)) 
                {                                      
                    $arrUpdateFields[] = " usuario_id_INT = {$this->usuarioId} ";
                }
                
                if (isset($parameters->pessoaId)) 
                {                                      
                    $arrUpdateFields[] = " pessoa_id_INT = {$this->pessoaId} ";
                }
            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE pessoa_equipe SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }

}

?>
