import * as React from 'react';
import * as Moment from 'moment';

import { ContextoMapa } from "lib/contexto";
import { PontoEletronicoServices } from "aplicacao/comum/dataService";

import { 
    PontoEletronico as PontoEletronicoMarker,
    Usuario as UsuarioMarker,
    Empresa as EmpresaMarker,
    BarraInferior
} from 'aplicacao/modulo/mapa/uiComponente';

import {
    PontoEletronico as PontoEletronicoModel,
    WifiNetwork as WifiNetworkModel,
    Usuario as UsuarioModel,
    Empresa as EmpresaModel
} from 'aplicacao/modulo/mapa/model';

import { MapaTemplate } from 'lib/uiTemplate';
import { ElementoVisualizacao } from 'lib/uiComponente';

export class MapaIntegrado extends ContextoMapa
{
    public pontoEletronicoDataService: PontoEletronicoServices;

    public static contextFactory() : void
    {
        $(document).ready(function ()
        {
            let aplicacaoContext = new MapaIntegrado('section#content', '#main-map-container');
            MapaIntegrado.appContext = aplicacaoContext;
        });
    }

    public constructor(domRootSelector: string, mapContainerSelector :string)
    {
        //chama o construtor do pai
        super(domRootSelector, mapContainerSelector);

        //instancia o data service
        this.pontoEletronicoDataService = new PontoEletronicoServices();

        //corrige layout da página para aplicar o mapa
        this.fixPageLayout();

        //adiciona componentes no layout
        this.addComponents();

        //inicializa mapa
        this.initializeMap();

        //carrega dados iniciais
        this.loadInitialDataFromService();
    }

    public fixPageLayout(): void
    {
        //corrige o layout no contexto específico da aplicação
        $('#wrapper').css('min-height', 'auto');
        $('#content > .container').css('padding-left', '0px');
        $('.internal-page-container').css('padding-left', '0px');
        $('.internal-page-container').css('padding-right', '0px');
    }

    public getPathToImageFolder()
    {
        return this.getPathToApplicationRoot() + "adm/imgs/";
    }

    public getDefaultMarkerFolder() :string
    {
        return this.getPathToImageFolder() + "mapa/markers/";
    }

    public addComponents() :void
    {
        var barraInferiorJXS = <BarraInferior 
                                    onTemaChange={this.onTemaChange.bind(this)}    
                                    onElementosVisiveisChange={this.onElementosVisiveisChange.bind(this)}                                   
                                />

        this.renderReactComponent(barraInferiorJXS, '#barra-inferior-container');
    }

    public onTemaChange(template :MapaTemplate) :void
    {
        this.mapObject.setTemplate(template);
    }

    public onElementosVisiveisChange(elementosVisiveis :Array<ElementoVisualizacao.Tipo>) :void
    {
        this.mapObject.setTiposElementosVisiveis(elementosVisiveis);
    }

    public async loadInitialDataFromService()
    {
        try
        {
            var dadosIniciais = await this.pontoEletronicoDataService.getDadosIniciaisDoMapa();
            if(dadosIniciais != null)
            {
                if(dadosIniciais.pontos != null)
                {
                    for(let pontoEletronico of dadosIniciais.pontos)
                    {
                        var pontoMapa = new PontoEletronicoMarker(this, PontoEletronicoModel.factory(pontoEletronico));
                        pontoMapa.render();
                        pontoMapa.addDefaultClickListener();
                    }
                }

                if(dadosIniciais.usuarios != null)
                {
                    for(let usuario of dadosIniciais.usuarios)
                    {
                        var usuarioMapa = new UsuarioMarker(this, UsuarioModel.parseFromRemoteObject(usuario));
                        usuarioMapa.render();
                        usuarioMapa.addDefaultClickListener();
                    }
                }

                let wifiNetworks = new Array<WifiNetworkModel>()
                if(dadosIniciais.wifi != null)
                {
                    for(let wifiNetwork of dadosIniciais.wifi)
                    {
                        wifiNetworks.push(WifiNetworkModel.parseFromRemoteObject(wifiNetwork));
                    }

                }

                if(dadosIniciais.empresas != null)
                {
                    for(let empresa of dadosIniciais.empresas)
                    {
                        var empresaModel = EmpresaModel.parseFromRemoteObject(empresa);
                        empresaModel.setValidWifiNetworks(wifiNetworks);

                        var empresaMapa = new EmpresaMarker(this, empresaModel);
                        empresaMapa.render();
                        empresaMapa.addDefaultClickListener();
                    }
                }

                this.mapObject.fixMainMapSize();
                this.mapObject.fitBounds();

            }
        }
        catch(ex)
        {
            console.log(ex);
        }

    }

    public async getPontosEletronicosFromService()
    {
        var arrPontosEletronicos = await this.pontoEletronicoDataService.getPontosEletronicos();
        for(let pontoEletronico of arrPontosEletronicos)
        {

        }
    }

    public async getUsuariosFromService()
    {
        var arrUsuarios = await this.pontoEletronicoDataService.getUsuarios();
        for(let usuario of arrUsuarios)
        {

        }
    }

    public async getEmpresasFromService()
    {
        var arrEmpresas = await this.pontoEletronicoDataService.getEmpresas();
        for(let empresa of arrEmpresas)
        {

        }
    }

    public async getWifiNetworksFromService()
    {
        var arrEmpresas = await this.pontoEletronicoDataService.getEmpresas();
        for(let empresa of arrEmpresas)
        {

        }
    }

}

export namespace MapaIntegrado
{
    //exports
    export var appContext: MapaIntegrado;  
}