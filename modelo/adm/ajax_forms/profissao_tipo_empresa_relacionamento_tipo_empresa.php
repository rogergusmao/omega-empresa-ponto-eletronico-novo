<?php

/*
*
* -------------------------------------------------------
* NOME DO FORMUL�RIO: profissao_tipo_empresa
* DATA DE GERA��O:    07.02.2012
* ARQUIVO:            profissao_tipo_empresa.php
* TABELA MYSQL:       profissao_tipo_empresa
* BANCO DE DADOS:     ponto_eletronico
* -------------------------------------------------------
*
*/

if (isset($_GET["contador"]))
{

    $numeroRegistroInterno = Helper::GET("contador");
}

if (isset($identificadorRelacionamento) && is_numeric($identificadorRelacionamento))
{

    $objProfissao_tipo_empresa = new EXTDAO_Profissao_tipo_empresa();
    $objProfissao_tipo_empresa->select($identificadorRelacionamento);
}
else
{

    $objProfissao_tipo_empresa = new EXTDAO_Profissao_tipo_empresa();
}

$objArgProfissao_tipo_empresa = new Generic_Argument();
$objProfissao_tipo_empresa->formatarParaExibicao();

?>

<input type="hidden" name="profissao_tipo_empresa_id_<?= $numeroRegistroInterno ?>"
       id="profissao_tipo_empresa_id_<?= $numeroRegistroInterno ?>" value="<?= $objProfissao_tipo_empresa->getId(); ?>">

<table class="tabela_form">

    <tr class="tr_form">


        <?

        $objArgProfissao_tipo_empresa = new Generic_Argument();

        $objArgProfissao_tipo_empresa->numeroDoRegistro = "";
        $objArgProfissao_tipo_empresa->label = $objProfissao_tipo_empresa->label_profissao_id_INT;
        $objArgProfissao_tipo_empresa->valor = $objProfissao_tipo_empresa->getProfissao_id_INT();
        $objArgProfissao_tipo_empresa->classeCss = "input_text";
        $objArgProfissao_tipo_empresa->classeCssFocus = "focus_text";
        $objArgProfissao_tipo_empresa->obrigatorio = false;
        $objArgProfissao_tipo_empresa->largura = 200;
        $objArgProfissao_tipo_empresa->nome = "profissao_tipo_empresa_profissao_id_INT_{$numeroRegistroInterno}";
        $objArgProfissao_tipo_empresa->id = "profissao_tipo_empresa_profissao_id_INT_{$numeroRegistroInterno}";

        $objProfissao_tipo_empresa->addInfoCampos("profissao_id_INT", $objArgProfissao_tipo_empresa->label, "TEXTO", $objArgProfissao_tipo_empresa->obrigatorio);

        ?>

        <td class="td_form_label"><?= $objArgProfissao_tipo_empresa->getLabel() ?></td>
        <td class="td_form_campo">
            <?= $objProfissao_tipo_empresa->objProfissao->getComboBox($objArgProfissao_tipo_empresa); ?>
        </td>


        <td class="td_form_label"></td>
        <td class="td_form_campo"></td>
    </tr>
    <tr>
        <td colspan="4" class="td_botao_remover_da_lista"><input class="botoes_form" type="button"
                                                                 value="Remover Profiss�o Do Ramo Da Empresa"
                                                                 onclick="javascript:removerDivAjaxEmLista(this);"></td>
    </tr>
</table><br/>

