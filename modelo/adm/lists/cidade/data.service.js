/*

Arquivo gerado atrav�s de gerador de c�digo em 12/01/2018 as 20:24:12.
Para que o arquivo n�o seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

Tabela correspondente: cidade
Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

*/

returnObject.getAllComboBoxesDataForCidadeFilter = function ()
{
    return $http({
        url: baseURLAdm + "/webservice.php?class=EXTDAO_Cidade&jsonOutput=true&action=__getFilterComboBoxesData",
        method: "POST",
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    });

};

returnObject.getCidadeList = function (listParams)
{
    listParams.listMappingType = Constants.ListMappingType.GRID;
    return $http.post(baseURLAdm + "/webservice.php?class=EXTDAO_Cidade&action=__getList", listParams);
};

returnObject.removeRecords = function (recordsPKs)
{
    return $http.post(baseURLAdm + "/webservice.php?class=EXTDAO_Cidade&action=remove", recordsPKs);
};

returnObject.getListOfUfIdForCidadeList = function ()
{
    var params = {filterData: null, listMappingType: Constants.ListMappingType.COMBOBOX};
    return $http.post(baseURLAdm + "/webservice.php?class=EXTDAO_Uf&action=__getList", params);
};
                

