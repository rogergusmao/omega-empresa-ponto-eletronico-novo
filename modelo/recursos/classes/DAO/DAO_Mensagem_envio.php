<?php


        /*

        Arquivo gerado através de gerador de código em 12/05/2018 as 14:47:14.
        Para que o arquivo não seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

        Tabela correspondente: mensagem_envio
        Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

        */
        


class DAO_Mensagem_envio extends Generic_DAO
                        {
                            const NOME_ENTIDADE = "mensagem_envio";

                            public static $nomeTabela = null;
                            public static $idSistemaTabela = false;
                            public static $databaseFieldsRelatedAttributes = null;
	                        public static $databaseFieldTypes = null;
	                        public static $databaseFieldNames = null;
	                        public static $listAliasTypes = null;
	                        public static $listAliasRelatedAttributes = null;
	                        
	                        public $id;
public $tipoCanalEnvioId;
public $mensagemId;
public $cadastroSec;
public $cadastroOffsec;
public $obs;
public $identificador;
public $tentativaInt;
public $registroEstadoId;
public $corporacaoId;

public $labelId;
public $labelTipoCanalEnvioId;
public $labelMensagemId;
public $labelCadastroSec;
public $labelCadastroOffsec;
public $labelObs;
public $labelIdentificador;
public $labelTentativaInt;
public $labelRegistroEstadoId;
public $labelCorporacaoId;

public static $arrPK = array("id");

public function __construct($configDAO = null)
        {
            parent::__construct($configDAO);
            static::__setStaticValues($this->database);           
        }

public static function idSistemaTabela($databaseObject = null){
            if(!(static::$idSistemaTabela))
            {
                static::$idSistemaTabela = EXTDAO_Sistema_tabela::getIdDaSistemaTabela(static::nomeTabela(), $databaseObject);
            }
            return static::$idSistemaTabela;
        }

public function getIdSistemaTabela(){
            return static::idSistemaTabela();
        }

public static function nomeTabela(){
            if(is_null(static::$nomeTabela))
            {
                static::$nomeTabela = "mensagem_envio";
            }
            return static::$nomeTabela;
        }

public function getNomeTabela(){
            return static::nomeTabela();
        }

public static function __setStaticValues($databaseObject = null)
                   {    
                        static::nomeTabela();
                        
                        static::idSistemaTabela($databaseObject);
                   
                        static::setDatabaseFieldNames();
                        static::setDatabaseFieldTypes();
                        static::setDatabaseFieldsRelatedAttributeNames();
                        
                        static::setListAliasTypes();
                        static::setListAliasRelatedAttributes();
                   }

public function getExpressaoLabel()
                   {
                        return "{id}";
                   }

public static function setDatabaseFieldNames()
        {
            if(is_null(static::$databaseFieldNames))
            {
                static::$databaseFieldNames = new stdClass();
                static::$databaseFieldNames->id = "id";
static::$databaseFieldNames->tipoCanalEnvioId = "tipo_canal_envio_id_INT";
static::$databaseFieldNames->mensagemId = "mensagem_id_INT";
static::$databaseFieldNames->cadastroSec = "cadastro_SEC";
static::$databaseFieldNames->cadastroOffsec = "cadastro_OFFSEC";
static::$databaseFieldNames->obs = "obs";
static::$databaseFieldNames->identificador = "identificador";
static::$databaseFieldNames->tentativaInt = "tentativa_INT";
static::$databaseFieldNames->registroEstadoId = "registro_estado_id_INT";
static::$databaseFieldNames->corporacaoId = "corporacao_id_INT";

            }
                      
        }

public static function setDatabaseFieldTypes()
        {
            if(is_null(static::$databaseFieldTypes))
            {
                static::$databaseFieldTypes = new stdClass();
                static::$databaseFieldTypes->id = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->tipoCanalEnvioId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->mensagemId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->cadastroSec = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$databaseFieldTypes->cadastroOffsec = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$databaseFieldTypes->obs = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->identificador = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->tentativaInt = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->registroEstadoId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->corporacaoId = static::TIPO_VARIAVEL_INTEGER;

            }
            
        }

public static function setDatabaseFieldsRelatedAttributeNames()
        {
            if(is_null(static::$databaseFieldsRelatedAttributes))
            {
                static::$databaseFieldsRelatedAttributes = new stdClass();
                static::$databaseFieldsRelatedAttributes->id = "id";
static::$databaseFieldsRelatedAttributes->tipo_canal_envio_id_INT = "tipoCanalEnvioId";
static::$databaseFieldsRelatedAttributes->mensagem_id_INT = "mensagemId";
static::$databaseFieldsRelatedAttributes->cadastro_SEC = "cadastroSec";
static::$databaseFieldsRelatedAttributes->cadastro_OFFSEC = "cadastroOffsec";
static::$databaseFieldsRelatedAttributes->obs = "obs";
static::$databaseFieldsRelatedAttributes->identificador = "identificador";
static::$databaseFieldsRelatedAttributes->tentativa_INT = "tentativaInt";
static::$databaseFieldsRelatedAttributes->registro_estado_id_INT = "registroEstadoId";
static::$databaseFieldsRelatedAttributes->corporacao_id_INT = "corporacaoId";

            }           
        }

public function setAllFieldsToHumanFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["tipo_canal_envio_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["mensagem_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["cadastro_SEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["cadastro_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["obs"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["identificador"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["tentativa_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["registro_estado_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
}

public function setAllFieldsToDatabaseFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["tipo_canal_envio_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["mensagem_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["cadastro_SEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["cadastro_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["obs"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["identificador"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["tentativa_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["registro_estado_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
}

  public function getFkObjTipo_canal_envio() 
                {
                    if($this->objTipo_canal_envio == null)
                    {                        
                        $this->objTipo_canal_envio = new EXTDAO_Tipo_canal_envio_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getTipo_canal_envio_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objTipo_canal_envio->clear();
                    }
                    elseif($this->objTipo_canal_envio->getId() != $idFK)
                    {
                        $this->objTipo_canal_envio->select($idFK);
                    }
                    return $this->objTipo_canal_envio;
                }
  public function getFkObjMensagem() 
                {
                    if($this->objMensagem == null)
                    {                        
                        $this->objMensagem = new EXTDAO_Mensagem_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getMensagem_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objMensagem->clear();
                    }
                    elseif($this->objMensagem->getId() != $idFK)
                    {
                        $this->objMensagem->select($idFK);
                    }
                    return $this->objMensagem;
                }
  public function getFkObjRegistro_estado() 
                {
                    if($this->objRegistro_estado == null)
                    {                        
                        $this->objRegistro_estado = new EXTDAO_Registro_estado_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getRegistro_estado_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objRegistro_estado->clear();
                    }
                    elseif($this->objRegistro_estado->getId() != $idFK)
                    {
                        $this->objRegistro_estado->select($idFK);
                    }
                    return $this->objRegistro_estado;
                }
  public function getFkObjCorporacao() 
                {
                    if($this->objCorporacao == null)
                    {                        
                        $this->objCorporacao = new EXTDAO_Corporacao_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getCorporacao_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objCorporacao->clear();
                    }
                    elseif($this->objCorporacao->getId() != $idFK)
                    {
                        $this->objCorporacao->select($idFK);
                    }
                    return $this->objCorporacao;
                }


public function setLabels()
        {$this->labelId = "";
$this->labelTipoCanalEnvioId = "";
$this->labelMensagemId = "";
$this->labelCadastroSec = "";
$this->labelCadastroOffsec = "";
$this->labelObs = "";
$this->labelIdentificador = "";
$this->labelTentativaInt = "";
$this->labelRegistroEstadoId = "";
$this->labelCorporacaoId = "";
}

public function __actionAdd($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                $this->setByObject($parameters);
                $this->setDataCadastroAndOffsetInSecondsFromUser($parameters);
                
                $this->formatarParaSQL();
                $msg = $this->insert(true);
                
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                return new Mensagem(null, I18N::getExpression("Mensagem envio adicionado com sucesso."));
                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionEdit($parameters = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try {

                $this->setByObject($parameters);
                $this->setAllFieldsToHumanFormat();
                
                $this->formatarParaSQL();
                $msg = $this->updateByInstanceUsingParameters($this->id, $parameters, true);
                if($msg != null && $msg->erro())
                {
                     return $msg;
                }
        
                return new Mensagem(null, I18N::getExpression("Mensagem envio editado com sucesso."));
            }
            catch(Exception $ex)
            {                
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionRemove($parameters = null)
        {
            $successulRemovalCount = 0;
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                foreach($parameters as $record)
                {
                    if(is_numeric($record->id))
                    {
                        $msg = $this->delete($record->id, true);
                        if($msg == null || $msg->ok())
                        {
                            $successulRemovalCount++;
                        }
                    }
                }
                
                if($successulRemovalCount > 0)
                {
                    $successMessage = count($successulRemovalCount) > 1 ? I18N::getExpression("{0} Mensagem envio foram removidos com sucesso.", $successulRemovalCount) : I18N::getExpression("Mensagem envio removido com sucesso.");
                    return new Mensagem(null, $successMessage);
                }
                else
                {
                    $errorMessage = count($parameters) > 1 ? I18N::getExpression("Falha ao remover Mensagem envio.") : I18N::getExpression("Falha ao remover Mensagem envio.");
                    return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, $errorMessage);                    
                }
            }
            catch (Exception $ex) 
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }

public function __getRecord($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }
            
            try
            {            
                $returnObject = new stdClass();            
                $whereClause = static::getWhereClauseForFilter($parameters->filterParameters);
                $query = "SELECT  id, tipo_canal_envio_id_INT, mensagem_id_INT, cadastro_SEC, cadastro_OFFSEC, obs, identificador, tentativa_INT, registro_estado_id_INT, corporacao_id_INT FROM mensagem_envio {$whereClause}";
    
                $msg = $this->database->queryMensagem($query);                
                if ($msg == null)
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Não foi encontrado nenhum registro"));
                    }                    
                    $resultSet = $this->database->result;
                    $resultObject = static::mapResultSetDataToRecord($resultSet);
                    
                    $returnObject->formData = $resultObject;
                    $returnObject->comboBoxesData = null;
                    if($parameters->loadComboBoxesData)
                    {
                        $this->setByObject($resultObject);
                        $returnObject->comboBoxesData =  $this->getFormComboBoxesData();
                    }                    
    
                    
    
                    return new Mensagem_generica($returnObject);
                } 
                else 
                {
                    return $msg;
                }
                
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }


        
        public function __getFilterComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
                
        public function __getFormComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
        
        protected function getFormComboBoxesData()
        {
            $comboBoxesData = new stdClass();
            $listParameters = new stdClass();
            $listParameters->listMappingType = static::LIST_MAPPING_TYPE_COMBOBOX;
            
            
                    
                    $objTipoCanalEnvio = new EXTDAO_Tipo_canal_envio();
                    $comboBoxesData->fieldTipoCanalEnvioId = $objTipoCanalEnvio->__getList($listParameters);
                    
                    $objMensagem = new EXTDAO_Mensagem();
                    $comboBoxesData->fieldMensagemId = $objMensagem->__getList($listParameters);
                    
                    $objRegistroEstado = new EXTDAO_Registro_estado();
                    $comboBoxesData->fieldRegistroEstadoId = $objRegistroEstado->__getList($listParameters);

            return $comboBoxesData;
        }
        
        


        
        public static function setListAliasTypes()
        {
            if(is_null(static::$listAliasTypes))
            {
                static::$listAliasTypes = new stdClass();
                static::$listAliasTypes->tipo_canal_envio__nome = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->mensagem__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->registro_estado__nome = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->mensagem_envio__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->mensagem_envio__nome = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->mensagem_envio__corporacao_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->mensagem_envio__cadastro_SEC = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$listAliasTypes->mensagem_envio__obs = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->mensagem_envio__identificador = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->mensagem_envio__tentativa_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->mensagem_envio__tipo_registro_id_INT = static::TIPO_VARIAVEL_INTEGER;
            }        
        }
        
        public static function setListAliasRelatedAttributes()
        {
            if(is_null(static::$listAliasRelatedAttributes))
            {
                static::$listAliasRelatedAttributes = new stdClass();
                static::$listAliasRelatedAttributes->tipo_canal_envio__nome = "tipoCanalEnvioNome";
static::$listAliasRelatedAttributes->mensagem__id = "mensagemId";
static::$listAliasRelatedAttributes->registro_estado__nome = "registroEstadoNome";
static::$listAliasRelatedAttributes->mensagem_envio__id = "id";
static::$listAliasRelatedAttributes->mensagem_envio__nome = "nome";
static::$listAliasRelatedAttributes->mensagem_envio__corporacao_id_INT = "corporacaoId";
static::$listAliasRelatedAttributes->mensagem_envio__cadastro_SEC = "cadastroSec";
static::$listAliasRelatedAttributes->mensagem_envio__obs = "obs";
static::$listAliasRelatedAttributes->mensagem_envio__identificador = "identificador";
static::$listAliasRelatedAttributes->mensagem_envio__tentativa_INT = "tentativaInt";
static::$listAliasRelatedAttributes->mensagem_envio__tipo_registro_id_INT = "tipoRegistroId";
            }         
        }

        public function __getList($parameters = null, $idCorporacao = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try
            {
                $filterParameters = new stdClass();
                if(isset($parameters->filterParameters))
                {
                    $filterParameters = $parameters->filterParameters;
                }
                                    
                            
            if(is_null($idCorporacao))
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            ListParameters::addFilterParameterToObject($filterParameters, "corporacaoId", $idCorporacao);           
                
                
                $sortingParameters = null;
                if(isset($parameters->sortingParameters))
                {
                    $sortingParameters = $parameters->sortingParameters;
                }
                
                $paginationParameters = null;
                if(isset($parameters->paginationParameters))
                {
                    $paginationParameters = $parameters->paginationParameters;
                }
                
                $listMappingType = static::getListMappingType($parameters->listMappingType);
                $mainTableAlias = "me";

                $whereClause = static::getWhereClauseForFilter($filterParameters, $mainTableAlias);
                $orderByClause = static::getOrderByClauseForFilter($sortingParameters, $mainTableAlias);
                $limitClause = static::getLimitClauseForFilter($paginationParameters);

                $queryWithoutLimit = "SELECT me.id FROM mensagem_envio me {$whereClause}";
                $query = "SELECT tce.nome AS tipo_canal_envio__nome, m.id AS mensagem__id, re.nome AS registro_estado__nome, me.id AS mensagem_envio__id, me.nome AS mensagem_envio__nome, me.corporacao_id_INT AS mensagem_envio__corporacao_id_INT, me.cadastro_SEC AS mensagem_envio__cadastro_SEC, me.obs AS mensagem_envio__obs, me.identificador AS mensagem_envio__identificador, me.tentativa_INT AS mensagem_envio__tentativa_INT, me.tipo_registro_id_INT AS mensagem_envio__tipo_registro_id_INT FROM mensagem_envio me LEFT JOIN tipo_canal_envio tce ON tce.id = me.tipo_canal_envio_id_INT LEFT JOIN mensagem m ON m.id = me.mensagem_id_INT LEFT JOIN registro_estado re ON re.id = me.registro_estado_id_INT {$whereClause} {$orderByClause} {$limitClause}";

                $msg = $this->database->queryMensagem($queryWithoutLimit);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                
                $totalNumberOfRecords = $this->database->rows();
                if($totalNumberOfRecords == 0)
                {
                    return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                }                        
                        
                $msg = $this->database->queryMensagem($query);
                
                //operacao realizaca com sucesso
                if ($msg == null) 
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                    }                        
                
                    $resultSet = $this->database->result;

                    $objReturn = new stdClass();
                    $objReturn->dataSet = static::mapResultSetDataToList($resultSet, $listMappingType, static::$listAliasTypes);

                    if(!Helper::isNullOrEmpty($paginationParameters->recordsPerPage) && !Helper::isNullOrEmpty($paginationParameters->currentPage))
                    {
                        $lastPage = ceil($totalNumberOfRecords / $paginationParameters->recordsPerPage);
                        $objReturn->paginationParameters = new PaginationParams($paginationParameters->recordsPerPage, $paginationParameters->currentPage, $lastPage);
                    }
                    
                    return new Mensagem_generica($objReturn);
                } 
                else
                {
                    return $msg;
                }                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }



public function getId()
            {
                return $this->id;
            }

public function getTipoCanalEnvioId()
            {
                return $this->tipoCanalEnvioId;
            }

public function getTipo_canal_envio_id_INT()
                {
                    return $this->tipoCanalEnvioId;
                }

public function getMensagemId()
            {
                return $this->mensagemId;
            }

public function getMensagem_id_INT()
                {
                    return $this->mensagemId;
                }

public function getCadastroSec()
            {
                return $this->cadastroSec;
            }

public function getCadastro_SEC()
                {
                    return $this->cadastroSec;
                }

public function getCadastroOffsec()
            {
                return $this->cadastroOffsec;
            }

public function getCadastro_OFFSEC()
                {
                    return $this->cadastroOffsec;
                }

public function getObs()
            {
                return $this->obs;
            }

public function getIdentificador()
            {
                return $this->identificador;
            }

public function getTentativaInt()
            {
                return $this->tentativaInt;
            }

public function getTentativa_INT()
                {
                    return $this->tentativaInt;
                }

public function getRegistroEstadoId()
            {
                return $this->registroEstadoId;
            }

public function getRegistro_estado_id_INT()
                {
                    return $this->registroEstadoId;
                }

public function getCorporacaoId()
            {
                return $this->corporacaoId;
            }

public function getCorporacao_id_INT()
                {
                    return $this->corporacaoId;
                }



function setId($value)
            {
                $this->id = $value;
            }

function setTipoCanalEnvioId($value)
            {
                $this->tipoCanalEnvioId = $value;
            }

public function setTipo_canal_envio_id_INT($value)
                { 
                    $this->tipoCanalEnvioId = $value; 
                }

function setMensagemId($value)
            {
                $this->mensagemId = $value;
            }

public function setMensagem_id_INT($value)
                { 
                    $this->mensagemId = $value; 
                }

function setCadastroSec($value)
            {
                $this->cadastroSec = $value;
            }

public function setCadastro_SEC($value)
                { 
                    $this->cadastroSec = $value; 
                }

function setCadastroOffsec($value)
            {
                $this->cadastroOffsec = $value;
            }

public function setCadastro_OFFSEC($value)
                { 
                    $this->cadastroOffsec = $value; 
                }

function setObs($value)
            {
                $this->obs = $value;
            }

function setIdentificador($value)
            {
                $this->identificador = $value;
            }

function setTentativaInt($value)
            {
                $this->tentativaInt = $value;
            }

public function setTentativa_INT($value)
                { 
                    $this->tentativaInt = $value; 
                }

function setRegistroEstadoId($value)
            {
                $this->registroEstadoId = $value;
            }

public function setRegistro_estado_id_INT($value)
                { 
                    $this->registroEstadoId = $value; 
                }

function setCorporacaoId($value)
            {
                $this->corporacaoId = $value;
            }

public function setCorporacao_id_INT($value)
                { 
                    $this->corporacaoId = $value; 
                }



public function clear()
        {$this->id = null;
$this->tipoCanalEnvioId = null;
if($this->objTipo_canal_envio != null) unset($this->objTipo_canal_envio);
$this->mensagemId = null;
if($this->objMensagem != null) unset($this->objMensagem);
$this->cadastroSec = null;
$this->cadastroOffsec = null;
$this->obs = null;
$this->identificador = null;
$this->tentativaInt = null;
$this->registroEstadoId = null;
if($this->objRegistro_estado != null) unset($this->objRegistro_estado);
$this->corporacaoId = null;
if($this->objCorporacao != null) unset($this->objCorporacao);
if(is_array($this->arrEstadosCampos)){
                        unset($this->arrEstadosCampos);
                     $this->arrEstadosCampos = array();
                    }
                }

public function formatarParaSQL()
        {
if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->id)){
$this->id = $this->formatarIntegerParaComandoSQL($this->id);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->id);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->tipoCanalEnvioId)){
$this->tipoCanalEnvioId = $this->formatarIntegerParaComandoSQL($this->tipoCanalEnvioId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->tipoCanalEnvioId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->mensagemId)){
$this->mensagemId = $this->formatarIntegerParaComandoSQL($this->mensagemId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->mensagemId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->cadastroSec)){
$this->cadastroSec = $this->formatarIntegerParaComandoSQL($this->cadastroSec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->cadastroSec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->cadastroOffsec)){
$this->cadastroOffsec = $this->formatarIntegerParaComandoSQL($this->cadastroOffsec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->cadastroOffsec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->obs)){
$this->obs = $this->formatarStringParaComandoSQL($this->obs);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->obs);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->identificador)){
$this->identificador = $this->formatarStringParaComandoSQL($this->identificador);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->identificador);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->tentativaInt)){
$this->tentativaInt = $this->formatarIntegerParaComandoSQL($this->tentativaInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->tentativaInt);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->registroEstadoId)){
$this->registroEstadoId = $this->formatarIntegerParaComandoSQL($this->registroEstadoId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->registroEstadoId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->corporacaoId)){
$this->corporacaoId = $this->formatarIntegerParaComandoSQL($this->corporacaoId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->corporacaoId);
}

}

public function formatarParaExibicao()
        {if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->obs)){
$this->obs = $this->formatarStringParaExibicao($this->obs);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->obs);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->identificador)){
$this->identificador = $this->formatarStringParaExibicao($this->identificador);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->identificador);
}

}

function select($id, $idCorporacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }   

            $sql = "SELECT id, tipo_canal_envio_id_INT, mensagem_id_INT, cadastro_SEC, cadastro_OFFSEC, obs, identificador, tentativa_INT, registro_estado_id_INT, corporacao_id_INT FROM mensagem_envio WHERE id = $id AND corporacao_id_INT = $idCorporacao";

            $msg = $this->database->queryMensagem($sql);
            $result = $this->database->result;

            if($msg != null && $msg->erro() )
            { 
            		$this->clear();
            		return $msg;
        	} 
        	else if($this->database->rows() == 0)
        	{
                return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nenhum registro foi encontrado."));
            }
        

            $row = $this->database->fetchArray(MYSQLI_NUM);
            
            		$this->id = $row[0];
		$this->tipoCanalEnvioId = $row[1];
		$this->mensagemId = $row[2];
		$this->cadastroSec = $row[3];
		$this->cadastroOffsec = $row[4];
		$this->obs = $row[5];
		$this->identificador = $row[6];
		$this->tentativaInt = $row[7];
		$this->registroEstadoId = $row[8];

            
        }

public function delete($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (is_null($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }

            $sql = "DELETE FROM mensagem_envio WHERE id= $id AND corporacao_id_INT = $idCorporacao";
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                    && $sincronizar
                        && static::idSistemaTabela() != null
                            && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_REMOVE,
                                null,
                                null,
                                $id,
                                $this->database);
            }

            return $msg;
        }

public function insert($sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null, $opcoes = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $this->defineDataCadastroInSecondsIfNotDefined();
$this->defineDataCadastroOffsetInSecondsIfNotDefined();

            
            if (is_null($opcoes))
            {
                $opcoes = $this->opcoesDAO;
            }

            if (!is_numeric($this->id))
            {
                $this->id = "null";
                if ($opcoes == null || $opcoes[Database::OPCAO_GERAR_ID] != false) 
                {
                    $this->id = EXTDAO_Sistema_sequencia::gerarId(static::nomeTabela());                    
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }    
                    else if($msg == null)
                    {
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
                else 
                {
                    $this->id = null;
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao));
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }
                    else if($msg == null)
                    {
                        $this->id = $this->database->getLastInsertId();
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
            }
            else 
            {
                $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }                        
                else if($msg == null)
                {
                   $msg = new Mensagem_token();
                   $msg->mValor = $this->id;
                }
            }
            if($msg == null || $msg->ok())
            {                
                if($sincronizar
                    && static::idSistemaTabela() != null
                    && isset($this->id)){
                    
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_INSERT,
                                null,
                                null,
                                $this->id,
                                $this->database); 
                	}
            } 
            else
            {
                $this->id = null;
            }
            
            return $msg;
            
        }

public function getSQLInsert($idCorporacao){ 

return "INSERT INTO mensagem_envio (id, tipo_canal_envio_id_INT, mensagem_id_INT, cadastro_SEC, cadastro_OFFSEC, obs, identificador, tentativa_INT, registro_estado_id_INT, corporacao_id_INT) VALUES ( $this->id ,  $this->tipoCanalEnvioId ,  $this->mensagemId ,  $this->cadastroSec ,  $this->cadastroOffsec ,  $this->obs ,  $this->identificador ,  $this->tentativaInt ,  $this->registroEstadoId ,  $idCorporacao ) "; 

}




        
        public function updateByParameters($id, $objParametros, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao))
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();

            
                
                if (isset($objParametros->tipoCanalEnvioId)) 
                {
                    $arrUpdateFields[] = " tipo_canal_envio_id_INT = {$objParametros->tipoCanalEnvioId} ";
                }


                
                if (isset($objParametros->mensagemId)) 
                {
                    $arrUpdateFields[] = " mensagem_id_INT = {$objParametros->mensagemId} ";
                }


                
                if (isset($objParametros->cadastroSec)) 
                {
                    $arrUpdateFields[] = " cadastro_SEC = {$objParametros->cadastroSec} ";
                }


                
                if (isset($objParametros->cadastroOffsec)) 
                {
                    $arrUpdateFields[] = " cadastro_OFFSEC = {$objParametros->cadastroOffsec} ";
                }


                
                if (isset($objParametros->obs)) 
                {
                    $arrUpdateFields[] = " obs = {$objParametros->obs} ";
                }


                
                if (isset($objParametros->identificador)) 
                {
                    $arrUpdateFields[] = " identificador = {$objParametros->identificador} ";
                }


                
                if (isset($objParametros->tentativaInt)) 
                {
                    $arrUpdateFields[] = " tentativa_INT = {$objParametros->tentativaInt} ";
                }


                
                if (isset($objParametros->registroEstadoId)) 
                {
                    $arrUpdateFields[] = " registro_estado_id_INT = {$objParametros->registroEstadoId} ";
                }



            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE mensagem_envio SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";

            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null)
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function update($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($this->tipoCanalEnvioId)) 
                {                                      
                    $arrUpdateFields[] = " tipo_canal_envio_id_INT = {$this->tipoCanalEnvioId} ";
                }


                
                if (isset($this->mensagemId)) 
                {                                      
                    $arrUpdateFields[] = " mensagem_id_INT = {$this->mensagemId} ";
                }


                
                if (isset($this->cadastroSec)) 
                {                                      
                    $arrUpdateFields[] = " cadastro_SEC = {$this->cadastroSec} ";
                }


                
                if (isset($this->cadastroOffsec)) 
                {                                      
                    $arrUpdateFields[] = " cadastro_OFFSEC = {$this->cadastroOffsec} ";
                }


                
                if (isset($this->obs)) 
                {                                      
                    $arrUpdateFields[] = " obs = {$this->obs} ";
                }


                
                if (isset($this->identificador)) 
                {                                      
                    $arrUpdateFields[] = " identificador = {$this->identificador} ";
                }


                
                if (isset($this->tentativaInt)) 
                {                                      
                    $arrUpdateFields[] = " tentativa_INT = {$this->tentativaInt} ";
                }


                
                if (isset($this->registroEstadoId)) 
                {                                      
                    $arrUpdateFields[] = " registro_estado_id_INT = {$this->registroEstadoId} ";
                }


            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE mensagem_envio SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function updateByInstanceUsingParameters($id, $parameters, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($parameters->tipoCanalEnvioId)) 
                {                                      
                    $arrUpdateFields[] = " tipo_canal_envio_id_INT = {$this->tipoCanalEnvioId} ";
                }
                
                if (isset($parameters->mensagemId)) 
                {                                      
                    $arrUpdateFields[] = " mensagem_id_INT = {$this->mensagemId} ";
                }
                
                if (isset($parameters->cadastroSec)) 
                {                                      
                    $arrUpdateFields[] = " cadastro_SEC = {$this->cadastroSec} ";
                }
                
                if (isset($parameters->cadastroOffsec)) 
                {                                      
                    $arrUpdateFields[] = " cadastro_OFFSEC = {$this->cadastroOffsec} ";
                }
                
                if (isset($parameters->obs)) 
                {                                      
                    $arrUpdateFields[] = " obs = {$this->obs} ";
                }
                
                if (isset($parameters->identificador)) 
                {                                      
                    $arrUpdateFields[] = " identificador = {$this->identificador} ";
                }
                
                if (isset($parameters->tentativaInt)) 
                {                                      
                    $arrUpdateFields[] = " tentativa_INT = {$this->tentativaInt} ";
                }
                
                if (isset($parameters->registroEstadoId)) 
                {                                      
                    $arrUpdateFields[] = " registro_estado_id_INT = {$this->registroEstadoId} ";
                }
            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE mensagem_envio SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }

}

?>
