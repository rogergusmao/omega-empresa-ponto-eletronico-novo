<?php


        /*

        Arquivo gerado através de gerador de código em 12/05/2018 as 14:51:17.
        Para que o arquivo não seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

        Tabela correspondente: tarefa_cotidiano_item
        Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

        */
        


class DAO_Tarefa_cotidiano_item extends Generic_DAO
                        {
                            const NOME_ENTIDADE = "tarefa_cotidiano_item";

                            public static $nomeTabela = null;
                            public static $idSistemaTabela = false;
                            public static $databaseFieldsRelatedAttributes = null;
	                        public static $databaseFieldTypes = null;
	                        public static $databaseFieldNames = null;
	                        public static $listAliasTypes = null;
	                        public static $listAliasRelatedAttributes = null;
	                        
	                        public $id;
public $descricao;
public $corporacaoId;
public $seqInt;
public $latitudeInt;
public $longitudeInt;
public $tarefaCotidianoId;

public $labelId;
public $labelDescricao;
public $labelCorporacaoId;
public $labelSeqInt;
public $labelLatitudeInt;
public $labelLongitudeInt;
public $labelTarefaCotidianoId;

public static $arrPK = array("id");

public function __construct($configDAO = null)
        {
            parent::__construct($configDAO);
            static::__setStaticValues($this->database);           
        }

public static function idSistemaTabela($databaseObject = null){
            if(!(static::$idSistemaTabela))
            {
                static::$idSistemaTabela = EXTDAO_Sistema_tabela::getIdDaSistemaTabela(static::nomeTabela(), $databaseObject);
            }
            return static::$idSistemaTabela;
        }

public function getIdSistemaTabela(){
            return static::idSistemaTabela();
        }

public static function nomeTabela(){
            if(is_null(static::$nomeTabela))
            {
                static::$nomeTabela = "tarefa_cotidiano_item";
            }
            return static::$nomeTabela;
        }

public function getNomeTabela(){
            return static::nomeTabela();
        }

public static function __setStaticValues($databaseObject = null)
                   {    
                        static::nomeTabela();
                        
                        static::idSistemaTabela($databaseObject);
                   
                        static::setDatabaseFieldNames();
                        static::setDatabaseFieldTypes();
                        static::setDatabaseFieldsRelatedAttributeNames();
                        
                        static::setListAliasTypes();
                        static::setListAliasRelatedAttributes();
                   }

public function getExpressaoLabel()
                   {
                        return "{id}";
                   }

public static function setDatabaseFieldNames()
        {
            if(is_null(static::$databaseFieldNames))
            {
                static::$databaseFieldNames = new stdClass();
                static::$databaseFieldNames->id = "id";
static::$databaseFieldNames->descricao = "descricao";
static::$databaseFieldNames->corporacaoId = "corporacao_id_INT";
static::$databaseFieldNames->seqInt = "seq_INT";
static::$databaseFieldNames->latitudeInt = "latitude_INT";
static::$databaseFieldNames->longitudeInt = "longitude_INT";
static::$databaseFieldNames->tarefaCotidianoId = "tarefa_cotidiano_id_INT";

            }
                      
        }

public static function setDatabaseFieldTypes()
        {
            if(is_null(static::$databaseFieldTypes))
            {
                static::$databaseFieldTypes = new stdClass();
                static::$databaseFieldTypes->id = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->descricao = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->corporacaoId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->seqInt = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->latitudeInt = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->longitudeInt = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->tarefaCotidianoId = static::TIPO_VARIAVEL_INTEGER;

            }
            
        }

public static function setDatabaseFieldsRelatedAttributeNames()
        {
            if(is_null(static::$databaseFieldsRelatedAttributes))
            {
                static::$databaseFieldsRelatedAttributes = new stdClass();
                static::$databaseFieldsRelatedAttributes->id = "id";
static::$databaseFieldsRelatedAttributes->descricao = "descricao";
static::$databaseFieldsRelatedAttributes->corporacao_id_INT = "corporacaoId";
static::$databaseFieldsRelatedAttributes->seq_INT = "seqInt";
static::$databaseFieldsRelatedAttributes->latitude_INT = "latitudeInt";
static::$databaseFieldsRelatedAttributes->longitude_INT = "longitudeInt";
static::$databaseFieldsRelatedAttributes->tarefa_cotidiano_id_INT = "tarefaCotidianoId";

            }           
        }

public function setAllFieldsToHumanFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["descricao"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["seq_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["latitude_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["longitude_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["tarefa_cotidiano_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
}

public function setAllFieldsToDatabaseFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["descricao"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["seq_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["latitude_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["longitude_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["tarefa_cotidiano_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
}

  public function getFkObjCorporacao() 
                {
                    if($this->objCorporacao == null)
                    {                        
                        $this->objCorporacao = new EXTDAO_Corporacao_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getCorporacao_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objCorporacao->clear();
                    }
                    elseif($this->objCorporacao->getId() != $idFK)
                    {
                        $this->objCorporacao->select($idFK);
                    }
                    return $this->objCorporacao;
                }
  public function getFkObjTarefa_cotidiano() 
                {
                    if($this->objTarefa_cotidiano == null)
                    {                        
                        $this->objTarefa_cotidiano = new EXTDAO_Tarefa_cotidiano_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getTarefa_cotidiano_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objTarefa_cotidiano->clear();
                    }
                    elseif($this->objTarefa_cotidiano->getId() != $idFK)
                    {
                        $this->objTarefa_cotidiano->select($idFK);
                    }
                    return $this->objTarefa_cotidiano;
                }


public function setLabels()
        {$this->labelId = "";
$this->labelDescricao = "";
$this->labelCorporacaoId = "";
$this->labelSeqInt = "";
$this->labelLatitudeInt = "";
$this->labelLongitudeInt = "";
$this->labelTarefaCotidianoId = "";
}

public function __actionAdd($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                $this->setByObject($parameters);
                $this->setDataCadastroAndOffsetInSecondsFromUser($parameters);
                
                $this->formatarParaSQL();
                $msg = $this->insert(true);
                
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                return new Mensagem(null, I18N::getExpression("Tarefa cotidiano item adicionado com sucesso."));
                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionEdit($parameters = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try {

                $this->setByObject($parameters);
                $this->setAllFieldsToHumanFormat();
                
                $this->formatarParaSQL();
                $msg = $this->updateByInstanceUsingParameters($this->id, $parameters, true);
                if($msg != null && $msg->erro())
                {
                     return $msg;
                }
        
                return new Mensagem(null, I18N::getExpression("Tarefa cotidiano item editado com sucesso."));
            }
            catch(Exception $ex)
            {                
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionRemove($parameters = null)
        {
            $successulRemovalCount = 0;
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                foreach($parameters as $record)
                {
                    if(is_numeric($record->id))
                    {
                        $msg = $this->delete($record->id, true);
                        if($msg == null || $msg->ok())
                        {
                            $successulRemovalCount++;
                        }
                    }
                }
                
                if($successulRemovalCount > 0)
                {
                    $successMessage = count($successulRemovalCount) > 1 ? I18N::getExpression("{0} Tarefa cotidiano item foram removidos com sucesso.", $successulRemovalCount) : I18N::getExpression("Tarefa cotidiano item removido com sucesso.");
                    return new Mensagem(null, $successMessage);
                }
                else
                {
                    $errorMessage = count($parameters) > 1 ? I18N::getExpression("Falha ao remover Tarefa cotidiano item.") : I18N::getExpression("Falha ao remover Tarefa cotidiano item.");
                    return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, $errorMessage);                    
                }
            }
            catch (Exception $ex) 
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }

public function __getRecord($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }
            
            try
            {            
                $returnObject = new stdClass();            
                $whereClause = static::getWhereClauseForFilter($parameters->filterParameters);
                $query = "SELECT  id, descricao, corporacao_id_INT, seq_INT, latitude_INT, longitude_INT, tarefa_cotidiano_id_INT FROM tarefa_cotidiano_item {$whereClause}";
    
                $msg = $this->database->queryMensagem($query);                
                if ($msg == null)
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Não foi encontrado nenhum registro"));
                    }                    
                    $resultSet = $this->database->result;
                    $resultObject = static::mapResultSetDataToRecord($resultSet);
                    
                    $returnObject->formData = $resultObject;
                    $returnObject->comboBoxesData = null;
                    if($parameters->loadComboBoxesData)
                    {
                        $this->setByObject($resultObject);
                        $returnObject->comboBoxesData =  $this->getFormComboBoxesData();
                    }                    
    
                    
    
                    return new Mensagem_generica($returnObject);
                } 
                else 
                {
                    return $msg;
                }
                
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }


        
        public function __getFilterComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
                
        public function __getFormComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
        
        protected function getFormComboBoxesData()
        {
            $comboBoxesData = new stdClass();
            $listParameters = new stdClass();
            $listParameters->listMappingType = static::LIST_MAPPING_TYPE_COMBOBOX;
            
            
                    
                    $objTarefaCotidiano = new EXTDAO_Tarefa_cotidiano();
                    $comboBoxesData->fieldTarefaCotidianoId = $objTarefaCotidiano->__getList($listParameters);

            return $comboBoxesData;
        }
        
        


        
        public static function setListAliasTypes()
        {
            if(is_null(static::$listAliasTypes))
            {
                static::$listAliasTypes = new stdClass();
                static::$listAliasTypes->tarefa_cotidiano__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->tarefa_cotidiano_item__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->tarefa_cotidiano_item__descricao = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->tarefa_cotidiano_item__seq_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->tarefa_cotidiano_item__latitude_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->tarefa_cotidiano_item__longitude_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->tarefa_cotidiano_item__relatorio_id_INT = static::TIPO_VARIAVEL_INTEGER;
            }        
        }
        
        public static function setListAliasRelatedAttributes()
        {
            if(is_null(static::$listAliasRelatedAttributes))
            {
                static::$listAliasRelatedAttributes = new stdClass();
                static::$listAliasRelatedAttributes->tarefa_cotidiano__id = "tarefaCotidianoId";
static::$listAliasRelatedAttributes->tarefa_cotidiano_item__id = "id";
static::$listAliasRelatedAttributes->tarefa_cotidiano_item__descricao = "descricao";
static::$listAliasRelatedAttributes->tarefa_cotidiano_item__seq_INT = "seqInt";
static::$listAliasRelatedAttributes->tarefa_cotidiano_item__latitude_INT = "latitudeInt";
static::$listAliasRelatedAttributes->tarefa_cotidiano_item__longitude_INT = "longitudeInt";
static::$listAliasRelatedAttributes->tarefa_cotidiano_item__relatorio_id_INT = "relatorioId";
            }         
        }

        public function __getList($parameters = null, $idCorporacao = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try
            {
                $filterParameters = new stdClass();
                if(isset($parameters->filterParameters))
                {
                    $filterParameters = $parameters->filterParameters;
                }
                                    
                            
            if(is_null($idCorporacao))
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            ListParameters::addFilterParameterToObject($filterParameters, "corporacaoId", $idCorporacao);           
                
                
                $sortingParameters = null;
                if(isset($parameters->sortingParameters))
                {
                    $sortingParameters = $parameters->sortingParameters;
                }
                
                $paginationParameters = null;
                if(isset($parameters->paginationParameters))
                {
                    $paginationParameters = $parameters->paginationParameters;
                }
                
                $listMappingType = static::getListMappingType($parameters->listMappingType);
                $mainTableAlias = "tci";

                $whereClause = static::getWhereClauseForFilter($filterParameters, $mainTableAlias);
                $orderByClause = static::getOrderByClauseForFilter($sortingParameters, $mainTableAlias);
                $limitClause = static::getLimitClauseForFilter($paginationParameters);

                $queryWithoutLimit = "SELECT tci.id FROM tarefa_cotidiano_item tci {$whereClause}";
                $query = "SELECT tc.id AS tarefa_cotidiano__id, tci.id AS tarefa_cotidiano_item__id, tci.descricao AS tarefa_cotidiano_item__descricao, tci.seq_INT AS tarefa_cotidiano_item__seq_INT, tci.latitude_INT AS tarefa_cotidiano_item__latitude_INT, tci.longitude_INT AS tarefa_cotidiano_item__longitude_INT, tci.relatorio_id_INT AS tarefa_cotidiano_item__relatorio_id_INT FROM tarefa_cotidiano_item tci LEFT JOIN tarefa_cotidiano tc ON tc.id = tci.tarefa_cotidiano_id_INT {$whereClause} {$orderByClause} {$limitClause}";

                $msg = $this->database->queryMensagem($queryWithoutLimit);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                
                $totalNumberOfRecords = $this->database->rows();
                if($totalNumberOfRecords == 0)
                {
                    return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                }                        
                        
                $msg = $this->database->queryMensagem($query);
                
                //operacao realizaca com sucesso
                if ($msg == null) 
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                    }                        
                
                    $resultSet = $this->database->result;

                    $objReturn = new stdClass();
                    $objReturn->dataSet = static::mapResultSetDataToList($resultSet, $listMappingType, static::$listAliasTypes);

                    if(!Helper::isNullOrEmpty($paginationParameters->recordsPerPage) && !Helper::isNullOrEmpty($paginationParameters->currentPage))
                    {
                        $lastPage = ceil($totalNumberOfRecords / $paginationParameters->recordsPerPage);
                        $objReturn->paginationParameters = new PaginationParams($paginationParameters->recordsPerPage, $paginationParameters->currentPage, $lastPage);
                    }
                    
                    return new Mensagem_generica($objReturn);
                } 
                else
                {
                    return $msg;
                }                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }



public function getId()
            {
                return $this->id;
            }

public function getDescricao()
            {
                return $this->descricao;
            }

public function getCorporacaoId()
            {
                return $this->corporacaoId;
            }

public function getCorporacao_id_INT()
                {
                    return $this->corporacaoId;
                }

public function getSeqInt()
            {
                return $this->seqInt;
            }

public function getSeq_INT()
                {
                    return $this->seqInt;
                }

public function getLatitudeInt()
            {
                return $this->latitudeInt;
            }

public function getLatitude_INT()
                {
                    return $this->latitudeInt;
                }

public function getLongitudeInt()
            {
                return $this->longitudeInt;
            }

public function getLongitude_INT()
                {
                    return $this->longitudeInt;
                }

public function getTarefaCotidianoId()
            {
                return $this->tarefaCotidianoId;
            }

public function getTarefa_cotidiano_id_INT()
                {
                    return $this->tarefaCotidianoId;
                }



function setId($value)
            {
                $this->id = $value;
            }

function setDescricao($value)
            {
                $this->descricao = $value;
            }

function setCorporacaoId($value)
            {
                $this->corporacaoId = $value;
            }

public function setCorporacao_id_INT($value)
                { 
                    $this->corporacaoId = $value; 
                }

function setSeqInt($value)
            {
                $this->seqInt = $value;
            }

public function setSeq_INT($value)
                { 
                    $this->seqInt = $value; 
                }

function setLatitudeInt($value)
            {
                $this->latitudeInt = $value;
            }

public function setLatitude_INT($value)
                { 
                    $this->latitudeInt = $value; 
                }

function setLongitudeInt($value)
            {
                $this->longitudeInt = $value;
            }

public function setLongitude_INT($value)
                { 
                    $this->longitudeInt = $value; 
                }

function setTarefaCotidianoId($value)
            {
                $this->tarefaCotidianoId = $value;
            }

public function setTarefa_cotidiano_id_INT($value)
                { 
                    $this->tarefaCotidianoId = $value; 
                }



public function clear()
        {$this->id = null;
$this->descricao = null;
$this->corporacaoId = null;
if($this->objCorporacao != null) unset($this->objCorporacao);
$this->seqInt = null;
$this->latitudeInt = null;
$this->longitudeInt = null;
$this->tarefaCotidianoId = null;
if($this->objTarefa_cotidiano != null) unset($this->objTarefa_cotidiano);
if(is_array($this->arrEstadosCampos)){
                        unset($this->arrEstadosCampos);
                     $this->arrEstadosCampos = array();
                    }
                }

public function formatarParaSQL()
        {
if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->id)){
$this->id = $this->formatarIntegerParaComandoSQL($this->id);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->id);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->descricao)){
$this->descricao = $this->formatarStringParaComandoSQL($this->descricao);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->descricao);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->corporacaoId)){
$this->corporacaoId = $this->formatarIntegerParaComandoSQL($this->corporacaoId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->corporacaoId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->seqInt)){
$this->seqInt = $this->formatarIntegerParaComandoSQL($this->seqInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->seqInt);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->latitudeInt)){
$this->latitudeInt = $this->formatarIntegerParaComandoSQL($this->latitudeInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->latitudeInt);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->longitudeInt)){
$this->longitudeInt = $this->formatarIntegerParaComandoSQL($this->longitudeInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->longitudeInt);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->tarefaCotidianoId)){
$this->tarefaCotidianoId = $this->formatarIntegerParaComandoSQL($this->tarefaCotidianoId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->tarefaCotidianoId);
}

}

public function formatarParaExibicao()
        {if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->descricao)){
$this->descricao = $this->formatarStringParaExibicao($this->descricao);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->descricao);
}

}

function select($id, $idCorporacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }   

            $sql = "SELECT id, descricao, corporacao_id_INT, seq_INT, latitude_INT, longitude_INT, tarefa_cotidiano_id_INT FROM tarefa_cotidiano_item WHERE id = $id AND corporacao_id_INT = $idCorporacao";

            $msg = $this->database->queryMensagem($sql);
            $result = $this->database->result;

            if($msg != null && $msg->erro() )
            { 
            		$this->clear();
            		return $msg;
        	} 
        	else if($this->database->rows() == 0)
        	{
                return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nenhum registro foi encontrado."));
            }
        

            $row = $this->database->fetchArray(MYSQLI_NUM);
            
            		$this->id = $row[0];
		$this->descricao = $row[1];
		$this->seqInt = $row[3];
		$this->latitudeInt = $row[4];
		$this->longitudeInt = $row[5];
		$this->tarefaCotidianoId = $row[6];

            
        }

public function delete($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (is_null($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }

            $sql = "DELETE FROM tarefa_cotidiano_item WHERE id= $id AND corporacao_id_INT = $idCorporacao";
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                    && $sincronizar
                        && static::idSistemaTabela() != null
                            && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_REMOVE,
                                null,
                                null,
                                $id,
                                $this->database);
            }

            return $msg;
        }

public function insert($sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null, $opcoes = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            
            
            if (is_null($opcoes))
            {
                $opcoes = $this->opcoesDAO;
            }

            if (!is_numeric($this->id))
            {
                $this->id = "null";
                if ($opcoes == null || $opcoes[Database::OPCAO_GERAR_ID] != false) 
                {
                    $this->id = EXTDAO_Sistema_sequencia::gerarId(static::nomeTabela());                    
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }    
                    else if($msg == null)
                    {
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
                else 
                {
                    $this->id = null;
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao));
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }
                    else if($msg == null)
                    {
                        $this->id = $this->database->getLastInsertId();
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
            }
            else 
            {
                $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }                        
                else if($msg == null)
                {
                   $msg = new Mensagem_token();
                   $msg->mValor = $this->id;
                }
            }
            if($msg == null || $msg->ok())
            {                
                if($sincronizar
                    && static::idSistemaTabela() != null
                    && isset($this->id)){
                    
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_INSERT,
                                null,
                                null,
                                $this->id,
                                $this->database); 
                	}
            } 
            else
            {
                $this->id = null;
            }
            
            return $msg;
            
        }

public function getSQLInsert($idCorporacao){ 

return "INSERT INTO tarefa_cotidiano_item (id, descricao, corporacao_id_INT, seq_INT, latitude_INT, longitude_INT, tarefa_cotidiano_id_INT) VALUES ( $this->id ,  $this->descricao ,  $idCorporacao ,  $this->seqInt ,  $this->latitudeInt ,  $this->longitudeInt ,  $this->tarefaCotidianoId ) "; 

}




        
        public function updateByParameters($id, $objParametros, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao))
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();

            
                
                if (isset($objParametros->descricao)) 
                {
                    $arrUpdateFields[] = " descricao = {$objParametros->descricao} ";
                }


                
                if (isset($objParametros->seqInt)) 
                {
                    $arrUpdateFields[] = " seq_INT = {$objParametros->seqInt} ";
                }


                
                if (isset($objParametros->latitudeInt)) 
                {
                    $arrUpdateFields[] = " latitude_INT = {$objParametros->latitudeInt} ";
                }


                
                if (isset($objParametros->longitudeInt)) 
                {
                    $arrUpdateFields[] = " longitude_INT = {$objParametros->longitudeInt} ";
                }


                
                if (isset($objParametros->tarefaCotidianoId)) 
                {
                    $arrUpdateFields[] = " tarefa_cotidiano_id_INT = {$objParametros->tarefaCotidianoId} ";
                }



            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE tarefa_cotidiano_item SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";

            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null)
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function update($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($this->descricao)) 
                {                                      
                    $arrUpdateFields[] = " descricao = {$this->descricao} ";
                }


                
                if (isset($this->seqInt)) 
                {                                      
                    $arrUpdateFields[] = " seq_INT = {$this->seqInt} ";
                }


                
                if (isset($this->latitudeInt)) 
                {                                      
                    $arrUpdateFields[] = " latitude_INT = {$this->latitudeInt} ";
                }


                
                if (isset($this->longitudeInt)) 
                {                                      
                    $arrUpdateFields[] = " longitude_INT = {$this->longitudeInt} ";
                }


                
                if (isset($this->tarefaCotidianoId)) 
                {                                      
                    $arrUpdateFields[] = " tarefa_cotidiano_id_INT = {$this->tarefaCotidianoId} ";
                }


            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE tarefa_cotidiano_item SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function updateByInstanceUsingParameters($id, $parameters, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($parameters->descricao)) 
                {                                      
                    $arrUpdateFields[] = " descricao = {$this->descricao} ";
                }
                
                if (isset($parameters->seqInt)) 
                {                                      
                    $arrUpdateFields[] = " seq_INT = {$this->seqInt} ";
                }
                
                if (isset($parameters->latitudeInt)) 
                {                                      
                    $arrUpdateFields[] = " latitude_INT = {$this->latitudeInt} ";
                }
                
                if (isset($parameters->longitudeInt)) 
                {                                      
                    $arrUpdateFields[] = " longitude_INT = {$this->longitudeInt} ";
                }
                
                if (isset($parameters->tarefaCotidianoId)) 
                {                                      
                    $arrUpdateFields[] = " tarefa_cotidiano_id_INT = {$this->tarefaCotidianoId} ";
                }
            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE tarefa_cotidiano_item SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }

}

?>
