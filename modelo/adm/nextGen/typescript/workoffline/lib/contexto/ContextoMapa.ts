import { ContextoGenerico } from 'lib/contexto';
import { Default } from 'lib/uiTemplate';
import { Mapa } from "lib/uiComponente";

export abstract class ContextoMapa extends ContextoGenerico
{
    public mapObject: Mapa<ContextoMapa>;
    public mapContainerSelector :string;

    public constructor(domRootSelector: string, mapContainerSelector :string)
    {
        super(domRootSelector);
        this.mapContainerSelector = mapContainerSelector;
    }

    public initializeMap(): void
    {
        this.mapObject = new Mapa<ContextoMapa>(this);

        this.mapObject.setTemplate(new Default());
        this.mapObject.setDefaultValues();

        this.mapObject.render();
        this.mapObject.addResizeListener();
    }

    public abstract getDefaultMarkerFolder() :string;

}