import { Model } from "lib/model";
import { GeoLocationHelper, Helper } from 'lib/geral';
import { WifiNetwork } from "aplicacao/modulo/mapa/model";
import { Empresa as EmpresaRemoteModel } from 'aplicacao/modulo/mapa/remoteModel'

export class Empresa extends Model
{
    public latitude :number;
    public longitude :number;
    public nomeEmpresa :string;
    public idEmpresa :number;
    public wifiNetworks :Array<WifiNetwork>;

    public static parseFromRemoteObject(remoteObject: EmpresaRemoteModel)
    {
        let instance = new Empresa();

        instance.latitude = GeoLocationHelper.getFloatValue(remoteObject.latitude);
        instance.longitude = GeoLocationHelper.getFloatValue(remoteObject.longitude);
        instance.idEmpresa = Helper.parseInt(remoteObject.idEmpresa);
        instance.nomeEmpresa = remoteObject.nomeEmpresa;
        instance.wifiNetworks = new Array<WifiNetwork>();

        return instance;
    }

    public setValidWifiNetworks(wifiNetworks: Array<WifiNetwork>)
    {
        if(wifiNetworks != null)
        {
            for(let wifiNetwork of wifiNetworks)
            {
                if(this.idEmpresa == wifiNetwork.idEmpresa)
                {
                    this.wifiNetworks.push(wifiNetwork);
                }
            }
        }

    }

}