<?php


        /*

        Arquivo gerado através de gerador de código em 12/05/2018 as 14:45:43.
        Para que o arquivo não seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

        Tabela correspondente: despesa_cotidiano
        Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

        */
        


class DAO_Despesa_cotidiano extends Generic_DAO
                        {
                            const NOME_ENTIDADE = "despesa_cotidiano";

                            public static $nomeTabela = null;
                            public static $idSistemaTabela = false;
                            public static $databaseFieldsRelatedAttributes = null;
	                        public static $databaseFieldTypes = null;
	                        public static $databaseFieldNames = null;
	                        public static $listAliasTypes = null;
	                        public static $listAliasRelatedAttributes = null;
	                        
	                        public $id;
public $despesaDaEmpresaId;
public $despesaDoUsuarioId;
public $cadastroUsuarioId;
public $idTipoDespesaCotidianoInt;
public $parametroInt;
public $parametroOffsec;
public $parametroSec;
public $parametroJson;
public $valorFloat;
public $dataLimiteCotidianoSec;
public $dataLimiteCotidianoOffsec;
public $corporacaoId;

public $labelId;
public $labelDespesaDaEmpresaId;
public $labelDespesaDoUsuarioId;
public $labelCadastroUsuarioId;
public $labelIdTipoDespesaCotidianoInt;
public $labelParametroInt;
public $labelParametroOffsec;
public $labelParametroSec;
public $labelParametroJson;
public $labelValorFloat;
public $labelDataLimiteCotidianoSec;
public $labelDataLimiteCotidianoOffsec;
public $labelCorporacaoId;

public static $arrPK = array("id");

public function __construct($configDAO = null)
        {
            parent::__construct($configDAO);
            static::__setStaticValues($this->database);           
        }

public static function idSistemaTabela($databaseObject = null){
            if(!(static::$idSistemaTabela))
            {
                static::$idSistemaTabela = EXTDAO_Sistema_tabela::getIdDaSistemaTabela(static::nomeTabela(), $databaseObject);
            }
            return static::$idSistemaTabela;
        }

public function getIdSistemaTabela(){
            return static::idSistemaTabela();
        }

public static function nomeTabela(){
            if(is_null(static::$nomeTabela))
            {
                static::$nomeTabela = "despesa_cotidiano";
            }
            return static::$nomeTabela;
        }

public function getNomeTabela(){
            return static::nomeTabela();
        }

public static function __setStaticValues($databaseObject = null)
                   {    
                        static::nomeTabela();
                        
                        static::idSistemaTabela($databaseObject);
                   
                        static::setDatabaseFieldNames();
                        static::setDatabaseFieldTypes();
                        static::setDatabaseFieldsRelatedAttributeNames();
                        
                        static::setListAliasTypes();
                        static::setListAliasRelatedAttributes();
                   }

public function getExpressaoLabel()
                   {
                        return "{id}";
                   }

public static function setDatabaseFieldNames()
        {
            if(is_null(static::$databaseFieldNames))
            {
                static::$databaseFieldNames = new stdClass();
                static::$databaseFieldNames->id = "id";
static::$databaseFieldNames->despesaDaEmpresaId = "despesa_da_empresa_id_INT";
static::$databaseFieldNames->despesaDoUsuarioId = "despesa_do_usuario_id_INT";
static::$databaseFieldNames->cadastroUsuarioId = "cadastro_usuario_id_INT";
static::$databaseFieldNames->idTipoDespesaCotidianoInt = "id_tipo_despesa_cotidiano_INT";
static::$databaseFieldNames->parametroInt = "parametro_INT";
static::$databaseFieldNames->parametroOffsec = "parametro_OFFSEC";
static::$databaseFieldNames->parametroSec = "parametro_SEC";
static::$databaseFieldNames->parametroJson = "parametro_json";
static::$databaseFieldNames->valorFloat = "valor_FLOAT";
static::$databaseFieldNames->dataLimiteCotidianoSec = "data_limite_cotidiano_SEC";
static::$databaseFieldNames->dataLimiteCotidianoOffsec = "data_limite_cotidiano_OFFSEC";
static::$databaseFieldNames->corporacaoId = "corporacao_id_INT";

            }
                      
        }

public static function setDatabaseFieldTypes()
        {
            if(is_null(static::$databaseFieldTypes))
            {
                static::$databaseFieldTypes = new stdClass();
                static::$databaseFieldTypes->id = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->despesaDaEmpresaId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->despesaDoUsuarioId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->cadastroUsuarioId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->idTipoDespesaCotidianoInt = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->parametroInt = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->parametroOffsec = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$databaseFieldTypes->parametroSec = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$databaseFieldTypes->parametroJson = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->valorFloat = static::TIPO_VARIAVEL_FLOAT;
static::$databaseFieldTypes->dataLimiteCotidianoSec = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$databaseFieldTypes->dataLimiteCotidianoOffsec = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$databaseFieldTypes->corporacaoId = static::TIPO_VARIAVEL_INTEGER;

            }
            
        }

public static function setDatabaseFieldsRelatedAttributeNames()
        {
            if(is_null(static::$databaseFieldsRelatedAttributes))
            {
                static::$databaseFieldsRelatedAttributes = new stdClass();
                static::$databaseFieldsRelatedAttributes->id = "id";
static::$databaseFieldsRelatedAttributes->despesa_da_empresa_id_INT = "despesaDaEmpresaId";
static::$databaseFieldsRelatedAttributes->despesa_do_usuario_id_INT = "despesaDoUsuarioId";
static::$databaseFieldsRelatedAttributes->cadastro_usuario_id_INT = "cadastroUsuarioId";
static::$databaseFieldsRelatedAttributes->id_tipo_despesa_cotidiano_INT = "idTipoDespesaCotidianoInt";
static::$databaseFieldsRelatedAttributes->parametro_INT = "parametroInt";
static::$databaseFieldsRelatedAttributes->parametro_OFFSEC = "parametroOffsec";
static::$databaseFieldsRelatedAttributes->parametro_SEC = "parametroSec";
static::$databaseFieldsRelatedAttributes->parametro_json = "parametroJson";
static::$databaseFieldsRelatedAttributes->valor_FLOAT = "valorFloat";
static::$databaseFieldsRelatedAttributes->data_limite_cotidiano_SEC = "dataLimiteCotidianoSec";
static::$databaseFieldsRelatedAttributes->data_limite_cotidiano_OFFSEC = "dataLimiteCotidianoOffsec";
static::$databaseFieldsRelatedAttributes->corporacao_id_INT = "corporacaoId";

            }           
        }

public function setAllFieldsToHumanFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["despesa_da_empresa_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["despesa_do_usuario_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["cadastro_usuario_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["id_tipo_despesa_cotidiano_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["parametro_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["parametro_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["parametro_SEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["parametro_json"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["valor_FLOAT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["data_limite_cotidiano_SEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["data_limite_cotidiano_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
}

public function setAllFieldsToDatabaseFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["despesa_da_empresa_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["despesa_do_usuario_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["cadastro_usuario_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["id_tipo_despesa_cotidiano_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["parametro_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["parametro_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["parametro_SEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["parametro_json"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["valor_FLOAT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["data_limite_cotidiano_SEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["data_limite_cotidiano_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
}

  public function getFkObjDespesa_da_empresa() 
                {
                    if($this->objDespesa_da_empresa == null)
                    {                        
                        $this->objDespesa_da_empresa = new EXTDAO_Despesa_da_empresa_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getDespesa_da_empresa_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objDespesa_da_empresa->clear();
                    }
                    elseif($this->objDespesa_da_empresa->getId() != $idFK)
                    {
                        $this->objDespesa_da_empresa->select($idFK);
                    }
                    return $this->objDespesa_da_empresa;
                }
  public function getFkObjDespesa_do_usuario() 
                {
                    if($this->objDespesa_do_usuario == null)
                    {                        
                        $this->objDespesa_do_usuario = new EXTDAO_Despesa_do_usuario_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getDespesa_do_usuario_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objDespesa_do_usuario->clear();
                    }
                    elseif($this->objDespesa_do_usuario->getId() != $idFK)
                    {
                        $this->objDespesa_do_usuario->select($idFK);
                    }
                    return $this->objDespesa_do_usuario;
                }
  public function getFkObjCadastro_usuario() 
                {
                    if($this->objCadastro_usuario == null)
                    {                        
                        $this->objCadastro_usuario = new EXTDAO_Cadastro_usuario_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getCadastro_usuario_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objCadastro_usuario->clear();
                    }
                    elseif($this->objCadastro_usuario->getId() != $idFK)
                    {
                        $this->objCadastro_usuario->select($idFK);
                    }
                    return $this->objCadastro_usuario;
                }
  public function getFkObjCorporacao() 
                {
                    if($this->objCorporacao == null)
                    {                        
                        $this->objCorporacao = new EXTDAO_Corporacao_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getCorporacao_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objCorporacao->clear();
                    }
                    elseif($this->objCorporacao->getId() != $idFK)
                    {
                        $this->objCorporacao->select($idFK);
                    }
                    return $this->objCorporacao;
                }


public function setLabels()
        {$this->labelId = "";
$this->labelDespesaDaEmpresaId = "";
$this->labelDespesaDoUsuarioId = "";
$this->labelCadastroUsuarioId = "";
$this->labelIdTipoDespesaCotidianoInt = "";
$this->labelParametroInt = "";
$this->labelParametroOffsec = "";
$this->labelParametroSec = "";
$this->labelParametroJson = "";
$this->labelValorFloat = "";
$this->labelDataLimiteCotidianoSec = "";
$this->labelDataLimiteCotidianoOffsec = "";
$this->labelCorporacaoId = "";
}

public function __actionAdd($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                $this->setByObject($parameters);
                $this->setDataCadastroAndOffsetInSecondsFromUser($parameters);
                
                $this->formatarParaSQL();
                $msg = $this->insert(true);
                
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                return new Mensagem(null, I18N::getExpression("Despesa cotidiano adicionado com sucesso."));
                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionEdit($parameters = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try {

                $this->setByObject($parameters);
                $this->setAllFieldsToHumanFormat();
                
                $this->formatarParaSQL();
                $msg = $this->updateByInstanceUsingParameters($this->id, $parameters, true);
                if($msg != null && $msg->erro())
                {
                     return $msg;
                }
        
                return new Mensagem(null, I18N::getExpression("Despesa cotidiano editado com sucesso."));
            }
            catch(Exception $ex)
            {                
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionRemove($parameters = null)
        {
            $successulRemovalCount = 0;
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                foreach($parameters as $record)
                {
                    if(is_numeric($record->id))
                    {
                        $msg = $this->delete($record->id, true);
                        if($msg == null || $msg->ok())
                        {
                            $successulRemovalCount++;
                        }
                    }
                }
                
                if($successulRemovalCount > 0)
                {
                    $successMessage = count($successulRemovalCount) > 1 ? I18N::getExpression("{0} Despesa cotidiano foram removidos com sucesso.", $successulRemovalCount) : I18N::getExpression("Despesa cotidiano removido com sucesso.");
                    return new Mensagem(null, $successMessage);
                }
                else
                {
                    $errorMessage = count($parameters) > 1 ? I18N::getExpression("Falha ao remover Despesa cotidiano.") : I18N::getExpression("Falha ao remover Despesa cotidiano.");
                    return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, $errorMessage);                    
                }
            }
            catch (Exception $ex) 
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }

public function __getRecord($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }
            
            try
            {            
                $returnObject = new stdClass();            
                $whereClause = static::getWhereClauseForFilter($parameters->filterParameters);
                $query = "SELECT  id, despesa_da_empresa_id_INT, despesa_do_usuario_id_INT, cadastro_usuario_id_INT, id_tipo_despesa_cotidiano_INT, parametro_INT, parametro_OFFSEC, parametro_SEC, parametro_json, valor_FLOAT, data_limite_cotidiano_SEC, data_limite_cotidiano_OFFSEC, corporacao_id_INT FROM despesa_cotidiano {$whereClause}";
    
                $msg = $this->database->queryMensagem($query);                
                if ($msg == null)
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Não foi encontrado nenhum registro"));
                    }                    
                    $resultSet = $this->database->result;
                    $resultObject = static::mapResultSetDataToRecord($resultSet);
                    
                    $returnObject->formData = $resultObject;
                    $returnObject->comboBoxesData = null;
                    if($parameters->loadComboBoxesData)
                    {
                        $this->setByObject($resultObject);
                        $returnObject->comboBoxesData =  $this->getFormComboBoxesData();
                    }                    
    
                    
    
                    return new Mensagem_generica($returnObject);
                } 
                else 
                {
                    return $msg;
                }
                
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }


        
        public function __getFilterComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
                
        public function __getFormComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
        
        protected function getFormComboBoxesData()
        {
            $comboBoxesData = new stdClass();
            $listParameters = new stdClass();
            $listParameters->listMappingType = static::LIST_MAPPING_TYPE_COMBOBOX;
            
            

            return $comboBoxesData;
        }
        
        


        
        public static function setListAliasTypes()
        {
            if(is_null(static::$listAliasTypes))
            {
                static::$listAliasTypes = new stdClass();
                static::$listAliasTypes->despesa_cotidiano__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->despesa_cotidiano__despesa_da_empresa_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->despesa_cotidiano__despesa_do_usuario_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->despesa_cotidiano__cadastro_usuario_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->despesa_cotidiano__id_tipo_despesa_cotidiano_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->despesa_cotidiano__parametro_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->despesa_cotidiano__parametro_SEC = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$listAliasTypes->despesa_cotidiano__parametro_json = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->despesa_cotidiano__valor_FLOAT = static::TIPO_VARIAVEL_FLOAT;
static::$listAliasTypes->despesa_cotidiano__data_limite_cotidiano_SEC = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
            }        
        }
        
        public static function setListAliasRelatedAttributes()
        {
            if(is_null(static::$listAliasRelatedAttributes))
            {
                static::$listAliasRelatedAttributes = new stdClass();
                static::$listAliasRelatedAttributes->despesa_cotidiano__id = "id";
static::$listAliasRelatedAttributes->despesa_cotidiano__despesa_da_empresa_id_INT = "despesaDaEmpresaId";
static::$listAliasRelatedAttributes->despesa_cotidiano__despesa_do_usuario_id_INT = "despesaDoUsuarioId";
static::$listAliasRelatedAttributes->despesa_cotidiano__cadastro_usuario_id_INT = "cadastroUsuarioId";
static::$listAliasRelatedAttributes->despesa_cotidiano__id_tipo_despesa_cotidiano_INT = "idTipoDespesaCotidianoInt";
static::$listAliasRelatedAttributes->despesa_cotidiano__parametro_INT = "parametroInt";
static::$listAliasRelatedAttributes->despesa_cotidiano__parametro_SEC = "parametroSec";
static::$listAliasRelatedAttributes->despesa_cotidiano__parametro_json = "parametroJson";
static::$listAliasRelatedAttributes->despesa_cotidiano__valor_FLOAT = "valorFloat";
static::$listAliasRelatedAttributes->despesa_cotidiano__data_limite_cotidiano_SEC = "dataLimiteCotidianoSec";
            }         
        }

        public function __getList($parameters = null, $idCorporacao = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try
            {
                $filterParameters = new stdClass();
                if(isset($parameters->filterParameters))
                {
                    $filterParameters = $parameters->filterParameters;
                }
                                    
                            
            if(is_null($idCorporacao))
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            ListParameters::addFilterParameterToObject($filterParameters, "corporacaoId", $idCorporacao);           
                
                
                $sortingParameters = null;
                if(isset($parameters->sortingParameters))
                {
                    $sortingParameters = $parameters->sortingParameters;
                }
                
                $paginationParameters = null;
                if(isset($parameters->paginationParameters))
                {
                    $paginationParameters = $parameters->paginationParameters;
                }
                
                $listMappingType = static::getListMappingType($parameters->listMappingType);
                $mainTableAlias = "dc";

                $whereClause = static::getWhereClauseForFilter($filterParameters, $mainTableAlias);
                $orderByClause = static::getOrderByClauseForFilter($sortingParameters, $mainTableAlias);
                $limitClause = static::getLimitClauseForFilter($paginationParameters);

                $queryWithoutLimit = "SELECT dc.id FROM despesa_cotidiano dc {$whereClause}";
                $query = "SELECT dc.id AS despesa_cotidiano__id, dc.despesa_da_empresa_id_INT AS despesa_cotidiano__despesa_da_empresa_id_INT, dc.despesa_do_usuario_id_INT AS despesa_cotidiano__despesa_do_usuario_id_INT, dc.cadastro_usuario_id_INT AS despesa_cotidiano__cadastro_usuario_id_INT, dc.id_tipo_despesa_cotidiano_INT AS despesa_cotidiano__id_tipo_despesa_cotidiano_INT, dc.parametro_INT AS despesa_cotidiano__parametro_INT, dc.parametro_SEC AS despesa_cotidiano__parametro_SEC, dc.parametro_json AS despesa_cotidiano__parametro_json, dc.valor_FLOAT AS despesa_cotidiano__valor_FLOAT, dc.data_limite_cotidiano_SEC AS despesa_cotidiano__data_limite_cotidiano_SEC FROM despesa_cotidiano dc  {$whereClause} {$orderByClause} {$limitClause}";

                $msg = $this->database->queryMensagem($queryWithoutLimit);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                
                $totalNumberOfRecords = $this->database->rows();
                if($totalNumberOfRecords == 0)
                {
                    return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                }                        
                        
                $msg = $this->database->queryMensagem($query);
                
                //operacao realizaca com sucesso
                if ($msg == null) 
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                    }                        
                
                    $resultSet = $this->database->result;

                    $objReturn = new stdClass();
                    $objReturn->dataSet = static::mapResultSetDataToList($resultSet, $listMappingType, static::$listAliasTypes);

                    if(!Helper::isNullOrEmpty($paginationParameters->recordsPerPage) && !Helper::isNullOrEmpty($paginationParameters->currentPage))
                    {
                        $lastPage = ceil($totalNumberOfRecords / $paginationParameters->recordsPerPage);
                        $objReturn->paginationParameters = new PaginationParams($paginationParameters->recordsPerPage, $paginationParameters->currentPage, $lastPage);
                    }
                    
                    return new Mensagem_generica($objReturn);
                } 
                else
                {
                    return $msg;
                }                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }



public function getId()
            {
                return $this->id;
            }

public function getDespesaDaEmpresaId()
            {
                return $this->despesaDaEmpresaId;
            }

public function getDespesa_da_empresa_id_INT()
                {
                    return $this->despesaDaEmpresaId;
                }

public function getDespesaDoUsuarioId()
            {
                return $this->despesaDoUsuarioId;
            }

public function getDespesa_do_usuario_id_INT()
                {
                    return $this->despesaDoUsuarioId;
                }

public function getCadastroUsuarioId()
            {
                return $this->cadastroUsuarioId;
            }

public function getCadastro_usuario_id_INT()
                {
                    return $this->cadastroUsuarioId;
                }

public function getIdTipoDespesaCotidianoInt()
            {
                return $this->idTipoDespesaCotidianoInt;
            }

public function getId_tipo_despesa_cotidiano_INT()
                {
                    return $this->idTipoDespesaCotidianoInt;
                }

public function getParametroInt()
            {
                return $this->parametroInt;
            }

public function getParametro_INT()
                {
                    return $this->parametroInt;
                }

public function getParametroOffsec()
            {
                return $this->parametroOffsec;
            }

public function getParametro_OFFSEC()
                {
                    return $this->parametroOffsec;
                }

public function getParametroSec()
            {
                return $this->parametroSec;
            }

public function getParametro_SEC()
                {
                    return $this->parametroSec;
                }

public function getParametroJson()
            {
                return $this->parametroJson;
            }

public function getParametro_json()
                {
                    return $this->parametroJson;
                }

public function getValorFloat()
            {
                return $this->valorFloat;
            }

public function getValor_FLOAT()
                {
                    return $this->valorFloat;
                }

public function getDataLimiteCotidianoSec()
            {
                return $this->dataLimiteCotidianoSec;
            }

public function getData_limite_cotidiano_SEC()
                {
                    return $this->dataLimiteCotidianoSec;
                }

public function getDataLimiteCotidianoOffsec()
            {
                return $this->dataLimiteCotidianoOffsec;
            }

public function getData_limite_cotidiano_OFFSEC()
                {
                    return $this->dataLimiteCotidianoOffsec;
                }

public function getCorporacaoId()
            {
                return $this->corporacaoId;
            }

public function getCorporacao_id_INT()
                {
                    return $this->corporacaoId;
                }



function setId($value)
            {
                $this->id = $value;
            }

function setDespesaDaEmpresaId($value)
            {
                $this->despesaDaEmpresaId = $value;
            }

public function setDespesa_da_empresa_id_INT($value)
                { 
                    $this->despesaDaEmpresaId = $value; 
                }

function setDespesaDoUsuarioId($value)
            {
                $this->despesaDoUsuarioId = $value;
            }

public function setDespesa_do_usuario_id_INT($value)
                { 
                    $this->despesaDoUsuarioId = $value; 
                }

function setCadastroUsuarioId($value)
            {
                $this->cadastroUsuarioId = $value;
            }

public function setCadastro_usuario_id_INT($value)
                { 
                    $this->cadastroUsuarioId = $value; 
                }

function setIdTipoDespesaCotidianoInt($value)
            {
                $this->idTipoDespesaCotidianoInt = $value;
            }

public function setId_tipo_despesa_cotidiano_INT($value)
                { 
                    $this->idTipoDespesaCotidianoInt = $value; 
                }

function setParametroInt($value)
            {
                $this->parametroInt = $value;
            }

public function setParametro_INT($value)
                { 
                    $this->parametroInt = $value; 
                }

function setParametroOffsec($value)
            {
                $this->parametroOffsec = $value;
            }

public function setParametro_OFFSEC($value)
                { 
                    $this->parametroOffsec = $value; 
                }

function setParametroSec($value)
            {
                $this->parametroSec = $value;
            }

public function setParametro_SEC($value)
                { 
                    $this->parametroSec = $value; 
                }

function setParametroJson($value)
            {
                $this->parametroJson = $value;
            }

public function setParametro_json($value)
                { 
                    $this->parametroJson = $value; 
                }

function setValorFloat($value)
            {
                $this->valorFloat = $value;
            }

public function setValor_FLOAT($value)
                { 
                    $this->valorFloat = $value; 
                }

function setDataLimiteCotidianoSec($value)
            {
                $this->dataLimiteCotidianoSec = $value;
            }

public function setData_limite_cotidiano_SEC($value)
                { 
                    $this->dataLimiteCotidianoSec = $value; 
                }

function setDataLimiteCotidianoOffsec($value)
            {
                $this->dataLimiteCotidianoOffsec = $value;
            }

public function setData_limite_cotidiano_OFFSEC($value)
                { 
                    $this->dataLimiteCotidianoOffsec = $value; 
                }

function setCorporacaoId($value)
            {
                $this->corporacaoId = $value;
            }

public function setCorporacao_id_INT($value)
                { 
                    $this->corporacaoId = $value; 
                }



public function clear()
        {$this->id = null;
$this->despesaDaEmpresaId = null;
if($this->objDespesa_da_empresa != null) unset($this->objDespesa_da_empresa);
$this->despesaDoUsuarioId = null;
if($this->objDespesa_do_usuario != null) unset($this->objDespesa_do_usuario);
$this->cadastroUsuarioId = null;
if($this->objCadastro_usuario != null) unset($this->objCadastro_usuario);
$this->idTipoDespesaCotidianoInt = null;
$this->parametroInt = null;
$this->parametroOffsec = null;
$this->parametroSec = null;
$this->parametroJson = null;
$this->valorFloat = null;
$this->dataLimiteCotidianoSec = null;
$this->dataLimiteCotidianoOffsec = null;
$this->corporacaoId = null;
if($this->objCorporacao != null) unset($this->objCorporacao);
if(is_array($this->arrEstadosCampos)){
                        unset($this->arrEstadosCampos);
                     $this->arrEstadosCampos = array();
                    }
                }

public function formatarParaSQL()
        {
if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->id)){
$this->id = $this->formatarIntegerParaComandoSQL($this->id);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->id);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->despesaDaEmpresaId)){
$this->despesaDaEmpresaId = $this->formatarIntegerParaComandoSQL($this->despesaDaEmpresaId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->despesaDaEmpresaId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->despesaDoUsuarioId)){
$this->despesaDoUsuarioId = $this->formatarIntegerParaComandoSQL($this->despesaDoUsuarioId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->despesaDoUsuarioId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->cadastroUsuarioId)){
$this->cadastroUsuarioId = $this->formatarIntegerParaComandoSQL($this->cadastroUsuarioId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->cadastroUsuarioId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->idTipoDespesaCotidianoInt)){
$this->idTipoDespesaCotidianoInt = $this->formatarIntegerParaComandoSQL($this->idTipoDespesaCotidianoInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->idTipoDespesaCotidianoInt);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->parametroInt)){
$this->parametroInt = $this->formatarIntegerParaComandoSQL($this->parametroInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->parametroInt);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->parametroOffsec)){
$this->parametroOffsec = $this->formatarIntegerParaComandoSQL($this->parametroOffsec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->parametroOffsec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->parametroSec)){
$this->parametroSec = $this->formatarIntegerParaComandoSQL($this->parametroSec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->parametroSec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->parametroJson)){
$this->parametroJson = $this->formatarStringParaComandoSQL($this->parametroJson);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->parametroJson);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->valorFloat)){
$this->valorFloat = $this->formatarFloatParaComandoSQL($this->valorFloat);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->valorFloat);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->dataLimiteCotidianoSec)){
$this->dataLimiteCotidianoSec = $this->formatarIntegerParaComandoSQL($this->dataLimiteCotidianoSec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->dataLimiteCotidianoSec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->dataLimiteCotidianoOffsec)){
$this->dataLimiteCotidianoOffsec = $this->formatarIntegerParaComandoSQL($this->dataLimiteCotidianoOffsec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->dataLimiteCotidianoOffsec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->corporacaoId)){
$this->corporacaoId = $this->formatarIntegerParaComandoSQL($this->corporacaoId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->corporacaoId);
}

}

public function formatarParaExibicao()
        {if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->parametroJson)){
$this->parametroJson = $this->formatarStringParaExibicao($this->parametroJson);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->parametroJson);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->valorFloat)){
$this->valorFloat = $this->formatarFloatParaExibicao($this->valorFloat);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->valorFloat);
}

}

function select($id, $idCorporacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }   

            $sql = "SELECT id, despesa_da_empresa_id_INT, despesa_do_usuario_id_INT, cadastro_usuario_id_INT, id_tipo_despesa_cotidiano_INT, parametro_INT, parametro_OFFSEC, parametro_SEC, parametro_json, valor_FLOAT, data_limite_cotidiano_SEC, data_limite_cotidiano_OFFSEC, corporacao_id_INT FROM despesa_cotidiano WHERE id = $id AND corporacao_id_INT = $idCorporacao";

            $msg = $this->database->queryMensagem($sql);
            $result = $this->database->result;

            if($msg != null && $msg->erro() )
            { 
            		$this->clear();
            		return $msg;
        	} 
        	else if($this->database->rows() == 0)
        	{
                return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nenhum registro foi encontrado."));
            }
        

            $row = $this->database->fetchArray(MYSQLI_NUM);
            
            		$this->id = $row[0];
		$this->despesaDaEmpresaId = $row[1];
		$this->despesaDoUsuarioId = $row[2];
		$this->cadastroUsuarioId = $row[3];
		$this->idTipoDespesaCotidianoInt = $row[4];
		$this->parametroInt = $row[5];
		$this->parametroOffsec = $row[6];
		$this->parametroSec = $row[7];
		$this->parametroJson = $row[8];
		$this->valorFloat = $row[9];
		$this->dataLimiteCotidianoSec = $row[10];
		$this->dataLimiteCotidianoOffsec = $row[11];

            
        }

public function delete($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (is_null($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }

            $sql = "DELETE FROM despesa_cotidiano WHERE id= $id AND corporacao_id_INT = $idCorporacao";
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                    && $sincronizar
                        && static::idSistemaTabela() != null
                            && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_REMOVE,
                                null,
                                null,
                                $id,
                                $this->database);
            }

            return $msg;
        }

public function insert($sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null, $opcoes = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            
            
            if (is_null($opcoes))
            {
                $opcoes = $this->opcoesDAO;
            }

            if (!is_numeric($this->id))
            {
                $this->id = "null";
                if ($opcoes == null || $opcoes[Database::OPCAO_GERAR_ID] != false) 
                {
                    $this->id = EXTDAO_Sistema_sequencia::gerarId(static::nomeTabela());                    
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }    
                    else if($msg == null)
                    {
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
                else 
                {
                    $this->id = null;
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao));
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }
                    else if($msg == null)
                    {
                        $this->id = $this->database->getLastInsertId();
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
            }
            else 
            {
                $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }                        
                else if($msg == null)
                {
                   $msg = new Mensagem_token();
                   $msg->mValor = $this->id;
                }
            }
            if($msg == null || $msg->ok())
            {                
                if($sincronizar
                    && static::idSistemaTabela() != null
                    && isset($this->id)){
                    
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_INSERT,
                                null,
                                null,
                                $this->id,
                                $this->database); 
                	}
            } 
            else
            {
                $this->id = null;
            }
            
            return $msg;
            
        }

public function getSQLInsert($idCorporacao){ 

return "INSERT INTO despesa_cotidiano (id, despesa_da_empresa_id_INT, despesa_do_usuario_id_INT, cadastro_usuario_id_INT, id_tipo_despesa_cotidiano_INT, parametro_INT, parametro_OFFSEC, parametro_SEC, parametro_json, valor_FLOAT, data_limite_cotidiano_SEC, data_limite_cotidiano_OFFSEC, corporacao_id_INT) VALUES ( $this->id ,  $this->despesaDaEmpresaId ,  $this->despesaDoUsuarioId ,  $this->cadastroUsuarioId ,  $this->idTipoDespesaCotidianoInt ,  $this->parametroInt ,  $this->parametroOffsec ,  $this->parametroSec ,  $this->parametroJson ,  $this->valorFloat ,  $this->dataLimiteCotidianoSec ,  $this->dataLimiteCotidianoOffsec ,  $idCorporacao ) "; 

}




        
        public function updateByParameters($id, $objParametros, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao))
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();

            
                
                if (isset($objParametros->despesaDaEmpresaId)) 
                {
                    $arrUpdateFields[] = " despesa_da_empresa_id_INT = {$objParametros->despesaDaEmpresaId} ";
                }


                
                if (isset($objParametros->despesaDoUsuarioId)) 
                {
                    $arrUpdateFields[] = " despesa_do_usuario_id_INT = {$objParametros->despesaDoUsuarioId} ";
                }


                
                if (isset($objParametros->cadastroUsuarioId)) 
                {
                    $arrUpdateFields[] = " cadastro_usuario_id_INT = {$objParametros->cadastroUsuarioId} ";
                }


                
                if (isset($objParametros->idTipoDespesaCotidianoInt)) 
                {
                    $arrUpdateFields[] = " id_tipo_despesa_cotidiano_INT = {$objParametros->idTipoDespesaCotidianoInt} ";
                }


                
                if (isset($objParametros->parametroInt)) 
                {
                    $arrUpdateFields[] = " parametro_INT = {$objParametros->parametroInt} ";
                }


                
                if (isset($objParametros->parametroOffsec)) 
                {
                    $arrUpdateFields[] = " parametro_OFFSEC = {$objParametros->parametroOffsec} ";
                }


                
                if (isset($objParametros->parametroSec)) 
                {
                    $arrUpdateFields[] = " parametro_SEC = {$objParametros->parametroSec} ";
                }


                
                if (isset($objParametros->parametroJson)) 
                {
                    $arrUpdateFields[] = " parametro_json = {$objParametros->parametroJson} ";
                }


                
                if (isset($objParametros->valorFloat)) 
                {
                    $arrUpdateFields[] = " valor_FLOAT = {$objParametros->valorFloat} ";
                }


                
                if (isset($objParametros->dataLimiteCotidianoSec)) 
                {
                    $arrUpdateFields[] = " data_limite_cotidiano_SEC = {$objParametros->dataLimiteCotidianoSec} ";
                }


                
                if (isset($objParametros->dataLimiteCotidianoOffsec)) 
                {
                    $arrUpdateFields[] = " data_limite_cotidiano_OFFSEC = {$objParametros->dataLimiteCotidianoOffsec} ";
                }



            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE despesa_cotidiano SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";

            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null)
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function update($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($this->despesaDaEmpresaId)) 
                {                                      
                    $arrUpdateFields[] = " despesa_da_empresa_id_INT = {$this->despesaDaEmpresaId} ";
                }


                
                if (isset($this->despesaDoUsuarioId)) 
                {                                      
                    $arrUpdateFields[] = " despesa_do_usuario_id_INT = {$this->despesaDoUsuarioId} ";
                }


                
                if (isset($this->cadastroUsuarioId)) 
                {                                      
                    $arrUpdateFields[] = " cadastro_usuario_id_INT = {$this->cadastroUsuarioId} ";
                }


                
                if (isset($this->idTipoDespesaCotidianoInt)) 
                {                                      
                    $arrUpdateFields[] = " id_tipo_despesa_cotidiano_INT = {$this->idTipoDespesaCotidianoInt} ";
                }


                
                if (isset($this->parametroInt)) 
                {                                      
                    $arrUpdateFields[] = " parametro_INT = {$this->parametroInt} ";
                }


                
                if (isset($this->parametroOffsec)) 
                {                                      
                    $arrUpdateFields[] = " parametro_OFFSEC = {$this->parametroOffsec} ";
                }


                
                if (isset($this->parametroSec)) 
                {                                      
                    $arrUpdateFields[] = " parametro_SEC = {$this->parametroSec} ";
                }


                
                if (isset($this->parametroJson)) 
                {                                      
                    $arrUpdateFields[] = " parametro_json = {$this->parametroJson} ";
                }


                
                if (isset($this->valorFloat)) 
                {                                      
                    $arrUpdateFields[] = " valor_FLOAT = {$this->valorFloat} ";
                }


                
                if (isset($this->dataLimiteCotidianoSec)) 
                {                                      
                    $arrUpdateFields[] = " data_limite_cotidiano_SEC = {$this->dataLimiteCotidianoSec} ";
                }


                
                if (isset($this->dataLimiteCotidianoOffsec)) 
                {                                      
                    $arrUpdateFields[] = " data_limite_cotidiano_OFFSEC = {$this->dataLimiteCotidianoOffsec} ";
                }


            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE despesa_cotidiano SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function updateByInstanceUsingParameters($id, $parameters, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($parameters->despesaDaEmpresaId)) 
                {                                      
                    $arrUpdateFields[] = " despesa_da_empresa_id_INT = {$this->despesaDaEmpresaId} ";
                }
                
                if (isset($parameters->despesaDoUsuarioId)) 
                {                                      
                    $arrUpdateFields[] = " despesa_do_usuario_id_INT = {$this->despesaDoUsuarioId} ";
                }
                
                if (isset($parameters->cadastroUsuarioId)) 
                {                                      
                    $arrUpdateFields[] = " cadastro_usuario_id_INT = {$this->cadastroUsuarioId} ";
                }
                
                if (isset($parameters->idTipoDespesaCotidianoInt)) 
                {                                      
                    $arrUpdateFields[] = " id_tipo_despesa_cotidiano_INT = {$this->idTipoDespesaCotidianoInt} ";
                }
                
                if (isset($parameters->parametroInt)) 
                {                                      
                    $arrUpdateFields[] = " parametro_INT = {$this->parametroInt} ";
                }
                
                if (isset($parameters->parametroOffsec)) 
                {                                      
                    $arrUpdateFields[] = " parametro_OFFSEC = {$this->parametroOffsec} ";
                }
                
                if (isset($parameters->parametroSec)) 
                {                                      
                    $arrUpdateFields[] = " parametro_SEC = {$this->parametroSec} ";
                }
                
                if (isset($parameters->parametroJson)) 
                {                                      
                    $arrUpdateFields[] = " parametro_json = {$this->parametroJson} ";
                }
                
                if (isset($parameters->valorFloat)) 
                {                                      
                    $arrUpdateFields[] = " valor_FLOAT = {$this->valorFloat} ";
                }
                
                if (isset($parameters->dataLimiteCotidianoSec)) 
                {                                      
                    $arrUpdateFields[] = " data_limite_cotidiano_SEC = {$this->dataLimiteCotidianoSec} ";
                }
                
                if (isset($parameters->dataLimiteCotidianoOffsec)) 
                {                                      
                    $arrUpdateFields[] = " data_limite_cotidiano_OFFSEC = {$this->dataLimiteCotidianoOffsec} ";
                }
            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE despesa_cotidiano SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }

}

?>
