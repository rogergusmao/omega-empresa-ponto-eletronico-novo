"use strict";
exports.__esModule = true;
var Cache_LocalStorage = (function () {
    function Cache_LocalStorage(serializer, unserializer) {
        //this._localStorage = new nodeLocalstorage.LocalStorage('./scratch');
        this._localStorage = localStorage;
        this._serializer = serializer;
        this._unserializer = unserializer;
    }
    Cache_LocalStorage.prototype.add = function (key, data) {
        this._localStorage.setItem(key, this.serialize(data));
    };
    Cache_LocalStorage.prototype.get = function (key) {
        var data = this._localStorage.getItem(key);
        if (data == null) {
            return null;
        }
        return this._unserializer.unserialize(data);
    };
    Cache_LocalStorage.prototype.remove = function (key) {
        this._localStorage.removeItem(key);
    };
    Cache_LocalStorage.prototype.serialize = function (data) {
        return this._serializer.serialize(data);
    };
    return Cache_LocalStorage;
}());
exports.Cache_LocalStorage = Cache_LocalStorage;
