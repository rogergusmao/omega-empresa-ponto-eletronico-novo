/*

 Arquivo gerado atrav�s de gerador de c�digo em 25/09/2017 as 14:49:06.
 Para que o arquivo n�o seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

 Tabela correspondente: pessoa
 Sobrescrita de dados: ---SOBRESCRITA_NAO_AUTORIZADA---

 */
omegaApp.controller('listPessoaController',
    ['$scope', '$rootScope', '$element', '$window', '$interval', '$timeout', 'dataService', 'Upload',
     function ($scope, $rootScope, $element, $window, $interval, $timeout, dataService, Upload)
     {
         var vm = this;
         var generatedTasks = omegaApp.generatedListPessoaController($scope, $rootScope, $element, $window, $interval, $timeout, dataService, Upload, vm);
         try
         {
             //remover chamada caso queira sobrescrever carregamento de dados inicial
             ApplicationUtil.runGeneratedTasks(generatedTasks);

             vm.filterOperators = {
                 id: Constants.FilterOperator.EQUALS,
                 identificador: Constants.FilterOperator.CONTAINS,
                 nome: Constants.FilterOperator.CONTAINS,
                 tipoDocumentoId: Constants.FilterOperator.EQUALS,
                 numeroDocumento: Constants.FilterOperator.CONTAINS,
                 sexoId: Constants.FilterOperator.EQUALS,
                 telefone: Constants.FilterOperator.CONTAINS,
                 celular: Constants.FilterOperator.CONTAINS,
                 email: Constants.FilterOperator.CONTAINS,
                 logradouro: Constants.FilterOperator.CONTAINS,
                 numero: Constants.FilterOperator.CONTAINS,
                 complemento: Constants.FilterOperator.CONTAINS,
                 cidadeId: Constants.FilterOperator.EQUALS
             };

             vm.editRecord = function (primaryKey, record)
             {
                 if (record.temUsuarioAssociado)
                 {
                     var idUsuario = record.usuario_id_INT;
                     document.location = 'index.php?tipo=forms&page=usuario&id=' + idUsuario;

                 }
                 else
                 {
                     var parameters = $.param(primaryKey);
                     document.location = 'index.php?tipo=forms&page=pessoa&' + parameters;
                 }

             };

         }
         catch (ex)
         {
             ApplicationUtil.showErrorMessage(ex.message, 'Erro no controller listPessoaController', vm);
         }
     }]
);