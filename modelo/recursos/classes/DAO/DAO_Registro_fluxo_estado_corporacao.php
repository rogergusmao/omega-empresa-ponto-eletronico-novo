<?php


        /*

        Arquivo gerado através de gerador de código em 12/05/2018 as 14:59:00.
        Para que o arquivo não seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

        Tabela correspondente: registro_fluxo_estado_corporacao
        Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

        */
        


class DAO_Registro_fluxo_estado_corporacao extends Generic_DAO
                        {
                            const NOME_ENTIDADE = "registro_fluxo_estado_corporacao";

                            public static $nomeTabela = null;
                            public static $idSistemaTabela = false;
                            public static $databaseFieldsRelatedAttributes = null;
	                        public static $databaseFieldTypes = null;
	                        public static $databaseFieldNames = null;
	                        public static $listAliasTypes = null;
	                        public static $listAliasRelatedAttributes = null;
	                        
	                        public $id;
public $origemRegistroEstadoCorporacaoId;
public $origemRegistroEstadoId;
public $destinoRegistroEstadoCorporacaoId;
public $destinoRegistroEstadoId;
public $templateJSON;
public $corporacaoId;

public $labelId;
public $labelOrigemRegistroEstadoCorporacaoId;
public $labelOrigemRegistroEstadoId;
public $labelDestinoRegistroEstadoCorporacaoId;
public $labelDestinoRegistroEstadoId;
public $labelTemplateJSON;
public $labelCorporacaoId;

public static $arrPK = array("id");

public function __construct($configDAO = null)
        {
            parent::__construct($configDAO);
            static::__setStaticValues($this->database);           
        }

public static function idSistemaTabela($databaseObject = null){
            if(!(static::$idSistemaTabela))
            {
                static::$idSistemaTabela = EXTDAO_Sistema_tabela::getIdDaSistemaTabela(static::nomeTabela(), $databaseObject);
            }
            return static::$idSistemaTabela;
        }

public function getIdSistemaTabela(){
            return static::idSistemaTabela();
        }

public static function nomeTabela(){
            if(is_null(static::$nomeTabela))
            {
                static::$nomeTabela = "registro_fluxo_estado_corporacao";
            }
            return static::$nomeTabela;
        }

public function getNomeTabela(){
            return static::nomeTabela();
        }

public static function __setStaticValues($databaseObject = null)
                   {    
                        static::nomeTabela();
                        
                        static::idSistemaTabela($databaseObject);
                   
                        static::setDatabaseFieldNames();
                        static::setDatabaseFieldTypes();
                        static::setDatabaseFieldsRelatedAttributeNames();
                        
                        static::setListAliasTypes();
                        static::setListAliasRelatedAttributes();
                   }

public function getExpressaoLabel()
                   {
                        return "{id}";
                   }

public static function setDatabaseFieldNames()
        {
            if(is_null(static::$databaseFieldNames))
            {
                static::$databaseFieldNames = new stdClass();
                static::$databaseFieldNames->id = "id";
static::$databaseFieldNames->origemRegistroEstadoCorporacaoId = "origem_registro_estado_corporacao_id_INT";
static::$databaseFieldNames->origemRegistroEstadoId = "origem_registro_estado_id_INT";
static::$databaseFieldNames->destinoRegistroEstadoCorporacaoId = "destino_registro_estado_corporacao_id_INT";
static::$databaseFieldNames->destinoRegistroEstadoId = "destino_registro_estado_id_INT";
static::$databaseFieldNames->templateJSON = "template_JSON";
static::$databaseFieldNames->corporacaoId = "corporacao_id_INT";

            }
                      
        }

public static function setDatabaseFieldTypes()
        {
            if(is_null(static::$databaseFieldTypes))
            {
                static::$databaseFieldTypes = new stdClass();
                static::$databaseFieldTypes->id = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->origemRegistroEstadoCorporacaoId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->origemRegistroEstadoId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->destinoRegistroEstadoCorporacaoId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->destinoRegistroEstadoId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->templateJSON = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->corporacaoId = static::TIPO_VARIAVEL_INTEGER;

            }
            
        }

public static function setDatabaseFieldsRelatedAttributeNames()
        {
            if(is_null(static::$databaseFieldsRelatedAttributes))
            {
                static::$databaseFieldsRelatedAttributes = new stdClass();
                static::$databaseFieldsRelatedAttributes->id = "id";
static::$databaseFieldsRelatedAttributes->origem_registro_estado_corporacao_id_INT = "origemRegistroEstadoCorporacaoId";
static::$databaseFieldsRelatedAttributes->origem_registro_estado_id_INT = "origemRegistroEstadoId";
static::$databaseFieldsRelatedAttributes->destino_registro_estado_corporacao_id_INT = "destinoRegistroEstadoCorporacaoId";
static::$databaseFieldsRelatedAttributes->destino_registro_estado_id_INT = "destinoRegistroEstadoId";
static::$databaseFieldsRelatedAttributes->template_JSON = "templateJSON";
static::$databaseFieldsRelatedAttributes->corporacao_id_INT = "corporacaoId";

            }           
        }

public function setAllFieldsToHumanFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["origem_registro_estado_corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["origem_registro_estado_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["destino_registro_estado_corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["destino_registro_estado_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["template_JSON"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
}

public function setAllFieldsToDatabaseFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["origem_registro_estado_corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["origem_registro_estado_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["destino_registro_estado_corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["destino_registro_estado_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["template_JSON"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
}

  public function getFkObjOrigem_registro_estado_corporacao() 
                {
                    if($this->objOrigem_registro_estado_corporacao == null)
                    {                        
                        $this->objOrigem_registro_estado_corporacao = new EXTDAO_Origem_registro_estado_corporacao_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getOrigem_registro_estado_corporacao_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objOrigem_registro_estado_corporacao->clear();
                    }
                    elseif($this->objOrigem_registro_estado_corporacao->getId() != $idFK)
                    {
                        $this->objOrigem_registro_estado_corporacao->select($idFK);
                    }
                    return $this->objOrigem_registro_estado_corporacao;
                }
  public function getFkObjOrigem_registro_estado() 
                {
                    if($this->objOrigem_registro_estado == null)
                    {                        
                        $this->objOrigem_registro_estado = new EXTDAO_Origem_registro_estado_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getOrigem_registro_estado_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objOrigem_registro_estado->clear();
                    }
                    elseif($this->objOrigem_registro_estado->getId() != $idFK)
                    {
                        $this->objOrigem_registro_estado->select($idFK);
                    }
                    return $this->objOrigem_registro_estado;
                }
  public function getFkObjDestino_registro_estado_corporacao() 
                {
                    if($this->objDestino_registro_estado_corporacao == null)
                    {                        
                        $this->objDestino_registro_estado_corporacao = new EXTDAO_Destino_registro_estado_corporacao_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getDestino_registro_estado_corporacao_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objDestino_registro_estado_corporacao->clear();
                    }
                    elseif($this->objDestino_registro_estado_corporacao->getId() != $idFK)
                    {
                        $this->objDestino_registro_estado_corporacao->select($idFK);
                    }
                    return $this->objDestino_registro_estado_corporacao;
                }
  public function getFkObjDestino_registro_estado() 
                {
                    if($this->objDestino_registro_estado == null)
                    {                        
                        $this->objDestino_registro_estado = new EXTDAO_Destino_registro_estado_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getDestino_registro_estado_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objDestino_registro_estado->clear();
                    }
                    elseif($this->objDestino_registro_estado->getId() != $idFK)
                    {
                        $this->objDestino_registro_estado->select($idFK);
                    }
                    return $this->objDestino_registro_estado;
                }
  public function getFkObjCorporacao() 
                {
                    if($this->objCorporacao == null)
                    {                        
                        $this->objCorporacao = new EXTDAO_Corporacao_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getCorporacao_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objCorporacao->clear();
                    }
                    elseif($this->objCorporacao->getId() != $idFK)
                    {
                        $this->objCorporacao->select($idFK);
                    }
                    return $this->objCorporacao;
                }


public function setLabels()
        {$this->labelId = "";
$this->labelOrigemRegistroEstadoCorporacaoId = "";
$this->labelOrigemRegistroEstadoId = "";
$this->labelDestinoRegistroEstadoCorporacaoId = "";
$this->labelDestinoRegistroEstadoId = "";
$this->labelTemplateJSON = "";
$this->labelCorporacaoId = "";
}

public function __actionAdd($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                $this->setByObject($parameters);
                $this->setDataCadastroAndOffsetInSecondsFromUser($parameters);
                
                $this->formatarParaSQL();
                $msg = $this->insert(true);
                
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                return new Mensagem(null, I18N::getExpression("Registro fluxo estado corporacao adicionado com sucesso."));
                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionEdit($parameters = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try {

                $this->setByObject($parameters);
                $this->setAllFieldsToHumanFormat();
                
                $this->formatarParaSQL();
                $msg = $this->updateByInstanceUsingParameters($this->id, $parameters, true);
                if($msg != null && $msg->erro())
                {
                     return $msg;
                }
        
                return new Mensagem(null, I18N::getExpression("Registro fluxo estado corporacao editado com sucesso."));
            }
            catch(Exception $ex)
            {                
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionRemove($parameters = null)
        {
            $successulRemovalCount = 0;
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                foreach($parameters as $record)
                {
                    if(is_numeric($record->id))
                    {
                        $msg = $this->delete($record->id, true);
                        if($msg == null || $msg->ok())
                        {
                            $successulRemovalCount++;
                        }
                    }
                }
                
                if($successulRemovalCount > 0)
                {
                    $successMessage = count($successulRemovalCount) > 1 ? I18N::getExpression("{0} Registro fluxo estado corporacao foram removidos com sucesso.", $successulRemovalCount) : I18N::getExpression("Registro fluxo estado corporacao removido com sucesso.");
                    return new Mensagem(null, $successMessage);
                }
                else
                {
                    $errorMessage = count($parameters) > 1 ? I18N::getExpression("Falha ao remover Registro fluxo estado corporacao.") : I18N::getExpression("Falha ao remover Registro fluxo estado corporacao.");
                    return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, $errorMessage);                    
                }
            }
            catch (Exception $ex) 
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }

public function __getRecord($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }
            
            try
            {            
                $returnObject = new stdClass();            
                $whereClause = static::getWhereClauseForFilter($parameters->filterParameters);
                $query = "SELECT  id, origem_registro_estado_corporacao_id_INT, origem_registro_estado_id_INT, destino_registro_estado_corporacao_id_INT, destino_registro_estado_id_INT, template_JSON, corporacao_id_INT FROM registro_fluxo_estado_corporacao {$whereClause}";
    
                $msg = $this->database->queryMensagem($query);                
                if ($msg == null)
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Não foi encontrado nenhum registro"));
                    }                    
                    $resultSet = $this->database->result;
                    $resultObject = static::mapResultSetDataToRecord($resultSet);
                    
                    $returnObject->formData = $resultObject;
                    $returnObject->comboBoxesData = null;
                    if($parameters->loadComboBoxesData)
                    {
                        $this->setByObject($resultObject);
                        $returnObject->comboBoxesData =  $this->getFormComboBoxesData();
                    }                    
    
                    
    
                    return new Mensagem_generica($returnObject);
                } 
                else 
                {
                    return $msg;
                }
                
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }


        
        public function __getFilterComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
                
        public function __getFormComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
        
        protected function getFormComboBoxesData()
        {
            $comboBoxesData = new stdClass();
            $listParameters = new stdClass();
            $listParameters->listMappingType = static::LIST_MAPPING_TYPE_COMBOBOX;
            
            

            return $comboBoxesData;
        }
        
        


        
        public static function setListAliasTypes()
        {
            if(is_null(static::$listAliasTypes))
            {
                static::$listAliasTypes = new stdClass();
                static::$listAliasTypes->registro_fluxo_estado_corporacao__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->registro_fluxo_estado_corporacao__origem_registro_estado_corporacao_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->registro_fluxo_estado_corporacao__origem_registro_estado_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->registro_fluxo_estado_corporacao__destino_registro_estado_corporacao_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->registro_fluxo_estado_corporacao__destino_registro_estado_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->registro_fluxo_estado_corporacao__template_JSON = static::TIPO_VARIAVEL_TEXT;
            }        
        }
        
        public static function setListAliasRelatedAttributes()
        {
            if(is_null(static::$listAliasRelatedAttributes))
            {
                static::$listAliasRelatedAttributes = new stdClass();
                static::$listAliasRelatedAttributes->registro_fluxo_estado_corporacao__id = "id";
static::$listAliasRelatedAttributes->registro_fluxo_estado_corporacao__origem_registro_estado_corporacao_id_INT = "origemRegistroEstadoCorporacaoId";
static::$listAliasRelatedAttributes->registro_fluxo_estado_corporacao__origem_registro_estado_id_INT = "origemRegistroEstadoId";
static::$listAliasRelatedAttributes->registro_fluxo_estado_corporacao__destino_registro_estado_corporacao_id_INT = "destinoRegistroEstadoCorporacaoId";
static::$listAliasRelatedAttributes->registro_fluxo_estado_corporacao__destino_registro_estado_id_INT = "destinoRegistroEstadoId";
static::$listAliasRelatedAttributes->registro_fluxo_estado_corporacao__template_JSON = "templateJSON";
            }         
        }

        public function __getList($parameters = null, $idCorporacao = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try
            {
                $filterParameters = new stdClass();
                if(isset($parameters->filterParameters))
                {
                    $filterParameters = $parameters->filterParameters;
                }
                                    
                            
            if(is_null($idCorporacao))
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            ListParameters::addFilterParameterToObject($filterParameters, "corporacaoId", $idCorporacao);           
                
                
                $sortingParameters = null;
                if(isset($parameters->sortingParameters))
                {
                    $sortingParameters = $parameters->sortingParameters;
                }
                
                $paginationParameters = null;
                if(isset($parameters->paginationParameters))
                {
                    $paginationParameters = $parameters->paginationParameters;
                }
                
                $listMappingType = static::getListMappingType($parameters->listMappingType);
                $mainTableAlias = "rfec";

                $whereClause = static::getWhereClauseForFilter($filterParameters, $mainTableAlias);
                $orderByClause = static::getOrderByClauseForFilter($sortingParameters, $mainTableAlias);
                $limitClause = static::getLimitClauseForFilter($paginationParameters);

                $queryWithoutLimit = "SELECT rfec.id FROM registro_fluxo_estado_corporacao rfec {$whereClause}";
                $query = "SELECT rfec.id AS registro_fluxo_estado_corporacao__id, rfec.origem_registro_estado_corporacao_id_INT AS registro_fluxo_estado_corporacao__origem_registro_estado_corporacao_id_INT, rfec.origem_registro_estado_id_INT AS registro_fluxo_estado_corporacao__origem_registro_estado_id_INT, rfec.destino_registro_estado_corporacao_id_INT AS registro_fluxo_estado_corporacao__destino_registro_estado_corporacao_id_INT, rfec.destino_registro_estado_id_INT AS registro_fluxo_estado_corporacao__destino_registro_estado_id_INT, rfec.template_JSON AS registro_fluxo_estado_corporacao__template_JSON FROM registro_fluxo_estado_corporacao rfec  {$whereClause} {$orderByClause} {$limitClause}";

                $msg = $this->database->queryMensagem($queryWithoutLimit);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                
                $totalNumberOfRecords = $this->database->rows();
                if($totalNumberOfRecords == 0)
                {
                    return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                }                        
                        
                $msg = $this->database->queryMensagem($query);
                
                //operacao realizaca com sucesso
                if ($msg == null) 
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                    }                        
                
                    $resultSet = $this->database->result;

                    $objReturn = new stdClass();
                    $objReturn->dataSet = static::mapResultSetDataToList($resultSet, $listMappingType, static::$listAliasTypes);

                    if(!Helper::isNullOrEmpty($paginationParameters->recordsPerPage) && !Helper::isNullOrEmpty($paginationParameters->currentPage))
                    {
                        $lastPage = ceil($totalNumberOfRecords / $paginationParameters->recordsPerPage);
                        $objReturn->paginationParameters = new PaginationParams($paginationParameters->recordsPerPage, $paginationParameters->currentPage, $lastPage);
                    }
                    
                    return new Mensagem_generica($objReturn);
                } 
                else
                {
                    return $msg;
                }                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }



public function getId()
            {
                return $this->id;
            }

public function getOrigemRegistroEstadoCorporacaoId()
            {
                return $this->origemRegistroEstadoCorporacaoId;
            }

public function getOrigem_registro_estado_corporacao_id_INT()
                {
                    return $this->origemRegistroEstadoCorporacaoId;
                }

public function getOrigemRegistroEstadoId()
            {
                return $this->origemRegistroEstadoId;
            }

public function getOrigem_registro_estado_id_INT()
                {
                    return $this->origemRegistroEstadoId;
                }

public function getDestinoRegistroEstadoCorporacaoId()
            {
                return $this->destinoRegistroEstadoCorporacaoId;
            }

public function getDestino_registro_estado_corporacao_id_INT()
                {
                    return $this->destinoRegistroEstadoCorporacaoId;
                }

public function getDestinoRegistroEstadoId()
            {
                return $this->destinoRegistroEstadoId;
            }

public function getDestino_registro_estado_id_INT()
                {
                    return $this->destinoRegistroEstadoId;
                }

public function getTemplateJSON()
            {
                return $this->templateJSON;
            }

public function getTemplate_JSON()
                {
                    return $this->templateJSON;
                }

public function getCorporacaoId()
            {
                return $this->corporacaoId;
            }

public function getCorporacao_id_INT()
                {
                    return $this->corporacaoId;
                }



function setId($value)
            {
                $this->id = $value;
            }

function setOrigemRegistroEstadoCorporacaoId($value)
            {
                $this->origemRegistroEstadoCorporacaoId = $value;
            }

public function setOrigem_registro_estado_corporacao_id_INT($value)
                { 
                    $this->origemRegistroEstadoCorporacaoId = $value; 
                }

function setOrigemRegistroEstadoId($value)
            {
                $this->origemRegistroEstadoId = $value;
            }

public function setOrigem_registro_estado_id_INT($value)
                { 
                    $this->origemRegistroEstadoId = $value; 
                }

function setDestinoRegistroEstadoCorporacaoId($value)
            {
                $this->destinoRegistroEstadoCorporacaoId = $value;
            }

public function setDestino_registro_estado_corporacao_id_INT($value)
                { 
                    $this->destinoRegistroEstadoCorporacaoId = $value; 
                }

function setDestinoRegistroEstadoId($value)
            {
                $this->destinoRegistroEstadoId = $value;
            }

public function setDestino_registro_estado_id_INT($value)
                { 
                    $this->destinoRegistroEstadoId = $value; 
                }

function setTemplateJSON($value)
            {
                $this->templateJSON = $value;
            }

public function setTemplate_JSON($value)
                { 
                    $this->templateJSON = $value; 
                }

function setCorporacaoId($value)
            {
                $this->corporacaoId = $value;
            }

public function setCorporacao_id_INT($value)
                { 
                    $this->corporacaoId = $value; 
                }



public function clear()
        {$this->id = null;
$this->origemRegistroEstadoCorporacaoId = null;
if($this->objOrigem_registro_estado_corporacao != null) unset($this->objOrigem_registro_estado_corporacao);
$this->origemRegistroEstadoId = null;
if($this->objOrigem_registro_estado != null) unset($this->objOrigem_registro_estado);
$this->destinoRegistroEstadoCorporacaoId = null;
if($this->objDestino_registro_estado_corporacao != null) unset($this->objDestino_registro_estado_corporacao);
$this->destinoRegistroEstadoId = null;
if($this->objDestino_registro_estado != null) unset($this->objDestino_registro_estado);
$this->templateJSON = null;
$this->corporacaoId = null;
if($this->objCorporacao != null) unset($this->objCorporacao);
if(is_array($this->arrEstadosCampos)){
                        unset($this->arrEstadosCampos);
                     $this->arrEstadosCampos = array();
                    }
                }

public function formatarParaSQL()
        {
if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->id)){
$this->id = $this->formatarIntegerParaComandoSQL($this->id);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->id);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->origemRegistroEstadoCorporacaoId)){
$this->origemRegistroEstadoCorporacaoId = $this->formatarIntegerParaComandoSQL($this->origemRegistroEstadoCorporacaoId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->origemRegistroEstadoCorporacaoId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->origemRegistroEstadoId)){
$this->origemRegistroEstadoId = $this->formatarIntegerParaComandoSQL($this->origemRegistroEstadoId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->origemRegistroEstadoId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->destinoRegistroEstadoCorporacaoId)){
$this->destinoRegistroEstadoCorporacaoId = $this->formatarIntegerParaComandoSQL($this->destinoRegistroEstadoCorporacaoId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->destinoRegistroEstadoCorporacaoId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->destinoRegistroEstadoId)){
$this->destinoRegistroEstadoId = $this->formatarIntegerParaComandoSQL($this->destinoRegistroEstadoId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->destinoRegistroEstadoId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->templateJSON)){
$this->templateJSON = $this->formatarStringParaComandoSQL($this->templateJSON);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->templateJSON);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->corporacaoId)){
$this->corporacaoId = $this->formatarIntegerParaComandoSQL($this->corporacaoId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->corporacaoId);
}

}

public function formatarParaExibicao()
        {if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->templateJSON)){
$this->templateJSON = $this->formatarStringParaExibicao($this->templateJSON);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->templateJSON);
}

}

function select($id, $idCorporacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }   

            $sql = "SELECT id, origem_registro_estado_corporacao_id_INT, origem_registro_estado_id_INT, destino_registro_estado_corporacao_id_INT, destino_registro_estado_id_INT, template_JSON, corporacao_id_INT FROM registro_fluxo_estado_corporacao WHERE id = $id AND corporacao_id_INT = $idCorporacao";

            $msg = $this->database->queryMensagem($sql);
            $result = $this->database->result;

            if($msg != null && $msg->erro() )
            { 
            		$this->clear();
            		return $msg;
        	} 
        	else if($this->database->rows() == 0)
        	{
                return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nenhum registro foi encontrado."));
            }
        

            $row = $this->database->fetchArray(MYSQLI_NUM);
            
            		$this->id = $row[0];
		$this->origemRegistroEstadoCorporacaoId = $row[1];
		$this->origemRegistroEstadoId = $row[2];
		$this->destinoRegistroEstadoCorporacaoId = $row[3];
		$this->destinoRegistroEstadoId = $row[4];
		$this->templateJSON = $row[5];

            
        }

public function delete($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (is_null($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }

            $sql = "DELETE FROM registro_fluxo_estado_corporacao WHERE id= $id AND corporacao_id_INT = $idCorporacao";
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                    && $sincronizar
                        && static::idSistemaTabela() != null
                            && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_REMOVE,
                                null,
                                null,
                                $id,
                                $this->database);
            }

            return $msg;
        }

public function insert($sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null, $opcoes = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            
            
            if (is_null($opcoes))
            {
                $opcoes = $this->opcoesDAO;
            }

            if (!is_numeric($this->id))
            {
                $this->id = "null";
                if ($opcoes == null || $opcoes[Database::OPCAO_GERAR_ID] != false) 
                {
                    $this->id = EXTDAO_Sistema_sequencia::gerarId(static::nomeTabela());                    
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }    
                    else if($msg == null)
                    {
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
                else 
                {
                    $this->id = null;
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao));
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }
                    else if($msg == null)
                    {
                        $this->id = $this->database->getLastInsertId();
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
            }
            else 
            {
                $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }                        
                else if($msg == null)
                {
                   $msg = new Mensagem_token();
                   $msg->mValor = $this->id;
                }
            }
            if($msg == null || $msg->ok())
            {                
                if($sincronizar
                    && static::idSistemaTabela() != null
                    && isset($this->id)){
                    
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_INSERT,
                                null,
                                null,
                                $this->id,
                                $this->database); 
                	}
            } 
            else
            {
                $this->id = null;
            }
            
            return $msg;
            
        }

public function getSQLInsert($idCorporacao){ 

return "INSERT INTO registro_fluxo_estado_corporacao (id, origem_registro_estado_corporacao_id_INT, origem_registro_estado_id_INT, destino_registro_estado_corporacao_id_INT, destino_registro_estado_id_INT, template_JSON, corporacao_id_INT) VALUES ( $this->id ,  $this->origemRegistroEstadoCorporacaoId ,  $this->origemRegistroEstadoId ,  $this->destinoRegistroEstadoCorporacaoId ,  $this->destinoRegistroEstadoId ,  $this->templateJSON ,  $idCorporacao ) "; 

}




        
        public function updateByParameters($id, $objParametros, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao))
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();

            
                
                if (isset($objParametros->origemRegistroEstadoCorporacaoId)) 
                {
                    $arrUpdateFields[] = " origem_registro_estado_corporacao_id_INT = {$objParametros->origemRegistroEstadoCorporacaoId} ";
                }


                
                if (isset($objParametros->origemRegistroEstadoId)) 
                {
                    $arrUpdateFields[] = " origem_registro_estado_id_INT = {$objParametros->origemRegistroEstadoId} ";
                }


                
                if (isset($objParametros->destinoRegistroEstadoCorporacaoId)) 
                {
                    $arrUpdateFields[] = " destino_registro_estado_corporacao_id_INT = {$objParametros->destinoRegistroEstadoCorporacaoId} ";
                }


                
                if (isset($objParametros->destinoRegistroEstadoId)) 
                {
                    $arrUpdateFields[] = " destino_registro_estado_id_INT = {$objParametros->destinoRegistroEstadoId} ";
                }


                
                if (isset($objParametros->templateJSON)) 
                {
                    $arrUpdateFields[] = " template_JSON = {$objParametros->templateJSON} ";
                }



            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE registro_fluxo_estado_corporacao SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";

            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null)
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function update($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($this->origemRegistroEstadoCorporacaoId)) 
                {                                      
                    $arrUpdateFields[] = " origem_registro_estado_corporacao_id_INT = {$this->origemRegistroEstadoCorporacaoId} ";
                }


                
                if (isset($this->origemRegistroEstadoId)) 
                {                                      
                    $arrUpdateFields[] = " origem_registro_estado_id_INT = {$this->origemRegistroEstadoId} ";
                }


                
                if (isset($this->destinoRegistroEstadoCorporacaoId)) 
                {                                      
                    $arrUpdateFields[] = " destino_registro_estado_corporacao_id_INT = {$this->destinoRegistroEstadoCorporacaoId} ";
                }


                
                if (isset($this->destinoRegistroEstadoId)) 
                {                                      
                    $arrUpdateFields[] = " destino_registro_estado_id_INT = {$this->destinoRegistroEstadoId} ";
                }


                
                if (isset($this->templateJSON)) 
                {                                      
                    $arrUpdateFields[] = " template_JSON = {$this->templateJSON} ";
                }


            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE registro_fluxo_estado_corporacao SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function updateByInstanceUsingParameters($id, $parameters, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($parameters->origemRegistroEstadoCorporacaoId)) 
                {                                      
                    $arrUpdateFields[] = " origem_registro_estado_corporacao_id_INT = {$this->origemRegistroEstadoCorporacaoId} ";
                }
                
                if (isset($parameters->origemRegistroEstadoId)) 
                {                                      
                    $arrUpdateFields[] = " origem_registro_estado_id_INT = {$this->origemRegistroEstadoId} ";
                }
                
                if (isset($parameters->destinoRegistroEstadoCorporacaoId)) 
                {                                      
                    $arrUpdateFields[] = " destino_registro_estado_corporacao_id_INT = {$this->destinoRegistroEstadoCorporacaoId} ";
                }
                
                if (isset($parameters->destinoRegistroEstadoId)) 
                {                                      
                    $arrUpdateFields[] = " destino_registro_estado_id_INT = {$this->destinoRegistroEstadoId} ";
                }
                
                if (isset($parameters->templateJSON)) 
                {                                      
                    $arrUpdateFields[] = " template_JSON = {$this->templateJSON} ";
                }
            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE registro_fluxo_estado_corporacao SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }

}

?>
