/*

 Arquivo gerado atrav�s de gerador de c�digo em 07/01/2018 as 15:54:09.
 Para que o arquivo n�o seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

 Tabela correspondente: cidade
 Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

 */

returnObject.getCidade = function (filterParameters)
{

    var parameters = {
        filterParameters: filterParameters,
        loadComboBoxesData: true
    };

    return $http({
        url: baseURLAdm + "/webservice.php?class=EXTDAO_Cidade&action=__getRecord",
        method: "POST",
        data: parameters,
        headers: {
            'Content-Type': 'application/json'
        }
    });

};

returnObject.addCidade = function (formData)
{

    return $http({
        url: baseURLAdm + "/webservice.php?class=EXTDAO_Cidade&jsonOutput=true&action=add",
        method: "POST",
        data: formData,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    });

};

returnObject.editCidade = function (formData)
{

    return $http({
        url: baseURLAdm + "/webservice.php?class=EXTDAO_Cidade&jsonOutput=true&action=edit",
        method: "POST",
        data: formData,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    });

};

returnObject.getAllComboBoxesDataForCidadeForm = function ()
{
    return $http({
        url: baseURLAdm + "/webservice.php?class=EXTDAO_Cidade&jsonOutput=true&action=__getFormComboBoxesData",
        method: "POST",
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    });

};

returnObject.getListOfUfForCidadeForm = function ()
{
    var params = {filterParameters: null, listMappingType: Constants.ListMappingType.COMBOBOX, hideLoading: true};
    return $http.post(baseURLAdm + "/webservice.php?class=EXTDAO_Uf&action=__getList", params);
};
                

