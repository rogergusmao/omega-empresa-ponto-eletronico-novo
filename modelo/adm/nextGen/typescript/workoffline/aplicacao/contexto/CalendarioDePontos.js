"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t;
    return { next: verb(0), "throw": verb(1), "return": verb(2) };
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
var React = require("react");
var Moment = require("moment");
var contexto_1 = require("lib/contexto");
var dataService_1 = require("aplicacao/comum/dataService");
var geral_1 = require("lib/geral");
var geral_2 = require("aplicacao/comum/geral");
var uiComponente_1 = require("aplicacao/modulo/calendario/uiComponente");
var model_1 = require("aplicacao/modulo/calendario/model");
var CalendarioDePontos = (function (_super) {
    __extends(CalendarioDePontos, _super);
    function CalendarioDePontos(domRootSelector) {
        var _this = 
        //chama o construtor do pai
        _super.call(this, domRootSelector) || this;
        //instancia o data service
        _this.pontoEletronicoDataService = new dataService_1.PontoEletronicoServices();
        //inicializa dicionário de cores
        _this.dadosPessoas = new Map();
        //corrige o layout da página para aplicar o calendário
        _this.fixPageLayout();
        //renderiza componentes
        _this.renderComponents();
        //define lingua padrão do MomentJS
        Moment.locale('pt-br');
        return _this;
    }
    CalendarioDePontos.contextFactory = function () {
        $(document).ready(function () {
            var aplicacaoContext = new CalendarioDePontos('section#content');
            CalendarioDePontos.appContext = aplicacaoContext;
        });
    };
    CalendarioDePontos.prototype.renderComponents = function () {
        //renderiza o calendário e seta a instância
        this.renderCalendar();
        //renderiza o totalizador de horas
        this.renderTotalizadorDeHoras();
        //renderiza botão de exportar dados
        this.renderBotaoExportarDados();
        //renderiza o gráfico de distribuição de horas
        this.renderDistribuicaoDeHoras();
        //renderiza o painel de problemas detectados
        this.renderPainelProblemasEncontrados();
    };
    CalendarioDePontos.prototype.renderBotaoExportarDados = function () {
        var botaoExportarDadosJSX = <a className="btn btn-xs btn-link" onClick={this.onButtonExportarDadosClick.bind(this)}>Exportar Dados</a>;
        this.renderReactComponent(botaoExportarDadosJSX, '#botao-exportar-dados-container');
    };
    CalendarioDePontos.prototype.onButtonExportarDadosClick = function () {
        var janelaModalJSX = <uiComponente_1.ExportarDados dataInicial={this.dataInicialVisualizacao} dataFinal={this.dataFinalVisualizacao} onExportarDadosClick={this.onConfirmarExportacaoDadosClick.bind(this)}/>;
        var janelaModal = this.renderReactComponent(janelaModalJSX, '#modal-container');
        janelaModal.show();
        janelaModal.atualizarDatasPadrao();
    };
    CalendarioDePontos.prototype.onSalvarDadosCorrigirPontoClick = function (pontoEletronico) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.pontoEletronicoDataService.gravarPontoEletronicoParaCorrecao(pontoEletronico)];
                    case 1:
                        _a.sent();
                        //chama webservice para recarregar os dados atualizados
                        if (this.dataInicialVisualizacao && this.dataFinalVisualizacao) {
                            this.loadDataFromService(this.dataInicialVisualizacao, this.dataFinalVisualizacao);
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    CalendarioDePontos.prototype.onConfirmarExportacaoDadosClick = function (dataInicial, dataFinal) {
        var workbook = new geral_1.Workbook("Pontos Eletronicos.xlsx");
        var sheet1 = workbook.createNewSheet("Pontos_Eletronicos");
        sheet1.appendCabecalho(["Teste 1", "Teste 2", "Teste 3", "Teste 4"], "Workoffline.com.br - Pontos Eletronicos");
        workbook.exportar();
    };
    CalendarioDePontos.prototype.renderCalendar = function () {
        var calendarioJSX = <uiComponente_1.CalendarioDePontos onEventClick={this.onCalendarEventClick.bind(this)} onCalendarViewChange={this.onCalendarViewChange.bind(this)} setDefaultOptions/>;
        this.calendario = this.renderReactComponent(calendarioJSX, '#calendario-container');
        this.calendario.resizeCalendar();
    };
    CalendarioDePontos.prototype.renderTotalizadorDeHoras = function () {
        var totalizadorJSX = <uiComponente_1.TotalizadorDeHoras />;
        this.totalizador = this.renderReactComponent(totalizadorJSX, '#painel-totalizador-horas-container');
    };
    CalendarioDePontos.prototype.renderDistribuicaoDeHoras = function () {
        var totalizadorJSX = <uiComponente_1.DistribuicaoDeHoras largura={$('#painel-distribuicao-horas-container').width()}/>;
        this.graficoDistribuicao = this.renderReactComponent(totalizadorJSX, '#painel-distribuicao-horas-container');
    };
    CalendarioDePontos.prototype.renderPainelProblemasEncontrados = function () {
        var painelProblemasEncontradosJSX = <uiComponente_1.ProblemasEncontrados onProblemaClick={this.onPainelProblemasEncontradosItemClick.bind(this)}/>;
        this.painelProblemasEncontrados = this.renderReactComponent(painelProblemasEncontradosJSX, '#painel-problemas-encontrador-container');
    };
    CalendarioDePontos.prototype.onCalendarViewChange = function (view, element) {
        this.dataInicialVisualizacao = view.intervalStart;
        this.dataFinalVisualizacao = view.intervalEnd;
        this.loadDataFromService(view.intervalStart, view.intervalEnd);
    };
    CalendarioDePontos.prototype.onCalendarEventClick = function (event, jsEvent, view) {
        var idPontoInicial = event.idPontoInicial;
        var idPessoa = event.idPessoa;
        this.showModalCorrigirPonto(idPessoa, idPontoInicial);
    };
    CalendarioDePontos.prototype.onPainelProblemasEncontradosItemClick = function (evento) {
        var idPontoInicial = evento.pontoInicial.idPonto;
        var idPessoa = evento.idPessoa;
        this.showModalCorrigirPonto(idPessoa, idPontoInicial);
    };
    CalendarioDePontos.prototype.showModalCorrigirPonto = function (idPessoa, idPontoInicial) {
        var pessoa = this.dadosPessoas.get(idPessoa);
        var eventoCorrigir = null;
        for (var _i = 0, _a = pessoa.eventos; _i < _a.length; _i++) {
            var evento = _a[_i];
            if (evento.pontoInicial.idPonto == idPontoInicial) {
                eventoCorrigir = evento;
                break;
            }
        }
        if (eventoCorrigir != null) {
            var dataPontoAnterior = null;
            var dataPontoPosterior = null;
            //percorre todos os pontos da pessoa para encontrar
            //o ponto imediatamente antes e o ponto imediatamente depois do ponto a corrigir
            //a fim de limitar as datas válidas no controle de data na modal a ser aberta        
            for (var _b = 0, _c = pessoa.eventos; _b < _c.length; _b++) {
                var evento = _c[_b];
                //caso o ponto corrente não seja o ponto a ser corrigido
                if (evento.pontoInicial.idPonto != eventoCorrigir.pontoInicial.idPonto) {
                    if (evento.pontoFinal != null
                        && evento.pontoFinal.dataPonto.isBefore(eventoCorrigir.pontoInicial.dataPonto)) {
                        if (dataPontoAnterior == null || (dataPontoAnterior.isBefore(evento.pontoFinal.dataPonto))) {
                            dataPontoAnterior = evento.pontoFinal.dataPonto;
                        }
                    }
                    else if (evento.pontoFinal == null
                        && evento.pontoInicial.dataPonto.isBefore(eventoCorrigir.pontoInicial.dataPonto)) {
                        if (dataPontoAnterior == null || (dataPontoAnterior.isBefore(evento.pontoInicial.dataPonto))) {
                            dataPontoAnterior = evento.pontoInicial.dataPonto;
                        }
                    }
                    if (evento.pontoInicial.dataPonto.isAfter(eventoCorrigir.pontoInicial.dataPonto)) {
                        if (dataPontoPosterior == null || (dataPontoPosterior.isAfter(evento.pontoInicial.dataPonto))) {
                            dataPontoPosterior = evento.pontoInicial.dataPonto;
                        }
                    }
                }
            }
            var dataMinimaPermitida = dataPontoAnterior ? dataPontoAnterior.clone() : null;
            var dataMaximaPermitida = dataPontoPosterior ? dataPontoPosterior.clone() : null;
            if (eventoCorrigir.pontoInicial.tipoPonto == geral_2.TipoPontoEletronico.PONTO_ENTRADA) {
                dataMinimaPermitida = eventoCorrigir.pontoInicial.dataPonto.clone();
            }
            else if (eventoCorrigir.pontoInicial.tipoPonto == geral_2.TipoPontoEletronico.PONTO_SAIDA) {
                dataMaximaPermitida = eventoCorrigir.pontoInicial.dataPonto.clone();
            }
            var janelaModalJSX = <uiComponente_1.CorrigirPonto id={"modal-corrigir-ponto-" + eventoCorrigir.pontoInicial.idPonto} evento={eventoCorrigir} dataMinimaPermitida={dataMinimaPermitida} dataMaximaPermitida={dataMaximaPermitida} onSalvarDadosClick={this.onSalvarDadosCorrigirPontoClick.bind(this)}/>;
            var janelaModal = this.renderReactComponent(janelaModalJSX, '#modal-container');
            janelaModal.show();
        }
    };
    CalendarioDePontos.prototype.fixPageLayout = function () {
        //corrige o layout no contexto específico da aplicação
        $('#wrapper').css('min-height', 'auto');
        $('#content > .container').css('padding-left', '0px');
        $('.internal-page-container').css('padding-left', '0px');
        $('.internal-page-container').css('padding-right', '0px');
    };
    CalendarioDePontos.prototype.loadDataFromService = function (startDatetime, endDatetime, append) {
        if (append === void 0) { append = false; }
        return __awaiter(this, void 0, void 0, function () {
            var dadosIniciais, pontoEletronicoAnterior, _i, _a, pontoEletronico, pontoEletronicoAtual, ex_1;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 2, , 3]);
                        if (!append) {
                            this.dadosPessoas = new Map();
                        }
                        return [4 /*yield*/, this.pontoEletronicoDataService.getDadosIniciaisDoCalendario(startDatetime, endDatetime)];
                    case 1:
                        dadosIniciais = _b.sent();
                        if (dadosIniciais != null) {
                            if (dadosIniciais.pontos != null) {
                                //adiciona um ponto nulo no final do array
                                dadosIniciais.pontos.push(null);
                                pontoEletronicoAnterior = null;
                                //faz iteração dos dados recebidos do servidor
                                //tenta montar o par pontoEletronicoAnterior,pontoEletronicoAtual
                                //em cada evento a ser exibido no calendário
                                for (_i = 0, _a = dadosIniciais.pontos; _i < _a.length; _i++) {
                                    pontoEletronico = _a[_i];
                                    if (pontoEletronico == null) {
                                        if (pontoEletronicoAnterior != null) {
                                            this.criarEvento(pontoEletronicoAnterior);
                                        }
                                        break;
                                    }
                                    pontoEletronicoAtual = model_1.PontoEletronico.factory(pontoEletronico);
                                    if (pontoEletronicoAnterior == null) {
                                        pontoEletronicoAnterior = pontoEletronicoAtual;
                                    }
                                    else {
                                        //caso a pessoa da iteração atual seja diferente da iteração anterior
                                        if (pontoEletronicoAtual.idPessoa != pontoEletronicoAnterior.idPessoa) {
                                            this.criarEvento(pontoEletronicoAnterior);
                                            pontoEletronicoAnterior = pontoEletronicoAtual;
                                        }
                                        else {
                                            if (pontoEletronicoAnterior.tipoPonto == geral_2.TipoPontoEletronico.PONTO_ENTRADA
                                                && pontoEletronicoAtual.tipoPonto == geral_2.TipoPontoEletronico.PONTO_SAIDA) {
                                                this.criarEvento(pontoEletronicoAnterior, pontoEletronicoAtual);
                                                pontoEletronicoAnterior = null;
                                            }
                                            else {
                                                this.criarEvento(pontoEletronicoAnterior);
                                                pontoEletronicoAnterior = pontoEletronicoAtual;
                                            }
                                        }
                                    }
                                }
                                //apos iterar a estrutura, adicionar eventos ao calendário              
                                this.adicionarEventosAoCalendario();
                                this.atualizarViewsAuxiliares();
                            }
                        }
                        return [3 /*break*/, 3];
                    case 2:
                        ex_1 = _b.sent();
                        console.log(ex_1);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    CalendarioDePontos.prototype.adicionarEventosAoCalendario = function () {
        this.calendario.limparEventos();
        var arrEventos = new Array();
        this.dadosPessoas.forEach(function (value, key) { return arrEventos = arrEventos.concat(value.eventos); });
        var fullCalendarEvents = arrEventos.map(function (evento) { return evento.getEventoFullCalendar(); });
        this.calendario.adicionarEventos(fullCalendarEvents);
    };
    CalendarioDePontos.prototype.atualizarViewsAuxiliares = function () {
        var arrDados = Array.from(this.dadosPessoas.values());
        this.totalizador.setData(arrDados);
        this.graficoDistribuicao.setData(arrDados);
        this.painelProblemasEncontrados.setData(arrDados);
    };
    CalendarioDePontos.prototype.criarEvento = function (pontoInicial, pontoFinal) {
        if (!this.dadosPessoas.has(pontoInicial.idPessoa)) {
            var pessoaModel = model_1.Pessoa.factory(pontoInicial, pontoFinal);
            pessoaModel.addEvento(pontoInicial, pontoFinal);
            this.dadosPessoas.set(pontoInicial.idPessoa, pessoaModel);
        }
        else {
            this.dadosPessoas.get(pontoInicial.idPessoa).addEvento(pontoInicial, pontoFinal);
        }
    };
    return CalendarioDePontos;
}(contexto_1.ContextoGenerico));
exports.CalendarioDePontos = CalendarioDePontos;
(function (CalendarioDePontos) {
})(CalendarioDePontos = exports.CalendarioDePontos || (exports.CalendarioDePontos = {}));
exports.CalendarioDePontos = CalendarioDePontos;
