<?php


        /*

        Arquivo gerado através de gerador de código em 12/05/2018 as 14:50:23.
        Para que o arquivo não seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

        Tabela correspondente: sistema_atributo
        Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

        */
        


class DAO_Sistema_atributo extends Generic_DAO
                        {
                            const NOME_ENTIDADE = "sistema_atributo";

                            public static $nomeTabela = null;
                            public static $idSistemaTabela = false;
                            public static $databaseFieldsRelatedAttributes = null;
	                        public static $databaseFieldTypes = null;
	                        public static $databaseFieldNames = null;
	                        public static $listAliasTypes = null;
	                        public static $listAliasRelatedAttributes = null;
	                        
	                        public $id;
public $nome;
public $nomeExibicao;
public $sistemaTabelaId;
public $tipoSql;
public $tipoSqlFicticio;
public $tamanhoInt;
public $decimalInt;
public $notNullBoolean;
public $primaryKeyBoolean;
public $autoIncrementBoolean;
public $valorDefault;
public $fkSistemaTabelaId;
public $atributoFk;
public $fkNome;
public $updateTipoFk;
public $deleteTipoFk;
public $label;
public $uniqueBoolean;
public $uniqueNome;
public $seqInt;

public $labelId;
public $labelNome;
public $labelNomeExibicao;
public $labelSistemaTabelaId;
public $labelTipoSql;
public $labelTipoSqlFicticio;
public $labelTamanhoInt;
public $labelDecimalInt;
public $labelNotNullBoolean;
public $labelPrimaryKeyBoolean;
public $labelAutoIncrementBoolean;
public $labelValorDefault;
public $labelFkSistemaTabelaId;
public $labelAtributoFk;
public $labelFkNome;
public $labelUpdateTipoFk;
public $labelDeleteTipoFk;
public $labelLabel;
public $labelUniqueBoolean;
public $labelUniqueNome;
public $labelSeqInt;

public static $arrPK = array("id");

public function __construct($configDAO = null)
        {
            parent::__construct($configDAO);
            static::__setStaticValues($this->database);           
        }

public static function idSistemaTabela($databaseObject = null){
            if(!(static::$idSistemaTabela))
            {
                static::$idSistemaTabela = EXTDAO_Sistema_tabela::getIdDaSistemaTabela(static::nomeTabela(), $databaseObject);
            }
            return static::$idSistemaTabela;
        }

public function getIdSistemaTabela(){
            return static::idSistemaTabela();
        }

public static function nomeTabela(){
            if(is_null(static::$nomeTabela))
            {
                static::$nomeTabela = "sistema_atributo";
            }
            return static::$nomeTabela;
        }

public function getNomeTabela(){
            return static::nomeTabela();
        }

public static function __setStaticValues($databaseObject = null)
                   {    
                        static::nomeTabela();
                        
                        static::idSistemaTabela($databaseObject);
                   
                        static::setDatabaseFieldNames();
                        static::setDatabaseFieldTypes();
                        static::setDatabaseFieldsRelatedAttributeNames();
                        
                        static::setListAliasTypes();
                        static::setListAliasRelatedAttributes();
                   }

public function getExpressaoLabel()
                   {
                        return "{nome}";
                   }

public static function setDatabaseFieldNames()
        {
            if(is_null(static::$databaseFieldNames))
            {
                static::$databaseFieldNames = new stdClass();
                static::$databaseFieldNames->id = "id";
static::$databaseFieldNames->nome = "nome";
static::$databaseFieldNames->nomeExibicao = "nome_exibicao";
static::$databaseFieldNames->sistemaTabelaId = "sistema_tabela_id_INT";
static::$databaseFieldNames->tipoSql = "tipo_sql";
static::$databaseFieldNames->tipoSqlFicticio = "tipo_sql_ficticio";
static::$databaseFieldNames->tamanhoInt = "tamanho_INT";
static::$databaseFieldNames->decimalInt = "decimal_INT";
static::$databaseFieldNames->notNullBoolean = "not_null_BOOLEAN";
static::$databaseFieldNames->primaryKeyBoolean = "primary_key_BOOLEAN";
static::$databaseFieldNames->autoIncrementBoolean = "auto_increment_BOOLEAN";
static::$databaseFieldNames->valorDefault = "valor_default";
static::$databaseFieldNames->fkSistemaTabelaId = "fk_sistema_tabela_id_INT";
static::$databaseFieldNames->atributoFk = "atributo_fk";
static::$databaseFieldNames->fkNome = "fk_nome";
static::$databaseFieldNames->updateTipoFk = "update_tipo_fk";
static::$databaseFieldNames->deleteTipoFk = "delete_tipo_fk";
static::$databaseFieldNames->label = "label";
static::$databaseFieldNames->uniqueBoolean = "unique_BOOLEAN";
static::$databaseFieldNames->uniqueNome = "unique_nome";
static::$databaseFieldNames->seqInt = "seq_INT";

            }
                      
        }

public static function setDatabaseFieldTypes()
        {
            if(is_null(static::$databaseFieldTypes))
            {
                static::$databaseFieldTypes = new stdClass();
                static::$databaseFieldTypes->id = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->nome = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->nomeExibicao = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->sistemaTabelaId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->tipoSql = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->tipoSqlFicticio = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->tamanhoInt = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->decimalInt = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->notNullBoolean = static::TIPO_VARIAVEL_BOOLEAN;
static::$databaseFieldTypes->primaryKeyBoolean = static::TIPO_VARIAVEL_BOOLEAN;
static::$databaseFieldTypes->autoIncrementBoolean = static::TIPO_VARIAVEL_BOOLEAN;
static::$databaseFieldTypes->valorDefault = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->fkSistemaTabelaId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->atributoFk = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->fkNome = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->updateTipoFk = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->deleteTipoFk = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->label = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->uniqueBoolean = static::TIPO_VARIAVEL_BOOLEAN;
static::$databaseFieldTypes->uniqueNome = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->seqInt = static::TIPO_VARIAVEL_INTEGER;

            }
            
        }

public static function setDatabaseFieldsRelatedAttributeNames()
        {
            if(is_null(static::$databaseFieldsRelatedAttributes))
            {
                static::$databaseFieldsRelatedAttributes = new stdClass();
                static::$databaseFieldsRelatedAttributes->id = "id";
static::$databaseFieldsRelatedAttributes->nome = "nome";
static::$databaseFieldsRelatedAttributes->nome_exibicao = "nomeExibicao";
static::$databaseFieldsRelatedAttributes->sistema_tabela_id_INT = "sistemaTabelaId";
static::$databaseFieldsRelatedAttributes->tipo_sql = "tipoSql";
static::$databaseFieldsRelatedAttributes->tipo_sql_ficticio = "tipoSqlFicticio";
static::$databaseFieldsRelatedAttributes->tamanho_INT = "tamanhoInt";
static::$databaseFieldsRelatedAttributes->decimal_INT = "decimalInt";
static::$databaseFieldsRelatedAttributes->not_null_BOOLEAN = "notNullBoolean";
static::$databaseFieldsRelatedAttributes->primary_key_BOOLEAN = "primaryKeyBoolean";
static::$databaseFieldsRelatedAttributes->auto_increment_BOOLEAN = "autoIncrementBoolean";
static::$databaseFieldsRelatedAttributes->valor_default = "valorDefault";
static::$databaseFieldsRelatedAttributes->fk_sistema_tabela_id_INT = "fkSistemaTabelaId";
static::$databaseFieldsRelatedAttributes->atributo_fk = "atributoFk";
static::$databaseFieldsRelatedAttributes->fk_nome = "fkNome";
static::$databaseFieldsRelatedAttributes->update_tipo_fk = "updateTipoFk";
static::$databaseFieldsRelatedAttributes->delete_tipo_fk = "deleteTipoFk";
static::$databaseFieldsRelatedAttributes->label = "label";
static::$databaseFieldsRelatedAttributes->unique_BOOLEAN = "uniqueBoolean";
static::$databaseFieldsRelatedAttributes->unique_nome = "uniqueNome";
static::$databaseFieldsRelatedAttributes->seq_INT = "seqInt";

            }           
        }

public function setAllFieldsToHumanFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["nome"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["nome_exibicao"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["sistema_tabela_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["tipo_sql"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["tipo_sql_ficticio"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["tamanho_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["decimal_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["not_null_BOOLEAN"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["primary_key_BOOLEAN"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["auto_increment_BOOLEAN"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["valor_default"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["fk_sistema_tabela_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["atributo_fk"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["fk_nome"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["update_tipo_fk"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["delete_tipo_fk"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["label"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["unique_BOOLEAN"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["unique_nome"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["seq_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
}

public function setAllFieldsToDatabaseFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["nome"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["nome_exibicao"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["sistema_tabela_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["tipo_sql"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["tipo_sql_ficticio"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["tamanho_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["decimal_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["not_null_BOOLEAN"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["primary_key_BOOLEAN"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["auto_increment_BOOLEAN"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["valor_default"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["fk_sistema_tabela_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["atributo_fk"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["fk_nome"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["update_tipo_fk"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["delete_tipo_fk"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["label"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["unique_BOOLEAN"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["unique_nome"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["seq_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
}

  public function getFkObjSistema_tabela() 
                {
                    if($this->objSistema_tabela == null)
                    {                        
                        $this->objSistema_tabela = new EXTDAO_Sistema_tabela_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getSistema_tabela_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objSistema_tabela->clear();
                    }
                    elseif($this->objSistema_tabela->getId() != $idFK)
                    {
                        $this->objSistema_tabela->select($idFK);
                    }
                    return $this->objSistema_tabela;
                }
  public function getFkObjFk_sistema_tabela() 
                {
                    if($this->objFk_sistema_tabela == null)
                    {                        
                        $this->objFk_sistema_tabela = new EXTDAO_Fk_sistema_tabela_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getFk_sistema_tabela_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objFk_sistema_tabela->clear();
                    }
                    elseif($this->objFk_sistema_tabela->getId() != $idFK)
                    {
                        $this->objFk_sistema_tabela->select($idFK);
                    }
                    return $this->objFk_sistema_tabela;
                }


public function setLabels()
        {$this->labelId = "";
$this->labelNome = "";
$this->labelNomeExibicao = "";
$this->labelSistemaTabelaId = "";
$this->labelTipoSql = "";
$this->labelTipoSqlFicticio = "";
$this->labelTamanhoInt = "";
$this->labelDecimalInt = "";
$this->labelNotNullBoolean = "";
$this->labelPrimaryKeyBoolean = "";
$this->labelAutoIncrementBoolean = "";
$this->labelValorDefault = "";
$this->labelFkSistemaTabelaId = "";
$this->labelAtributoFk = "";
$this->labelFkNome = "";
$this->labelUpdateTipoFk = "";
$this->labelDeleteTipoFk = "";
$this->labelLabel = "";
$this->labelUniqueBoolean = "";
$this->labelUniqueNome = "";
$this->labelSeqInt = "";
}

public function __actionAdd($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                $this->setByObject($parameters);
                $this->setDataCadastroAndOffsetInSecondsFromUser($parameters);
                
                $this->formatarParaSQL();
                $msg = $this->insert(true);
                
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                return new Mensagem(null, I18N::getExpression("Sistema atributo adicionado com sucesso."));
                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionEdit($parameters = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try {

                $this->setByObject($parameters);
                $this->setAllFieldsToHumanFormat();
                
                $this->formatarParaSQL();
                $msg = $this->updateByInstanceUsingParameters($this->id, $parameters, true);
                if($msg != null && $msg->erro())
                {
                     return $msg;
                }
        
                return new Mensagem(null, I18N::getExpression("Sistema atributo editado com sucesso."));
            }
            catch(Exception $ex)
            {                
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionRemove($parameters = null)
        {
            $successulRemovalCount = 0;
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                foreach($parameters as $record)
                {
                    if(is_numeric($record->id))
                    {
                        $msg = $this->delete($record->id, true);
                        if($msg == null || $msg->ok())
                        {
                            $successulRemovalCount++;
                        }
                    }
                }
                
                if($successulRemovalCount > 0)
                {
                    $successMessage = count($successulRemovalCount) > 1 ? I18N::getExpression("{0} Sistema atributo foram removidos com sucesso.", $successulRemovalCount) : I18N::getExpression("Sistema atributo removido com sucesso.");
                    return new Mensagem(null, $successMessage);
                }
                else
                {
                    $errorMessage = count($parameters) > 1 ? I18N::getExpression("Falha ao remover Sistema atributo.") : I18N::getExpression("Falha ao remover Sistema atributo.");
                    return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, $errorMessage);                    
                }
            }
            catch (Exception $ex) 
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }

public function __getRecord($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }
            
            try
            {            
                $returnObject = new stdClass();            
                $whereClause = static::getWhereClauseForFilter($parameters->filterParameters);
                $query = "SELECT  id, nome, nome_exibicao, sistema_tabela_id_INT, tipo_sql, tipo_sql_ficticio, tamanho_INT, decimal_INT, not_null_BOOLEAN, primary_key_BOOLEAN, auto_increment_BOOLEAN, valor_default, fk_sistema_tabela_id_INT, atributo_fk, fk_nome, update_tipo_fk, delete_tipo_fk, label, unique_BOOLEAN, unique_nome, seq_INT FROM sistema_atributo {$whereClause}";
    
                $msg = $this->database->queryMensagem($query);                
                if ($msg == null)
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Não foi encontrado nenhum registro"));
                    }                    
                    $resultSet = $this->database->result;
                    $resultObject = static::mapResultSetDataToRecord($resultSet);
                    
                    $returnObject->formData = $resultObject;
                    $returnObject->comboBoxesData = null;
                    if($parameters->loadComboBoxesData)
                    {
                        $this->setByObject($resultObject);
                        $returnObject->comboBoxesData =  $this->getFormComboBoxesData();
                    }                    
    
                    
    
                    return new Mensagem_generica($returnObject);
                } 
                else 
                {
                    return $msg;
                }
                
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }


        
        public function __getFilterComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
                
        public function __getFormComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
        
        protected function getFormComboBoxesData()
        {
            $comboBoxesData = new stdClass();
            $listParameters = new stdClass();
            $listParameters->listMappingType = static::LIST_MAPPING_TYPE_COMBOBOX;
            
            
                    
                    $objSistemaTabela = new EXTDAO_Sistema_tabela();
                    $comboBoxesData->fieldSistemaTabelaId = $objSistemaTabela->__getList($listParameters);

            return $comboBoxesData;
        }
        
        


        
        public static function setListAliasTypes()
        {
            if(is_null(static::$listAliasTypes))
            {
                static::$listAliasTypes = new stdClass();
                static::$listAliasTypes->sistema_tabela__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->sistema_atributo__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->sistema_atributo__nome = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->sistema_atributo__nome_exibicao = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->sistema_atributo__atributos_chave_unica = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->sistema_atributo__tipo_sql = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->sistema_atributo__tipo_sql_ficticio = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->sistema_atributo__tamanho_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->sistema_atributo__decimal_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->sistema_atributo__not_null_BOOLEAN = static::TIPO_VARIAVEL_BOOLEAN;
static::$listAliasTypes->sistema_atributo__primary_key_BOOLEAN = static::TIPO_VARIAVEL_BOOLEAN;
static::$listAliasTypes->sistema_atributo__auto_increment_BOOLEAN = static::TIPO_VARIAVEL_BOOLEAN;
static::$listAliasTypes->sistema_atributo__valor_default = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->sistema_atributo__fk_sistema_tabela_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->sistema_atributo__atributo_fk = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->sistema_atributo__fk_nome = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->sistema_atributo__update_tipo_fk = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->sistema_atributo__delete_tipo_fk = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->sistema_atributo__label = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->sistema_atributo__unique_BOOLEAN = static::TIPO_VARIAVEL_BOOLEAN;
static::$listAliasTypes->sistema_atributo__unique_nome = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->sistema_atributo__seq_INT = static::TIPO_VARIAVEL_INTEGER;
            }        
        }
        
        public static function setListAliasRelatedAttributes()
        {
            if(is_null(static::$listAliasRelatedAttributes))
            {
                static::$listAliasRelatedAttributes = new stdClass();
                static::$listAliasRelatedAttributes->sistema_tabela__id = "sistemaTabelaId";
static::$listAliasRelatedAttributes->sistema_atributo__id = "id";
static::$listAliasRelatedAttributes->sistema_atributo__nome = "nome";
static::$listAliasRelatedAttributes->sistema_atributo__nome_exibicao = "nomeExibicao";
static::$listAliasRelatedAttributes->sistema_atributo__atributos_chave_unica = "atributosChaveUnica";
static::$listAliasRelatedAttributes->sistema_atributo__tipo_sql = "tipoSql";
static::$listAliasRelatedAttributes->sistema_atributo__tipo_sql_ficticio = "tipoSqlFicticio";
static::$listAliasRelatedAttributes->sistema_atributo__tamanho_INT = "tamanhoInt";
static::$listAliasRelatedAttributes->sistema_atributo__decimal_INT = "decimalInt";
static::$listAliasRelatedAttributes->sistema_atributo__not_null_BOOLEAN = "notNullBoolean";
static::$listAliasRelatedAttributes->sistema_atributo__primary_key_BOOLEAN = "primaryKeyBoolean";
static::$listAliasRelatedAttributes->sistema_atributo__auto_increment_BOOLEAN = "autoIncrementBoolean";
static::$listAliasRelatedAttributes->sistema_atributo__valor_default = "valorDefault";
static::$listAliasRelatedAttributes->sistema_atributo__fk_sistema_tabela_id_INT = "fkSistemaTabelaId";
static::$listAliasRelatedAttributes->sistema_atributo__atributo_fk = "atributoFk";
static::$listAliasRelatedAttributes->sistema_atributo__fk_nome = "fkNome";
static::$listAliasRelatedAttributes->sistema_atributo__update_tipo_fk = "updateTipoFk";
static::$listAliasRelatedAttributes->sistema_atributo__delete_tipo_fk = "deleteTipoFk";
static::$listAliasRelatedAttributes->sistema_atributo__label = "label";
static::$listAliasRelatedAttributes->sistema_atributo__unique_BOOLEAN = "uniqueBoolean";
static::$listAliasRelatedAttributes->sistema_atributo__unique_nome = "uniqueNome";
static::$listAliasRelatedAttributes->sistema_atributo__seq_INT = "seqInt";
            }         
        }

        public function __getList($parameters = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try
            {
                $filterParameters = null;
                if(isset($parameters->filterParameters))
                {
                    $filterParameters = $parameters->filterParameters;
                }
                                    
                    
                
                $sortingParameters = null;
                if(isset($parameters->sortingParameters))
                {
                    $sortingParameters = $parameters->sortingParameters;
                }
                
                $paginationParameters = null;
                if(isset($parameters->paginationParameters))
                {
                    $paginationParameters = $parameters->paginationParameters;
                }
                
                $listMappingType = static::getListMappingType($parameters->listMappingType);
                $mainTableAlias = "sa";

                $whereClause = static::getWhereClauseForFilter($filterParameters, $mainTableAlias);
                $orderByClause = static::getOrderByClauseForFilter($sortingParameters, $mainTableAlias);
                $limitClause = static::getLimitClauseForFilter($paginationParameters);

                $queryWithoutLimit = "SELECT sa.id FROM sistema_atributo sa {$whereClause}";
                $query = "SELECT st.id AS sistema_tabela__id, sa.id AS sistema_atributo__id, sa.nome AS sistema_atributo__nome, sa.nome_exibicao AS sistema_atributo__nome_exibicao, sa.atributos_chave_unica AS sistema_atributo__atributos_chave_unica, sa.tipo_sql AS sistema_atributo__tipo_sql, sa.tipo_sql_ficticio AS sistema_atributo__tipo_sql_ficticio, sa.tamanho_INT AS sistema_atributo__tamanho_INT, sa.decimal_INT AS sistema_atributo__decimal_INT, sa.not_null_BOOLEAN AS sistema_atributo__not_null_BOOLEAN, sa.primary_key_BOOLEAN AS sistema_atributo__primary_key_BOOLEAN, sa.auto_increment_BOOLEAN AS sistema_atributo__auto_increment_BOOLEAN, sa.valor_default AS sistema_atributo__valor_default, sa.fk_sistema_tabela_id_INT AS sistema_atributo__fk_sistema_tabela_id_INT, sa.atributo_fk AS sistema_atributo__atributo_fk, sa.fk_nome AS sistema_atributo__fk_nome, sa.update_tipo_fk AS sistema_atributo__update_tipo_fk, sa.delete_tipo_fk AS sistema_atributo__delete_tipo_fk, sa.label AS sistema_atributo__label, sa.unique_BOOLEAN AS sistema_atributo__unique_BOOLEAN, sa.unique_nome AS sistema_atributo__unique_nome, sa.seq_INT AS sistema_atributo__seq_INT FROM sistema_atributo sa LEFT JOIN sistema_tabela st ON st.id = sa.sistema_tabela_id_INT {$whereClause} {$orderByClause} {$limitClause}";

                $msg = $this->database->queryMensagem($queryWithoutLimit);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                
                $totalNumberOfRecords = $this->database->rows();
                if($totalNumberOfRecords == 0)
                {
                    return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                }                        
                        
                $msg = $this->database->queryMensagem($query);
                
                //operacao realizaca com sucesso
                if ($msg == null) 
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                    }                        
                
                    $resultSet = $this->database->result;

                    $objReturn = new stdClass();
                    $objReturn->dataSet = static::mapResultSetDataToList($resultSet, $listMappingType, static::$listAliasTypes);

                    if(!Helper::isNullOrEmpty($paginationParameters->recordsPerPage) && !Helper::isNullOrEmpty($paginationParameters->currentPage))
                    {
                        $lastPage = ceil($totalNumberOfRecords / $paginationParameters->recordsPerPage);
                        $objReturn->paginationParameters = new PaginationParams($paginationParameters->recordsPerPage, $paginationParameters->currentPage, $lastPage);
                    }
                    
                    return new Mensagem_generica($objReturn);
                } 
                else
                {
                    return $msg;
                }                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }



public function getId()
            {
                return $this->id;
            }

public function getNome()
            {
                return $this->nome;
            }

public function getNomeExibicao()
            {
                return $this->nomeExibicao;
            }

public function getNome_exibicao()
                {
                    return $this->nomeExibicao;
                }

public function getSistemaTabelaId()
            {
                return $this->sistemaTabelaId;
            }

public function getSistema_tabela_id_INT()
                {
                    return $this->sistemaTabelaId;
                }

public function getTipoSql()
            {
                return $this->tipoSql;
            }

public function getTipo_sql()
                {
                    return $this->tipoSql;
                }

public function getTipoSqlFicticio()
            {
                return $this->tipoSqlFicticio;
            }

public function getTipo_sql_ficticio()
                {
                    return $this->tipoSqlFicticio;
                }

public function getTamanhoInt()
            {
                return $this->tamanhoInt;
            }

public function getTamanho_INT()
                {
                    return $this->tamanhoInt;
                }

public function getDecimalInt()
            {
                return $this->decimalInt;
            }

public function getDecimal_INT()
                {
                    return $this->decimalInt;
                }

public function getNotNullBoolean()
            {
                return $this->notNullBoolean;
            }

public function getNot_null_BOOLEAN()
                {
                    return $this->notNullBoolean;
                }

public function getPrimaryKeyBoolean()
            {
                return $this->primaryKeyBoolean;
            }

public function getPrimary_key_BOOLEAN()
                {
                    return $this->primaryKeyBoolean;
                }

public function getAutoIncrementBoolean()
            {
                return $this->autoIncrementBoolean;
            }

public function getAuto_increment_BOOLEAN()
                {
                    return $this->autoIncrementBoolean;
                }

public function getValorDefault()
            {
                return $this->valorDefault;
            }

public function getValor_default()
                {
                    return $this->valorDefault;
                }

public function getFkSistemaTabelaId()
            {
                return $this->fkSistemaTabelaId;
            }

public function getFk_sistema_tabela_id_INT()
                {
                    return $this->fkSistemaTabelaId;
                }

public function getAtributoFk()
            {
                return $this->atributoFk;
            }

public function getAtributo_fk()
                {
                    return $this->atributoFk;
                }

public function getFkNome()
            {
                return $this->fkNome;
            }

public function getFk_nome()
                {
                    return $this->fkNome;
                }

public function getUpdateTipoFk()
            {
                return $this->updateTipoFk;
            }

public function getUpdate_tipo_fk()
                {
                    return $this->updateTipoFk;
                }

public function getDeleteTipoFk()
            {
                return $this->deleteTipoFk;
            }

public function getDelete_tipo_fk()
                {
                    return $this->deleteTipoFk;
                }

public function getLabel()
            {
                return $this->label;
            }

public function getUniqueBoolean()
            {
                return $this->uniqueBoolean;
            }

public function getUnique_BOOLEAN()
                {
                    return $this->uniqueBoolean;
                }

public function getUniqueNome()
            {
                return $this->uniqueNome;
            }

public function getUnique_nome()
                {
                    return $this->uniqueNome;
                }

public function getSeqInt()
            {
                return $this->seqInt;
            }

public function getSeq_INT()
                {
                    return $this->seqInt;
                }



function setId($value)
            {
                $this->id = $value;
            }

function setNome($value)
            {
                $this->nome = $value;
            }

function setNomeExibicao($value)
            {
                $this->nomeExibicao = $value;
            }

public function setNome_exibicao($value)
                { 
                    $this->nomeExibicao = $value; 
                }

function setSistemaTabelaId($value)
            {
                $this->sistemaTabelaId = $value;
            }

public function setSistema_tabela_id_INT($value)
                { 
                    $this->sistemaTabelaId = $value; 
                }

function setTipoSql($value)
            {
                $this->tipoSql = $value;
            }

public function setTipo_sql($value)
                { 
                    $this->tipoSql = $value; 
                }

function setTipoSqlFicticio($value)
            {
                $this->tipoSqlFicticio = $value;
            }

public function setTipo_sql_ficticio($value)
                { 
                    $this->tipoSqlFicticio = $value; 
                }

function setTamanhoInt($value)
            {
                $this->tamanhoInt = $value;
            }

public function setTamanho_INT($value)
                { 
                    $this->tamanhoInt = $value; 
                }

function setDecimalInt($value)
            {
                $this->decimalInt = $value;
            }

public function setDecimal_INT($value)
                { 
                    $this->decimalInt = $value; 
                }

function setNotNullBoolean($value)
            {
                $this->notNullBoolean = $value;
            }

public function setNot_null_BOOLEAN($value)
                { 
                    $this->notNullBoolean = $value; 
                }

function setPrimaryKeyBoolean($value)
            {
                $this->primaryKeyBoolean = $value;
            }

public function setPrimary_key_BOOLEAN($value)
                { 
                    $this->primaryKeyBoolean = $value; 
                }

function setAutoIncrementBoolean($value)
            {
                $this->autoIncrementBoolean = $value;
            }

public function setAuto_increment_BOOLEAN($value)
                { 
                    $this->autoIncrementBoolean = $value; 
                }

function setValorDefault($value)
            {
                $this->valorDefault = $value;
            }

public function setValor_default($value)
                { 
                    $this->valorDefault = $value; 
                }

function setFkSistemaTabelaId($value)
            {
                $this->fkSistemaTabelaId = $value;
            }

public function setFk_sistema_tabela_id_INT($value)
                { 
                    $this->fkSistemaTabelaId = $value; 
                }

function setAtributoFk($value)
            {
                $this->atributoFk = $value;
            }

public function setAtributo_fk($value)
                { 
                    $this->atributoFk = $value; 
                }

function setFkNome($value)
            {
                $this->fkNome = $value;
            }

public function setFk_nome($value)
                { 
                    $this->fkNome = $value; 
                }

function setUpdateTipoFk($value)
            {
                $this->updateTipoFk = $value;
            }

public function setUpdate_tipo_fk($value)
                { 
                    $this->updateTipoFk = $value; 
                }

function setDeleteTipoFk($value)
            {
                $this->deleteTipoFk = $value;
            }

public function setDelete_tipo_fk($value)
                { 
                    $this->deleteTipoFk = $value; 
                }

function setLabel($value)
            {
                $this->label = $value;
            }

function setUniqueBoolean($value)
            {
                $this->uniqueBoolean = $value;
            }

public function setUnique_BOOLEAN($value)
                { 
                    $this->uniqueBoolean = $value; 
                }

function setUniqueNome($value)
            {
                $this->uniqueNome = $value;
            }

public function setUnique_nome($value)
                { 
                    $this->uniqueNome = $value; 
                }

function setSeqInt($value)
            {
                $this->seqInt = $value;
            }

public function setSeq_INT($value)
                { 
                    $this->seqInt = $value; 
                }



public function clear()
        {$this->id = null;
$this->nome = null;
$this->nomeExibicao = null;
$this->sistemaTabelaId = null;
if($this->objSistema_tabela != null) unset($this->objSistema_tabela);
$this->tipoSql = null;
$this->tipoSqlFicticio = null;
$this->tamanhoInt = null;
$this->decimalInt = null;
$this->notNullBoolean = null;
$this->primaryKeyBoolean = null;
$this->autoIncrementBoolean = null;
$this->valorDefault = null;
$this->fkSistemaTabelaId = null;
if($this->objFk_sistema_tabela != null) unset($this->objFk_sistema_tabela);
$this->atributoFk = null;
$this->fkNome = null;
$this->updateTipoFk = null;
$this->deleteTipoFk = null;
$this->label = null;
$this->uniqueBoolean = null;
$this->uniqueNome = null;
$this->seqInt = null;
if(is_array($this->arrEstadosCampos)){
                        unset($this->arrEstadosCampos);
                     $this->arrEstadosCampos = array();
                    }
                }

public function formatarParaSQL()
        {
if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->id)){
$this->id = $this->formatarIntegerParaComandoSQL($this->id);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->id);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->nome)){
$this->nome = $this->formatarStringParaComandoSQL($this->nome);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->nome);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->nomeExibicao)){
$this->nomeExibicao = $this->formatarStringParaComandoSQL($this->nomeExibicao);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->nomeExibicao);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->sistemaTabelaId)){
$this->sistemaTabelaId = $this->formatarIntegerParaComandoSQL($this->sistemaTabelaId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->sistemaTabelaId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->tipoSql)){
$this->tipoSql = $this->formatarStringParaComandoSQL($this->tipoSql);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->tipoSql);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->tipoSqlFicticio)){
$this->tipoSqlFicticio = $this->formatarStringParaComandoSQL($this->tipoSqlFicticio);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->tipoSqlFicticio);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->tamanhoInt)){
$this->tamanhoInt = $this->formatarIntegerParaComandoSQL($this->tamanhoInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->tamanhoInt);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->decimalInt)){
$this->decimalInt = $this->formatarIntegerParaComandoSQL($this->decimalInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->decimalInt);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->notNullBoolean)){
$this->notNullBoolean = $this->formatarBooleanParaComandoSQL($this->notNullBoolean);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->notNullBoolean);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->primaryKeyBoolean)){
$this->primaryKeyBoolean = $this->formatarBooleanParaComandoSQL($this->primaryKeyBoolean);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->primaryKeyBoolean);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->autoIncrementBoolean)){
$this->autoIncrementBoolean = $this->formatarBooleanParaComandoSQL($this->autoIncrementBoolean);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->autoIncrementBoolean);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->valorDefault)){
$this->valorDefault = $this->formatarStringParaComandoSQL($this->valorDefault);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->valorDefault);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->fkSistemaTabelaId)){
$this->fkSistemaTabelaId = $this->formatarIntegerParaComandoSQL($this->fkSistemaTabelaId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->fkSistemaTabelaId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->atributoFk)){
$this->atributoFk = $this->formatarStringParaComandoSQL($this->atributoFk);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->atributoFk);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->fkNome)){
$this->fkNome = $this->formatarStringParaComandoSQL($this->fkNome);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->fkNome);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->updateTipoFk)){
$this->updateTipoFk = $this->formatarStringParaComandoSQL($this->updateTipoFk);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->updateTipoFk);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->deleteTipoFk)){
$this->deleteTipoFk = $this->formatarStringParaComandoSQL($this->deleteTipoFk);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->deleteTipoFk);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->label)){
$this->label = $this->formatarStringParaComandoSQL($this->label);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->label);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->uniqueBoolean)){
$this->uniqueBoolean = $this->formatarBooleanParaComandoSQL($this->uniqueBoolean);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->uniqueBoolean);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->uniqueNome)){
$this->uniqueNome = $this->formatarStringParaComandoSQL($this->uniqueNome);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->uniqueNome);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->seqInt)){
$this->seqInt = $this->formatarIntegerParaComandoSQL($this->seqInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->seqInt);
}

}

public function formatarParaExibicao()
        {if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->nome)){
$this->nome = $this->formatarStringParaExibicao($this->nome);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->nome);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->nomeExibicao)){
$this->nomeExibicao = $this->formatarStringParaExibicao($this->nomeExibicao);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->nomeExibicao);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->tipoSql)){
$this->tipoSql = $this->formatarStringParaExibicao($this->tipoSql);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->tipoSql);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->tipoSqlFicticio)){
$this->tipoSqlFicticio = $this->formatarStringParaExibicao($this->tipoSqlFicticio);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->tipoSqlFicticio);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->notNullBoolean)){
$this->notNullBoolean = $this->formatarBooleanParaExibicao($this->notNullBoolean);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->notNullBoolean);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->primaryKeyBoolean)){
$this->primaryKeyBoolean = $this->formatarBooleanParaExibicao($this->primaryKeyBoolean);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->primaryKeyBoolean);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->autoIncrementBoolean)){
$this->autoIncrementBoolean = $this->formatarBooleanParaExibicao($this->autoIncrementBoolean);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->autoIncrementBoolean);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->valorDefault)){
$this->valorDefault = $this->formatarStringParaExibicao($this->valorDefault);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->valorDefault);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->atributoFk)){
$this->atributoFk = $this->formatarStringParaExibicao($this->atributoFk);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->atributoFk);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->fkNome)){
$this->fkNome = $this->formatarStringParaExibicao($this->fkNome);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->fkNome);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->updateTipoFk)){
$this->updateTipoFk = $this->formatarStringParaExibicao($this->updateTipoFk);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->updateTipoFk);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->deleteTipoFk)){
$this->deleteTipoFk = $this->formatarStringParaExibicao($this->deleteTipoFk);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->deleteTipoFk);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->label)){
$this->label = $this->formatarStringParaExibicao($this->label);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->label);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->uniqueBoolean)){
$this->uniqueBoolean = $this->formatarBooleanParaExibicao($this->uniqueBoolean);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->uniqueBoolean);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->uniqueNome)){
$this->uniqueNome = $this->formatarStringParaExibicao($this->uniqueNome);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->uniqueNome);
}

}

function select($id, $idCorporacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }   

            $sql = "SELECT id, nome, nome_exibicao, sistema_tabela_id_INT, tipo_sql, tipo_sql_ficticio, tamanho_INT, decimal_INT, not_null_BOOLEAN, primary_key_BOOLEAN, auto_increment_BOOLEAN, valor_default, fk_sistema_tabela_id_INT, atributo_fk, fk_nome, update_tipo_fk, delete_tipo_fk, label, unique_BOOLEAN, unique_nome, seq_INT FROM sistema_atributo WHERE id = $id";

            $msg = $this->database->queryMensagem($sql);
            $result = $this->database->result;

            if($msg != null && $msg->erro() )
            { 
            		$this->clear();
            		return $msg;
        	} 
        	else if($this->database->rows() == 0)
        	{
                return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nenhum registro foi encontrado."));
            }
        

            $row = $this->database->fetchArray(MYSQLI_NUM);
            
            		$this->id = $row[0];
		$this->nome = $row[1];
		$this->nomeExibicao = $row[2];
		$this->sistemaTabelaId = $row[3];
		$this->tipoSql = $row[4];
		$this->tipoSqlFicticio = $row[5];
		$this->tamanhoInt = $row[6];
		$this->decimalInt = $row[7];
		$this->notNullBoolean = $row[8];
		$this->primaryKeyBoolean = $row[9];
		$this->autoIncrementBoolean = $row[10];
		$this->valorDefault = $row[11];
		$this->fkSistemaTabelaId = $row[12];
		$this->atributoFk = $row[13];
		$this->fkNome = $row[14];
		$this->updateTipoFk = $row[15];
		$this->deleteTipoFk = $row[16];
		$this->label = $row[17];
		$this->uniqueBoolean = $row[18];
		$this->uniqueNome = $row[19];
		$this->seqInt = $row[20];

            
        }

public function delete($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (is_null($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }

            $sql = "DELETE FROM sistema_atributo WHERE id= $id";
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                    && $sincronizar
                        && static::idSistemaTabela() != null
                            && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_REMOVE,
                                null,
                                null,
                                $id,
                                $this->database);
            }

            return $msg;
        }

public function insert($sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null, $opcoes = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            
            
            if (is_null($opcoes))
            {
                $opcoes = $this->opcoesDAO;
            }

            if (!is_numeric($this->id))
            {
                $this->id = "null";
                if ($opcoes == null || $opcoes[Database::OPCAO_GERAR_ID] != false) 
                {
                    $this->id = EXTDAO_Sistema_sequencia::gerarId(static::nomeTabela());                    
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }    
                    else if($msg == null)
                    {
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
                else 
                {
                    $this->id = null;
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao));
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }
                    else if($msg == null)
                    {
                        $this->id = $this->database->getLastInsertId();
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
            }
            else 
            {
                $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }                        
                else if($msg == null)
                {
                   $msg = new Mensagem_token();
                   $msg->mValor = $this->id;
                }
            }
            if($msg == null || $msg->ok())
            {                
                if($sincronizar
                    && static::idSistemaTabela() != null
                    && isset($this->id)){
                    
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_INSERT,
                                null,
                                null,
                                $this->id,
                                $this->database); 
                	}
            } 
            else
            {
                $this->id = null;
            }
            
            return $msg;
            
        }

public function getSQLInsert($idCorporacao){ 

return "INSERT INTO sistema_atributo (id, nome, nome_exibicao, sistema_tabela_id_INT, tipo_sql, tipo_sql_ficticio, tamanho_INT, decimal_INT, not_null_BOOLEAN, primary_key_BOOLEAN, auto_increment_BOOLEAN, valor_default, fk_sistema_tabela_id_INT, atributo_fk, fk_nome, update_tipo_fk, delete_tipo_fk, label, unique_BOOLEAN, unique_nome, seq_INT) VALUES ( $this->id ,  $this->nome ,  $this->nomeExibicao ,  $this->sistemaTabelaId ,  $this->tipoSql ,  $this->tipoSqlFicticio ,  $this->tamanhoInt ,  $this->decimalInt ,  $this->notNullBoolean ,  $this->primaryKeyBoolean ,  $this->autoIncrementBoolean ,  $this->valorDefault ,  $this->fkSistemaTabelaId ,  $this->atributoFk ,  $this->fkNome ,  $this->updateTipoFk ,  $this->deleteTipoFk ,  $this->label ,  $this->uniqueBoolean ,  $this->uniqueNome ,  $this->seqInt ) "; 

}




        
        public function updateByParameters($id, $objParametros, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao))
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();

            
                
                if (isset($objParametros->nome)) 
                {
                    $arrUpdateFields[] = " nome = {$objParametros->nome} ";
                }


                
                if (isset($objParametros->nomeExibicao)) 
                {
                    $arrUpdateFields[] = " nome_exibicao = {$objParametros->nomeExibicao} ";
                }


                
                if (isset($objParametros->sistemaTabelaId)) 
                {
                    $arrUpdateFields[] = " sistema_tabela_id_INT = {$objParametros->sistemaTabelaId} ";
                }


                
                if (isset($objParametros->tipoSql)) 
                {
                    $arrUpdateFields[] = " tipo_sql = {$objParametros->tipoSql} ";
                }


                
                if (isset($objParametros->tipoSqlFicticio)) 
                {
                    $arrUpdateFields[] = " tipo_sql_ficticio = {$objParametros->tipoSqlFicticio} ";
                }


                
                if (isset($objParametros->tamanhoInt)) 
                {
                    $arrUpdateFields[] = " tamanho_INT = {$objParametros->tamanhoInt} ";
                }


                
                if (isset($objParametros->decimalInt)) 
                {
                    $arrUpdateFields[] = " decimal_INT = {$objParametros->decimalInt} ";
                }


                
                if (isset($objParametros->notNullBoolean)) 
                {
                    $arrUpdateFields[] = " not_null_BOOLEAN = {$objParametros->notNullBoolean} ";
                }


                
                if (isset($objParametros->primaryKeyBoolean)) 
                {
                    $arrUpdateFields[] = " primary_key_BOOLEAN = {$objParametros->primaryKeyBoolean} ";
                }


                
                if (isset($objParametros->autoIncrementBoolean)) 
                {
                    $arrUpdateFields[] = " auto_increment_BOOLEAN = {$objParametros->autoIncrementBoolean} ";
                }


                
                if (isset($objParametros->valorDefault)) 
                {
                    $arrUpdateFields[] = " valor_default = {$objParametros->valorDefault} ";
                }


                
                if (isset($objParametros->fkSistemaTabelaId)) 
                {
                    $arrUpdateFields[] = " fk_sistema_tabela_id_INT = {$objParametros->fkSistemaTabelaId} ";
                }


                
                if (isset($objParametros->atributoFk)) 
                {
                    $arrUpdateFields[] = " atributo_fk = {$objParametros->atributoFk} ";
                }


                
                if (isset($objParametros->fkNome)) 
                {
                    $arrUpdateFields[] = " fk_nome = {$objParametros->fkNome} ";
                }


                
                if (isset($objParametros->updateTipoFk)) 
                {
                    $arrUpdateFields[] = " update_tipo_fk = {$objParametros->updateTipoFk} ";
                }


                
                if (isset($objParametros->deleteTipoFk)) 
                {
                    $arrUpdateFields[] = " delete_tipo_fk = {$objParametros->deleteTipoFk} ";
                }


                
                if (isset($objParametros->label)) 
                {
                    $arrUpdateFields[] = " label = {$objParametros->label} ";
                }


                
                if (isset($objParametros->uniqueBoolean)) 
                {
                    $arrUpdateFields[] = " unique_BOOLEAN = {$objParametros->uniqueBoolean} ";
                }


                
                if (isset($objParametros->uniqueNome)) 
                {
                    $arrUpdateFields[] = " unique_nome = {$objParametros->uniqueNome} ";
                }


                
                if (isset($objParametros->seqInt)) 
                {
                    $arrUpdateFields[] = " seq_INT = {$objParametros->seqInt} ";
                }



            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE sistema_atributo SET {$strUpdateFields} WHERE id = {$id}";

            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null)
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function update($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($this->nome)) 
                {                                      
                    $arrUpdateFields[] = " nome = {$this->nome} ";
                }


                
                if (isset($this->nomeExibicao)) 
                {                                      
                    $arrUpdateFields[] = " nome_exibicao = {$this->nomeExibicao} ";
                }


                
                if (isset($this->sistemaTabelaId)) 
                {                                      
                    $arrUpdateFields[] = " sistema_tabela_id_INT = {$this->sistemaTabelaId} ";
                }


                
                if (isset($this->tipoSql)) 
                {                                      
                    $arrUpdateFields[] = " tipo_sql = {$this->tipoSql} ";
                }


                
                if (isset($this->tipoSqlFicticio)) 
                {                                      
                    $arrUpdateFields[] = " tipo_sql_ficticio = {$this->tipoSqlFicticio} ";
                }


                
                if (isset($this->tamanhoInt)) 
                {                                      
                    $arrUpdateFields[] = " tamanho_INT = {$this->tamanhoInt} ";
                }


                
                if (isset($this->decimalInt)) 
                {                                      
                    $arrUpdateFields[] = " decimal_INT = {$this->decimalInt} ";
                }


                
                if (isset($this->notNullBoolean)) 
                {                                      
                    $arrUpdateFields[] = " not_null_BOOLEAN = {$this->notNullBoolean} ";
                }


                
                if (isset($this->primaryKeyBoolean)) 
                {                                      
                    $arrUpdateFields[] = " primary_key_BOOLEAN = {$this->primaryKeyBoolean} ";
                }


                
                if (isset($this->autoIncrementBoolean)) 
                {                                      
                    $arrUpdateFields[] = " auto_increment_BOOLEAN = {$this->autoIncrementBoolean} ";
                }


                
                if (isset($this->valorDefault)) 
                {                                      
                    $arrUpdateFields[] = " valor_default = {$this->valorDefault} ";
                }


                
                if (isset($this->fkSistemaTabelaId)) 
                {                                      
                    $arrUpdateFields[] = " fk_sistema_tabela_id_INT = {$this->fkSistemaTabelaId} ";
                }


                
                if (isset($this->atributoFk)) 
                {                                      
                    $arrUpdateFields[] = " atributo_fk = {$this->atributoFk} ";
                }


                
                if (isset($this->fkNome)) 
                {                                      
                    $arrUpdateFields[] = " fk_nome = {$this->fkNome} ";
                }


                
                if (isset($this->updateTipoFk)) 
                {                                      
                    $arrUpdateFields[] = " update_tipo_fk = {$this->updateTipoFk} ";
                }


                
                if (isset($this->deleteTipoFk)) 
                {                                      
                    $arrUpdateFields[] = " delete_tipo_fk = {$this->deleteTipoFk} ";
                }


                
                if (isset($this->label)) 
                {                                      
                    $arrUpdateFields[] = " label = {$this->label} ";
                }


                
                if (isset($this->uniqueBoolean)) 
                {                                      
                    $arrUpdateFields[] = " unique_BOOLEAN = {$this->uniqueBoolean} ";
                }


                
                if (isset($this->uniqueNome)) 
                {                                      
                    $arrUpdateFields[] = " unique_nome = {$this->uniqueNome} ";
                }


                
                if (isset($this->seqInt)) 
                {                                      
                    $arrUpdateFields[] = " seq_INT = {$this->seqInt} ";
                }


            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE sistema_atributo SET {$strUpdateFields} WHERE id = {$id}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function updateByInstanceUsingParameters($id, $parameters, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($parameters->nome)) 
                {                                      
                    $arrUpdateFields[] = " nome = {$this->nome} ";
                }
                
                if (isset($parameters->nomeExibicao)) 
                {                                      
                    $arrUpdateFields[] = " nome_exibicao = {$this->nomeExibicao} ";
                }
                
                if (isset($parameters->sistemaTabelaId)) 
                {                                      
                    $arrUpdateFields[] = " sistema_tabela_id_INT = {$this->sistemaTabelaId} ";
                }
                
                if (isset($parameters->tipoSql)) 
                {                                      
                    $arrUpdateFields[] = " tipo_sql = {$this->tipoSql} ";
                }
                
                if (isset($parameters->tipoSqlFicticio)) 
                {                                      
                    $arrUpdateFields[] = " tipo_sql_ficticio = {$this->tipoSqlFicticio} ";
                }
                
                if (isset($parameters->tamanhoInt)) 
                {                                      
                    $arrUpdateFields[] = " tamanho_INT = {$this->tamanhoInt} ";
                }
                
                if (isset($parameters->decimalInt)) 
                {                                      
                    $arrUpdateFields[] = " decimal_INT = {$this->decimalInt} ";
                }
                
                if (isset($parameters->notNullBoolean)) 
                {                                      
                    $arrUpdateFields[] = " not_null_BOOLEAN = {$this->notNullBoolean} ";
                }
                
                if (isset($parameters->primaryKeyBoolean)) 
                {                                      
                    $arrUpdateFields[] = " primary_key_BOOLEAN = {$this->primaryKeyBoolean} ";
                }
                
                if (isset($parameters->autoIncrementBoolean)) 
                {                                      
                    $arrUpdateFields[] = " auto_increment_BOOLEAN = {$this->autoIncrementBoolean} ";
                }
                
                if (isset($parameters->valorDefault)) 
                {                                      
                    $arrUpdateFields[] = " valor_default = {$this->valorDefault} ";
                }
                
                if (isset($parameters->fkSistemaTabelaId)) 
                {                                      
                    $arrUpdateFields[] = " fk_sistema_tabela_id_INT = {$this->fkSistemaTabelaId} ";
                }
                
                if (isset($parameters->atributoFk)) 
                {                                      
                    $arrUpdateFields[] = " atributo_fk = {$this->atributoFk} ";
                }
                
                if (isset($parameters->fkNome)) 
                {                                      
                    $arrUpdateFields[] = " fk_nome = {$this->fkNome} ";
                }
                
                if (isset($parameters->updateTipoFk)) 
                {                                      
                    $arrUpdateFields[] = " update_tipo_fk = {$this->updateTipoFk} ";
                }
                
                if (isset($parameters->deleteTipoFk)) 
                {                                      
                    $arrUpdateFields[] = " delete_tipo_fk = {$this->deleteTipoFk} ";
                }
                
                if (isset($parameters->label)) 
                {                                      
                    $arrUpdateFields[] = " label = {$this->label} ";
                }
                
                if (isset($parameters->uniqueBoolean)) 
                {                                      
                    $arrUpdateFields[] = " unique_BOOLEAN = {$this->uniqueBoolean} ";
                }
                
                if (isset($parameters->uniqueNome)) 
                {                                      
                    $arrUpdateFields[] = " unique_nome = {$this->uniqueNome} ";
                }
                
                if (isset($parameters->seqInt)) 
                {                                      
                    $arrUpdateFields[] = " seq_INT = {$this->seqInt} ";
                }
            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE sistema_atributo SET {$strUpdateFields} WHERE id = {$id}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }

}

?>
