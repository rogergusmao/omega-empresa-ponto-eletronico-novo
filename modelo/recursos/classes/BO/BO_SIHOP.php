<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BO_SIHOP
 *
 * @author home
 */
class BO_SIHOP
{

//http://127.0.0.1/hospedagem/10001/adm/actions.php?class=Servicos_web&action=consultaHospedagensDoUsuario&email=roger%40omegasoftware.com.br&sistema=1
    public static function consultaHospedagensDoUsuario($email, $idSistema)
    {

        $emailEncode = urlencode($email);
        $url = DOMINIO_DE_ACESSO_SERVICOS_SIHOP . "consultaHospedagensDoUsuario&sistema=$idSistema&email=$emailEncode";

        $json  = Helper::chamadaCurlJson($url);

        if (Interface_mensagem::checkOk($json)) {
            $mensagem = new Mensagem_vetor_protocolo(new Protocolo_dados_hospedagem());
            $mensagem->inicializa($json);
            return $mensagem;
        } else {
            return Mensagem::factoryJson($json);
        }
    }

    public static function getDominioDoSistema($idSistema)
    {
        //http://127.0.0.1/hospedagem/10001/adm/actions.php?class=Servicos_web&action=getDominioDoSistema&id_sistema=1
        $url = DOMINIO_DE_ACESSO_SERVICOS_SIHOP . "getDominioDoSistema&id_sistema=$idSistema";
        return Helper::chamadaCurlMensagemToken($url);
    }

    public static function loginCorporacao(
        $usuario,
        $corporacao,
        $senha,
        $idSistema)
    {
        $usuario = urlencode($usuario);
        $corporacao = urlencode($corporacao);
        $senha = urlencode($senha);
        $idSistema = urlencode($idSistema);
        $msgDominioCorporacao = BO_SICOB::getDominioDaCorporacao($corporacao);

        if ($msgDominioCorporacao->mCodRetorno == PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO) {
            $dominio = Helper::getPathComBarra($msgDominioCorporacao->mValor);
            header("location: {$dominio}public_html/adm/actions.php?class=Seguranca&action=login"
                ."&txtLogin=".$usuario
                ."&txtCorporacao=".$corporacao
                ."&txtSenha=".$senha);
            return null;
        } else {
            $msgErro = "";
            if ($msgDominioCorporacao->mCodRetorno == PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR) {
                $msgErro = urlencode("Não foi possível realizar o login, por favor tente novamente mais tarde.");
            } else {
                $msgErro = urlencode($msgDominioCorporacao->mValor);
            }
//            header("location: login.php&msgErro=$msgErro");
            Helper::mudarLocation(DOMINIO_DE_ACESSO_SICOB . "&msgErro=$msgErro");
            return null;
        }
        //http://127.0.0.1/hospedagem/10001/adm/actions.php?class=Servicos_web&action=getDominioDoSistema&id_sistema=1
        $url = DOMINIO_DE_ACESSO_SERVICOS_SIHOP . "getDominioDoSistema&sistema=$idSistema";
        return Helper::chamadaCurlMensagemToken($url);

    }


}
