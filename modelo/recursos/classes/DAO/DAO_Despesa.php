<?php


        /*

        Arquivo gerado através de gerador de código em 12/05/2018 as 14:44:57.
        Para que o arquivo não seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

        Tabela correspondente: despesa
        Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

        */
        


class DAO_Despesa extends Generic_DAO
                        {
                            const NOME_ENTIDADE = "despesa";

                            public static $nomeTabela = null;
                            public static $idSistemaTabela = false;
                            public static $databaseFieldsRelatedAttributes = null;
	                        public static $databaseFieldTypes = null;
	                        public static $databaseFieldNames = null;
	                        public static $listAliasTypes = null;
	                        public static $listAliasRelatedAttributes = null;
	                        
	                        public $id;
public $valorFloat;
public $empresaId;
public $vencimentoSec;
public $vencimentoOffsec;
public $pagamentoSec;
public $pagamentoOffsec;
public $despesaCotidianoId;
public $valorPagamentoFloat;
public $cadastroUsuarioId;
public $pagamentoUsuarioId;
public $protocoloInt;
public $corporacaoId;

public $labelId;
public $labelValorFloat;
public $labelEmpresaId;
public $labelVencimentoSec;
public $labelVencimentoOffsec;
public $labelPagamentoSec;
public $labelPagamentoOffsec;
public $labelDespesaCotidianoId;
public $labelValorPagamentoFloat;
public $labelCadastroUsuarioId;
public $labelPagamentoUsuarioId;
public $labelProtocoloInt;
public $labelCorporacaoId;

public static $arrPK = array("id");

public function __construct($configDAO = null)
        {
            parent::__construct($configDAO);
            static::__setStaticValues($this->database);           
        }

public static function idSistemaTabela($databaseObject = null){
            if(!(static::$idSistemaTabela))
            {
                static::$idSistemaTabela = EXTDAO_Sistema_tabela::getIdDaSistemaTabela(static::nomeTabela(), $databaseObject);
            }
            return static::$idSistemaTabela;
        }

public function getIdSistemaTabela(){
            return static::idSistemaTabela();
        }

public static function nomeTabela(){
            if(is_null(static::$nomeTabela))
            {
                static::$nomeTabela = "despesa";
            }
            return static::$nomeTabela;
        }

public function getNomeTabela(){
            return static::nomeTabela();
        }

public static function __setStaticValues($databaseObject = null)
                   {    
                        static::nomeTabela();
                        
                        static::idSistemaTabela($databaseObject);
                   
                        static::setDatabaseFieldNames();
                        static::setDatabaseFieldTypes();
                        static::setDatabaseFieldsRelatedAttributeNames();
                        
                        static::setListAliasTypes();
                        static::setListAliasRelatedAttributes();
                   }

public function getExpressaoLabel()
                   {
                        return "{id}";
                   }

public static function setDatabaseFieldNames()
        {
            if(is_null(static::$databaseFieldNames))
            {
                static::$databaseFieldNames = new stdClass();
                static::$databaseFieldNames->id = "id";
static::$databaseFieldNames->valorFloat = "valor_FLOAT";
static::$databaseFieldNames->empresaId = "empresa_id_INT";
static::$databaseFieldNames->vencimentoSec = "vencimento_SEC";
static::$databaseFieldNames->vencimentoOffsec = "vencimento_OFFSEC";
static::$databaseFieldNames->pagamentoSec = "pagamento_SEC";
static::$databaseFieldNames->pagamentoOffsec = "pagamento_OFFSEC";
static::$databaseFieldNames->despesaCotidianoId = "despesa_cotidiano_id_INT";
static::$databaseFieldNames->valorPagamentoFloat = "valor_pagamento_FLOAT";
static::$databaseFieldNames->cadastroUsuarioId = "cadastro_usuario_id_INT";
static::$databaseFieldNames->pagamentoUsuarioId = "pagamento_usuario_id_INT";
static::$databaseFieldNames->protocoloInt = "protocolo_INT";
static::$databaseFieldNames->corporacaoId = "corporacao_id_INT";

            }
                      
        }

public static function setDatabaseFieldTypes()
        {
            if(is_null(static::$databaseFieldTypes))
            {
                static::$databaseFieldTypes = new stdClass();
                static::$databaseFieldTypes->id = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->valorFloat = static::TIPO_VARIAVEL_FLOAT;
static::$databaseFieldTypes->empresaId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->vencimentoSec = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$databaseFieldTypes->vencimentoOffsec = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$databaseFieldTypes->pagamentoSec = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$databaseFieldTypes->pagamentoOffsec = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$databaseFieldTypes->despesaCotidianoId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->valorPagamentoFloat = static::TIPO_VARIAVEL_FLOAT;
static::$databaseFieldTypes->cadastroUsuarioId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->pagamentoUsuarioId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->protocoloInt = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->corporacaoId = static::TIPO_VARIAVEL_INTEGER;

            }
            
        }

public static function setDatabaseFieldsRelatedAttributeNames()
        {
            if(is_null(static::$databaseFieldsRelatedAttributes))
            {
                static::$databaseFieldsRelatedAttributes = new stdClass();
                static::$databaseFieldsRelatedAttributes->id = "id";
static::$databaseFieldsRelatedAttributes->valor_FLOAT = "valorFloat";
static::$databaseFieldsRelatedAttributes->empresa_id_INT = "empresaId";
static::$databaseFieldsRelatedAttributes->vencimento_SEC = "vencimentoSec";
static::$databaseFieldsRelatedAttributes->vencimento_OFFSEC = "vencimentoOffsec";
static::$databaseFieldsRelatedAttributes->pagamento_SEC = "pagamentoSec";
static::$databaseFieldsRelatedAttributes->pagamento_OFFSEC = "pagamentoOffsec";
static::$databaseFieldsRelatedAttributes->despesa_cotidiano_id_INT = "despesaCotidianoId";
static::$databaseFieldsRelatedAttributes->valor_pagamento_FLOAT = "valorPagamentoFloat";
static::$databaseFieldsRelatedAttributes->cadastro_usuario_id_INT = "cadastroUsuarioId";
static::$databaseFieldsRelatedAttributes->pagamento_usuario_id_INT = "pagamentoUsuarioId";
static::$databaseFieldsRelatedAttributes->protocolo_INT = "protocoloInt";
static::$databaseFieldsRelatedAttributes->corporacao_id_INT = "corporacaoId";

            }           
        }

public function setAllFieldsToHumanFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["valor_FLOAT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["empresa_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["vencimento_SEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["vencimento_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["pagamento_SEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["pagamento_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["despesa_cotidiano_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["valor_pagamento_FLOAT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["cadastro_usuario_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["pagamento_usuario_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["protocolo_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
}

public function setAllFieldsToDatabaseFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["valor_FLOAT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["empresa_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["vencimento_SEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["vencimento_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["pagamento_SEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["pagamento_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["despesa_cotidiano_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["valor_pagamento_FLOAT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["cadastro_usuario_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["pagamento_usuario_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["protocolo_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
}

  public function getFkObjEmpresa() 
                {
                    if($this->objEmpresa == null)
                    {                        
                        $this->objEmpresa = new EXTDAO_Empresa_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getEmpresa_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objEmpresa->clear();
                    }
                    elseif($this->objEmpresa->getId() != $idFK)
                    {
                        $this->objEmpresa->select($idFK);
                    }
                    return $this->objEmpresa;
                }
  public function getFkObjDespesa_cotidiano() 
                {
                    if($this->objDespesa_cotidiano == null)
                    {                        
                        $this->objDespesa_cotidiano = new EXTDAO_Despesa_cotidiano_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getDespesa_cotidiano_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objDespesa_cotidiano->clear();
                    }
                    elseif($this->objDespesa_cotidiano->getId() != $idFK)
                    {
                        $this->objDespesa_cotidiano->select($idFK);
                    }
                    return $this->objDespesa_cotidiano;
                }
  public function getFkObjCadastro_usuario() 
                {
                    if($this->objCadastro_usuario == null)
                    {                        
                        $this->objCadastro_usuario = new EXTDAO_Cadastro_usuario_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getCadastro_usuario_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objCadastro_usuario->clear();
                    }
                    elseif($this->objCadastro_usuario->getId() != $idFK)
                    {
                        $this->objCadastro_usuario->select($idFK);
                    }
                    return $this->objCadastro_usuario;
                }
  public function getFkObjPagamento_usuario() 
                {
                    if($this->objPagamento_usuario == null)
                    {                        
                        $this->objPagamento_usuario = new EXTDAO_Pagamento_usuario_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getPagamento_usuario_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objPagamento_usuario->clear();
                    }
                    elseif($this->objPagamento_usuario->getId() != $idFK)
                    {
                        $this->objPagamento_usuario->select($idFK);
                    }
                    return $this->objPagamento_usuario;
                }
  public function getFkObjCorporacao() 
                {
                    if($this->objCorporacao == null)
                    {                        
                        $this->objCorporacao = new EXTDAO_Corporacao_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getCorporacao_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objCorporacao->clear();
                    }
                    elseif($this->objCorporacao->getId() != $idFK)
                    {
                        $this->objCorporacao->select($idFK);
                    }
                    return $this->objCorporacao;
                }


public function setLabels()
        {$this->labelId = "";
$this->labelValorFloat = "";
$this->labelEmpresaId = "";
$this->labelVencimentoSec = "";
$this->labelVencimentoOffsec = "";
$this->labelPagamentoSec = "";
$this->labelPagamentoOffsec = "";
$this->labelDespesaCotidianoId = "";
$this->labelValorPagamentoFloat = "";
$this->labelCadastroUsuarioId = "";
$this->labelPagamentoUsuarioId = "";
$this->labelProtocoloInt = "";
$this->labelCorporacaoId = "";
}

public function __actionAdd($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                $this->setByObject($parameters);
                $this->setDataCadastroAndOffsetInSecondsFromUser($parameters);
                
                $this->formatarParaSQL();
                $msg = $this->insert(true);
                
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                return new Mensagem(null, I18N::getExpression("Despesa adicionado com sucesso."));
                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionEdit($parameters = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try {

                $this->setByObject($parameters);
                $this->setAllFieldsToHumanFormat();
                
                $this->formatarParaSQL();
                $msg = $this->updateByInstanceUsingParameters($this->id, $parameters, true);
                if($msg != null && $msg->erro())
                {
                     return $msg;
                }
        
                return new Mensagem(null, I18N::getExpression("Despesa editado com sucesso."));
            }
            catch(Exception $ex)
            {                
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionRemove($parameters = null)
        {
            $successulRemovalCount = 0;
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                foreach($parameters as $record)
                {
                    if(is_numeric($record->id))
                    {
                        $msg = $this->delete($record->id, true);
                        if($msg == null || $msg->ok())
                        {
                            $successulRemovalCount++;
                        }
                    }
                }
                
                if($successulRemovalCount > 0)
                {
                    $successMessage = count($successulRemovalCount) > 1 ? I18N::getExpression("{0} Despesa foram removidos com sucesso.", $successulRemovalCount) : I18N::getExpression("Despesa removido com sucesso.");
                    return new Mensagem(null, $successMessage);
                }
                else
                {
                    $errorMessage = count($parameters) > 1 ? I18N::getExpression("Falha ao remover Despesa.") : I18N::getExpression("Falha ao remover Despesa.");
                    return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, $errorMessage);                    
                }
            }
            catch (Exception $ex) 
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }

public function __getRecord($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }
            
            try
            {            
                $returnObject = new stdClass();            
                $whereClause = static::getWhereClauseForFilter($parameters->filterParameters);
                $query = "SELECT  id, valor_FLOAT, empresa_id_INT, vencimento_SEC, vencimento_OFFSEC, pagamento_SEC, pagamento_OFFSEC, despesa_cotidiano_id_INT, valor_pagamento_FLOAT, cadastro_usuario_id_INT, pagamento_usuario_id_INT, protocolo_INT, corporacao_id_INT FROM despesa {$whereClause}";
    
                $msg = $this->database->queryMensagem($query);                
                if ($msg == null)
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Não foi encontrado nenhum registro"));
                    }                    
                    $resultSet = $this->database->result;
                    $resultObject = static::mapResultSetDataToRecord($resultSet);
                    
                    $returnObject->formData = $resultObject;
                    $returnObject->comboBoxesData = null;
                    if($parameters->loadComboBoxesData)
                    {
                        $this->setByObject($resultObject);
                        $returnObject->comboBoxesData =  $this->getFormComboBoxesData();
                    }                    
    
                    
    
                    return new Mensagem_generica($returnObject);
                } 
                else 
                {
                    return $msg;
                }
                
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }


        
        public function __getFilterComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
                
        public function __getFormComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
        
        protected function getFormComboBoxesData()
        {
            $comboBoxesData = new stdClass();
            $listParameters = new stdClass();
            $listParameters->listMappingType = static::LIST_MAPPING_TYPE_COMBOBOX;
            
            
                    
                    $objEmpresa = new EXTDAO_Empresa();
                    $comboBoxesData->fieldEmpresaId = $objEmpresa->__getList($listParameters);
                    
                    $objDespesaCotidiano = new EXTDAO_Despesa_cotidiano();
                    $comboBoxesData->fieldDespesaCotidianoId = $objDespesaCotidiano->__getList($listParameters);

            return $comboBoxesData;
        }
        
        


        
        public static function setListAliasTypes()
        {
            if(is_null(static::$listAliasTypes))
            {
                static::$listAliasTypes = new stdClass();
                static::$listAliasTypes->empresa__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->despesa_cotidiano__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->despesa__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->despesa__valor_FLOAT = static::TIPO_VARIAVEL_FLOAT;
static::$listAliasTypes->despesa__ind_celular_valido_BOOLEAN = static::TIPO_VARIAVEL_BOOLEAN;
static::$listAliasTypes->despesa__vencimento_SEC = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$listAliasTypes->despesa__pagamento_SEC = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$listAliasTypes->despesa__corporacao_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->despesa__valor_pagamento_FLOAT = static::TIPO_VARIAVEL_FLOAT;
static::$listAliasTypes->despesa__cadastro_usuario_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->despesa__pagamento_usuario_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->despesa__protocolo_INT = static::TIPO_VARIAVEL_INTEGER;
            }        
        }
        
        public static function setListAliasRelatedAttributes()
        {
            if(is_null(static::$listAliasRelatedAttributes))
            {
                static::$listAliasRelatedAttributes = new stdClass();
                static::$listAliasRelatedAttributes->empresa__id = "empresaId";
static::$listAliasRelatedAttributes->despesa_cotidiano__id = "despesaCotidianoId";
static::$listAliasRelatedAttributes->despesa__id = "id";
static::$listAliasRelatedAttributes->despesa__valor_FLOAT = "valorFloat";
static::$listAliasRelatedAttributes->despesa__ind_celular_valido_BOOLEAN = "indCelularValidoBoolean";
static::$listAliasRelatedAttributes->despesa__vencimento_SEC = "vencimentoSec";
static::$listAliasRelatedAttributes->despesa__pagamento_SEC = "pagamentoSec";
static::$listAliasRelatedAttributes->despesa__corporacao_id_INT = "corporacaoId";
static::$listAliasRelatedAttributes->despesa__valor_pagamento_FLOAT = "valorPagamentoFloat";
static::$listAliasRelatedAttributes->despesa__cadastro_usuario_id_INT = "cadastroUsuarioId";
static::$listAliasRelatedAttributes->despesa__pagamento_usuario_id_INT = "pagamentoUsuarioId";
static::$listAliasRelatedAttributes->despesa__protocolo_INT = "protocoloInt";
            }         
        }

        public function __getList($parameters = null, $idCorporacao = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try
            {
                $filterParameters = new stdClass();
                if(isset($parameters->filterParameters))
                {
                    $filterParameters = $parameters->filterParameters;
                }
                                    
                            
            if(is_null($idCorporacao))
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            ListParameters::addFilterParameterToObject($filterParameters, "corporacaoId", $idCorporacao);           
                
                
                $sortingParameters = null;
                if(isset($parameters->sortingParameters))
                {
                    $sortingParameters = $parameters->sortingParameters;
                }
                
                $paginationParameters = null;
                if(isset($parameters->paginationParameters))
                {
                    $paginationParameters = $parameters->paginationParameters;
                }
                
                $listMappingType = static::getListMappingType($parameters->listMappingType);
                $mainTableAlias = "d";

                $whereClause = static::getWhereClauseForFilter($filterParameters, $mainTableAlias);
                $orderByClause = static::getOrderByClauseForFilter($sortingParameters, $mainTableAlias);
                $limitClause = static::getLimitClauseForFilter($paginationParameters);

                $queryWithoutLimit = "SELECT d.id FROM despesa d {$whereClause}";
                $query = "SELECT e.id AS empresa__id, dc.id AS despesa_cotidiano__id, d.id AS despesa__id, d.valor_FLOAT AS despesa__valor_FLOAT, d.ind_celular_valido_BOOLEAN AS despesa__ind_celular_valido_BOOLEAN, d.vencimento_SEC AS despesa__vencimento_SEC, d.pagamento_SEC AS despesa__pagamento_SEC, d.corporacao_id_INT AS despesa__corporacao_id_INT, d.valor_pagamento_FLOAT AS despesa__valor_pagamento_FLOAT, d.cadastro_usuario_id_INT AS despesa__cadastro_usuario_id_INT, d.pagamento_usuario_id_INT AS despesa__pagamento_usuario_id_INT, d.protocolo_INT AS despesa__protocolo_INT FROM despesa d LEFT JOIN empresa e ON e.id = d.empresa_id_INT LEFT JOIN despesa_cotidiano dc ON dc.id = d.despesa_cotidiano_id_INT {$whereClause} {$orderByClause} {$limitClause}";

                $msg = $this->database->queryMensagem($queryWithoutLimit);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                
                $totalNumberOfRecords = $this->database->rows();
                if($totalNumberOfRecords == 0)
                {
                    return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                }                        
                        
                $msg = $this->database->queryMensagem($query);
                
                //operacao realizaca com sucesso
                if ($msg == null) 
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                    }                        
                
                    $resultSet = $this->database->result;

                    $objReturn = new stdClass();
                    $objReturn->dataSet = static::mapResultSetDataToList($resultSet, $listMappingType, static::$listAliasTypes);

                    if(!Helper::isNullOrEmpty($paginationParameters->recordsPerPage) && !Helper::isNullOrEmpty($paginationParameters->currentPage))
                    {
                        $lastPage = ceil($totalNumberOfRecords / $paginationParameters->recordsPerPage);
                        $objReturn->paginationParameters = new PaginationParams($paginationParameters->recordsPerPage, $paginationParameters->currentPage, $lastPage);
                    }
                    
                    return new Mensagem_generica($objReturn);
                } 
                else
                {
                    return $msg;
                }                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }



public function getId()
            {
                return $this->id;
            }

public function getValorFloat()
            {
                return $this->valorFloat;
            }

public function getValor_FLOAT()
                {
                    return $this->valorFloat;
                }

public function getEmpresaId()
            {
                return $this->empresaId;
            }

public function getEmpresa_id_INT()
                {
                    return $this->empresaId;
                }

public function getVencimentoSec()
            {
                return $this->vencimentoSec;
            }

public function getVencimento_SEC()
                {
                    return $this->vencimentoSec;
                }

public function getVencimentoOffsec()
            {
                return $this->vencimentoOffsec;
            }

public function getVencimento_OFFSEC()
                {
                    return $this->vencimentoOffsec;
                }

public function getPagamentoSec()
            {
                return $this->pagamentoSec;
            }

public function getPagamento_SEC()
                {
                    return $this->pagamentoSec;
                }

public function getPagamentoOffsec()
            {
                return $this->pagamentoOffsec;
            }

public function getPagamento_OFFSEC()
                {
                    return $this->pagamentoOffsec;
                }

public function getDespesaCotidianoId()
            {
                return $this->despesaCotidianoId;
            }

public function getDespesa_cotidiano_id_INT()
                {
                    return $this->despesaCotidianoId;
                }

public function getValorPagamentoFloat()
            {
                return $this->valorPagamentoFloat;
            }

public function getValor_pagamento_FLOAT()
                {
                    return $this->valorPagamentoFloat;
                }

public function getCadastroUsuarioId()
            {
                return $this->cadastroUsuarioId;
            }

public function getCadastro_usuario_id_INT()
                {
                    return $this->cadastroUsuarioId;
                }

public function getPagamentoUsuarioId()
            {
                return $this->pagamentoUsuarioId;
            }

public function getPagamento_usuario_id_INT()
                {
                    return $this->pagamentoUsuarioId;
                }

public function getProtocoloInt()
            {
                return $this->protocoloInt;
            }

public function getProtocolo_INT()
                {
                    return $this->protocoloInt;
                }

public function getCorporacaoId()
            {
                return $this->corporacaoId;
            }

public function getCorporacao_id_INT()
                {
                    return $this->corporacaoId;
                }



function setId($value)
            {
                $this->id = $value;
            }

function setValorFloat($value)
            {
                $this->valorFloat = $value;
            }

public function setValor_FLOAT($value)
                { 
                    $this->valorFloat = $value; 
                }

function setEmpresaId($value)
            {
                $this->empresaId = $value;
            }

public function setEmpresa_id_INT($value)
                { 
                    $this->empresaId = $value; 
                }

function setVencimentoSec($value)
            {
                $this->vencimentoSec = $value;
            }

public function setVencimento_SEC($value)
                { 
                    $this->vencimentoSec = $value; 
                }

function setVencimentoOffsec($value)
            {
                $this->vencimentoOffsec = $value;
            }

public function setVencimento_OFFSEC($value)
                { 
                    $this->vencimentoOffsec = $value; 
                }

function setPagamentoSec($value)
            {
                $this->pagamentoSec = $value;
            }

public function setPagamento_SEC($value)
                { 
                    $this->pagamentoSec = $value; 
                }

function setPagamentoOffsec($value)
            {
                $this->pagamentoOffsec = $value;
            }

public function setPagamento_OFFSEC($value)
                { 
                    $this->pagamentoOffsec = $value; 
                }

function setDespesaCotidianoId($value)
            {
                $this->despesaCotidianoId = $value;
            }

public function setDespesa_cotidiano_id_INT($value)
                { 
                    $this->despesaCotidianoId = $value; 
                }

function setValorPagamentoFloat($value)
            {
                $this->valorPagamentoFloat = $value;
            }

public function setValor_pagamento_FLOAT($value)
                { 
                    $this->valorPagamentoFloat = $value; 
                }

function setCadastroUsuarioId($value)
            {
                $this->cadastroUsuarioId = $value;
            }

public function setCadastro_usuario_id_INT($value)
                { 
                    $this->cadastroUsuarioId = $value; 
                }

function setPagamentoUsuarioId($value)
            {
                $this->pagamentoUsuarioId = $value;
            }

public function setPagamento_usuario_id_INT($value)
                { 
                    $this->pagamentoUsuarioId = $value; 
                }

function setProtocoloInt($value)
            {
                $this->protocoloInt = $value;
            }

public function setProtocolo_INT($value)
                { 
                    $this->protocoloInt = $value; 
                }

function setCorporacaoId($value)
            {
                $this->corporacaoId = $value;
            }

public function setCorporacao_id_INT($value)
                { 
                    $this->corporacaoId = $value; 
                }



public function clear()
        {$this->id = null;
$this->valorFloat = null;
$this->empresaId = null;
if($this->objEmpresa != null) unset($this->objEmpresa);
$this->vencimentoSec = null;
$this->vencimentoOffsec = null;
$this->pagamentoSec = null;
$this->pagamentoOffsec = null;
$this->despesaCotidianoId = null;
if($this->objDespesa_cotidiano != null) unset($this->objDespesa_cotidiano);
$this->valorPagamentoFloat = null;
$this->cadastroUsuarioId = null;
if($this->objCadastro_usuario != null) unset($this->objCadastro_usuario);
$this->pagamentoUsuarioId = null;
if($this->objPagamento_usuario != null) unset($this->objPagamento_usuario);
$this->protocoloInt = null;
$this->corporacaoId = null;
if($this->objCorporacao != null) unset($this->objCorporacao);
if(is_array($this->arrEstadosCampos)){
                        unset($this->arrEstadosCampos);
                     $this->arrEstadosCampos = array();
                    }
                }

public function formatarParaSQL()
        {
if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->id)){
$this->id = $this->formatarIntegerParaComandoSQL($this->id);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->id);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->valorFloat)){
$this->valorFloat = $this->formatarFloatParaComandoSQL($this->valorFloat);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->valorFloat);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->empresaId)){
$this->empresaId = $this->formatarIntegerParaComandoSQL($this->empresaId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->empresaId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->vencimentoSec)){
$this->vencimentoSec = $this->formatarIntegerParaComandoSQL($this->vencimentoSec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->vencimentoSec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->vencimentoOffsec)){
$this->vencimentoOffsec = $this->formatarIntegerParaComandoSQL($this->vencimentoOffsec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->vencimentoOffsec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->pagamentoSec)){
$this->pagamentoSec = $this->formatarIntegerParaComandoSQL($this->pagamentoSec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->pagamentoSec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->pagamentoOffsec)){
$this->pagamentoOffsec = $this->formatarIntegerParaComandoSQL($this->pagamentoOffsec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->pagamentoOffsec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->despesaCotidianoId)){
$this->despesaCotidianoId = $this->formatarIntegerParaComandoSQL($this->despesaCotidianoId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->despesaCotidianoId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->valorPagamentoFloat)){
$this->valorPagamentoFloat = $this->formatarFloatParaComandoSQL($this->valorPagamentoFloat);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->valorPagamentoFloat);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->cadastroUsuarioId)){
$this->cadastroUsuarioId = $this->formatarIntegerParaComandoSQL($this->cadastroUsuarioId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->cadastroUsuarioId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->pagamentoUsuarioId)){
$this->pagamentoUsuarioId = $this->formatarIntegerParaComandoSQL($this->pagamentoUsuarioId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->pagamentoUsuarioId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->protocoloInt)){
$this->protocoloInt = $this->formatarIntegerParaComandoSQL($this->protocoloInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->protocoloInt);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->corporacaoId)){
$this->corporacaoId = $this->formatarIntegerParaComandoSQL($this->corporacaoId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->corporacaoId);
}

}

public function formatarParaExibicao()
        {if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->valorFloat)){
$this->valorFloat = $this->formatarFloatParaExibicao($this->valorFloat);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->valorFloat);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->valorPagamentoFloat)){
$this->valorPagamentoFloat = $this->formatarFloatParaExibicao($this->valorPagamentoFloat);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->valorPagamentoFloat);
}

}

function select($id, $idCorporacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }   

            $sql = "SELECT id, valor_FLOAT, empresa_id_INT, vencimento_SEC, vencimento_OFFSEC, pagamento_SEC, pagamento_OFFSEC, despesa_cotidiano_id_INT, valor_pagamento_FLOAT, cadastro_usuario_id_INT, pagamento_usuario_id_INT, protocolo_INT, corporacao_id_INT FROM despesa WHERE id = $id AND corporacao_id_INT = $idCorporacao";

            $msg = $this->database->queryMensagem($sql);
            $result = $this->database->result;

            if($msg != null && $msg->erro() )
            { 
            		$this->clear();
            		return $msg;
        	} 
        	else if($this->database->rows() == 0)
        	{
                return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nenhum registro foi encontrado."));
            }
        

            $row = $this->database->fetchArray(MYSQLI_NUM);
            
            		$this->id = $row[0];
		$this->valorFloat = $row[1];
		$this->empresaId = $row[2];
		$this->vencimentoSec = $row[3];
		$this->vencimentoOffsec = $row[4];
		$this->pagamentoSec = $row[5];
		$this->pagamentoOffsec = $row[6];
		$this->despesaCotidianoId = $row[7];
		$this->valorPagamentoFloat = $row[8];
		$this->cadastroUsuarioId = $row[9];
		$this->pagamentoUsuarioId = $row[10];
		$this->protocoloInt = $row[11];

            
        }

public function delete($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (is_null($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }

            $sql = "DELETE FROM despesa WHERE id= $id AND corporacao_id_INT = $idCorporacao";
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                    && $sincronizar
                        && static::idSistemaTabela() != null
                            && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_REMOVE,
                                null,
                                null,
                                $id,
                                $this->database);
            }

            return $msg;
        }

public function insert($sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null, $opcoes = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            
            
            if (is_null($opcoes))
            {
                $opcoes = $this->opcoesDAO;
            }

            if (!is_numeric($this->id))
            {
                $this->id = "null";
                if ($opcoes == null || $opcoes[Database::OPCAO_GERAR_ID] != false) 
                {
                    $this->id = EXTDAO_Sistema_sequencia::gerarId(static::nomeTabela());                    
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }    
                    else if($msg == null)
                    {
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
                else 
                {
                    $this->id = null;
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao));
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }
                    else if($msg == null)
                    {
                        $this->id = $this->database->getLastInsertId();
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
            }
            else 
            {
                $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }                        
                else if($msg == null)
                {
                   $msg = new Mensagem_token();
                   $msg->mValor = $this->id;
                }
            }
            if($msg == null || $msg->ok())
            {                
                if($sincronizar
                    && static::idSistemaTabela() != null
                    && isset($this->id)){
                    
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_INSERT,
                                null,
                                null,
                                $this->id,
                                $this->database); 
                	}
            } 
            else
            {
                $this->id = null;
            }
            
            return $msg;
            
        }

public function getSQLInsert($idCorporacao){ 

return "INSERT INTO despesa (id, valor_FLOAT, empresa_id_INT, vencimento_SEC, vencimento_OFFSEC, pagamento_SEC, pagamento_OFFSEC, despesa_cotidiano_id_INT, valor_pagamento_FLOAT, cadastro_usuario_id_INT, pagamento_usuario_id_INT, protocolo_INT, corporacao_id_INT) VALUES ( $this->id ,  $this->valorFloat ,  $this->empresaId ,  $this->vencimentoSec ,  $this->vencimentoOffsec ,  $this->pagamentoSec ,  $this->pagamentoOffsec ,  $this->despesaCotidianoId ,  $this->valorPagamentoFloat ,  $this->cadastroUsuarioId ,  $this->pagamentoUsuarioId ,  $this->protocoloInt ,  $idCorporacao ) "; 

}




        
        public function updateByParameters($id, $objParametros, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao))
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();

            
                
                if (isset($objParametros->valorFloat)) 
                {
                    $arrUpdateFields[] = " valor_FLOAT = {$objParametros->valorFloat} ";
                }


                
                if (isset($objParametros->empresaId)) 
                {
                    $arrUpdateFields[] = " empresa_id_INT = {$objParametros->empresaId} ";
                }


                
                if (isset($objParametros->vencimentoSec)) 
                {
                    $arrUpdateFields[] = " vencimento_SEC = {$objParametros->vencimentoSec} ";
                }


                
                if (isset($objParametros->vencimentoOffsec)) 
                {
                    $arrUpdateFields[] = " vencimento_OFFSEC = {$objParametros->vencimentoOffsec} ";
                }


                
                if (isset($objParametros->pagamentoSec)) 
                {
                    $arrUpdateFields[] = " pagamento_SEC = {$objParametros->pagamentoSec} ";
                }


                
                if (isset($objParametros->pagamentoOffsec)) 
                {
                    $arrUpdateFields[] = " pagamento_OFFSEC = {$objParametros->pagamentoOffsec} ";
                }


                
                if (isset($objParametros->despesaCotidianoId)) 
                {
                    $arrUpdateFields[] = " despesa_cotidiano_id_INT = {$objParametros->despesaCotidianoId} ";
                }


                
                if (isset($objParametros->valorPagamentoFloat)) 
                {
                    $arrUpdateFields[] = " valor_pagamento_FLOAT = {$objParametros->valorPagamentoFloat} ";
                }


                
                if (isset($objParametros->cadastroUsuarioId)) 
                {
                    $arrUpdateFields[] = " cadastro_usuario_id_INT = {$objParametros->cadastroUsuarioId} ";
                }


                
                if (isset($objParametros->pagamentoUsuarioId)) 
                {
                    $arrUpdateFields[] = " pagamento_usuario_id_INT = {$objParametros->pagamentoUsuarioId} ";
                }


                
                if (isset($objParametros->protocoloInt)) 
                {
                    $arrUpdateFields[] = " protocolo_INT = {$objParametros->protocoloInt} ";
                }



            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE despesa SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";

            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null)
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function update($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($this->valorFloat)) 
                {                                      
                    $arrUpdateFields[] = " valor_FLOAT = {$this->valorFloat} ";
                }


                
                if (isset($this->empresaId)) 
                {                                      
                    $arrUpdateFields[] = " empresa_id_INT = {$this->empresaId} ";
                }


                
                if (isset($this->vencimentoSec)) 
                {                                      
                    $arrUpdateFields[] = " vencimento_SEC = {$this->vencimentoSec} ";
                }


                
                if (isset($this->vencimentoOffsec)) 
                {                                      
                    $arrUpdateFields[] = " vencimento_OFFSEC = {$this->vencimentoOffsec} ";
                }


                
                if (isset($this->pagamentoSec)) 
                {                                      
                    $arrUpdateFields[] = " pagamento_SEC = {$this->pagamentoSec} ";
                }


                
                if (isset($this->pagamentoOffsec)) 
                {                                      
                    $arrUpdateFields[] = " pagamento_OFFSEC = {$this->pagamentoOffsec} ";
                }


                
                if (isset($this->despesaCotidianoId)) 
                {                                      
                    $arrUpdateFields[] = " despesa_cotidiano_id_INT = {$this->despesaCotidianoId} ";
                }


                
                if (isset($this->valorPagamentoFloat)) 
                {                                      
                    $arrUpdateFields[] = " valor_pagamento_FLOAT = {$this->valorPagamentoFloat} ";
                }


                
                if (isset($this->cadastroUsuarioId)) 
                {                                      
                    $arrUpdateFields[] = " cadastro_usuario_id_INT = {$this->cadastroUsuarioId} ";
                }


                
                if (isset($this->pagamentoUsuarioId)) 
                {                                      
                    $arrUpdateFields[] = " pagamento_usuario_id_INT = {$this->pagamentoUsuarioId} ";
                }


                
                if (isset($this->protocoloInt)) 
                {                                      
                    $arrUpdateFields[] = " protocolo_INT = {$this->protocoloInt} ";
                }


            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE despesa SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function updateByInstanceUsingParameters($id, $parameters, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($parameters->valorFloat)) 
                {                                      
                    $arrUpdateFields[] = " valor_FLOAT = {$this->valorFloat} ";
                }
                
                if (isset($parameters->empresaId)) 
                {                                      
                    $arrUpdateFields[] = " empresa_id_INT = {$this->empresaId} ";
                }
                
                if (isset($parameters->vencimentoSec)) 
                {                                      
                    $arrUpdateFields[] = " vencimento_SEC = {$this->vencimentoSec} ";
                }
                
                if (isset($parameters->vencimentoOffsec)) 
                {                                      
                    $arrUpdateFields[] = " vencimento_OFFSEC = {$this->vencimentoOffsec} ";
                }
                
                if (isset($parameters->pagamentoSec)) 
                {                                      
                    $arrUpdateFields[] = " pagamento_SEC = {$this->pagamentoSec} ";
                }
                
                if (isset($parameters->pagamentoOffsec)) 
                {                                      
                    $arrUpdateFields[] = " pagamento_OFFSEC = {$this->pagamentoOffsec} ";
                }
                
                if (isset($parameters->despesaCotidianoId)) 
                {                                      
                    $arrUpdateFields[] = " despesa_cotidiano_id_INT = {$this->despesaCotidianoId} ";
                }
                
                if (isset($parameters->valorPagamentoFloat)) 
                {                                      
                    $arrUpdateFields[] = " valor_pagamento_FLOAT = {$this->valorPagamentoFloat} ";
                }
                
                if (isset($parameters->cadastroUsuarioId)) 
                {                                      
                    $arrUpdateFields[] = " cadastro_usuario_id_INT = {$this->cadastroUsuarioId} ";
                }
                
                if (isset($parameters->pagamentoUsuarioId)) 
                {                                      
                    $arrUpdateFields[] = " pagamento_usuario_id_INT = {$this->pagamentoUsuarioId} ";
                }
                
                if (isset($parameters->protocoloInt)) 
                {                                      
                    $arrUpdateFields[] = " protocolo_INT = {$this->protocoloInt} ";
                }
            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE despesa SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }

}

?>
