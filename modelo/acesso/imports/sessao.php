<?php

session_name(IDENTIFICADOR_SESSAO_CLIENTE);
session_start();

if (!isset($_SESSION["cliente_id"])) {

    $pagina = $_GET["page"];

    if (!$pagina) {

        $pagina = "";

    }

    if (in_array($pagina, Seguranca_Pagamento::$paginasComAutenticacao)) {

        $objSeguranca = new Seguranca_Pagamento();

        if (isset($_COOKIE["cliente"]) && is_numeric($_COOKIE["cliente"])) {

            $objSeguranca->efetuarLogout(MENSAGEM_EXPIRACAO_SESSAO);

        } else {

            $objSeguranca->efetuarLogout("Faça o Login para continuar.");

        }

    }

    $nomeDoScript = Helper::getNomeDoScriptAtual();

    if ($nomeDoScript == "actions.php") {

        $class = $_POST["class"];
        $action = $_POST["action"];

        if (!strlen($action)) {

            $class = $_GET["class"];
            $action = $_GET["action"];

        }

        if (!strlen($action)) {

            Helper::imprimirMensagem("Ação inválida!", MENSAGEM_ERRO);

        } elseif (in_array($action, Seguranca_Pagamento::$actionsComAutenticacao)) {

            header("location: index.php?tipo=paginas&page=login&next_action={$class}::{$action}");

        }

    }

}

//debugmode
error_reporting(E_ALL & ~(E_WARNING | E_NOTICE | E_DEPRECATED));
//set_error_handler(array("EXTDAO_Erro", "errorHandler"));
date_default_timezone_set("Brazil/East");

?>
