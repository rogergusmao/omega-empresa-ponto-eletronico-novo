"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var React = require("react");
var react_bootstrap_1 = require("react-bootstrap");
var uiComponente_1 = require("lib/uiComponente");
var PainelFlutuante = (function (_super) {
    __extends(PainelFlutuante, _super);
    function PainelFlutuante(props, reactContext) {
        var _this = _super.call(this, props || {}, reactContext) || this;
        _this.handleShow = _this.handleShow.bind(_this);
        _this.handleClose = _this.handleClose.bind(_this);
        _this.state = { show: false };
        return _this;
    }
    PainelFlutuante.prototype.setDefaultHeader = function () {
    };
    PainelFlutuante.prototype.setDefaultBody = function () {
    };
    PainelFlutuante.prototype.handleClose = function () {
        this.setState({ show: false });
    };
    PainelFlutuante.prototype.handleShow = function () {
        this.setState({ show: true });
    };
    PainelFlutuante.prototype.render = function () {
        if (this.props.setDefaultBody) {
            this.setDefaultBody();
        }
        if (this.props.setDefaultHeader) {
            this.setDefaultHeader();
        }
        return (<react_bootstrap_1.Modal show={this.state.show} onHide={this.handleClose}>
                <react_bootstrap_1.Modal.Header closeButton>
                    {this.headerContent}
                </react_bootstrap_1.Modal.Header>
                <react_bootstrap_1.Modal.Body>
                    {this.bodyContent}
                </react_bootstrap_1.Modal.Body>
                <react_bootstrap_1.Modal.Footer>
                    {this.footerContent}
                </react_bootstrap_1.Modal.Footer>
            </react_bootstrap_1.Modal>);
    };
    return PainelFlutuante;
}(uiComponente_1.ReactComponent));
exports.PainelFlutuante = PainelFlutuante;
