"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t;
    return { next: verb(0), "throw": verb(1), "return": verb(2) };
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
var React = require("react");
var contexto_1 = require("lib/contexto");
var dataService_1 = require("aplicacao/comum/dataService");
var uiComponente_1 = require("aplicacao/modulo/mapa/uiComponente");
var model_1 = require("aplicacao/modulo/mapa/model");
var MapaIntegrado = (function (_super) {
    __extends(MapaIntegrado, _super);
    function MapaIntegrado(domRootSelector, mapContainerSelector) {
        var _this = 
        //chama o construtor do pai
        _super.call(this, domRootSelector, mapContainerSelector) || this;
        //instancia o data service
        _this.pontoEletronicoDataService = new dataService_1.PontoEletronicoServices();
        //corrige layout da página para aplicar o mapa
        _this.fixPageLayout();
        //adiciona componentes no layout
        _this.addComponents();
        //inicializa mapa
        _this.initializeMap();
        //carrega dados iniciais
        _this.loadInitialDataFromService();
        return _this;
    }
    MapaIntegrado.contextFactory = function () {
        $(document).ready(function () {
            var aplicacaoContext = new MapaIntegrado('section#content', '#main-map-container');
            MapaIntegrado.appContext = aplicacaoContext;
        });
    };
    MapaIntegrado.prototype.fixPageLayout = function () {
        //corrige o layout no contexto específico da aplicação
        $('#wrapper').css('min-height', 'auto');
        $('#content > .container').css('padding-left', '0px');
        $('.internal-page-container').css('padding-left', '0px');
        $('.internal-page-container').css('padding-right', '0px');
    };
    MapaIntegrado.prototype.getPathToImageFolder = function () {
        return this.getPathToApplicationRoot() + "adm/imgs/";
    };
    MapaIntegrado.prototype.getDefaultMarkerFolder = function () {
        return this.getPathToImageFolder() + "mapa/markers/";
    };
    MapaIntegrado.prototype.addComponents = function () {
        var barraInferiorJXS = <uiComponente_1.BarraInferior onTemaChange={this.onTemaChange.bind(this)} onElementosVisiveisChange={this.onElementosVisiveisChange.bind(this)}/>;
        this.renderReactComponent(barraInferiorJXS, '#barra-inferior-container');
    };
    MapaIntegrado.prototype.onTemaChange = function (template) {
        this.mapObject.setTemplate(template);
    };
    MapaIntegrado.prototype.onElementosVisiveisChange = function (elementosVisiveis) {
        this.mapObject.setTiposElementosVisiveis(elementosVisiveis);
    };
    MapaIntegrado.prototype.loadInitialDataFromService = function () {
        return __awaiter(this, void 0, void 0, function () {
            var dadosIniciais, _i, _a, pontoEletronico, pontoMapa, _b, _c, usuario, usuarioMapa, wifiNetworks, _d, _e, wifiNetwork, _f, _g, empresa, empresaModel, empresaMapa, ex_1;
            return __generator(this, function (_h) {
                switch (_h.label) {
                    case 0:
                        _h.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.pontoEletronicoDataService.getDadosIniciaisDoMapa()];
                    case 1:
                        dadosIniciais = _h.sent();
                        if (dadosIniciais != null) {
                            if (dadosIniciais.pontos != null) {
                                for (_i = 0, _a = dadosIniciais.pontos; _i < _a.length; _i++) {
                                    pontoEletronico = _a[_i];
                                    pontoMapa = new uiComponente_1.PontoEletronico(this, model_1.PontoEletronico.factory(pontoEletronico));
                                    pontoMapa.render();
                                    pontoMapa.addDefaultClickListener();
                                }
                            }
                            if (dadosIniciais.usuarios != null) {
                                for (_b = 0, _c = dadosIniciais.usuarios; _b < _c.length; _b++) {
                                    usuario = _c[_b];
                                    usuarioMapa = new uiComponente_1.Usuario(this, model_1.Usuario.parseFromRemoteObject(usuario));
                                    usuarioMapa.render();
                                    usuarioMapa.addDefaultClickListener();
                                }
                            }
                            wifiNetworks = new Array();
                            if (dadosIniciais.wifi != null) {
                                for (_d = 0, _e = dadosIniciais.wifi; _d < _e.length; _d++) {
                                    wifiNetwork = _e[_d];
                                    wifiNetworks.push(model_1.WifiNetwork.parseFromRemoteObject(wifiNetwork));
                                }
                            }
                            if (dadosIniciais.empresas != null) {
                                for (_f = 0, _g = dadosIniciais.empresas; _f < _g.length; _f++) {
                                    empresa = _g[_f];
                                    empresaModel = model_1.Empresa.parseFromRemoteObject(empresa);
                                    empresaModel.setValidWifiNetworks(wifiNetworks);
                                    empresaMapa = new uiComponente_1.Empresa(this, empresaModel);
                                    empresaMapa.render();
                                    empresaMapa.addDefaultClickListener();
                                }
                            }
                            this.mapObject.fixMainMapSize();
                            this.mapObject.fitBounds();
                        }
                        return [3 /*break*/, 3];
                    case 2:
                        ex_1 = _h.sent();
                        console.log(ex_1);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    MapaIntegrado.prototype.getPontosEletronicosFromService = function () {
        return __awaiter(this, void 0, void 0, function () {
            var arrPontosEletronicos, _i, arrPontosEletronicos_1, pontoEletronico;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.pontoEletronicoDataService.getPontosEletronicos()];
                    case 1:
                        arrPontosEletronicos = _a.sent();
                        for (_i = 0, arrPontosEletronicos_1 = arrPontosEletronicos; _i < arrPontosEletronicos_1.length; _i++) {
                            pontoEletronico = arrPontosEletronicos_1[_i];
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    MapaIntegrado.prototype.getUsuariosFromService = function () {
        return __awaiter(this, void 0, void 0, function () {
            var arrUsuarios, _i, arrUsuarios_1, usuario;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.pontoEletronicoDataService.getUsuarios()];
                    case 1:
                        arrUsuarios = _a.sent();
                        for (_i = 0, arrUsuarios_1 = arrUsuarios; _i < arrUsuarios_1.length; _i++) {
                            usuario = arrUsuarios_1[_i];
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    MapaIntegrado.prototype.getEmpresasFromService = function () {
        return __awaiter(this, void 0, void 0, function () {
            var arrEmpresas, _i, arrEmpresas_1, empresa;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.pontoEletronicoDataService.getEmpresas()];
                    case 1:
                        arrEmpresas = _a.sent();
                        for (_i = 0, arrEmpresas_1 = arrEmpresas; _i < arrEmpresas_1.length; _i++) {
                            empresa = arrEmpresas_1[_i];
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    MapaIntegrado.prototype.getWifiNetworksFromService = function () {
        return __awaiter(this, void 0, void 0, function () {
            var arrEmpresas, _i, arrEmpresas_2, empresa;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.pontoEletronicoDataService.getEmpresas()];
                    case 1:
                        arrEmpresas = _a.sent();
                        for (_i = 0, arrEmpresas_2 = arrEmpresas; _i < arrEmpresas_2.length; _i++) {
                            empresa = arrEmpresas_2[_i];
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    return MapaIntegrado;
}(contexto_1.ContextoMapa));
exports.MapaIntegrado = MapaIntegrado;
(function (MapaIntegrado) {
})(MapaIntegrado = exports.MapaIntegrado || (exports.MapaIntegrado = {}));
exports.MapaIntegrado = MapaIntegrado;
