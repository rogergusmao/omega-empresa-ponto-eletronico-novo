<?php

/*
*
* -------------------------------------------------------
* NOME DO FORMUL�RIO: setor_tipo_empresa
* DATA DE GERA��O:    07.02.2012
* ARQUIVO:            setor_tipo_empresa.php
* TABELA MYSQL:       setor_tipo_empresa
* BANCO DE DADOS:     ponto_eletronico
* -------------------------------------------------------
*
*/

if (isset($_GET["contador"]))
{

    $numeroRegistroInterno = Helper::GET("contador");
}

if (isset($identificadorRelacionamento) && is_numeric($identificadorRelacionamento))
{

    $objSetor_tipo_empresa = new EXTDAO_Setor_tipo_empresa();
    $objSetor_tipo_empresa->select($identificadorRelacionamento);
}
else
{

    $objSetor_tipo_empresa = new EXTDAO_Setor_tipo_empresa();
}

$objArgSetor_tipo_empresa = new Generic_Argument();
$objSetor_tipo_empresa->formatarParaExibicao();

?>

<input type="hidden" name="setor_tipo_empresa_id_<?= $numeroRegistroInterno ?>"
       id="setor_tipo_empresa_id_<?= $numeroRegistroInterno ?>" value="<?= $objSetor_tipo_empresa->getId(); ?>">

<table class="tabela_form">

    <tr class="tr_form">


        <?

        $objArgSetor_tipo_empresa = new Generic_Argument();

        $objArgSetor_tipo_empresa->numeroDoRegistro = "";
        $objArgSetor_tipo_empresa->label = $objSetor_tipo_empresa->label_setor_id_INT;
        $objArgSetor_tipo_empresa->valor = $objSetor_tipo_empresa->getSetor_id_INT();
        $objArgSetor_tipo_empresa->classeCss = "input_text";
        $objArgSetor_tipo_empresa->classeCssFocus = "focus_text";
        $objArgSetor_tipo_empresa->obrigatorio = false;
        $objArgSetor_tipo_empresa->largura = 200;
        $objArgSetor_tipo_empresa->nome = "setor_tipo_empresa_setor_id_INT_{$numeroRegistroInterno}";
        $objArgSetor_tipo_empresa->id = "setor_tipo_empresa_setor_id_INT_{$numeroRegistroInterno}";

        $objSetor_tipo_empresa->addInfoCampos("setor_id_INT", $objArgSetor_tipo_empresa->label, "TEXTO", $objArgSetor_tipo_empresa->obrigatorio);

        ?>

        <td class="td_form_label"><?= $objArgSetor_tipo_empresa->getLabel() ?></td>
        <td class="td_form_campo">
            <?= $objSetor_tipo_empresa->objSetor->getComboBox($objArgSetor_tipo_empresa); ?>
        </td>


        <td class="td_form_label"></td>
        <td class="td_form_campo"></td>
    </tr>
    <tr>
        <td colspan="4" class="td_botao_remover_da_lista"><input class="botoes_form" type="button"
                                                                 value="Remover Setor Do Ramo Da Empresa"
                                                                 onclick="javascript:removerDivAjaxEmLista(this);"></td>
    </tr>
</table><br/>

