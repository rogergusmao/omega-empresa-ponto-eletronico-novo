<?php


        /*

        Arquivo gerado através de gerador de código em 12/05/2018 as 14:53:08.
        Para que o arquivo não seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

        Tabela correspondente: usuario_tipo_menu
        Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

        */
        


class DAO_Usuario_tipo_menu extends Generic_DAO
                        {
                            const NOME_ENTIDADE = "usuario_tipo_menu";

                            public static $nomeTabela = null;
                            public static $idSistemaTabela = false;
                            public static $databaseFieldsRelatedAttributes = null;
	                        public static $databaseFieldTypes = null;
	                        public static $databaseFieldNames = null;
	                        public static $listAliasTypes = null;
	                        public static $listAliasRelatedAttributes = null;
	                        
	                        public $id;
public $usuarioTipoId;
public $areaMenu;
public $areaMenuId;
public $corporacaoId;

public $labelId;
public $labelUsuarioTipoId;
public $labelAreaMenu;
public $labelAreaMenuId;
public $labelCorporacaoId;

public static $arrPK = array("id");

public function __construct($configDAO = null)
        {
            parent::__construct($configDAO);
            static::__setStaticValues($this->database);           
        }

public static function idSistemaTabela($databaseObject = null){
            if(!(static::$idSistemaTabela))
            {
                static::$idSistemaTabela = EXTDAO_Sistema_tabela::getIdDaSistemaTabela(static::nomeTabela(), $databaseObject);
            }
            return static::$idSistemaTabela;
        }

public function getIdSistemaTabela(){
            return static::idSistemaTabela();
        }

public static function nomeTabela(){
            if(is_null(static::$nomeTabela))
            {
                static::$nomeTabela = "usuario_tipo_menu";
            }
            return static::$nomeTabela;
        }

public function getNomeTabela(){
            return static::nomeTabela();
        }

public static function __setStaticValues($databaseObject = null)
                   {    
                        static::nomeTabela();
                        
                        static::idSistemaTabela($databaseObject);
                   
                        static::setDatabaseFieldNames();
                        static::setDatabaseFieldTypes();
                        static::setDatabaseFieldsRelatedAttributeNames();
                        
                        static::setListAliasTypes();
                        static::setListAliasRelatedAttributes();
                   }

public function getExpressaoLabel()
                   {
                        return "{id}";
                   }

public static function setDatabaseFieldNames()
        {
            if(is_null(static::$databaseFieldNames))
            {
                static::$databaseFieldNames = new stdClass();
                static::$databaseFieldNames->id = "id";
static::$databaseFieldNames->usuarioTipoId = "usuario_tipo_id_INT";
static::$databaseFieldNames->areaMenu = "area_menu";
static::$databaseFieldNames->areaMenuId = "area_menu_id_INT";
static::$databaseFieldNames->corporacaoId = "corporacao_id_INT";

            }
                      
        }

public static function setDatabaseFieldTypes()
        {
            if(is_null(static::$databaseFieldTypes))
            {
                static::$databaseFieldTypes = new stdClass();
                static::$databaseFieldTypes->id = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->usuarioTipoId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->areaMenu = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->areaMenuId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->corporacaoId = static::TIPO_VARIAVEL_INTEGER;

            }
            
        }

public static function setDatabaseFieldsRelatedAttributeNames()
        {
            if(is_null(static::$databaseFieldsRelatedAttributes))
            {
                static::$databaseFieldsRelatedAttributes = new stdClass();
                static::$databaseFieldsRelatedAttributes->id = "id";
static::$databaseFieldsRelatedAttributes->usuario_tipo_id_INT = "usuarioTipoId";
static::$databaseFieldsRelatedAttributes->area_menu = "areaMenu";
static::$databaseFieldsRelatedAttributes->area_menu_id_INT = "areaMenuId";
static::$databaseFieldsRelatedAttributes->corporacao_id_INT = "corporacaoId";

            }           
        }

public function setAllFieldsToHumanFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["usuario_tipo_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["area_menu"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["area_menu_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
}

public function setAllFieldsToDatabaseFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["usuario_tipo_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["area_menu"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["area_menu_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
}

  public function getFkObjUsuario_tipo() 
                {
                    if($this->objUsuario_tipo == null)
                    {                        
                        $this->objUsuario_tipo = new EXTDAO_Usuario_tipo_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getUsuario_tipo_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objUsuario_tipo->clear();
                    }
                    elseif($this->objUsuario_tipo->getId() != $idFK)
                    {
                        $this->objUsuario_tipo->select($idFK);
                    }
                    return $this->objUsuario_tipo;
                }
  public function getFkObjArea_menu() 
                {
                    if($this->objArea_menu == null)
                    {                        
                        $this->objArea_menu = new EXTDAO_Area_menu_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getArea_menu_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objArea_menu->clear();
                    }
                    elseif($this->objArea_menu->getId() != $idFK)
                    {
                        $this->objArea_menu->select($idFK);
                    }
                    return $this->objArea_menu;
                }
  public function getFkObjCorporacao() 
                {
                    if($this->objCorporacao == null)
                    {                        
                        $this->objCorporacao = new EXTDAO_Corporacao_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getCorporacao_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objCorporacao->clear();
                    }
                    elseif($this->objCorporacao->getId() != $idFK)
                    {
                        $this->objCorporacao->select($idFK);
                    }
                    return $this->objCorporacao;
                }


public function setLabels()
        {$this->labelId = "id";
$this->labelUsuarioTipoId = "usuariotipoidINT";
$this->labelAreaMenu = "areamenu";
$this->labelAreaMenuId = "areamenuidINT";
$this->labelCorporacaoId = "corporacaoidINT";
}

public function __actionAdd($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                $this->setByObject($parameters);
                $this->setDataCadastroAndOffsetInSecondsFromUser($parameters);
                
                $this->formatarParaSQL();
                $msg = $this->insert(true);
                
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                return new Mensagem(null, I18N::getExpression("Usuario tipo menu adicionado com sucesso."));
                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionEdit($parameters = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try {

                $this->setByObject($parameters);
                $this->setAllFieldsToHumanFormat();
                
                $this->formatarParaSQL();
                $msg = $this->updateByInstanceUsingParameters($this->id, $parameters, true);
                if($msg != null && $msg->erro())
                {
                     return $msg;
                }
        
                return new Mensagem(null, I18N::getExpression("Usuario tipo menu editado com sucesso."));
            }
            catch(Exception $ex)
            {                
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionRemove($parameters = null)
        {
            $successulRemovalCount = 0;
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                foreach($parameters as $record)
                {
                    if(is_numeric($record->id))
                    {
                        $msg = $this->delete($record->id, true);
                        if($msg == null || $msg->ok())
                        {
                            $successulRemovalCount++;
                        }
                    }
                }
                
                if($successulRemovalCount > 0)
                {
                    $successMessage = count($successulRemovalCount) > 1 ? I18N::getExpression("{0} Usuario tipo menu foram removidos com sucesso.", $successulRemovalCount) : I18N::getExpression("Usuario tipo menu removido com sucesso.");
                    return new Mensagem(null, $successMessage);
                }
                else
                {
                    $errorMessage = count($parameters) > 1 ? I18N::getExpression("Falha ao remover Usuario tipo menu.") : I18N::getExpression("Falha ao remover Usuario tipo menu.");
                    return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, $errorMessage);                    
                }
            }
            catch (Exception $ex) 
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }

public function __getRecord($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }
            
            try
            {            
                $returnObject = new stdClass();            
                $whereClause = static::getWhereClauseForFilter($parameters->filterParameters);
                $query = "SELECT  id, usuario_tipo_id_INT, area_menu, area_menu_id_INT, corporacao_id_INT FROM usuario_tipo_menu {$whereClause}";
    
                $msg = $this->database->queryMensagem($query);                
                if ($msg == null)
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Não foi encontrado nenhum registro"));
                    }                    
                    $resultSet = $this->database->result;
                    $resultObject = static::mapResultSetDataToRecord($resultSet);
                    
                    $returnObject->formData = $resultObject;
                    $returnObject->comboBoxesData = null;
                    if($parameters->loadComboBoxesData)
                    {
                        $this->setByObject($resultObject);
                        $returnObject->comboBoxesData =  $this->getFormComboBoxesData();
                    }                    
    
                    
    
                    return new Mensagem_generica($returnObject);
                } 
                else 
                {
                    return $msg;
                }
                
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }


        
        public function __getFilterComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
                
        public function __getFormComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
        
        protected function getFormComboBoxesData()
        {
            $comboBoxesData = new stdClass();
            $listParameters = new stdClass();
            $listParameters->listMappingType = static::LIST_MAPPING_TYPE_COMBOBOX;
            
            
                    
                    $objUsuarioTipo = new EXTDAO_Usuario_tipo();
                    $comboBoxesData->fieldUsuarioTipoId = $objUsuarioTipo->__getList($listParameters);
                    
                    $objAreaMenu = new EXTDAO_Area_menu();
                    $comboBoxesData->fieldAreaMenuId = $objAreaMenu->__getList($listParameters);

            return $comboBoxesData;
        }
        
        


        
        public static function setListAliasTypes()
        {
            if(is_null(static::$listAliasTypes))
            {
                static::$listAliasTypes = new stdClass();
                static::$listAliasTypes->usuario_tipo__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->area_menu__nome = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->usuario_tipo_menu__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->usuario_tipo_menu__corporacao_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->usuario_tipo_menu__area_menu = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->usuario_tipo_menu__nome = static::TIPO_VARIAVEL_TEXT;
            }        
        }
        
        public static function setListAliasRelatedAttributes()
        {
            if(is_null(static::$listAliasRelatedAttributes))
            {
                static::$listAliasRelatedAttributes = new stdClass();
                static::$listAliasRelatedAttributes->usuario_tipo__id = "usuarioTipoId";
static::$listAliasRelatedAttributes->area_menu__nome = "areaMenuNome";
static::$listAliasRelatedAttributes->usuario_tipo_menu__id = "id";
static::$listAliasRelatedAttributes->usuario_tipo_menu__corporacao_id_INT = "corporacaoId";
static::$listAliasRelatedAttributes->usuario_tipo_menu__area_menu = "areaMenu";
static::$listAliasRelatedAttributes->usuario_tipo_menu__nome = "nome";
            }         
        }

        public function __getList($parameters = null, $idCorporacao = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try
            {
                $filterParameters = new stdClass();
                if(isset($parameters->filterParameters))
                {
                    $filterParameters = $parameters->filterParameters;
                }
                                    
                            
            if(is_null($idCorporacao))
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            ListParameters::addFilterParameterToObject($filterParameters, "corporacaoId", $idCorporacao);           
                
                
                $sortingParameters = null;
                if(isset($parameters->sortingParameters))
                {
                    $sortingParameters = $parameters->sortingParameters;
                }
                
                $paginationParameters = null;
                if(isset($parameters->paginationParameters))
                {
                    $paginationParameters = $parameters->paginationParameters;
                }
                
                $listMappingType = static::getListMappingType($parameters->listMappingType);
                $mainTableAlias = "utm";

                $whereClause = static::getWhereClauseForFilter($filterParameters, $mainTableAlias);
                $orderByClause = static::getOrderByClauseForFilter($sortingParameters, $mainTableAlias);
                $limitClause = static::getLimitClauseForFilter($paginationParameters);

                $queryWithoutLimit = "SELECT utm.id FROM usuario_tipo_menu utm {$whereClause}";
                $query = "SELECT ut.id AS usuario_tipo__id, am.nome AS area_menu__nome, utm.id AS usuario_tipo_menu__id, utm.corporacao_id_INT AS usuario_tipo_menu__corporacao_id_INT, utm.area_menu AS usuario_tipo_menu__area_menu, utm.nome AS usuario_tipo_menu__nome FROM usuario_tipo_menu utm LEFT JOIN usuario_tipo ut ON ut.id = utm.usuario_tipo_id_INT LEFT JOIN area_menu am ON am.id = utm.area_menu_id_INT {$whereClause} {$orderByClause} {$limitClause}";

                $msg = $this->database->queryMensagem($queryWithoutLimit);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                
                $totalNumberOfRecords = $this->database->rows();
                if($totalNumberOfRecords == 0)
                {
                    return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                }                        
                        
                $msg = $this->database->queryMensagem($query);
                
                //operacao realizaca com sucesso
                if ($msg == null) 
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                    }                        
                
                    $resultSet = $this->database->result;

                    $objReturn = new stdClass();
                    $objReturn->dataSet = static::mapResultSetDataToList($resultSet, $listMappingType, static::$listAliasTypes);

                    if(!Helper::isNullOrEmpty($paginationParameters->recordsPerPage) && !Helper::isNullOrEmpty($paginationParameters->currentPage))
                    {
                        $lastPage = ceil($totalNumberOfRecords / $paginationParameters->recordsPerPage);
                        $objReturn->paginationParameters = new PaginationParams($paginationParameters->recordsPerPage, $paginationParameters->currentPage, $lastPage);
                    }
                    
                    return new Mensagem_generica($objReturn);
                } 
                else
                {
                    return $msg;
                }                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }



public function getId()
            {
                return $this->id;
            }

public function getUsuarioTipoId()
            {
                return $this->usuarioTipoId;
            }

public function getUsuario_tipo_id_INT()
                {
                    return $this->usuarioTipoId;
                }

public function getAreaMenu()
            {
                return $this->areaMenu;
            }

public function getArea_menu()
                {
                    return $this->areaMenu;
                }

public function getAreaMenuId()
            {
                return $this->areaMenuId;
            }

public function getArea_menu_id_INT()
                {
                    return $this->areaMenuId;
                }

public function getCorporacaoId()
            {
                return $this->corporacaoId;
            }

public function getCorporacao_id_INT()
                {
                    return $this->corporacaoId;
                }



function setId($value)
            {
                $this->id = $value;
            }

function setUsuarioTipoId($value)
            {
                $this->usuarioTipoId = $value;
            }

public function setUsuario_tipo_id_INT($value)
                { 
                    $this->usuarioTipoId = $value; 
                }

function setAreaMenu($value)
            {
                $this->areaMenu = $value;
            }

public function setArea_menu($value)
                { 
                    $this->areaMenu = $value; 
                }

function setAreaMenuId($value)
            {
                $this->areaMenuId = $value;
            }

public function setArea_menu_id_INT($value)
                { 
                    $this->areaMenuId = $value; 
                }

function setCorporacaoId($value)
            {
                $this->corporacaoId = $value;
            }

public function setCorporacao_id_INT($value)
                { 
                    $this->corporacaoId = $value; 
                }



public function clear()
        {$this->id = null;
$this->usuarioTipoId = null;
if($this->objUsuario_tipo != null) unset($this->objUsuario_tipo);
$this->areaMenu = null;
$this->areaMenuId = null;
if($this->objArea_menu != null) unset($this->objArea_menu);
$this->corporacaoId = null;
if($this->objCorporacao != null) unset($this->objCorporacao);
if(is_array($this->arrEstadosCampos)){
                        unset($this->arrEstadosCampos);
                     $this->arrEstadosCampos = array();
                    }
                }

public function formatarParaSQL()
        {
if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->id)){
$this->id = $this->formatarIntegerParaComandoSQL($this->id);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->id);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->usuarioTipoId)){
$this->usuarioTipoId = $this->formatarIntegerParaComandoSQL($this->usuarioTipoId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->usuarioTipoId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->areaMenu)){
$this->areaMenu = $this->formatarStringParaComandoSQL($this->areaMenu);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->areaMenu);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->areaMenuId)){
$this->areaMenuId = $this->formatarIntegerParaComandoSQL($this->areaMenuId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->areaMenuId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->corporacaoId)){
$this->corporacaoId = $this->formatarIntegerParaComandoSQL($this->corporacaoId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->corporacaoId);
}

}

public function formatarParaExibicao()
        {if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->areaMenu)){
$this->areaMenu = $this->formatarStringParaExibicao($this->areaMenu);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->areaMenu);
}

}

function select($id, $idCorporacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }   

            $sql = "SELECT id, usuario_tipo_id_INT, area_menu, area_menu_id_INT, corporacao_id_INT FROM usuario_tipo_menu WHERE id = $id AND corporacao_id_INT = $idCorporacao";

            $msg = $this->database->queryMensagem($sql);
            $result = $this->database->result;

            if($msg != null && $msg->erro() )
            { 
            		$this->clear();
            		return $msg;
        	} 
        	else if($this->database->rows() == 0)
        	{
                return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nenhum registro foi encontrado."));
            }
        

            $row = $this->database->fetchArray(MYSQLI_NUM);
            
            		$this->id = $row[0];
		$this->usuarioTipoId = $row[1];
		$this->areaMenu = $row[2];
		$this->areaMenuId = $row[3];

            
        }

public function delete($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (is_null($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }

            $sql = "DELETE FROM usuario_tipo_menu WHERE id= $id AND corporacao_id_INT = $idCorporacao";
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                    && $sincronizar
                        && static::idSistemaTabela() != null
                            && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_REMOVE,
                                null,
                                null,
                                $id,
                                $this->database);
            }

            return $msg;
        }

public function insert($sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null, $opcoes = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            
            
            if (is_null($opcoes))
            {
                $opcoes = $this->opcoesDAO;
            }

            if (!is_numeric($this->id))
            {
                $this->id = "null";
                if ($opcoes == null || $opcoes[Database::OPCAO_GERAR_ID] != false) 
                {
                    $this->id = EXTDAO_Sistema_sequencia::gerarId(static::nomeTabela());                    
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }    
                    else if($msg == null)
                    {
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
                else 
                {
                    $this->id = null;
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao));
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }
                    else if($msg == null)
                    {
                        $this->id = $this->database->getLastInsertId();
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
            }
            else 
            {
                $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }                        
                else if($msg == null)
                {
                   $msg = new Mensagem_token();
                   $msg->mValor = $this->id;
                }
            }
            if($msg == null || $msg->ok())
            {                
                if($sincronizar
                    && static::idSistemaTabela() != null
                    && isset($this->id)){
                    
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_INSERT,
                                null,
                                null,
                                $this->id,
                                $this->database); 
                	}
            } 
            else
            {
                $this->id = null;
            }
            
            return $msg;
            
        }

public function getSQLInsert($idCorporacao){ 

return "INSERT INTO usuario_tipo_menu (id, usuario_tipo_id_INT, area_menu, area_menu_id_INT, corporacao_id_INT) VALUES ( $this->id ,  $this->usuarioTipoId ,  $this->areaMenu ,  $this->areaMenuId ,  $idCorporacao ) "; 

}




        
        public function updateByParameters($id, $objParametros, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao))
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();

            
                
                if (isset($objParametros->usuarioTipoId)) 
                {
                    $arrUpdateFields[] = " usuario_tipo_id_INT = {$objParametros->usuarioTipoId} ";
                }


                
                if (isset($objParametros->areaMenu)) 
                {
                    $arrUpdateFields[] = " area_menu = {$objParametros->areaMenu} ";
                }


                
                if (isset($objParametros->areaMenuId)) 
                {
                    $arrUpdateFields[] = " area_menu_id_INT = {$objParametros->areaMenuId} ";
                }



            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE usuario_tipo_menu SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";

            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null)
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function update($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($this->usuarioTipoId)) 
                {                                      
                    $arrUpdateFields[] = " usuario_tipo_id_INT = {$this->usuarioTipoId} ";
                }


                
                if (isset($this->areaMenu)) 
                {                                      
                    $arrUpdateFields[] = " area_menu = {$this->areaMenu} ";
                }


                
                if (isset($this->areaMenuId)) 
                {                                      
                    $arrUpdateFields[] = " area_menu_id_INT = {$this->areaMenuId} ";
                }


            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE usuario_tipo_menu SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function updateByInstanceUsingParameters($id, $parameters, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($parameters->usuarioTipoId)) 
                {                                      
                    $arrUpdateFields[] = " usuario_tipo_id_INT = {$this->usuarioTipoId} ";
                }
                
                if (isset($parameters->areaMenu)) 
                {                                      
                    $arrUpdateFields[] = " area_menu = {$this->areaMenu} ";
                }
                
                if (isset($parameters->areaMenuId)) 
                {                                      
                    $arrUpdateFields[] = " area_menu_id_INT = {$this->areaMenuId} ";
                }
            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE usuario_tipo_menu SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }

}

?>
