<?php

require_once '../recursos/php/configuracao_biblioteca_compartilhada.php';

require_once $workspaceRaiz . DIRETORIO_BIBLIOTECAS_COMPARTILHADAS . '/imports/header.php';

ConfiguracaoCorporacaoDinamica::init(getWebservicesSemCache());

require_once '../recursos/php/funcoes.php';

$script = new AngularJsViewPadrao();
$script->render();

require_once '../recursos/php/on_finish.php';
