/*

 Arquivo gerado atrav�s de gerador de c�digo em 07/01/2018 as 15:54:10.
 Para que o arquivo n�o seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

 Tabela correspondente: operadora
 Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

 */

returnObject.getOperadora = function (filterParameters)
{

    var parameters = {
        filterParameters: filterParameters,
        loadComboBoxesData: true
    };

    return $http({
        url: baseURLAdm + "/webservice.php?class=EXTDAO_Operadora&action=__getRecord",
        method: "POST",
        data: parameters,
        headers: {
            'Content-Type': 'application/json'
        }
    });

};

returnObject.addOperadora = function (formData)
{

    return $http({
        url: baseURLAdm + "/webservice.php?class=EXTDAO_Operadora&jsonOutput=true&action=add",
        method: "POST",
        data: formData,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    });

};

returnObject.editOperadora = function (formData)
{

    return $http({
        url: baseURLAdm + "/webservice.php?class=EXTDAO_Operadora&jsonOutput=true&action=edit",
        method: "POST",
        data: formData,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    });

};

returnObject.getAllComboBoxesDataForOperadoraForm = function ()
{
    return $http({
        url: baseURLAdm + "/webservice.php?class=EXTDAO_Operadora&jsonOutput=true&action=__getFormComboBoxesData",
        method: "POST",
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    });

};



