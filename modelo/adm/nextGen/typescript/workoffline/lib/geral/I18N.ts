import * as Moment from 'moment';

export class I18N
{
    public static getMomentInstance(dateObject :Date)
    {
        return Moment(dateObject);
    }

}