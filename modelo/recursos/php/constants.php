<?php

define('TITULO_PAGINAS_CLIENTE', "WorkOffline.com.br");
define('PATH_RELATIVO_PROJETO', 'adm/');

define('IDENTIFICADOR_SISTEMA', 'PTO_');
define('MODO_BANCO_COM_CORPORACAO', true);
define('BANCO_COM_TRATAMENTO_EXCLUSAO', false);

define('ENVIAR_EMAIL', false);
define('ID_SISTEMA_SIHOP', 1);
define('PROJETO', 'OEW');



if (isset($_SERVER["HTTP_HOST"]) && substr_count($_SERVER["HTTP_HOST"], "workoffline.com.br") >= 1)
{

    define('DOMINIO_DE_ACESSO', "http://workoffline.com.br");

//    define('IDENTIFICADOR_GOOGLE_ANALYTICS', 'UA-108255191-1');
//    define('DOMINIO_GOOGLE_ANALYTICS', DOMINIO_DE_ACESSO);

    define('DOMINIO_DE_ACESSO_SICOB', "http://my.workoffline.com.br/client_area/");
    define('DOMINIO_DE_ACESSO_SERVICOS_SICOB', "http://my.workoffline.com.br/adm/webservice.php?class=Servicos_web&action=");
    define('DOMINIO_DE_ACESSO_SERVICOS_SIHOP', "http://hosting.workoffline.com.br/adm/actions.php?class=Servicos_web&action=");

    define('DIRETORIO_BIBLIOTECAS_COMPARTILHADAS', 'biblioteca-compartilhada/public_html/');

}
else
{
    if (isset($_SERVER["HTTP_HOST"]) &&  substr_count($_SERVER["HTTP_HOST"], "pontoeletronico.empresa.omegasoftware.com.br") >= 1)
    {

        define('DOMINIO_DE_ACESSO', "http://pontoeletronico.empresa.omegasoftware.com.br");
        define('DOMINIO_DE_ACESSO_SICOB', "http://empresa.omegasoftware.com.br/site/");
        define('DOMINIO_DE_ACESSO_SERVICOS_SICOB', "http://empresa.omegasoftware.com.br/site/adm/webservice.php?class=Servicos_web&action=");
        define('DOMINIO_DE_ACESSO_SERVICOS_SIHOP', "http://hospedagem.empresa.omegasoftware.com.br/adm/actions.php?class=Servicos_web&action=");

        define('DIRETORIO_BIBLIOTECAS_COMPARTILHADAS', 'BibliotecasCompartilhadas/');

    }
    elseif (isset($_SERVER["DOCUMENT_ROOT"]) && strtolower($_SERVER["DOCUMENT_ROOT"]) == strtolower("D:/wamp/www/") && $_SERVER["SERVER_ADMIN"] == "eduardo@omegasoftware.com.br")
    {

        define('DOMINIO_DE_ACESSO', "127.0.0.1/OmegaEmpresa/PontoEletronicoAngularJS/Trunk/modelo/");
        define('DOMINIO_DE_ACESSO_SICOB', "http://127.0.0.1/OmegaEmpresa/Cobranca/Trunk/client_area/");
        define('DOMINIO_DE_ACESSO_SERVICOS_SICOB', "http://127.0.0.1/OmegaEmpresa/Cobranca/Trunk/adm/webservice.php?class=Servicos_web&action=");
        define('DOMINIO_DE_ACESSO_SERVICOS_SIHOP', "http://127.0.0.1/OmegaEmpresa/Hospedagem/Trunk/adm/actions.php?class=Servicos_web&action=");

        define('DIRETORIO_BIBLIOTECAS_COMPARTILHADAS', 'BibliotecaCompartilhada/Trunk/');

    }
    else
    {

        define('DOMINIO_DE_ACESSO', "localhost/PontoEletronicoNovo/PE10002Corporacao/public_html/");
        define('DOMINIO_DE_ACESSO_SICOB', "http://127.0.0.1/Cobranca/CO10003Corporacao/client_area/");
        define('DOMINIO_DE_ACESSO_SERVICOS_SICOB', "http://127.0.0.1/Cobranca/CO10003Corporacao/adm/webservice.php?class=Servicos_web&action=");
        define('DOMINIO_DE_ACESSO_SERVICOS_SIHOP', "http://127.0.0.1/hospedagem/HO10002/adm/actions.php?class=Servicos_web&action=");

        define('DIRETORIO_BIBLIOTECAS_COMPARTILHADAS', 'BibliotecasCompartilhadas/BC10002Corporacao/');

    }
}

define('PAGINA_INICIAL_PADRAO', "nextGen/view/calendarioDePontos.php");

define('ENDERECO_DE_ACESSO', "http://" + DOMINIO_DE_ACESSO + "/");
define('ENDERECO_DE_ACESSO_SSL', "https://" + DOMINIO_DE_ACESSO + "/");

