<?php

class BO_Calendario
{
    public function getDadosPontosEletronicos($idCorporacao = null, $dataInicialSec, $dataFinalSec)
    {
        if ($idCorporacao == null)
        {
            $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
        }

        $q = "SELECT p.data_SEC AS dataSec, 
                  p.data_OFFSEC AS dataOffsec, 
                  pe.nome AS nomePessoa, 
                  pe.id AS idPessoa,
                  is_entrada_BOOLEAN AS isEntrada,
                  p.id AS idPonto
                  FROM ponto p      
                    JOIN pessoa pe
                      ON pe.id = p.pessoa_id_INT       
                    JOIN empresa e 
                      ON e.id = p.empresa_id_INT
                    JOIN profissao pr
                      ON pr.id = p.profissao_id_INT          
                  WHERE p.corporacao_id_INT = {$idCorporacao}
                  AND p.data_SEC >= {$dataInicialSec}
                  AND p.data_SEC <= {$dataFinalSec}
                  ORDER BY pe.id, p.data_SEC + p.data_OFFSEC";

        $db = new Database();
        $msg = $db->queryMensagem($q);
        if (Interface_mensagem::checkErro($msg))
        {
            return $msg;
        }

        return Helper::getResultSetToMatriz($db->result, 1, 0);
    }

    public function gravarPontoEletronicoParaCorrecao($idCorporacao = null, $idPontoReferencia, $idPessoa, $timestampPonto, $timezoneOffsetPonto, $tipoPonto)
    {
        try
        {
            if ($idCorporacao == null)
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }

            $objPontoReferencia = new EXTDAO_Ponto();
            $objPontoReferencia->select($idPontoReferencia);

            $objPonto = new EXTDAO_Ponto();
            $objPonto->setData_SEC($timestampPonto);
            $objPonto->setData_OFFSEC($timezoneOffsetPonto);
            $objPonto->setTipo_ponto_id_INT($tipoPonto);
            $objPonto->setPessoa_id_INT($idPessoa);
            $objPonto->setIs_entrada_BOOLEAN($tipoPonto === "0");
            $objPonto->setCorporacao_id_INT($idCorporacao);
            $objPonto->setEmpresa_id_INT($objPontoReferencia->getEmpresa_id_INT());
            $objPonto->setProfissao_id_INT($objPontoReferencia->getProfissao_id_INT());

            $objPonto->formatarParaSQL();
            $msgInsert = $objPonto->insert(true);

            if (Interface_mensagem::checkErro($msgInsert))
            {
                return $msgInsert;
            }

        }
        catch (Exception $ex)
        {
            return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
        }

        return $msgInsert;

    }

}

?>