<?php

/*

Arquivo gerado atravï¿½s de gerador de cï¿½digo em 24/05/2017 as 09:57:43.
Para que o arquivo nï¿½o seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

Tabela correspondente: permissao_categoria_permissao
Sobrescrita de dados: ---SOBRESCRITA_NAO_AUTORIZADA---

*/
?>

<?php

class EXTDAO_Permissao_categoria_permissao extends DAO_Permissao_categoria_permissao
{
    public function __construct($configDAO = null)
    {
        parent::__construct($configDAO);
        $this->nomeClasse = "EXTDAO_Permissao_categoria_permissao";
    }

    public function factory()
    {
        return new EXTDAO_Permissao_categoria_permissao();
    }

    public static function getListaPermissaoDaCategoriaDePermissao($idCategoriaPermissao, Database $db = null)
    {
        if ($db == null)
        {
            $db = new Database();
        }
        $q = "SELECT p.id
FROM permissao p JOIN permissao_categoria_permissao pcp ON p.id = pcp.permissao_id_INT
WHERE pcp.categoria_permissao_id_INT = $idCategoriaPermissao";
        $db->query($q);

        return Helper::getResultSetToArrayDeUmCampo($db->result);
    }
}
