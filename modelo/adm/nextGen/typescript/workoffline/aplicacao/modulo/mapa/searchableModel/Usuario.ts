import { SearchableModel, SearchableAttribute } from "lib/model";
import { Helper } from "aplicacao/comum/geral";

export class Usuario extends SearchableModel
{
    public static readonly Color = {DefaultDarkLuma: 120, DefaultLightLuma: 120 };

    public id: SearchableAttribute<number>;
    public nome: SearchableAttribute<string>;
    public cor: SearchableAttribute<string>;
    public empresa: SearchableAttribute<string>;
    public isOnline: SearchableAttribute<boolean>;

    public constructor(id: number, nome: string, isOnline: boolean);
    public constructor(id: number, nome: string, isOnline: boolean, cor?: string)
    {
        super('Usuario');
        cor = cor || Helper.getRandomColor(Usuario.Color.DefaultDarkLuma, Usuario.Color.DefaultLightLuma);

        this.id = new SearchableAttribute(id, true, true);
        this.nome = new SearchableAttribute(nome, true);
        this.isOnline = new SearchableAttribute(isOnline, false);
        this.cor = new SearchableAttribute(cor, false);
    }

    public isOnlineClass(): string
    {
        return this.isOnline ? 'is-online' : '';
    }

}