<?php


        /*

        Arquivo gerado através de gerador de código em 12/05/2018 as 14:13:34.
        Para que o arquivo não seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

        Tabela correspondente: empresa_venda
        Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

        */
        


class DAO_Empresa_venda extends Generic_DAO
                        {
                            const NOME_ENTIDADE = "empresa_venda";

                            public static $nomeTabela = null;
                            public static $idSistemaTabela = false;
                            public static $databaseFieldsRelatedAttributes = null;
	                        public static $databaseFieldTypes = null;
	                        public static $databaseFieldNames = null;
	                        public static $listAliasTypes = null;
	                        public static $listAliasRelatedAttributes = null;
	                        
	                        public $id;
public $formaPagamentoId;
public $valorTotalFloat;
public $descontoFloat;
public $dataSec;
public $dataOffsec;
public $usuarioId;
public $clienteEmpresaId;
public $fornecedorEmpresaId;
public $pessoaId;
public $foto;
public $acrescimoFloat;
public $registroEstadoId;
public $registroEstadoCorporacaoId;
public $relatorioId;
public $descricao;
public $vencimentoSec;
public $vencimentoOffsec;
public $fechamentoSec;
public $fechamentoOffsec;
public $valorPagoFloat;
public $mesaId;
public $identificador;
public $protocoloInt;
public $corporacaoId;

public $labelId;
public $labelFormaPagamentoId;
public $labelValorTotalFloat;
public $labelDescontoFloat;
public $labelDataSec;
public $labelDataOffsec;
public $labelUsuarioId;
public $labelClienteEmpresaId;
public $labelFornecedorEmpresaId;
public $labelPessoaId;
public $labelFoto;
public $labelAcrescimoFloat;
public $labelRegistroEstadoId;
public $labelRegistroEstadoCorporacaoId;
public $labelRelatorioId;
public $labelDescricao;
public $labelVencimentoSec;
public $labelVencimentoOffsec;
public $labelFechamentoSec;
public $labelFechamentoOffsec;
public $labelValorPagoFloat;
public $labelMesaId;
public $labelIdentificador;
public $labelProtocoloInt;
public $labelCorporacaoId;

public static $arrPK = array("id");

public function __construct($configDAO = null)
        {
            parent::__construct($configDAO);
            static::__setStaticValues($this->database);           
        }

public static function idSistemaTabela($databaseObject = null){
            if(!(static::$idSistemaTabela))
            {
                static::$idSistemaTabela = EXTDAO_Sistema_tabela::getIdDaSistemaTabela(static::nomeTabela(), $databaseObject);
            }
            return static::$idSistemaTabela;
        }

public function getIdSistemaTabela(){
            return static::idSistemaTabela();
        }

public static function nomeTabela(){
            if(is_null(static::$nomeTabela))
            {
                static::$nomeTabela = "empresa_venda";
            }
            return static::$nomeTabela;
        }

public function getNomeTabela(){
            return static::nomeTabela();
        }

public static function __setStaticValues($databaseObject = null)
                   {    
                        static::nomeTabela();
                        
                        static::idSistemaTabela($databaseObject);
                   
                        static::setDatabaseFieldNames();
                        static::setDatabaseFieldTypes();
                        static::setDatabaseFieldsRelatedAttributeNames();
                        
                        static::setListAliasTypes();
                        static::setListAliasRelatedAttributes();
                   }

public function getExpressaoLabel()
                   {
                        return "{id}";
                   }

public static function setDatabaseFieldNames()
        {
            if(is_null(static::$databaseFieldNames))
            {
                static::$databaseFieldNames = new stdClass();
                static::$databaseFieldNames->id = "id";
static::$databaseFieldNames->formaPagamentoId = "forma_pagamento_id_INT";
static::$databaseFieldNames->valorTotalFloat = "valor_total_FLOAT";
static::$databaseFieldNames->descontoFloat = "desconto_FLOAT";
static::$databaseFieldNames->dataSec = "data_SEC";
static::$databaseFieldNames->dataOffsec = "data_OFFSEC";
static::$databaseFieldNames->usuarioId = "usuario_id_INT";
static::$databaseFieldNames->clienteEmpresaId = "cliente_empresa_id_INT";
static::$databaseFieldNames->fornecedorEmpresaId = "fornecedor_empresa_id_INT";
static::$databaseFieldNames->pessoaId = "pessoa_id_INT";
static::$databaseFieldNames->foto = "foto";
static::$databaseFieldNames->acrescimoFloat = "acrescimo_FLOAT";
static::$databaseFieldNames->registroEstadoId = "registro_estado_id_INT";
static::$databaseFieldNames->registroEstadoCorporacaoId = "registro_estado_corporacao_id_INT";
static::$databaseFieldNames->relatorioId = "relatorio_id_INT";
static::$databaseFieldNames->descricao = "descricao";
static::$databaseFieldNames->vencimentoSec = "vencimento_SEC";
static::$databaseFieldNames->vencimentoOffsec = "vencimento_OFFSEC";
static::$databaseFieldNames->fechamentoSec = "fechamento_SEC";
static::$databaseFieldNames->fechamentoOffsec = "fechamento_OFFSEC";
static::$databaseFieldNames->valorPagoFloat = "valor_pago_FLOAT";
static::$databaseFieldNames->mesaId = "mesa_id_INT";
static::$databaseFieldNames->identificador = "identificador";
static::$databaseFieldNames->protocoloInt = "protocolo_INT";
static::$databaseFieldNames->corporacaoId = "corporacao_id_INT";

            }
                      
        }

public static function setDatabaseFieldTypes()
        {
            if(is_null(static::$databaseFieldTypes))
            {
                static::$databaseFieldTypes = new stdClass();
                static::$databaseFieldTypes->id = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->formaPagamentoId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->valorTotalFloat = static::TIPO_VARIAVEL_FLOAT;
static::$databaseFieldTypes->descontoFloat = static::TIPO_VARIAVEL_FLOAT;
static::$databaseFieldTypes->dataSec = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$databaseFieldTypes->dataOffsec = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$databaseFieldTypes->usuarioId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->clienteEmpresaId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->fornecedorEmpresaId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->pessoaId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->foto = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->acrescimoFloat = static::TIPO_VARIAVEL_FLOAT;
static::$databaseFieldTypes->registroEstadoId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->registroEstadoCorporacaoId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->relatorioId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->descricao = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->vencimentoSec = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$databaseFieldTypes->vencimentoOffsec = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$databaseFieldTypes->fechamentoSec = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$databaseFieldTypes->fechamentoOffsec = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$databaseFieldTypes->valorPagoFloat = static::TIPO_VARIAVEL_FLOAT;
static::$databaseFieldTypes->mesaId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->identificador = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->protocoloInt = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->corporacaoId = static::TIPO_VARIAVEL_INTEGER;

            }
            
        }

public static function setDatabaseFieldsRelatedAttributeNames()
        {
            if(is_null(static::$databaseFieldsRelatedAttributes))
            {
                static::$databaseFieldsRelatedAttributes = new stdClass();
                static::$databaseFieldsRelatedAttributes->id = "id";
static::$databaseFieldsRelatedAttributes->forma_pagamento_id_INT = "formaPagamentoId";
static::$databaseFieldsRelatedAttributes->valor_total_FLOAT = "valorTotalFloat";
static::$databaseFieldsRelatedAttributes->desconto_FLOAT = "descontoFloat";
static::$databaseFieldsRelatedAttributes->data_SEC = "dataSec";
static::$databaseFieldsRelatedAttributes->data_OFFSEC = "dataOffsec";
static::$databaseFieldsRelatedAttributes->usuario_id_INT = "usuarioId";
static::$databaseFieldsRelatedAttributes->cliente_empresa_id_INT = "clienteEmpresaId";
static::$databaseFieldsRelatedAttributes->fornecedor_empresa_id_INT = "fornecedorEmpresaId";
static::$databaseFieldsRelatedAttributes->pessoa_id_INT = "pessoaId";
static::$databaseFieldsRelatedAttributes->foto = "foto";
static::$databaseFieldsRelatedAttributes->acrescimo_FLOAT = "acrescimoFloat";
static::$databaseFieldsRelatedAttributes->registro_estado_id_INT = "registroEstadoId";
static::$databaseFieldsRelatedAttributes->registro_estado_corporacao_id_INT = "registroEstadoCorporacaoId";
static::$databaseFieldsRelatedAttributes->relatorio_id_INT = "relatorioId";
static::$databaseFieldsRelatedAttributes->descricao = "descricao";
static::$databaseFieldsRelatedAttributes->vencimento_SEC = "vencimentoSec";
static::$databaseFieldsRelatedAttributes->vencimento_OFFSEC = "vencimentoOffsec";
static::$databaseFieldsRelatedAttributes->fechamento_SEC = "fechamentoSec";
static::$databaseFieldsRelatedAttributes->fechamento_OFFSEC = "fechamentoOffsec";
static::$databaseFieldsRelatedAttributes->valor_pago_FLOAT = "valorPagoFloat";
static::$databaseFieldsRelatedAttributes->mesa_id_INT = "mesaId";
static::$databaseFieldsRelatedAttributes->identificador = "identificador";
static::$databaseFieldsRelatedAttributes->protocolo_INT = "protocoloInt";
static::$databaseFieldsRelatedAttributes->corporacao_id_INT = "corporacaoId";

            }           
        }

public function setAllFieldsToHumanFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["forma_pagamento_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["valor_total_FLOAT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["desconto_FLOAT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["data_SEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["data_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["usuario_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["cliente_empresa_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["fornecedor_empresa_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["pessoa_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["foto"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["acrescimo_FLOAT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["registro_estado_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["registro_estado_corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["relatorio_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["descricao"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["vencimento_SEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["vencimento_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["fechamento_SEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["fechamento_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["valor_pago_FLOAT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["mesa_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["identificador"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["protocolo_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
}

public function setAllFieldsToDatabaseFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["forma_pagamento_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["valor_total_FLOAT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["desconto_FLOAT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["data_SEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["data_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["usuario_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["cliente_empresa_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["fornecedor_empresa_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["pessoa_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["foto"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["acrescimo_FLOAT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["registro_estado_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["registro_estado_corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["relatorio_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["descricao"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["vencimento_SEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["vencimento_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["fechamento_SEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["fechamento_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["valor_pago_FLOAT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["mesa_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["identificador"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["protocolo_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
}

  public function getFkObjForma_pagamento() 
                {
                    if($this->objForma_pagamento == null)
                    {                        
                        $this->objForma_pagamento = new EXTDAO_Forma_pagamento_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getForma_pagamento_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objForma_pagamento->clear();
                    }
                    elseif($this->objForma_pagamento->getId() != $idFK)
                    {
                        $this->objForma_pagamento->select($idFK);
                    }
                    return $this->objForma_pagamento;
                }
  public function getFkObjUsuario() 
                {
                    if($this->objUsuario == null)
                    {                        
                        $this->objUsuario = new EXTDAO_Usuario_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getUsuario_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objUsuario->clear();
                    }
                    elseif($this->objUsuario->getId() != $idFK)
                    {
                        $this->objUsuario->select($idFK);
                    }
                    return $this->objUsuario;
                }
  public function getFkObjCliente_empresa() 
                {
                    if($this->objCliente_empresa == null)
                    {                        
                        $this->objCliente_empresa = new EXTDAO_Cliente_empresa_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getCliente_empresa_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objCliente_empresa->clear();
                    }
                    elseif($this->objCliente_empresa->getId() != $idFK)
                    {
                        $this->objCliente_empresa->select($idFK);
                    }
                    return $this->objCliente_empresa;
                }
  public function getFkObjFornecedor_empresa() 
                {
                    if($this->objFornecedor_empresa == null)
                    {                        
                        $this->objFornecedor_empresa = new EXTDAO_Fornecedor_empresa_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getFornecedor_empresa_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objFornecedor_empresa->clear();
                    }
                    elseif($this->objFornecedor_empresa->getId() != $idFK)
                    {
                        $this->objFornecedor_empresa->select($idFK);
                    }
                    return $this->objFornecedor_empresa;
                }
  public function getFkObjPessoa() 
                {
                    if($this->objPessoa == null)
                    {                        
                        $this->objPessoa = new EXTDAO_Pessoa_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getPessoa_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objPessoa->clear();
                    }
                    elseif($this->objPessoa->getId() != $idFK)
                    {
                        $this->objPessoa->select($idFK);
                    }
                    return $this->objPessoa;
                }
  public function getFkObjRegistro_estado() 
                {
                    if($this->objRegistro_estado == null)
                    {                        
                        $this->objRegistro_estado = new EXTDAO_Registro_estado_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getRegistro_estado_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objRegistro_estado->clear();
                    }
                    elseif($this->objRegistro_estado->getId() != $idFK)
                    {
                        $this->objRegistro_estado->select($idFK);
                    }
                    return $this->objRegistro_estado;
                }
  public function getFkObjRegistro_estado_corporacao() 
                {
                    if($this->objRegistro_estado_corporacao == null)
                    {                        
                        $this->objRegistro_estado_corporacao = new EXTDAO_Registro_estado_corporacao_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getRegistro_estado_corporacao_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objRegistro_estado_corporacao->clear();
                    }
                    elseif($this->objRegistro_estado_corporacao->getId() != $idFK)
                    {
                        $this->objRegistro_estado_corporacao->select($idFK);
                    }
                    return $this->objRegistro_estado_corporacao;
                }
  public function getFkObjRelatorio() 
                {
                    if($this->objRelatorio == null)
                    {                        
                        $this->objRelatorio = new EXTDAO_Relatorio_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getRelatorio_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objRelatorio->clear();
                    }
                    elseif($this->objRelatorio->getId() != $idFK)
                    {
                        $this->objRelatorio->select($idFK);
                    }
                    return $this->objRelatorio;
                }
  public function getFkObjMesa() 
                {
                    if($this->objMesa == null)
                    {                        
                        $this->objMesa = new EXTDAO_Mesa_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getMesa_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objMesa->clear();
                    }
                    elseif($this->objMesa->getId() != $idFK)
                    {
                        $this->objMesa->select($idFK);
                    }
                    return $this->objMesa;
                }
  public function getFkObjCorporacao() 
                {
                    if($this->objCorporacao == null)
                    {                        
                        $this->objCorporacao = new EXTDAO_Corporacao_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getCorporacao_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objCorporacao->clear();
                    }
                    elseif($this->objCorporacao->getId() != $idFK)
                    {
                        $this->objCorporacao->select($idFK);
                    }
                    return $this->objCorporacao;
                }


public function setLabels()
        {$this->labelId = "id";
$this->labelFormaPagamentoId = "formapagamentoidINT";
$this->labelValorTotalFloat = "valortotalFLOAT";
$this->labelDescontoFloat = "descontoFLOAT";
$this->labelDataSec = "";
$this->labelDataOffsec = "";
$this->labelUsuarioId = "usuarioidINT";
$this->labelClienteEmpresaId = "clienteempresaidINT";
$this->labelFornecedorEmpresaId = "fornecedorempresaidINT";
$this->labelPessoaId = "pessoaidINT";
$this->labelFoto = "foto";
$this->labelAcrescimoFloat = "";
$this->labelRegistroEstadoId = "";
$this->labelRegistroEstadoCorporacaoId = "";
$this->labelRelatorioId = "";
$this->labelDescricao = "";
$this->labelVencimentoSec = "";
$this->labelVencimentoOffsec = "";
$this->labelFechamentoSec = "";
$this->labelFechamentoOffsec = "";
$this->labelValorPagoFloat = "";
$this->labelMesaId = "";
$this->labelIdentificador = "";
$this->labelProtocoloInt = "";
$this->labelCorporacaoId = "corporacaoidINT";
}

public function __actionAdd($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                $this->setByObject($parameters);
                $this->setDataCadastroAndOffsetInSecondsFromUser($parameters);
                
                $this->formatarParaSQL();
                $msg = $this->insert(true);
                
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                return new Mensagem(null, I18N::getExpression("Empresa venda adicionado com sucesso."));
                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionEdit($parameters = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try {

                $this->setByObject($parameters);
                $this->setAllFieldsToHumanFormat();
                
                $this->formatarParaSQL();
                $msg = $this->updateByInstanceUsingParameters($this->id, $parameters, true);
                if($msg != null && $msg->erro())
                {
                     return $msg;
                }
        
                return new Mensagem(null, I18N::getExpression("Empresa venda editado com sucesso."));
            }
            catch(Exception $ex)
            {                
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionRemove($parameters = null)
        {
            $successulRemovalCount = 0;
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                foreach($parameters as $record)
                {
                    if(is_numeric($record->id))
                    {
                        $msg = $this->delete($record->id, true);
                        if($msg == null || $msg->ok())
                        {
                            $successulRemovalCount++;
                        }
                    }
                }
                
                if($successulRemovalCount > 0)
                {
                    $successMessage = count($successulRemovalCount) > 1 ? I18N::getExpression("{0} Empresa venda foram removidos com sucesso.", $successulRemovalCount) : I18N::getExpression("Empresa venda removido com sucesso.");
                    return new Mensagem(null, $successMessage);
                }
                else
                {
                    $errorMessage = count($parameters) > 1 ? I18N::getExpression("Falha ao remover Empresa venda.") : I18N::getExpression("Falha ao remover Empresa venda.");
                    return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, $errorMessage);                    
                }
            }
            catch (Exception $ex) 
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }

public function __getRecord($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }
            
            try
            {            
                $returnObject = new stdClass();            
                $whereClause = static::getWhereClauseForFilter($parameters->filterParameters);
                $query = "SELECT  id, forma_pagamento_id_INT, valor_total_FLOAT, desconto_FLOAT, data_SEC, data_OFFSEC, usuario_id_INT, cliente_empresa_id_INT, fornecedor_empresa_id_INT, pessoa_id_INT, foto, acrescimo_FLOAT, registro_estado_id_INT, registro_estado_corporacao_id_INT, relatorio_id_INT, descricao, vencimento_SEC, vencimento_OFFSEC, fechamento_SEC, fechamento_OFFSEC, valor_pago_FLOAT, mesa_id_INT, identificador, protocolo_INT, corporacao_id_INT FROM empresa_venda {$whereClause}";
    
                $msg = $this->database->queryMensagem($query);                
                if ($msg == null)
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Não foi encontrado nenhum registro"));
                    }                    
                    $resultSet = $this->database->result;
                    $resultObject = static::mapResultSetDataToRecord($resultSet);
                    
                    $returnObject->formData = $resultObject;
                    $returnObject->comboBoxesData = null;
                    if($parameters->loadComboBoxesData)
                    {
                        $this->setByObject($resultObject);
                        $returnObject->comboBoxesData =  $this->getFormComboBoxesData();
                    }                    
    
                    
    
                    return new Mensagem_generica($returnObject);
                } 
                else 
                {
                    return $msg;
                }
                
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }


        
        public function __getFilterComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
                
        public function __getFormComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
        
        protected function getFormComboBoxesData()
        {
            $comboBoxesData = new stdClass();
            $listParameters = new stdClass();
            $listParameters->listMappingType = static::LIST_MAPPING_TYPE_COMBOBOX;
            
            
                    
                    $objFormaPagamento = new EXTDAO_Forma_pagamento();
                    $comboBoxesData->fieldFormaPagamentoId = $objFormaPagamento->__getList($listParameters);
                    
                    $objUsuario = new EXTDAO_Usuario();
                    $comboBoxesData->fieldUsuarioId = $objUsuario->__getList($listParameters);
                    
                    $objPessoa = new EXTDAO_Pessoa();
                    $comboBoxesData->fieldPessoaId = $objPessoa->__getList($listParameters);

            return $comboBoxesData;
        }
        
        


        
        public static function setListAliasTypes()
        {
            if(is_null(static::$listAliasTypes))
            {
                static::$listAliasTypes = new stdClass();
                static::$listAliasTypes->forma_pagamento__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->usuario__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->pessoa__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->empresa_venda__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->empresa_venda__corporacao_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->empresa_venda__valor_total_FLOAT = static::TIPO_VARIAVEL_FLOAT;
static::$listAliasTypes->empresa_venda__desconto_FLOAT = static::TIPO_VARIAVEL_FLOAT;
static::$listAliasTypes->empresa_venda__data_SEC = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$listAliasTypes->empresa_venda__data_OFFSEC = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$listAliasTypes->empresa_venda__cadastro_OFFSEC = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$listAliasTypes->empresa_venda__cliente_empresa_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->empresa_venda__fornecedor_empresa_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->empresa_venda__ind_celular_valido_BOOLEAN = static::TIPO_VARIAVEL_BOOLEAN;
static::$listAliasTypes->empresa_venda__foto = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->empresa_venda__acrescimo_FLOAT = static::TIPO_VARIAVEL_FLOAT;
            }        
        }
        
        public static function setListAliasRelatedAttributes()
        {
            if(is_null(static::$listAliasRelatedAttributes))
            {
                static::$listAliasRelatedAttributes = new stdClass();
                static::$listAliasRelatedAttributes->forma_pagamento__id = "formaPagamentoId";
static::$listAliasRelatedAttributes->usuario__id = "usuarioId";
static::$listAliasRelatedAttributes->pessoa__id = "pessoaId";
static::$listAliasRelatedAttributes->empresa_venda__id = "id";
static::$listAliasRelatedAttributes->empresa_venda__corporacao_id_INT = "corporacaoId";
static::$listAliasRelatedAttributes->empresa_venda__valor_total_FLOAT = "valorTotalFloat";
static::$listAliasRelatedAttributes->empresa_venda__desconto_FLOAT = "descontoFloat";
static::$listAliasRelatedAttributes->empresa_venda__data_SEC = "dataSec";
static::$listAliasRelatedAttributes->empresa_venda__data_OFFSEC = "dataOffsec";
static::$listAliasRelatedAttributes->empresa_venda__cadastro_OFFSEC = "cadastroOffsec";
static::$listAliasRelatedAttributes->empresa_venda__cliente_empresa_id_INT = "clienteEmpresaId";
static::$listAliasRelatedAttributes->empresa_venda__fornecedor_empresa_id_INT = "fornecedorEmpresaId";
static::$listAliasRelatedAttributes->empresa_venda__ind_celular_valido_BOOLEAN = "indCelularValidoBoolean";
static::$listAliasRelatedAttributes->empresa_venda__foto = "foto";
static::$listAliasRelatedAttributes->empresa_venda__acrescimo_FLOAT = "acrescimoFloat";
            }         
        }

        public function __getList($parameters = null, $idCorporacao = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try
            {
                $filterParameters = new stdClass();
                if(isset($parameters->filterParameters))
                {
                    $filterParameters = $parameters->filterParameters;
                }
                                    
                            
            if(is_null($idCorporacao))
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            ListParameters::addFilterParameterToObject($filterParameters, "corporacaoId", $idCorporacao);           
                
                
                $sortingParameters = null;
                if(isset($parameters->sortingParameters))
                {
                    $sortingParameters = $parameters->sortingParameters;
                }
                
                $paginationParameters = null;
                if(isset($parameters->paginationParameters))
                {
                    $paginationParameters = $parameters->paginationParameters;
                }
                
                $listMappingType = static::getListMappingType($parameters->listMappingType);
                $mainTableAlias = "ev";

                $whereClause = static::getWhereClauseForFilter($filterParameters, $mainTableAlias);
                $orderByClause = static::getOrderByClauseForFilter($sortingParameters, $mainTableAlias);
                $limitClause = static::getLimitClauseForFilter($paginationParameters);

                $queryWithoutLimit = "SELECT ev.id FROM empresa_venda ev {$whereClause}";
                $query = "SELECT fp.id AS forma_pagamento__id, u.id AS usuario__id, p.id AS pessoa__id, ev.id AS empresa_venda__id, ev.corporacao_id_INT AS empresa_venda__corporacao_id_INT, ev.valor_total_FLOAT AS empresa_venda__valor_total_FLOAT, ev.desconto_FLOAT AS empresa_venda__desconto_FLOAT, ev.data_SEC AS empresa_venda__data_SEC, ev.data_OFFSEC AS empresa_venda__data_OFFSEC, ev.cadastro_OFFSEC AS empresa_venda__cadastro_OFFSEC, ev.cliente_empresa_id_INT AS empresa_venda__cliente_empresa_id_INT, ev.fornecedor_empresa_id_INT AS empresa_venda__fornecedor_empresa_id_INT, ev.ind_celular_valido_BOOLEAN AS empresa_venda__ind_celular_valido_BOOLEAN, ev.foto AS empresa_venda__foto, ev.acrescimo_FLOAT AS empresa_venda__acrescimo_FLOAT FROM empresa_venda ev LEFT JOIN forma_pagamento fp ON fp.id = ev.forma_pagamento_id_INT LEFT JOIN usuario u ON u.id = ev.usuario_id_INT LEFT JOIN pessoa p ON p.id = ev.pessoa_id_INT {$whereClause} {$orderByClause} {$limitClause}";

                $msg = $this->database->queryMensagem($queryWithoutLimit);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                
                $totalNumberOfRecords = $this->database->rows();
                if($totalNumberOfRecords == 0)
                {
                    return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                }                        
                        
                $msg = $this->database->queryMensagem($query);
                
                //operacao realizaca com sucesso
                if ($msg == null) 
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                    }                        
                
                    $resultSet = $this->database->result;

                    $objReturn = new stdClass();
                    $objReturn->dataSet = static::mapResultSetDataToList($resultSet, $listMappingType, static::$listAliasTypes);

                    if(!Helper::isNullOrEmpty($paginationParameters->recordsPerPage) && !Helper::isNullOrEmpty($paginationParameters->currentPage))
                    {
                        $lastPage = ceil($totalNumberOfRecords / $paginationParameters->recordsPerPage);
                        $objReturn->paginationParameters = new PaginationParams($paginationParameters->recordsPerPage, $paginationParameters->currentPage, $lastPage);
                    }
                    
                    return new Mensagem_generica($objReturn);
                } 
                else
                {
                    return $msg;
                }                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }



public function getId()
            {
                return $this->id;
            }

public function getFormaPagamentoId()
            {
                return $this->formaPagamentoId;
            }

public function getForma_pagamento_id_INT()
                {
                    return $this->formaPagamentoId;
                }

public function getValorTotalFloat()
            {
                return $this->valorTotalFloat;
            }

public function getValor_total_FLOAT()
                {
                    return $this->valorTotalFloat;
                }

public function getDescontoFloat()
            {
                return $this->descontoFloat;
            }

public function getDesconto_FLOAT()
                {
                    return $this->descontoFloat;
                }

public function getDataSec()
            {
                return $this->dataSec;
            }

public function getData_SEC()
                {
                    return $this->dataSec;
                }

public function getDataOffsec()
            {
                return $this->dataOffsec;
            }

public function getData_OFFSEC()
                {
                    return $this->dataOffsec;
                }

public function getUsuarioId()
            {
                return $this->usuarioId;
            }

public function getUsuario_id_INT()
                {
                    return $this->usuarioId;
                }

public function getClienteEmpresaId()
            {
                return $this->clienteEmpresaId;
            }

public function getCliente_empresa_id_INT()
                {
                    return $this->clienteEmpresaId;
                }

public function getFornecedorEmpresaId()
            {
                return $this->fornecedorEmpresaId;
            }

public function getFornecedor_empresa_id_INT()
                {
                    return $this->fornecedorEmpresaId;
                }

public function getPessoaId()
            {
                return $this->pessoaId;
            }

public function getPessoa_id_INT()
                {
                    return $this->pessoaId;
                }

public function getFoto()
            {
                return $this->foto;
            }

public function getAcrescimoFloat()
            {
                return $this->acrescimoFloat;
            }

public function getAcrescimo_FLOAT()
                {
                    return $this->acrescimoFloat;
                }

public function getRegistroEstadoId()
            {
                return $this->registroEstadoId;
            }

public function getRegistro_estado_id_INT()
                {
                    return $this->registroEstadoId;
                }

public function getRegistroEstadoCorporacaoId()
            {
                return $this->registroEstadoCorporacaoId;
            }

public function getRegistro_estado_corporacao_id_INT()
                {
                    return $this->registroEstadoCorporacaoId;
                }

public function getRelatorioId()
            {
                return $this->relatorioId;
            }

public function getRelatorio_id_INT()
                {
                    return $this->relatorioId;
                }

public function getDescricao()
            {
                return $this->descricao;
            }

public function getVencimentoSec()
            {
                return $this->vencimentoSec;
            }

public function getVencimento_SEC()
                {
                    return $this->vencimentoSec;
                }

public function getVencimentoOffsec()
            {
                return $this->vencimentoOffsec;
            }

public function getVencimento_OFFSEC()
                {
                    return $this->vencimentoOffsec;
                }

public function getFechamentoSec()
            {
                return $this->fechamentoSec;
            }

public function getFechamento_SEC()
                {
                    return $this->fechamentoSec;
                }

public function getFechamentoOffsec()
            {
                return $this->fechamentoOffsec;
            }

public function getFechamento_OFFSEC()
                {
                    return $this->fechamentoOffsec;
                }

public function getValorPagoFloat()
            {
                return $this->valorPagoFloat;
            }

public function getValor_pago_FLOAT()
                {
                    return $this->valorPagoFloat;
                }

public function getMesaId()
            {
                return $this->mesaId;
            }

public function getMesa_id_INT()
                {
                    return $this->mesaId;
                }

public function getIdentificador()
            {
                return $this->identificador;
            }

public function getProtocoloInt()
            {
                return $this->protocoloInt;
            }

public function getProtocolo_INT()
                {
                    return $this->protocoloInt;
                }

public function getCorporacaoId()
            {
                return $this->corporacaoId;
            }

public function getCorporacao_id_INT()
                {
                    return $this->corporacaoId;
                }



function setId($value)
            {
                $this->id = $value;
            }

function setFormaPagamentoId($value)
            {
                $this->formaPagamentoId = $value;
            }

public function setForma_pagamento_id_INT($value)
                { 
                    $this->formaPagamentoId = $value; 
                }

function setValorTotalFloat($value)
            {
                $this->valorTotalFloat = $value;
            }

public function setValor_total_FLOAT($value)
                { 
                    $this->valorTotalFloat = $value; 
                }

function setDescontoFloat($value)
            {
                $this->descontoFloat = $value;
            }

public function setDesconto_FLOAT($value)
                { 
                    $this->descontoFloat = $value; 
                }

function setDataSec($value)
            {
                $this->dataSec = $value;
            }

public function setData_SEC($value)
                { 
                    $this->dataSec = $value; 
                }

function setDataOffsec($value)
            {
                $this->dataOffsec = $value;
            }

public function setData_OFFSEC($value)
                { 
                    $this->dataOffsec = $value; 
                }

function setUsuarioId($value)
            {
                $this->usuarioId = $value;
            }

public function setUsuario_id_INT($value)
                { 
                    $this->usuarioId = $value; 
                }

function setClienteEmpresaId($value)
            {
                $this->clienteEmpresaId = $value;
            }

public function setCliente_empresa_id_INT($value)
                { 
                    $this->clienteEmpresaId = $value; 
                }

function setFornecedorEmpresaId($value)
            {
                $this->fornecedorEmpresaId = $value;
            }

public function setFornecedor_empresa_id_INT($value)
                { 
                    $this->fornecedorEmpresaId = $value; 
                }

function setPessoaId($value)
            {
                $this->pessoaId = $value;
            }

public function setPessoa_id_INT($value)
                { 
                    $this->pessoaId = $value; 
                }

function setFoto($value)
            {
                $this->foto = $value;
            }

function setAcrescimoFloat($value)
            {
                $this->acrescimoFloat = $value;
            }

public function setAcrescimo_FLOAT($value)
                { 
                    $this->acrescimoFloat = $value; 
                }

function setRegistroEstadoId($value)
            {
                $this->registroEstadoId = $value;
            }

public function setRegistro_estado_id_INT($value)
                { 
                    $this->registroEstadoId = $value; 
                }

function setRegistroEstadoCorporacaoId($value)
            {
                $this->registroEstadoCorporacaoId = $value;
            }

public function setRegistro_estado_corporacao_id_INT($value)
                { 
                    $this->registroEstadoCorporacaoId = $value; 
                }

function setRelatorioId($value)
            {
                $this->relatorioId = $value;
            }

public function setRelatorio_id_INT($value)
                { 
                    $this->relatorioId = $value; 
                }

function setDescricao($value)
            {
                $this->descricao = $value;
            }

function setVencimentoSec($value)
            {
                $this->vencimentoSec = $value;
            }

public function setVencimento_SEC($value)
                { 
                    $this->vencimentoSec = $value; 
                }

function setVencimentoOffsec($value)
            {
                $this->vencimentoOffsec = $value;
            }

public function setVencimento_OFFSEC($value)
                { 
                    $this->vencimentoOffsec = $value; 
                }

function setFechamentoSec($value)
            {
                $this->fechamentoSec = $value;
            }

public function setFechamento_SEC($value)
                { 
                    $this->fechamentoSec = $value; 
                }

function setFechamentoOffsec($value)
            {
                $this->fechamentoOffsec = $value;
            }

public function setFechamento_OFFSEC($value)
                { 
                    $this->fechamentoOffsec = $value; 
                }

function setValorPagoFloat($value)
            {
                $this->valorPagoFloat = $value;
            }

public function setValor_pago_FLOAT($value)
                { 
                    $this->valorPagoFloat = $value; 
                }

function setMesaId($value)
            {
                $this->mesaId = $value;
            }

public function setMesa_id_INT($value)
                { 
                    $this->mesaId = $value; 
                }

function setIdentificador($value)
            {
                $this->identificador = $value;
            }

function setProtocoloInt($value)
            {
                $this->protocoloInt = $value;
            }

public function setProtocolo_INT($value)
                { 
                    $this->protocoloInt = $value; 
                }

function setCorporacaoId($value)
            {
                $this->corporacaoId = $value;
            }

public function setCorporacao_id_INT($value)
                { 
                    $this->corporacaoId = $value; 
                }



public function clear()
        {$this->id = null;
$this->formaPagamentoId = null;
if($this->objForma_pagamento != null) unset($this->objForma_pagamento);
$this->valorTotalFloat = null;
$this->descontoFloat = null;
$this->dataSec = null;
$this->dataOffsec = null;
$this->usuarioId = null;
if($this->objUsuario != null) unset($this->objUsuario);
$this->clienteEmpresaId = null;
if($this->objCliente_empresa != null) unset($this->objCliente_empresa);
$this->fornecedorEmpresaId = null;
if($this->objFornecedor_empresa != null) unset($this->objFornecedor_empresa);
$this->pessoaId = null;
if($this->objPessoa != null) unset($this->objPessoa);
$this->foto = null;
$this->acrescimoFloat = null;
$this->registroEstadoId = null;
if($this->objRegistro_estado != null) unset($this->objRegistro_estado);
$this->registroEstadoCorporacaoId = null;
if($this->objRegistro_estado_corporacao != null) unset($this->objRegistro_estado_corporacao);
$this->relatorioId = null;
if($this->objRelatorio != null) unset($this->objRelatorio);
$this->descricao = null;
$this->vencimentoSec = null;
$this->vencimentoOffsec = null;
$this->fechamentoSec = null;
$this->fechamentoOffsec = null;
$this->valorPagoFloat = null;
$this->mesaId = null;
if($this->objMesa != null) unset($this->objMesa);
$this->identificador = null;
$this->protocoloInt = null;
$this->corporacaoId = null;
if($this->objCorporacao != null) unset($this->objCorporacao);
if(is_array($this->arrEstadosCampos)){
                        unset($this->arrEstadosCampos);
                     $this->arrEstadosCampos = array();
                    }
                }

public function formatarParaSQL()
        {
if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->id)){
$this->id = $this->formatarIntegerParaComandoSQL($this->id);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->id);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->formaPagamentoId)){
$this->formaPagamentoId = $this->formatarIntegerParaComandoSQL($this->formaPagamentoId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->formaPagamentoId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->valorTotalFloat)){
$this->valorTotalFloat = $this->formatarFloatParaComandoSQL($this->valorTotalFloat);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->valorTotalFloat);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->descontoFloat)){
$this->descontoFloat = $this->formatarFloatParaComandoSQL($this->descontoFloat);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->descontoFloat);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->dataSec)){
$this->dataSec = $this->formatarIntegerParaComandoSQL($this->dataSec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->dataSec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->dataOffsec)){
$this->dataOffsec = $this->formatarIntegerParaComandoSQL($this->dataOffsec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->dataOffsec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->usuarioId)){
$this->usuarioId = $this->formatarIntegerParaComandoSQL($this->usuarioId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->usuarioId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->clienteEmpresaId)){
$this->clienteEmpresaId = $this->formatarIntegerParaComandoSQL($this->clienteEmpresaId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->clienteEmpresaId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->fornecedorEmpresaId)){
$this->fornecedorEmpresaId = $this->formatarIntegerParaComandoSQL($this->fornecedorEmpresaId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->fornecedorEmpresaId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->pessoaId)){
$this->pessoaId = $this->formatarIntegerParaComandoSQL($this->pessoaId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->pessoaId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->foto)){
$this->foto = $this->formatarStringParaComandoSQL($this->foto);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->foto);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->acrescimoFloat)){
$this->acrescimoFloat = $this->formatarFloatParaComandoSQL($this->acrescimoFloat);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->acrescimoFloat);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->registroEstadoId)){
$this->registroEstadoId = $this->formatarIntegerParaComandoSQL($this->registroEstadoId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->registroEstadoId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->registroEstadoCorporacaoId)){
$this->registroEstadoCorporacaoId = $this->formatarIntegerParaComandoSQL($this->registroEstadoCorporacaoId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->registroEstadoCorporacaoId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->relatorioId)){
$this->relatorioId = $this->formatarIntegerParaComandoSQL($this->relatorioId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->relatorioId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->descricao)){
$this->descricao = $this->formatarStringParaComandoSQL($this->descricao);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->descricao);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->vencimentoSec)){
$this->vencimentoSec = $this->formatarIntegerParaComandoSQL($this->vencimentoSec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->vencimentoSec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->vencimentoOffsec)){
$this->vencimentoOffsec = $this->formatarIntegerParaComandoSQL($this->vencimentoOffsec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->vencimentoOffsec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->fechamentoSec)){
$this->fechamentoSec = $this->formatarIntegerParaComandoSQL($this->fechamentoSec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->fechamentoSec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->fechamentoOffsec)){
$this->fechamentoOffsec = $this->formatarIntegerParaComandoSQL($this->fechamentoOffsec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->fechamentoOffsec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->valorPagoFloat)){
$this->valorPagoFloat = $this->formatarFloatParaComandoSQL($this->valorPagoFloat);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->valorPagoFloat);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->mesaId)){
$this->mesaId = $this->formatarIntegerParaComandoSQL($this->mesaId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->mesaId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->identificador)){
$this->identificador = $this->formatarStringParaComandoSQL($this->identificador);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->identificador);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->protocoloInt)){
$this->protocoloInt = $this->formatarIntegerParaComandoSQL($this->protocoloInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->protocoloInt);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->corporacaoId)){
$this->corporacaoId = $this->formatarIntegerParaComandoSQL($this->corporacaoId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->corporacaoId);
}

}

public function formatarParaExibicao()
        {if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->valorTotalFloat)){
$this->valorTotalFloat = $this->formatarFloatParaExibicao($this->valorTotalFloat);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->valorTotalFloat);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->descontoFloat)){
$this->descontoFloat = $this->formatarFloatParaExibicao($this->descontoFloat);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->descontoFloat);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->foto)){
$this->foto = $this->formatarStringParaExibicao($this->foto);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->foto);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->acrescimoFloat)){
$this->acrescimoFloat = $this->formatarFloatParaExibicao($this->acrescimoFloat);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->acrescimoFloat);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->descricao)){
$this->descricao = $this->formatarStringParaExibicao($this->descricao);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->descricao);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->valorPagoFloat)){
$this->valorPagoFloat = $this->formatarFloatParaExibicao($this->valorPagoFloat);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->valorPagoFloat);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->identificador)){
$this->identificador = $this->formatarStringParaExibicao($this->identificador);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->identificador);
}

}

function select($id, $idCorporacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }   

            $sql = "SELECT id, forma_pagamento_id_INT, valor_total_FLOAT, desconto_FLOAT, data_SEC, data_OFFSEC, usuario_id_INT, cliente_empresa_id_INT, fornecedor_empresa_id_INT, pessoa_id_INT, foto, acrescimo_FLOAT, registro_estado_id_INT, registro_estado_corporacao_id_INT, relatorio_id_INT, descricao, vencimento_SEC, vencimento_OFFSEC, fechamento_SEC, fechamento_OFFSEC, valor_pago_FLOAT, mesa_id_INT, identificador, protocolo_INT, corporacao_id_INT FROM empresa_venda WHERE id = $id AND corporacao_id_INT = $idCorporacao";

            $msg = $this->database->queryMensagem($sql);
            $result = $this->database->result;

            if($msg != null && $msg->erro() )
            { 
            		$this->clear();
            		return $msg;
        	} 
        	else if($this->database->rows() == 0)
        	{
                return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nenhum registro foi encontrado."));
            }
        

            $row = $this->database->fetchArray(MYSQLI_NUM);
            
            		$this->id = $row[0];
		$this->formaPagamentoId = $row[1];
		$this->valorTotalFloat = $row[2];
		$this->descontoFloat = $row[3];
		$this->dataSec = $row[4];
		$this->dataOffsec = $row[5];
		$this->usuarioId = $row[6];
		$this->clienteEmpresaId = $row[7];
		$this->fornecedorEmpresaId = $row[8];
		$this->pessoaId = $row[9];
		$this->foto = $row[10];
		$this->acrescimoFloat = $row[11];
		$this->registroEstadoId = $row[12];
		$this->registroEstadoCorporacaoId = $row[13];
		$this->relatorioId = $row[14];
		$this->descricao = $row[15];
		$this->vencimentoSec = $row[16];
		$this->vencimentoOffsec = $row[17];
		$this->fechamentoSec = $row[18];
		$this->fechamentoOffsec = $row[19];
		$this->valorPagoFloat = $row[20];
		$this->mesaId = $row[21];
		$this->identificador = $row[22];
		$this->protocoloInt = $row[23];

            
        }

public function delete($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (is_null($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }

            $sql = "DELETE FROM empresa_venda WHERE id= $id AND corporacao_id_INT = $idCorporacao";
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                    && $sincronizar
                        && static::idSistemaTabela() != null
                            && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_REMOVE,
                                null,
                                null,
                                $id,
                                $this->database);
            }

            return $msg;
        }

public function insert($sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null, $opcoes = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            
            
            if (is_null($opcoes))
            {
                $opcoes = $this->opcoesDAO;
            }

            if (!is_numeric($this->id))
            {
                $this->id = "null";
                if ($opcoes == null || $opcoes[Database::OPCAO_GERAR_ID] != false) 
                {
                    $this->id = EXTDAO_Sistema_sequencia::gerarId(static::nomeTabela());                    
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }    
                    else if($msg == null)
                    {
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
                else 
                {
                    $this->id = null;
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao));
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }
                    else if($msg == null)
                    {
                        $this->id = $this->database->getLastInsertId();
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
            }
            else 
            {
                $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }                        
                else if($msg == null)
                {
                   $msg = new Mensagem_token();
                   $msg->mValor = $this->id;
                }
            }
            if($msg == null || $msg->ok())
            {                
                if($sincronizar
                    && static::idSistemaTabela() != null
                    && isset($this->id)){
                    
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_INSERT,
                                null,
                                null,
                                $this->id,
                                $this->database); 
                	}
            } 
            else
            {
                $this->id = null;
            }
            
            return $msg;
            
        }

public function getSQLInsert($idCorporacao){ 

return "INSERT INTO empresa_venda (id, forma_pagamento_id_INT, valor_total_FLOAT, desconto_FLOAT, data_SEC, data_OFFSEC, usuario_id_INT, cliente_empresa_id_INT, fornecedor_empresa_id_INT, pessoa_id_INT, foto, acrescimo_FLOAT, registro_estado_id_INT, registro_estado_corporacao_id_INT, relatorio_id_INT, descricao, vencimento_SEC, vencimento_OFFSEC, fechamento_SEC, fechamento_OFFSEC, valor_pago_FLOAT, mesa_id_INT, identificador, protocolo_INT, corporacao_id_INT) VALUES ( $this->id ,  $this->formaPagamentoId ,  $this->valorTotalFloat ,  $this->descontoFloat ,  $this->dataSec ,  $this->dataOffsec ,  $this->usuarioId ,  $this->clienteEmpresaId ,  $this->fornecedorEmpresaId ,  $this->pessoaId ,  $this->foto ,  $this->acrescimoFloat ,  $this->registroEstadoId ,  $this->registroEstadoCorporacaoId ,  $this->relatorioId ,  $this->descricao ,  $this->vencimentoSec ,  $this->vencimentoOffsec ,  $this->fechamentoSec ,  $this->fechamentoOffsec ,  $this->valorPagoFloat ,  $this->mesaId ,  $this->identificador ,  $this->protocoloInt ,  $idCorporacao ) "; 

}




        
        public function updateByParameters($id, $objParametros, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao))
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();

            
                
                if (isset($objParametros->formaPagamentoId)) 
                {
                    $arrUpdateFields[] = " forma_pagamento_id_INT = {$objParametros->formaPagamentoId} ";
                }


                
                if (isset($objParametros->valorTotalFloat)) 
                {
                    $arrUpdateFields[] = " valor_total_FLOAT = {$objParametros->valorTotalFloat} ";
                }


                
                if (isset($objParametros->descontoFloat)) 
                {
                    $arrUpdateFields[] = " desconto_FLOAT = {$objParametros->descontoFloat} ";
                }


                
                if (isset($objParametros->dataSec)) 
                {
                    $arrUpdateFields[] = " data_SEC = {$objParametros->dataSec} ";
                }


                
                if (isset($objParametros->dataOffsec)) 
                {
                    $arrUpdateFields[] = " data_OFFSEC = {$objParametros->dataOffsec} ";
                }


                
                if (isset($objParametros->usuarioId)) 
                {
                    $arrUpdateFields[] = " usuario_id_INT = {$objParametros->usuarioId} ";
                }


                
                if (isset($objParametros->clienteEmpresaId)) 
                {
                    $arrUpdateFields[] = " cliente_empresa_id_INT = {$objParametros->clienteEmpresaId} ";
                }


                
                if (isset($objParametros->fornecedorEmpresaId)) 
                {
                    $arrUpdateFields[] = " fornecedor_empresa_id_INT = {$objParametros->fornecedorEmpresaId} ";
                }


                
                if (isset($objParametros->pessoaId)) 
                {
                    $arrUpdateFields[] = " pessoa_id_INT = {$objParametros->pessoaId} ";
                }


                
                if (isset($objParametros->foto)) 
                {
                    $arrUpdateFields[] = " foto = {$objParametros->foto} ";
                }


                
                if (isset($objParametros->acrescimoFloat)) 
                {
                    $arrUpdateFields[] = " acrescimo_FLOAT = {$objParametros->acrescimoFloat} ";
                }


                
                if (isset($objParametros->registroEstadoId)) 
                {
                    $arrUpdateFields[] = " registro_estado_id_INT = {$objParametros->registroEstadoId} ";
                }


                
                if (isset($objParametros->registroEstadoCorporacaoId)) 
                {
                    $arrUpdateFields[] = " registro_estado_corporacao_id_INT = {$objParametros->registroEstadoCorporacaoId} ";
                }


                
                if (isset($objParametros->relatorioId)) 
                {
                    $arrUpdateFields[] = " relatorio_id_INT = {$objParametros->relatorioId} ";
                }


                
                if (isset($objParametros->descricao)) 
                {
                    $arrUpdateFields[] = " descricao = {$objParametros->descricao} ";
                }


                
                if (isset($objParametros->vencimentoSec)) 
                {
                    $arrUpdateFields[] = " vencimento_SEC = {$objParametros->vencimentoSec} ";
                }


                
                if (isset($objParametros->vencimentoOffsec)) 
                {
                    $arrUpdateFields[] = " vencimento_OFFSEC = {$objParametros->vencimentoOffsec} ";
                }


                
                if (isset($objParametros->fechamentoSec)) 
                {
                    $arrUpdateFields[] = " fechamento_SEC = {$objParametros->fechamentoSec} ";
                }


                
                if (isset($objParametros->fechamentoOffsec)) 
                {
                    $arrUpdateFields[] = " fechamento_OFFSEC = {$objParametros->fechamentoOffsec} ";
                }


                
                if (isset($objParametros->valorPagoFloat)) 
                {
                    $arrUpdateFields[] = " valor_pago_FLOAT = {$objParametros->valorPagoFloat} ";
                }


                
                if (isset($objParametros->mesaId)) 
                {
                    $arrUpdateFields[] = " mesa_id_INT = {$objParametros->mesaId} ";
                }


                
                if (isset($objParametros->identificador)) 
                {
                    $arrUpdateFields[] = " identificador = {$objParametros->identificador} ";
                }


                
                if (isset($objParametros->protocoloInt)) 
                {
                    $arrUpdateFields[] = " protocolo_INT = {$objParametros->protocoloInt} ";
                }



            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE empresa_venda SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";

            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null)
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function update($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($this->formaPagamentoId)) 
                {                                      
                    $arrUpdateFields[] = " forma_pagamento_id_INT = {$this->formaPagamentoId} ";
                }


                
                if (isset($this->valorTotalFloat)) 
                {                                      
                    $arrUpdateFields[] = " valor_total_FLOAT = {$this->valorTotalFloat} ";
                }


                
                if (isset($this->descontoFloat)) 
                {                                      
                    $arrUpdateFields[] = " desconto_FLOAT = {$this->descontoFloat} ";
                }


                
                if (isset($this->dataSec)) 
                {                                      
                    $arrUpdateFields[] = " data_SEC = {$this->dataSec} ";
                }


                
                if (isset($this->dataOffsec)) 
                {                                      
                    $arrUpdateFields[] = " data_OFFSEC = {$this->dataOffsec} ";
                }


                
                if (isset($this->usuarioId)) 
                {                                      
                    $arrUpdateFields[] = " usuario_id_INT = {$this->usuarioId} ";
                }


                
                if (isset($this->clienteEmpresaId)) 
                {                                      
                    $arrUpdateFields[] = " cliente_empresa_id_INT = {$this->clienteEmpresaId} ";
                }


                
                if (isset($this->fornecedorEmpresaId)) 
                {                                      
                    $arrUpdateFields[] = " fornecedor_empresa_id_INT = {$this->fornecedorEmpresaId} ";
                }


                
                if (isset($this->pessoaId)) 
                {                                      
                    $arrUpdateFields[] = " pessoa_id_INT = {$this->pessoaId} ";
                }


                
                if (isset($this->foto)) 
                {                                      
                    $arrUpdateFields[] = " foto = {$this->foto} ";
                }


                
                if (isset($this->acrescimoFloat)) 
                {                                      
                    $arrUpdateFields[] = " acrescimo_FLOAT = {$this->acrescimoFloat} ";
                }


                
                if (isset($this->registroEstadoId)) 
                {                                      
                    $arrUpdateFields[] = " registro_estado_id_INT = {$this->registroEstadoId} ";
                }


                
                if (isset($this->registroEstadoCorporacaoId)) 
                {                                      
                    $arrUpdateFields[] = " registro_estado_corporacao_id_INT = {$this->registroEstadoCorporacaoId} ";
                }


                
                if (isset($this->relatorioId)) 
                {                                      
                    $arrUpdateFields[] = " relatorio_id_INT = {$this->relatorioId} ";
                }


                
                if (isset($this->descricao)) 
                {                                      
                    $arrUpdateFields[] = " descricao = {$this->descricao} ";
                }


                
                if (isset($this->vencimentoSec)) 
                {                                      
                    $arrUpdateFields[] = " vencimento_SEC = {$this->vencimentoSec} ";
                }


                
                if (isset($this->vencimentoOffsec)) 
                {                                      
                    $arrUpdateFields[] = " vencimento_OFFSEC = {$this->vencimentoOffsec} ";
                }


                
                if (isset($this->fechamentoSec)) 
                {                                      
                    $arrUpdateFields[] = " fechamento_SEC = {$this->fechamentoSec} ";
                }


                
                if (isset($this->fechamentoOffsec)) 
                {                                      
                    $arrUpdateFields[] = " fechamento_OFFSEC = {$this->fechamentoOffsec} ";
                }


                
                if (isset($this->valorPagoFloat)) 
                {                                      
                    $arrUpdateFields[] = " valor_pago_FLOAT = {$this->valorPagoFloat} ";
                }


                
                if (isset($this->mesaId)) 
                {                                      
                    $arrUpdateFields[] = " mesa_id_INT = {$this->mesaId} ";
                }


                
                if (isset($this->identificador)) 
                {                                      
                    $arrUpdateFields[] = " identificador = {$this->identificador} ";
                }


                
                if (isset($this->protocoloInt)) 
                {                                      
                    $arrUpdateFields[] = " protocolo_INT = {$this->protocoloInt} ";
                }


            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE empresa_venda SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function updateByInstanceUsingParameters($id, $parameters, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($parameters->formaPagamentoId)) 
                {                                      
                    $arrUpdateFields[] = " forma_pagamento_id_INT = {$this->formaPagamentoId} ";
                }
                
                if (isset($parameters->valorTotalFloat)) 
                {                                      
                    $arrUpdateFields[] = " valor_total_FLOAT = {$this->valorTotalFloat} ";
                }
                
                if (isset($parameters->descontoFloat)) 
                {                                      
                    $arrUpdateFields[] = " desconto_FLOAT = {$this->descontoFloat} ";
                }
                
                if (isset($parameters->dataSec)) 
                {                                      
                    $arrUpdateFields[] = " data_SEC = {$this->dataSec} ";
                }
                
                if (isset($parameters->dataOffsec)) 
                {                                      
                    $arrUpdateFields[] = " data_OFFSEC = {$this->dataOffsec} ";
                }
                
                if (isset($parameters->usuarioId)) 
                {                                      
                    $arrUpdateFields[] = " usuario_id_INT = {$this->usuarioId} ";
                }
                
                if (isset($parameters->clienteEmpresaId)) 
                {                                      
                    $arrUpdateFields[] = " cliente_empresa_id_INT = {$this->clienteEmpresaId} ";
                }
                
                if (isset($parameters->fornecedorEmpresaId)) 
                {                                      
                    $arrUpdateFields[] = " fornecedor_empresa_id_INT = {$this->fornecedorEmpresaId} ";
                }
                
                if (isset($parameters->pessoaId)) 
                {                                      
                    $arrUpdateFields[] = " pessoa_id_INT = {$this->pessoaId} ";
                }
                
                if (isset($parameters->foto)) 
                {                                      
                    $arrUpdateFields[] = " foto = {$this->foto} ";
                }
                
                if (isset($parameters->acrescimoFloat)) 
                {                                      
                    $arrUpdateFields[] = " acrescimo_FLOAT = {$this->acrescimoFloat} ";
                }
                
                if (isset($parameters->registroEstadoId)) 
                {                                      
                    $arrUpdateFields[] = " registro_estado_id_INT = {$this->registroEstadoId} ";
                }
                
                if (isset($parameters->registroEstadoCorporacaoId)) 
                {                                      
                    $arrUpdateFields[] = " registro_estado_corporacao_id_INT = {$this->registroEstadoCorporacaoId} ";
                }
                
                if (isset($parameters->relatorioId)) 
                {                                      
                    $arrUpdateFields[] = " relatorio_id_INT = {$this->relatorioId} ";
                }
                
                if (isset($parameters->descricao)) 
                {                                      
                    $arrUpdateFields[] = " descricao = {$this->descricao} ";
                }
                
                if (isset($parameters->vencimentoSec)) 
                {                                      
                    $arrUpdateFields[] = " vencimento_SEC = {$this->vencimentoSec} ";
                }
                
                if (isset($parameters->vencimentoOffsec)) 
                {                                      
                    $arrUpdateFields[] = " vencimento_OFFSEC = {$this->vencimentoOffsec} ";
                }
                
                if (isset($parameters->fechamentoSec)) 
                {                                      
                    $arrUpdateFields[] = " fechamento_SEC = {$this->fechamentoSec} ";
                }
                
                if (isset($parameters->fechamentoOffsec)) 
                {                                      
                    $arrUpdateFields[] = " fechamento_OFFSEC = {$this->fechamentoOffsec} ";
                }
                
                if (isset($parameters->valorPagoFloat)) 
                {                                      
                    $arrUpdateFields[] = " valor_pago_FLOAT = {$this->valorPagoFloat} ";
                }
                
                if (isset($parameters->mesaId)) 
                {                                      
                    $arrUpdateFields[] = " mesa_id_INT = {$this->mesaId} ";
                }
                
                if (isset($parameters->identificador)) 
                {                                      
                    $arrUpdateFields[] = " identificador = {$this->identificador} ";
                }
                
                if (isset($parameters->protocoloInt)) 
                {                                      
                    $arrUpdateFields[] = " protocolo_INT = {$this->protocoloInt} ";
                }
            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE empresa_venda SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }

}

?>
