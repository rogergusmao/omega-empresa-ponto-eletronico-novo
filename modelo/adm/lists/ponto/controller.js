/*

 Arquivo gerado atrav�s de gerador de c�digo em 11/12/2017 as 22:44:38.
 Para que o arquivo n�o seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

 Tabela correspondente: ponto
 Sobrescrita de dados: ---SOBRESCRITA_NAO_AUTORIZADA---

 */

omegaApp.controller('listPontoController',

    ['$scope', '$rootScope', '$element', '$window', '$interval', '$timeout', 'dataService', 'Upload',

     function ($scope, $rootScope, $element, $window, $interval, $timeout, dataService, Upload)
     {
         var vm = this;
         try
         {
             vm.rootControllerElement = $element;
             vm.fieldsTypes = {};
             vm.isModalView = false;

             vm.disabledFilterFields = {
                 fieldId: false,
                 fieldPessoaId: false,
                 fieldEmpresaId: false,
                 fieldIsEntradaBoolean: false,
                 fieldTipoPontoId: false,
                 fieldDataSecDe: false,
                 fieldDataSecAte: false
             };

             vm.requiredFilterFields = {
                 fieldId: false,
                 fieldPessoaId: false,
                 fieldEmpresaId: false,
                 fieldIsEntradaBoolean: false,
                 fieldTipoPontoId: false,
                 fieldDataSecDe: false,
                 fieldDataSecAte: false
             };

             vm.filterFieldsParameters = {
                 fieldId: null,
                 fieldPessoaId: {remoteData: null},
                 fieldEmpresaId: {remoteData: null},
                 fieldIsEntradaBoolean: {options: angular.copy(FieldsParametersUtil.getDefaultSelectForBooleanOptions())},
                 fieldTipoPontoId: {remoteData: null},
                 fieldDataSecDe: {
                     popupAberto: false,
                     datePickerParams: angular.copy(FieldsParametersUtil.defaultDatePickerParams)
                 },
                 fieldDataSecAte: {
                     popupAberto: false,
                     datePickerParams: angular.copy(FieldsParametersUtil.defaultDatePickerParams)
                 }
             };

             vm.filterData = {
                 id: null,
                 pessoaId: null,
                 empresaId: null,
                 isEntradaBoolean: null,
                 tipoPontoId: null,
                 dataSecDe: null,
                 dataSecAte: null
             };

             vm.filterOperators = {
                 id: Constants.FilterOperator.EQUALS,
                 pessoaId: Constants.FilterOperator.EQUALS,
                 empresaId: Constants.FilterOperator.EQUALS,
                 isEntradaBoolean: Constants.FilterOperator.EQUALS,
                 tipoPontoId: Constants.FilterOperator.EQUALS,
                 dataSecDe: Constants.FilterOperator.GREATER_THAN_OR_EQUALS,
                 dataSecAte: Constants.FilterOperator.LESS_THAN_OR_EQUALS
             };

             vm.sortingClass = {
                 id: Constants.ListSortingClass.IDLE,
                 pessoaId: Constants.ListSortingClass.IDLE,
                 empresaId: Constants.ListSortingClass.IDLE,
                 isEntradaBoolean: Constants.ListSortingClass.IDLE,
                 tipoPontoId: Constants.ListSortingClass.IDLE,
                 dataSec: Constants.ListSortingClass.IDLE
             };

             vm.paginationParameters = {
                 currentPage: 1,
                 recordsPerPage: Constants.LIST_DEFAULT_NUMBER_OF_RECORDS_PER_PAGE,
                 lastPage: null,
                 visibleButtonsArray: []
             };

             vm.sortingParameters = [];

             var relationshipObject = {
                 pessoaId: 'fieldPessoaId',
                 empresaId: 'fieldEmpresaId',
                 tipoPontoId: 'fieldTipoPontoId'
             };

             vm.isInitialLoadFinished = false;
             $timeout(function ()
             {
                 dataService.getAllComboBoxesDataForPontoFilter().then(function (returnContent)
                 {
                     if (RemoteDataUtil.validateRemoteResponse(returnContent))
                     {
                         var responseObject = RemoteDataUtil.getResponseObject(returnContent);
                         RemoteDataUtil.refactorInitialFilterLoadRemoteResponseData(vm, responseObject, relationshipObject);
                     }
                     else
                     {
                         LogUtil.logRemoteResponseError(returnContent);
                     }

                     vm.isInitialLoadFinished = true;
                     vm.getRemoteListDataFromQueryString();

                 });
             }, 200);

             $rootScope.filterListWatch(vm);

             vm.getRemoteListDataFromQueryString = function ()
             {
                 var listParametersObject = OmegaListUtil.getListParametersFromBrowseUrlQueryString();

                 if (!GeneralUtil.isNullOrEmptyObject(listParametersObject.preservedFilterData))
                 {
                     vm.filterData = listParametersObject.preservedFilterData;
                 }

                 var parametersObject = {};
                 if (!GeneralUtil.isNullOrEmptyObject(listParametersObject.filterParameters))
                 {
                     parametersObject.filterParameters = listParametersObject.filterParameters;
                 }
                 else
                 {
                     parametersObject.filterParameters = GeneralUtil.getFilterParameters({}, vm.filterOperators)
                 }

                 if (!GeneralUtil.isNullOrEmptyObject(listParametersObject.sortingParameters))
                 {
                     parametersObject.sortingParameters = listParametersObject.sortingParameters;
                 }
                 else
                 {
                     parametersObject.sortingParameters = vm.sortingParameters;
                 }

                 if (!GeneralUtil.isNullOrEmptyObject(listParametersObject.paginationParameters))
                 {
                     parametersObject.paginationParameters = listParametersObject.paginationParameters;
                 }
                 else
                 {
                     parametersObject.paginationParameters = vm.paginationParameters;
                 }

                 vm.applyFilterSortingAndPaginationParameters(parametersObject);

             };
             vm.getRemoteListData = function ()
             {
                 var filterData = RemoteDataUtil.refactorRemoteRequestForFilter(vm.preservedFilterData);
                 var parametersObject =
                     {
                         filterParameters: GeneralUtil.getFilterParameters(filterData, vm.filterOperators),
                         sortingParameters: vm.sortingParameters,
                         paginationParameters: vm.paginationParameters
                     };

                 vm.applyFilterSortingAndPaginationParameters(parametersObject);

             };

             vm.applyFilterSortingAndPaginationParameters = function (parametersObject)
             {
                 OmegaListUtil.addListParametersToBrowserUrlQueryString(parametersObject, vm.preservedFilterData);

                 dataService.getPontoList(parametersObject).then(function (returnContent)
                 {
                     if (RemoteDataUtil.validateRemoteResponseForList(returnContent))
                     {
                         var responseObject = RemoteDataUtil.getResponseObject(returnContent);

                         for(var i=0; i < responseObject.dataSet.length; i++)
                         {
                             var record = responseObject.dataSet[i];
                             var timestamp = parseInt(record.ponto__data_SEC);
                             var offset = parseInt(record.ponto__data_OFFSEC);
                             record.ponto__data_SEC_forHuman = DateUtil.getFormattedDateTime(timestamp + offset);
                         }

                         vm.originalDataSet = responseObject.dataSet;
                         vm.dataSet = angular.copy(vm.originalDataSet);
                         vm.isFiltered = true;

                         OmegaListUtil.doActionsAfterRemoteListDataReceived(vm, responseObject);
                     }
                     else
                     {
                         RemoteDataUtil.defaultRemoteErrorHandling(returnContent);
                     }
                 });

             };

             vm.getListOfPessoaId = function ()
             {
                 dataService.getListOfPessoaIdForPontoList().then(function (returnContent)
                 {
                     if (RemoteDataUtil.validateRemoteResponseForList(returnContent))
                     {
                         var remoteDataSet = RemoteDataUtil.getResponseObjectDataSetOrEmptyArray(returnContent);
                         vm.filterFieldsParameters.fieldPessoaId.remoteData = remoteDataSet;
                     }
                     else
                     {
                         LogUtil.logRemoteResponseError(returnContent);
                     }

                 });

             };

             vm.getListOfEmpresaId = function ()
             {
                 dataService.getListOfEmpresaIdForPontoList().then(function (returnContent)
                 {
                     if (RemoteDataUtil.validateRemoteResponseForList(returnContent))
                     {
                         var remoteDataSet = RemoteDataUtil.getResponseObjectDataSetOrEmptyArray(returnContent);
                         vm.filterFieldsParameters.fieldEmpresaId.remoteData = remoteDataSet;
                     }
                     else
                     {
                         LogUtil.logRemoteResponseError(returnContent);
                     }

                 });

             };

             vm.getListOfProfissaoId = function ()
             {
                 dataService.getListOfProfissaoIdForPontoList().then(function (returnContent)
                 {
                     if (RemoteDataUtil.validateRemoteResponseForList(returnContent))
                     {
                         var remoteDataSet = RemoteDataUtil.getResponseObjectDataSetOrEmptyArray(returnContent);
                         vm.filterFieldsParameters.fieldProfissaoId.remoteData = remoteDataSet;
                     }
                     else
                     {
                         LogUtil.logRemoteResponseError(returnContent);
                     }

                 });

             };

             vm.getListOfUsuarioId = function ()
             {
                 dataService.getListOfUsuarioIdForPontoList().then(function (returnContent)
                 {
                     if (RemoteDataUtil.validateRemoteResponseForList(returnContent))
                     {
                         var remoteDataSet = RemoteDataUtil.getResponseObjectDataSetOrEmptyArray(returnContent);
                         vm.filterFieldsParameters.fieldUsuarioId.remoteData = remoteDataSet;
                     }
                     else
                     {
                         LogUtil.logRemoteResponseError(returnContent);
                     }

                 });

             };

             vm.getListOfTipoPontoId = function ()
             {
                 dataService.getListOfTipoPontoIdForPontoList().then(function (returnContent)
                 {
                     if (RemoteDataUtil.validateRemoteResponseForList(returnContent))
                     {
                         var remoteDataSet = RemoteDataUtil.getResponseObjectDataSetOrEmptyArray(returnContent);
                         vm.filterFieldsParameters.fieldTipoPontoId.remoteData = remoteDataSet;
                     }
                     else
                     {
                         LogUtil.logRemoteResponseError(returnContent);
                     }

                 });

             };

             vm.getListOfCorporacaoId = function ()
             {
                 dataService.getListOfCorporacaoIdForPontoList().then(function (returnContent)
                 {
                     if (RemoteDataUtil.validateRemoteResponseForList(returnContent))
                     {
                         var remoteDataSet = RemoteDataUtil.getResponseObjectDataSetOrEmptyArray(returnContent);
                         vm.filterFieldsParameters.fieldCorporacaoId.remoteData = remoteDataSet;
                     }
                     else
                     {
                         LogUtil.logRemoteResponseError(returnContent);
                     }

                 });

             };

             vm.selectedRecords = [];
             vm.selectedRecordsForRemoval = [];

             vm.showRemoveMessageDialog = function (records)
             {
                 records = records || vm.selectedRecords;
                 vm.selectedRecordsForRemoval = angular.copy(records);

                 $rootScope.showDialog(vm, 'before-remove-template.html');
             };

             vm.closeRemoveMessageDialog = function ()
             {
                 vm.selectedRecordsForRemoval = [];
                 $rootScope.closeMessageDialog();
             };

             vm.removeSelectedRecords = function ()
             {
                 var parameters = vm.selectedRecordsForRemoval;
                 dataService.removeRecords(parameters).then(function (returnContent)
                 {
                     vm.closeRemoveMessageDialog();
                     if (RemoteDataUtil.validateRemoteResponse(returnContent))
                     {
                         vm.getRemoteListDataFromQueryString();
                     }
                     else
                     {
                         RemoteDataUtil.defaultRemoteErrorHandling(returnContent);
                     }
                 });
             };

             vm.editRecord = function (primaryKey)
             {
                 var parameters = $.param(primaryKey);
                 document.location = 'index.php?tipo=forms&page=ponto&' + parameters;
             };

         }
         catch (ex)
         {
             ApplicationUtil.showErrorMessage(ex.message, 'Erro no controller listPontoController', vm);
         }

     }]
);