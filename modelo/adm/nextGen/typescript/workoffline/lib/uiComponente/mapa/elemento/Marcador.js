/// <reference path="../../../__referencias/google.maps.markerwithlabel.d.ts" />
"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t;
    return { next: verb(0), "throw": verb(1), "return": verb(2) };
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
var uiComponente_1 = require("lib/uiComponente");
var geral_1 = require("lib/geral");
var Marcador = (function (_super) {
    __extends(Marcador, _super);
    function Marcador(contexto, latitude, longitude, tipoElemento) {
        var _this = _super.call(this, contexto, [new google.maps.LatLng(latitude, longitude)], tipoElemento) || this;
        if (!geral_1.GeoLocationHelper.isValidCoordinate(latitude, longitude)) {
            throw new Error("Coordenadas inválidas: " + latitude + ", " + longitude);
        }
        _this.latitude = latitude;
        _this.longitude = longitude;
        _this.marker = new MarkerWithLabel();
        return _this;
    }
    Marcador.prototype.setDefaultIconeObject = function (iconeUrl) {
        iconeUrl = iconeUrl || this.getDefaultIconePath();
        this.iconeObject = new uiComponente_1.Icone(iconeUrl);
    };
    Marcador.prototype.setVisible = function (visible) {
        this.marker.setVisible(visible);
    };
    Marcador.prototype.setIconeObject = function (icone) {
        this.iconeObject = icone;
    };
    Marcador.prototype.setDefaultLabelObject = function (texto) {
        this.labelObject = new uiComponente_1.Label(texto);
    };
    Marcador.prototype.setLabelObject = function (label) {
        this.labelObject = label;
    };
    Marcador.prototype.getDefaultIconePath = function () {
        return this.contexto.getDefaultMarkerFolder() + "map_mainroad.png";
    };
    Marcador.prototype.getGoogleMarkerInstance = function () {
        return this.marker;
    };
    Marcador.prototype.tryToSetHtmlElement = function () {
        return __awaiter(this, void 0, void 0, function () {
            var identificador, tituloOriginal, jQuerySet;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!this.isRendered()) return [3 /*break*/, 2];
                        identificador = uiComponente_1.MapaElemento.getNextIdentifier();
                        tituloOriginal = this.marker.getTitle();
                        this.marker.setTitle(identificador);
                        return [4 /*yield*/, geral_1.Helper.sleep(3000)];
                    case 1:
                        _a.sent();
                        jQuerySet = this.contexto.mapObject.$('div[title=' + identificador + ']');
                        if (jQuerySet.length > 0) {
                            this.htmlElement = jQuerySet[0];
                        }
                        this.marker.setTitle(tituloOriginal);
                        _a.label = 2;
                    case 2: return [2 /*return*/];
                }
            });
        });
    };
    Marcador.prototype.isRendered = function () {
        return this.marker != null && this.marker.getMap() != null;
    };
    Marcador.prototype.render = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                if (this.iconeObject == null) {
                    this.setDefaultIconeObject();
                }
                this.marker = new MarkerWithLabel({
                    icon: this.iconeObject,
                    position: new google.maps.LatLng(this.latitude, this.longitude),
                    draggable: false,
                    map: this.getGoogleMapInstance(),
                    labelContent: this.labelObject.text,
                    labelInBackground: true,
                    labelAnchor: this.iconeObject.labelOrigin,
                    labelClass: "map-marker-label " + uiComponente_1.MapaElemento.getNextIdentifier(),
                    labelVisible: this.labelObject != null
                });
                return [2 /*return*/];
            });
        });
    };
    Marcador.prototype.removeFromGoogleMap = function () {
        this.marker.setMap(null);
    };
    Marcador.prototype.updateState = function () {
        this.render();
    };
    Marcador.prototype.addClickListener = function (handler) {
        this.marker.addListener('click', handler);
    };
    return Marcador;
}(uiComponente_1.MapaElemento));
exports.Marcador = Marcador;
