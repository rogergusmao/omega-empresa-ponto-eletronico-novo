<?php


        /*

        Arquivo gerado através de gerador de código em 12/05/2018 as 14:51:06.
        Para que o arquivo não seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

        Tabela correspondente: tela
        Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

        */
        


class DAO_Tela extends Generic_DAO
                        {
                            const NOME_ENTIDADE = "tela";

                            public static $nomeTabela = null;
                            public static $idSistemaTabela = false;
                            public static $databaseFieldsRelatedAttributes = null;
	                        public static $databaseFieldTypes = null;
	                        public static $databaseFieldNames = null;
	                        public static $listAliasTypes = null;
	                        public static $listAliasRelatedAttributes = null;
	                        
	                        public $id;
public $nome;
public $telaEstadoId;
public $telaTipoId;
public $isAndroidBoolean;
public $isWebBoolean;
public $configuracaoJson;
public $dataCriacaoSec;
public $dataCriacaoOffsec;
public $dataModificacaoSec;
public $dataModificacaoOffsec;
public $modificacaoUsuarioId;
public $cricacaoUsuarioId;
public $ativaBoolean;
public $permissaoId;
public $areaMenuId;
public $corporacaoId;

public $labelId;
public $labelNome;
public $labelTelaEstadoId;
public $labelTelaTipoId;
public $labelIsAndroidBoolean;
public $labelIsWebBoolean;
public $labelConfiguracaoJson;
public $labelDataCriacaoSec;
public $labelDataCriacaoOffsec;
public $labelDataModificacaoSec;
public $labelDataModificacaoOffsec;
public $labelModificacaoUsuarioId;
public $labelCricacaoUsuarioId;
public $labelAtivaBoolean;
public $labelPermissaoId;
public $labelAreaMenuId;
public $labelCorporacaoId;

public static $arrPK = array("id");

public function __construct($configDAO = null)
        {
            parent::__construct($configDAO);
            static::__setStaticValues($this->database);           
        }

public static function idSistemaTabela($databaseObject = null){
            if(!(static::$idSistemaTabela))
            {
                static::$idSistemaTabela = EXTDAO_Sistema_tabela::getIdDaSistemaTabela(static::nomeTabela(), $databaseObject);
            }
            return static::$idSistemaTabela;
        }

public function getIdSistemaTabela(){
            return static::idSistemaTabela();
        }

public static function nomeTabela(){
            if(is_null(static::$nomeTabela))
            {
                static::$nomeTabela = "tela";
            }
            return static::$nomeTabela;
        }

public function getNomeTabela(){
            return static::nomeTabela();
        }

public static function __setStaticValues($databaseObject = null)
                   {    
                        static::nomeTabela();
                        
                        static::idSistemaTabela($databaseObject);
                   
                        static::setDatabaseFieldNames();
                        static::setDatabaseFieldTypes();
                        static::setDatabaseFieldsRelatedAttributeNames();
                        
                        static::setListAliasTypes();
                        static::setListAliasRelatedAttributes();
                   }

public function getExpressaoLabel()
                   {
                        return "{id}";
                   }

public static function setDatabaseFieldNames()
        {
            if(is_null(static::$databaseFieldNames))
            {
                static::$databaseFieldNames = new stdClass();
                static::$databaseFieldNames->id = "id";
static::$databaseFieldNames->nome = "nome";
static::$databaseFieldNames->telaEstadoId = "tela_estado_id_INT";
static::$databaseFieldNames->telaTipoId = "tela_tipo_id_INT";
static::$databaseFieldNames->isAndroidBoolean = "is_android_BOOLEAN";
static::$databaseFieldNames->isWebBoolean = "is_web_BOOLEAN";
static::$databaseFieldNames->configuracaoJson = "configuracao_json";
static::$databaseFieldNames->dataCriacaoSec = "data_criacao_SEC";
static::$databaseFieldNames->dataCriacaoOffsec = "data_criacao_OFFSEC";
static::$databaseFieldNames->dataModificacaoSec = "data_modificacao_SEC";
static::$databaseFieldNames->dataModificacaoOffsec = "data_modificacao_OFFSEC";
static::$databaseFieldNames->modificacaoUsuarioId = "modificacao_usuario_id_INT";
static::$databaseFieldNames->cricacaoUsuarioId = "cricacao_usuario_id_INT";
static::$databaseFieldNames->ativaBoolean = "ativa_BOOLEAN";
static::$databaseFieldNames->permissaoId = "permissao_id_INT";
static::$databaseFieldNames->areaMenuId = "area_menu_id_INT";
static::$databaseFieldNames->corporacaoId = "corporacao_id_INT";

            }
                      
        }

public static function setDatabaseFieldTypes()
        {
            if(is_null(static::$databaseFieldTypes))
            {
                static::$databaseFieldTypes = new stdClass();
                static::$databaseFieldTypes->id = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->nome = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->telaEstadoId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->telaTipoId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->isAndroidBoolean = static::TIPO_VARIAVEL_BOOLEAN;
static::$databaseFieldTypes->isWebBoolean = static::TIPO_VARIAVEL_BOOLEAN;
static::$databaseFieldTypes->configuracaoJson = static::TIPO_VARIAVEL_TEXT;
static::$databaseFieldTypes->dataCriacaoSec = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$databaseFieldTypes->dataCriacaoOffsec = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$databaseFieldTypes->dataModificacaoSec = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$databaseFieldTypes->dataModificacaoOffsec = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$databaseFieldTypes->modificacaoUsuarioId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->cricacaoUsuarioId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->ativaBoolean = static::TIPO_VARIAVEL_BOOLEAN;
static::$databaseFieldTypes->permissaoId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->areaMenuId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->corporacaoId = static::TIPO_VARIAVEL_INTEGER;

            }
            
        }

public static function setDatabaseFieldsRelatedAttributeNames()
        {
            if(is_null(static::$databaseFieldsRelatedAttributes))
            {
                static::$databaseFieldsRelatedAttributes = new stdClass();
                static::$databaseFieldsRelatedAttributes->id = "id";
static::$databaseFieldsRelatedAttributes->nome = "nome";
static::$databaseFieldsRelatedAttributes->tela_estado_id_INT = "telaEstadoId";
static::$databaseFieldsRelatedAttributes->tela_tipo_id_INT = "telaTipoId";
static::$databaseFieldsRelatedAttributes->is_android_BOOLEAN = "isAndroidBoolean";
static::$databaseFieldsRelatedAttributes->is_web_BOOLEAN = "isWebBoolean";
static::$databaseFieldsRelatedAttributes->configuracao_json = "configuracaoJson";
static::$databaseFieldsRelatedAttributes->data_criacao_SEC = "dataCriacaoSec";
static::$databaseFieldsRelatedAttributes->data_criacao_OFFSEC = "dataCriacaoOffsec";
static::$databaseFieldsRelatedAttributes->data_modificacao_SEC = "dataModificacaoSec";
static::$databaseFieldsRelatedAttributes->data_modificacao_OFFSEC = "dataModificacaoOffsec";
static::$databaseFieldsRelatedAttributes->modificacao_usuario_id_INT = "modificacaoUsuarioId";
static::$databaseFieldsRelatedAttributes->cricacao_usuario_id_INT = "cricacaoUsuarioId";
static::$databaseFieldsRelatedAttributes->ativa_BOOLEAN = "ativaBoolean";
static::$databaseFieldsRelatedAttributes->permissao_id_INT = "permissaoId";
static::$databaseFieldsRelatedAttributes->area_menu_id_INT = "areaMenuId";
static::$databaseFieldsRelatedAttributes->corporacao_id_INT = "corporacaoId";

            }           
        }

public function setAllFieldsToHumanFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["nome"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["tela_estado_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["tela_tipo_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["is_android_BOOLEAN"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["is_web_BOOLEAN"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["configuracao_json"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["data_criacao_SEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["data_criacao_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["data_modificacao_SEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["data_modificacao_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["modificacao_usuario_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["cricacao_usuario_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["ativa_BOOLEAN"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["permissao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["area_menu_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
}

public function setAllFieldsToDatabaseFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["nome"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["tela_estado_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["tela_tipo_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["is_android_BOOLEAN"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["is_web_BOOLEAN"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["configuracao_json"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["data_criacao_SEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["data_criacao_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["data_modificacao_SEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["data_modificacao_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["modificacao_usuario_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["cricacao_usuario_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["ativa_BOOLEAN"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["permissao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["area_menu_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
}

  public function getFkObjTela_estado() 
                {
                    if($this->objTela_estado == null)
                    {                        
                        $this->objTela_estado = new EXTDAO_Tela_estado_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getTela_estado_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objTela_estado->clear();
                    }
                    elseif($this->objTela_estado->getId() != $idFK)
                    {
                        $this->objTela_estado->select($idFK);
                    }
                    return $this->objTela_estado;
                }
  public function getFkObjTela_tipo() 
                {
                    if($this->objTela_tipo == null)
                    {                        
                        $this->objTela_tipo = new EXTDAO_Tela_tipo_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getTela_tipo_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objTela_tipo->clear();
                    }
                    elseif($this->objTela_tipo->getId() != $idFK)
                    {
                        $this->objTela_tipo->select($idFK);
                    }
                    return $this->objTela_tipo;
                }
  public function getFkObjModificacao_usuario() 
                {
                    if($this->objModificacao_usuario == null)
                    {                        
                        $this->objModificacao_usuario = new EXTDAO_Modificacao_usuario_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getModificacao_usuario_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objModificacao_usuario->clear();
                    }
                    elseif($this->objModificacao_usuario->getId() != $idFK)
                    {
                        $this->objModificacao_usuario->select($idFK);
                    }
                    return $this->objModificacao_usuario;
                }
  public function getFkObjCricacao_usuario() 
                {
                    if($this->objCricacao_usuario == null)
                    {                        
                        $this->objCricacao_usuario = new EXTDAO_Cricacao_usuario_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getCricacao_usuario_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objCricacao_usuario->clear();
                    }
                    elseif($this->objCricacao_usuario->getId() != $idFK)
                    {
                        $this->objCricacao_usuario->select($idFK);
                    }
                    return $this->objCricacao_usuario;
                }
  public function getFkObjPermissao() 
                {
                    if($this->objPermissao == null)
                    {                        
                        $this->objPermissao = new EXTDAO_Permissao_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getPermissao_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objPermissao->clear();
                    }
                    elseif($this->objPermissao->getId() != $idFK)
                    {
                        $this->objPermissao->select($idFK);
                    }
                    return $this->objPermissao;
                }
  public function getFkObjArea_menu() 
                {
                    if($this->objArea_menu == null)
                    {                        
                        $this->objArea_menu = new EXTDAO_Area_menu_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getArea_menu_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objArea_menu->clear();
                    }
                    elseif($this->objArea_menu->getId() != $idFK)
                    {
                        $this->objArea_menu->select($idFK);
                    }
                    return $this->objArea_menu;
                }
  public function getFkObjCorporacao() 
                {
                    if($this->objCorporacao == null)
                    {                        
                        $this->objCorporacao = new EXTDAO_Corporacao_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getCorporacao_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objCorporacao->clear();
                    }
                    elseif($this->objCorporacao->getId() != $idFK)
                    {
                        $this->objCorporacao->select($idFK);
                    }
                    return $this->objCorporacao;
                }


public function setLabels()
        {$this->labelId = "id";
$this->labelNome = "nome";
$this->labelTelaEstadoId = "telaestadoidINT";
$this->labelTelaTipoId = "telatipoidINT";
$this->labelIsAndroidBoolean = "isandroidBOOLEAN";
$this->labelIsWebBoolean = "iswebBOOLEAN";
$this->labelConfiguracaoJson = "configuracaojson";
$this->labelDataCriacaoSec = "";
$this->labelDataCriacaoOffsec = "";
$this->labelDataModificacaoSec = "";
$this->labelDataModificacaoOffsec = "";
$this->labelModificacaoUsuarioId = "modificacaousuarioidINT";
$this->labelCricacaoUsuarioId = "cricacaousuarioidINT";
$this->labelAtivaBoolean = "ativaBOOLEAN";
$this->labelPermissaoId = "permissaoidINT";
$this->labelAreaMenuId = "areamenuidINT";
$this->labelCorporacaoId = "corporacaoidINT";
}

public function __actionAdd($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                $this->setByObject($parameters);
                $this->setDataCadastroAndOffsetInSecondsFromUser($parameters);
                
                $this->formatarParaSQL();
                $msg = $this->insert(true);
                
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                return new Mensagem(null, I18N::getExpression("Tela adicionado com sucesso."));
                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionEdit($parameters = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try {

                $this->setByObject($parameters);
                $this->setAllFieldsToHumanFormat();
                
                $this->formatarParaSQL();
                $msg = $this->updateByInstanceUsingParameters($this->id, $parameters, true);
                if($msg != null && $msg->erro())
                {
                     return $msg;
                }
        
                return new Mensagem(null, I18N::getExpression("Tela editado com sucesso."));
            }
            catch(Exception $ex)
            {                
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionRemove($parameters = null)
        {
            $successulRemovalCount = 0;
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                foreach($parameters as $record)
                {
                    if(is_numeric($record->id))
                    {
                        $msg = $this->delete($record->id, true);
                        if($msg == null || $msg->ok())
                        {
                            $successulRemovalCount++;
                        }
                    }
                }
                
                if($successulRemovalCount > 0)
                {
                    $successMessage = count($successulRemovalCount) > 1 ? I18N::getExpression("{0} Tela foram removidos com sucesso.", $successulRemovalCount) : I18N::getExpression("Tela removido com sucesso.");
                    return new Mensagem(null, $successMessage);
                }
                else
                {
                    $errorMessage = count($parameters) > 1 ? I18N::getExpression("Falha ao remover Tela.") : I18N::getExpression("Falha ao remover Tela.");
                    return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, $errorMessage);                    
                }
            }
            catch (Exception $ex) 
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }

public function __getRecord($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }
            
            try
            {            
                $returnObject = new stdClass();            
                $whereClause = static::getWhereClauseForFilter($parameters->filterParameters);
                $query = "SELECT  id, nome, tela_estado_id_INT, tela_tipo_id_INT, is_android_BOOLEAN, is_web_BOOLEAN, configuracao_json, data_criacao_SEC, data_criacao_OFFSEC, data_modificacao_SEC, data_modificacao_OFFSEC, modificacao_usuario_id_INT, cricacao_usuario_id_INT, ativa_BOOLEAN, permissao_id_INT, area_menu_id_INT, corporacao_id_INT FROM tela {$whereClause}";
    
                $msg = $this->database->queryMensagem($query);                
                if ($msg == null)
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Não foi encontrado nenhum registro"));
                    }                    
                    $resultSet = $this->database->result;
                    $resultObject = static::mapResultSetDataToRecord($resultSet);
                    
                    $returnObject->formData = $resultObject;
                    $returnObject->comboBoxesData = null;
                    if($parameters->loadComboBoxesData)
                    {
                        $this->setByObject($resultObject);
                        $returnObject->comboBoxesData =  $this->getFormComboBoxesData();
                    }                    
    
                    
    
                    return new Mensagem_generica($returnObject);
                } 
                else 
                {
                    return $msg;
                }
                
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }


        
        public function __getFilterComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
                
        public function __getFormComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
        
        protected function getFormComboBoxesData()
        {
            $comboBoxesData = new stdClass();
            $listParameters = new stdClass();
            $listParameters->listMappingType = static::LIST_MAPPING_TYPE_COMBOBOX;
            
            
                    
                    $objTelaEstado = new EXTDAO_Tela_estado();
                    $comboBoxesData->fieldTelaEstadoId = $objTelaEstado->__getList($listParameters);
                    
                    $objTelaTipo = new EXTDAO_Tela_tipo();
                    $comboBoxesData->fieldTelaTipoId = $objTelaTipo->__getList($listParameters);
                    
                    $objPermissao = new EXTDAO_Permissao();
                    $comboBoxesData->fieldPermissaoId = $objPermissao->__getList($listParameters);
                    
                    $objAreaMenu = new EXTDAO_Area_menu();
                    $comboBoxesData->fieldAreaMenuId = $objAreaMenu->__getList($listParameters);

            return $comboBoxesData;
        }
        
        


        
        public static function setListAliasTypes()
        {
            if(is_null(static::$listAliasTypes))
            {
                static::$listAliasTypes = new stdClass();
                static::$listAliasTypes->tela_estado__nome = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->tela_tipo__nome = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->permissao__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->area_menu__nome = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->tela__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->tela__nome = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->tela__nome = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->tela__seq_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->tela__is_android_BOOLEAN = static::TIPO_VARIAVEL_BOOLEAN;
static::$listAliasTypes->tela__is_web_BOOLEAN = static::TIPO_VARIAVEL_BOOLEAN;
static::$listAliasTypes->tela__configuracao_json = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->tela__data_criacao_SEC = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$listAliasTypes->tela__data_criacao_OFFSEC = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$listAliasTypes->tela__data_modificacao_SEC = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$listAliasTypes->tela__data_modificacao_OFFSEC = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$listAliasTypes->tela__modificacao_usuario_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->tela__cricacao_usuario_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->tela__ativa_BOOLEAN = static::TIPO_VARIAVEL_BOOLEAN;
static::$listAliasTypes->tela__pai_permissao_id_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->tela__nome = static::TIPO_VARIAVEL_TEXT;
            }        
        }
        
        public static function setListAliasRelatedAttributes()
        {
            if(is_null(static::$listAliasRelatedAttributes))
            {
                static::$listAliasRelatedAttributes = new stdClass();
                static::$listAliasRelatedAttributes->tela_estado__nome = "telaEstadoNome";
static::$listAliasRelatedAttributes->tela_tipo__nome = "telaTipoNome";
static::$listAliasRelatedAttributes->permissao__id = "permissaoId";
static::$listAliasRelatedAttributes->area_menu__nome = "areaMenuNome";
static::$listAliasRelatedAttributes->tela__id = "id";
static::$listAliasRelatedAttributes->tela__nome = "nome";
static::$listAliasRelatedAttributes->tela__nome = "nome";
static::$listAliasRelatedAttributes->tela__seq_INT = "seqInt";
static::$listAliasRelatedAttributes->tela__is_android_BOOLEAN = "isAndroidBoolean";
static::$listAliasRelatedAttributes->tela__is_web_BOOLEAN = "isWebBoolean";
static::$listAliasRelatedAttributes->tela__configuracao_json = "configuracaoJson";
static::$listAliasRelatedAttributes->tela__data_criacao_SEC = "dataCriacaoSec";
static::$listAliasRelatedAttributes->tela__data_criacao_OFFSEC = "dataCriacaoOffsec";
static::$listAliasRelatedAttributes->tela__data_modificacao_SEC = "dataModificacaoSec";
static::$listAliasRelatedAttributes->tela__data_modificacao_OFFSEC = "dataModificacaoOffsec";
static::$listAliasRelatedAttributes->tela__modificacao_usuario_id_INT = "modificacaoUsuarioId";
static::$listAliasRelatedAttributes->tela__cricacao_usuario_id_INT = "cricacaoUsuarioId";
static::$listAliasRelatedAttributes->tela__ativa_BOOLEAN = "ativaBoolean";
static::$listAliasRelatedAttributes->tela__pai_permissao_id_INT = "paiPermissaoId";
static::$listAliasRelatedAttributes->tela__nome = "nome";
            }         
        }

        public function __getList($parameters = null, $idCorporacao = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try
            {
                $filterParameters = new stdClass();
                if(isset($parameters->filterParameters))
                {
                    $filterParameters = $parameters->filterParameters;
                }
                                    
                            
            if(is_null($idCorporacao))
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            ListParameters::addFilterParameterToObject($filterParameters, "corporacaoId", $idCorporacao);           
                
                
                $sortingParameters = null;
                if(isset($parameters->sortingParameters))
                {
                    $sortingParameters = $parameters->sortingParameters;
                }
                
                $paginationParameters = null;
                if(isset($parameters->paginationParameters))
                {
                    $paginationParameters = $parameters->paginationParameters;
                }
                
                $listMappingType = static::getListMappingType($parameters->listMappingType);
                $mainTableAlias = "t";

                $whereClause = static::getWhereClauseForFilter($filterParameters, $mainTableAlias);
                $orderByClause = static::getOrderByClauseForFilter($sortingParameters, $mainTableAlias);
                $limitClause = static::getLimitClauseForFilter($paginationParameters);

                $queryWithoutLimit = "SELECT t.id FROM tela t {$whereClause}";
                $query = "SELECT te.nome AS tela_estado__nome, tt.nome AS tela_tipo__nome, p.id AS permissao__id, am.nome AS area_menu__nome, t.id AS tela__id, t.nome AS tela__nome, t.nome AS tela__nome, t.seq_INT AS tela__seq_INT, t.is_android_BOOLEAN AS tela__is_android_BOOLEAN, t.is_web_BOOLEAN AS tela__is_web_BOOLEAN, t.configuracao_json AS tela__configuracao_json, t.data_criacao_SEC AS tela__data_criacao_SEC, t.data_criacao_OFFSEC AS tela__data_criacao_OFFSEC, t.data_modificacao_SEC AS tela__data_modificacao_SEC, t.data_modificacao_OFFSEC AS tela__data_modificacao_OFFSEC, t.modificacao_usuario_id_INT AS tela__modificacao_usuario_id_INT, t.cricacao_usuario_id_INT AS tela__cricacao_usuario_id_INT, t.ativa_BOOLEAN AS tela__ativa_BOOLEAN, t.pai_permissao_id_INT AS tela__pai_permissao_id_INT, t.nome AS tela__nome FROM tela t LEFT JOIN tela_estado te ON te.id = t.tela_estado_id_INT LEFT JOIN tela_tipo tt ON tt.id = t.tela_tipo_id_INT LEFT JOIN permissao p ON p.id = t.permissao_id_INT LEFT JOIN area_menu am ON am.id = t.area_menu_id_INT {$whereClause} {$orderByClause} {$limitClause}";

                $msg = $this->database->queryMensagem($queryWithoutLimit);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                
                $totalNumberOfRecords = $this->database->rows();
                if($totalNumberOfRecords == 0)
                {
                    return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                }                        
                        
                $msg = $this->database->queryMensagem($query);
                
                //operacao realizaca com sucesso
                if ($msg == null) 
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                    }                        
                
                    $resultSet = $this->database->result;

                    $objReturn = new stdClass();
                    $objReturn->dataSet = static::mapResultSetDataToList($resultSet, $listMappingType, static::$listAliasTypes);

                    if(!Helper::isNullOrEmpty($paginationParameters->recordsPerPage) && !Helper::isNullOrEmpty($paginationParameters->currentPage))
                    {
                        $lastPage = ceil($totalNumberOfRecords / $paginationParameters->recordsPerPage);
                        $objReturn->paginationParameters = new PaginationParams($paginationParameters->recordsPerPage, $paginationParameters->currentPage, $lastPage);
                    }
                    
                    return new Mensagem_generica($objReturn);
                } 
                else
                {
                    return $msg;
                }                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }



public function getId()
            {
                return $this->id;
            }

public function getNome()
            {
                return $this->nome;
            }

public function getTelaEstadoId()
            {
                return $this->telaEstadoId;
            }

public function getTela_estado_id_INT()
                {
                    return $this->telaEstadoId;
                }

public function getTelaTipoId()
            {
                return $this->telaTipoId;
            }

public function getTela_tipo_id_INT()
                {
                    return $this->telaTipoId;
                }

public function getIsAndroidBoolean()
            {
                return $this->isAndroidBoolean;
            }

public function getIs_android_BOOLEAN()
                {
                    return $this->isAndroidBoolean;
                }

public function getIsWebBoolean()
            {
                return $this->isWebBoolean;
            }

public function getIs_web_BOOLEAN()
                {
                    return $this->isWebBoolean;
                }

public function getConfiguracaoJson()
            {
                return $this->configuracaoJson;
            }

public function getConfiguracao_json()
                {
                    return $this->configuracaoJson;
                }

public function getDataCriacaoSec()
            {
                return $this->dataCriacaoSec;
            }

public function getData_criacao_SEC()
                {
                    return $this->dataCriacaoSec;
                }

public function getDataCriacaoOffsec()
            {
                return $this->dataCriacaoOffsec;
            }

public function getData_criacao_OFFSEC()
                {
                    return $this->dataCriacaoOffsec;
                }

public function getDataModificacaoSec()
            {
                return $this->dataModificacaoSec;
            }

public function getData_modificacao_SEC()
                {
                    return $this->dataModificacaoSec;
                }

public function getDataModificacaoOffsec()
            {
                return $this->dataModificacaoOffsec;
            }

public function getData_modificacao_OFFSEC()
                {
                    return $this->dataModificacaoOffsec;
                }

public function getModificacaoUsuarioId()
            {
                return $this->modificacaoUsuarioId;
            }

public function getModificacao_usuario_id_INT()
                {
                    return $this->modificacaoUsuarioId;
                }

public function getCricacaoUsuarioId()
            {
                return $this->cricacaoUsuarioId;
            }

public function getCricacao_usuario_id_INT()
                {
                    return $this->cricacaoUsuarioId;
                }

public function getAtivaBoolean()
            {
                return $this->ativaBoolean;
            }

public function getAtiva_BOOLEAN()
                {
                    return $this->ativaBoolean;
                }

public function getPermissaoId()
            {
                return $this->permissaoId;
            }

public function getPermissao_id_INT()
                {
                    return $this->permissaoId;
                }

public function getAreaMenuId()
            {
                return $this->areaMenuId;
            }

public function getArea_menu_id_INT()
                {
                    return $this->areaMenuId;
                }

public function getCorporacaoId()
            {
                return $this->corporacaoId;
            }

public function getCorporacao_id_INT()
                {
                    return $this->corporacaoId;
                }



function setId($value)
            {
                $this->id = $value;
            }

function setNome($value)
            {
                $this->nome = $value;
            }

function setTelaEstadoId($value)
            {
                $this->telaEstadoId = $value;
            }

public function setTela_estado_id_INT($value)
                { 
                    $this->telaEstadoId = $value; 
                }

function setTelaTipoId($value)
            {
                $this->telaTipoId = $value;
            }

public function setTela_tipo_id_INT($value)
                { 
                    $this->telaTipoId = $value; 
                }

function setIsAndroidBoolean($value)
            {
                $this->isAndroidBoolean = $value;
            }

public function setIs_android_BOOLEAN($value)
                { 
                    $this->isAndroidBoolean = $value; 
                }

function setIsWebBoolean($value)
            {
                $this->isWebBoolean = $value;
            }

public function setIs_web_BOOLEAN($value)
                { 
                    $this->isWebBoolean = $value; 
                }

function setConfiguracaoJson($value)
            {
                $this->configuracaoJson = $value;
            }

public function setConfiguracao_json($value)
                { 
                    $this->configuracaoJson = $value; 
                }

function setDataCriacaoSec($value)
            {
                $this->dataCriacaoSec = $value;
            }

public function setData_criacao_SEC($value)
                { 
                    $this->dataCriacaoSec = $value; 
                }

function setDataCriacaoOffsec($value)
            {
                $this->dataCriacaoOffsec = $value;
            }

public function setData_criacao_OFFSEC($value)
                { 
                    $this->dataCriacaoOffsec = $value; 
                }

function setDataModificacaoSec($value)
            {
                $this->dataModificacaoSec = $value;
            }

public function setData_modificacao_SEC($value)
                { 
                    $this->dataModificacaoSec = $value; 
                }

function setDataModificacaoOffsec($value)
            {
                $this->dataModificacaoOffsec = $value;
            }

public function setData_modificacao_OFFSEC($value)
                { 
                    $this->dataModificacaoOffsec = $value; 
                }

function setModificacaoUsuarioId($value)
            {
                $this->modificacaoUsuarioId = $value;
            }

public function setModificacao_usuario_id_INT($value)
                { 
                    $this->modificacaoUsuarioId = $value; 
                }

function setCricacaoUsuarioId($value)
            {
                $this->cricacaoUsuarioId = $value;
            }

public function setCricacao_usuario_id_INT($value)
                { 
                    $this->cricacaoUsuarioId = $value; 
                }

function setAtivaBoolean($value)
            {
                $this->ativaBoolean = $value;
            }

public function setAtiva_BOOLEAN($value)
                { 
                    $this->ativaBoolean = $value; 
                }

function setPermissaoId($value)
            {
                $this->permissaoId = $value;
            }

public function setPermissao_id_INT($value)
                { 
                    $this->permissaoId = $value; 
                }

function setAreaMenuId($value)
            {
                $this->areaMenuId = $value;
            }

public function setArea_menu_id_INT($value)
                { 
                    $this->areaMenuId = $value; 
                }

function setCorporacaoId($value)
            {
                $this->corporacaoId = $value;
            }

public function setCorporacao_id_INT($value)
                { 
                    $this->corporacaoId = $value; 
                }



public function clear()
        {$this->id = null;
$this->nome = null;
$this->telaEstadoId = null;
if($this->objTela_estado != null) unset($this->objTela_estado);
$this->telaTipoId = null;
if($this->objTela_tipo != null) unset($this->objTela_tipo);
$this->isAndroidBoolean = null;
$this->isWebBoolean = null;
$this->configuracaoJson = null;
$this->dataCriacaoSec = null;
$this->dataCriacaoOffsec = null;
$this->dataModificacaoSec = null;
$this->dataModificacaoOffsec = null;
$this->modificacaoUsuarioId = null;
if($this->objModificacao_usuario != null) unset($this->objModificacao_usuario);
$this->cricacaoUsuarioId = null;
if($this->objCricacao_usuario != null) unset($this->objCricacao_usuario);
$this->ativaBoolean = null;
$this->permissaoId = null;
if($this->objPermissao != null) unset($this->objPermissao);
$this->areaMenuId = null;
if($this->objArea_menu != null) unset($this->objArea_menu);
$this->corporacaoId = null;
if($this->objCorporacao != null) unset($this->objCorporacao);
if(is_array($this->arrEstadosCampos)){
                        unset($this->arrEstadosCampos);
                     $this->arrEstadosCampos = array();
                    }
                }

public function formatarParaSQL()
        {
if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->id)){
$this->id = $this->formatarIntegerParaComandoSQL($this->id);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->id);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->nome)){
$this->nome = $this->formatarStringParaComandoSQL($this->nome);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->nome);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->telaEstadoId)){
$this->telaEstadoId = $this->formatarIntegerParaComandoSQL($this->telaEstadoId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->telaEstadoId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->telaTipoId)){
$this->telaTipoId = $this->formatarIntegerParaComandoSQL($this->telaTipoId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->telaTipoId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->isAndroidBoolean)){
$this->isAndroidBoolean = $this->formatarBooleanParaComandoSQL($this->isAndroidBoolean);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->isAndroidBoolean);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->isWebBoolean)){
$this->isWebBoolean = $this->formatarBooleanParaComandoSQL($this->isWebBoolean);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->isWebBoolean);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->configuracaoJson)){
$this->configuracaoJson = $this->formatarStringParaComandoSQL($this->configuracaoJson);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->configuracaoJson);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->dataCriacaoSec)){
$this->dataCriacaoSec = $this->formatarIntegerParaComandoSQL($this->dataCriacaoSec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->dataCriacaoSec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->dataCriacaoOffsec)){
$this->dataCriacaoOffsec = $this->formatarIntegerParaComandoSQL($this->dataCriacaoOffsec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->dataCriacaoOffsec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->dataModificacaoSec)){
$this->dataModificacaoSec = $this->formatarIntegerParaComandoSQL($this->dataModificacaoSec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->dataModificacaoSec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->dataModificacaoOffsec)){
$this->dataModificacaoOffsec = $this->formatarIntegerParaComandoSQL($this->dataModificacaoOffsec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->dataModificacaoOffsec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->modificacaoUsuarioId)){
$this->modificacaoUsuarioId = $this->formatarIntegerParaComandoSQL($this->modificacaoUsuarioId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->modificacaoUsuarioId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->cricacaoUsuarioId)){
$this->cricacaoUsuarioId = $this->formatarIntegerParaComandoSQL($this->cricacaoUsuarioId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->cricacaoUsuarioId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->ativaBoolean)){
$this->ativaBoolean = $this->formatarBooleanParaComandoSQL($this->ativaBoolean);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->ativaBoolean);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->permissaoId)){
$this->permissaoId = $this->formatarIntegerParaComandoSQL($this->permissaoId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->permissaoId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->areaMenuId)){
$this->areaMenuId = $this->formatarIntegerParaComandoSQL($this->areaMenuId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->areaMenuId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->corporacaoId)){
$this->corporacaoId = $this->formatarIntegerParaComandoSQL($this->corporacaoId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->corporacaoId);
}

}

public function formatarParaExibicao()
        {if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->nome)){
$this->nome = $this->formatarStringParaExibicao($this->nome);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->nome);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->isAndroidBoolean)){
$this->isAndroidBoolean = $this->formatarBooleanParaExibicao($this->isAndroidBoolean);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->isAndroidBoolean);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->isWebBoolean)){
$this->isWebBoolean = $this->formatarBooleanParaExibicao($this->isWebBoolean);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->isWebBoolean);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->configuracaoJson)){
$this->configuracaoJson = $this->formatarStringParaExibicao($this->configuracaoJson);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->configuracaoJson);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->ativaBoolean)){
$this->ativaBoolean = $this->formatarBooleanParaExibicao($this->ativaBoolean);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->ativaBoolean);
}

}

function select($id, $idCorporacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }   

            $sql = "SELECT id, nome, tela_estado_id_INT, tela_tipo_id_INT, is_android_BOOLEAN, is_web_BOOLEAN, configuracao_json, data_criacao_SEC, data_criacao_OFFSEC, data_modificacao_SEC, data_modificacao_OFFSEC, modificacao_usuario_id_INT, cricacao_usuario_id_INT, ativa_BOOLEAN, permissao_id_INT, area_menu_id_INT, corporacao_id_INT FROM tela WHERE id = $id AND corporacao_id_INT = $idCorporacao";

            $msg = $this->database->queryMensagem($sql);
            $result = $this->database->result;

            if($msg != null && $msg->erro() )
            { 
            		$this->clear();
            		return $msg;
        	} 
        	else if($this->database->rows() == 0)
        	{
                return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nenhum registro foi encontrado."));
            }
        

            $row = $this->database->fetchArray(MYSQLI_NUM);
            
            		$this->id = $row[0];
		$this->nome = $row[1];
		$this->telaEstadoId = $row[2];
		$this->telaTipoId = $row[3];
		$this->isAndroidBoolean = $row[4];
		$this->isWebBoolean = $row[5];
		$this->configuracaoJson = $row[6];
		$this->dataCriacaoSec = $row[7];
		$this->dataCriacaoOffsec = $row[8];
		$this->dataModificacaoSec = $row[9];
		$this->dataModificacaoOffsec = $row[10];
		$this->modificacaoUsuarioId = $row[11];
		$this->cricacaoUsuarioId = $row[12];
		$this->ativaBoolean = $row[13];
		$this->permissaoId = $row[14];
		$this->areaMenuId = $row[15];

            
        }

public function delete($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (is_null($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }

            $sql = "DELETE FROM tela WHERE id= $id AND corporacao_id_INT = $idCorporacao";
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                    && $sincronizar
                        && static::idSistemaTabela() != null
                            && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_REMOVE,
                                null,
                                null,
                                $id,
                                $this->database);
            }

            return $msg;
        }

public function insert($sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null, $opcoes = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            
            
            if (is_null($opcoes))
            {
                $opcoes = $this->opcoesDAO;
            }

            if (!is_numeric($this->id))
            {
                $this->id = "null";
                if ($opcoes == null || $opcoes[Database::OPCAO_GERAR_ID] != false) 
                {
                    $this->id = EXTDAO_Sistema_sequencia::gerarId(static::nomeTabela());                    
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }    
                    else if($msg == null)
                    {
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
                else 
                {
                    $this->id = null;
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao));
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }
                    else if($msg == null)
                    {
                        $this->id = $this->database->getLastInsertId();
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
            }
            else 
            {
                $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }                        
                else if($msg == null)
                {
                   $msg = new Mensagem_token();
                   $msg->mValor = $this->id;
                }
            }
            if($msg == null || $msg->ok())
            {                
                if($sincronizar
                    && static::idSistemaTabela() != null
                    && isset($this->id)){
                    
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_INSERT,
                                null,
                                null,
                                $this->id,
                                $this->database); 
                	}
            } 
            else
            {
                $this->id = null;
            }
            
            return $msg;
            
        }

public function getSQLInsert($idCorporacao){ 

return "INSERT INTO tela (id, nome, tela_estado_id_INT, tela_tipo_id_INT, is_android_BOOLEAN, is_web_BOOLEAN, configuracao_json, data_criacao_SEC, data_criacao_OFFSEC, data_modificacao_SEC, data_modificacao_OFFSEC, modificacao_usuario_id_INT, cricacao_usuario_id_INT, ativa_BOOLEAN, permissao_id_INT, area_menu_id_INT, corporacao_id_INT) VALUES ( $this->id ,  $this->nome ,  $this->telaEstadoId ,  $this->telaTipoId ,  $this->isAndroidBoolean ,  $this->isWebBoolean ,  $this->configuracaoJson ,  $this->dataCriacaoSec ,  $this->dataCriacaoOffsec ,  $this->dataModificacaoSec ,  $this->dataModificacaoOffsec ,  $this->modificacaoUsuarioId ,  $this->cricacaoUsuarioId ,  $this->ativaBoolean ,  $this->permissaoId ,  $this->areaMenuId ,  $idCorporacao ) "; 

}




        
        public function updateByParameters($id, $objParametros, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao))
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();

            
                
                if (isset($objParametros->nome)) 
                {
                    $arrUpdateFields[] = " nome = {$objParametros->nome} ";
                }


                
                if (isset($objParametros->telaEstadoId)) 
                {
                    $arrUpdateFields[] = " tela_estado_id_INT = {$objParametros->telaEstadoId} ";
                }


                
                if (isset($objParametros->telaTipoId)) 
                {
                    $arrUpdateFields[] = " tela_tipo_id_INT = {$objParametros->telaTipoId} ";
                }


                
                if (isset($objParametros->isAndroidBoolean)) 
                {
                    $arrUpdateFields[] = " is_android_BOOLEAN = {$objParametros->isAndroidBoolean} ";
                }


                
                if (isset($objParametros->isWebBoolean)) 
                {
                    $arrUpdateFields[] = " is_web_BOOLEAN = {$objParametros->isWebBoolean} ";
                }


                
                if (isset($objParametros->configuracaoJson)) 
                {
                    $arrUpdateFields[] = " configuracao_json = {$objParametros->configuracaoJson} ";
                }


                
                if (isset($objParametros->dataCriacaoSec)) 
                {
                    $arrUpdateFields[] = " data_criacao_SEC = {$objParametros->dataCriacaoSec} ";
                }


                
                if (isset($objParametros->dataCriacaoOffsec)) 
                {
                    $arrUpdateFields[] = " data_criacao_OFFSEC = {$objParametros->dataCriacaoOffsec} ";
                }


                
                if (isset($objParametros->dataModificacaoSec)) 
                {
                    $arrUpdateFields[] = " data_modificacao_SEC = {$objParametros->dataModificacaoSec} ";
                }


                
                if (isset($objParametros->dataModificacaoOffsec)) 
                {
                    $arrUpdateFields[] = " data_modificacao_OFFSEC = {$objParametros->dataModificacaoOffsec} ";
                }


                
                if (isset($objParametros->modificacaoUsuarioId)) 
                {
                    $arrUpdateFields[] = " modificacao_usuario_id_INT = {$objParametros->modificacaoUsuarioId} ";
                }


                
                if (isset($objParametros->cricacaoUsuarioId)) 
                {
                    $arrUpdateFields[] = " cricacao_usuario_id_INT = {$objParametros->cricacaoUsuarioId} ";
                }


                
                if (isset($objParametros->ativaBoolean)) 
                {
                    $arrUpdateFields[] = " ativa_BOOLEAN = {$objParametros->ativaBoolean} ";
                }


                
                if (isset($objParametros->permissaoId)) 
                {
                    $arrUpdateFields[] = " permissao_id_INT = {$objParametros->permissaoId} ";
                }


                
                if (isset($objParametros->areaMenuId)) 
                {
                    $arrUpdateFields[] = " area_menu_id_INT = {$objParametros->areaMenuId} ";
                }



            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE tela SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";

            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null)
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function update($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($this->nome)) 
                {                                      
                    $arrUpdateFields[] = " nome = {$this->nome} ";
                }


                
                if (isset($this->telaEstadoId)) 
                {                                      
                    $arrUpdateFields[] = " tela_estado_id_INT = {$this->telaEstadoId} ";
                }


                
                if (isset($this->telaTipoId)) 
                {                                      
                    $arrUpdateFields[] = " tela_tipo_id_INT = {$this->telaTipoId} ";
                }


                
                if (isset($this->isAndroidBoolean)) 
                {                                      
                    $arrUpdateFields[] = " is_android_BOOLEAN = {$this->isAndroidBoolean} ";
                }


                
                if (isset($this->isWebBoolean)) 
                {                                      
                    $arrUpdateFields[] = " is_web_BOOLEAN = {$this->isWebBoolean} ";
                }


                
                if (isset($this->configuracaoJson)) 
                {                                      
                    $arrUpdateFields[] = " configuracao_json = {$this->configuracaoJson} ";
                }


                
                if (isset($this->dataCriacaoSec)) 
                {                                      
                    $arrUpdateFields[] = " data_criacao_SEC = {$this->dataCriacaoSec} ";
                }


                
                if (isset($this->dataCriacaoOffsec)) 
                {                                      
                    $arrUpdateFields[] = " data_criacao_OFFSEC = {$this->dataCriacaoOffsec} ";
                }


                
                if (isset($this->dataModificacaoSec)) 
                {                                      
                    $arrUpdateFields[] = " data_modificacao_SEC = {$this->dataModificacaoSec} ";
                }


                
                if (isset($this->dataModificacaoOffsec)) 
                {                                      
                    $arrUpdateFields[] = " data_modificacao_OFFSEC = {$this->dataModificacaoOffsec} ";
                }


                
                if (isset($this->modificacaoUsuarioId)) 
                {                                      
                    $arrUpdateFields[] = " modificacao_usuario_id_INT = {$this->modificacaoUsuarioId} ";
                }


                
                if (isset($this->cricacaoUsuarioId)) 
                {                                      
                    $arrUpdateFields[] = " cricacao_usuario_id_INT = {$this->cricacaoUsuarioId} ";
                }


                
                if (isset($this->ativaBoolean)) 
                {                                      
                    $arrUpdateFields[] = " ativa_BOOLEAN = {$this->ativaBoolean} ";
                }


                
                if (isset($this->permissaoId)) 
                {                                      
                    $arrUpdateFields[] = " permissao_id_INT = {$this->permissaoId} ";
                }


                
                if (isset($this->areaMenuId)) 
                {                                      
                    $arrUpdateFields[] = " area_menu_id_INT = {$this->areaMenuId} ";
                }


            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE tela SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function updateByInstanceUsingParameters($id, $parameters, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($parameters->nome)) 
                {                                      
                    $arrUpdateFields[] = " nome = {$this->nome} ";
                }
                
                if (isset($parameters->telaEstadoId)) 
                {                                      
                    $arrUpdateFields[] = " tela_estado_id_INT = {$this->telaEstadoId} ";
                }
                
                if (isset($parameters->telaTipoId)) 
                {                                      
                    $arrUpdateFields[] = " tela_tipo_id_INT = {$this->telaTipoId} ";
                }
                
                if (isset($parameters->isAndroidBoolean)) 
                {                                      
                    $arrUpdateFields[] = " is_android_BOOLEAN = {$this->isAndroidBoolean} ";
                }
                
                if (isset($parameters->isWebBoolean)) 
                {                                      
                    $arrUpdateFields[] = " is_web_BOOLEAN = {$this->isWebBoolean} ";
                }
                
                if (isset($parameters->configuracaoJson)) 
                {                                      
                    $arrUpdateFields[] = " configuracao_json = {$this->configuracaoJson} ";
                }
                
                if (isset($parameters->dataCriacaoSec)) 
                {                                      
                    $arrUpdateFields[] = " data_criacao_SEC = {$this->dataCriacaoSec} ";
                }
                
                if (isset($parameters->dataCriacaoOffsec)) 
                {                                      
                    $arrUpdateFields[] = " data_criacao_OFFSEC = {$this->dataCriacaoOffsec} ";
                }
                
                if (isset($parameters->dataModificacaoSec)) 
                {                                      
                    $arrUpdateFields[] = " data_modificacao_SEC = {$this->dataModificacaoSec} ";
                }
                
                if (isset($parameters->dataModificacaoOffsec)) 
                {                                      
                    $arrUpdateFields[] = " data_modificacao_OFFSEC = {$this->dataModificacaoOffsec} ";
                }
                
                if (isset($parameters->modificacaoUsuarioId)) 
                {                                      
                    $arrUpdateFields[] = " modificacao_usuario_id_INT = {$this->modificacaoUsuarioId} ";
                }
                
                if (isset($parameters->cricacaoUsuarioId)) 
                {                                      
                    $arrUpdateFields[] = " cricacao_usuario_id_INT = {$this->cricacaoUsuarioId} ";
                }
                
                if (isset($parameters->ativaBoolean)) 
                {                                      
                    $arrUpdateFields[] = " ativa_BOOLEAN = {$this->ativaBoolean} ";
                }
                
                if (isset($parameters->permissaoId)) 
                {                                      
                    $arrUpdateFields[] = " permissao_id_INT = {$this->permissaoId} ";
                }
                
                if (isset($parameters->areaMenuId)) 
                {                                      
                    $arrUpdateFields[] = " area_menu_id_INT = {$this->areaMenuId} ";
                }
            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE tela SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }

}

?>
