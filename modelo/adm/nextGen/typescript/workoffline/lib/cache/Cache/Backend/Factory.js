"use strict";
exports.__esModule = true;
var Cache_Backend_Factory = (function () {
    function Cache_Backend_Factory() {
    }
    Cache_Backend_Factory.factory = function (type, config) {
        var backend = Cache_Backend_Factory._backendTypes[type];
        // Check if the backend type is available
        if (backend == null) {
            return null;
        }
        // Set the config
        backend.setConfig(config);
        return backend;
    };
    // Register a new backend definition
    Cache_Backend_Factory.addBackendDefinition = function (backend) {
        Cache_Backend_Factory._backendTypes[backend.getType()] = backend;
    };
    return Cache_Backend_Factory;
}());
exports.Cache_Backend_Factory = Cache_Backend_Factory;
