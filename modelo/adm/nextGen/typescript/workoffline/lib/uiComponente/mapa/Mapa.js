"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var uiComponente_1 = require("lib/uiComponente");
var Mapa = (function (_super) {
    __extends(Mapa, _super);
    function Mapa(contexto) {
        var _this = _super.call(this, contexto, contexto.mapContainerSelector) || this;
        _this.mainMapOptions = {
            panControl: false,
            zoomControl: true,
            mapTypeControl: false,
            scaleControl: false,
            streetViewControl: false,
            overviewMapControl: true
        };
        _this.mainMapDiv = $(contexto.mapContainerSelector);
        _this.componentesMapa = new Array();
        return _this;
    }
    Mapa.prototype.getGoogleMapInstance = function () {
        return this.mainMap;
    };
    Mapa.prototype.render = function () {
        var me = this;
        me.mainMap = new google.maps.Map(me.$()[0], me.mainMapOptions);
        me.fixMainMapSize();
    };
    Mapa.prototype.isRendered = function () {
        return this.mainMap != null && this.mainMap instanceof google.maps.Map;
    };
    Mapa.prototype.setDefaultValues = function () {
        this.setDefaultCenter();
        this.setDefaultZoom();
    };
    Mapa.prototype.getDefaultZoom = function () {
        return 5;
    };
    Mapa.prototype.setTiposElementosVisiveis = function (tiposElementosVisiveis) {
        this.tiposElementosVisiveis = tiposElementosVisiveis;
        this.refreshTiposElementosVisiveis();
    };
    Mapa.prototype.refreshTiposElementosVisiveis = function () {
        var tiposElementosVisiveis = this.tiposElementosVisiveis;
        if (this.isRendered()) {
            for (var _i = 0, _a = this.componentesMapa; _i < _a.length; _i++) {
                var componente = _a[_i];
                if (componente.isRendered()) {
                    if (tiposElementosVisiveis.indexOf(componente.getTipoElemento()) > -1) {
                        componente.setVisible(true);
                    }
                    else {
                        componente.setVisible(false);
                    }
                }
            }
            if (tiposElementosVisiveis.indexOf('estabelecimento') > -1) {
                this.setInternalMapElementVisibility('poi.business', true);
            }
            else {
                this.setInternalMapElementVisibility('poi.business', false);
            }
            if (tiposElementosVisiveis.indexOf('estrada') > -1) {
                this.setInternalMapElementVisibility('road.highway', true);
            }
            else {
                this.setInternalMapElementVisibility('road.highway', false);
            }
            if (tiposElementosVisiveis.indexOf('rua') > -1) {
                this.setInternalMapElementVisibility('road.arterial', true);
                this.setInternalMapElementVisibility('road.local', true);
            }
            else {
                this.setInternalMapElementVisibility('road.arterial', false);
                this.setInternalMapElementVisibility('road.local', false);
            }
            this.refreshMapLayers();
        }
    };
    Mapa.prototype.refreshMapLayers = function () {
        if (this.isRendered()) {
            this.mainMap.setZoom(this.mainMap.getZoom());
        }
    };
    Mapa.prototype.setDefaultZoom = function () {
        var defaultZoom = this.getDefaultZoom();
        this.mainMapOptions.zoom = defaultZoom;
        if (this.isRendered()) {
            this.mainMap.setZoom(defaultZoom);
        }
    };
    Mapa.prototype.getCenter = function () {
        return this.isRendered() ? this.mainMap.getCenter() : null;
    };
    Mapa.prototype.setCenter = function (latLng) {
        this.mainMapOptions.center = latLng;
        if (this.isRendered()) {
            this.mainMap.setCenter(latLng);
        }
    };
    Mapa.prototype.getDefaultCenter = function () {
        return new google.maps.LatLng(-10.333333, -53.200000);
    };
    Mapa.prototype.setDefaultCenter = function () {
        var defaultCenter = this.getDefaultCenter();
        this.mainMapOptions.center = defaultCenter;
        if (this.isRendered()) {
            this.mainMap.setCenter(defaultCenter);
        }
    };
    Mapa.prototype.setTemplate = function (template) {
        var styles = template.getStyles();
        this.mainMapOptions.styles = styles;
        if (this.isRendered() && (this.template == null || this.template.nomeTemplate != template.nomeTemplate)) {
            this.mainMap.set('styles', styles);
            this.refreshTiposElementosVisiveis();
        }
        this.template = template;
    };
    Mapa.prototype.setInternalMapElementVisibility = function (tipoElemento, visible) {
        var currentStyles = this.mainMap.get('styles');
        var estiloEncontrado = false;
        var googleMapsVisibility = visible ? 'on' : 'off';
        for (var _i = 0, currentStyles_1 = currentStyles; _i < currentStyles_1.length; _i++) {
            var currentStyle = currentStyles_1[_i];
            if (currentStyle.featureType == tipoElemento) {
                for (var _a = 0, _b = currentStyle.stylers; _a < _b.length; _a++) {
                    var styler = _b[_a];
                    if (styler.visibility != null) {
                        styler.visibility = googleMapsVisibility;
                        estiloEncontrado = true;
                        break;
                    }
                }
                if (!estiloEncontrado) {
                    currentStyle.stylers.push({ visibility: googleMapsVisibility });
                    estiloEncontrado = true;
                }
            }
            if (estiloEncontrado) {
                break;
            }
        }
        if (!estiloEncontrado) {
            currentStyles.push({
                featureType: tipoElemento,
                stylers: [{ visibility: googleMapsVisibility }]
            });
        }
        this.mainMap.set('styles', currentStyles);
    };
    Mapa.prototype.addResizeListener = function () {
        var _this = this;
        $(window).resize(function () {
            _this.fixMainMapSize();
        });
    };
    Mapa.prototype.fitBounds = function () {
        if (this.componentesMapa.length > 0) {
            var latLngBounds = new google.maps.LatLngBounds();
            for (var _i = 0, _a = this.componentesMapa; _i < _a.length; _i++) {
                var componente = _a[_i];
                for (var _b = 0, _c = componente.elementsLatLngs; _b < _c.length; _b++) {
                    var latLng = _c[_b];
                    latLngBounds.extend(latLng);
                }
            }
            this.mainMap.fitBounds(latLngBounds);
        }
    };
    Mapa.prototype.fixMainMapSize = function () {
        var maxHeight = uiComponente_1.DomContainer.getMaxAllowedHeight();
        var maxWidth = uiComponente_1.DomContainer.getMaxAllowedWidth();
        this.$root('.over-main-map, .below-main-map').each(function () {
            maxHeight -= $(this).outerHeight();
        });
        this.mainMapDiv.height(maxHeight - 1);
        this.$root('.left-main-map, .right-main-map').each(function () {
            maxWidth -= $(this).outerWidth();
        });
        this.mainMapDiv.width(maxWidth);
        google.maps.event.trigger(this.mainMap, "resize");
    };
    Mapa.prototype.addComponente = function (elemento) {
        this.componentesMapa.push(elemento);
    };
    Mapa.prototype.removeAllComponentes = function (tipo) {
        for (var i = this.componentesMapa.length; i > -0; i--) {
            var componente = this.componentesMapa[i];
            if (typeof componente == tipo.toString()) {
                if (componente.isRendered()) {
                    componente.removeFromGoogleMap();
                }
                this.componentesMapa.splice(i, 1);
            }
        }
    };
    return Mapa;
}(uiComponente_1.DomContainer));
exports.Mapa = Mapa;
