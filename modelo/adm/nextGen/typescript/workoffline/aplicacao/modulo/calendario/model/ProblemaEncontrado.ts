import * as Moment from 'moment';
import * as FullCalendarInputTypes from 'fullcalendar/src/types/input-types';
import { EventObjectInput } from 'fullcalendar';

import { Model } from 'lib/model';
import { Color, MomentHelper } from 'lib/geral';
import { Evento } from 'aplicacao/modulo/calendario/model';
import { TipoPontoEletronico } from 'aplicacao/comum/geral';

export class ProblemaEncontrado extends Model
{
    public id: number;
    public nomePessoa: string;
    public cor :Color;
    public eventoAssociado: Evento;

    public getTituloProblema() :string
    {
        switch(this.eventoAssociado.pontoInicial.tipoPonto)
        {
            case TipoPontoEletronico.PONTO_ENTRADA:
                return "Ponto de Entrada sem Saida";
             
            case TipoPontoEletronico.PONTO_SAIDA:
                return "Ponto de Saida sem Entrada";

        }  
    }

    public getDescricaoProblema() :string
    {
        return this.eventoAssociado.pontoInicial.dataPonto.toDate().toLocaleString();
    }

}