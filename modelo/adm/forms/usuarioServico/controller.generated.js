

        /*

        Arquivo gerado atrav�s de gerador de c�digo em 07/09/2017 as 01:26:40.
        Para que o arquivo n�o seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

        Tabela correspondente: usuario_servico
        Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

        */

omegaApp.generatedFormUsuarioServicoController = function($scope, $rootScope, $window, $interval, $timeout, dataService, Upload, vm) {try { vm.fieldsTypes = {};

vm.formData = {
        statusBoolean: null,
servicoId: null,
usuarioId: null,
};

vm.disabledFields = {
        fieldStatusBoolean: false,
fieldServicoId: false,
fieldUsuarioId: false,
};

vm.requiredFields = {
        fieldStatusBoolean: true,
fieldServicoId: true,
fieldUsuarioId: true,
};

vm.relatedEntities =
        {
        };

vm.fieldsParameters = {
        fieldStatusBoolean: null,
fieldServicoId: {remoteData: null},
fieldUsuarioId: {remoteData: null},
};

vm.submitForm = function(formInstance)
        {
            if($rootScope.validateForm(formInstance))
            {
                var formData = angular.merge({}, vm.formData, GeneralUtil.getRelatedEntitiesData(vm.relatedEntities));
                if (!GeneralUtil.isNullOrEmpty(vm.formData.id))
                {
                    dataService.editUsuarioServico(formData).then(function(returnContent)
                    {
                        if (GeneralUtil.validateRemoteResponse(returnContent))
                        {
                            $rootScope.showDialog(vm, 'after-usuarioServico-update-template.html');
                        }
                        else
                        {
                            $rootScope.instantErrorMessages.push(returnContent.data.mMensagem);
                        }
                    });
                }
                else
                {
                    dataService.addUsuarioServico(formData).then(function(returnContent)
                    {
                        if (GeneralUtil.validateRemoteResponse(returnContent))
                        {
                            $rootScope.showDialog(vm, 'after-usuarioServico-insert-template.html');
                        }
                        else
                        {
                            $rootScope.instantErrorMessages.push(returnContent.data.mMensagem);
                        }
                    });
                }
            }

        };

if (!GeneralUtil.isNullOrEmpty(__GET) && !GeneralUtil.isNullOrEmpty(__GET.id))
        {

            var filterData = { id: __GET.id };
            var filterOperators = { id: Constants.FilterOperator.EQUALS };
            var parameters = GeneralUtil.getFilterParameters(filterData, filterOperators);

            dataService.getUsuarioServico(parameters).then(function (returnContent)
            {
                if (GeneralUtil.validateRemoteResponse(returnContent))
                {
                    vm.formData = returnContent.data.mObj;
                }
                else
                {
                    $rootScope.instantErrorMessages.push(returnContent.data.mMensagem);
                }

            });
        }

vm.openServicoIdModal = function()
                {
                    $rootScope.uibModalInstance = $rootScope.showModalDialog('forms', 'Servico');
                    
                    //atualizando valores da lista
                    var unregisterModal = $rootScope.$watch(function() { return $rootScope.uibModalInstance }, function () {
    
                        if($rootScope.uibModalInstance == null)
                        {
                            vm.getListOfServicoId();
                            vm.formData.servicoId = null;
                            unregisterModal();
                        }
    
                    });
                    
                };
                vm.openUsuarioIdModal = function()
                {
                    $rootScope.uibModalInstance = $rootScope.showModalDialog('forms', 'Usuario');
                    
                    //atualizando valores da lista
                    var unregisterModal = $rootScope.$watch(function() { return $rootScope.uibModalInstance }, function () {
    
                        if($rootScope.uibModalInstance == null)
                        {
                            vm.getListOfUsuarioId();
                            vm.formData.usuarioId = null;
                            unregisterModal();
                        }
    
                    });
                    
                };
                

vm.getListOfServicoId = function()
                {
                    dataService.getListOfServico().then(function (returnContent)
                    {
                        if (GeneralUtil.validateRemoteResponse(returnContent))
                        {
                            vm.fieldsParameters.fieldServicoId.remoteData = returnContent.data.mObj.gridData;
                        }
                        else
                        {
                            $rootScope.instantErrorMessages.push(returnContent.data.mMensagem);
                        }

                    });

                };
                vm.getListOfUsuarioId = function()
                {
                    dataService.getListOfUsuario().then(function (returnContent)
                    {
                        if (GeneralUtil.validateRemoteResponse(returnContent))
                        {
                            vm.fieldsParameters.fieldUsuarioId.remoteData = returnContent.data.mObj.gridData;
                        }
                        else
                        {
                            $rootScope.instantErrorMessages.push(returnContent.data.mMensagem);
                        }

                    });

                };
                

vm.loadListOfUsuarioServico = function()
        {
            document.location.href = 'index.php?tipo=lists&page=usuarioServico';
        };

        vm.addNewUsuarioServico = function()
        {
            document.location.href = document.location.href;
        };

}
                        catch(ex)
                        {
                            $rootScope.instantErrorMessages.push(new Message('Erro no controller generatedFormUsuarioServicoController', ex.message));
                        }

                }