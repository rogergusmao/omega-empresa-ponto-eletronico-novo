<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

//DEPRECATED - OLD VERSION
class DatabaseSincronizador
{
    private static $DELIMITADOR_LINHA = "+!+";
    private static $CODIGO_DUPLICADA = "-2";
    private static $CODIGO_INEXISTENTE = "-3";
    private static $DELIMITADOR_COLUNA = "%%$%%";
    private static $NULL = "null";
    public static $INDEX_OPERACAO_REMOVE = "1";
    public static $INDEX_OPERACAO_INSERT = "2";
    public static $INDEX_OPERACAO_EDIT = "3";

    public function __construct()
    {
    }

    public static function isServerOnline()
    {
        echo "TRUE";
    }

    public static function __uploadArquivo_zip_ARQUIVO($numRegistro = 1, $urlErro, $pathArquivo)
    {
        $dirUpload = $pathArquivo;

        $objUpload = new Upload();

        $objUpload->arrPermitido = "";
        $objUpload->tamanhoMax = "";
        $objUpload->file = $_FILES["logfile{$numRegistro}"];
//        $objUpload->nome =  Helper::getExtensaoDoArquivo($objUpload->file["name"]);
        $objUpload->nome = $objUpload->file["name"];
        $objUpload->uploadPath = $dirUpload;
        $success = $objUpload->uploadFile();

        if (!$success)
        {
            Helper::setSession("erro", true);

            return array("location: $urlErro&msgErro=Erro no upload do arquivo $pathArquivo:" . $objUpload->erro);
        }
        else
        {
            return array($objUpload->nome, $pathArquivo);
        }
    }

    public static function getListIdOfReceiveZipFileQueryInsert()
    {
        $delimiterQuery = "%%$%%";

        if (Helper::verificarUploadArquivo("logfile"))
        {
            $nomeArquivo = "";
            $raiz = Helper::acharRaiz();
            $pathArquivo = $raiz . EXTDAO_Sistema_tipo_download_arquivo::getPathDoTipo(Helper::POSTGET("tipo_arquivo"));

            if (Helper::verificarUploadArquivo("logfile"))
            {
                if (is_array($arquivo = DatabaseSincronizador::__uploadArquivo_zip_ARQUIVO("", $urlErro, $pathArquivo)))
                {
                    $nomeArquivo = $arquivo[0];
                }
            }
            else
            {
                echo "NOTOK: O arquivo é inválido";
                return;
            }

            $handlerZip = zip_open($pathArquivo);

            $contadorGlobal = 0;
            $strErro = "";
            $isFirstIdPrinted = false;

            while ($arquivoNoZip = zip_read($handlerZip))
            {
                if (zip_entry_open($handlerZip, $arquivoNoZip))
                {
                    $nomeArquivoNoZip = zip_entry_name($arquivoNoZip);

                    if (substr_count(strtolower($nomeArquivoNoZip), ".jpg") == 1 ||
                        substr_count(strtolower($nomeArquivoNoZip), ".png") == 1
                    )
                    {
                    }
                    else
                    {
                        $conteudoArquivo = zip_entry_read($arquivoNoZip);

                        $linhas = explode($delimiterQuery, $conteudoArquivo);

                        if (!count($linhas))
                        {
                            $strErro .= "Arquivo {$arquivoNoZip} inválido. ";
                        }

                        for ($i = 0; $i < count($linhas); $i++)
                        {
                            $conteudoLinha = $linhas[$i];

                            if (!(strlen($conteudoLinha) > 0))
                            {
                                continue;
                            }
                            $query = $conteudoLinha;

                            $objDatabase = new Database();
                            $vLastId = $objDatabase->query($query);

                            echo $vLastId;
                            if ($isFirstIdPrinted)
                            {
                                echo $delimiterQuery;
                                $isFirstIdPrinted = true;
                            }
                            echo $vLastId;
                        }
                    }
                }
            }

            if (!strlen($strErro))
            {
            }
            else
            {
                echo "NOTOK: {$strErro}";
            }
        }
        else
        {
            echo "NOTOK: os parametros imei e logfile sï¿½o obrigatï¿½rios";
        }
    }

    public static function exportarUltimaImagemDoUsuario()
    {
        $vIdUsuario = Helper::POSTGET("usuario");
        if (strlen($vIdUsuario))
        {
            $objBanco = new Database();
            $objBanco->query("SELECT  uf.foto, MAX(CONCAT(uf.data_DATE, ' ', uf.hora_TIME)) AS data_hora            FROM usuario_foto AS uf
                WHERE uf.usuario_id_INT = {$vIdUsuario}");

            $v_vetorObjetos = Helper::getResultSetToArrayDeObjetos($objBanco->result);
            if (count($v_vetorObjetos) == 1)
            {
                $vIsPrimeiraVez = true;
                foreach ($v_vetorObjetos as $key => $obj)
                {
                    $vStrFoto = $obj->foto;
                    echo DatabaseSincronizador::exportarImagem($vStrFoto);
                    break;
                }
            }
        }
    }

    public static function exportarImagemUsuarioFoto()
    {
        if (!DatabaseSincronizador::checkUsuarioCorporacao())
        {
            echo "FALSE 0";
            return;
        }
        $pIdUsuarioFoto = Helper::POSTGET("usuario_foto");
        $pIdCorporacao = Helper::POSTGET("id_corporacao");
        if (strlen($pIdUsuarioFoto))
        {
            $vObjUsuarioFoto = new EXTDAO_Usuario_Foto();
            $vObjUsuarioFoto->select($pIdUsuarioFoto, $pIdCorporacao);
            $vFoto = $vObjUsuarioFoto->getFoto();
            //novo
            DatabaseSincronizador::exportarImagem($vFoto);
        }
    }

    public static function downloadArquivoDoTipo()
    {
        $idSTDA = Helper::POSTGET("id_sistema_tipo_download_arquivo");
        $idCorp = Helper::POSTGET("id_corporacao");
        $idUsu = Helper::POSTGET("id_usuario");
        $nomeArquivo = Helper::POSTGET("arquivo");
        $path = EXTDAO_Sistema_tipo_download_arquivo::getPathDoTipo($idSTDA, $idCorp, $idUsu);
        $pathArquivo = $path . $nomeArquivo;

        $objDownload = new Download($pathArquivo);
        $fp = $objDownload->df_download();
        if ($fp)
        {
            print $fp;
        }
    }

    private static function exportarImagem($pStrArquivoImagem)
    {
        $raiz = Helper::acharRaiz();
        $pathArquivo = "{$raiz}conteudo/fotos_download/" . $pStrArquivoImagem;

        if (strlen($pathArquivo))
        {
            $objDownload = new Download($pathArquivo);
            $fp = $objDownload->df_download();
            if ($fp)
            {
                print $fp;
            }
        }
    }

    public static function receiveZipFoto()
    {
        if (Helper::verificarUploadArquivo("logfile"))
        {
            $nomeArquivo = "";
            $raiz = Helper::acharRaiz();

            $pathDiretorio = $raiz . EXTDAO_Sistema_tipo_download_arquivo::getPathDoTipo(Helper::POSTGET("tipo"), Helper::POSTGET("id_corporacao"), Helper::POSTGET("usuario"));

            if (!is_dir($pathDiretorio))
            {
                Helper::mkdir($pathDiretorio, 0777, true);
            }

            echo "TIPO: " . $pathDiretorio . ";";
            if (Helper::verificarUploadArquivo("logfile"))
            {
                if (is_array($arquivo = DatabaseSincronizador::__uploadArquivo_zip_ARQUIVO("", $urlErro, $pathDiretorio)))
                {
                    $nomeArquivo = $arquivo[0];
                }
            }
            else
            {
                echo "NOTOK: O arquivo Ã© invalido";
                return;
            }

            $pathArquivoZip = $pathDiretorio . $nomeArquivo;
            echo $pathArquivoZip;
            echo "DIRETORIO ZIP: " . $pathDiretorio . ";";

            $handlerZip = zip_open($pathArquivoZip);

            $strErro = "";
            while ($arquivoNoZip = zip_read($handlerZip))
            {
                if (zip_entry_open($handlerZip, $arquivoNoZip))
                {
                    $nomeArquivoNoZip = zip_entry_name($arquivoNoZip);

                    $nomeArquivo = $nomeArquivoNoZip;
                    $diretorioImagens = $pathDiretorio . $nomeArquivo;

                    if ($handlerArquivo = fopen($diretorioImagens, 'w'))
                    {
                        fwrite($handlerArquivo, zip_entry_read($arquivoNoZip, zip_entry_filesize($arquivoNoZip)));
                        fclose($handlerArquivo);
                    }
                }
            }

            if (!strlen($strErro))
            {
                echo "TRUE";
            }
            else
            {
                echo "NOTOK: {$strErro}";
            }
            if (is_file($pathArquivoZip))
            {
                unlink($pathArquivoZip);
            }
        }
        else
        {
            echo "NOTOK: os parametros imei e logfile sï¿½o obrigatï¿½rios";
        }
    }

    public static function receiveFoto()
    {
        if (!DatabaseSincronizador::checkUsuarioCorporacao())
        {
            echo "FALSE 0";
            return;
        }
        if (Helper::verificarUploadArquivo("logfile"))
        {
            $nomeArquivo = "";
            $raiz = Helper::acharRaiz();
            $pathArquivo = "{$raiz}conteudo/fotos_download/";

            if (Helper::verificarUploadArquivo("logfile"))
            {
                if (is_array($arquivo = DatabaseSincronizador::__uploadArquivo_zip_ARQUIVO("", $urlErro, $pathArquivo)))
                {
                    $nomeArquivo = $arquivo[0];
                    echo "TRUE";
                }
            }
            else
            {
                echo "NOTOK: O arquivo é invalido";
                return;
            }
        }
        else
        {
            echo "NOTOK: os parametros imei e logfile sï¿½o obrigatï¿½rios";
        }
    }

    public static function receiveZipFileQuery()
    {
        if (!DatabaseSincronizador::checkUsuarioCorporacao())
        {
            echo "FALSE 0";
            return;
        }
        $delimiterQuery = "%%$%%";
//        print_r($_FILES);
        if (Helper::verificarUploadArquivoZip("logfile"))
        {
            $nomeArquivo = "";
            $raiz = Helper::acharRaiz();
            $pathArquivo = "{$raiz}conteudo/arquivos_download/";

            if (is_array($arquivo = DatabaseSincronizador::__uploadArquivo_zip_ARQUIVO("", $urlErro, $pathArquivo)))
            {
                $nomeArquivo = $arquivo[0];
            }

            $pathArquivo = "{$raiz}conteudo/arquivos_download/{$nomeArquivo}";

            $handlerZip = zip_open($pathArquivo);

            $contadorGlobal = 0;
            $strErro = "";
            while ($arquivoNoZip = zip_read($handlerZip))
            {
                if (zip_entry_open($handlerZip, $arquivoNoZip))
                {
                    $nomeArquivoNoZip = zip_entry_name($arquivoNoZip);
                    if (substr_count(strtolower($nomeArquivoNoZip), ".jpg") == 1 ||
                        substr_count(strtolower($nomeArquivoNoZip), ".png") == 1
                    )
                    {
                        $extensoes = array(".jpg", ".png");

                        $nomeArquivo = $nomeArquivoNoZip;
                        $diretorioImagens = "{$raiz}conteudo/fotos_download/{$nomeArquivo}";

                        if ($handlerArquivo = fopen($diretorioImagens, 'w'))
                        {
                            fwrite($handlerArquivo, zip_entry_read($arquivoNoZip, zip_entry_filesize($arquivoNoZip)));
                            fclose($handlerArquivo);
                        }
                    }
                    else
                    {
                        $conteudoArquivo = zip_entry_read($arquivoNoZip);

                        $linhas = explode($delimiterQuery, $conteudoArquivo);

                        if (!count($linhas))
                        {
                            $strErro .= "Arquivo {$arquivoNoZip} inválido. ";
                        }

                        for ($i = 0; $i < count($linhas); $i++)
                        {
                            $conteudoLinha = $linhas[$i];
                            if (!(strlen($conteudoLinha) > 0))
                            {
                                continue;
                            }
                            $query = $conteudoLinha;

                            $objDatabase = new Database();
                            $vStrResult = $objDatabase->query($query);
                        }
                    }
                }
            }

            if (!strlen($strErro))
            {
                echo "TRUE";
            }
            else
            {
                echo "NOTOK: {$strErro}";
            }
            unlink($pathArquivo);
        }
        else
        {
            echo "NOTOK: os parametros imei e logfile são obrigatórios";
        }
    }

    private static function checkUsuarioCorporacao()
    {
        $idCorporacao = Helper::POSTGET("id_corporacao");
        $idUsuario = Helper::POSTGET("usuario");
        $senha = Helper::POSTGET("senha");
        $vObjSeguranca = new Seguranca();
        $msg = $vObjSeguranca->verificaSenha(
            array("id" => $idUsuario, "id_corporacao" => $idCorporacao),
            $senha,
            null,
            false);
        if ($msg != null && !$msg->ok())
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public static function loginMobile()
    {
        $corporacao = Helper::POSTGET("corporacao");
        $email = Helper::POSTGET("email");
        $senha = Helper::POSTGET("senha");
        $vObjSeguranca = new Seguranca();

        $msg = $vObjSeguranca->verificaSenha(
            array("email" => $email, "corporacao" => $corporacao),
            $senha,
            null,
            false);
        if ($msg != null && !$msg->ok())
        {
            echo "FALSE 0";
            return;
        }
        else
        {
            echo "TRUE";
        }
    }

    public static function removeAllPermissaoCategoriaPermissao()
    {
        if (!DatabaseSincronizador::checkUsuarioCorporacao())
        {
            echo "FALSE 0";
            return;
        }

        $idCategoriaPermissao = Helper::POSTGET("categoria_permissao");
        if (strlen($idCategoriaPermissao) > 0)
        {
            $vObjBanco = new Database();
            $vQuery = "SELECT DISTINCT pcp.id
                            FROM permissao p, permissao_categoria_permissao pcp
                            WHERE pcp.permissao_id_INT = p.id AND
                            pcp.categoria_permissao_id_INT = {$idCategoriaPermissao}";
            $vObjBanco->query($vQuery);
            $vVetorIdPermissao = Helper::getResultSetToArrayDeUmCampo($vObjBanco->result);
            $vObjPCP = new EXTDAO_Permissao_categoria_permissao();
            foreach ($vVetorIdPermissao as $key => $vIdPermissao)
            {
                $vObjPCP->delete($vIdPermissao);
            }
            echo "TRUE";
        }
        else
        {
            echo "FALSE 1";
        }
    }

    public static function receiveLastTuplaUsuarioFoto()
    {
        $idUsuario = Helper::POSTGET("usuario");
        $idUsuarioFoto = Helper::POSTGET("usuario_foto");

        if (strlen($idUsuario) > 0)
        {
            $objBanco = new Database();
            $strQuery = "SELECT MAX(us.id) id, us.data_DATE data, us.hora_TIME hora, us.foto_interna foto_interna, us.foto_externa foto_externa
                            FROM usuario_foto AS us
                        WHERE us.usuario_id_INT = {$idUsuario} ";

            $strQuery .= " ORDER BY id DESC";
            $objBanco->query($strQuery);
            $v_vetorObjetos = Helper::getResultSetToArrayDeObjetos($objBanco->result);

            if (count($v_vetorObjetos) > 0)
            {
                foreach ($v_vetorObjetos as $key => $obj)
                {
                    if (strlen($idUsuarioFoto) > 0)
                    {
                        if ($idUsuarioFoto == $obj->id)
                        {
                            echo "FALSE";
                            break;
                        }
                    }
                    if (strlen($obj->id) == 0)
                    {
                        echo "FALSE";
                        break;
                    }
                    else
                    {
                        echo $obj->id . ";";
                        echo $obj->data . ";";
                        echo $obj->hora . ";";
                        echo $obj->foto_interna . ";";
                        echo $obj->foto_externa;
                        break;
                    }
                }
            }
            else
            {
                echo "FALSE";
            }
        }
    }

    public static function receiveTabelaVeiculoPosicaoAndroidDatabase()
    {
        if (!DatabaseSincronizador::checkUsuarioCorporacao())
        {
            echo "FALSE 0";
            return;
        }
        $pIdVeiculoUsuario = Helper::POSTGET("veiculo_usuario");
        $pIdCorporacao = Helper::POSTGET("id_corporacao");
        if (strlen($pIdVeiculoUsuario) > 0)
        {
            $objBanco = new Database();
            $objBanco->query("SELECT vu.id AS id, vup.id AS vup_id, MAX(CONCAT(vup.data_DATE, ' ', vup.hora_TIME)) AS data_hora   
                                        FROM usuario_posicao AS vup
                                        LEFT JOIN veiculo_usuario AS vu ON vu.id=vup.veiculo_usuario_id_INT
                                        WHERE vu.is_ativo_BOOLEAN=1 AND vu.id != {$pIdVeiculoUsuario} AND
                                        vup.corporacao_id_INT = {$pIdCorporacao}
                                        GROUP BY vu.id");

            $v_vetorObjetos = Helper::getResultSetToArrayDeObjetos($objBanco->result);
            if (count($v_vetorObjetos) > 0)
            {
                $vIsPrimeiraVez = true;
                foreach ($v_vetorObjetos as $key => $obj)
                {
                    $vObj = new EXTDAO_usuario_posicao();
                    $vObj->select($obj->vup_id, $pIdCorporacao);
//                    $vObj->formatarParaExibicao();
                    if (!$vIsPrimeiraVez)
                    {
                        echo DatabaseSincronizador::$DELIMITADOR_COLUNA;
                    }
                    else
                    {
                        $vIsPrimeiraVez = false;
                    }

                    echo $vObj->getData_DATE() . ";";
                    echo $vObj->getHora_TIME() . ";";
                    echo $vObj->getVeiculo_usuario_id_INT() . ";";
                    echo $vObj->getLongitude_INT() . ";";
                    echo $vObj->getLatitude_INT();
                }
            }
        }
    }

    public static function receiveListaTuplaUsuarioFoto()
    {
        if (!DatabaseSincronizador::checkUsuarioCorporacao())
        {
            echo "FALSE 0";
            return;
        }
        $pIdUsuario = Helper::POSTGET("usuario");
        $pIdCorporacao = Helper::POSTGET("id_corporacao");
        $pListaStrTupla = Helper::POSTGET("lista_tupla");

        $vVetorTupla = explode("%%", $pListaStrTupla);
        if (count($vVetorTupla) == 0)
        {
            echo "FALSE 1";
            return;
        }
        $vObjUsuarioFoto = new EXTDAO_Usuario_foto();
        foreach ($vVetorTupla as $key => $vTupla)
        {
            $vVetorValorAtributo = explode(";", $vTupla);
            $vObjUsuarioFoto->setUsuario_id_INT($pIdUsuario);

            //sem usuario e sem corporacao
            if (count($vVetorValorAtributo) == 4)
            {
                $vData = $vVetorValorAtributo[0];
                $vObjUsuarioFoto->setData_DATE($vData);
                $vHora = $vVetorValorAtributo[1];
                $vObjUsuarioFoto->setHora_TIME($vHora);

                //faca a verificacao quando for escolhido o veiculo usuario
                //ESTE DEVE ESTAR SINCRONIZADO
                $vFotoInterna = $vVetorValorAtributo[2];
                if (strcmp($vFotoInterna, "null") != 0)
                {
                    $vObjUsuarioFoto->setFoto_interna($vFotoInterna);
                }
                else
                {
                    $vObjUsuarioFoto->setFoto_interna(null);
                }

                $vFotoExterna = $vVetorValorAtributo[3];
                if (strcmp($vFotoExterna, "null") != 0)
                {
                    $vObjUsuarioFoto->setFoto_externa($vFotoExterna);
                }
                else
                {
                    $vObjUsuarioFoto->setFoto_externa(null);
                }
                $vObjUsuarioFoto->formatarParaSQL();
                $vObjUsuarioFoto->setCorporacao_id_INT($vObjUsuarioFoto->formatarDados($pIdCorporacao));
                $vObjUsuarioFoto->insert(false, $pIdCorporacao);
            }
        }
        echo "TRUE";
    }

    public static function receiveListaTuplaUsuarioPosicao()
    {
        if (!DatabaseSincronizador::checkUsuarioCorporacao())
        {
            echo "FALSE 0";
            return;
        }

        $idUsuario = Helper::POSTGET("usuario");
        $idCorporacao = Helper::POSTGET("id_corporacao");
        $vListaStrTupla = Helper::POSTGET("lista_tupla");

        $vVetorTupla = explode("%%", $vListaStrTupla);
        if (count($vVetorTupla) == 0)
        {
            echo "FALSE 1";
            return;
        }
        $vObjUsuarioPosicao = new EXTDAO_Usuario_posicao();
        foreach ($vVetorTupla as $key => $vTupla)
        {
            $vVetorValorAtributo = explode(";", $vTupla);
            $vObjUsuarioPosicao->setUsuario_id_INT($idUsuario);

            //sem usuario e sem corporacao
            if (count($vVetorValorAtributo) == 9)
            {
                $vData = $vVetorValorAtributo[0];
                $vObjUsuarioPosicao->setData_DATE($vData);
                $vHora = $vVetorValorAtributo[1];
                $vObjUsuarioPosicao->setHora_TIME($vHora);

                //faca a verificacao quando for escolhido o veiculo usuario
                //ESTE DEVE ESTAR SINCRONIZADO
                $vVeiculoUsuario = $vVetorValorAtributo[2];
                if (strcmp($vVeiculoUsuario, "null") != 0)
                {
                    $vObjUsuarioPosicao->setVeiculo_usuario_id_INT($vVeiculoUsuario);
                }
                else
                {
                    $vObjUsuarioPosicao->setVeiculo_usuario_id_INT(null);
                }
                $vIdUsuario = $vVetorValorAtributo[3];
                if (strcmp($vIdUsuario, "null") != 0)
                {
                    $vObjUsuarioPosicao->setUsuario_id_INT($vIdUsuario);
                }
                else
                {
                    $vObjUsuarioPosicao->setUsuario_id_INT(null);
                }

                $vLongitude = $vVetorValorAtributo[4];
                $vObjUsuarioPosicao->setLongitude_INT($vLongitude);
                $vLatitude = $vVetorValorAtributo[5];
                $vObjUsuarioPosicao->setLatitude_INT($vLatitude);
                $vVelocidade = $vVetorValorAtributo[6];
                $vObjUsuarioPosicao->setVelocidade_INT($vVelocidade);

                $vFotoInterna = $vVetorValorAtributo[7];
                if (strcmp($vFotoInterna, "null") != 0)
                {
                    $vObjUsuarioPosicao->setFoto_interna($vFotoInterna);
                }
                else
                {
                    $vObjUsuarioPosicao->setFoto_interna(null);
                }

                $vFotoExterna = $vVetorValorAtributo[8];
                if (strcmp($vFotoExterna, "null") != 0)
                {
                    $vObjUsuarioPosicao->setFoto_externa($vFotoExterna);
                }
                else
                {
                    $vObjUsuarioPosicao->setFoto_externa(null);
                }
                $vObjUsuarioPosicao->formatarParaSQL();
                $vObjUsuarioPosicao->insert(false, $idCorporacao);
            }
        }
        echo "TRUE";
    }

    public static function checaIntegridadeDeServicosDoUsuario()
    {
        if (!DatabaseSincronizador::checkUsuarioCorporacao())
        {
            echo "FALSE 0";
            return;
        }

        $idUsuario = Helper::POSTGET("usuario");
        $pUsuario = Helper::POSTGET("p_usuario");
        $idCorporacao = Helper::POSTGET("id_corporacao");
        $imei = Helper::POSTGET("imei");
        $idPrograma = Helper::POSTGET("programa");

        if (strlen($idUsuario) > 0)
        {
            $objBanco = new Database();
            $objBanco->query("SELECT COUNT(DISTINCT(s.id))
                                    FROM usuario_servico us, servico s
                                    WHERE us.usuario_id_INT = {$pUsuario} AND 
                                    us.servico_id_INT = s.id AND 
                                    us.corporacao_id_INT = {$idCorporacao}
                                    LIMIT 0,1");
            $possuiOsServicosCadastrados = $objBanco->getPrimeiraTuplaDoResultSet(0);
            echo "POSSUI = " . $possuiOsServicosCadastrados;
            if ($possuiOsServicosCadastrados == 0)
            {
                DatabaseSincronizador::insertTuplasDosServicosDoUsuario(
                    $idUsuario, $idCorporacao, $pUsuario, $imei, $idPrograma);
            }
            echo "TRUE";
        }
    }

    public static function receiveListaIdServicoAtivo()
    {
        if (!DatabaseSincronizador::checkUsuarioCorporacao())
        {
            echo "FALSE 0";
            return;
        }

        $idUsuario = Helper::POSTGET("p_usuario");
        $idCorporacao = Helper::POSTGET("id_corporacao");

        if (strlen($idUsuario) > 0)
        {
            $objBanco = new Database();
            $q = "SELECT DISTINCT(s.id)
                    FROM usuario_servico us, servico s
                    WHERE us.usuario_id_INT = {$idUsuario} AND 
                    us.servico_id_INT = s.id AND 
                    us.status_BOOLEAN = '1' AND 
                    us.corporacao_id_INT = {$idCorporacao}";

            $objBanco->query($q);
            $vValidade = false;
            $vVetor = Helper::getResultSetToArrayDeUmCampo($objBanco->result);
            foreach ($vVetor as $key => $value)
            {
                if (!$vValidade)
                {
                    $vValidade = true;
                }
                else
                {
                    echo DatabaseSincronizador::$DELIMITADOR_COLUNA;
                }

                echo $value;
            }
        }
    }

    public static function rastrearRotaUsuarioAndroidDatabase()
    {
        if (!DatabaseSincronizador::checkUsuarioCorporacao())
        {
            echo "FALSE 0";
            return;
        }
        //se o veiculo = null => significa que todo veiculo do usuario em questao eh valido
        $idVeiculo = Helper::POSTGET("veiculo");
        //se o usuario = null => significa que todo usuario que dirigiu o veiculo em questao eh valido
        $idUsuario = Helper::POSTGET("p_usuario");

        $dataInicio = Helper::POSTGET("data_inicio");
        $horaInicio = Helper::POSTGET("hora_inicio");

        $dataFim = Helper::POSTGET("data_fim");
        $horaFim = Helper::POSTGET("hora_fim");

        $velocidadeMinima = Helper::POSTGET("velocidade_minima");

        $limiteInferior = Helper::POSTGET("limite_inferior");
        $numeroTuplas = Helper::POSTGET("numero_tuplas");

        $somenteComFoto = Helper::POSTGET("somente_com_foto");

        $idVeiculoSelecionado = false;
        $idUsuarioSelecionado = false;

        if (strlen($idVeiculo) > 0)
        {
            $idVeiculoSelecionado = true;
        }

        if (strlen($idUsuario) > 0)
        {
            $idUsuarioSelecionado = true;
        }

        if ($idVeiculoSelecionado || $idUsuarioSelecionado &&
            (is_numeric($limiteInferior) && is_numeric($numeroTuplas))
        )
        {
            //SELECT
            $vStrQuerySelect = "SELECT DISTINCT up.id AS up_id, 
                CONCAT(up.data_DATE, ' ', up.hora_TIME) AS data_hora,
                up.veiculo_usuario_id_INT vu_id, 
                up.usuario_id_INT u_id,
                up.latitude_INT latitude, 
                up.longitude_INT longitude,
                up.velocidade_INT velocidade,
                up.foto_interna foto_interna,
                up.foto_externa foto_externa ";

            //FROM
            $vStrQuerySelect .= " FROM usuario_posicao up LEFT OUTER JOIN veiculo_usuario vu ON up.veiculo_usuario_id_INT = vu.id ";

            //WHERE

            $vStrCorpoWhere = "";

            if (strlen($idVeiculo) > 0)
            {
                if (strlen($vStrCorpoWhere))
                {
                    $vStrCorpoWhere .= " AND vu.veiculo_id_INT = {$idVeiculo} ";
                }
                else
                {
                    $vStrCorpoWhere .= " vu.veiculo_id_INT = {$idVeiculo} ";
                }
            }

            if (strlen($dataInicio) > 0 && strlen($horaInicio) > 0)
            {
                $dataInicio = trim($dataInicio);
                $horaInicio = trim($horaInicio);
                $datetimeInicio = $dataInicio . " " . $horaInicio;
                if (strlen($vStrCorpoWhere))
                {
                    $vStrCorpoWhere .= " AND CONCAT(up.data_DATE, ' ', up.hora_TIME)  >= '$datetimeInicio' ";
                }
                else
                {
                    $vStrCorpoWhere .= " CONCAT(up.data_DATE, ' ', up.hora_TIME)  >= '$datetimeInicio' ";
                }
            }
            elseif (strlen($dataInicio) > 0)
            {
                $dataInicio = trim($dataInicio);
                if (strlen($vStrCorpoWhere))
                {
                    $vStrCorpoWhere .= " AND up.data_DATE >= '$dataInicio' ";
                }
                else
                {
                    $vStrCorpoWhere .= " up.data_DATE >= '$dataInicio' ";
                }
            }

            if (strlen($velocidadeMinima) > 0)
            {
                $velocidadeMinima = trim($velocidadeMinima);
                if (strlen($vStrCorpoWhere))
                {
                    $vStrCorpoWhere .= " AND up.velocidade_INT >= '$velocidadeMinima' ";
                }
                else
                {
                    $vStrCorpoWhere .= " up.velocidade_INT >= '$velocidadeMinima' ";
                }
            }

            if (strlen($dataFim) > 0 && strlen($horaFim) > 0)
            {
                $dataFim = trim($dataFim);
                $horaFim = trim($horaFim);
                $datetimeFim = $dataFim . " " . $horaFim;
                if (strlen($vStrCorpoWhere))
                {
                    $vStrCorpoWhere .= " AND CONCAT(up.data_DATE, ' ', up.hora_TIME)  <= '$datetimeFim' ";
                }
                else
                {
                    $vStrCorpoWhere .= " CONCAT(up.data_DATE, ' ', up.hora_TIME)  <= '$datetimeFim' ";
                }
            }
            elseif (strlen($dataFim) > 0)
            {
                $dataFim = trim($dataFim);
                if (strlen($vStrCorpoWhere))
                {
                    $vStrCorpoWhere .= " AND up.data_DATE <= '$dataFim' ";
                }
                else
                {
                    $vStrCorpoWhere .= " up.data_DATE <= '$dataFim' ";
                }
            }
            if (strlen($somenteComFoto) > 0)
            {
                if ($somenteComFoto == "1")
                {
                    if (strlen($vStrCorpoWhere))
                    {
                        $vStrCorpoWhere .= " AND (up.foto_interna NOT IS NULL OR up.foto_externa NOT IS NULL) ";
                    }
                    else
                    {
                        $vStrCorpoWhere .= " (up.foto_interna NOT IS NULL OR up.foto_externa NOT IS NULL) ";
                    }
                }
            }

            $vStrOrderBy .= " ORDER BY data_hora ";
            $objBanco = new Database();
            if (strlen($vStrCorpoWhere))
            {
                $vStrCorpoWhere = " WHERE " . $vStrCorpoWhere;
            }

            $vStrQuery = $vStrQuerySelect . $vStrCorpoWhere . $vStrOrderBy;
            $vStrQuery .= " LIMIT " . $limiteInferior . ", " . $numeroTuplas;
            $objBanco->query($vStrQuery);

            $v_vetorObjetos = Helper::getResultSetToArrayDeObjetosWithExplicitNull($objBanco->result);

            if (count($v_vetorObjetos) > 0)
            {
                $isPrimeiraVez = true;
                foreach ($v_vetorObjetos as $key => $obj)
                {
                    if ($isPrimeiraVez)
                    {
                        $isPrimeiraVez = false;
                    }
                    else
                    {
                        echo DatabaseSincronizador::$DELIMITADOR_LINHA;
                    }

                    echo $obj["up_id"] . DatabaseSincronizador::$DELIMITADOR_COLUNA;
                    echo $obj["data_hora"] . DatabaseSincronizador::$DELIMITADOR_COLUNA;
                    echo $obj["vu_id"] . DatabaseSincronizador::$DELIMITADOR_COLUNA;
                    echo $obj["u_id"] . DatabaseSincronizador::$DELIMITADOR_COLUNA;
                    echo $obj["latitude"] . DatabaseSincronizador::$DELIMITADOR_COLUNA;
                    echo $obj["longitude"] . DatabaseSincronizador::$DELIMITADOR_COLUNA;
                    echo $obj["velocidade"] . DatabaseSincronizador::$DELIMITADOR_COLUNA;
                    echo $obj["foto_interna"] . DatabaseSincronizador::$DELIMITADOR_COLUNA;
                    echo $obj["foto_externa"];
                }
            }
        }
    }

    public static function getTotalTuplasRastrearRotaUsuarioAndroidDatabase()
    {
        if (!DatabaseSincronizador::checkUsuarioCorporacao())
        {
            echo "FALSE 0";
            return;
        }

        //se o veiculo = null => significa que todo veiculo do usuario em questao eh valido
        $idVeiculo = Helper::POSTGET("veiculo");

        //se o usuario = null => significa que todo usuario que dirigiu o veiculo em questao eh valido
        $idUsuario = Helper::POSTGET("p_usuario");

        $dataInicio = Helper::POSTGET("data_inicio");
        $horaInicio = Helper::POSTGET("hora_inicio");

        $dataFim = Helper::POSTGET("data_fim");
        $horaFim = Helper::POSTGET("hora_fim");

        $velocidadeMinima = Helper::POSTGET("velocidade_minima");
        $somenteComFoto = Helper::POSTGET("somente_com_foto");

        $idVeiculoSelecionado = false;
        $idUsuarioSelecionado = false;

        if (strlen($idVeiculo) > 0)
        {
            $idVeiculoSelecionado = true;
        }

        if (strlen($idUsuario) > 0)
        {
            $idUsuarioSelecionado = true;
        }

        if ($idVeiculoSelecionado || $idUsuarioSelecionado)
        {
            //SELECT
            $vStrQuerySelect = "SELECT COUNT(up.id) AS total ";

            //FROM
            $vStrQuerySelect .= " FROM usuario_posicao up LEFT OUTER JOIN veiculo_usuario vu ON up.veiculo_usuario_id_INT = vu.id ";

            //WHERE

            $vStrCorpoWhere = "";

            if (strlen($idVeiculo) > 0)
            {
                if (strlen($vStrCorpoWhere))
                {
                    $vStrCorpoWhere .= " AND vu.veiculo_id_INT = {$idVeiculo} ";
                }
                else
                {
                    $vStrCorpoWhere .= " vu.veiculo_id_INT = {$idVeiculo} ";
                }
            }

            if (strlen($dataInicio) > 0 && strlen($horaInicio) > 0)
            {
                $dataInicio = trim($dataInicio);
                $horaInicio = trim($horaInicio);
                $datetimeInicio = $dataInicio . " " . $horaInicio;
                if (strlen($vStrCorpoWhere))
                {
                    $vStrCorpoWhere .= " AND CONCAT(up.data_DATE, ' ', up.hora_TIME)  >= '$datetimeInicio' ";
                }
                else
                {
                    $vStrCorpoWhere .= " CONCAT(up.data_DATE, ' ', up.hora_TIME)  >= '$datetimeInicio' ";
                }
            }
            elseif (strlen($dataInicio) > 0)
            {
                $dataInicio = trim($dataInicio);
                if (strlen($vStrCorpoWhere))
                {
                    $vStrCorpoWhere .= " AND up.data_DATE >= '$dataInicio' ";
                }
                else
                {
                    $vStrCorpoWhere .= " up.data_DATE >= '$dataInicio' ";
                }
            }

            if (strlen($velocidadeMinima) > 0)
            {
                $velocidadeMinima = trim($velocidadeMinima);
                if (strlen($vStrCorpoWhere))
                {
                    $vStrCorpoWhere .= " AND up.velocidade_INT >= '$velocidadeMinima' ";
                }
                else
                {
                    $vStrCorpoWhere .= " up.velocidade_INT >= '$velocidadeMinima' ";
                }
            }

            if (strlen($dataFim) > 0 && strlen($horaFim) > 0)
            {
                $dataFim = trim($dataFim);
                $horaFim = trim($horaFim);
                $datetimeFim = $dataFim . " " . $horaFim;
                if (strlen($vStrCorpoWhere))
                {
                    $vStrCorpoWhere .= " AND CONCAT(up.data_DATE, ' ', up.hora_TIME)  <= '$datetimeFim' ";
                }
                else
                {
                    $vStrCorpoWhere .= " CONCAT(up.data_DATE, ' ', up.hora_TIME)  <= '$datetimeFim' ";
                }
            }
            elseif (strlen($dataFim) > 0)
            {
                $dataFim = trim($dataFim);
                if (strlen($vStrCorpoWhere))
                {
                    $vStrCorpoWhere .= " AND up.data_DATE <= '$dataFim' ";
                }
                else
                {
                    $vStrCorpoWhere .= " up.data_DATE <= '$dataFim' ";
                }
            }
            if (strlen($somenteComFoto) > 0)
            {
                if ($somenteComFoto == "1")
                {
                    if (strlen($vStrCorpoWhere))
                    {
                        $vStrCorpoWhere .= " AND (up.foto_interna NOT IS NULL OR up.foto_externa NOT IS NULL) ";
                    }
                    else
                    {
                        $vStrCorpoWhere .= " (up.foto_interna NOT IS NULL OR up.foto_externa NOT IS NULL) ";
                    }
                }
            }

            $objBanco = new Database();
            if (strlen($vStrCorpoWhere))
            {
                $vStrCorpoWhere = " WHERE " . $vStrCorpoWhere;
            }

            $vStrQuery = $vStrQuerySelect . $vStrCorpoWhere;
            $objBanco->query($vStrQuery);

            $v_vetorObjetos = Helper::getResultSetToArrayDeObjetosWithExplicitNull($objBanco->result);

            if (count($v_vetorObjetos) > 0)
            {
                $isPrimeiraVez = true;
                foreach ($v_vetorObjetos as $key => $obj)
                {
                    echo $obj["total"];
                    break;
                }
            }
        }
    }

    private static function getLastDatetimeIdUsuarioFotoAndroidDatabase($usuario, $corporacao)
    {
        if (strlen($usuario) > 0 && strlen($corporacao) > 0)
        {
            $vStrQuery = "SELECT MIN(uf.id) AS uf_id,
                            CONCAT(uf.data_DATE, ' ', uf.hora_TIME) AS data_hora 
                            FROM usuario_foto as uf 
                            WHERE 
                            uf.usuario_id_INT = {$usuario} ";
            $objBanco = new Database();
            $objBanco->query($vStrQuery);

            $v_vetorObjetos = Helper::getResultSetToArrayDeObjetosWithExplicitNull($objBanco->result);
            if (count($v_vetorObjetos) > 0)
            {
                foreach ($v_vetorObjetos as $key => $obj)
                {
                    return $obj->data_hora;
                }
            }
        }
    }

    public static function getListIdUsuarioFotoAndroidDatabase()
    {
        //se o veiculo = null => significa que todo veiculo do usuario em questao eh valido
        //se o usuario = null => significa que todo usuario que dirigiu o veiculo em questao eh valido
        $idUsuario = Helper::POSTGET("usuario");
        $idCorporacao = Helper::POSTGET("id_corporacao");

        $dataInicio = Helper::POSTGET("data_inicio");
        $horaInicio = Helper::POSTGET("hora_inicio");

        $dataFim = Helper::POSTGET("data_fim");
        $horaFim = Helper::POSTGET("hora_fim");

        if (strlen($idUsuario) > 0 &&
            strlen($idCorporacao) > 0 &&
            (strlen($dataInicio) > 0 || strlen($dataFim) > 0)
        )
        {
            //SELECT
            $vStrQuery = "SELECT uf.id AS uf_id,
                CONCAT(uf.data_DATE, ' ', uf.hora_TIME) AS data_hora ";

            //FROM
            $vStrQuery .= " FROM usuario_foto as uf ";

            //WHERE
            $vStrQuery .= " WHERE ";
            $vStrQuery .= "  uf.usuario_id_INT = {$idUsuario} ";

            if (strlen($dataInicio) > 0 && strlen($horaInicio) > 0)
            {
                $dataInicio = trim($dataInicio);
                $horaInicio = trim($horaInicio);
                $datetimeInicio = $dataInicio . " " . $horaInicio;
                $vStrQuery .= " AND CONCAT(uf.data_DATE, ' ', uf.hora_TIME)  >= '$datetimeInicio' ";
            }
            elseif (strlen($dataInicio) > 0)
            {
                $dataInicio = trim($dataInicio);
                $vStrQuery .= " AND uf.data_DATE >= '$dataInicio' ";
            }

            if (strlen($dataFim) > 0 && strlen($horaFim) > 0)
            {
                $dataFim = trim($dataFim);
                $horaFim = trim($horaFim);
                $datetimeFim = $dataFim . " " . $horaFim;
                $vStrQuery .= " AND CONCAT(uf.data_DATE, ' ', uf.hora_TIME)  <= '$datetimeFim' ";
            }
            elseif (strlen($dataFim) > 0)
            {
                $dataFim = trim($dataFim);
                $vStrQuery .= " AND uf.data_DATE <= '$dataFim' ";
            }

            $vStrQuery .= " ORDER BY data_hora ";
            $objBanco = new Database();
            $objBanco->query($vStrQuery);
            $v_vetorObjetos = Helper::getResultSetToArrayDeObjetosWithExplicitNull($objBanco->result);

            if (count($v_vetorObjetos) > 0)
            {
                $vIsPrimeiraVez = true;

                foreach ($v_vetorObjetos as $key => $obj)
                {
                    if (strlen($obj["uf_id"]) > 0)
                    {
                        if (!$vIsPrimeiraVez)
                        {
                            echo DatabaseSincronizador::$DELIMITADOR_LINHA;
                        }
                        else
                        {
                            $vIsPrimeiraVez = false;
                        }
                        echo $obj["uf_id"] . DatabaseSincronizador::$DELIMITADOR_COLUNA;
                        echo $obj["data_hora"];
                    }
                    else
                    {
                        continue;
                    }
                }
                if ($vIsPrimeiraVez)
                {
                    $dataHora = DatabaseSincronizador::getLastDatetimeIdUsuarioFotoAndroidDatabase($idUsuario, $idCorporacao);
                    if (strlen($dataHora) > 0)
                    {
                        echo "FALSE# " . $dataHora;
                    }
                }
            }
            else
            {
                $dataHora = DatabaseSincronizador::getLastDatetimeIdUsuarioFotoAndroidDatabase($idUsuario, $idCorporacao);
                if (strlen($dataHora) > 0)
                {
                    echo "FALSE# " . $dataHora;
                }
                else
                {
                    echo "FALSE";
                }
            }
        }
    }

    public static function disponibilizaTodoVeiculoOcupadoPeloUsuario()
    {
        if (!DatabaseSincronizador::checkUsuarioCorporacao())
        {
            echo "FALSE 0";
            return;
        }

        $pIdUsuario = Helper::POSTGET("usuario");
        $pIdCorpoaracao = Helper::POSTGET("id_corporacao");
        if (strlen($pIdUsuario) > 0)
        {
            $objBanco = new Database();
            $query = "SELECT vu.id 
                         FROM veiculo_usuario vu
                         WHERE vu.usuario_id_INT = {$pIdUsuario} AND
                         vu.corporacao_id_INT = {$pIdCorpoaracao} AND
                         vu.is_ativo_BOOLEAN = '1'  ";

            $objBanco->query($query);
            if ($objBanco->rows() == 1)
            {
                $vIdVeiculoUsuarioAtual = $objBanco->getPrimeiraTuplaDoResultSet("id");
                $vVeiculoUsuario = new EXTDAO_Veiculo_usuario();
                $vVeiculoUsuario->select($vIdVeiculoUsuarioAtual, $pIdCorpoaracao);
                $vVeiculoUsuario->setIs_ativo_BOOLEAN("0");
                $vVeiculoUsuario->formatarParaSQL();
                $vVeiculoUsuario->update($vIdVeiculoUsuarioAtual, "vazio", 1, true, $pIdCorporacao);
            }
            else
            {
                if ($objBanco->rows() == 0)
                {
                    echo "TRUE";
                }
                else
                {
                    echo "ERROR 1";
                }
            }
        }
        else
        {
            echo "ERROR 2";
        }
    }

    public static function disponibilizaVeiculo()
    {
        if (!DatabaseSincronizador::checkUsuarioCorporacao())
        {
            echo "FALSE 0";
            return;
        }
        $pIdVeiculoUsuario = Helper::POSTGET("veiculo_usuario");
        $pIdVeiculo = Helper::POSTGET("veiculo");
        $pIdCorporacao = Helper::POSTGET("id_corporacao");
        if (strlen($pIdVeiculoUsuario) > 0 &&
            strlen($pIdVeiculo) > 0
        )
        {
            $vVeiculoUsuario = new EXTDAO_Veiculo_usuario();
            $vVeiculoUsuario->select($pIdVeiculoUsuario, $pIdCorporacao);
            $vVeiculoUsuario->setIs_ativo_BOOLEAN("0");
            $vVeiculoUsuario->formatarParaSQL();
            $vVeiculoUsuario->update($pIdVeiculoUsuario, "vazio", 1, true, $pIdCorporacao);
        }
        else
        {
            echo "ERROR : parametros invalidos";
        }
    }

    public static function disponibilizaVeiculoPorOutroUsuario()
    {
        if (!DatabaseSincronizador::checkUsuarioCorporacao())
        {
            echo "FALSE 0";
            return;
        }
        $pIdVeiculo = Helper::POSTGET("veiculo");
        $pIdCorporacao = Helper::POSTGET("id_corporacao");
        if (strlen($pIdVeiculo) > 0)
        {
            $objBanco = new Database();
            $query = "SELECT vu.id 
                         FROM veiculo_usuario vu
                         WHERE vu.veiculo_id_INT = {$pIdVeiculo} AND
                         vu.corporacao_id_INT = {$pIdCorporacao} AND
                         vu.is_ativo_BOOLEAN = '1'  ";

            $objBanco->query($query);
            if ($objBanco->rows() == 1)
            {
                $vIdVeiculoUsuarioAtual = $objBanco->getPrimeiraTuplaDoResultSet("id");
                $vVeiculoUsuario = new EXTDAO_Veiculo_usuario();
                $vVeiculoUsuario->select($vIdVeiculoUsuarioAtual, $pIdCorporacao);
                $vVeiculoUsuario->setIs_ativo_BOOLEAN("0");
                $vVeiculoUsuario->formatarParaSQL();
                $vVeiculoUsuario->update($vIdVeiculoUsuarioAtual, "vazio", 1, true, $pIdCorporacao);
            }
            else
            {
                if ($objBanco->rows() == 0)
                {
                    echo "TRUE";
                }
                else
                {
                    echo "ERROR 1";
                }
            }
        }
        else
        {
            echo "ERROR 2";
        }
    }

    public static function receiveTabelaTarefaAndroidDatabase()
    {
        if (!DatabaseSincronizador::checkUsuarioCorporacao())
        {
            echo "FALSE 0";
            return;
        }

        $vIdVeiculoUsuario = Helper::POSTGET("veiculo_usuario");
        $listaIdTarefaAbertaExistente = Helper::POSTGET("tarefa_aberta");
        $listaIdTarefaPropriaExistente = Helper::POSTGET("tarefa_propria");
        $listaIdTarefaPropriaExecucaoExistente = Helper::POSTGET("tarefa_propria_execucao");
        $vIdCorporacao = Helper::POSTGET("id_corporacao");

        $vetorTarefaAberta = array();
        $vetorTarefaPropria = array();

        if (strlen($listaIdTarefaAbertaExistente) > 0)
        {
            $vetorTarefaAberta = explode(DatabaseSincronizador::$DELIMITADOR_COLUNA, $listaIdTarefaAbertaExistente);
        }
        if (strlen($listaIdTarefaPropriaExistente) > 0)
        {
            $vetorTarefaPropria = explode(DatabaseSincronizador::$DELIMITADOR_COLUNA, $listaIdTarefaPropriaExistente);
        }

        if (strlen($listaIdTarefaPropriaExecucaoExistente) > 0)
        {
            $vetorTarefaPropriaExecucao = explode(DatabaseSincronizador::$DELIMITADOR_COLUNA, $listaIdTarefaPropriaExecucaoExistente);
        }

        $vDiaAtual = Helper::getDiaAtual();

        if (strlen($vIdVeiculoUsuario) > 0)
        {
            $objBanco = new Database();
            $query = "SELECT DISTINCT
                            t.id id,
                            t.veiculo_usuario_id_INT veiculo_usuario,
                            CONCAT(
                                t.origem_logradouro,
                                ', ',
                                t.origem_numero,
                                ', ',
                                b1.nome,
                                ' - ',
                                c1.nome,
                                ', ',
                                e1.nome
                            ) origem_endereco,
                            t.origem_complemento origem_complemento,
                            CONCAT(
                                t.destino_logradouro,
                                ', ',
                                t.destino_numero,
                                ', ',
                                b2.nome,
                                ' - ',
                                c2.nome,
                                ', ',
                                e2.nome
                            ) destino_endereco,
                            t.destino_complemento destino_complemento,
                            t.hora_cadastro_TIME hora_cadastro,
                            t.data_cadastro_DATE data_cadastro,
                            t.descricao_HTML descricao,
                            t.data_exibir_DATE data_abertura,
                            t.inicio_data_programada_DATE data_programada,
                            t.inicio_hora_programada_TIME hora_programada,
                            t.inicio_data_DATE data_inicio,
                            t.inicio_hora_TIME hora_inicio
                        FROM tarefa t LEFT OUTER JOIN bairro b1 ON t.origem_bairro_id_INT = b1.id
                        LEFT OUTER JOIN cidade c1 ON t.origem_cidade_id_INT = c1.id
                        LEFT OUTER JOIN uf e1 ON c1.uf_id_INT = e1.id
                        LEFT OUTER JOIN bairro b2 ON t.destino_bairro_id_INT = b2.id
                        LEFT OUTER JOIN cidade c2 ON t.destino_cidade_id_INT = c2.id
                        LEFT OUTER JOIN uf e2 ON c2.uf_id_INT = e2.id
                        WHERE(
                            t.veiculo_usuario_id_INT = '$vIdVeiculoUsuario'
                            OR t.veiculo_usuario_id_INT IS NULL
                        )
                        AND t.fim_hora_TIME IS NULL
                        AND t.fim_data_DATE IS NULL
                        AND(
                            t.data_exibir_DATE > '$vDiaAtual'
                            OR t.data_exibir_DATE IS NULL
                        )
                        AND t.corporacao_id_INT = '$vIdCorporacao'
                        ";

            $objBanco->query($query);
            $v_vetorObjetos = Helper::getResultSetToArrayDeObjetosWithExplicitNull($objBanco->result);
            $vArrayListTarefasChecadas = array();

            //lista contendo as minhas tarefas NAO FINALIZADAS e as tarefas abertas NAO FINALIZADAS
            foreach ($v_vetorObjetos as $key => $obj)
            {
                $vIsTarefaAberta = false;
                if ($obj["veiculo_usuario"] == "null")
                {
                    $vIsTarefaAberta = true;
                }
                $vIdTarefa = $obj["id"];

//                if(array_search($vIdTarefa, $vetorTarefaAberta)){
                if (Helper::arrayContainValue($vIdTarefa, $vetorTarefaAberta))
                {
                    //se nao estiver mais aberta envia o id da tarefa agora
                    //ocupada pelo proprio usuario
                    if (!$vIsTarefaAberta)
                    {
                        echo $obj["id"] . DatabaseSincronizador::$DELIMITADOR_LINHA;
                        //se o id tarefa esta contido na lista de tarefa aberta
                        //checa se a tarefa ainda continua aberta
                        $vArrayListTarefasChecadas[count($vArrayListTarefasChecadas)] = $obj["id"];
                    }
//                } else if( array_search($vIdTarefa, $vetorTarefaPropria)){
                }
                else
                {
                    if (Helper::arrayContainValue($vIdTarefa, $vetorTarefaPropria))
                    {
                        //se o id tarefa esta contido na lista de tarefa do usuario
                        //se a tarefa estiver aberta
                        if ($vIsTarefaAberta)
                        {
                            echo $obj["id"] . DatabaseSincronizador::$DELIMITADOR_LINHA;
                            //se o id tarefa esta contido na lista de tarefa aberta
                            //checa se a tarefa ainda continua aberta
                            $vArrayListTarefasChecadas[count($vArrayListTarefasChecadas)] = $obj["id"];
                        }
                    }
                    else
                    {
                        if (Helper::arrayContainValue($vIdTarefa, $vetorTarefaPropriaExecucao))
                        {
//                    Nao faz nada
                            continue;
                        }
                        else
                        { //se for uma nova tarefa envia todas as informacoes
                            //podendo ser aberta ou do usuario
                            echo $obj["id"] . DatabaseSincronizador::$DELIMITADOR_COLUNA;
                            echo $obj["veiculo_usuario"] . DatabaseSincronizador::$DELIMITADOR_COLUNA;
                            echo $obj["origem_endereco"] . DatabaseSincronizador::$DELIMITADOR_COLUNA;
                            echo $obj["origem_complemento"] . DatabaseSincronizador::$DELIMITADOR_COLUNA;
                            echo $obj["destino_endereco"] . DatabaseSincronizador::$DELIMITADOR_COLUNA;
                            echo $obj["destino_complemento"] . DatabaseSincronizador::$DELIMITADOR_COLUNA;
                            echo $obj["hora_cadastro"] . DatabaseSincronizador::$DELIMITADOR_COLUNA;
                            echo $obj["data_cadastro"] . DatabaseSincronizador::$DELIMITADOR_COLUNA;
                            echo $obj["descricao"] . DatabaseSincronizador::$DELIMITADOR_COLUNA;
                            echo $obj["data_abertura"] . DatabaseSincronizador::$DELIMITADOR_COLUNA;
                            echo $obj["data_programada"] . DatabaseSincronizador::$DELIMITADOR_COLUNA;
                            echo $obj["hora_programada"] . DatabaseSincronizador::$DELIMITADOR_COLUNA;
                            echo $obj["data_inicio"] . DatabaseSincronizador::$DELIMITADOR_COLUNA;
                            echo $obj["hora_inicio"] . DatabaseSincronizador::$DELIMITADOR_LINHA;
                        }
                    }
                }
            }
            $vValidade = false;
            //verifica os id's que nao estao mais nos id's das atuais listas enviadas
            //DELETE;1;2;3
            foreach ($vArrayListTarefasChecadas as $key => $vIndex)
            {
                //se constava na lista de tarefas abertas e nao consta mais 
                //entao deleta a tarefa pois eh de outro veiculo usuario
                if (!array_search($vIndex, $vetorTarefaAberta))
                {
                    if (!$vValidade)
                    {
                        echo "DELETE";
                        $vValidade = true;
                    }
                    echo DatabaseSincronizador::$DELIMITADOR_COLUNA . $vIndex;
                }
                else
                {
                    if (!array_search($vIndex, $vetorTarefaPropria))
                    {
                        //se constava na lista propria de tarefas e nao consta mais
                        //entao deleta a tarefa pois eh de outro veiculo usuario
                        if (!$vValidade)
                        {
                            echo "DELETE";
                            $vValidade = true;
                        }
                        echo DatabaseSincronizador::$DELIMITADOR_COLUNA . $vIndex;
                    }
                }
            }
        }
    }

    public static function isVeiculoDisponivel()
    {
        if (!DatabaseSincronizador::checkUsuarioCorporacao())
        {
            echo "FALSE 0";
            return;
        }
        $pIdVeiculoUsuario = Helper::POSTGET("veiculo_usuario");
        $pIdVeiculo = Helper::POSTGET("veiculo");
        $pIdCorporacao = Helper::POSTGET("id_corporacao");
        if (strlen($pIdVeiculoUsuario) > 0 &&
            strlen($pIdVeiculo) > 0
        )
        {
            $objBanco = new Database();
            $query = "SELECT vu.id 
                         FROM veiculo_usuario vu
                         WHERE vu.veiculo_id_INT = {$pIdVeiculo} AND
                         vu.is_ativo_BOOLEAN = '1' AND 
                         vu.corporacao_id_INT = {$pIdCorporacao}";

            $objBanco->query($query);
            if ($objBanco->rows() == 1)
            {
                $vIdVeiculoUsuarioAtual = $objBanco->getPrimeiraTuplaDoResultSet("id");
                if ($vIdVeiculoUsuarioAtual == $pIdVeiculoUsuario)
                {
                    echo "TRUE";
                }
                else
                {
                    echo "ERROR 1: Veiculo ocupado por $vIdVeiculoUsuarioAtual.";
                }
            }
            else
            {
                if ($objBanco->rows() == 0)
                {
                    $vVeiculoUsuario = new EXTDAO_Veiculo_usuario();
                    $vVeiculoUsuario->select($pIdVeiculoUsuario, $pIdCorporacao);
                    $vVeiculoUsuario->setIs_ativo_BOOLEAN("1");
                    $vVeiculoUsuario->formatarParaSQL();
                    $vVeiculoUsuario->update($pIdVeiculoUsuario, "vazio", 1, true, $pIdCorporacao);

                    echo "TRUE";
                }
                else
                {
                    echo "ERROR 1";
                }
            }
        }
        else
        {
            echo "ERROR 2";
        }
    }

    public static function checkVeiculoUsuarioDaTarefa()
    {
        if (!DatabaseSincronizador::checkUsuarioCorporacao())
        {
            echo "FALSE 0";
            return;
        }
//        $pIdSistemaTabela = Helper::POSTGET("sistema_tabela");
        $pIdSistemaTabelaTarefa = DatabaseSincronizador::getIdDaSistemaTabela("tarefa");
        $pUsuario = Helper::POSTGET("usuario");
        $idCorporacao = Helper::POSTGET("id_corporacao");
        $pIMEI = Helper::POSTGET("imei");
        $idPrograma = Helper::POSTGET("id_programa");
        $pIdTarefa = Helper::POSTGET("tarefa");

        if (strlen($pIdTarefa) > 0 &&
            strlen($pIdSistemaTabelaTarefa) > 0
        )
        {
            $EXTDAOTarefa = new EXTDAO_Tarefa();
            $EXTDAOTarefa->select($pIdTarefa, $idCorporacao);

            $vIdUsuarioDaTarefa = $EXTDAOTarefa->getUsuario_id_INT();
            $vIsTarefaOcupada = true;
            if (DatabaseSincronizador::checkSeATarefaEstaAberta($idCorporacao, $pIdTarefa))
            {
                $vIsTarefaOcupada = false;
            }

            if ($vIsTarefaOcupada == true)
            {
                echo "ERROR 1: Tarefa '$pIdTarefa' ocupada por '$vIdUsuarioDaTarefa'.";
            }
            else
            {
                $EXTDAOTarefa->setUsuario_id_INT($pUsuario);
                $EXTDAOTarefa->formatarParaSQL();
                $EXTDAOTarefa->update($pIdTarefa, "vazio", 1, true, $idCorporacao);

                EXTDAO_Sistema_registro_sincronizador::insertTupla(
                    $pIdSistemaTabelaTarefa,
                    $pUsuario,
                    $idCorporacao,
                    '1',
                    DatabaseSincronizador::$INDEX_OPERACAO_EDIT,
                    $pIMEI,
                    $idPrograma,
                    $pIdTarefa);

                echo "TRUE";
            }
        }
        else
        {
            echo "ERROR 2: parametros invalidos: $pUsuario, $pIdTarefa.";
        }
    }

    public static function setDataDeFimDeExcecucaoDaTarefa()
    {
        if (!DatabaseSincronizador::checkUsuarioCorporacao())
        {
            echo "FALSE 0";
            return;
        }
        $pLatitude = Helper::POSTGET("latitude");
        $pLongitude = Helper::POSTGET("longitude");
        $pIMEI = Helper::POSTGET("imei");
        $idPrograma = Helper::POSTGET("id_programa");
        $pIdTarefa = Helper::POSTGET("tarefa");
//        $pIdSistemaTabela = Helper::POSTGET("sistema_tabela");
        $pIdSistemaTabelaTarefa = DatabaseSincronizador::getIdDaSistemaTabela("tarefa");
        $idCorporacao = Helper::POSTGET("id_corporacao");
        $pUsuario = Helper::POSTGET("usuario");
        if (strlen($pIdTarefa) > 0 &&
            strlen($pIdSistemaTabelaTarefa) > 0 &&
            strlen($pLatitude) > 0 && strlen($pLongitude) > 0
        )
        {
            if (DatabaseSincronizador::checkSeATarefaEstaFinalizada($idCorporacao, $pIdTarefa))
            {
                echo "FALSE 2: Tarefa já foi finalizada";
                return;
            }

            $EXTDAOTarefa = new EXTDAO_Tarefa();
            $EXTDAOTarefa->select($pIdTarefa, $idCorporacao);

            $vStrDataAtual = Helper::getDiaAtual();
            $vStrHoraAtual = Helper::getHoraAtual();

            $vIdUsuario = $EXTDAOTarefa->getUsuario_id_INT();
            if (DatabaseSincronizador::checkSeATarefaDoUsuario($idCorporacao, $vIdUsuario, $pIdTarefa))
            {
                $EXTDAOTarefa->setUsuario_id_INT($pUsuario);
                $EXTDAOTarefa->setFim_hora_TIME($vStrHoraAtual);
                $EXTDAOTarefa->setFim_data_DATE($vStrDataAtual);
                $EXTDAOTarefa->setDestino_latitude_real_INT($pLatitude);
                $EXTDAOTarefa->setDestino_longitude_real_INT($pLongitude);
                $EXTDAOTarefa->formatarParaSQL();
                $EXTDAOTarefa->update($pIdTarefa, "vazio", 1, true, $idCorporacao);

                EXTDAO_Sistema_registro_sincronizador::insertTupla(
                    $pIdSistemaTabelaTarefa, $pUsuario, $idCorporacao, '1', DatabaseSincronizador::$INDEX_OPERACAO_EDIT, $pIMEI, $idPrograma, $pIdTarefa);

                echo "TRUE: $vStrDataAtual $vStrHoraAtual";
            }
            else
            {
                echo "ERROR 1: Tarefa '$pIdTarefa' ocupada por outro veiculo, '$vIdVeiculoUsuario'.";
            }
        }
        else
        {
            echo "ERROR 2: parametros invalidos: $vIdUsuario, $pIdTarefa.";
        }
    }

    private static function checkSeATarefaDoUsuario($pIdCorporacao, $pIdUsuario, $pIdTarefa)
    {
        $objBanco = new Database();

        //checa a existencia do funcionario
        $query = "SELECT t.id as id
                FROM tarefa t LEFT JOIN veiculo_usuario vu ON 
                                (t.veiculo_usuario_id_INT = vu.id AND vu.usuario_id_INT = $pIdUsuario) 
                                    OR 
                                (t.veiculo_id_INT = vu.veiculo_id_INT AND vu.usuario_id_INT = $pIdUsuario )
                    LEFT JOIN usuario_categoria_permissao ucp ON ucp.usuario_id_INT = $pIdUsuario AND ucp.categoria_permissao_id_INT = t.categoria_permissao_id_INT
                WHERE t.corporacao_id_INT = $pIdCorporacao AND
                    t.id = $pIdTarefa AND (t.usuario_id_INT IS NULL OR t.usuario_id_INT = $pIdUsuario)
                LIMIT 0,1";

        $objBanco->query($query);

        //o usuario ja eh existente
        if ($objBanco->rows() == 1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private static function checkSeATarefaEstaFinalizada($pIdCorporacao, $pIdTarefa)
    {
        $objBanco = new Database();

        //checa a existencia do funcionario
        $query = "SELECT t.id as id
                FROM tarefa t
                WHERE t.corporacao_id_INT = $pIdCorporacao AND
                    t.id = $pIdTarefa AND t.fim_data_DATE IS NULL
                LIMIT 0,1";

        $objBanco->query($query);

        if ($objBanco->rows() == 1)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    private static function checkSeATarefaEstaInicializada($pIdCorporacao, $pIdTarefa)
    {
        $objBanco = new Database();

        //checa a existencia do funcionario
        $query = "SELECT t.id as id
                FROM tarefa t
                WHERE t.corporacao_id_INT = $pIdCorporacao AND
                    t.id = $pIdTarefa AND t.inicio_data_DATE IS NULL
                LIMIT 0,1";

        $objBanco->query($query);

        //o usuario ja eh existente
        if ($objBanco->rows() == 1)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    private static function checkSeATarefaEstaAberta($pIdCorporacao, $pIdTarefa)
    {
        $objBanco = new Database();

        //checa a existencia do funcionario
        $query = "SELECT t.id as id
                FROM tarefa t
                WHERE t.corporacao_id_INT = $pIdCorporacao AND
                    t.id = $pIdTarefa AND t.usuario_id_INT IS NULL
                LIMIT 0,1";

        $objBanco->query($query);

        //o usuario ja eh existente
        if ($objBanco->rows() == 1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private static function checkSeDataMaiorOuNulaDaTarefa($pIdCorporacao, $pIdTarefa, $pData, $pHora)
    {
        $objBanco = new Database();

        //checa a existencia do funcionario
        $query = "SELECT t.id as id
                FROM tarefa t
                WHERE t.corporacao_id_INT = $pIdCorporacao AND
                    t.id = $pIdTarefa AND 
                    (
                        CONCAT(t.inicio_data_DATE, ' ', t.inicio_hora_TIME) < CONCAT($pData, ' ', $pHora) OR
                        t.inicio_data_DATE IS NULL AND t.inicio_hora_TIME IS NULL
                    )
                LIMIT 0,1";

        $objBanco->query($query);

        //o usuario ja eh existente
        if ($objBanco->rows() == 1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private static function checkSeDataMenorOuNulaDaTarefa($pIdCorporacao, $pIdTarefa, $pData, $pHora)
    {
        $objBanco = new Database();

        //checa a existencia do funcionario
        $query = "SELECT t.id as id
                FROM tarefa t
                WHERE t.corporacao_id_INT = $pIdCorporacao AND
                    t.id = $pIdTarefa AND 
                    (
                        CONCAT(t.inicio_data_DATE, ' ', t.inicio_hora_TIME) > CONCAT($pData, ' ', $pHora) OR
                        t.inicio_data_DATE IS NULL AND t.inicio_hora_TIME IS NULL
                    )
                LIMIT 0,1";

        $objBanco->query($query);

        //o usuario ja eh existente
        if ($objBanco->rows() == 1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static function setDataDeInicioDeExcecucaoDaTarefaOffline()
    {
        if (!DatabaseSincronizador::checkUsuarioCorporacao())
        {
            echo "FALSE 0";
            return;
        }
        $pIMEI = Helper::POSTGET("imei");
        $idPrograma = Helper::POSTGET("id_programa");

        $pIdTarefa = Helper::POSTGET("tarefa");
        $pLatitude = Helper::POSTGET("latitude");
        $pLongitude = Helper::POSTGET("longitude");
        $pData = Helper::POSTGET("data");
        $pHora = Helper::POSTGET("hora");
        $pIdSistemaTabelaTarefa = DatabaseSincronizador::getIdDaSistemaTabela("tarefa");

        $pUsuario = Helper::POSTGET("usuario");
        $idCorporacao = Helper::POSTGET("id_corporacao");

        if (strlen($pIdTarefa) > 0 && strlen($pIdSistemaTabelaTarefa) > 0 &&
            strlen($pLatitude) > 0 && strlen($pLongitude) > 0
        )
        {
            $EXTDAOTarefa = new EXTDAO_Tarefa();
            $EXTDAOTarefa->select($pIdTarefa, $idCorporacao);
            //checar se a data e hora da tarefa no servidor eh menor ou nula comparada a acao de tarefa
            //executada offline
            if (DatabaseSincronizador::checkSeDataMenorOuNulaDaTarefa($idCorporacao, $pTarefa, $pData, $pHora))
            {
                $vStrDataAtual = $pData;
                $vStrHoraAtual = $pHora;

                $vIdUsuario = $EXTDAOTarefa->getUsuario_id_INT();
                if (DatabaseSincronizador::checkSeATarefaDoUsuario($idCorporacao, $vIdUsuario, $pIdTarefa))
                {
                    $EXTDAOTarefa->setUsuario_id_INT($pUsuario);
                    $EXTDAOTarefa->setInicio_hora_TIME($vStrHoraAtual);
                    $EXTDAOTarefa->setInicio_data_DATE($vStrDataAtual);
                    $EXTDAOTarefa->setOrigem_latitude_real_INT($pLatitude);
                    $EXTDAOTarefa->setOrigem_longitude_real_INT($pLongitude);
                    $EXTDAOTarefa->formatarParaSQL();
                    $EXTDAOTarefa->update($pIdTarefa, "vazio", 1, true, $idCorporacao);

                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                        $pIdSistemaTabelaTarefa, $pUsuario, $idCorporacao, '1', DatabaseSincronizador::$INDEX_OPERACAO_EDIT, $pIMEI, $idPrograma, $pIdTarefa);

                    echo "TRUE: $vStrDataAtual $vStrHoraAtual";
                }
                else
                {
                    echo "ERROR 1: Tarefa '$pIdTarefa' não é pertinente ao usuário, '$pUsuario' .";
                }
            }
            else
            {
                echo "ERROR 2: Tarefa foi inicializada anteriormente, logo essa atualizacao nao é valida";
            }
        }
        else
        {
            echo "ERROR 2";
        }
    }

    public static function setDataDeFimDeExcecucaoDaTarefaOffline()
    {
        if (!DatabaseSincronizador::checkUsuarioCorporacao())
        {
            echo "FALSE 0";
            return;
        }

        $pIMEI = Helper::POSTGET("imei");
        $idPrograma = Helper::POSTGET("id_programa");
        $pIdTarefa = Helper::POSTGET("tarefa");

        $pData = Helper::POSTGET("data");
        $pHora = Helper::POSTGET("hora");
        $pLatitude = Helper::POSTGET("latitude");
        $pLongitude = Helper::POSTGET("longitude");

        $pIdSistemaTabelaTarefa = DatabaseSincronizador::getIdDaSistemaTabela("tarefa");

        $pUsuario = Helper::POSTGET("usuario");
        $idCorporacao = Helper::POSTGET("id_corporacao");

        if (strlen($pIdTarefa) > 0 && strlen($pIdSistemaTabelaTarefa) > 0 &&
            strlen($pLatitude) > 0 && strlen($pLongitude) > 0
        )
        {
            $EXTDAOTarefa = new EXTDAO_Tarefa();
            $EXTDAOTarefa->select($pIdTarefa, $idCorporacao);
            //checar se a data e hora da tarefa no servidor eh menor ou nula comparada a acao de tarefa
            //executada offline
            if (DatabaseSincronizador::checkSeDataMaiorOuNulaDaTarefa($idCorporacao, $pTarefa, $pData, $pHora))
            {
                $vStrDataAtual = $pData;
                $vStrHoraAtual = $pHora;

                $vIdUsuario = $EXTDAOTarefa->getUsuario_id_INT();
                if (DatabaseSincronizador::checkSeATarefaDoUsuario($idCorporacao, $vIdUsuario, $pIdTarefa))
                {
                    $EXTDAOTarefa->setUsuario_id_INT($pUsuario);
                    $EXTDAOTarefa->setFim_hora_TIME($vStrHoraAtual);
                    $EXTDAOTarefa->setFim_data_DATE($vStrDataAtual);
                    $EXTDAOTarefa->setDestino_latitude_real_INT($pLatitude);
                    $EXTDAOTarefa->setDestino_longitude_real_INT($pLongitude);

                    $EXTDAOTarefa->formatarParaSQL();
                    $EXTDAOTarefa->update($pIdTarefa, "vazio", 1, true, $idCorporacao);

                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                        $pIdSistemaTabelaTarefa, $pUsuario, $idCorporacao, '1', DatabaseSincronizador::$INDEX_OPERACAO_EDIT, $pIMEI, $idPrograma, $pIdTarefa);

                    echo "TRUE: $vStrDataAtual $vStrHoraAtual";
                }
                else
                {
                    echo "ERROR 1: Tarefa '$pIdTarefa' não é pertinente ao usuário, '$pUsuario' .";
                }
            }
            else
            {
                echo "ERROR 2: Tarefa foi inicializada anteriormente, logo essa atualizacao nao é valida";
            }
        }
        else
        {
            echo "ERROR 2";
        }
    }

    public static function setDataDeInicioDeExcecucaoDaTarefa()
    {
        if (!DatabaseSincronizador::checkUsuarioCorporacao())
        {
            echo "FALSE 0";
            return;
        }
        $pIMEI = Helper::POSTGET("imei");
        $idPrograma = Helper::POSTGET("id_programa");

        $pIdTarefa = Helper::POSTGET("tarefa");

        $pIdSistemaTabelaTarefa = DatabaseSincronizador::getIdDaSistemaTabela("tarefa");
        $pLatitude = Helper::POSTGET("latitude");
        $pLongitude = Helper::POSTGET("longitude");
        $pUsuario = Helper::POSTGET("usuario");
        $idCorporacao = Helper::POSTGET("id_corporacao");
        if (strlen($pIdTarefa) > 0 && strlen($pIdSistemaTabelaTarefa) > 0 &&
            strlen($pLatitude) > 0 && strlen($pLongitude) > 0
        )
        {
            if (DatabaseSincronizador::checkSeATarefaEstaInicializada($idCorporacao, $pIdTarefa))
            {
                echo "FALSE 2: Tarefa já esta inicializada";
                return;
            }

            $EXTDAOTarefa = new EXTDAO_Tarefa();
            $EXTDAOTarefa->select($pIdTarefa, $idCorporacao);

            $vStrDataAtual = Helper::getDiaAtual();
            $vStrHoraAtual = Helper::getHoraAtual();

            $vIdUsuario = $EXTDAOTarefa->getUsuario_id_INT();
            if (DatabaseSincronizador::checkSeATarefaDoUsuario($idCorporacao, $vIdUsuario, $pIdTarefa))
            {
                $EXTDAOTarefa->setUsuario_id_INT($pUsuario);
                $EXTDAOTarefa->setInicio_hora_TIME($vStrHoraAtual);
                $EXTDAOTarefa->setInicio_data_DATE($vStrDataAtual);
                $EXTDAOTarefa->setOrigem_latitude_real_INT($pLatitude);
                $EXTDAOTarefa->setOrigem_longitude_real_INT($pLongitude);
                $EXTDAOTarefa->formatarParaSQL();
                $EXTDAOTarefa->update($pIdTarefa, "vazio", 1, true, $idCorporacao);

                EXTDAO_Sistema_registro_sincronizador::insertTupla(
                    $pIdSistemaTabelaTarefa, $pUsuario, $idCorporacao, '1', DatabaseSincronizador::$INDEX_OPERACAO_EDIT, $pIMEI, $idPrograma, $pIdTarefa);

                echo "TRUE: $vStrDataAtual $vStrHoraAtual";
            }
            else
            {
                echo "ERROR 1: Tarefa '$pIdTarefa' não é pertinente ao usuário, '$pUsuario' .";
            }
        }
        else
        {
            echo "ERROR 2";
        }
    }

    public static function getIdDoUsuario($email)
    {
        if (strlen($email) > 0)
        {
            $objBanco = new Database();

            //checa a existencia do funcionario
            $query = "SELECT u.id as id
                 FROM usuario u
                 WHERE u.email = '" . Helper::formatarCampoTextoHTMLParaSQL($email) . "' ";

            $objBanco->query($query);

            //o usuario ja eh existente
            if ($objBanco->rows() == 1)
            {
                return $objBanco->getPrimeiraTuplaDoResultSet("id");
            }
            else
            {
                return null;
            }
        }
    }

    public static function getIdDaSistemaTabela($pNomeTabela)
    {
        if (strlen($pNomeTabela) > 0)
        {
            $objBanco = new Database();

            //checa a existencia do funcionario
            $query = "SELECT ts.id as id
                 FROM sistema_tabela ts
                 WHERE ts.nome = '" . Helper::formatarCampoTextoHTMLParaSQL($pNomeTabela) . "' ";

            $objBanco->query($query);

            //o usuario ja eh existente
            if ($objBanco->rows() == 1)
            {
                return $objBanco->getPrimeiraTuplaDoResultSet("id");
            }
            else
            {
                return null;
            }
        }
    }

    public static function getIdDaCorporacao($nome)
    {
        if (strlen($nome) > 0)
        {
            $objBanco = new Database();

            //checa a existencia do funcionario
            $query = "SELECT u.id as id
                 FROM corporacao u
                 WHERE u.nome = '" . Helper::formatarCampoTextoHTMLParaSQL($nome) . "' ";

            $objBanco->query($query);

            //o usuario ja eh existente
            if ($objBanco->rows() == 1)
            {
                return $objBanco->getPrimeiraTuplaDoResultSet("id");
            }
            else
            {
                return null;
            }
        }
    }

    public static function getIdDoUsuarioCorporacao($usuario, $corporacao)
    {
        if (strlen($usuario) > 0 && strlen($corporacao) > 0)
        {
            $objBanco = new Database();

            //checa a existencia do funcionario
            $query = "SELECT uc.id as id
                 FROM usuario_corporacao uc
                 WHERE uc.usuario_id_INT = '$usuario' AND
                    uc.corporacao_id_INT = '$corporacao'";

            $objBanco->query($query);

            //o usuario ja eh existente
            if ($objBanco->rows() == 1)
            {
                return $objBanco->getPrimeiraTuplaDoResultSet("id");
            }
            else
            {
                return null;
            }
        }
    }

    public static function checkExistenciaCorporacao($corporacao)
    {
        if (strlen($corporacao) > 0)
        {
            $objBanco = new Database();
            //checa a existencia do funcionario
            $query = "SELECT c.id
            FROM corporacao c
            WHERE c.nome = '" . Helper::formatarCampoTextoHTMLParaSQL($corporacao) . "' ";

            $objBanco->query($query);

            $v_vetorObjetos = Helper::getResultSetToArrayDeObjetos($objBanco->result);

            //o usuario ja eh existente
            if (count($v_vetorObjetos) > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        return false;
    }

    //cadastro do usuario administrativo de um nova corporacao
    public static function receiveCadastroCorporacao()
    {
        if (!DatabaseSincronizador::checkUsuarioCorporacao())
        {
            echo "FALSE 0";
            return;
        }
        //nome do usuario
        $pIdUsuario = Helper::POSTGET("usuario");

        //nome da corporacao ainda nao cadastrada
        $pCorporacao = Helper::POSTGET("p_corporacao");
        $pCorporacaoSemAcento = Helper::POSTGET("p_corporacao_sem_acento");
        $pIMEI = Helper::POSTGET("imei");
        $idPrograma = Helper::POSTGET("id_programa");

        if (strlen($pIdUsuario) > 0 && strlen($pCorporacao) > 0)
        {
            $vIdUsuarioCorporacaoSistemaTabela = DatabaseSincronizador::getIdDaSistemaTabela("usuario_corporacao");

            if (DatabaseSincronizador::checkExistenciaCorporacao($pCorporacao))
            {
                return;
            }
            $vCorporacao = new EXTDAO_Corporacao();
            $vCorporacao->setNome($pCorporacao);
            $vCorporacao->formatarParaSQL();
            $vCorporacao->insert();
            $vIdCorporacao = $vCorporacao->getIdDoUltimoRegistroInserido();

            if (strcmp($vIdCorporacao, "null") == 0)
            {
                $vIdCorporacao = null;
            }
            if ($vIdCorporacao == null)
            {
                echo "ERROR: insercao da corporacao";
                return;
            }

            DatabaseSincronizador::insertTuplasDosServicosDoUsuario(
                $pIdUsuario, $vIdCorporacao, $pIdUsuario, $pIMEI, $idPrograma);

            DatabaseSincronizador::insertTuplasDoTipoDeDocumento(
                $pIdUsuario, $vIdCorporacao, $pIdUsuario, $pIMEI, $idPrograma);
            //cadastra a tupla usuario corporacao
            $vUsuarioCorporacao = new EXTDAO_Usuario_corporacao();
            $vUsuarioCorporacao->setUsuario_id_INT($pIdUsuario);

            $vUsuarioCorporacao->setStatus_BOOLEAN("1");
            $vUsuarioCorporacao->setIs_adm_BOOLEAN("1");
            $vUsuarioCorporacao->formatarParaSQL();
            $vUsuarioCorporacao->insert(false, $vIdCorporacao);
            $vNewIdUsuarioCorporacao = $vUsuarioCorporacao->getIdDoUltimoRegistroInserido($vIdCorporacao);

            EXTDAO_Sistema_registro_sincronizador::insertTupla(
                $vIdUsuarioCorporacaoSistemaTabela, $pIdUsuario, $vIdCorporacao, '1', DatabaseSincronizador::$INDEX_OPERACAO_INSERT, $pIMEI, $idPrograma, $vNewIdUsuarioCorporacao);
            DatabaseSincronizador::procedureAddUsuarioTipo(
                $pIdUsuario, $vIdCorporacao, $pIdUsuario, $pIMEI, $idPrograma);
            echo "TRUE";
            return;
        }
        else
        {
            echo "ERROR: parametro invalido";
            return;
        }
    }

    public static function removeCorporacao()
    {
        if (!DatabaseSincronizador::checkUsuarioCorporacao())
        {
            echo "FALSE 0";
            return;
        }
        //nome do usuario
        $vIdUsuario = Helper::POSTGET("usuario");

        //nome da corporacao ainda nao cadastrada
        $vIdCorporacao = Helper::POSTGET("p_corporacao");

        if (strlen($vIdUsuario) && strlen($vIdCorporacao) > 0)
        {
            if (!DatabaseSincronizador::isUsuarioAdministradorDaCorporacao($vIdUsuario, $vIdCorporacao))
            {
                echo "FALSE 1";
                return;
            }
            else
            {
                $vObjIdCorporacao = new EXTDAO_Corporacao();
                $vObjIdCorporacao->select($vIdCorporacao);
                $nomeCorporacao = $vObjIdCorporacao->getNome();

                $query = "SELECT DISTINCT uc.usuario_id_INT uc_id, u.email email
                FROM usuario_corporacao uc, usuario u
                WHERE uc.usuario_id_INT = u.id AND uc.corporacao_id_INT = '$vIdCorporacao' ";
                $db = new Database();
                $db->query($query);
                $v_vetorObjetos = Helper::getResultSetToArrayDeObjetosWithExplicitNull($db->result);
                if (count($v_vetorObjetos) > 0)
                {
                    foreach ($v_vetorObjetos as $key => $obj)
                    {
                        $email = $obj["email"];
                        DatabaseSincronizador::sendInformacoesParaEmail($email, ASSUNTO_REMOCAO_CORPORACAO, GRUPO . " " . $nomeCorporacao . " " . CONTEUDO_REMOCAO_CORPORACAO);
                    }
                }
                $vObjIdCorporacao->delete($vIdCorporacao);
                echo "TRUE";
                return;
            }
        }
        echo "FALSE 3";
    }

    public static function isAdmnistradorDaCorporacao()
    {
        if (!DatabaseSincronizador::checkUsuarioCorporacao())
        {
            echo "FALSE 0";
            return;
        }
        //nome do usuario
        $vIdUsuario = Helper::POSTGET("usuario");
        $vIdCorporacao = Helper::POSTGET("p_corporacao");

        if (strlen($vIdUsuario) && strlen($vIdCorporacao) > 0)
        {
            if (!DatabaseSincronizador::isUsuarioAdministradorDaCorporacao($vIdUsuario, $vIdCorporacao))
            {
                echo "FALSE 1";
                return;
            }
            else
            {
                echo "TRUE";
                return;
            }
        }
        echo "FALSE 2";
    }

    private static function isUsuarioAdministradorDaCorporacao($pIdUsuario, $pIdCorporacao)
    {
        if (strlen($pIdUsuario) > 0 && strlen($pIdCorporacao))
        {
            $objBanco = new Database();

            //checa a existencia do funcionario
            $query = "SELECT uc.id as id
                 FROM usuario_corporacao uc
                 WHERE uc.usuario_id_INT = '" . $pIdUsuario . "' AND 
                     uc.corporacao_id_INT = '" . $pIdCorporacao . "' AND
                         uc.is_adm_BOOLEAN = '1'";

            $objBanco->query($query);

            //o usuario ja eh existente
            if ($objBanco->rows() == 1)
            {
                $vIdUC = $objBanco->getPrimeiraTuplaDoResultSet("id");
                if ($vIdUC != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        return false;
    }

    private static function procedureAddUsuarioTipo(
        $pIdUsuario, $pIdCorporacao, $pIdUsuarioAdm, $pIMEI, $idPrograma)
    {
        if (!strlen($pIdCorporacao))
        {
            $pIdCorporacao = Seguranca::getIdDaCorporacaoLogada();
        }

        if (strlen($pIdUsuario))
        {
            $vObjUsuarioTipo = new EXTDAO_Usuario_tipo();
            $vObjUsuarioTipo->setNome("USUÁRIO ADMINISTRADOR");
            $vObjUsuarioTipo->setNome_visivel("USUÁRIO ADMINISTRADOR");
            $vObjUsuarioTipo->setStatus_BOOLEAN("1");
            $vObjUsuarioTipo->formatarParaSQL();
            $vObjUsuarioTipo->insert(false, $pIdCorporacao);
            $vIdUsuarioTipo = $vObjUsuarioTipo->getIdDoUltimoRegistroInserido($pIdCorporacao);

            $vIdSistemaTabelaUsuarioTipo = DatabaseSincronizador::getIdDaSistemaTabela("usuario_tipo");
            $vIdSistemaTabelaUsuarioTipoCorporacao = DatabaseSincronizador::getIdDaSistemaTabela("usuario_tipo_corporacao");
            $vIdSistemaTabelaUsuarioTipoMenu = DatabaseSincronizador::getIdDaSistemaTabela("usuario_tipo_menu");

            EXTDAO_Sistema_registro_sincronizador::insertTupla(
                $vIdSistemaTabelaUsuarioTipo, $pIdUsuarioAdm, $pIdCorporacao, '1', DatabaseSincronizador::$INDEX_OPERACAO_INSERT, $pIMEI, $idPrograma, $vIdUsuarioTipo);

            $vObjUsuarioTipoCorporacao = new EXTDAO_Usuario_tipo_corporacao();
            $vObjUsuarioTipoCorporacao->setUsuario_id_INT($pIdUsuario);
            $vObjUsuarioTipoCorporacao->setUsuario_tipo_id_INT($vIdUsuarioTipo);
            $vObjUsuarioTipoCorporacao->formatarParaSQL();
            $vObjUsuarioTipoCorporacao->insert(false, $pIdCorporacao);
            $vIdUsuarioTipoCorporacao = $vObjUsuarioTipoCorporacao->getIdDoUltimoRegistroInserido($pIdCorporacao);

            EXTDAO_Sistema_registro_sincronizador::insertTupla(
                $vIdSistemaTabelaUsuarioTipoCorporacao, $pIdUsuarioAdm, $pIdCorporacao, '1', DatabaseSincronizador::$INDEX_OPERACAO_INSERT, $pIMEI, $idPrograma, $vIdUsuarioTipoCorporacao);

            $objMenu = Registry::get('Menu');
            $vVetorAreaMenu = $objMenu->getVetorDasOpcoesDoMenu();

            foreach ($vVetorAreaMenu as $vAreaMenu)
            {
                $vObjUsuarioTipoMenu = new EXTDAO_Usuario_tipo_menu();
                $vObjUsuarioTipoMenu->setUsuario_tipo_id_INT($vIdUsuarioTipo);
                $vObjUsuarioTipoMenu->setArea_menu($vAreaMenu);
                $vObjUsuarioTipoMenu->formatarParaSQL();
                $vObjUsuarioTipoMenu->insert(false, $pIdCorporacao);
                $vIdUsuarioTipoMenu = $vObjUsuarioTipoMenu->getIdDoUltimoRegistroInserido($pIdCorporacao);

                EXTDAO_Sistema_registro_sincronizador::insertTupla(
                    $vIdSistemaTabelaUsuarioTipoMenu, $pIdUsuarioAdm, $pIdCorporacao, '1', DatabaseSincronizador::$INDEX_OPERACAO_INSERT, $pIMEI, $idPrograma, $vIdUsuarioTipoMenu);
            }
            return true;
        }
        return false;
    }

    //cadastro do usuario administrativo de um nova corporacao
    public static function receiveCadastroUsuarioECorporacao()
    {
        //nome do usuario
        $nome = Helper::POSTGET("p_nome");
        $nomeSemAcento = Helper::POSTGET("p_nome_sem_acento");
        $email = Helper::POSTGET("p_email");
        //nome da corporacao ainda nao cadastrada
        $corporacao = Helper::POSTGET("p_corporacao");
        $corporacaoSemAcento = Helper::POSTGET("p_corporacao_sem_acento");
        $senha = Helper::POSTGET("p_senha");
        $pIMEI = Helper::POSTGET("imei");
        $idPrograma = Helper::POSTGET("id_programa");

        if (strlen($nome) > 0 &&
            strlen($email) > 0 &&
            strlen($corporacao) > 0 &&
            strlen($senha) > 0 &&
            strlen($corporacaoSemAcento) &&
            strlen($nomeSemAcento)
        )
        {
            $vIdUsuarioSistemaTabela = DatabaseSincronizador::getIdDaSistemaTabela("usuario");
            $vIdUsuarioCorporacaoSistemaTabela = DatabaseSincronizador::getIdDaSistemaTabela("usuario_corporacao");
            $vIdUsuario = DatabaseSincronizador::getIdDoUsuario($email);
            if ($vIdUsuario != null)
            {
                $vObjSeguranca = new Seguranca();
                if (!$vObjSeguranca->verificaUsuarioESenha($vIdUsuario, $senha))
                {
                    echo "ERROR: senha invalida do usuario ja existente";
                    return;
                }
            }

            $vIdCorporacao = null;
            if (DatabaseSincronizador::checkExistenciaCorporacao($corporacao))
            {
                echo "ERROR: corporacao duplicado";
                return;
            }
            //cadastrando usuario ainda nao existente 
            if ($vIdUsuario == null)
            {
                $vUsuario = new EXTDAO_Usuario();
                $vUsuario->setEmail($email);

                $vUsuario->setNome($nome);
                $vUsuario->setNome_normalizado($nomeSemAcento);
                $objCriptografia = new Crypt();
                $vUsuario->setSenha($objCriptografia->crypt($senha));
                $vUsuario->setStatus_BOOLEAN("1");
                $vUsuario->formatarParaSQL();
                $vUsuario->insert();
                $vIdUsuario = $vUsuario->getIdDoUltimoRegistroInserido();

                if (!strlen($vIdUsuario))
                {
                    echo "FALSE 0";
                    return;
                }
            }

            $vCorporacao = new EXTDAO_Corporacao();
            $vCorporacao->setNome($corporacao);
            $vCorporacao->setNome_normalizado($corporacaoSemAcento);
            $vCorporacao->formatarParaSQL();
            $vCorporacao->insert();
            $vIdCorporacao = $vCorporacao->getIdDoUltimoRegistroInserido();

            DatabaseSincronizador::procedureAddUsuarioTipo(
                $vIdUsuario, $vIdCorporacao, $vIdUsuario, $pIMEI, $idPrograma);

            EXTDAO_Sistema_registro_sincronizador::insertTupla(
                $vIdUsuarioSistemaTabela, $vIdUsuario, $vIdCorporacao, '1', DatabaseSincronizador::$INDEX_OPERACAO_INSERT, $pIMEI, $idPrograma, $vIdUsuario);

            if (strcmp($vIdCorporacao, "null") == 0)
            {
                $vIdCorporacao = null;
            }

            if ($vIdCorporacao == null)
            {
                echo "ERROR: insercao da corporacao";
                return;
            }
            DatabaseSincronizador::insertTuplasDosServicosDoUsuario(
                $vIdUsuario, $vIdCorporacao, $vIdUsuario, $pIMEI, $idPrograma);

            DatabaseSincronizador::insertTuplasDoTipoDeDocumento(
                $vIdUsuario, $vIdCorporacao, $vIdUsuario, $pIMEI, $idPrograma);
            //cadastra a tupla usuario corporacao
            $vUsuarioCorporacao = new EXTDAO_Usuario_corporacao();
            $vUsuarioCorporacao->setUsuario_id_INT($vIdUsuario);
            $vUsuarioCorporacao->setStatus_BOOLEAN("1");
            $vUsuarioCorporacao->setIs_adm_BOOLEAN("1");
            $vUsuarioCorporacao->formatarParaSQL();
            $vUsuarioCorporacao->setCorporacao_id_INT($vUsuarioCorporacao->formatarDados($vIdCorporacao));
            $vUsuarioCorporacao->insert(false, $vIdCorporacao);
            $vNewIdUsuarioCorporacao = $vUsuarioCorporacao->getIdDoUltimoRegistroInserido($vIdCorporacao);

            EXTDAO_Sistema_registro_sincronizador::insertTupla(
                $vIdUsuarioCorporacaoSistemaTabela, $vIdUsuario, $vIdCorporacao, '1', DatabaseSincronizador::$INDEX_OPERACAO_INSERT, $pIMEI, $idPrograma, $vNewIdUsuarioCorporacao);

            DatabaseSincronizador::sendInformacoesParaEmail($email, ASSUNTO_CADASTRO_USUARIO, CONTEUDO_ASSUNTO_CADASTRO_USUARIO);
            echo "TRUE";
            return;
        }
        else
        {
            echo "ERROR: parametro invalido";
            return;
        }
    }

    public static function isEmailDoUsuarioExistente()
    {
        if (!DatabaseSincronizador::checkUsuarioCorporacao())
        {
            echo "FALSE 0";
            return;
        }
        $email = Helper::POSTGET("p_email");
        $idCorporacao = Helper::POSTGET("id_corporacao");
        if (strlen($email) > 0)
        {
            $vIdUsuario = DatabaseSincronizador::getIdDoUsuario($email);
            //cadastrando usuario ainda nao existente 
            if ($vIdUsuario == null)
            {
                echo "FALSE 1";
            }
            else
            {
                $vIdUsuarioCorporacao = DatabaseSincronizador::getIdDoUsuarioCorporacao($vIdUsuario, $idCorporacao);

                if ($vIdUsuarioCorporacao != null)
                {
                    echo "TRUE 2: usuario existente e cadastrado na corporacao";
                }
                else
                {
                    echo "TRUE 1: usuario existente e nao cadastrado na corporacao";
                }
            }
        }
    }

    public static function existeRelacionamentoComAPessoa()
    {
        if (!DatabaseSincronizador::checkUsuarioCorporacao())
        {
            echo "FALSE 0";
            return;
        }
        $idPessoa = Helper::POSTGET("p_pessoa");

        if (strlen($idPessoa) > 0)
        {
            if (EXTDAO_Pessoa_usuario::existeRelacionamentoComAPessoa($idPessoa))
            {
                echo "TRUE";
                return;
            }
        }
        echo "FALSE";
    }

    public static function updateStatusServicoDoUsuario()
    {
        if (!DatabaseSincronizador::checkUsuarioCorporacao())
        {
            echo "FALSE 0";
            return;
        }
        $vIdUsuario = Helper::POSTGET("p_usuario");
        $vStrVetorIdUsuarioServicoAtivo = Helper::POSTGET("vetor_servico_ativo");
        $vStrVetorIdUsuarioServicoInativo = Helper::POSTGET("vetor_servico_inativo");
        $pIdCorporacao = Helper::POSTGET("id_corporacao");
        if (strlen($vIdUsuario) &&
            (strlen($vStrVetorIdUsuarioServicoAtivo) ||
                strlen($vStrVetorIdUsuarioServicoInativo))
        )
        {
            $vVetorIdUsuarioServicoAtivo = explode(",", $vStrVetorIdUsuarioServicoAtivo);
            $vVetorIdUsuarioServicoInativo = explode(",", $vStrVetorIdUsuarioServicoInativo);
            if (count($vVetorIdUsuarioServicoAtivo) > 0)
            {
                foreach ($vVetorIdUsuarioServicoAtivo as $key => $vIdUsuarioServico)
                {
                    $vObjUS = new EXTDAO_Usuario_servico();

                    $vObjUS->select($vIdUsuarioServico, $pIdCorporacao);
                    $vObjUS->setStatus_BOOLEAN("1");
                    $vObjUS->formatarParaSQL();
                    $vObjUS->update($vIdUsuarioServico, "vazio", 1, true, $pIdCorporacao);
                }
            }
            if (count($vVetorIdUsuarioServicoInativo) > 0)
            {
                foreach ($vVetorIdUsuarioServicoInativo as $key => $vIdUsuarioServico)
                {
                    $vObjUS = new EXTDAO_Usuario_servico();

                    $vObjUS->select($vIdUsuarioServico, $pIdCorporacao);
                    $vObjUS->setStatus_BOOLEAN("0");
                    $vObjUS->formatarParaSQL();
                    $vObjUS->update($vIdUsuarioServico, "vazio", 1, true, $pIdCorporacao);
                }
            }
        }
    }

    public static function insertTuplasDoTipoDeDocumento(
        $pIdUsuarioAdm, $pIdCorporacao, $pUsuario, $pIMEI, $idPrograma)
    {
        $vIdTipoDocumentoSistemaTabela = DatabaseSincronizador::getIdDaSistemaTabela("tipo_documento");
        if (strlen($pUsuario) &&
            strlen($vIdTipoDocumentoSistemaTabela) &&
            strlen($pIdCorporacao) &&
            strlen($pIdUsuarioAdm)
        )
        {
            $vetorNomeTipoDocumento = array("CNPJ", "CPF", "RG");
            $vetorNomeNormalizadoTipoDocumento = array("CNPJ", "CPF", "RG");

            if (count($vetorNomeTipoDocumento) > 0)
            {
                for ($index = 0; $index < count($vetorNomeTipoDocumento); $index++)
                {
                    $vNomeTipoDocumento = $vetorNomeTipoDocumento[$index];

                    if (!strlen($vNomeTipoDocumento))
                    {
                        continue;
                    }
                    else
                    {
                        $vObjTipoDocumento = new EXTDAO_Tipo_documento();
                        $vObjTipoDocumento->setNome($vNomeTipoDocumento);
                        $vObjTipoDocumento->formatarParaSQL();
                        $vObjTipoDocumento->insert(false, $pIdCorporacao);
                        $vIdTipoDocumento = $vObjTipoDocumento->getIdDoUltimoRegistroInserido($pIdCorporacao);

                        EXTDAO_Sistema_registro_sincronizador::insertTupla(
                            $vIdTipoDocumentoSistemaTabela, $pIdUsuarioAdm, $pIdCorporacao, '1', DatabaseSincronizador::$INDEX_OPERACAO_EDIT, $pIMEI, $idPrograma, $vIdTipoDocumento);
                    }
                }
            }
            else
            {
                return;
            }
        }
    }

    public static function insertTuplasDosServicosDoUsuario(
        $pIdUsuarioAdm, $pIdCorporacao, $pUsuario, $pIMEI, $idPrograma)
    {
        $vIdUsuarioServicoSistemaTabela = DatabaseSincronizador::getIdDaSistemaTabela("usuario_servico");
        if (strlen($pUsuario) &&
            strlen($vIdUsuarioServicoSistemaTabela) &&
            strlen($pIdCorporacao) &&
            strlen($pIdUsuarioAdm)
        )
        {
            $vetorIdServico = DatabaseSincronizador::getListIdTodosServicos();
            if (count($vetorIdServico) > 0)
            {
                foreach ($vetorIdServico as $key => $vIdServico)
                {
                    if (!strlen($vIdServico))
                    {
                        continue;
                    }
                    else
                    {
                        $vObjServico = new EXTDAO_Usuario_servico();
                        $vObjServico->setServico_id_INT($vIdServico);
                        $vObjServico->setUsuario_id_INT($pUsuario);
                        $vObjServico->setStatus_BOOLEAN("1");
                        $vObjServico->formatarParaSQL();
                        $vObjServico->insert(false, $pIdCorporacao);

                        $vIdUsuarioServico = $vObjServico->getIdDoUltimoRegistroInserido($pIdCorporacao);

                        EXTDAO_Sistema_registro_sincronizador::insertTupla(
                            $vIdUsuarioServicoSistemaTabela, $pIdUsuarioAdm, $pIdCorporacao, '1', DatabaseSincronizador::$INDEX_OPERACAO_EDIT, $pIMEI, $idPrograma, $vIdUsuarioServico);
                    }
                }
            }
            else
            {
                return;
            }
        }
    }

    public static function receiveRemocaoUsuario()
    {
        if (!DatabaseSincronizador::checkUsuarioCorporacao())
        {
            echo "FALSE 0";
            return;
        }
        $idSistemaTabela = Helper::POSTGET("sistema_tabela");
        $idUsuarioCorporacao = Helper::POSTGET("usuario_corporacao");
        $idUsuario = Helper::POSTGET("usuario");
        $idCorporacao = Helper::POSTGET("id_corporacao");
        $pIMEI = Helper::POSTGET("imei");
        $idPrograma = Helper::POSTGET("id_programa");

        if (strlen($idUsuarioCorporacao) &&
            strlen($idSistemaTabela)
        )
        {
            $objUsuarioCorporacao = new EXTDAO_Usuario_corporacao();
            $objUsuarioCorporacao->delete($idUsuarioCorporacao);

            EXTDAO_Sistema_registro_sincronizador::insertTupla(
                $idSistemaTabela, $idUsuario, $idCorporacao, '1', DatabaseSincronizador::$INDEX_OPERACAO_REMOVE, $pIMEI, $idPrograma, $idUsuarioCorporacao);

            echo "TRUE";
        }
        echo "FALSE 1";
    }

    public function receiveAlterarSenha()
    {
        if (!DatabaseSincronizador::checkUsuarioCorporacao())
        {
            echo "FALSE 0";
            return;
        }
        $senha = Helper::POSTGET("senha1");
        $confirmacao = Helper::POSTGET("txtSenhaNovaConfirm");
        $idUsuario = Helper::POSTGET("usuario");
        $pIdCorporacao = Helper::POSTGET("id_corporacao");
        if ($senha == $confirmacao && strlen($idUsuario))
        {
            $objUsuario = new EXTDAO_Usuario();
            $objUsuario->select($idUsuario);
            $email = $objUsuario->getEmail();
            $objCriptografia = new Crypt();
            $objUsuario->setSenha($objCriptografia->crypt($senha));
            $objUsuario->formatarParaSQL();
            $objUsuario->update($idUsuario, "vazio", 1, true, $pIdCorporacao);
            DatabaseSincronizador::sendInformacoesParaEmail($email, ASSUNTO_SENHA_ALTERADA, CONTEUDO_ASSUNTO_SENHA_ALTERADA);
            echo "TRUE: UsuarioModel: " . $idUsuario . "</br>";
        }
        else
        {
            echo "FALSE 1";
        }
    }

    public static function receiveEdicaoUsuario()
    {
        if (!DatabaseSincronizador::checkUsuarioCorporacao())
        {
            echo "FALSE 0";
            return;
        }
        //nome do usuario
        $pNome = Helper::POSTGET("p_nome");
        $pNomeSemAcento = Helper::POSTGET("p_nome_sem_acento");
        $pIdUsuario = Helper::POSTGET("p_usuario");
        $pUsuarioAdm = Helper::POSTGET("usuario");
        //id da corporacao ainda nao cadastrada
        $pIdCorporacao = Helper::POSTGET("id_corporacao");

        $pIMEI = Helper::POSTGET("imei");
        $idPrograma = Helper::POSTGET("id_programa");

        $pIsAdm = Helper::POSTGET("is_adm");
        $pStatusBoolean = Helper::POSTGET("status_boolean");
        $pUsuarioCategoriaPermissao = Helper::POSTGET("usuario_categoria_permissao");
        $pIdUsuarioCorporacao = Helper::POSTGET("usuario_corporacao");
        $pCategoriaPermissao = Helper::POSTGET("categoria_permissao");

        $vIdSistemaTabelaUsuario = DatabaseSincronizador::getIdDaSistemaTabela("usuario");
        $vIdSistemaTabelaUsuarioCorporacao = DatabaseSincronizador::getIdDaSistemaTabela("usuario_corporacao");
        $vIdSistemaTabelaUsuarioCategoriaPermissao = DatabaseSincronizador::getIdDaSistemaTabela("usuario_categoria_permissao");

        if (strlen($pIdCorporacao) > 0 &&
            (strlen($pCategoriaPermissao) ||
                strlen($pIsAdm)) &&
            strlen($pIdUsuario) &&
            strlen($pIdUsuarioCorporacao) &&
            strlen($vIdSistemaTabelaUsuarioCorporacao) &&
            strlen($vIdSistemaTabelaUsuarioCategoriaPermissao) &&
            strlen($vIdSistemaTabelaUsuario)
        )
        {
//            Somente o proprio usuario pode alterar o proprio nome
            $email = null;
            if (strlen($pNome) && strlen($pNomeSemAcento))
            {
                $vUsuario = new EXTDAO_Usuario();
                $vUsuario->select($pIdUsuario);
                $email = $vUsuario->getEmail();
                $vUsuario->setNome($pNome);
                $vUsuario->setNome_normalizado($pNomeSemAcento);
                $vUsuario->formatarParaSQL();
                $vUsuario->update($pIdUsuario, "vazio", 1, false, $pIdCorporacao);

                //cadastrando usuario ainda nao existente
                EXTDAO_Sistema_registro_sincronizador::insertTupla(
                    $vIdSistemaTabelaUsuario, $pUsuarioAdm, $pIdCorporacao, '1', DatabaseSincronizador::$INDEX_OPERACAO_EDIT, $pIMEI, $idPrograma, $pIdUsuario);
            }
            //cadastra a tupla usuario corporacao
            $vUsuarioCorporacao = new EXTDAO_Usuario_corporacao();
            $vUsuarioCorporacao->select($pIdUsuarioCorporacao, $pIdCorporacao);
            $vUsuarioCorporacao->setStatus_BOOLEAN($pStatusBoolean);
            if ($pIsAdm == "1")
            {
                $vUsuarioCorporacao->setIs_adm_BOOLEAN("1");
                $pCategoriaPermissao = null;
            }
            else
            {
                $vUsuarioCorporacao->setIs_adm_BOOLEAN("0");
            }
            $vUsuarioCorporacao->formatarParaSQL();
            $vUsuarioCorporacao->update($pIdUsuarioCorporacao, "vazio", 1, false, $pIdCorporacao);

            $vObjCorporacao = new EXTDAO_Corporacao();
            $vObjCorporacao->select($pIdCorporacao);
            $vNomeCorporacao = $vObjCorporacao->getNome();

            EXTDAO_Sistema_registro_sincronizador::insertTupla(
                $vIdSistemaTabelaUsuarioCorporacao, $pUsuarioAdm, $pIdCorporacao, '1', DatabaseSincronizador::$INDEX_OPERACAO_EDIT, $pIMEI, $idPrograma, $pIdUsuarioCorporacao);

//            Se o usuario possuia categoria de permissao e nao eh administrador, 
//            entao a categoria de permissao antiga deve ser atualizada para a nova
            if (strlen($pUsuarioCategoriaPermissao) &&
                $pIsAdm == "0" &&
                strlen($pCategoriaPermissao)
            )
            {
                $vObjUsuarioCategoriaPermissao = new EXTDAO_Usuario_categoria_permissao();
                $vObjUsuarioCategoriaPermissao->select($pUsuarioCategoriaPermissao, $pIdCorporacao);
                $vObjUsuarioCategoriaPermissao->setCategoria_permissao_id_INT($pCategoriaPermissao);
                $vObjUsuarioCategoriaPermissao->formatarParaSQL();
                $vObjUsuarioCategoriaPermissao->update($pUsuarioCategoriaPermissao, "vazio", 1, false, $pIdCorporacao);

                EXTDAO_Sistema_registro_sincronizador::insertTupla(
                    $vIdSistemaTabelaUsuarioCategoriaPermissao, $pUsuarioAdm, $pIdCorporacao, '1', DatabaseSincronizador::$INDEX_OPERACAO_EDIT, $pIMEI, $idPrograma, $pUsuarioCategoriaPermissao);

//                Se o usuario nao possuia categoria de permissao, e agora nao eh administrado 
//                entao deve ser adicionado a nova tupla de categoria de permissao do usuario
            }
            elseif (strlen($pCategoriaPermissao) &&
                $pIsAdm == "0" &&
                !strlen($pUsuarioCategoriaPermissao)
            )
            {
                $vObjUsuarioCategoriaPermissao = new EXTDAO_Usuario_categoria_permissao();
//                $vObjUsuarioCategoriaPermissao->select($vUsuarioCategoriaPermissao);
                $vObjUsuarioCategoriaPermissao->setCategoria_permissao_id_INT($pCategoriaPermissao);
                $vObjUsuarioCategoriaPermissao->setUsuario_id_INT($pIdUsuario);
                $vObjUsuarioCategoriaPermissao->formatarParaSQL();

                $vObjUsuarioCategoriaPermissao->insert(false, $pIdCorporacao);

                $pUsuarioCategoriaPermissao = $vObjUsuarioCategoriaPermissao->getIdDoUltimoRegistroInserido($pIdCorporacao);

                EXTDAO_Sistema_registro_sincronizador::insertTupla(
                    $vIdSistemaTabelaUsuarioCategoriaPermissao, $pUsuarioAdm, $pIdCorporacao, '1', DatabaseSincronizador::$INDEX_OPERACAO_INSERT, $pIMEI, $idPrograma, $pUsuarioCategoriaPermissao);
            }
            elseif (strlen($pUsuarioCategoriaPermissao) &&
                $pIsAdm == "1"
            )
            {
//                Se o usuario possuia categoria de permissao e agora ele eh um administrador, 
//                entao sua categoria de permissao de ser removida
                $vObjUsuarioCategoriaPermissao = new EXTDAO_Usuario_categoria_permissao();
                $vObjUsuarioCategoriaPermissao->delete($pUsuarioCategoriaPermissao);

                EXTDAO_Sistema_registro_sincronizador::insertTupla(
                    $vIdSistemaTabelaUsuarioCategoriaPermissao, $pUsuarioAdm, $pIdCorporacao, '1', DatabaseSincronizador::$INDEX_OPERACAO_REMOVE, $pIMEI, $idPrograma, $pUsuarioCategoriaPermissao);
            }
            DatabaseSincronizador::sendInformacoesParaEmail($email, ASSUNTO_CATEGORIA_PERMISSAO_ALTERADA, CONTEUDO_CATEGORIA_PERMISSAO_ALTERADA . $vNomeCorporacao);
            if (strlen($pUsuarioCategoriaPermissao) == 0)
            {
                $pUsuarioCategoriaPermissao = "null";
            }
            if (strlen($pIdUsuarioCorporacao) == 0)
            {
                $pIdUsuarioCorporacao = "null";
            }
            echo "TRUE:" . $pIdUsuario . ":" . $pUsuarioCategoriaPermissao . ":" . $pIdUsuarioCorporacao;

            return;
        }
        else
        {
            echo "ERROR: parametro invalido";
            return;
        }
    }

    public static function sendSenhaToEmail()
    {
        $emailCadastrado = Helper::POSTGET("email");

        $vObjDatabase = new Database();
        $vObjDatabase->query("SELECT id, nome, email, senha
                        FROM usuario
                        WHERE email='{$emailCadastrado}'");

        if ($vObjDatabase->rows() == 1)
        {
            $idUsuario = $vObjDatabase->getPrimeiraTuplaDoResultSet(0);
            $emailDestino = $vObjDatabase->getPrimeiraTuplaDoResultSet(2);
            $senha = $vObjDatabase->getPrimeiraTuplaDoResultSet(3);

            $objCrypt = new Crypt();
            $senhaDescriptografada = $objCrypt->decrypt($senha);

            $mensagem = "--- Lembrete de senha de acesso ao " . TITULO_PAGINAS . " ---\n
                                                Endereço para Acesso: " . ENDERECO_DE_ACESSO . "
                                                Usuário: {$emailDestino}
                                                Senha  : {$senhaDescriptografada}
                                                ";

            $mensagemUsuarioCorporacao = DatabaseSincronizador::getMensagemUsuarioCorporacao($idUsuario);

            $objEmail = new Email_Sender(EMAIL_PADRAO_REMETENTE_MENSAGENS, $emailDestino);
            $objEmail->setAssunto(ASSUNTO_DO_EMAIL_DE_REENVIO_SENHA);
            $objEmail->setConteudo($mensagem . $mensagemUsuarioCorporacao, false);
            if ($objEmail->enviarEmail())
            {
                echo "TRUE";
            }
            else
            {
                echo "FALSE: falha ao enviar o email";
            }
        }
        elseif ($vObjDatabase->rows() == 0)
        {
            echo "ERROR: email nao cadastrado";
        }
    }

    public static function getMensagemUsuarioCorporacao($idUsuario)
    {
        $mensagemUsuarioCorporacao = "";
        $query = "SELECT DISTINCT uc.id uc_id, uc.is_adm_BOOLEAN is_adm, c.nome nome_corporacao, uc.status_BOOLEAN status
                FROM usuario_corporacao uc, corporacao c 
                WHERE uc.usuario_id_INT = '$idUsuario' AND
                        c.id = uc.corporacao_id_INT ";
        $vObjDatabase = new Database();
        $vObjDatabase->query($query);
        $v_vetorObjetos = Helper::getResultSetToArrayDeObjetosWithExplicitNull($vObjDatabase->result);
        $vObjDatabaseAuxiliar = new Database();
        if (count($v_vetorObjetos) > 0)
        {
            foreach ($v_vetorObjetos as $key => $obj)
            {
                $idAdm = $obj["is_adm"];
                $nomeCorporacao = $obj["nome_corporacao"];
                $mensagemUsuarioCorporacao .= "
Grupo: " . ucwords($nomeCorporacao);
                if ($idAdm == "1")
                {
                    $mensagemUsuarioCorporacao .= "
- Categoria de Permissão: 'Administrador'";
                }
                else
                {
                    $query = "SELECT DISTINCT c.id, c.nome nome_categoria
                        FROM categoria_permissao c, usuario_categoria_permissao uc
                        WHERE c.id = uc.categoria_permissao_id_INT AND 
                        uc.usuario_id_INT = $idUsuario ";
                    $vObjDatabaseAuxiliar->query($query);
                    $v_vetorObjetosCategoriaPermissao = Helper::getResultSetToArrayDeObjetosWithExplicitNull($vObjDatabaseAuxiliar->result);
                    if ($v_vetorObjetosCategoriaPermissao > 0)
                    {
                        foreach ($v_vetorObjetos as $key => $objCategoriaPermissao)
                        {
                            $nomeCategoriaPermissao = $objCategoriaPermissao["nome_categoria"];
                            $mensagemUsuarioCorporacao .= "
 - '" . ucwords($nomeCategoriaPermissao) . "'";
                        }
                    }
                    else
                    {
                        $mensagemUsuarioCorporacao .= " - " . ucwords(CATEGORIA_PERMISSAO) . ": O seu usuário não possui categoria de permissão nesta corporação, fale com o administrador para atualizar a sua 'categoria permissão' na tela 'Editar Usuário'.";
                    }
                }
                $vStrStatus = "Inativo";
                if ($obj["status"] == "1")
                {
                    $vStrStatus = "Ativo";
                }
                $mensagemUsuarioCorporacao .= "
Status:  $vStrStatus
============================================================
                    ";
            }
        }
        else
        {
            $mensagemUsuarioCorporacao .= " 
--- O seu usuário não está cadastrado em nenhum grupo, crie um novo grupo para seu usuário na tela inicial, ou comunique ao usuário de um grupo para cadastrar-lo.";
        }
        return $mensagemUsuarioCorporacao;
    }

    private static function sendInformacoesParaEmail(
        $emailCadastrado,
        $assunto,
        $conteudoAssunto)
    {
        $vObjDatabase = new Database();
        $vObjDatabase->query("SELECT id, nome, email, senha
                        FROM usuario
                        WHERE email='{$emailCadastrado}'");

        if ($vObjDatabase->rows() == 1)
        {
            $idUsuario = $vObjDatabase->getPrimeiraTuplaDoResultSet(0);
            $nome = ucwords($vObjDatabase->getPrimeiraTuplaDoResultSet(1));
            $emailDestino = $vObjDatabase->getPrimeiraTuplaDoResultSet(2);
            $senha = $vObjDatabase->getPrimeiraTuplaDoResultSet(3);

            $objCrypt = new Crypt();
            $senhaDescriptografada = $objCrypt->decrypt($senha);

            $mensagem = " Sr. " . ucwords($nome) . "
                        $conteudoAssunto
                        --- Lembrete de senha de acesso ao " . TITULO_PAGINAS . " ---\n
                                                Endereço para Acesso: " . ENDERECO_DE_ACESSO . "
                                                Usuário: {$emailDestino}
                                                Senha  : {$senhaDescriptografada}
                                                ";

            $mensagemUsuarioCorporacao = DatabaseSincronizador::getMensagemUsuarioCorporacao($idUsuario);

            $objEmail = new Email_Sender(EMAIL_PADRAO_REMETENTE_MENSAGENS, $emailDestino);
            $objEmail->setAssunto($assunto);
            $objEmail->setConteudo($mensagem . $mensagemUsuarioCorporacao, false);
            if ($objEmail->enviarEmail())
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    public static function sendErroToEmail()
    {
        $emailCadastrado = Helper::POSTGET("email");
        $nomeCadastrado = Helper::POSTGET("nome");
        $mensagem = Helper::POSTGET("mensagem");
        $tag = Helper::POSTGET("tag");
        if (strlen($emailCadastrado))
        {
            $mensagem = "--- Sr. $nomeCadastrado seu erro já está sendo solucionado por nossa equipe ---
                            ERROR $tag: $mensagem 

                            FAVOR NÃO RESPONDER A ESTA MENSAGEM";

            $objEmail = new Email_Sender($emailCadastrado, EMAIL_PADRAO_ERROR_MENSAGENS);
            $objEmail->setAssunto("EQUIPE OMEGA: ERROR SOFTWARE ANDROID");
            $objEmail->setConteudo($mensagem, false);
            $objEmail->enviarEmail();

            $objEmail = new Email_Sender(EMAIL_PADRAO_ERROR_MENSAGENS, $emailCadastrado);
            $objEmail->setAssunto("EQUIPE OMEGA: ERROR SOFTWARE ANDROID");
            $objEmail->setConteudo($mensagem, false);
            $objEmail->enviarEmail();
        }
        echo "TRUE";
    }

    public static function sendMelhoriaToEmail()
    {
        $emailCadastrado = Helper::POSTGET("email");
        $nomeCadastrado = Helper::POSTGET("nome");
        $mensagem = Helper::POSTGET("mensagem");
        $tag = Helper::POSTGET("tag");
        $mensagem = "--- Sr. $nomeCadastrado a melhoria sugerida foi tomada como nota ---
                        SUGESTÃO $tag: $mensagem 
            
                        FAVOR NÃO RESPONDER A ESTA MENSAGEM";

        $objEmail = new Email_Sender($emailCadastrado, EMAIL_PADRAO_MELHORIA_MENSAGENS);
        $objEmail->setAssunto("EQUIPE OMEGA: ERROR SOFTWARE ANDROID");
        $objEmail->setConteudo($mensagem, false);
        $objEmail->enviarEmail();

        $objEmail = new Email_Sender(EMAIL_PADRAO_MELHORIA_MENSAGENS, $emailCadastrado);
        $objEmail->setAssunto("EQUIPE OMEGA: ERROR SOFTWARE ANDROID");
        $objEmail->setConteudo($mensagem, false);
        $objEmail->enviarEmail();
        echo "TRUE";
    }

    public static function receiveCadastroUsuario()
    {
        if (!DatabaseSincronizador::checkUsuarioCorporacao())
        {
            echo "FALSE 0";
            return;
        }
        //nome do usuario
        $pNome = Helper::POSTGET("p_nome");
        $pNomeSemAcento = Helper::POSTGET("p_nome_sem_acento");
        $pEmail = Helper::POSTGET("p_email");
        $pUsuarioAdm = Helper::POSTGET("usuario");
        $pUsuarioTipo = Helper::POSTGET("usuario_tipo");
        $pIMEI = Helper::POSTGET("imei");
        $idPrograma = Helper::POSTGET("id_programa");
        //id da corporacao ainda nao cadastrada
        $pIdCorporacao = Helper::POSTGET("id_corporacao");
        $pSenha = Helper::POSTGET("p_senha");
        $pIdPessoa = Helper::POSTGET("p_pessoa");

        $pStatus = Helper::POSTGET("p_status");
        $pIsAdm = Helper::POSTGET("is_adm");
        $pCategoriaPermissao = Helper::POSTGET("categoria_permissao");

        $vIdSistemaTabelaUsuario = DatabaseSincronizador::getIdDaSistemaTabela("usuario");
        $vIdSistemaTabelaUsuarioCorporacao = DatabaseSincronizador::getIdDaSistemaTabela("usuario_corporacao");
        $vIdSistemaTabelaUsuarioCategoriaPermissao = DatabaseSincronizador::getIdDaSistemaTabela("usuario_categoria_permissao");
        $vIdSistemaTabelaUsuarioServico = DatabaseSincronizador::getIdDaSistemaTabela("usuario_servico");

        if (strlen($pNome) > 0 &&
            strlen($pEmail) > 0 &&
            strlen($pIdCorporacao) > 0 &&
            strlen($pSenha) > 0 &&
            (strlen($pCategoriaPermissao) ||
                strlen($pIsAdm)) &&
            strlen($vIdSistemaTabelaUsuarioCategoriaPermissao) &&
            strlen($vIdSistemaTabelaUsuarioServico) &&
            strlen($vIdSistemaTabelaUsuarioCorporacao) &&
            strlen($vIdSistemaTabelaUsuario) > 0 &&
            strlen($pStatus)
        )
        {
            $vNewIdUsuario = DatabaseSincronizador::getIdDoUsuario($pEmail);
            $vNewIdUsuarioCategoriaPermissao = "";

            if (strlen($pIdPessoa) > 0)
            {
                if (EXTDAO_Pessoa_usuario::existeRelacionamentoComAPessoa($pIdPessoa))
                {
                    echo "ERROR 2: já existe usuário associado.";
                    return;
                }
            }

            //cadastrando usuario ainda nao existente
            if ($vNewIdUsuario == null)
            {
                $vUsuario = new EXTDAO_Usuario();
                $vUsuario->setEmail($pEmail);
                $vUsuario->setNome($pNome);
                $vUsuario->setNome_normalizado($pNomeSemAcento);

                $objCriptografia = new Crypt();
                $vUsuario->setSenha($objCriptografia->crypt($pSenha));
                $vUsuario->setStatus_BOOLEAN($pStatus);
                $vUsuario->formatarParaSQL();
                $vUsuario->insert(false);

                $vNewIdUsuario = $vUsuario->getIdDoUltimoRegistroInserido();
                if (!strlen($vNewIdUsuario))
                {
                    echo "ERROR";
                    return;
                }
                else
                {
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                        $vIdSistemaTabelaUsuario, $pUsuarioAdm, $pIdCorporacao, '1', DatabaseSincronizador::$INDEX_OPERACAO_INSERT, $pIMEI, $idPrograma, $vNewIdUsuario);
                }
            }
            $vObjUsuarioTipoCorporacao = new EXTDAO_Usuario_tipo_corporacao();

            $vObjUsuarioTipoCorporacao->setUsuario_id_INT($vNewIdUsuario);
            $vObjUsuarioTipoCorporacao->setUsuario_tipo_id_INT($pUsuarioTipo);
            $vObjUsuarioTipoCorporacao->formatarParaSQL();
            $vObjUsuarioTipoCorporacao->insert(false, $pIdCorporacao);
            $vIdUsuarioTipoCorporacao = $vObjUsuarioTipoCorporacao->getIdDoUltimoRegistroInserido($pIdCorporacao);

            $vIdSistemaTabelaUsuarioTipoCorporacao = DatabaseSincronizador::getIdDaSistemaTabela("usuario_tipo_corporacao");

            EXTDAO_Sistema_registro_sincronizador::insertTupla(
                $vIdSistemaTabelaUsuarioTipoCorporacao, $pUsuarioAdm, $pIdCorporacao, '1', DatabaseSincronizador::$INDEX_OPERACAO_INSERT, $pIMEI, $idPrograma, $vIdUsuarioTipoCorporacao);

            DatabaseSincronizador::insertTuplasDosServicosDoUsuario(
                $pUsuarioAdm, $pIdCorporacao, $vNewIdUsuario, $pIMEI, $idPrograma);

            //cadastra a tupla usuario corporacao
            $vUsuarioCorporacao = new EXTDAO_Usuario_corporacao();
            $vUsuarioCorporacao->setUsuario_id_INT($vNewIdUsuario);

            $vUsuarioCorporacao->setStatus_BOOLEAN("1");
            if ($pIsAdm == "1")
            {
                $vUsuarioCorporacao->setIs_adm_BOOLEAN("1");
            }
            else
            {
                if (strlen($pCategoriaPermissao))
                {
                    $vObjUsuarioCategoriaPermissao = new EXTDAO_Usuario_categoria_permissao();
                    $vObjUsuarioCategoriaPermissao->setUsuario_id_INT($vNewIdUsuario);
                    $vObjUsuarioCategoriaPermissao->setCategoria_permissao_id_INT($pCategoriaPermissao);
                    $vObjUsuarioCategoriaPermissao->formatarParaSQL();
                    $vObjUsuarioCategoriaPermissao->insert(false, $pIdCorporacao);
                    $vNewIdUsuarioCategoriaPermissao = $vObjUsuarioCategoriaPermissao->getIdDoUltimoRegistroInserido($pIdCorporacao);

                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                        $vIdSistemaTabelaUsuarioCategoriaPermissao, $pUsuarioAdm, $pIdCorporacao, '1', DatabaseSincronizador::$INDEX_OPERACAO_INSERT, $pIMEI, $idPrograma, $vNewIdUsuarioCategoriaPermissao);
                }
                $vUsuarioCorporacao->setIs_adm_BOOLEAN("0");
            }

            $vUsuarioCorporacao->formatarParaSQL();
            $vUsuarioCorporacao->insert(false, $pIdCorporacao);

            $vNewIdUsuarioCorporacao = $vUsuarioCorporacao->getIdDoUltimoRegistroInserido($pIdCorporacao);
            EXTDAO_Sistema_registro_sincronizador::insertTupla(
                $vIdSistemaTabelaUsuarioCorporacao, $pUsuarioAdm, $pIdCorporacao, '1', DatabaseSincronizador::$INDEX_OPERACAO_INSERT, $pIMEI, $idPrograma, $vNewIdUsuarioCorporacao);

            DatabaseSincronizador::sendInformacoesParaEmail(
                $pEmail, ASSUNTO_CADASTRO_USUARIO, CONTEUDO_ASSUNTO_CADASTRO_USUARIO);

            if (strlen($vNewIdUsuarioCategoriaPermissao) == 0)
            {
                $vNewIdUsuarioCategoriaPermissao = "null";
            }
            if (strlen($vNewIdUsuarioCorporacao) == 0)
            {
                $vNewIdUsuarioCorporacao = "null";
            }
            echo "TRUE:" . $vNewIdUsuario . ":" . $vNewIdUsuarioCategoriaPermissao . ":" . $vNewIdUsuarioCorporacao;
            return;
        }
        else
        {
            echo "ERROR: parametro invalido";
            return;
        }
    }

    private static function getListIdTodosServicos()
    {
        $objBanco = new Database();
        $query = "SELECT s.id id
        FROM servico s";

        $objBanco->query($query);
        $v_vetorObjetos = Helper::getResultSetToArrayDeUmCampo($objBanco->result);
        return $v_vetorObjetos;
    }

    public static function receiveInformacaoUsuairo()
    {
        $email = Helper::POSTGET("email");
        $corporacao = Helper::POSTGET("corporacao");
        $senha = Helper::POSTGET("senha");

        if (strlen($email) > 0 &&
            strlen($corporacao) > 0 &&
            strlen($senha)
        )
        {
            $objSeguranca = new Seguranca();

            $msg = $objSeguranca->verificaSenha(
                array("email" => $email, "corporacao" => $corporacao),
                $senha,
                null,
                false);
            if ($msg != null && !$msg->ok())
            {
                echo "FALSE 0";
                return;
            }
            $objBanco = new Database();
            $query = "SELECT u.id id, 
                u.nome nome, 
                u.nome_normalizado nome_normalizado, 
                uc.corporacao_id_INT corporacao,  
                u.status_BOOLEAN status,
                uc.id id_corporacao,
                uc.status_BOOLEAN status_corporacao,
                uc.is_adm_BOOLEAN is_adm,
                ucp.categoria_permissao_id_INT categoria_permissao
             FROM usuario u 
                JOIN usuario_corporacao uc ON u.id = uc.usuario_id_INT
                JOIN corporacao c ON uc.corporacao_id_INT = c.id 
                LEFT JOIN usuario_categoria_permissao ucp ON u.id = ucp.usuario_id_INT AND ucp.corporacao_id_INT = c.id
             WHERE u.email = '$email' AND
                c.nome='$corporacao' AND 
                uc.status_BOOLEAN='1' AND
                u.status_BOOLEAN='1'";

            $objBanco->query($query);
            $v_vetorObjetos = Helper::getResultSetToArrayDeObjetos($objBanco->result);

            if ($objBanco->rows() == null)
            {
                echo "ERROR: 1: Count: " . $objBanco->rows();
            }
            else
            {
                if ($objBanco->rows() == 1)
                {
                    $obj = $v_vetorObjetos[0];

                    echo $obj->id . DatabaseSincronizador::$DELIMITADOR_COLUNA;
                    echo $obj->nome . DatabaseSincronizador::$DELIMITADOR_COLUNA;
                    echo $obj->nome_normalizado . DatabaseSincronizador::$DELIMITADOR_COLUNA;
                    echo $obj->corporacao . DatabaseSincronizador::$DELIMITADOR_COLUNA;
                    echo $obj->status . DatabaseSincronizador::$DELIMITADOR_COLUNA;
                    echo $obj->id_corporacao . DatabaseSincronizador::$DELIMITADOR_COLUNA;
                    echo $obj->status_corporacao . DatabaseSincronizador::$DELIMITADOR_COLUNA;
                    echo $obj->is_adm . DatabaseSincronizador::$DELIMITADOR_COLUNA;
                    echo strlen($obj->categoria_permissao) > 0 ? $obj->categoria_permissao : "NULL";
                    // se o usuario não for administrador
                    if ($obj->is_adm != "1" && strlen($obj->categoria_permissao))
                    {
                        $vetorObjPermissao = DatabaseSincronizador::getListaPermissaoDaCategoriaDePermissao($obj->categoria_permissao);
                        if (count($vetorObjPermissao))
                        {
                            echo DatabaseSincronizador::$DELIMITADOR_LINHA;
                            echo count($vetorObjPermissao);
                            echo DatabaseSincronizador::$DELIMITADOR_LINHA;

                            for ($i = 0; $i < count($vetorObjPermissao); $i++)
                            {
                                if ($i > 0)
                                {
                                    echo DatabaseSincronizador::$DELIMITADOR_COLUNA;
                                }
                                echo $vetorObjPermissao[$i];
                            }
                        }
                    }
                }
                else
                {
                    echo "ERROR: 3: Number of result: " . $objBanco->rows() . "</br>";
                }
            }
        }
        else
        {
            echo "ERROR: 4";
        }
    }

    private static function getListaPermissaoDaCategoriaDePermissao($categoriaPermissao)
    {
        $db = new Database();
        $q = "SELECT p.id
FROM permissao p JOIN permissao_categoria_permissao pcp ON p.id = pcp.permissao_id_INT
WHERE pcp.categoria_permissao_id_INT = $categoriaPermissao";
        $db->query($q);
        return Helper::getResultSetToArrayDeUmCampo($db->result);
    }

    public static function receiveTabelaAndroidDatabase()
    {
        $query = Helper::POSTGET("query");
        if (strlen($query) > 0)
        {
            $objDatabase = new Database();
            $vStrResult = $objDatabase->query($query);
            if (strlen($vStrResult) == 0)
            {
                echo $query;
            }
            else
            {
                echo "TRUE";
            }
        }
        else
        {
            echo $query;
        }
    }

    public static function sendUltimoIdSincronizadorAndroid()
    {
        if (!DatabaseSincronizador::checkUsuarioCorporacao())
        {
            echo "FALSE 0";
            return;
        }
        $vUltimoIdSincronizadorAndroid = Helper::POSTGET("ultimo_id_sincronizador_android");
        if (strlen($vUltimoIdSincronizadorAndroid))
        {
            $vIdUsuario = Helper::POSTGET("usuario");
            $vIdCorporacao = Helper::POSTGET("id_corporacao");
            $objDatabase = new Database();
            $vIsFirstInsert = true;
            $vLastIdSincronizador = -1;
            $vQueryLastIdSincronizado = "SELECT MAX(id)
                                         FROM sistema_registro_sincronizador s
                                         WHERE id > $vUltimoIdSincronizadorAndroid";
            $objDatabase->query($vQueryLastIdSincronizado);
            $vVetorSincronizador = Helper::getResultSetToArrayDeUmCampo($objDatabase->result);
            if (count($vVetorSincronizador) > 0)
            {
                $vLastIdSincronizador = $vVetorSincronizador[0];
                if ($vLastIdSincronizador > $vUltimoIdSincronizadorAndroid)
                {
                    if (!$vIsFirstInsert)
                    {
                        echo DatabaseSincronizador::$DELIMITADOR_LINHA;
                    }
                    echo "OP%%LAST_ID_SINCRONIZADOR" . DatabaseSincronizador::$DELIMITADOR_LINHA . $vLastIdSincronizador;
                    $vIsFirstInsert = false;
                }
                else
                {
                    if ($vLastIdSincronizador == $vUltimoIdSincronizadorAndroid)
                    {
                        return;
                    }
                    else
                    {
                        if ($vLastIdSincronizador < $vUltimoIdSincronizadorAndroid)
                        {
                            return;
                        }
                    }
                }
            }
            else
            {
                return;
            }
        }
    }

    public static function sendListaInformacaoSincronizador()
    {
        if (!DatabaseSincronizador::checkUsuarioCorporacao())
        {
            echo "FALSE 0";
            return;
        }
        $pUltimoIdSincronizadorAndroid = Helper::POSTGET("ultimo_id_sincronizador_android");
        if (strlen($pUltimoIdSincronizadorAndroid))
        {
            $pIdUsuario = Helper::POSTGET("usuario");
            $pIdCorporacao = Helper::POSTGET("id_corporacao");
            $pIMEI = Helper::POSTGET("imei");
            $idPrograma = Helper::POSTGET("id_programa");

            $objDatabase = new Database();
            $vIsFirstInsert = true;
            $vLastIdSincronizador = -1;
            $vQueryLastIdSincronizado = "SELECT MAX(id)
                                         FROM sistema_registro_sincronizador s
                                         WHERE id > $pUltimoIdSincronizadorAndroid";
            $objDatabase->query($vQueryLastIdSincronizado);
            $vVetorSincronizador = Helper::getResultSetToArrayDeUmCampo($objDatabase->result);
            if (count($vVetorSincronizador) > 0)
            {
                $vLastIdSincronizador = $vVetorSincronizador[0];
                if ($vLastIdSincronizador > $pUltimoIdSincronizadorAndroid)
                {
                    if (!$vIsFirstInsert)
                    {
                        echo DatabaseSincronizador::$DELIMITADOR_LINHA;
                    }
                    echo "OP%%LAST_ID_SINCRONIZADOR" . DatabaseSincronizador::$DELIMITADOR_LINHA . $vLastIdSincronizador;
                    $vIsFirstInsert = false;
                }
                else
                {
                    if ($vLastIdSincronizador == $pUltimoIdSincronizadorAndroid)
                    {
                        return;
                    }
                    else
                    {
                        if ($vLastIdSincronizador < $pUltimoIdSincronizadorAndroid)
                        {
                            return;
                        }
                    }
                }
            }
            else
            {
                return;
            }
            $vStrQueryIdRemove = "SELECT s.id id, s.id_tabela_INT id_tabela, s.sistema_tabela_id_INT id_sistema_tabela
                            FROM sistema_registro_sincronizador s
                            WHERE (
                            (s.is_from_android_BOOLEAN = '1' AND 
                            s.IMEI != '$pIMEI' AND
                            s.app_id_INT != '$idPrograma' AND
                            s.usuario_id_INT != " . $pIdUsuario . ") OR
                            s.is_from_android_BOOLEAN = '0') AND
                            s.tipo_operacao_banco_id_INT='" . DatabaseSincronizador::$INDEX_OPERACAO_REMOVE . "' AND
                            s.id > " . $pUltimoIdSincronizadorAndroid . " AND
                            s.id <= '" . $vLastIdSincronizador . "' AND
                            s.corporacao_id_INT = '" . $pIdCorporacao . "'
                            ORDER BY s.id_tabela_INT";

            $objDatabase->query($vStrQueryIdRemove);
            $vVetorObjRemove = Helper::getResultSetToArrayDeObjetosWithExplicitNull($objDatabase->result);

            $vVetorIdRemove = array();
            if (count($vVetorObjRemove) > 0)
            {
                if (!$vIsFirstInsert)
                {
                    echo DatabaseSincronizador::$DELIMITADOR_LINHA;
                }
                echo "OP%%REMOVE";
                $vIsFirstInsert = false;
            }
            foreach ($vVetorObjRemove as $key => $vObjRemove)
            {
                $vIdTabelaObjRemove = $vObjRemove["id_tabela"];
                $vIdSincronizador = $vObjRemove["id"];

                $vVetorIdRemove[count($vVetorIdRemove)] = $vIdTabelaObjRemove;
                echo DatabaseSincronizador::$DELIMITADOR_LINHA . $vIdSincronizador . DatabaseSincronizador::$DELIMITADOR_COLUNA;
                echo $vIdTabelaObjRemove . DatabaseSincronizador::$DELIMITADOR_COLUNA;
                echo $vObjRemove["id_sistema_tabela"];
            }

            $vVetorIdInsert = array();

            $vStrQueryIdInsert = "SELECT s.id id, s.id_tabela_INT id_tabela, s.sistema_tabela_id_INT id_sistema_tabela
                            FROM sistema_registro_sincronizador s
                            WHERE (
                            (s.is_from_android_BOOLEAN = '1' AND 
                             s.IMEI != '$pIMEI' AND
                            s.app_id_INT != '$idPrograma' AND
                            s.usuario_id_INT != " . $pIdUsuario . ") OR
                            s.is_from_android_BOOLEAN = '0') AND
                            s.tipo_operacao_banco_id_INT='" . DatabaseSincronizador::$INDEX_OPERACAO_INSERT . "' AND
                            s.id > " . $pUltimoIdSincronizadorAndroid . " AND
                            s.id <= '" . $vLastIdSincronizador . "' AND
                            s.corporacao_id_INT = '" . $pIdCorporacao . "'
                            ORDER BY s.id_tabela_INT";

            $objDatabase->query($vStrQueryIdInsert);
            $vVetorObjInsert = $v_vetorObjetos = Helper::getResultSetToArrayDeObjetosWithExplicitNull($objDatabase->result);
            if (count($vVetorObjInsert) > 0)
            {
                if (!$vIsFirstInsert)
                {
                    echo DatabaseSincronizador::$DELIMITADOR_LINHA;
                }
                echo "OP%%INSERT";
                $vIsFirstInsert = false;
            }
            foreach ($vVetorObjInsert as $key => $vObjInsert)
            {
                $vIdTabelaObjInsert = $vObjInsert["id_tabela"];
                $vVetorIdInsert[count($vVetorIdInsert)] = $vIdTabelaObjInsert;
                $vIdSincronizador = $vObjInsert["id"];

                if (!array_search($vIdTabelaObjInsert, $vVetorIdRemove))
                {
                    echo DatabaseSincronizador::$DELIMITADOR_LINHA . $vIdSincronizador . DatabaseSincronizador::$DELIMITADOR_COLUNA;
                    echo $vIdTabelaObjInsert . DatabaseSincronizador::$DELIMITADOR_COLUNA;
                    echo $vObjInsert["id_sistema_tabela"];
                }
            }

            $vStrQueryIdEdit = "SELECT s.id id, s.id_tabela_INT id_tabela, s.sistema_tabela_id_INT id_sistema_tabela
                            FROM sistema_registro_sincronizador s
                            WHERE (
                            (s.is_from_android_BOOLEAN = '1' AND 
                             s.IMEI != '$pIMEI' AND
                            s.app_id_INT != '$idPrograma' AND
                            s.usuario_id_INT != " . $pIdUsuario . ") OR
                            s.is_from_android_BOOLEAN = '0') AND
                            s.tipo_operacao_banco_id_INT='" . DatabaseSincronizador::$INDEX_OPERACAO_EDIT . "' AND
                            s.id > " . $pUltimoIdSincronizadorAndroid . " AND
                            s.id <= '" . $vLastIdSincronizador . "' AND
                            s.corporacao_id_INT = '" . $pIdCorporacao . "'
                            ORDER BY s.id_tabela_INT";

            $objDatabase->query($vStrQueryIdEdit);
            $vVetorObjEdit = Helper::getResultSetToArrayDeObjetosWithExplicitNull($objDatabase->result);
            if (count($vVetorObjEdit) > 0)
            {
                if (!$vIsFirstInsert)
                {
                    echo DatabaseSincronizador::$DELIMITADOR_LINHA;
                }
                echo "OP%%EDIT";
                $vIsFirstInsert = false;
            }
            foreach ($vVetorObjEdit as $key => $vObjEdit)
            {
                $vIdObjTabelaEdit = $vObjEdit["id_tabela"];
                $vIdSincronizador = $vObjEdit["id"];

                if (!array_search($vIdObjTabelaEdit, $vVetorIdInsert) &&
                    !array_search($vIdObjTabelaEdit, $vVetorIdRemove)
                )
                {
                    echo DatabaseSincronizador::$DELIMITADOR_LINHA . $vIdSincronizador . DatabaseSincronizador::$DELIMITADOR_COLUNA;
                    echo $vIdObjTabelaEdit . DatabaseSincronizador::$DELIMITADOR_COLUNA;
                    echo $vObjEdit["id_sistema_tabela"];
                }
            }
        }
    }

    public static function sendListQueryOfListIdSincronizador()
    {
        header("Content-Type: text/html; charset=UTF-8");
        if (!DatabaseSincronizador::checkUsuarioCorporacao())
        {
            echo "FALSE 0";
            return;
        }

        $pIdCorporacao = Helper::POSTGET("id_corporacao");
        $pStrVetorIdSistemaTabela = Helper::POSTGET("vetor_id_sincronizador");

        $pIdSistemaTabela = Helper::POSTGET("id_sistema_tabela");
        $pIdTipoOperacaoBanco = Helper::POSTGET("id_tipo_operacao_banco");

        $pIMEI = Helper::POSTGET("imei");
        $idPrograma = Helper::POSTGET("id_programa");

        if (strlen($pStrVetorIdSistemaTabela) &&
            strlen($pIdSistemaTabela) &&
            strlen($pIdCorporacao) &&
            strlen($pIdTipoOperacaoBanco)
        )
        {
            $vVetorIdSistemaTabela = explode(DatabaseSincronizador::$DELIMITADOR_COLUNA, $pStrVetorIdSistemaTabela);

            $vVetorIdTabela = array();
            $vStrNomeTabela = null;

            $vObjSistemaTabela = new EXTDAO_Sistema_tabela();
            $vObjSistemaTabela->select($pIdSistemaTabela);
            $vStrNomeTabela = $vObjSistemaTabela->getNome();

            if (!strlen($vStrNomeTabela))
            {
                echo "FALSE 1";
                return;
            }
            $vIndex = 0;
            foreach ($vVetorIdSistemaTabela as $key => $vIdSincronizador)
            {
                $objSincronizador = new EXTDAO_Sistema_registro_sincronizador();

                $objSincronizador->select($vIdSincronizador, $pIdCorporacao);
                $vIdCorporacaoAtual = $objSincronizador->getCorporacao_id_INT();

                if ($vIdCorporacaoAtual != $pIdCorporacao)
                {
                    continue;
                }
                else
                {
                    $vIdSistemaTabelaAtual = $objSincronizador->getSistema_tabela_id_INT();

                    if ($vIdSistemaTabelaAtual != $pIdSistemaTabela)
                    {
                        continue;
                    }
                    else
                    {
                        $vIdTabela = $objSincronizador->getId_tabela_INT();
                        $vVetorIdTabela[$vIndex] = $vIdTabela;
                        $vIndex += 1;
                    }
                }
            }
            if (count($vVetorIdTabela) > 0)
            {
                switch ($pIdTipoOperacaoBanco)
                {
                    case DatabaseSincronizador::$INDEX_OPERACAO_INSERT:
                        $vStrReturn = DatabaseSincronizador::printStrQueryInsertMultipleAndroidDatabaseOfVectorId($vStrNomeTabela, $vVetorIdTabela);
                        if ($vStrReturn == null)
                        {
                            echo "FALSE 2";
                        }
                        else
                        {
                            echo html_entity_decode($vStrReturn, ENT_QUOTES, "UTF-8");
                        }

                        exit();
                    case DatabaseSincronizador::$INDEX_OPERACAO_EDIT:
                        $vStrReturn = DatabaseSincronizador::printStrQueryEditMultipleAndroidDatabaseOfVectorId($vStrNomeTabela, $vVetorIdTabela);
                        if ($vStrReturn == null)
                        {
                            echo "FALSE 3";
                        }
                        else
                        {
                            echo html_entity_decode($vStrReturn, ENT_QUOTES, "UTF-8");
                        }

                        exit();
                    default:
                        echo "FALSE 4";
                        return;
                }
            }
        }
    }

    public static function getIdOfAttributeInVector($pVetorAttr, $pAttr)
    {
        if (count($pVetorAttr) > 0 && strlen($pAttr) > 0)
        {
            $pAttrLower = strtolower($pAttr);
            $i = 0;
            foreach ($pVetorAttr as $key => $vAttr)
            {
                if (strcmp(strtolower($vAttr), $pAttrLower) == 0)
                {
                    return $i;
                }
                $i += 1;
            }
        }
        return -1;
    }

    public static function isCorporacaoAttributeExistent($pObjDatabase, $p_strTableName)
    {
        if (strlen($p_strTableName))
        {
            $pObjDatabase->query("SHOW COLUMNS FROM " . $p_strTableName . ";");

            $vVetorAttr = Helper::getResultSetToArrayDeUmCampo($pObjDatabase->result);

            foreach ($vVetorAttr as $key => $vStrAttrName)
            {
                $vStrAttrNameLower = strtolower($vStrAttrName);
                if (strcmp($vStrAttrNameLower, "corporacao_id_int") == 0)
                {
                    return true;
                }
            }
            return false;
        }
    }

    public static function updateOperadoraEmpresa()
    {
        if (!DatabaseSincronizador::checkUsuarioCorporacao() && strlen(Helper::POSTGET("id_tupla")))
        {
            echo "FALSE 0";
            return;
        }

        $pIdCorporacao = Helper::POSTGET("id_corporacao");
        $pIdTupla = Helper::POSTGET("id_tupla");
        $vObjEmpresa = new EXTDAO_Empresa();
        $vObjEmpresa->select($pIdTupla, $pIdCorporacao);

        $vIdOperadora = $vObjEmpresa->getOperadora_id_INT();

        $vObjEmpresa->updateOperadora();
        $vIdNovaOperadora = $vObjEmpresa->getOperadora_id_INT();
        if ($vIdOperadora != $vIdNovaOperadora)
        {
            $vObjEmpresa->formatarParaSQL();
            $pIdUsuario = Helper::POSTGET("usuario");
            $vObjEmpresa->update($pIdTupla, "vazio", 1, false, $pIdCorporacao, $pIdUsuario);
            $pIdSistemaTabela = EXTDAO_Sistema_tabela::getIdDaSistemaTabela("empresa");
//            Considero que a insercao foi realizada via web, caso ocorra algum erro durante o protocolo entre o celular e o sistema web
            EXTDAO_Sistema_registro_sincronizador::insertTupla(
                $pIdSistemaTabela, $pIdUsuario, $pIdCorporacao, "0", DatabaseSincronizador::$INDEX_OPERACAO_EDIT, null, null, $pIdTupla);
            if ($vIdNovaOperadora == null || !strlen($vIdNovaOperadora))
            {
                echo DatabaseSincronizador::$NULL;
            }
            else
            {
                echo $vIdNovaOperadora;
            }
        }
        else
        {
            echo "TRUE 1";
        }
    }

    public static function updateOperadoraPessoa()
    {
        if (!DatabaseSincronizador::checkUsuarioCorporacao() && strlen(Helper::POSTGET("id_tupla")))
        {
            echo "FALSE 0";
            return;
        }

        $pIdCorporacao = Helper::POSTGET("id_corporacao");
        $pIdTupla = Helper::POSTGET("id_tupla");
        $vObjPessoa = new EXTDAO_Pessoa();
        $vObjPessoa->select($pIdTupla, $pIdCorporacao);

        $vIdOperadora = $vObjPessoa->getOperadora_id_INT();

        $vObjPessoa->updateOperadora();
        $vIdNovaOperadora = $vObjPessoa->getOperadora_id_INT();
        if ($vIdOperadora != $vIdNovaOperadora)
        {
            $vObjPessoa->formatarParaSQL();
            $pIdUsuario = Helper::POSTGET("usuario");
            $vObjPessoa->update($pIdTupla, "vazio", 1, false, $pIdCorporacao, $pIdUsuario);
            $pIdSistemaTabela = EXTDAO_Sistema_tabela::getIdDaSistemaTabela("pessoa");
//            Considero que a insercao foi realizada via web, caso ocorra algum erro durante o protocolo entre o celular e o sistema web
            EXTDAO_Sistema_registro_sincronizador::insertTupla(
                $pIdSistemaTabela,
                $pIdUsuario,
                $pIdCorporacao,
                "0",
                DatabaseSincronizador::$INDEX_OPERACAO_EDIT,
                null,
                null,
                $pIdTupla);
            if ($vIdNovaOperadora == null || !strlen($vIdNovaOperadora))
            {
                echo DatabaseSincronizador::$NULL;
            }
            else
            {
                echo $vIdNovaOperadora;
            }
        }
        else
        {
            echo "TRUE 1";
        }
    }

    public function factory()
    {
        return new DatabaseSincronizador();
    }

    public static function verificaEstruturaBancoAndroid()
    {
        $vStrListaTabelaAndroid = Helper::POSTGET("lista_tabela");
        $vListaTabelaAndroid = Helper::explode(",", $vStrListaTabelaAndroid);

        $database = new Database();
        $database->query("SELECT DISTINCT nome 
        FROM sistema_tabela 
        WHERE banco_mobile_BOOLEAN = 1 AND banco_web_BOOLEAN = 1");

        $vListaTabelaWeb = Helper::getResultSetToArrayDeUmCampo($database->result);
        $vIsPrimeiraVez = true;

        foreach ($vListaTabelaWeb as $tabelaWeb)
        {
            if (!in_array($tabelaWeb, $vListaTabelaAndroid))
            {
                if ($vIsPrimeiraVez)
                {
                    echo "TABELAS FALTANTES NO BANCO ANDROID";
                    echo ",$tabelaWeb";
                    $vIsPrimeiraVez = false;
                }
                else
                {
                    echo ",$tabelaWeb";
                }
            }
        }
        $vIsPrimeiraVez = true;
        foreach ($vListaTabelaAndroid as $tabelaAndroid)
        {
            if (!in_array($tabelaAndroid, $vListaTabelaWeb))
            {
                if ($vIsPrimeiraVez)
                {
                    echo ",TABELAS EXCEDENTES NO BANCO ANDROID";
                    echo ",$tabelaAndroid";
                    $vIsPrimeiraVez = false;
                }
                else
                {
                    echo ",$tabelaAndroid";
                }
            }
        }
    }

    public static function verificaEstruturaTabelaAndroid()
    {
        $tabelaAndroid = Helper::POSTGET("tabela");

        $vStrListaAtributoAndroid = Helper::POSTGET("lista_atributo");
        $vListaAtributoAndroid = Helper::explode(",", $vStrListaAtributoAndroid);

        $database = new Database();
        $database->query("SHOW COLUMNS FROM $tabelaAndroid");

        $vListaAtributoWeb = Helper::getResultSetToArrayDeUmCampo($database->result);

        $vIsPrimeiraVez = true;
        foreach ($vListaAtributoAndroid as $atributoAndroid)
        {
            if (!in_array($atributoAndroid, $vListaAtributoWeb))
            {
                if ($vIsPrimeiraVez)
                {
                    echo ",ATRIBUTOS FALTANTES NA TABELA '$tabelaAndroid' ANDROID,";
                    echo ",$atributoAndroid";
                    $vIsPrimeiraVez = false;
                }
                else
                {
                    echo ",$atributoAndroid";
                }
            }
        }

        $vIsPrimeiraVez = true;
        foreach ($vListaAtributoWeb as $atributoWeb)
        {
            if (!in_array($atributoWeb, $vListaAtributoAndroid))
            {
                if ($vIsPrimeiraVez)
                {
                    echo ",ATRIBUTOS EXCEDENTES NA TABELA '$tabelaAndroid' ANDROID,";
                    echo ",$atributoWeb";
                    $vIsPrimeiraVez = false;
                }
                else
                {
                    echo ",$atributoWeb";
                }
            }
        }
    }
}

?>
