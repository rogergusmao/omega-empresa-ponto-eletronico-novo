<?php
/**
 * Created by PhpStorm.
 * User: W10
 * Date: 02/02/2018
 * Time: 23:22
 */


class BO_Usuario_posicao
{

    public function salvarPosicoesNoBanco(
        $idUsuario
        , $idCorporacao
        , $strValores
    ){
        $registros = explode("%%", $strValores);
        if (count($registros) == 0)
        {
            return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO);
        }
        $objUsuarioPosicao = new EXTDAO_Usuario_posicao();
        foreach ($registros as $reg)
        {
            $valores = explode(";", $reg);
            $objUsuarioPosicao->setUsuario_id_INT($idUsuario);

            //sem usuario e sem corporacao
            if (count($valores) >= 9)
            {

                $objUsuarioPosicao->setCadastro_SEC( $valores[0]);
                $vHora = $valores[1];
                $objUsuarioPosicao->setCadastro_OFFSEC($vHora);

                //faca a verificacao quando for escolhido o veiculo usuario
                //ESTE DEVE ESTAR SINCRONIZADO
                $vVeiculoUsuario = $valores[2];
                if (strcmp($vVeiculoUsuario, "null") != 0)
                {
                    $objUsuarioPosicao->setVeiculo_usuario_id_INT($vVeiculoUsuario);
                }
                else
                {
                    $objUsuarioPosicao->setVeiculo_usuario_id_INT(null);
                }
                $vIdUsuario = $valores[3];
                if (is_numeric($vIdUsuario))
                {
                    $objUsuarioPosicao->setUsuario_id_INT($vIdUsuario);
                }
                else
                {
                    $objUsuarioPosicao->setUsuario_id_INT(null);
                }

                $objUsuarioPosicao->setLongitude_INT($valores[4]);
                $objUsuarioPosicao->setLatitude_INT($valores[5]);
                $objUsuarioPosicao->setVelocidade_INT($valores[6]);

                $fotoInterna = $valores[7];
                if (strcmp($fotoInterna, "null") != 0)
                {
                    $objUsuarioPosicao->setFoto_interna($fotoInterna);
                }
                else
                {
                    $objUsuarioPosicao->setFoto_interna(null);
                }

                $fotoExterna = $valores[8];
                if (strcmp($fotoExterna, "null") != 0)
                {
                    $objUsuarioPosicao->setFoto_externa($fotoExterna);
                }
                else
                {
                    $objUsuarioPosicao->setFoto_externa(null);
                }
                if(isset($valores[9]))
                    $objUsuarioPosicao->setFonte_informacao_INT($valores[9]);
                $objUsuarioPosicao->formatarParaSQL();
                $objUsuarioPosicao->insert(false, $idCorporacao);
            }
        }
        return new Mensagem(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO);
    }


    public function procedimentoGetDadosMapa($idCorporacao){
        $q = "SELECT up.latitude_INT lat, 
                        up.longitude_INT lng, 
                        up.velocidade_INT velocidade, 
                        u.nome nome, 
                        up.fonte_informacao_INT fonteInformacao,
                        max(up.cadastro_SEC) cadastroSec,
                        up.cadastro_OFFSEC cadastroOffsec
  FROM usuario_posicao up 
    JOIN usuario u 
      ON up.usuario_id_INT = u.id
  WHERE up.corporacao_id_INT = $idCorporacao
  GROUP BY up.usuario_id_INT";
        $db = new Database();

        $msg = $db->queryMensagem($q);
        if(Interface_mensagem::checkErro($msg)) return $msg;

        $dados = Helper::getResultSetToMatriz($db->result, 1, 0);
        if($dados == null || count($dados ) == 0){
            return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("N�o temos coordenadas de nenhum usu�rio do seu aplicativo. Fa�a o download do aplicativo Ponto Eletr�nico para rastrear!"));
        }
        else
            return new Mensagem_generica($dados);
    }

    public function getDadosMapa($idCorporacao){
        $q = "SELECT up.latitude_INT lat, 
                        up.longitude_INT lng, 
                        up.velocidade_INT velocidade, 
                        u.nome nome, 
                        up.fonte_informacao_INT fonteInformacao,
                        max(up.cadastro_SEC) cadastroSec,
                        up.cadastro_OFFSEC cadastroOffsec,
                        up.usuario_id_INT idUsuario
  FROM usuario_posicao up 
    JOIN usuario u 
      ON up.usuario_id_INT = u.id
  WHERE up.corporacao_id_INT = $idCorporacao
  GROUP BY up.usuario_id_INT";
        $db = new Database();

        $db->queryMensagemThrowException($q);

        $dados = Helper::getResultSetToMatriz($db->result, 1, 0);
        return $dados;
    }

    public function salvarArquivo($hora, $idUsuario, $corporacao){
        //vai salvar o arquivo temporariamente em um arquivo a parte
        //diretorios:
        //      conteudo/corporacao/data_hora_das_coordenadas/id_usuario.postions
        //$nomeArquivo =

//        $configuracaoSite = Registry::get('ConfiguracaoSite');
//        $path = $configuracaoSite->PATH_CONTEUDO."usuario_posicao/{$corporacao}/$hora/$idUsuario.out";

//      Somente um processo pode escrever no arquivo
//        $fp = fopen($file, 'a+');
//        flock($fp, LOCK_UN);
//        while($count > $loop) {
//            if (flock($fp, LOCK_EX)) {
//                fwrite($fp, $text);
//            }
//            flock($fp, LOCK_UN);
//        }
//        fclose($fp);
    }

}