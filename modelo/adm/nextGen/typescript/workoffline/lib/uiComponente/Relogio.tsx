import * as React from "react";
import { Modal } from 'react-bootstrap';

import { ReactComponent } from "lib/uiComponente";
import { ReactProps } from 'lib/geral';

export namespace Relogio
{
    export interface RelogioProps extends ReactProps
    {

    }

    export interface RelogioState
    {
        dataAtual :Date
    }
    
}

export class Relogio extends ReactComponent<Relogio.RelogioProps, Relogio.RelogioState>
{
    protected constructor(props: Relogio.RelogioProps, reactContext: any)
    {
        super(props || {} as Relogio.RelogioProps, reactContext);

        this.state = { dataAtual: new Date() };
        setInterval(() => this.atualizar(), 1000);                      
    }
    
    public atualizar() :void
    {
        this.setState({ dataAtual: new Date() });
    }
    
    public render()
    {        
        return (
            <div className="relogio">{this.state.dataAtual.toLocaleTimeString()}</div>
        );
    }

}
