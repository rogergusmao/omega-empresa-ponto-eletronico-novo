<?php

/*

Arquivo gerado atravï¿½s de gerador de cï¿½digo em 30/05/2017 as 02:04:02.
Para que o arquivo nï¿½o seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

Tabela correspondente: corporacao
Sobrescrita de dados: ---SOBRESCRITA_NAO_AUTORIZADA---

*/
?>

<?php

class EXTDAO_Corporacao extends DAO_Corporacao
{
    public function __construct($configDAO = null)
    {
        parent::__construct($configDAO);
        $this->nomeClasse = "EXTDAO_Corporacao";
    }

    public function factory()
    {
        return new EXTDAO_Corporacao();
    }

    static $TABELAS_COM_CORPORACAO = array();

    public static function isCorporacaoAttributeExistent($pObjDatabase, $p_strTableName)
    {
        if (!strlen($p_strTableName))
        {
            return null;
        }
        if (isset(self::$TABELAS_COM_CORPORACAO[$p_strTableName]))
        {
            return self::$TABELAS_COM_CORPORACAO[$p_strTableName];
        }

        $msg = $pObjDatabase->queryMensagem("SHOW COLUMNS FROM {$p_strTableName}  WHERE Field = 'corporacao_id_INT'");
        if ($msg != null && $msg->erro())
        {
            return $msg;
        }
        else
        {
            if ($msg->rows() > 0)
            {
                self::$TABELAS_COM_CORPORACAO[$p_strTableName] = true;

                return new Mensagem(null, null);
            }
            else
            {
                self::$TABELAS_COM_CORPORACAO[$p_strTableName] = false;

                return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO);
            }
        }
    }

//    public function insert($pIsToInsertSincronizador = false, $id = null, $objSincronizador = null)
//    {
//
//
//        //$this->id = ""; //limpar chave com autoincremento
//
//        if ($id != null)
//            $sql = "INSERT INTO corporacao ( id, nome,nome_normalizado,usuario_dropbox,senha_dropbox ) VALUES ( $id, $this->nome,$this->nome_normalizado,$this->usuario_dropbox,$this->senha_dropbox )";
//        else
//            $sql = "INSERT INTO corporacao ( nome,nome_normalizado,usuario_dropbox,senha_dropbox ) VALUES (  $this->nome,$this->nome_normalizado,$this->usuario_dropbox,$this->senha_dropbox )";
//
//        $this->database->query($sql);
//
//        if ($pIsToInsertSincronizador &&
//            strlen($this->idSistemaTabela)
//        ) {
//
//            $vUltimoIdInserido = $this->getIdDoUltimoRegistroInserido();
//            if (strlen($vUltimoIdInserido)) {
//
//                if ($objSincronizador != null)
//                    $objSincronizador->inserirRegistro(
//                        $this->idSistemaTabela,
//                        Seguranca::getId(),
//                        Seguranca::getIdDaCorporacaoLogada(),
//                        '0',
//                        DatabaseSincronizador::$INDEX_OPERACAO_INSERT,
//                        null,
//                        null,
//                        $vUltimoIdInserido);
//                else
//                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
//                        $this->idSistemaTabela,
//                        Seguranca::getId(),
//                        Seguranca::getIdDaCorporacaoLogada(),
//                        '0',
//                        DatabaseSincronizador::$INDEX_OPERACAO_INSERT,
//                        null,
//                        null,
//                        $vUltimoIdInserido);
//
//            }
//        }
//    }

    public static function getIdCorporacao($nome, $db = null)
    {
        if (strlen($nome) > 0)
        {
            if ($db == null)
            {
                $objBanco = new Database();
            }
            else
            {
                $objBanco = $db;
            }

            //checa a existencia do funcionario
            $query = "SELECT u.id as id
                 FROM corporacao u
                 WHERE u.nome = '" . Helper::strtoupperlatin1(Helper::formatarCampoTextoHTMLParaSQL($nome)) . "' ";

            $objBanco->query($query);
            $id = $objBanco->getPrimeiraTuplaDoResultSet(0);
//                $objBanco->close();
            //o usuario ja eh existente
            if ($id)
            {

                return $id;
            }
            else
            {
                return null;
            }
        }
    }

    public static function getIdDaCorporacao($nome)
    {
        if (strlen($nome) > 0)
        {

            $objBanco = new Database();

            //checa a existencia do funcionario
            $query = "SELECT u.id as id
                 FROM corporacao u
                 WHERE u.nome = '" . Helper::formatarCampoTextoHTMLParaSQL($nome) . "' ";

            $objBanco->query($query);

            //o usuario ja eh existente
            if ($objBanco->rows() == 1)
            {

                return $objBanco->getPrimeiraTuplaDoResultSet("id");
            }
            else
            {
                return null;
            }
        }
    }

    public static function updateNome(Database $db, $idCorporacao, $nomeCorporacao)
    {
        $q = "UPDATE corporacao 
              SET nome='$nomeCorporacao', nome_normalizado='$nomeCorporacao'
              WHERE id=$idCorporacao";

        return $db->queryMensagem($q);
    }

    public static function getCorporacaoNome(Database $db, $idCorporacao)
    {
        $q = "SELECT nome
          FROM corporacao
          WHERE id = $idCorporacao ";
        $msg = $db->queryMensagem($q);


        if ($msg != null && !$msg->ok())
        {
            return $msg;
        }
        else
        {
            $corp = $db->getPrimeiraTuplaDoResultSet(0);

            return new Mensagem_token(null, null, $corp);
        }
    }

}

