<?php


        /*

        Arquivo gerado através de gerador de código em 12/05/2018 as 14:33:00.
        Para que o arquivo não seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

        Tabela correspondente: empresa_atividade
        Sobrescrita de dados: ---SOBRESCRITA_AUTORIZADA---

        */
        


class DAO_Empresa_atividade extends Generic_DAO
                        {
                            const NOME_ENTIDADE = "empresa_atividade";

                            public static $nomeTabela = null;
                            public static $idSistemaTabela = false;
                            public static $databaseFieldsRelatedAttributes = null;
	                        public static $databaseFieldTypes = null;
	                        public static $databaseFieldNames = null;
	                        public static $listAliasTypes = null;
	                        public static $listAliasRelatedAttributes = null;
	                        
	                        public $id;
public $empresaId;
public $prazoEntregaDiaInt;
public $duracaoHorasInt;
public $cadastroSec;
public $cadastroOffsec;
public $precoCustoFloat;
public $precoVendaFloat;
public $atividadeId;
public $corporacaoId;

public $labelId;
public $labelEmpresaId;
public $labelPrazoEntregaDiaInt;
public $labelDuracaoHorasInt;
public $labelCadastroSec;
public $labelCadastroOffsec;
public $labelPrecoCustoFloat;
public $labelPrecoVendaFloat;
public $labelAtividadeId;
public $labelCorporacaoId;

public static $arrPK = array("id");

public function __construct($configDAO = null)
        {
            parent::__construct($configDAO);
            static::__setStaticValues($this->database);           
        }

public static function idSistemaTabela($databaseObject = null){
            if(!(static::$idSistemaTabela))
            {
                static::$idSistemaTabela = EXTDAO_Sistema_tabela::getIdDaSistemaTabela(static::nomeTabela(), $databaseObject);
            }
            return static::$idSistemaTabela;
        }

public function getIdSistemaTabela(){
            return static::idSistemaTabela();
        }

public static function nomeTabela(){
            if(is_null(static::$nomeTabela))
            {
                static::$nomeTabela = "empresa_atividade";
            }
            return static::$nomeTabela;
        }

public function getNomeTabela(){
            return static::nomeTabela();
        }

public static function __setStaticValues($databaseObject = null)
                   {    
                        static::nomeTabela();
                        
                        static::idSistemaTabela($databaseObject);
                   
                        static::setDatabaseFieldNames();
                        static::setDatabaseFieldTypes();
                        static::setDatabaseFieldsRelatedAttributeNames();
                        
                        static::setListAliasTypes();
                        static::setListAliasRelatedAttributes();
                   }

public function getExpressaoLabel()
                   {
                        return "{id}";
                   }

public static function setDatabaseFieldNames()
        {
            if(is_null(static::$databaseFieldNames))
            {
                static::$databaseFieldNames = new stdClass();
                static::$databaseFieldNames->id = "id";
static::$databaseFieldNames->empresaId = "empresa_id_INT";
static::$databaseFieldNames->prazoEntregaDiaInt = "prazo_entrega_dia_INT";
static::$databaseFieldNames->duracaoHorasInt = "duracao_horas_INT";
static::$databaseFieldNames->cadastroSec = "cadastro_SEC";
static::$databaseFieldNames->cadastroOffsec = "cadastro_OFFSEC";
static::$databaseFieldNames->precoCustoFloat = "preco_custo_FLOAT";
static::$databaseFieldNames->precoVendaFloat = "preco_venda_FLOAT";
static::$databaseFieldNames->atividadeId = "atividade_id_INT";
static::$databaseFieldNames->corporacaoId = "corporacao_id_INT";

            }
                      
        }

public static function setDatabaseFieldTypes()
        {
            if(is_null(static::$databaseFieldTypes))
            {
                static::$databaseFieldTypes = new stdClass();
                static::$databaseFieldTypes->id = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->empresaId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->prazoEntregaDiaInt = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->duracaoHorasInt = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->cadastroSec = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$databaseFieldTypes->cadastroOffsec = static::TIPO_VARIAVEL_TIMEZONE_OFFSET_IN_SECONDS;
static::$databaseFieldTypes->precoCustoFloat = static::TIPO_VARIAVEL_FLOAT;
static::$databaseFieldTypes->precoVendaFloat = static::TIPO_VARIAVEL_FLOAT;
static::$databaseFieldTypes->atividadeId = static::TIPO_VARIAVEL_INTEGER;
static::$databaseFieldTypes->corporacaoId = static::TIPO_VARIAVEL_INTEGER;

            }
            
        }

public static function setDatabaseFieldsRelatedAttributeNames()
        {
            if(is_null(static::$databaseFieldsRelatedAttributes))
            {
                static::$databaseFieldsRelatedAttributes = new stdClass();
                static::$databaseFieldsRelatedAttributes->id = "id";
static::$databaseFieldsRelatedAttributes->empresa_id_INT = "empresaId";
static::$databaseFieldsRelatedAttributes->prazo_entrega_dia_INT = "prazoEntregaDiaInt";
static::$databaseFieldsRelatedAttributes->duracao_horas_INT = "duracaoHorasInt";
static::$databaseFieldsRelatedAttributes->cadastro_SEC = "cadastroSec";
static::$databaseFieldsRelatedAttributes->cadastro_OFFSEC = "cadastroOffsec";
static::$databaseFieldsRelatedAttributes->preco_custo_FLOAT = "precoCustoFloat";
static::$databaseFieldsRelatedAttributes->preco_venda_FLOAT = "precoVendaFloat";
static::$databaseFieldsRelatedAttributes->atividade_id_INT = "atividadeId";
static::$databaseFieldsRelatedAttributes->corporacao_id_INT = "corporacaoId";

            }           
        }

public function setAllFieldsToHumanFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["empresa_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["prazo_entrega_dia_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["duracao_horas_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["cadastro_SEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["cadastro_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["preco_custo_FLOAT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["preco_venda_FLOAT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["atividade_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
$this->arrEstadosCampos["corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_EXIBICAO;
}

public function setAllFieldsToDatabaseFormat()
        {            
        $this->arrEstadosCampos["id"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["empresa_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["prazo_entrega_dia_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["duracao_horas_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["cadastro_SEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["cadastro_OFFSEC"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["preco_custo_FLOAT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["preco_venda_FLOAT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["atividade_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
$this->arrEstadosCampos["corporacao_id_INT"] = static::ESTADO_FORMATACAO_CAMPO_DB;
}

  public function getFkObjEmpresa() 
                {
                    if($this->objEmpresa == null)
                    {                        
                        $this->objEmpresa = new EXTDAO_Empresa_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getEmpresa_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objEmpresa->clear();
                    }
                    elseif($this->objEmpresa->getId() != $idFK)
                    {
                        $this->objEmpresa->select($idFK);
                    }
                    return $this->objEmpresa;
                }
  public function getFkObjAtividade() 
                {
                    if($this->objAtividade == null)
                    {                        
                        $this->objAtividade = new EXTDAO_Atividade_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getAtividade_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objAtividade->clear();
                    }
                    elseif($this->objAtividade->getId() != $idFK)
                    {
                        $this->objAtividade->select($idFK);
                    }
                    return $this->objAtividade;
                }
  public function getFkObjCorporacao() 
                {
                    if($this->objCorporacao == null)
                    {                        
                        $this->objCorporacao = new EXTDAO_Corporacao_id_INT($this->getConfiguracaoDAO());
                    }
                    $idFK = $this->getCorporacao_id_INT();
                    if(!isset($idFK))
                    {
                        $this->objCorporacao->clear();
                    }
                    elseif($this->objCorporacao->getId() != $idFK)
                    {
                        $this->objCorporacao->select($idFK);
                    }
                    return $this->objCorporacao;
                }


public function setLabels()
        {$this->labelId = "";
$this->labelEmpresaId = "";
$this->labelPrazoEntregaDiaInt = "";
$this->labelDuracaoHorasInt = "";
$this->labelCadastroSec = "";
$this->labelCadastroOffsec = "";
$this->labelPrecoCustoFloat = "";
$this->labelPrecoVendaFloat = "";
$this->labelAtividadeId = "";
$this->labelCorporacaoId = "";
}

public function __actionAdd($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                $this->setByObject($parameters);
                $this->setDataCadastroAndOffsetInSecondsFromUser($parameters);
                
                $this->formatarParaSQL();
                $msg = $this->insert(true);
                
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                return new Mensagem(null, I18N::getExpression("Empresa atividade adicionado com sucesso."));
                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionEdit($parameters = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try {

                $this->setByObject($parameters);
                $this->setAllFieldsToHumanFormat();
                
                $this->formatarParaSQL();
                $msg = $this->updateByInstanceUsingParameters($this->id, $parameters, true);
                if($msg != null && $msg->erro())
                {
                     return $msg;
                }
        
                return new Mensagem(null, I18N::getExpression("Empresa atividade editado com sucesso."));
            }
            catch(Exception $ex)
            {                
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }

public function __actionRemove($parameters = null)
        {
            $successulRemovalCount = 0;
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }

            try 
            {
                foreach($parameters as $record)
                {
                    if(is_numeric($record->id))
                    {
                        $msg = $this->delete($record->id, true);
                        if($msg == null || $msg->ok())
                        {
                            $successulRemovalCount++;
                        }
                    }
                }
                
                if($successulRemovalCount > 0)
                {
                    $successMessage = count($successulRemovalCount) > 1 ? I18N::getExpression("{0} Empresa atividade foram removidos com sucesso.", $successulRemovalCount) : I18N::getExpression("Empresa atividade removido com sucesso.");
                    return new Mensagem(null, $successMessage);
                }
                else
                {
                    $errorMessage = count($parameters) > 1 ? I18N::getExpression("Falha ao remover Empresa atividade.") : I18N::getExpression("Falha ao remover Empresa atividade.");
                    return new Mensagem(PROTOCOLO_SISTEMA::ERRO_SEM_SER_EXCECAO, $errorMessage);                    
                }
            }
            catch (Exception $ex) 
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }

public function __getRecord($parameters = null)
        {
            if(is_null($parameters))
            {
                $parameters = Helper::getPhpInputObject();
            }
            
            try
            {            
                $returnObject = new stdClass();            
                $whereClause = static::getWhereClauseForFilter($parameters->filterParameters);
                $query = "SELECT  id, empresa_id_INT, prazo_entrega_dia_INT, duracao_horas_INT, cadastro_SEC, cadastro_OFFSEC, preco_custo_FLOAT, preco_venda_FLOAT, atividade_id_INT, corporacao_id_INT FROM empresa_atividade {$whereClause}";
    
                $msg = $this->database->queryMensagem($query);                
                if ($msg == null)
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Não foi encontrado nenhum registro"));
                    }                    
                    $resultSet = $this->database->result;
                    $resultObject = static::mapResultSetDataToRecord($resultSet);
                    
                    $returnObject->formData = $resultObject;
                    $returnObject->comboBoxesData = null;
                    if($parameters->loadComboBoxesData)
                    {
                        $this->setByObject($resultObject);
                        $returnObject->comboBoxesData =  $this->getFormComboBoxesData();
                    }                    
    
                    
    
                    return new Mensagem_generica($returnObject);
                } 
                else 
                {
                    return $msg;
                }
                
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }


        
        public function __getFilterComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
                
        public function __getFormComboBoxesData()
        {
            try
            {
                $returnObject = new stdClass();
                $returnObject->comboBoxesData = $this->getFormComboBoxesData();
                
                static::appendTypesProperty($returnObject, static::$databaseFieldTypes);
                return new Mensagem_generica($returnObject);
            }
            catch (Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }
        }
        
        protected function getFormComboBoxesData()
        {
            $comboBoxesData = new stdClass();
            $listParameters = new stdClass();
            $listParameters->listMappingType = static::LIST_MAPPING_TYPE_COMBOBOX;
            
            
                    
                    $objEmpresa = new EXTDAO_Empresa();
                    $comboBoxesData->fieldEmpresaId = $objEmpresa->__getList($listParameters);
                    
                    $objAtividade = new EXTDAO_Atividade();
                    $comboBoxesData->fieldAtividadeId = $objAtividade->__getList($listParameters);

            return $comboBoxesData;
        }
        
        


        
        public static function setListAliasTypes()
        {
            if(is_null(static::$listAliasTypes))
            {
                static::$listAliasTypes = new stdClass();
                static::$listAliasTypes->empresa__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->atividade__nome = static::TIPO_VARIAVEL_TEXT;
static::$listAliasTypes->empresa_atividade__id = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->empresa_atividade__ind_celular_valido_BOOLEAN = static::TIPO_VARIAVEL_BOOLEAN;
static::$listAliasTypes->empresa_atividade__prazo_entrega_dia_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->empresa_atividade__duracao_horas_INT = static::TIPO_VARIAVEL_INTEGER;
static::$listAliasTypes->empresa_atividade__cadastro_SEC = static::TIPO_VARIAVEL_TIME_IN_SECONDS;
static::$listAliasTypes->empresa_atividade__preco_custo_FLOAT = static::TIPO_VARIAVEL_FLOAT;
static::$listAliasTypes->empresa_atividade__preco_venda_FLOAT = static::TIPO_VARIAVEL_FLOAT;
static::$listAliasTypes->empresa_atividade__relatorio_id_INT = static::TIPO_VARIAVEL_INTEGER;
            }        
        }
        
        public static function setListAliasRelatedAttributes()
        {
            if(is_null(static::$listAliasRelatedAttributes))
            {
                static::$listAliasRelatedAttributes = new stdClass();
                static::$listAliasRelatedAttributes->empresa__id = "empresaId";
static::$listAliasRelatedAttributes->atividade__nome = "atividadeNome";
static::$listAliasRelatedAttributes->empresa_atividade__id = "id";
static::$listAliasRelatedAttributes->empresa_atividade__ind_celular_valido_BOOLEAN = "indCelularValidoBoolean";
static::$listAliasRelatedAttributes->empresa_atividade__prazo_entrega_dia_INT = "prazoEntregaDiaInt";
static::$listAliasRelatedAttributes->empresa_atividade__duracao_horas_INT = "duracaoHorasInt";
static::$listAliasRelatedAttributes->empresa_atividade__cadastro_SEC = "cadastroSec";
static::$listAliasRelatedAttributes->empresa_atividade__preco_custo_FLOAT = "precoCustoFloat";
static::$listAliasRelatedAttributes->empresa_atividade__preco_venda_FLOAT = "precoVendaFloat";
static::$listAliasRelatedAttributes->empresa_atividade__relatorio_id_INT = "relatorioId";
            }         
        }

        public function __getList($parameters = null, $idCorporacao = null)
        {
            if(is_null($parameters)) $parameters = Helper::getPhpInputObject();

            try
            {
                $filterParameters = new stdClass();
                if(isset($parameters->filterParameters))
                {
                    $filterParameters = $parameters->filterParameters;
                }
                                    
                            
            if(is_null($idCorporacao))
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            ListParameters::addFilterParameterToObject($filterParameters, "corporacaoId", $idCorporacao);           
                
                
                $sortingParameters = null;
                if(isset($parameters->sortingParameters))
                {
                    $sortingParameters = $parameters->sortingParameters;
                }
                
                $paginationParameters = null;
                if(isset($parameters->paginationParameters))
                {
                    $paginationParameters = $parameters->paginationParameters;
                }
                
                $listMappingType = static::getListMappingType($parameters->listMappingType);
                $mainTableAlias = "ea";

                $whereClause = static::getWhereClauseForFilter($filterParameters, $mainTableAlias);
                $orderByClause = static::getOrderByClauseForFilter($sortingParameters, $mainTableAlias);
                $limitClause = static::getLimitClauseForFilter($paginationParameters);

                $queryWithoutLimit = "SELECT ea.id FROM empresa_atividade ea {$whereClause}";
                $query = "SELECT e.id AS empresa__id, a.nome AS atividade__nome, ea.id AS empresa_atividade__id, ea.ind_celular_valido_BOOLEAN AS empresa_atividade__ind_celular_valido_BOOLEAN, ea.prazo_entrega_dia_INT AS empresa_atividade__prazo_entrega_dia_INT, ea.duracao_horas_INT AS empresa_atividade__duracao_horas_INT, ea.cadastro_SEC AS empresa_atividade__cadastro_SEC, ea.preco_custo_FLOAT AS empresa_atividade__preco_custo_FLOAT, ea.preco_venda_FLOAT AS empresa_atividade__preco_venda_FLOAT, ea.relatorio_id_INT AS empresa_atividade__relatorio_id_INT FROM empresa_atividade ea LEFT JOIN empresa e ON e.id = ea.empresa_id_INT LEFT JOIN atividade a ON a.id = ea.atividade_id_INT {$whereClause} {$orderByClause} {$limitClause}";

                $msg = $this->database->queryMensagem($queryWithoutLimit);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }
                
                $totalNumberOfRecords = $this->database->rows();
                if($totalNumberOfRecords == 0)
                {
                    return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                }                        
                        
                $msg = $this->database->queryMensagem($query);
                
                //operacao realizaca com sucesso
                if ($msg == null) 
                {
                    if($this->database->rows() == 0)
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nao encontrou nenhum registro"));
                    }                        
                
                    $resultSet = $this->database->result;

                    $objReturn = new stdClass();
                    $objReturn->dataSet = static::mapResultSetDataToList($resultSet, $listMappingType, static::$listAliasTypes);

                    if(!Helper::isNullOrEmpty($paginationParameters->recordsPerPage) && !Helper::isNullOrEmpty($paginationParameters->currentPage))
                    {
                        $lastPage = ceil($totalNumberOfRecords / $paginationParameters->recordsPerPage);
                        $objReturn->paginationParameters = new PaginationParams($paginationParameters->recordsPerPage, $paginationParameters->currentPage, $lastPage);
                    }
                    
                    return new Mensagem_generica($objReturn);
                } 
                else
                {
                    return $msg;
                }                
            }
            catch(Exception $ex)
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, null, $ex);
            }

        }



public function getId()
            {
                return $this->id;
            }

public function getEmpresaId()
            {
                return $this->empresaId;
            }

public function getEmpresa_id_INT()
                {
                    return $this->empresaId;
                }

public function getPrazoEntregaDiaInt()
            {
                return $this->prazoEntregaDiaInt;
            }

public function getPrazo_entrega_dia_INT()
                {
                    return $this->prazoEntregaDiaInt;
                }

public function getDuracaoHorasInt()
            {
                return $this->duracaoHorasInt;
            }

public function getDuracao_horas_INT()
                {
                    return $this->duracaoHorasInt;
                }

public function getCadastroSec()
            {
                return $this->cadastroSec;
            }

public function getCadastro_SEC()
                {
                    return $this->cadastroSec;
                }

public function getCadastroOffsec()
            {
                return $this->cadastroOffsec;
            }

public function getCadastro_OFFSEC()
                {
                    return $this->cadastroOffsec;
                }

public function getPrecoCustoFloat()
            {
                return $this->precoCustoFloat;
            }

public function getPreco_custo_FLOAT()
                {
                    return $this->precoCustoFloat;
                }

public function getPrecoVendaFloat()
            {
                return $this->precoVendaFloat;
            }

public function getPreco_venda_FLOAT()
                {
                    return $this->precoVendaFloat;
                }

public function getAtividadeId()
            {
                return $this->atividadeId;
            }

public function getAtividade_id_INT()
                {
                    return $this->atividadeId;
                }

public function getCorporacaoId()
            {
                return $this->corporacaoId;
            }

public function getCorporacao_id_INT()
                {
                    return $this->corporacaoId;
                }



function setId($value)
            {
                $this->id = $value;
            }

function setEmpresaId($value)
            {
                $this->empresaId = $value;
            }

public function setEmpresa_id_INT($value)
                { 
                    $this->empresaId = $value; 
                }

function setPrazoEntregaDiaInt($value)
            {
                $this->prazoEntregaDiaInt = $value;
            }

public function setPrazo_entrega_dia_INT($value)
                { 
                    $this->prazoEntregaDiaInt = $value; 
                }

function setDuracaoHorasInt($value)
            {
                $this->duracaoHorasInt = $value;
            }

public function setDuracao_horas_INT($value)
                { 
                    $this->duracaoHorasInt = $value; 
                }

function setCadastroSec($value)
            {
                $this->cadastroSec = $value;
            }

public function setCadastro_SEC($value)
                { 
                    $this->cadastroSec = $value; 
                }

function setCadastroOffsec($value)
            {
                $this->cadastroOffsec = $value;
            }

public function setCadastro_OFFSEC($value)
                { 
                    $this->cadastroOffsec = $value; 
                }

function setPrecoCustoFloat($value)
            {
                $this->precoCustoFloat = $value;
            }

public function setPreco_custo_FLOAT($value)
                { 
                    $this->precoCustoFloat = $value; 
                }

function setPrecoVendaFloat($value)
            {
                $this->precoVendaFloat = $value;
            }

public function setPreco_venda_FLOAT($value)
                { 
                    $this->precoVendaFloat = $value; 
                }

function setAtividadeId($value)
            {
                $this->atividadeId = $value;
            }

public function setAtividade_id_INT($value)
                { 
                    $this->atividadeId = $value; 
                }

function setCorporacaoId($value)
            {
                $this->corporacaoId = $value;
            }

public function setCorporacao_id_INT($value)
                { 
                    $this->corporacaoId = $value; 
                }



public function clear()
        {$this->id = null;
$this->empresaId = null;
if($this->objEmpresa != null) unset($this->objEmpresa);
$this->prazoEntregaDiaInt = null;
$this->duracaoHorasInt = null;
$this->cadastroSec = null;
$this->cadastroOffsec = null;
$this->precoCustoFloat = null;
$this->precoVendaFloat = null;
$this->atividadeId = null;
if($this->objAtividade != null) unset($this->objAtividade);
$this->corporacaoId = null;
if($this->objCorporacao != null) unset($this->objCorporacao);
if(is_array($this->arrEstadosCampos)){
                        unset($this->arrEstadosCampos);
                     $this->arrEstadosCampos = array();
                    }
                }

public function formatarParaSQL()
        {
if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->id)){
$this->id = $this->formatarIntegerParaComandoSQL($this->id);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->id);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->empresaId)){
$this->empresaId = $this->formatarIntegerParaComandoSQL($this->empresaId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->empresaId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->prazoEntregaDiaInt)){
$this->prazoEntregaDiaInt = $this->formatarIntegerParaComandoSQL($this->prazoEntregaDiaInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->prazoEntregaDiaInt);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->duracaoHorasInt)){
$this->duracaoHorasInt = $this->formatarIntegerParaComandoSQL($this->duracaoHorasInt);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->duracaoHorasInt);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->cadastroSec)){
$this->cadastroSec = $this->formatarIntegerParaComandoSQL($this->cadastroSec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->cadastroSec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->cadastroOffsec)){
$this->cadastroOffsec = $this->formatarIntegerParaComandoSQL($this->cadastroOffsec);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->cadastroOffsec);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->precoCustoFloat)){
$this->precoCustoFloat = $this->formatarFloatParaComandoSQL($this->precoCustoFloat);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->precoCustoFloat);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->precoVendaFloat)){
$this->precoVendaFloat = $this->formatarFloatParaComandoSQL($this->precoVendaFloat);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->precoVendaFloat);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->atividadeId)){
$this->atividadeId = $this->formatarIntegerParaComandoSQL($this->atividadeId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->atividadeId);
}

if($this->isCampoFormatadoParaExibicaoOuSemFormatacaoDefinida(static::$databaseFieldNames->corporacaoId)){
$this->corporacaoId = $this->formatarIntegerParaComandoSQL($this->corporacaoId);
$this->marcarFormatacaoDoCampoComoBancoDeDados(static::$databaseFieldNames->corporacaoId);
}

}

public function formatarParaExibicao()
        {if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->precoCustoFloat)){
$this->precoCustoFloat = $this->formatarFloatParaExibicao($this->precoCustoFloat);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->precoCustoFloat);
}

if($this->isCampoFormatadoParaBancoDeDadosOuSemFormatacaoDefinida(static::$databaseFieldNames->precoVendaFloat)){
$this->precoVendaFloat = $this->formatarFloatParaExibicao($this->precoVendaFloat);
$this->marcarFormatacaoDoCampoComoExibicao(static::$databaseFieldNames->precoVendaFloat);
}

}

function select($id, $idCorporacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }   

            $sql = "SELECT id, empresa_id_INT, prazo_entrega_dia_INT, duracao_horas_INT, cadastro_SEC, cadastro_OFFSEC, preco_custo_FLOAT, preco_venda_FLOAT, atividade_id_INT, corporacao_id_INT FROM empresa_atividade WHERE id = $id AND corporacao_id_INT = $idCorporacao";

            $msg = $this->database->queryMensagem($sql);
            $result = $this->database->result;

            if($msg != null && $msg->erro() )
            { 
            		$this->clear();
            		return $msg;
        	} 
        	else if($this->database->rows() == 0)
        	{
                return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, I18N::getExpression("Nenhum registro foi encontrado."));
            }
        

            $row = $this->database->fetchArray(MYSQLI_NUM);
            
            		$this->id = $row[0];
		$this->empresaId = $row[1];
		$this->prazoEntregaDiaInt = $row[2];
		$this->duracaoHorasInt = $row[3];
		$this->cadastroSec = $row[4];
		$this->cadastroOffsec = $row[5];
		$this->precoCustoFloat = $row[6];
		$this->precoVendaFloat = $row[7];
		$this->atividadeId = $row[8];

            
        }

public function delete($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (is_null($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (is_null($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }

            $sql = "DELETE FROM empresa_atividade WHERE id= $id AND corporacao_id_INT = $idCorporacao";
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                    && $sincronizar
                        && static::idSistemaTabela() != null
                            && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_REMOVE,
                                null,
                                null,
                                $id,
                                $this->database);
            }

            return $msg;
        }

public function insert($sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null, $opcoes = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $this->defineDataCadastroInSecondsIfNotDefined();
$this->defineDataCadastroOffsetInSecondsIfNotDefined();

            
            if (is_null($opcoes))
            {
                $opcoes = $this->opcoesDAO;
            }

            if (!is_numeric($this->id))
            {
                $this->id = "null";
                if ($opcoes == null || $opcoes[Database::OPCAO_GERAR_ID] != false) 
                {
                    $this->id = EXTDAO_Sistema_sequencia::gerarId(static::nomeTabela());                    
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }    
                    else if($msg == null)
                    {
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
                else 
                {
                    $this->id = null;
                    $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao));
                    
                    if($msg != null && $msg->erro())
                    {
                        return $msg;
                    }
                    else if($msg == null)
                    {
                        $this->id = $this->database->getLastInsertId();
                        $msg = new Mensagem_token();
                        $msg->mValor = $this->id;
                    }
                }
            }
            else 
            {
                $msg = $this->database->queryMensagem(static::getSQLInsert($idCorporacao), Database::OPCAO_NAO_BUSCAR_ID);
                if($msg != null && $msg->erro())
                {
                    return $msg;
                }                        
                else if($msg == null)
                {
                   $msg = new Mensagem_token();
                   $msg->mValor = $this->id;
                }
            }
            if($msg == null || $msg->ok())
            {                
                if($sincronizar
                    && static::idSistemaTabela() != null
                    && isset($this->id)){
                    
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_INSERT,
                                null,
                                null,
                                $this->id,
                                $this->database); 
                	}
            } 
            else
            {
                $this->id = null;
            }
            
            return $msg;
            
        }

public function getSQLInsert($idCorporacao){ 

return "INSERT INTO empresa_atividade (id, empresa_id_INT, prazo_entrega_dia_INT, duracao_horas_INT, cadastro_SEC, cadastro_OFFSEC, preco_custo_FLOAT, preco_venda_FLOAT, atividade_id_INT, corporacao_id_INT) VALUES ( $this->id ,  $this->empresaId ,  $this->prazoEntregaDiaInt ,  $this->duracaoHorasInt ,  $this->cadastroSec ,  $this->cadastroOffsec ,  $this->precoCustoFloat ,  $this->precoVendaFloat ,  $this->atividadeId ,  $idCorporacao ) "; 

}




        
        public function updateByParameters($id, $objParametros, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao))
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();

            
                
                if (isset($objParametros->empresaId)) 
                {
                    $arrUpdateFields[] = " empresa_id_INT = {$objParametros->empresaId} ";
                }


                
                if (isset($objParametros->prazoEntregaDiaInt)) 
                {
                    $arrUpdateFields[] = " prazo_entrega_dia_INT = {$objParametros->prazoEntregaDiaInt} ";
                }


                
                if (isset($objParametros->duracaoHorasInt)) 
                {
                    $arrUpdateFields[] = " duracao_horas_INT = {$objParametros->duracaoHorasInt} ";
                }


                
                if (isset($objParametros->cadastroSec)) 
                {
                    $arrUpdateFields[] = " cadastro_SEC = {$objParametros->cadastroSec} ";
                }


                
                if (isset($objParametros->cadastroOffsec)) 
                {
                    $arrUpdateFields[] = " cadastro_OFFSEC = {$objParametros->cadastroOffsec} ";
                }


                
                if (isset($objParametros->precoCustoFloat)) 
                {
                    $arrUpdateFields[] = " preco_custo_FLOAT = {$objParametros->precoCustoFloat} ";
                }


                
                if (isset($objParametros->precoVendaFloat)) 
                {
                    $arrUpdateFields[] = " preco_venda_FLOAT = {$objParametros->precoVendaFloat} ";
                }


                
                if (isset($objParametros->atividadeId)) 
                {
                    $arrUpdateFields[] = " atividade_id_INT = {$objParametros->atividadeId} ";
                }



            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE empresa_atividade SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";

            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null)
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function update($id, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($this->empresaId)) 
                {                                      
                    $arrUpdateFields[] = " empresa_id_INT = {$this->empresaId} ";
                }


                
                if (isset($this->prazoEntregaDiaInt)) 
                {                                      
                    $arrUpdateFields[] = " prazo_entrega_dia_INT = {$this->prazoEntregaDiaInt} ";
                }


                
                if (isset($this->duracaoHorasInt)) 
                {                                      
                    $arrUpdateFields[] = " duracao_horas_INT = {$this->duracaoHorasInt} ";
                }


                
                if (isset($this->cadastroSec)) 
                {                                      
                    $arrUpdateFields[] = " cadastro_SEC = {$this->cadastroSec} ";
                }


                
                if (isset($this->cadastroOffsec)) 
                {                                      
                    $arrUpdateFields[] = " cadastro_OFFSEC = {$this->cadastroOffsec} ";
                }


                
                if (isset($this->precoCustoFloat)) 
                {                                      
                    $arrUpdateFields[] = " preco_custo_FLOAT = {$this->precoCustoFloat} ";
                }


                
                if (isset($this->precoVendaFloat)) 
                {                                      
                    $arrUpdateFields[] = " preco_venda_FLOAT = {$this->precoVendaFloat} ";
                }


                
                if (isset($this->atividadeId)) 
                {                                      
                    $arrUpdateFields[] = " atividade_id_INT = {$this->atividadeId} ";
                }


            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE empresa_atividade SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }


        
        public function updateByInstanceUsingParameters($id, $parameters, $sincronizar = false, $idCorporacao = null, $idUsuarioOperacao = null)
        {
            if (Helper::isNullOrEmpty($idCorporacao)) 
            {
                $idCorporacao = Seguranca::getIdDaCorporacaoLogada();
            }
            if (Helper::isNullOrEmpty($idUsuarioOperacao)) 
            {
                $idUsuarioOperacao = Seguranca::getId();
            }
            
            $arrUpdateFields = array();       

            
                
                if (isset($parameters->empresaId)) 
                {                                      
                    $arrUpdateFields[] = " empresa_id_INT = {$this->empresaId} ";
                }
                
                if (isset($parameters->prazoEntregaDiaInt)) 
                {                                      
                    $arrUpdateFields[] = " prazo_entrega_dia_INT = {$this->prazoEntregaDiaInt} ";
                }
                
                if (isset($parameters->duracaoHorasInt)) 
                {                                      
                    $arrUpdateFields[] = " duracao_horas_INT = {$this->duracaoHorasInt} ";
                }
                
                if (isset($parameters->cadastroSec)) 
                {                                      
                    $arrUpdateFields[] = " cadastro_SEC = {$this->cadastroSec} ";
                }
                
                if (isset($parameters->cadastroOffsec)) 
                {                                      
                    $arrUpdateFields[] = " cadastro_OFFSEC = {$this->cadastroOffsec} ";
                }
                
                if (isset($parameters->precoCustoFloat)) 
                {                                      
                    $arrUpdateFields[] = " preco_custo_FLOAT = {$this->precoCustoFloat} ";
                }
                
                if (isset($parameters->precoVendaFloat)) 
                {                                      
                    $arrUpdateFields[] = " preco_venda_FLOAT = {$this->precoVendaFloat} ";
                }
                
                if (isset($parameters->atividadeId)) 
                {                                      
                    $arrUpdateFields[] = " atividade_id_INT = {$this->atividadeId} ";
                }
            
            $strUpdateFields = implode(", ", $arrUpdateFields);
            $sql = "UPDATE empresa_atividade SET {$strUpdateFields} WHERE id = {$id} AND corporacao_id_INT = {$idCorporacao}";
            
            $msg = $this->database->queryMensagem($sql);
            if($msg == null
                && $sincronizar
                    && static::idSistemaTabela() != null
                        && isset($id))
            {
                
                    EXTDAO_Sistema_registro_sincronizador::insertTupla(
                                static::idSistemaTabela(),
                                $idUsuarioOperacao,
                                $idCorporacao,
                                '0',
                                Generic_DAO::INDEX_OPERACAO_EDIT,
                                null,
                                null,
                                $id,
                                $this->database);
                
            }
            return $msg;

        }

}

?>
