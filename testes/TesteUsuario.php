<?php

use PHPUnit\Framework\TestCase;

//DICA 1

//F.I.R.S.T Principles of Unit Testing
//FAST, ISOLATED/INDEPENDENT, REPEATABLE, SELF-VALIDATING and THOROUGH/TIMELY
//https://github.com/ghsukumar/SFDC_Best_Practices/wiki/F.I.R.S.T-Principles-of-Unit-Testing


//DICA 2

//What happens when a user is created? For example that exists one more user in database. For example you can count users before and after the POST calls.

//You dont really need to test POST, but what happens when POST calls is committed.

//And you dont test that a /user/{id} exists. This make test dependent to an id: tests MUST be independent. You can put in the same test a POST (to create the user), get the last id from the response or from the database and then GET that user. This is a way to check that exactly same user is in the database. And is a way to remove the id. If you depend on ids, the test run only once. Your tests must be repeatable.

//Reset database each time costs too much. I suggest you to do the //following steps:

//1) recreate database with fixtures (just what you need for tests) 2) //run all tests

//Remember also that tests must be fast. A test suite with a duration //over 15/20 minutes is slow! You need to be fast.

//Reading test testReadSingleUser, I suggest you to create the user "by //hand", retrieve the id, finally to GET the user with that id. This //makes the test repeatable infinite times.


class TesteUsuario extends TestCase
{
	protected $baseUrl = 'http://localhost/pmv2';

	use DatabaseMigrations;

	public function testCreatesUser()
	{
		echo "\nTest: POST /users => Create new user";

		$data = [
			'first_name'   => 'first_new_user',
			'last_name'    => 'last_new_user',
			'email'        => 'email_new@pm.com',
			'password'     => 'new_password',
			'phone_number' => '3333333333',
			'status'       => 'active',
			'created_at'   => '2000-1-1 10:10:00',
			'updated_at'   => '2000-1-1 10:10:00',
		];

		$response = $this->post('/users', $data);
		$response->assertStatus(200);

		$this->assertDatabaseHas('users', ['email' => $data['email']]);
	}

	public function testReadAllUsers()
	{
		$this->seed('UsersTableSeeder');

		echo "\nTest: GET /users => Read all users";

		$this->seed('UsersTableSeeder');

		$response = $this->get('/users');

		$response->assertStatus(200);
		$response->assertJson([
			'found' => true,
			'users' => [],
		]);
	}

	public function testReadSingleUser()
	{
		$this->seed('UsersTableSeeder');

		echo "\nTest: POST /users => Read single user";

		$response = $this->get('/users/1');

		$response->assertStatus(200);
		$response->assertJson([
			'found' => true,
			'user'  => [],
		]);

	}

	public function testUpdateUser()
	{
		$this->seed('UsersTableSeeder');

		echo "\nTest: POST /users => Create new user";

		$data = [
			'first_name'   => 'first_updated_user',
			'last_name'    => 'last_updated_user',
			'email'        => 'email_updated@pm.com',
			'password'     => 'updated_password',
			'phone_number' => '44444444444',
			'updated_at'   => '2000-1-1 10:10:00',
		];

		$response = $this->put('/users/1', $data);

		$response->assertStatus(200);
		$this->assertDatabaseHas('users', ['email' => $data['email']]);
	}
}

?>