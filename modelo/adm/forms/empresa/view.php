<?php

/*

Arquivo gerado através de gerador de código em 07/01/2018 as 16:14:45.
Para que o arquivo não seja sobrescrito pelo gerador, altere o valor para SOBRESCRITA_NAO_AUTORIZADA na linha abaixo, mantendo os --- antes e depois da constante

Tabela correspondente: empresa
Sobrescrita de dados: ---SOBRESCRITA_NAO_AUTORIZADA---

*/
?>

<!--Definição da View Angular JS -->
<?

$objForm = new FormEnvironment("mainForm", "formEmpresaController", "vm");
$objForm->setLayoutOneColumn();

?>
<div class='row' id='content-wrapper' ng-controller="<?= $objForm->getAngularControllerAttributeValue() ?>">

    <?= $objForm->setModalEnvironmentIfApplicable(); ?>
    <?= $objForm->setEditionVariablesIfApplicable(); ?>

    <!--Definição do template do dialog exibido após inserção -->
    <script type="text/ng-template" id="after-empresa-insert-template.html">
        <div class="modal-header">
            <h4 class="modal-title">
                <span class="glyphicon"></span>
                <?= I18N::getExpression("Empresa adicionada com sucesso!"); ?>
            </h4>
        </div>
        <div class="modal-body">
            <span class="help-block"><?= I18N::getExpression("O que deseja fazer agora?"); ?></span>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" ng-click="vm.loadListOfEmpresa()">
                <?= I18N::getExpression("Listar empresas"); ?>
            </button>
            <button type="button" class="btn" ng-click="closeMessageDialog()">
                <?= I18N::getExpression("Permanecer na tela"); ?>
            </button>
            <button type="button" class="btn btn-success" ng-click="vm.addNewEmpresa()">
                <?= I18N::getExpression("Adicionar empresa"); ?>
            </button>
        </div>
    </script>

    <!--Definição do template do exibido dialog após update -->
    <script type="text/ng-template" id="after-empresa-update-template.html">
        <div class="modal-header">
            <h4 class="modal-title">
                <span class="glyphicon"></span>
                <?= I18N::getExpression("Empresa alterada com sucesso!"); ?>

            </h4>
        </div>
        <div class="modal-body">
            <span class="help-block"><?= I18N::getExpression("O que deseja fazer agora?"); ?></span>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" ng-click="vm.loadListOfEmpresa()">
                <?= I18N::getExpression("Listar empresas"); ?>
            </button>
            <button type="button" class="btn" ng-click="closeMessageDialog()">
                <?= I18N::getExpression("Permanecer na tela"); ?>
            </button>
            <button type="button" class="btn btn-success" ng-click="vm.addNewEmpresa()">
                <?= I18N::getExpression("Adicionar empresa"); ?>
            </button>
        </div>
    </script>

    <div class='col-xs-12'>

        <div class='row'>
            <div class='col-sm-12'>
                <div class='page-header'>
                    <h1 class='pull-left'>
                        <i class='icon-edit'></i>
                        <span><?= I18N::getExpression("Cadastro de empresa"); ?></span>
                    </h1>
                    <div class='pull-right close-button-container'>
                        <?= $objForm->renderCloseModalButtonIfExists(); ?>
                    </div>
                </div>
            </div>
        </div>

        <?= $objForm->getFormDebugPanel(); ?>
        <?= $objForm->getModalMessagesPanel(); ?>

        <div class='row'>
            <div class='col-sm-12'>
                <div class='box'>
                    <div class='box-header blue-background'>
                        <div class='title'>
                            <div class='icon-edit'></div>
                            <?= I18N::getExpression("Dados da empresa"); ?>
                        </div>
                        <div class='actions hidden'>
                            <a class="btn box-collapse btn-xs btn-link" href="javascript:void(0);"><i></i></a>
                        </div>
                    </div>
                    <div class='box-content'>

                        <form name="<?= $objForm->getFormName() ?>" class="form form-horizontal"
                              style="margin-bottom: 0;" novalidate>


                            <?php

                            $objCampo = $objForm->getTextInputInstance("nome", TextInput::TRANSFORMATION_DIRECTIVE_UPPER);
                            $objCampo->setLabel(I18N::getExpression("Nome"));

                            $objCampo->setDefaultValues();
                            $objCampo->setPlaceHolder("Informe o Nome.");
                            $objCampo->addValidation(new MaxLengthValidation(255, I18N::getExpression("O tamanho máximo do Nome deve ser de {0} caracteres.")));
                            $objCampo->addValidation(new RequiredValidation(I18N::getExpression("O Nome é de preenchimento obrigatório.")));

                            ?>

                            <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                 ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                                <?= $objCampo->renderLabel(); ?>
                                <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                    <?= $objCampo->render(); ?>
                                    <?= $objCampo->renderValidationBlock(); ?>

                                </div>


                            </div>

                            <?= $objForm->renderHorizontalLine(); ?>


                            <?php

                            $objCampo = $objForm->getSelectInstance("tipoEmpresaId");
                            $objCampo->setLabel(I18N::getExpression("Tipo de empresa"));

                            $objCampo->setPlaceHolder(I18N::getExpression("Selecione um tipo de empresa da lista..."), false);
                            $objCampo->setMatchExpression("{{nome}}");
                            $objCampo->setCloseOnSelect(true);

                            $propsFilter = new PropsFilter();
                            $propsFilter->addFieldToFilter("nome");

                            $selectChoiceContent = new SelectChoiceContent("tipoEmpresaId");
                            $selectChoiceContent->setMainString(new AngularString("nome", "div", Select::getSelectHighLightFilter()));

                            $objCampo->setFilter($propsFilter);
                            $objCampo->setSelectChoiceContent($selectChoiceContent);
                            $objCampo->addValidation(new RequiredValidation(I18N::getExpression("O Tipo de empresa é de preenchimento obrigatório.")));

                            ?>

                            <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                 ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                                <?= $objCampo->renderLabel(); ?>
                                <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                    <?= $objCampo->render(); ?>
                                    <?= $objCampo->renderValidationBlock(); ?>

                                </div>

                                <div class='col-md-2'>

                                    <?= $objCampo->renderAddEntityButton(I18N::getExpression("Adicionar Tipo de empresa")); ?>

                                </div>

                            </div>

                            <?= $objForm->renderHorizontalLine(); ?>

                            <?php

                            $objCampo = $objForm->getTextInputInstance("email");
                            $objCampo->setLabel(I18N::getExpression("Email"));

                            $objCampo->setDefaultValues();
                            $objCampo->setPlaceHolder(I18N::getExpression("Informe o Email."));
                            $objCampo->addValidation(new RequiredValidation(I18N::getExpression("O Email é de preenchimento obrigatório.")));
                            //$objCampo->addValidation(new EmailValidation(I18N::getExpression("O Email inválido.")));
                            ?>

                            <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                 ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                                <?= $objCampo->renderLabel(); ?>
                                <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                    <?= $objCampo->render(); ?>
                                    <?= $objCampo->renderValidationBlock(); ?>

                                </div>


                            </div>

                            <?= $objForm->renderHorizontalLine(); ?>



                            <?php

                            $objCampo = $objForm->getSelectInstance("tipoDocumentoId");
                            $objCampo->setLabel(I18N::getExpression("Tipo de documento"));

                            $objCampo->setPlaceHolder(I18N::getExpression("Selecione um tipo de documento da lista..."), false);
                            $objCampo->setMatchExpression("{{nome}}");
                            $objCampo->setCloseOnSelect(true);

                            $propsFilter = new PropsFilter();
                            $propsFilter->addFieldToFilter("nome");

                            $selectChoiceContent = new SelectChoiceContent("tipoDocumentoId");
                            $selectChoiceContent->setMainString(new AngularString("nome", "div", Select::getSelectHighLightFilter()));

                            $objCampo->setFilter($propsFilter);
                            $objCampo->setSelectChoiceContent($selectChoiceContent);
                            $objCampo->addValidation(new RequiredValidation(I18N::getExpression("O Tipo de documento é de preenchimento obrigatório.")));

                            ?>

                            <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                 ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                                <?= $objCampo->renderLabel(); ?>
                                <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                    <?= $objCampo->render(); ?>
                                    <?= $objCampo->renderValidationBlock(); ?>

                                </div>

                                <div class='col-md-2'>

                                    <?= $objCampo->renderAddEntityButton(I18N::getExpression("Adicionar Tipo de documento")); ?>

                                </div>

                            </div>

                            <?= $objForm->renderHorizontalLine(); ?>



                            <?php

                            $objCampo = $objForm->getTextInputInstance("numeroDocumento");
                            $objCampo->setLabel(I18N::getExpression("Número do documento"));

                            $objCampo->setDefaultValues();
                            $objCampo->setPlaceHolder(I18N::getExpression("Informe o Número do documento."));
                            $objCampo->addValidation(new RequiredValidation(I18N::getExpression("O Número do documento é de preenchimento obrigatório.")));

                            ?>

                            <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                 ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                                <?= $objCampo->renderLabel(); ?>
                                <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                    <?= $objCampo->render(); ?>
                                    <?= $objCampo->renderValidationBlock(); ?>

                                </div>


                            </div>

                            <?= $objForm->renderHorizontalLine(); ?>

                            <div class="col-sm-12 box box-nomargin box-collapsed omega-form-section-box">
                                <div class="box-header small blue-background">
                                    <div class="title">
                                        <div class='icon-phone'></div>
                                        <?= I18N::getExpression("Telefones") ?>
                                    </div>
                                    <div class="actions">
                                        <a class="btn box-collapse btn-xs btn-link" href="javascript: void(0);"><i></i></a>
                                    </div>
                                </div>
                                <div class="box-content">

                                    <?php

                                    $objCampo = $objForm->getPhoneInputInstance("telefone1");
                                    $objCampo->setLabel(I18N::getExpression("Telefone 1"));

                                    $objCampo->setDefaultValues();
                                    $objCampo->setPlaceHolder("Informe o Telefone 1.");
                                    $objCampo->addValidation(new RequiredValidation(I18N::getExpression("O Telefone 1 é de preenchimento obrigatório.")));

                                    ?>

                                    <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                         ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                                        <?= $objCampo->renderLabel(); ?>
                                        <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                            <?= $objCampo->render(); ?>
                                            <?= $objCampo->renderValidationBlock(); ?>

                                        </div>


                                    </div>

                                    <?= $objForm->renderHorizontalLine(); ?>



                                    <?php

                                    $objCampo = $objForm->getPhoneInputInstance("telefone2");
                                    $objCampo->setLabel(I18N::getExpression("Telefone 2"));

                                    $objCampo->setDefaultValues();
                                    $objCampo->setPlaceHolder("Informe o Telefone 2.");
                                    $objCampo->addValidation(new RequiredValidation(I18N::getExpression("O Telefone 2 é de preenchimento obrigatório.")));

                                    ?>

                                    <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                         ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                                        <?= $objCampo->renderLabel(); ?>
                                        <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                            <?= $objCampo->render(); ?>
                                            <?= $objCampo->renderValidationBlock(); ?>

                                        </div>


                                    </div>

                                    <?= $objForm->renderHorizontalLine(); ?>



                                    <?php

                                    $objCampo = $objForm->getPhoneInputInstance("fax");
                                    $objCampo->setLabel(I18N::getExpression("Fax"));

                                    $objCampo->setDefaultValues();
                                    $objCampo->setPlaceHolder("Informe o Fax.");
                                    $objCampo->addValidation(new RequiredValidation(I18N::getExpression("O Fax é de preenchimento obrigatório.")));

                                    ?>

                                    <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                         ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                                        <?= $objCampo->renderLabel(); ?>
                                        <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                            <?= $objCampo->render(); ?>
                                            <?= $objCampo->renderValidationBlock(); ?>

                                        </div>


                                    </div>

                                    <?= $objForm->renderHorizontalLine(); ?>



                                    <?php

                                    $objCampo = $objForm->getPhoneInputInstance("celular");
                                    $objCampo->setLabel(I18N::getExpression("Celular"));

                                    $objCampo->setDefaultValues();
                                    $objCampo->setPlaceHolder("Informe o Celular.");
                                    $objCampo->addValidation(new RequiredValidation(I18N::getExpression("O Celular é de preenchimento obrigatório.")));

                                    ?>

                                    <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                         ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                                        <?= $objCampo->renderLabel(); ?>
                                        <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                            <?= $objCampo->render(); ?>
                                            <?= $objCampo->renderValidationBlock(); ?>

                                        </div>


                                    </div>

                                    <!--
                                    <?= $objForm->renderHorizontalLine(); ?>

                                    <?php

                                    $objCampo = $objForm->getSelectInstance("operadoraId");
                                    $objCampo->setLabel(I18N::getExpression("Operadora"));

                                    $objCampo->setPlaceHolder(I18N::getExpression("Selecione uma operadora da lista..."), false);
                                    $objCampo->setMatchExpression("{{nome}}");
                                    $objCampo->setCloseOnSelect(true);

                                    $propsFilter = new PropsFilter();
                                    $propsFilter->addFieldToFilter("nome");

                                    $selectChoiceContent = new SelectChoiceContent("operadoraId");
                                    $selectChoiceContent->setMainString(new AngularString("nome", "div", Select::getSelectHighLightFilter()));

                                    $objCampo->setFilter($propsFilter);
                                    $objCampo->setSelectChoiceContent($selectChoiceContent);
                                    $objCampo->addValidation(new RequiredValidation(I18N::getExpression("A Operadora é de preenchimento obrigatório.")));

                                    ?>

                                    <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                         ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                                        <?= $objCampo->renderLabel(); ?>
                                        <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                            <?= $objCampo->render(); ?>
                                            <?= $objCampo->renderValidationBlock(); ?>

                                        </div>

                                        <div class='col-md-2'>

                                            <?= $objCampo->renderAddEntityButton(I18N::getExpression("Adicionar Operadora")); ?>

                                        </div>

                                    </div>

                                    <?= $objForm->renderHorizontalLine(); ?>

                                    <?php

                                    $objCampo = $objForm->getTextInputInstance("celularSms");
                                    $objCampo->setLabel(I18N::getExpression("Celular para SMS"));

                                    $objCampo->setDefaultValues();
                                    $objCampo->setPlaceHolder("Informe o Celular para SMS.");
                                    $objCampo->addValidation(new MaxLengthValidation(255, I18N::getExpression("O tamanho máximo do Celular para SMS deve ser de {0} caracteres.")));
                                    $objCampo->addValidation(new RequiredValidation(I18N::getExpression("O Celular para SMS é de preenchimento obrigatório.")));

                                    ?>

                                    <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                         ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                                        <?= $objCampo->renderLabel(); ?>
                                        <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                            <?= $objCampo->render(); ?>
                                            <?= $objCampo->renderValidationBlock(); ?>

                                        </div>

                                    </div>
                                    -->


                                </div>
                            </div>


                            <div class="col-sm-12 box box-nomargin box-collapsed omega-form-section-box">
                                <div class="box-header small blue-background">
                                    <div class="title">
                                        <div class='icon-map-marker'></div>
                                        <?= I18N::getExpression("Endereço") ?>
                                    </div>
                                    <div class="actions">
                                        <a class="btn box-collapse btn-xs btn-link" href="javascript: void(0);"><i></i></a>
                                    </div>
                                </div>
                                <div class="box-content">

                                    <?php

                                    $objCampo = $objForm->getTextInputInstance("logradouro");
                                    $objCampo->setLabel(I18N::getExpression("Logradouro"));

                                    $objCampo->setDefaultValues();
                                    $objCampo->setPlaceHolder("Informe o Logradouro.");
                                    $objCampo->addValidation(new MaxLengthValidation(255, I18N::getExpression("O tamanho máximo do Logradouro deve ser de {0} caracteres.")));
                                    $objCampo->addValidation(new RequiredValidation(I18N::getExpression("O Logradouro é de preenchimento obrigatório.")));

                                    ?>

                                    <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                         ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                                        <?= $objCampo->renderLabel(); ?>
                                        <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                            <?= $objCampo->render(); ?>
                                            <?= $objCampo->renderValidationBlock(); ?>

                                        </div>


                                    </div>

                                    <?= $objForm->renderHorizontalLine(); ?>



                                    <?php

                                    $objCampo = $objForm->getTextInputInstance("numero");
                                    $objCampo->setLabel(I18N::getExpression("Número"));

                                    $objCampo->setDefaultValues();
                                    $objCampo->setPlaceHolder("Informe o Número.");
                                    $objCampo->addValidation(new MaxLengthValidation(255, I18N::getExpression("O tamanho máximo do Número deve ser de {0} caracteres.")));
                                    $objCampo->addValidation(new RequiredValidation(I18N::getExpression("O Número é de preenchimento obrigatório.")));

                                    ?>

                                    <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                         ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                                        <?= $objCampo->renderLabel(); ?>
                                        <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                            <?= $objCampo->render(); ?>
                                            <?= $objCampo->renderValidationBlock(); ?>

                                        </div>


                                    </div>

                                    <?= $objForm->renderHorizontalLine(); ?>



                                    <?php

                                    $objCampo = $objForm->getTextInputInstance("complemento");
                                    $objCampo->setLabel(I18N::getExpression("Complemento"));

                                    $objCampo->setDefaultValues();
                                    $objCampo->setPlaceHolder("Informe o Complemento.");
                                    $objCampo->addValidation(new MaxLengthValidation(255, I18N::getExpression("O tamanho máximo do Complemento deve ser de {0} caracteres.")));
                                    $objCampo->addValidation(new RequiredValidation(I18N::getExpression("O Complemento é de preenchimento obrigatório.")));

                                    ?>

                                    <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                         ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                                        <?= $objCampo->renderLabel(); ?>
                                        <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                            <?= $objCampo->render(); ?>
                                            <?= $objCampo->renderValidationBlock(); ?>

                                        </div>


                                    </div>

                                    <?= $objForm->renderHorizontalLine(); ?>

                                    <?php

                                    $objCampo = $objForm->getSelectInstance("cidadeId");
                                    $objCampo->setLabel(I18N::getExpression("Cidade"));

                                    $objCampo->setPlaceHolder(I18N::getExpression("Selecione uma cidade da lista..."), false);
                                    $objCampo->setMatchExpression("{{nome}}");
                                    $objCampo->setCloseOnSelect(true);

                                    $propsFilter = new PropsFilter();
                                    $propsFilter->addFieldToFilter("nome");

                                    $selectChoiceContent = new SelectChoiceContent("cidadeId");
                                    $selectChoiceContent->setMainString(new AngularString("nome", "div", Select::getSelectHighLightFilter()));

                                    $objCampo->setFilter($propsFilter);
                                    $objCampo->setSelectChoiceContent($selectChoiceContent);
                                    $objCampo->addValidation(new RequiredValidation(I18N::getExpression("A Cidade é de preenchimento obrigatório.")));

                                    ?>

                                    <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                         ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                                        <?= $objCampo->renderLabel(); ?>
                                        <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                            <?= $objCampo->render(); ?>
                                            <?= $objCampo->renderValidationBlock(); ?>

                                        </div>

                                        <div class='col-md-2'>

                                            <?= $objCampo->renderAddEntityButton(I18N::getExpression("Adicionar Cidade")); ?>

                                        </div>

                                    </div>

                                    <?= $objForm->renderHorizontalLine(); ?>

                                    <?php

                                    $objCampo = $objForm->getSelectInstance("bairroId");
                                    $objCampo->setLabel(I18N::getExpression("Bairro"));

                                    $objCampo->setPlaceHolder(I18N::getExpression("Selecione um bairro da lista..."), false);
                                    $objCampo->setMatchExpression("{{nome}}");
                                    $objCampo->setCloseOnSelect(true);

                                    $propsFilter = new PropsFilter();
                                    $propsFilter->addFieldToFilter("nome");

                                    $selectChoiceContent = new SelectChoiceContent("bairroId");
                                    $selectChoiceContent->setMainString(new AngularString("nome", "div", Select::getSelectHighLightFilter()));

                                    $objCampo->setFilter($propsFilter);
                                    $objCampo->setSelectChoiceContent($selectChoiceContent);
                                    $objCampo->addValidation(new RequiredValidation(I18N::getExpression("O Bairro é de preenchimento obrigatório.")));

                                    ?>

                                    <div class='<?= $objCampo->getFormGroupCssClass() ?>'
                                         ng-class="<?= $objCampo->getFormGroupAngularClass(); ?>">

                                        <?= $objCampo->renderLabel(); ?>
                                        <div class='<?= $objCampo->getContainerCssClass(); ?>'>

                                            <?= $objCampo->render(); ?>
                                            <?= $objCampo->renderValidationBlock(); ?>

                                        </div>

                                        <div class='col-md-2'>

                                            <?= $objCampo->renderAddEntityButton(I18N::getExpression("Adicionar Bairro")); ?>

                                        </div>

                                    </div>


                                </div>

                            </div>

                            <div class='form-actions form-actions-padding-sm'>
                                <div class='row'>
                                    <div class='col-md-10 action-buttons-bar'>

                                        <?= $objForm->renderSubmitButton(); ?>

                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php

if (!Helper::isAngularModal())
{
    $arrDataServicesRelacionados = array("forms/operadora", "forms/tipoDocumento", "forms/tipoEmpresa", "forms/bairro", "forms/cidade", "forms/uf", "forms/pais");
    $arrControllersRelacionados = array("forms/operadora", "forms/tipoDocumento", "forms/tipoEmpresa", "forms/bairro", "forms/cidade", "forms/uf", "forms/pais");

    ?>

    <!--Definição do Controller Angular JS -->
    <script type="text/javascript">

        <?=Helper::incluirConteudoDataServices(true, $arrDataServicesRelacionados); ?>

    </script>

    <?= Helper::incluirControllerAngularJS(true, $arrControllersRelacionados); ?>

<?php } ?>

        
