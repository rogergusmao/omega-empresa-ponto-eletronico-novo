export enum Sexo
{
    NAO_DEFINIDO = null,
    MASCULINO = 1,
    FEMININO = 2
}