<?php

/*
*
* -------------------------------------------------------
* NOME DO FORMUL�RIO: uf
* DATA DE GERA��O:    30.06.2012
* ARQUIVO:            uf.php
* TABELA MYSQL:       uf
* BANCO DE DADOS:     ponto_eletronico
* -------------------------------------------------------
*
*/

if (isset($_GET["contador"]))
{

    $numeroRegistroInterno = Helper::GET("contador");
}

if (isset($identificadorRelacionamento) && is_numeric($identificadorRelacionamento))
{

    $objUf = new EXTDAO_Uf();
    $objUf->select($identificadorRelacionamento);
}
else
{

    $objUf = new EXTDAO_Uf();
}

$objArgUf = new Generic_Argument();
$objUf->formatarParaExibicao();

?>

<input type="hidden" name="uf_id_<?= $numeroRegistroInterno ?>" id="uf_id_<?= $numeroRegistroInterno ?>"
       value="<?= $objUf->getId(); ?>">

<table class="tabela_form">

    <tr class="tr_form">


        <?

        $objArgUf = new Generic_Argument();

        $objArgUf->numeroDoRegistro = "";
        $objArgUf->label = $objUf->label_nome;
        $objArgUf->valor = $objUf->getNome();
        $objArgUf->classeCss = "input_text";
        $objArgUf->classeCssFocus = "focus_text";
        $objArgUf->obrigatorio = true;
        $objArgUf->largura = 200;
        $objArgUf->nome = "uf_nome_{$numeroRegistroInterno}";
        $objArgUf->id = "uf_nome_{$numeroRegistroInterno}";

        ?>

        <td class="td_form_label"><?= $objArgUf->getLabel() ?></td>
        <td class="td_form_campo"><?= $objUf->campoTexto($objArgUf); ?></td>


        <?

        $objArgUf = new Generic_Argument();

        $objArgUf->numeroDoRegistro = "";
        $objArgUf->label = $objUf->label_nome_normalizado;
        $objArgUf->valor = $objUf->getNome_normalizado();
        $objArgUf->classeCss = "input_text";
        $objArgUf->classeCssFocus = "focus_text";
        $objArgUf->obrigatorio = false;
        $objArgUf->largura = 200;
        $objArgUf->nome = "uf_nome_normalizado_{$numeroRegistroInterno}";
        $objArgUf->id = "uf_nome_normalizado_{$numeroRegistroInterno}";

        ?>

        <td class="td_form_label"><?= $objArgUf->getLabel() ?></td>
        <td class="td_form_campo"><?= $objUf->campoTexto($objArgUf); ?></td>
    </tr>
    <tr class="tr_form">


        <?

        $objArgUf = new Generic_Argument();

        $objArgUf->numeroDoRegistro = "";
        $objArgUf->label = $objUf->label_sigla;
        $objArgUf->valor = $objUf->getSigla();
        $objArgUf->classeCss = "input_text";
        $objArgUf->classeCssFocus = "focus_text";
        $objArgUf->obrigatorio = true;
        $objArgUf->largura = 200;
        $objArgUf->nome = "uf_sigla_{$numeroRegistroInterno}";
        $objArgUf->id = "uf_sigla_{$numeroRegistroInterno}";

        ?>

        <td class="td_form_label"><?= $objArgUf->getLabel() ?></td>
        <td class="td_form_campo"><?= $objUf->campoTexto($objArgUf); ?></td>


        <?

        $objArgUf = new Generic_Argument();

        $objArgUf->numeroDoRegistro = "";
        $objArgUf->label = $objUf->label_corporacao_id_INT;
        $objArgUf->valor = $objUf->getCorporacao_id_INT();
        $objArgUf->classeCss = "input_text";
        $objArgUf->classeCssFocus = "focus_text";
        $objArgUf->obrigatorio = false;
        $objArgUf->largura = 200;
        $objArgUf->nome = "uf_corporacao_id_INT_{$numeroRegistroInterno}";
        $objArgUf->id = "uf_corporacao_id_INT_{$numeroRegistroInterno}";

        $objUf->addInfoCampos("corporacao_id_INT", $objArgUf->label, "TEXTO", $objArgUf->obrigatorio);

        ?>

        <td class="td_form_label"><?= $objArgUf->getLabel() ?></td>
        <td class="td_form_campo">
            <?= $objUf->objCorporacao->getComboBox($objArgUf); ?>
        </td>


    </tr>
    <tr>
        <td colspan="4" class="td_botao_remover_da_lista"><input class="botoes_form" type="button"
                                                                 value="Remover Estado"
                                                                 onclick="javascript:removerDivAjaxEmLista(this);"></td>
    </tr>
</table><br/>

